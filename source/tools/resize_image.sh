#!/bin/bash 

#dependecy imagemagic

#usage example
# ./resize_image origin_directory_name destination_directory_name

origin=$1
destination=$2

mkdir $destination/ -p

echo "Image resize started"

for file in $origin/*
do
filename=`basename "$file"`
W=`identify $origin/$filename | cut -f 3 -d " " | sed s/x.*//` #width
H=`identify $origin/$filename | cut -f 3 -d " " | sed s/.*x//` #height
echo $W
echo $H

if [ $W -ge $H ] 
then
    convert -resize 960X720 $origin/$filename $destination/$filename;
else
    convert -resize 720X960 $origin/$filename $destination/$filename
fi

done

echo "Image resize finished"
