<?php if ($profileModelCompleted) : ?>
    <?= $this->render("invoice_details", ["profileModel" => $profileModel]) ?>
<?php else: ?>
    <?= $this->render("billing_details", ["formModel" => $formModel]) ?>
<?php endif; ?>
