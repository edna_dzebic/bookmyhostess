<?php
$this->title = Yii::t('app.exhibitor', 'Checkout - overview');
?>

<?php if ($applyPaypalForm): ?>
    <div class="row margin-top-20">    
        <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
            <?=
            $this->render("paypal", [
                'payPalAmount' => $payPalAmount,
                "profileModel" => $profileModel,
                "coupon" => $coupon,
                'ids' => $ids,
            ])
            ?>
        </div>
    </div>   
<?php else: ?>
    <div class="row margin-top-20">    
        <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
            <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/complete-booking"]) ?>" class='btn btn-red'> <?= Yii::t('app.exhibitor', 'COMPLETE BOOKING') ?></a>
        </div>
    </div>   
<?php endif; ?>

<div class="row">
    <div class="col-lg-12 text-left">
        <h3 class="green-text"><?= Yii::t('app.exhibitor', 'Final overview'); ?></h3>
        <span><?= Yii::t('app.exhibitor', 'Please check one more time your selection and the information.') ?></span>
    </div>
</div>
<div class="row margin-top-20">
    <div class="col-xs-12">

        <div class="">

            <div class="row">
                <div class="col-lg-12 text-left">
                    <h4><?= Yii::t('app.exhibitor', 'You are going to book:'); ?></h4>
                </div>
            </div>

            <div class="table-responsive items">

                <table cellspacing="0">
                    <tr>
                        <td valign='top' align='center'>#</td>
                        <td valign='top' align='center'><?= Yii::t('app.exhibitor', 'Hostess name') ?></td>                      
                        <td valign='top' align='center'><?= Yii::t('app.exhibitor', 'Event name') ?></td>
                        <td valign='top' align='center'><?= Yii::t('app.exhibitor', 'Period') ?>
                        </td>
                        <td valign='top' align='center'><?= Yii::t('app.exhibitor', 'Days') ?></td>
                        <td valign='top' align='center'><?= Yii::t('app.exhibitor', 'Price (EUR)/<br/>Day') ?>
                        </td>
                        <td valign='top' align='center'><?= Yii::t('app.exhibitor', 'Amount <br/>(EUR)') ?><br/>
                        </td>
                    </tr>

                    <?php
                    $i = 0;

                    foreach ($models as $request)
                    {
                        ?>
                        <tr>
                            <td  valign='top'
                                 align='center'> <?= ++$i ?>
                            </td>
                            <td valign='top'
                                align='center'><?= $request->user->username; ?>
                            </td>   
                            <td valign='top'
                                align='center'><?= $request->event->name; ?>
                            </td>
                            <td  valign='top'
                                 align='center'><?= Yii::$app->util->formatDate($request->date_from); ?>
                                <br>
                                - 
                                <br>
                                <?= Yii::$app->util->formatDate($request->date_to); ?>
                            </td>
                            <td   valign='top'
                                  align='center'><?= $request->number_of_days; ?>
                            </td>                                                    
                            <td  valign='top'
                                 align='center'><?= Yii::$app->util->formatPrice($request->booking_price_per_day); ?>
                            </td>
                            <td   valign='top'
                                  align='right'><?= Yii::$app->util->formatPrice($request->total_booking_price) ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php if (Yii::$app->util->isGerman($profileModel->country_id)): ?>

                        <tr>
                            <td  colspan='6' align='left'>
                                <b><?= Yii::t('app.exhibitor', 'Net amount') ?></b>
                            </td>  
                            <td  align="right">
                                <?= Yii::$app->util->formatPrice($bookingAmount) ?>
                            </td>
                        </tr>
                        <tr>
                            <td  colspan='6' align='left'>
                                <b><?= Yii::t('app.exhibitor', '+19% value added tax') ?></b>
                            </td>   
                            <td  align="right">
                                <?= Yii::$app->util->formatPrice($taxAmount) ?>
                            </td>
                        </tr>
                        <tr>
                            <td  colspan='6' align='left'>
                                <b><?= Yii::t('app.exhibitor', 'Gross amount') ?></b>
                            </td>     
                            <td  align="right">
                                <b><?= Yii::$app->util->formatPrice($grossAmount) ?></b>
                            </td>
                        </tr>

                        <?php if (is_object($coupon)) : ?>


                            <tr>
                                <td  colspan='6' align='left'>
                                    <b><?=
                                        Yii::t('app.exhibitor', 'Discount for coupon with code {couponCode} - {couponNettoAmount} EUR', [
                                            "couponCode" => $coupon->code,
                                            "couponNettoAmount" => Yii::$app->util->formatPrice($couponNettoAmount)
                                        ])
                                        ?>
                                    </b>
                                </td>     
                                <td  align="right">

                                </td>
                            </tr>

                            <tr>
                                <td  colspan='6' align='left'>
                                    <b><?=
                                        Yii::t('app.exhibitor', 'Coupon 19% VAT - {couponTaxAmount} EUR', [
                                            "couponTaxAmount" => Yii::$app->util->formatPrice($couponTaxAmount)])
                                        ?>
                                    </b>
                                </td>     
                                <td  align="right">
                                    <b>- <?= Yii::$app->util->formatPrice($couponGrossAmount) ?></b>

                                </td>
                            </tr>
                            <tr>
                                <td  colspan='6' align='left'>
                                    <b><?= Yii::t('app.exhibitor', 'Total amount with discount') ?></b>
                                </td>     
                                <td  align="right">

                                    <b><?= Yii::$app->util->formatPrice($payPalAmount) ?></b>
                                </td>
                            </tr>
                        <?php endif; ?>

                    <?php else: ?>
                        <tr>
                            <td  colspan='6' align='left'>
                                <b><?= Yii::t('app.exhibitor', 'Total') ?> </b>
                            </td>  
                            <td  align="right">
                                <?= Yii::$app->util->formatPrice($grossAmount) ?>
                            </td>
                        </tr>


                        <?php if (is_object($coupon)) : ?>


                            <tr>
                                <td  colspan='6' align='left'>
                                    <b><?= Yii::t('app.exhibitor', 'Discount for coupon with code {couponCode}', ["couponCode" => $coupon->code]) ?></b>
                                </td>     
                                <td  align="right">
                                    - <?= Yii::$app->util->formatPrice($couponGrossAmount) ?>

                                </td>
                            </tr>
                            <tr>
                                <td  colspan='6' align='left'>
                                    <b><?= Yii::t('app.exhibitor', 'Total amount with discount') ?></b>
                                </td>     
                                <td  align="right">

                                    <b><?= Yii::$app->util->formatPrice($payPalAmount) ?></b>
                                </td>
                            </tr>
                        <?php endif; ?>

                    <?php endif; ?>


                </table>
            </div>
        </div>
    </div>
</div>
<div class="row margin-top-20">
    <div class="col-xs-12 ">

        <div class="">
            <div class='row'>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 text-left">
                            <h4><?= Yii::t('app.exhibitor', 'Invoice recipient:'); ?></h4>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Payment method') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $paymentMethodLabel ?>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Email') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->user->email ?>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Tax number') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->tax_number ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Company') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->company_name ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Street') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->street ?> <?= $profileModel->home_number ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'City') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->postal_code ?>&nbsp; <?= $profileModel->city ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Country') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= Yii::t('app', $profileModel->country->name) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row margin-top-20">
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
        <?php if ($applyPaypalForm): ?>

            <?=
            $this->render("paypal", [
                'payPalAmount' => $payPalAmount,
                "profileModel" => $profileModel,
                "coupon" => $coupon,
                'ids' => $ids,
            ])
            ?>
        <?php else : ?>
            <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/complete-booking"]) ?>" class='btn btn-red'> <?= Yii::t('app.exhibitor', 'COMPLETE BOOKING') ?></a>
        <?php endif; ?>
    </div>
</div>
<div class="row margin-top-20 ">
    <div class="col-xs-12 text-center-xs">
        <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/payment"]) ?>" class='green-text'> <?= Yii::t('app.exhibitor', 'Back to payment method') ?></a>
    </div>
</div>