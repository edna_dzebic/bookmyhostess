<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app.exhibitor', 'Checkout - Invoice recipient');
?>


<?php $form = ActiveForm::begin(['id' => 'form']); ?>
<div class="row margin-top-20">    
    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
        <?= Html::submitButton(Yii::t('app.exhibitor', 'SAVE AND CONTINUE TO METHOD OF PAYMENT'), ['class' => 'btn btn-red']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-left">
        <h3 class="green-text"><?= Yii::t('app.exhibitor', 'Invoice recipient'); ?></h3>
        <span><?= Yii::t('app.exhibitor', 'Please complete all fields below in order to proceed booking process.') ?></span>
    </div>
</div>
<div class="row margin-top-20">
    <div class="col-xs-12 ">
        <div class="">
            <div class="row">
                <div class="col-xs-12 text-left">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($formModel, 'billing_company_name')->textInput(['maxlength' => 255, 'placeholder' => Yii::t('app.exhibitor', 'Company*')])->label(false) ?>
                        </div>
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($formModel, 'billing_tax_number')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.exhibitor', 'Tax number*')])->label(false) ?>
                        </div>
                    </div>     
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($formModel, 'billing_street')->textInput(['maxlength' => 255, 'placeholder' => Yii::t('app.exhibitor', 'Street*')])->label(false) ?>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($formModel, 'billing_home_number')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.exhibitor', 'House number*')])->label(false) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($formModel, 'billing_postal_code')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.exhibitor', 'Postal code*')])->label(false) ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($formModel, 'billing_city')->textInput(['maxlength' => 255, 'placeholder' => Yii::t('app.exhibitor', 'City*')])->label(false) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($formModel, 'billing_country')->dropDownList(\common\models\Country::getListForDropdown(), ["prompt" => \Yii::t('app.exhibitor', '- Please select country -')])->label(false) ?>
                        </div>
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($formModel, 'phone')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.exhibitor', 'Phone*')])->label(false) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($formModel, 'website')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.exhibitor', 'Website')])->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row margin-top-20">
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
        <?= Html::submitButton(Yii::t('app.exhibitor', 'SAVE AND CONTINUE TO METHOD OF PAYMENT'), ['class' => 'btn btn-red']) ?>
    </div>
</div>
<div class="row margin-top-20 ">
    <div class="col-xs-12 text-center-xs">
        <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/index"]) ?>" class='green-text'> <?= Yii::t('app.exhibitor', 'Back to requests overview') ?></a>
    </div>
</div>

<?php ActiveForm::end(); ?>


