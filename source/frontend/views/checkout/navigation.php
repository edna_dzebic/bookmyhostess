<div class="row">
    <div class="card">
        <div class="card-body mb-4">

            <h2 class="text-center font-weight-bold pt-4 pb-5">
                <strong>
                    <?= Yii::t('app.exhibitor', 'Booking') ?>
                </strong>
            </h2>
            
            <br>

            <div class="steps-form">
                <div class="steps-row setup-panel">

                    <?php
                    foreach ($steps as $step) :
                        $class= '';

                        if ($activeStep == $step["index"])
                        {
                            $class = 'active';
                        }
                        else if ($activeStep > $step["index"])
                        {
                            $class = 'completed';
                        }
                        else {
                            $class = 'disabled';
                        }
                        ?>
                        <div class="steps-step">
                            <a href="<?= $step["url"] ?>" type="button" class="
                               btn btn-default btn-circle 
                               <?= $class ?>
                               ">
                                   <?= $step["index"] ?>
                            </a>
                            <p><?= $step["label"] ?></p>
                        </div> 
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
