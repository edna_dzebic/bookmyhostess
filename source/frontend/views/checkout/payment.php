<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app.exhibitor', 'Checkout - Method of payment ');
?>

<?php $form = ActiveForm::begin(['id' => 'payment-form']); ?>

<div class="row margin-top-20">    
    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
        <?= Html::submitButton(Yii::t('app.exhibitor', 'CONTINUE TO FINAL OVERVIEW'), ['class' => 'btn btn-red']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-left">
        <h3 class="green-text"><?= Yii::t('app.exhibitor', 'Method of payment'); ?></h3>
        <span><?= Yii::t('app.exhibitor', 'Please select one of the offered payment methods. You will be able to check all the booking details again in the next step.') ?></span>
    </div>
</div>
<div class="row margin-top-20">
    <div class="col-xs-12 ">
        <div class="">
            <div class="row">
                <div class="col-xs-12 text-left">

                    <hr>
                    <?php foreach ($allowedPayments as $value) : ?>
                        <div class="row margin-top-20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment-method-cont">
                                <?=
                                Html::radio("payment_type", $selectedPayment == $value["value"], [
                                    "value" => $value["value"],
                                    "label" => $value["label"],
                                    "class" => "payment-method-radio"
                                ])
                                ?>
                                <p class="payment-description">
                                    <?= $value["description"] ?>
                                </p>
                            </div>
                        </div> 
                        <hr>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row margin-top-20">
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
        <?= Html::submitButton(Yii::t('app.exhibitor', 'CONTINUE TO FINAL OVERVIEW'), ['class' => 'btn btn-red']) ?>
    </div>
</div>
<div class="row margin-top-20 ">
    <div class="col-xs-12 text-center-xs">
        <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/invoice-details"]) ?>" class='green-text'> <?= Yii::t('app.exhibitor', 'Back to invoice details') ?></a>
    </div>
</div>


<?php ActiveForm::end(); ?>