<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app.exhibitor', 'Checkout - Requests overview');
?>

<div class="row margin-top-20">    
    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
        <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/invoice-details"]) ?>" class='btn btn-red'> <?= Yii::t('app.exhibitor', 'CONTINUE TO INVOICE DETAILS') ?></a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-left">
        <h3 class="green-text"><?= Yii::t('app.exhibitor', 'Requests overview'); ?></h3>
        <span><?= Yii::t('app.exhibitor', 'This table illustrates the booking fee for the selected person. The daily rate will be paid after the fair against an invoice.') ?></span>
    </div>
</div>
<div class="row margin-top-20">
    <div class="col-xs-12">
        <div class="">

            <div class="row">
                <div class="col-lg-12 text-left">
                    <h4><?= Yii::t('app.exhibitor', 'Requests'); ?></h4>
                </div>
            </div>

            <div class="table-responsive items">

                <table cellspacing="0">
                    <thead>
                        <tr>
                            <td valign='top' align='center' >#</td>
                            <td valign='top' align='center' ><?= Yii::t('app.exhibitor', 'Hostess name') ?></td>                      
                            <td valign='top' align='center' ><?= Yii::t('app.exhibitor', 'Event name') ?></td>
                            <td valign='top' align='center' ><?= Yii::t('app.exhibitor', 'Period') ?>
                            </td>
                            <td valign='top' align='center' ><?= Yii::t('app.exhibitor', 'Days') ?></td>
                            <td valign='top' align='center' ><?= Yii::t('app.exhibitor', 'Price (EUR)/<br/>Day') ?>
                            </td>
                            <td valign='top' align='center' ><?= Yii::t('app.exhibitor', 'Amount <br/>(EUR)') ?><br/>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;

                        foreach ($models as $request)
                        {
                            ?>
                            <tr>
                                <td valign='top'
                                    align='center'> <?= ++$i ?>
                                </td>
                                <td valign='top'
                                    align='center'><?= $request->user->username; ?>
                                </td>   
                                <td valign='top'
                                    align='center'><?= $request->event->name; ?>
                                </td>
                                <td  valign='top'
                                     align='center'><?= Yii::$app->util->formatDate($request->date_from); ?>
                                    <br>
                                    - 
                                    <br>
                                    <?= Yii::$app->util->formatDate($request->date_to); ?>
                                </td>
                                <td   valign='top'
                                      align='center'><?= $request->number_of_days; ?>
                                </td>                                                    
                                <td  valign='top'
                                     align='center'><?= Yii::$app->util->formatPrice($request->booking_price_per_day); ?>
                                </td>
                                <td   valign='top'
                                      align='right'><?= Yii::$app->util->formatPrice($request->total_booking_price) ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if (Yii::$app->util->isGerman($profileModel->country_id)): ?>

                            <tr>
                                <td  colspan='6' align='left'>
                                    <b><?= Yii::t('app.exhibitor', 'Net amount') ?></b>
                                </td>  
                                <td  align="right">
                                    <?= Yii::$app->util->formatPrice($bookingAmount) ?>
                                </td>
                            </tr>
                            <tr>
                                <td  colspan='6' align='left'>
                                    <b><?= Yii::t('app.exhibitor', '+19% value added tax') ?></b>
                                </td>   
                                <td  align="right">
                                    <?= Yii::$app->util->formatPrice($taxAmount) ?>
                                </td>
                            </tr>
                            <tr>
                                <td  colspan='6' align='left'>
                                    <b><?= Yii::t('app.exhibitor', 'Gross amount') ?></b>
                                </td>     
                                <td  align="right">
                                    <b><?= Yii::$app->util->formatPrice($grossAmount) ?></b>
                                </td>
                            </tr>

                            <?php if (is_object($coupon)) : ?>


                                <tr>
                                    <td  colspan='6' align='left'>
                                        <b><?=
                                            Yii::t('app.exhibitor', 'Discount for coupon with code {couponCode} - {couponNettoAmount} EUR', [
                                                "couponCode" => $coupon->code,
                                                "couponNettoAmount" => Yii::$app->util->formatPrice($couponNettoAmount)
                                            ])
                                            ?>
                                        </b>
                                    </td>     
                                    <td  align="right">

                                    </td>
                                </tr>

                                <tr>
                                    <td  colspan='6' align='left'>
                                        <b><?=
                                            Yii::t('app.exhibitor', 'Coupon 19% VAT - {couponTaxAmount} EUR', [
                                                "couponTaxAmount" => Yii::$app->util->formatPrice($couponTaxAmount)])
                                            ?>
                                        </b>
                                    </td>     
                                    <td  align="right">
                                        <b>- <?= Yii::$app->util->formatPrice($couponGrossAmount) ?></b>

                                    </td>
                                </tr>
                                <tr>
                                    <td  colspan='6' align='left'>
                                        <b><?= Yii::t('app.exhibitor', 'Total amount with discount') ?></b>
                                    </td>     
                                    <td  align="right">

                                        <b><?= Yii::$app->util->formatPrice($payPalAmount) ?></b>
                                    </td>
                                </tr>
                            <?php endif; ?>

                        <?php else: ?>
                            <tr>
                                <td  colspan='6' align='left'>
                                    <b><?= Yii::t('app.exhibitor', 'Total') ?> </b>
                                </td>  
                                <td  align="right">
                                    <?= Yii::$app->util->formatPrice($grossAmount) ?>
                                </td>
                            </tr>


                            <?php if (is_object($coupon)) : ?>


                                <tr>
                                    <td  colspan='6' align='left'>
                                        <b><?= Yii::t('app.exhibitor', 'Discount for coupon with code {couponCode}', ["couponCode" => $coupon->code]) ?></b>
                                    </td>     
                                    <td  align="right">
                                        - <?= Yii::$app->util->formatPrice($couponGrossAmount) ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td  colspan='6' align='left'>
                                        <b><?= Yii::t('app.exhibitor', 'Total amount with discount') ?></b>
                                    </td>     
                                    <td  align="right">

                                        <b><?= Yii::$app->util->formatPrice($payPalAmount) ?></b>
                                    </td>
                                </tr>
                            <?php endif; ?>

                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<?php if ($payPalAmount > 0) : ?>

    <div class="row margin-top-20">
        <div class="col-xs-12">

            <div class="">

                <div class="row">
                    <div class="col-lg-12 text-left">
                        <h4><?= Yii::t('app.exhibitor', 'Coupon:'); ?></h4>
                    </div>
                </div>

                <?php
                $form = ActiveForm::begin([
                            'action' => ['checkout/apply-coupon'],
                            'id' => 'coupon-form'
                ]);
                ?>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="row ">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <?= Yii::t('app.exhibitor', 'Do you have a coupon? Apply it before payment and use your discount.') ?>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                                <input type="text" class="form-control" name="coupon" value="<?= isset($coupon) ? $coupon->code : '' ?>" placeholder="<?= Yii::t('app.exhibitor', 'Enter the voucher code') ?>">
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">

                                <?= Html::submitButton(Yii::t('app.exhibitor', 'Redeem the voucher'), ['class' => 'btn btn-default full-width', 'name' => 'coupon-button']) ?>

                            </div>
                        </div>
                        <div class="row">
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
    <br>
<?php endif; ?>

<div class="row margin-top-20">
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
        <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/invoice-details"]) ?>" class='btn btn-red'> <?= Yii::t('app.exhibitor', 'CONTINUE TO INVOICE DETAILS') ?></a>
    </div>
</div>
<div class="row margin-top-20 ">
    <div class="col-xs-12 text-center-xs">
        <a href="<?= Yii::$app->urlManager->createUrl(["/exhibitor/event-request"]) ?>" class='green-text'> <?= Yii::t('app.exhibitor', 'Back to requests') ?></a>
    </div>
</div>

