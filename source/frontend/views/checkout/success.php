<?php
\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Checkout - success');
?>

<div class="row margin-top-20">
    <div class="col-xs-12">
        <div class="">
            <div class="row text-center">
                <div class="col-xs-12">
                    <h4><?= Yii::t('app.exhibitor', 'Your booking is now complete.'); ?></h4>
                    <br>
                    <h4><?= Yii::t('app.exhibitor', 'Thank you for using our platform!'); ?></h4>
                </div>
            </div>

            <div class="row ">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center margin-top-20">
                    <a href="<?= Yii::$app->urlManager->createUrl(["/"]) ?>" class='btn btn-default'> <?= Yii::t('app.exhibitor', 'Back to home page') ?></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center margin-top-20">
                    <a href="<?= $invoiceLink ?>" target="_blank" class='btn btn-red'> <?= Yii::t('app.exhibitor', 'DOWNLOAD INVOICE') ?></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
                    
                </div>
            </div>
        </div>
    </div>
</div>




