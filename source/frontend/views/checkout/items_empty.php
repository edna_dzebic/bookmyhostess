<?php
\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Checkout - Requests overview');
?>

<div class="row margin-top-20">
    <div class="col-xs-12">
        <div class="padding-bordered-cont">
            <div class="row text-center">
                <div class="col-xs-12">
                    <h4><?= Yii::t('app.exhibitor', 'You do not have any requests selected for booking.'); ?></h4>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">

                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3 ">
                    <a href="<?= Yii::$app->urlManager->createUrl(["/exhibitor/event-request"]) ?>" class='btn btn-green'> <?= Yii::t('app.exhibitor', 'To requests ') ?></a>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">

                </div>
            </div>
        </div>
    </div>
</div>




