<?php

use yii\helpers\Html;

$this->title = Yii::t('app.exhibitor', 'Checkout - Invoice recipient');
?>
<div class="row margin-top-20">    
    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
        <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/payment"]) ?>" class='btn btn-red'> <?= Yii::t('app.exhibitor', 'CONTINUE TO METHOD OF PAYMENT') ?></a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-left">
        <h3 class="green-text"><?= Yii::t('app.exhibitor', 'Invoice recipient'); ?></h3>
        <span><?= Yii::t('app.exhibitor', 'Please check the invoice details that will appear on your invoice.') ?></span>
    </div>
</div>
<div class="row margin-top-20">
    <div class="col-xs-12 ">

        <div class="">
            <div class='row'>
                <div class="col-xs-12">
                    <div class='row'>
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Email') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->user->email ?>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Tax number') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->tax_number ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Company') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->company_name ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Street') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->street ?>  <?= $profileModel->home_number ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'City') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= $profileModel->postal_code ?>&nbsp; <?= $profileModel->city ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            <label><b><?= Yii::t('app.exhibitor', 'Country') ?></b></label>
                        </div>
                        <div class="col-xs-8">
                            <?= Yii::t('app', $profileModel->country->name) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <?= Html::a(Yii::t('app.exhibitor', 'Change data'), ["exhibitor/company", "ref" => "/checkout/payment", "edit" => true], ["class" => "green-text"]) ?>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="row margin-top-20">
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 hidden-xs"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right text-center-xs">
        <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/payment"]) ?>" class='btn btn-red'> <?= Yii::t('app.exhibitor', 'CONTINUE TO METHOD OF PAYMENT') ?></a>
    </div>
</div>
<div class="row margin-top-20 ">
    <div class="col-xs-12 text-center-xs">
        <a href="<?= Yii::$app->urlManager->createUrl(["/checkout/index"]) ?>" class='green-text'> <?= Yii::t('app.exhibitor', 'Back to requests overview') ?></a>
    </div>
</div>
