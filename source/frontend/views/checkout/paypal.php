<?php

use yii\helpers\Url;
?>
<form action="<?= Yii::$app->params["paypal"]["url"] ?>" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="<?= Yii::$app->params["paypal"]['business'] ?>">
    <input type="hidden" name="item_name" value="<?= Yii::$app->params["paypal"]['item_name'] ?>">
    <input type="hidden" name="item_number" value="<?= $ids ?>">
    <input type="hidden" name="amount" value="<?= $payPalAmount; ?>">
    <input type="hidden" name="quantity" value="1">
    <input type="hidden" name="no_note" value="0">
    <input type="hidden" name="currency_code" value="<?= Yii::$app->params["paypal"]['currency_code'] ?>">
    <input type="hidden" name="first_name" value="<?= $profileModel->user->first_name; ?>">
    <input type="hidden" name="last_name" value="<?= $profileModel->user->last_name; ?>">
    <input type="hidden" name="address1"
           value="<?= isset($profileModel->street) ? $profileModel->street : ""; ?>">
    <input type="hidden" name="city"
           value="<?= isset($profileModel->city) ? $profileModel->city : ""; ?>">
    <input type="hidden" name="country"
           value="<?= isset($profileModel->country) ? $profileModel->country->name : ""; ?>">
    <input type="hidden" name="custom" value="<?= sha1(Yii::$app->params['paypal']['custom']) . "_" . $profileModel->user->id; ?>">
    <input type="hidden" name="notify_url" value="<?= Url::to([Yii::$app->params["paypal"]['notify_url']], true); ?>">
    <input type="hidden" name="return" value="<?= Url::to([Yii::$app->params["paypal"]['return']], true); ?>">
    <input type="hidden" name="on0" value="opc" >
    <input type="hidden" name="os0" value="<?= isset($coupon) ? $coupon->code : '' ?>">
    <input type="image" name="submit" border="0"
           src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif"
           alt="PayPal - The safer, easier way to pay online">
</form>