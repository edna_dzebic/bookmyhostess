<?php

\frontend\assets\CheckoutNewAsset::register($this);
\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Checkout');
?>

<?php if ($hideNavigation === false) : ?>

    <?= $this->render("navigation", ["steps" => $steps, "activeStep" => $activeStep]) ?>

<?php endif; ?>

<?= $this->render("//partials/_flash") ?>

<?= $this->render($view, $data) ?>

