<table style="width: 100%;">
    <tr>
        <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
        </td>
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td style="font-size: 18px; font-weight: bold;" class="green-text">
            <?= Yii::t('app.exhibitor', 'Working experience'); ?>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width: 100%;" class="table-padding table-bordered">
                <thead>
                    <tr>
                        <td><?= Yii::t('app.exhibitor', 'Starting date'); ?></td>
                        <td><?= Yii::t('app.exhibitor', 'Ending date'); ?></td>
                        <td><?= Yii::t('app.exhibitor', 'Event name'); ?></td>
                        <td><?= Yii::t('app.exhibitor', 'Job description'); ?></td>
                        <td><?= Yii::t('app.exhibitor', 'Position'); ?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($model->pastWork) && count($model->pastWork) > 0)
                    {

                        foreach ($model->pastWork as $pastWorkModel)
                        {
                            ?>
                            <tr>
                                <td><?= Yii::$app->util->formatDate($pastWorkModel->start_date); ?></td>
                                <td><?= Yii::$app->util->formatDate($pastWorkModel->end_date); ?></td>
                                <td><?= $pastWorkModel->event_name ?></td>
                                <td><?= $pastWorkModel->job_description ?></td>
                                <td><?= $pastWorkModel->position ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
        </td>
    </tr>
</table>


