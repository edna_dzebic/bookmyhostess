<?php ?>

<table style="width: 100%;">     
    <tr>
        <td colspan="2" style="font-size: 18px; font-weight: bold;"  class="green-text">
            <?= Yii::t('app.exhibitor', 'Statistics') ?>
        </td>
    </tr>
    <tr style="width: 100%;">
        <td style="width: 50%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 40%;"> <?= Yii::t('app.exhibitor', 'Last login:'); ?> </td>
                    <td style="width: 60%;"><?= Yii::$app->util->formatDate($model->user->last_login) ?></td>
                </tr>

                <tr>
                    <td style="width: 40%;"> <?= Yii::t('app.exhibitor', 'Request:'); ?> </td>
                    <td style="width: 60%;"> <?= $model->getTotalRequestsCount(); ?></td>
                </tr>

                <tr>
                    <td style="width: 40%;"><?= Yii::t('app.exhibitor', 'Rejected:'); ?> </td>
                    <td style="width: 60%;"><?= $model->getRejectedRequestCount(); ?></td>
                </tr>
                <tr>
                    <td style="width: 40%;"> <?= Yii::t('app.exhibitor', 'Not answered:'); ?> </td>
                    <td style="width: 60%;"> <?= $model->getNotAnsweredRequestCount(); ?></td>
                </tr>
            </table>
        </td>
        <td style="width: 50%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 40%;"> <?= Yii::t('app.exhibitor', 'Profile views:'); ?></td>
                    <td> <?= $model->profile_views ?></td>
                </tr>
                <tr>
                    <td style="width: 40%;"> <?= Yii::t('app.exhibitor', 'Time to respond:'); ?> </td>
                    <td>  <?= $model->getTimeToRespond(); ?></td>
                </tr>
                <tr>
                    <td style="width: 40%;">  <?= Yii::t('app.exhibitor', 'Confirmed:'); ?> </td>
                    <td> <?= $model->getConfirmedRequestsCount(); ?></td>
                </tr>
                <tr>
                    <td style="width: 40%;"> <?= Yii::t('app.exhibitor', 'Booked:'); ?> </td>
                    <td> <?= $model->getBookedRequestCount(); ?></td>
                </tr>

            </table>
        </td>
    </tr>
</table>
