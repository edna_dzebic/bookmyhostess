<table>  
    <tr>
        <td><?= Yii::t('app.exhibitor', 'Graduation:'); ?> </td>
        <td><?= $model->education_graduation ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('app.exhibitor', 'University:'); ?> </td>
        <td><?= $model->education_university ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('app.exhibitor', 'Vocational Training:'); ?> </td>
        <td><?= $model->education_vocational_training ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('app.exhibitor', 'Profession:'); ?> </td>
        <td><?= $model->education_profession ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('app.exhibitor', 'Special Knowledge:'); ?> </td>
        <td><?= $model->education_special_knowledge ?></td>
    </tr>
</table>