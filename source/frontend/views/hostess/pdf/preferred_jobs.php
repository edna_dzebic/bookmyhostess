<table style="width: 100%;">
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td style="font-size: 18px; font-weight: bold;"  class="green-text">
            <?= Yii::t('app.exhibitor', 'Preferred jobs'); ?>
        </td>
    </tr>


    <tr>
        <td>
            <?php if (empty($model->preferredJobs)) : ?>
                <?= Yii::t('app.exhibitor', 'No entry yet') ?>
            <?php else: ?>

                <?= $model->getTranslatedPreferredJobs() ?>

            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
        </td>
    </tr>
</table>