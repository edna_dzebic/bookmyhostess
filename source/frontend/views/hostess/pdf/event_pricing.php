<?php if (is_object($eventReg)): ?>

    <table style="width: 100%;">
        <tr>
            <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
            </td>
        </tr>
        <tr>
            <td height="20">           
            </td>
        </tr>
        <tr>
            <td style="font-size: 18px; font-weight: bold;"  class="green-text">
                <?= Yii::t('app.exhibitor', 'Availability'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <table class="table-padding  table-bordered" style="width: 100%;">
                    <thead>
                        <tr>

                            <td style="text-align: left;"><?= Yii::t('app.exhibitor', 'Event'); ?></td>
                            <td style="text-align: center;"><?= Yii::t('app.exhibitor', 'Price per day'); ?><sup>2, 4</sup></td>
                            <td style="text-align: center;"><?= Yii::t('app.exhibitor', 'Overtime hour'); ?><sup>3, 4</sup></td>
                            <td style="text-align: center;"><?= Yii::t('app.exhibitor', 'Booking charge/day'); ?><sup>1, 4</sup></td>

                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td style="text-align: left;"> <?= $eventReg->event->name ?></td>
                            <td style="text-align: center;"> <?= Yii::$app->util->formatEventPrice($eventReg->event, $eventReg->price) ?></td>
                            <td style="text-align: center;"><?= Yii::$app->util->formatPrice(15) . " " . Yii::t('app.exhibitor', 'EUR') ?></td>
                            <td style="text-align: center;"> <?= Yii::$app->util->formatPrice(Yii::$app->settings->booking_price) . " " . Yii::t('app.exhibitor', 'EUR') ?></td>
                        </tr>

                        <tr>
                            <td colspan="4">
                                <sup>1</sup> <?= Yii::t('app.exhibitor', 'Payment is due on completion of the booking process.') ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <sup>2</sup> <?= Yii::t('app.exhibitor', 'Payment is due latest on the last day of the trade show/event directly to the personnel.') ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <sup>3</sup> <?= Yii::t('app.exhibitor', 'Official trade show opening hours are considered as one working day. If a working day is longer than 9 hours over time fee applies for the rest of the time.') ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <sup>4</sup> <?= Yii::t('app.exhibitor', 'For late bookings made {days} or less days before the fair, a surcharge of {percentage}% will apply.', ["days" => Yii::$app->settings->last_minute_days, "percentage" => Yii::$app->settings->last_minute_percentage]) ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <?= Yii::t('app.exhibitor', '19% vat applies for companies based in Germany') ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="20">           
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
            </td>
        </tr>
    </table>

<?php endif; ?>