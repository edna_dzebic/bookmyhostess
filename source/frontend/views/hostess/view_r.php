<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

\frontend\assets\HostessViewAsset::register($this);

$this->title = $model->user->username;

$prevClass = "col-lg-3 col-md-3 col-sm-3 col-xs-12 prev-profile";
$middleClass = "col-lg-6 col-md-6 col-sm-6 col-xs-12 to-results";
$nextClass = "col-lg-3 col-md-3 col-sm-3  col-xs-12 next-profile";

if (isset($prev) && !isset($next))
{
    $middleClass = "col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center to-results";
}
if (isset($prev) && isset($next))
{
    $middleClass = "col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center to-results";
}
if (!isset($prev) && isset($next))
{
    $middleClass = "col-lg-9 col-md-9 col-sm-9 col-xs-12 text-left to-results";
}
if (!isset($prev) && !isset($next))
{
    $middleClass = "col-lg-12 col-md-12 col-sm-12 col-xs-12 to-results text-left";
}
?>

<?= $this->render("//partials/_flash") ?>

<div class="text-center loading-content">
    <div>
        <?= Yii::t('app.exhibitor', 'Please wait...') ?>
    </div>
    <br>
    <div class="loader js-loader"></div>    
</div>

<div class="row js-hostes-view-profile hostess-view-profile">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-lg-12 text-left">
                <?php
                if ($model->user_id === Yii::$app->user->id)
                {
                    ?>
                    <span class="">
                        <?=
                        Html::a(
                                Yii::t('app.exhibitor', '< Go back to profile'), Yii::$app->urlManager->createUrl(["//hostess/profile"]), ["class" => "go-back-link"]
                        );
                        ?>
                    </span>
                <?php } ?>
                <?php
                if (Yii::$app->user->identity->isExhibitor)
                {
                    ?>
                    <div class="row hidden-xs">
                        <?php if (isset($prev)) : ?>
                            <div class="<?= $prevClass ?>">                        
                                <?= Html::img($prev->hostessProfile->getThumbnailImageUrl(35, 50), ["class" => "prev-profile-image xs-small-profile-image hover"]) ?>
                                <i class="glyphicon glyphicon-chevron-left"></i>
                                <?= Html::a(Yii::t('app.exhibitor', 'Previous profile'), ["hostess/view", "id" => $prev->user->id, "event" => $event], ['id' => 'hostess-go-back', 'class' => 'go-back-link']); ?>
                            </div>
                        <?php endif; ?>

                        <div class="<?= $middleClass ?>">                    
                            <i class="glyphicon glyphicon-list"></i>
                            <?= Html::a(Yii::t('app.exhibitor', 'To results'), $goBackUrl, ['id' => 'hostess-go-back', 'class' => 'go-back-link']); ?>
                        </div>

                        <?php if (isset($next)) : ?>
                            <div class="<?= $nextClass ?>">
                                <?= Html::a(Yii::t('app.exhibitor', 'Next profile'), ["hostess/view", "id" => $next->user->id, "event" => $event], ['id' => 'hostess-go-back', 'class' => 'go-back-link']); ?>
                                <i class="glyphicon glyphicon-chevron-right"></i>
                                <?= Html::img($next->hostessProfile->getThumbnailImageUrl(35, 50), ["class" => "next-profile-image xs-small-profile-image hover"]) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="row visible-xs">

                        <div class="col-xs-12 text-center">
                            <i class="glyphicon glyphicon-list"></i>
                            <?= Html::a(Yii::t('app.exhibitor', 'To results'), $goBackUrl, ['id' => 'hostess-go-back', 'class' => 'go-back-link']); ?>
                        </div>

                        <?php if (isset($prev)) : ?>
                            <div class="col-xs-6 text-left margin-top-10 prev-profile">                        
                                <?= Html::img($prev->hostessProfile->getThumbnailImageUrl(20, 20), ["class" => "prev-profile-image xs-small-profile-image hover"]) ?>
                                <i class="glyphicon glyphicon-chevron-left"></i>
                                <?= Html::a(Yii::t('app.exhibitor', 'Previous profile'), ["hostess/view", "id" => $prev->user->id, "event" => $event], ['id' => 'hostess-go-back', 'class' => 'go-back-link']); ?>
                            </div>
                        <?php endif; ?>



                        <?php if (isset($next)) : ?>
                            <div class="col-xs-6 text-right margin-top-10 next-profile">
                                <?= Html::a(Yii::t('app.exhibitor', 'Next profile'), ["hostess/view", "id" => $next->user->id, "event" => $event], ['id' => 'hostess-go-back', 'class' => 'go-back-link']); ?>
                                <i class="glyphicon glyphicon-chevron-right"></i>
                                <?= Html::img($next->hostessProfile->getThumbnailImageUrl(20, 20), ["class" => "next-profile-image xs-small-profile-image hover"]) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 margin-top-10">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="green-line"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 bh-title">
                        <?= $model->user->username; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <?= $this->render("view/images", ["model" => $model]) ?>
                        </div>    
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">    
                        <div class="row">
                            <?=
                            $this->render("view/rating_info", [
                                "model" => $model,
                                "eventReg" => $eventReg,
                                "isLastMinute" => $isLastMinute,
                                "lastMinutePriceAddition" => $lastMinutePriceAddition
                            ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 margin-top-10">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="green-line"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 bh-title">
                        <?= Yii::t('app.exhibitor', 'REQUEST') ?>
                    </div>
                </div>
                <div class="row">
                    <?=
                    $this->render("view/booking", [
                        "model" => $model,
                        "event" => $event,
                        "availabilityData" => $availabilityData,
                        'startDate' => $startDate,
                        'endDate' => $endDate,
                        'dressCode' => $dressCode,
                        'eventJobs' => $eventJobs,
                        'selectedJobs' => $selectedJobs,
                        'selectedPaymentType' => $selectedPaymentType,
                        'eventDressCodes' => $eventDressCodes,
                        'notificationData' => $notificationData
                    ])
                    ?>
                </div>
            </div>
        </div>
        <div class="margin-top-40">
            <?=
            Tabs::widget([
                'navType' => 'nav-pills green-nav-pills',
                'items' => [
                    [
                        'label' => Yii::t('app.exhibitor', 'Basic data'),
                        'content' => $this->render("view/basic", ["model" => $model]),
                        'active' => true,
                        'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
                    ],
                    [
                        'label' => Yii::t('app.exhibitor', 'Languages'),
                        'content' => $this->render("view/languages", ["model" => $model]),
                        'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
                    ],
                    [
                        'label' => Yii::t('app.exhibitor', 'Statistics'),
                        'content' => $this->render("view/statistics", ["model" => $model]),
                        'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
                    ],
                    [
                        'label' => Yii::t('app.exhibitor', 'Reviews'),
                        'content' => $this->render("view/reviews", ["model" => $model, "reviews" => $reviews]),
                        'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6 js-reviews-tab']
                    ],
                    [
                        'label' => Yii::t('app.exhibitor', 'Work experience'),
                        'content' => $this->render("view/work_experience", ["model" => $model, "pastWork" => $pastWork]),
                        'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
                    ],
                    [
                        'label' => Yii::t('app.exhibitor', 'Education'),
                        'content' => $this->render("view/education", ["model" => $model]),
                        'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
                    ]
                ],
            ]);
            ?>
        </div>

    </div>
</div>

<?php
echo $this->render("_booking_alert");
echo $this->render("view/_price_modal");
echo $this->render("view/_last_minute_modal");
echo $this->render("view/_mobile_app_modal");
echo $this->render("view/_reliability_modal");
