<?php

use yii\helpers\Html;

$defaultHostessImage = $model->hostessProfile->getDefaultImageModel(true);

$viewUrlParams = ["//hostess/view", "id" => $model->user_id];
$searchData = $widget->view->context->searchData;

if (!empty($searchData['country']))
{
    $viewUrlParams['EventRegisterSearch[country]'] = $searchData['country'];
}
if (!empty($searchData['city']))
{
    $viewUrlParams['EventRegisterSearch[city]'] = $searchData['city'];
}
if (!empty($searchData['event']))
{
    $viewUrlParams['EventRegisterSearch[event]'] = $searchData['event'];
}

$hasMobile = $model->hostessProfile->isInstalledMobile();

$models = $widget->dataProvider->getModels();
?>

<?php
if ($defaultHostessImage !== null && !empty($defaultHostessImage->image_url)) :
    ?>
    <div class="row img-hostess <?= $model->hostessProfile->isPremium() ? "premium" : "" ?>">
        <div class="col-lg-12 relative">
            <?php echo Html::a('<div class="profile-image" style="background-image:url(' . $model->hostessProfile->getThumbnailImageUrl(220, 170) . ');"></div>', $viewUrlParams); ?>
            <span class="premium-hostess">
                <?php
                echo Html::img(\Yii::$app->request->BaseUrl . '/images/premium_hostess.png');
                ?>
            </span>

            <div class="img-options <?= $hasMobile ? 'has-mobile' : '' ?>">

                <?php if ($hasMobile) : ?>
                    <span class="mobile" title="<?= Yii::t('app.exhibitor', 'This user has mobile application installed.') ?>">
                        <i class="glyphicon glyphicon-phone"></i>
                    </span>
                <?php endif; ?>

                <span class="zoom-in">
                    <a href="<?= \Yii::$app->request->BaseUrl . '/uploads/' . $defaultHostessImage->image_url ?>"
                        data-fancybox="images">
                        <i class="glyphicon glyphicon-search"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-lg-12 text-center hostess-name">
            <span class="username">  <a href="<?= Yii::$app->urlManager->createUrl($viewUrlParams) ?>"><?= $model->user->username; ?></a></span>
        </div>
    </div>

<?php endif; ?>
