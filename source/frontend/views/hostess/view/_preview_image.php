<?php

use yii\helpers\Html;
?>
<div class="profile-images row margin-top-10" id="profile-images">
    <?php if (isset($model->images) && count($model->images) > 0) : ?>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <div class="controls text-left">
                <button class="btn js-prev">< </button>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="frame" id="centered" >
                <ul class="clearfix">
                    <?php foreach ($model->images as $image) : ?>
                        <li>
                            <a href="<?= Yii::getAlias("@web") . $image->getImagePath(); ?>" data-fancybox="images">
                                <div class="item">
                                    <?= Html::img($image->getCachedImage(53, 75), ["height" => "75px", "width" => "55px"]) ?>
                                </div>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

        </div>
        <div class="col-lg-2 col-md-2 col-sm-2  col-xs-2">
            <div class="controls text-right">
                <button class="btn js-next"> > </button>
            </div>
        </div>
    <?php endif; ?>    
</div>
