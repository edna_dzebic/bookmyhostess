<?php

use yii\helpers\Html;
?>

<div class="col-lg-12 images-content">

    <?= $this->render("_slider_image", ["model" => $model]) ?>
    <?= $this->render("_preview_image", ["model" => $model]) ?>

</div>

<?php if (!empty($model->summary)) : ?>
    <div class="col-lg-12 margin-top-20">
        <div class="row">
            <div class="col-xs-12 about-me-title">
                <?= Yii::t('app.exhibitor', 'About me'); ?>
            </div> 
            <div class="col-xs-12 attr-value margin-top-10 about-me-content">
                <?= $model->summary ?> 
            </div>
        </div>
    </div>
<?php endif; ?>
