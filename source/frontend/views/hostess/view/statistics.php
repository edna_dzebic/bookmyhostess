<?php
$totalRequests = $model->getTotalRequestsCount();
$confirmed = $model->getConfirmedRequestsCount();
$rejected = $model->getRejectedRequestCount();
$booked = $model->getBookedRequestCount();
$notAnswered = $model->getNotAnsweredRequestCount();
$colors = $model->getRequestColors();
?>

<div class="row statistics hostess-tab-content">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                <?= Yii::t('app.exhibitor', 'Profile views:'); ?>
            </div> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-value">
                <?= $model->profile_views ?> 
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Last login:'); ?></div> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-value"><?= Yii::$app->util->formatDate($model->user->last_login) ?></div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Time to respond:'); ?></div> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-value"> 
                <span class=" statistic-color <?= $colors["time"] ?>">
                    <?= $model->getTimeToRespond(); ?>
                </span> 
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Requests:'); ?></div> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-value"><?= $totalRequests ?></div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Booked:'); ?></div> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-value">
                <div>
                    <span class="statistic-value"><?= $booked ?></span>
                    <span class="statistic-separator">/</span>
                    <span class="statistic-value"><?= $confirmed ?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Confirmed:'); ?></div> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-value">
                <span class="statistic-color <?= $colors["confirmed"] ?> orange">
                    <span class="statistic-value"><?= $confirmed ?></span>
                    <span class="statistic-separator">/</span>
                    <span class="statistic-value"><?= $totalRequests ?></span>
                </span>
            </div>
        </div>
        <div class="row ">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Rejected:'); ?></div> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-value">
                <span class="statistic-color <?= $colors["rejected"] ?> yellow">
                    <span class="statistic-value"><?= $rejected ?></span>
                    <span class="statistic-separator">/</span>
                    <span class="statistic-value"><?= $totalRequests ?></span>        
                </span>
            </div>
        </div>        
        <div class="row  ">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Not answered:'); ?></div> 
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-value">
                <span class="statistic-color <?= $colors["notAnswered"] ?> red">
                    <span class="statistic-value"><?= $notAnswered ?></span>
                    <span class="statistic-separator">/</span>
                    <span class="statistic-value"><?= $totalRequests ?></span>
                </span>                
            </div>
        </div>
    </div>
</div>
