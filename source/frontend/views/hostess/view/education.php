<?php ?>

<div class="row education hostess-tab-content">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Graduation:'); ?> </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 attr-value"><?= $model->education_graduation ?></div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'University:'); ?> </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 attr-value"><?= $model->education_university ?></div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Vocational Training:'); ?> </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 attr-value"><?= $model->education_vocational_training ?></div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Profession:'); ?> </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 attr-value"><?= $model->education_profession ?></div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 attr-label"><?= Yii::t('app.exhibitor', 'Special Knowledge:'); ?> </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 attr-value"><?= $model->education_special_knowledge ?></div>
        </div>
    </div>
</div>