<div class="modal fade" id="reliability-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.exhibitor', 'Reliability index') ?></h3>
            </div>
            <div class="modal-body panel-body">
                <div class="row">
                    <div class="col-xs-12">
                        <?= Yii::t('app.exhibitor', 'The index indicates on a scale of 0 to a maximum of 100 how reliable a person is. The value is determined by an algorithm based on several criteria. For example, it takes into account how much information a person provides on its profile, how often she or he has answered or canceled a request, and how many assignments she or he has already completed.') ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.exhibitor', 'Close Dialog') ?>
                </button>
            </div>           
        </div>
    </div>
</div>