<?php

use yii\helpers\Html;
?>
<div id="carousel-images" class="carousel slide" data-ride="carousel">

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php if (isset($model->images) && count($model->images) > 0) : ?>
            <?php
            $i = 0;
            foreach ($model->images as $image) :
                $i++;
                ?>
                <div class="item <?= $i == 1 ? 'active' : '' ?>">
                    <?= Html::img($image->getCachedImage(360, 400), ["class" => "img-responsive"]) ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>