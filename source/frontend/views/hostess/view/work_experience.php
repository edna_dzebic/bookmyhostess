<?php

use yii\grid\GridView;
?>
<div class="row work-experience hostess-tab-content">
    <div class="col-lg-12">
        <?=
        GridView::widget(
                [
                    'options' => [
                        'class' => 'grid-view table-responsive',
                    ],
                    'tableOptions' => [
                        'class' => 'table table-bordered',
                    ],
                    'layout' => "{items}",
                    'dataProvider' => $pastWork,
                    'columns' => [
                        [
                            'label' => Yii::t('app.exhibitor', 'Starting date'),
                            'value' => function ($model) {
                                return Yii::$app->util->formatDate($model->start_date);
                            }
                        ],
                        [
                            'label' => Yii::t('app.exhibitor', 'Ending date'),
                            'value' => function ($model) {
                                return Yii::$app->util->formatDate($model->end_date);
                            }
                        ],
                        [
                            'attribute' => 'event_name',
                            'label' => Yii::t('app.exhibitor', 'Event name')
                        ],
                        [
                            'attribute' => 'job_description',
                            'label' => Yii::t('app.exhibitor', 'Job description'),
                        ],
                        [
                            'attribute' => 'position',
                            'label' => Yii::t('app.exhibitor', 'Position'),
                        ]
                    ]
                ]
        )
        ?>
    </div>
</div>