<?php

use yii\helpers\Html;

$hasMobile = $model->isInstalledMobile();

use kartik\rating\StarRating;

$pluginOptions = [
    'readonly' => true,
    'showClear' => false,
    'showCaption' => false,
    'size' => 'xs'
];
?>

<?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->getIsExhibitor() !== false) : ?>

    <div class="col-lg-12 rating-price-content text-center">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 daily-rate-label info-label js-overall-rating-info-icon">
                        <?= Yii::t('app.exhibitor', 'Customer review') ?><span>&nbsp;<i class="glyphicon glyphicon-info-sign"></i></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 rating-value">
                        <?=
                        StarRating::widget([
                            'name' => 'rating',
                            'value' => $model->cached_rating,
                            'pluginOptions' => $pluginOptions
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-top-15">
            <div class="col-xs-12">
                <?php if (is_object($eventReg)): ?>
                    <div class="row">
                        <div class="col-xs-12 daily-rate-label ">
                            <span class="info-label js-daily-rate-info-icon">
                                <?= Yii::t('app.exhibitor', 'Daily rate') ?>
                                <i class="glyphicon glyphicon-info-sign"></i>
                            </span>
                            <?php if ($isLastMinute) : ?>
                                <span class="info-label js-surcharge-info-icon">
                                    <?= Yii::t('app.exhibitor', '(Last minute charge)') ?>
                                    <i class="glyphicon glyphicon-info-sign"></i>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="event-price">
                                <?= Yii::$app->util->formatEventPrice($eventReg->event, $eventReg->price) ?>
                            </span>

                            <?php if ($isLastMinute) : ?>   
                                <span class="event-price">(
                                    <?= Yii::$app->util->formatEventPrice($eventReg->event, $lastMinutePriceAddition) ?> )
                                </span>

                            <?php endif; ?>

                        </div>
                    </div>
                <?php else: ?>
                    <p>
                        <?= Yii::t('app.exhibitor', 'The availability of this user is displayed when the event is entered into the Hostess Search form.') ?>
                    </p>
                <?php endif; ?>
            </div>
        </div>
        <div class="row margin-top-15">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 daily-rate-label info-label js-mobile-app-info-icon">
                        <?= Yii::t('app.exhibitor', 'Mobile app') ?><span>&nbsp;<i class="glyphicon glyphicon-info-sign"></i></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 event-price ">
                        <?php if ($hasMobile) : ?>
                            <?= Yii::t('app.exhibitor', 'Installed') ?>
                        <?php else: ?>
                            <?= Yii::t('app.exhibitor', 'Not installed') ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-top-25">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-top-10">
                <?php
                echo Html::a(
                        Yii::t('app.exhibitor', "Add to favorites"), ['exhibitor/add-favorite', 'id' => $model->user_id], ["class" => "btn btn-standard btn-large full-width"]
                );
                ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-top-10">
                <?php if (is_object($eventReg)): ?>
                    <?php
                    echo Html::a(
                            Yii::t('app.exhibitor', "Download profile"), ['hostess/pdf', 'id' => $model->user_id, 'event' => $eventReg->event_id], ["class" => "btn btn-standard btn-large  full-width", "target" => "_blank"]
                    );
                    ?>
                <?php else: ?>
                    <?php
                    echo Html::a(
                            Yii::t('app.exhibitor', "Download profile"), ['hostess/pdf', 'id' => $model->user_id], ["class" => "btn btn-standard btn-large  full-width", "target" => "_blank"]
                    );
                    ?>
                <?php endif; ?>

            </div>
        </div>

    </div>
<?php endif; ?>
