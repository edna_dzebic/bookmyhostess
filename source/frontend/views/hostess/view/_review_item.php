<?php

use kartik\rating\StarRating;
use yii\helpers\Html;

$pluginOptions = [
    'readonly' => true,
    'showClear' => false,
    'showCaption' => false,
    'size' => 'xs'
];
?>


<div class="review-item-content">
    <div class="row border-right">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <label class="green-text"> <b>  <?= $model->event->name ?></b></label>
                </div>
            </div>               
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-10">

                    <div class="row">
                        <div class="col-xs-12">
                            <label class="green-text"><?= Yii::t('app.exhibitor', 'Rating') ?></label>
                        </div>
                        <div class="col-xs-12">
                            <?=
                            StarRating::widget([
                                'name' => "rating",
                                'value' => $model->rating,
                                'pluginOptions' => $pluginOptions
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row margin-top-10 comment-section">
                <div class="col-xs-12">
                    <label class="green-text"><?= Yii::t('app.exhibitor', 'Comment') ?></label>
                </div>
                <div class="col-xs-12">
                    <?= $model->comment ?>
                </div>
            </div>
            <div class="row margin-top-10">

                <div class="col-xs-12 text-right">
                    <?= Yii::$app->util->formatDate($model->date_updated) ?>
                </div>                
            </div>

        </div>
    </div>
</div>

