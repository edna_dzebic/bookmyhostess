<div class="modal fade" id="price-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.exhibitor', 'Pricing') ?></h3>
            </div>


            <div class="modal-body panel-body">

                <div class="row">
                    <div class="col-xs-12">
                        <?= Yii::t('app.exhibitor', 'Check out our simple and transparent price structure') ?>
                    </div>
                </div>

                <div class="table-responsive margin-top-20">
                    <table class="table table-bordered table-condensed pricing-table">
                        <thead>
                            <tr>
                                <td><?= Yii::t('app.exhibitor', 'Country') ?></td>
                                <td>
                                    <?= Yii::t('app.exhibitor', 'Booking service per day &amp; per hostess') ?><sup>1, 4</sup>
                                </td>

                                <td>
                                    <?= Yii::t('app.exhibitor', 'Staff per day') ?><sup>2, 4</sup>
                                </td>
                                <td>
                                    <?= Yii::t('app.exhibitor', 'Overtime hour') ?><sup>3, 4</sup>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach (Yii::$app->params['pricing'] as $countryName => $country): ?>

                                <tr>
                                    <td>
                                        <?= $country["name"] ?>
                                    </td>

                                    <td>
                                        <?= Yii::$app->util->formatPrice($country["booking_fee"]) . " EUR" ?>
                                    </td>
                                    <td>
                                        <?= Yii::$app->util->formatPrice($country["min_price"]) . " - " . Yii::$app->util->formatPrice($country["max_price"]) . " " . $country ["currency"] ?>
                                    </td>
                                    <td>
                                        <?= Yii::$app->util->formatPrice($country["overtime_fee"]) . " " . $country ["currency"] ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>    
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <sup>1</sup> <?= Yii::t('app.exhibitor', 'Payment is due upon completion of the booking. The customer receives our invoice by e-mail.') ?>
                    </div>
                    <div class="col-xs-12">
                        <sup>2</sup> <?= Yii::t('app.exhibitor', 'The payment is due 7 days after the event and will be paid directly to the booked person after receipt of invoice.') ?>
                    </div>
                    <div class="col-xs-12">
                        <sup>3</sup> <?= Yii::t('app.exhibitor', 'Official trade show opening hours are considered as one working day. If a working day is longer than 9 hours over time fee applies for the rest of the time.') ?>
                    </div>     
                    <div class="col-xs-12">
                        <sup>4</sup> 
                        <?= Yii::t('app.exhibitor', 'For late bookings made {days} or less days before the fair, a surcharge of {percentage}% will apply.', ["days" => Yii::$app->settings->last_minute_days, "percentage" => Yii::$app->settings->last_minute_percentage]) ?>
                    </div>
                    <div class="col-xs-12">
                        <?= Yii::t('app.exhibitor', '19% vat applies for companies based in Germany') ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.exhibitor', 'Close Dialog') ?>
                </button>
            </div>           
        </div>
    </div>
</div>