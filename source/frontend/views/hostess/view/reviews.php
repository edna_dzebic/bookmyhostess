<?php
use yii\widgets\ListView;

?>

<div class="row hostess-tab-content">

    <div class = "col-lg-12">

        <div class="row">
            <div class="col-xs-12">
                <?php
                echo ListView::widget([
                    'options' => [],
                    'emptyText' => '<div class="text-center">' . Yii::t('app.exhibitor', 'No reviews') . '</div>',
                    'dataProvider' => $reviews,
                    'itemOptions' => ['class' => 'margin-top-20 review-item col-lg-6 col-md-6 col-sm-6 col-xs-12'],
                    'layout' => '<div class="row">{items}</div>',
                    'itemView' => function ($model, $key, $index, $widget)
            {

                return $this->render('_review_item', ['model' => $model, 'widget' => $widget]);
            },
                ]);
                ?>  
            </div>
        </div>
    </div>
</div>
