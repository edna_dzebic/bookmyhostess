<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use dosamigos\multiselect\MultiSelect;
?>

<?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->getIsExhibitor() !== false) : ?>

    <div class="col-xs-12 book">
        <div class="row ">
            <?=
            Html::beginForm(
                    Yii::$app->urlManager->createUrl("//hostess/book"), 'get', [
                'id' => 'hostess-book-form',
                'data-alert-message' => Yii::t(
                        'app.exhibitor', 'Please select all fields before booking!'
                )
                    ]
            );
            ?>
            <div class="col-lg-12">

                <div class="row">

                    <div class="col-lg-12">
                        <span class="title"><?= Yii::t('app.exhibitor', 'Select event'); ?></span>

                        <?=
                        Select2::widget([
                            'language' => 'en',
                            'name' => 'event',
                            'value' => $event,
                            'data' => $availabilityData,
                            'hideSearch' => true,
                            'options' => [
                                'class' => 'hostess-book-event js-select-event kartik-styled-select',
                                'id' => 'hostess-book-event-list',
                                'data-url' => Yii::$app->urlManager->createUrl(["//event/get-event-details-for-booking"]),
                                'prompt' => Yii::t('app.exhibitor', "Select event"),
                            ],
                        ]);
                        ?>

                        <div class="hostess-date-range-picker margin-top-10" id="datepicker-cont" data-startDate ="<?= $startDate ?>" data-endDate="<?= $endDate ?>">

                            <span class="title"><?= Yii::t('app.exhibitor', 'Select booking dates'); ?></span>
                            <?php
                            echo DatePicker::widget(
                                    [
                                        'id' => 'hostess-book-date',
                                        'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                                        'layout' => '<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>{input1}{separator}{input2}',
                                        'name' => 'date_from',
                                        'value' => date('d-m-Y', strtotime($startDate)),
                                        'type' => DatePicker::TYPE_RANGE,
                                        'name2' => 'date_to',
                                        'value2' => date('d-m-Y', strtotime($endDate)),
                                        'language' => 'en',
                                        'pluginOptions' => [
                                            'container' => '#hostess-book-date-kvdate',
                                            'autoclose' => true,
                                            'format' => 'dd-mm-yyyy',
                                            'language' => 'en'
                                        ]
                                    ]
                            );
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-12 margin-top-10">
                        <span class="title"><?= Yii::t('app.exhibitor', 'Select jobs'); ?></span>
                        <?=
                        MultiSelect::widget([
                            'name' => 'event_job',
                            'id' => 'event-jobs',
                            "options" => ['multiple' => "multiple"],
                            'data' => $eventJobs,
                            'value' => $selectedJobs,
                            "clientOptions" =>
                            [

                                'nSelectedText' => Yii::t('app.exhibitor', "selected jobs"),
                                "enableCaseInsensitiveFiltering" => true,
                                "filterPlaceholder" => Yii::t('app.exhibitor', "Select jobs"),
                                "nonSelectedText" => Yii::t('app.exhibitor', "Select jobs"),
                                "enableFiltering" => true,
                                "includeSelectAllOption" => false,
                                'numberDisplayed' => 0,
                                'buttonContainer' => '<div class="multi-cont-event-jobs"></div>',
                                'maxHeight' => 200
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-lg-12 margin-top-10">
                        <span class="title"><?= Yii::t('app.exhibitor', 'Select dresscode'); ?></span>
                        <?=
                        Select2::widget([
                            'name' => 'event_dress_code',
                            'language' => 'en',
                            'value' => $dressCode,
                            'data' => $eventDressCodes,
                            'hideSearch' => true,
                            'options' => [
                                'class' => 'hostess-book-event js-select-dresscode',
                                'prompt' => Yii::t('app.exhibitor', "Select dresscode"),
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-lg-12 margin-top-10">
                        <span class="title"><?= Yii::t('app.exhibitor', 'Select payment of the daily rate'); ?></span>
                        <?=
                        Select2::widget([
                            'name' => 'payment_type',
                            'language' => 'en',
                            'value' => $selectedPaymentType,
                            'data' => Yii::$app->util->getPaymentTypeForHostess(),
                            'hideSearch' => true,
                            'addon' => [
                                'append' => '<span class="small">'. Yii::t('app.exhibitor', '* Recommended for national and international corporate clients') . '<br>' . Yii::t('app.exhibitor', '** Recommended for national agencies') . '</span>',
                            ],
                            'options' => [
                                'class' => 'hostess-book-event js-select-payment-type',
                                'prompt' => Yii::t('app.exhibitor', "Select payment of the daily rate"),
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-xs-12 margin-top-40">
                        <input type="hidden" name="hostess_id" value="<?= $model->user_id; ?>">
                        <?= Html::submitButton(Yii::t('app.exhibitor', "Send non-binding request"), ["class" => "btn btn-red full-width btn-send-request"]); ?>
                    </div>
                    <?php if ($notificationData["show"]) : ?>
                        <div class="col-xs-12 margin-top-10">
                            <label class="notification-request" style="color: <?= $notificationData["color"] ?>"><?= $notificationData["message"] ?></label>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?= Html::endForm(); ?>
        </div> 
    </div>
<?php endif; ?>
