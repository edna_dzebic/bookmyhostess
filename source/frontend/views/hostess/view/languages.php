<?php ?>

<div class="row languages hostess-tab-content">
    <div class="col-lg-12">
        <?php
        foreach ($model->getLanguageList() as $languageName => $level) :
            ?>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 attr-label">
                    <?= $languageName ?>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-6 attr-value">
                    <?= $level ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
