<div class="modal fade" id="last-minute-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.exhibitor', 'Surcharge') ?></h3>
            </div>


            <div class="modal-body panel-body">

                <div class="row">
                    <div class="col-xs-12">                        
                        <?= Yii::t('app.exhibitor', 'For late bookings made {days} or less days before the fair, a surcharge of {percentage}% will apply.', ["days" => Yii::$app->settings->last_minute_days, "percentage" => Yii::$app->settings->last_minute_percentage]) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.exhibitor', 'Close Dialog') ?>
                </button>
            </div>           
        </div>
    </div>
</div>