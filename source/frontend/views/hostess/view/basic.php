<?php

use frontend\components\booleanWidget\BooleanWidget;

$totalRequests = $model->getTotalRequestsCount();
$confirmed = $model->getConfirmedRequestsCount();
$rejected = $model->getRejectedRequestCount();
$booked = $model->getBookedRequestCount();
$notAnswered = $model->getNotAnsweredRequestCount();
$colors = $model->getRequestColors();
?>

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="row basic hostess-tab-content">
            <div class="col-lg-12">
                <h4 class="green-text border-bottom-green"><?= Yii::t('app.exhibitor', 'Details') ?></h4>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Age:'); ?>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?= $model->getCurrentAge(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Hair color:'); ?> 
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?= isset($model->hairColor->name) == true ? Yii::t('app', $model->hairColor->name) : ""; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Height:'); ?> 
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?= $model->height; ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Weight:'); ?>  
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?= $model->weight; ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Jeans waist size:'); ?>  
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?= $model->waist_size; ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Shoe size:'); ?> 
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?= $model->shoe_size; ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Visible piercing/tattoo:'); ?>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?=
                        BooleanWidget::widget(
                                [
                                    "value" => $model->has_tattoo
                                ]
                        );
                        ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Owns a car:'); ?> 
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?=
                        BooleanWidget::widget(
                                [
                                    "value" => $model->has_car
                                ]
                        );
                        ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Driving licence:'); ?> 
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?=
                        BooleanWidget::widget(
                                [
                                    "value" => $model->has_driving_licence
                                ]
                        );
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Trade licence:'); ?> 
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6  attr-value">
                        <?=
                        BooleanWidget::widget(
                                [
                                    "value" => $model->has_trade_licence
                                ]
                        );
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 attr-label">
                        <?= Yii::t('app.exhibitor', 'Health cert:'); ?> 
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6 attr-value">
                        <?=
                        BooleanWidget::widget(
                                [
                                    "value" => $model->has_health_certificate
                                ]
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="row preferred-jobs hostess-tab-content">
            <div class="col-lg-12">
                <h4 class="green-text border-bottom-green"><?= Yii::t('app.exhibitor', 'Preferred jobs') ?></h4>
            </div>
            <div class="col-lg-12">

                <?php if (count($model->preferredJobs) > 0) : ?>

                    <?php foreach ($model->preferredJobs as $job) : ?>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 attr-value">
                                <?= $job->eventJob->translatedName; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>

                <?php else: ?>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 attr-value">
                            <?= Yii::t('app.exhibitor', 'No preferences.') ?>
                        </div>
                    </div>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

