<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app.exhibitor', 'Hostess search request');
?>

<div class="hostess-request-form">
    <div class="row">
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="green-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-title">
                    <?= Yii::t('app.exhibitor', 'HOSTESS SEARCH REQUEST') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-subtitle">
                    <?= Yii::t('app.exhibitor', 'Sorry, we did not find a person matching Your search request. Please specify here Your search criteria and we will get in touch with You as soon as we have found a suitable person.') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php $form = ActiveForm::begin(['id' => 'hostess-request-form']); ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?= $form->field($model, 'additional_description')->textarea(['rows' => '6', 'placeholder' => Yii::t('app.exhibitor', 'Brief Description')])->label(false) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <?= Html::a(Yii::t('app.exhibitor', 'Back to Search'), ['hostess/index'], ['class' => 'btn btn-default full-width']) ?>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">

                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 text-right">
                            <?= Html::submitButton(Yii::t('app.exhibitor', 'Send'), ['class' => 'btn btn-green full-width', 'name' => 'register-button']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
        </div>
    </div>
</div>