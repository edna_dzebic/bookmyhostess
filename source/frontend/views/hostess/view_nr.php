<?php

use yii\helpers\Html;

$defaultImage = $model->getDefaultImageModel(true);

$this->title = $model->user->username;
?>

<div class="container view_nr">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-1 hidden-xs">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-10 col-xs-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="green-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-title">
                    <?= $model->user->username; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-top-10">
                    <?php if ($defaultImage !== null): ?>
                        <div class="hostess-profile-image image" style="
                             background-image: url('<?= $model->getThumbnailImageUrl(330, 258) ?>');
                             ">
                        </div>                    
                    <?php endif; ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6  col-xs-12 margin-top-10">
                    <div class="hostess-box">
                        <p><?= Yii::t('app.exhibitor', 'You need to login as exhibitor to use this service.'); ?></p>
                        <div class='buttons'>
                            <?= Html::a(Yii::t('app.exhibitor', "Register"), \Yii::$app->urlManager->createUrl(["//exhibitor/register", "refhos" => $model->user->id, "ev" => $event, "cp" => $cp]), ["class" => "btn btn-standard"]); ?>

                            <?= Html::a(Yii::t('app.exhibitor', "Login"), \Yii::$app->urlManager->createUrl(["//site/login", "ref" => $model->user->id, "ev" => $event, "cp" => $cp]), ["class" => "btn btn-standard"]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-1 hidden-xs">
        </div>
    </div>
</div>
