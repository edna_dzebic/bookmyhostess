<?php

use frontend\models\EventRegisterSearch;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use dosamigos\multiselect\MultiSelect;
use yii\helpers\Html;

?>

<?php
$form = ActiveForm::begin(["method" => "get", "action" => ["//hostess/index"], "id" => "advanced-search-form"]);
?>
<div class=" col-xs-12 hostess-filter">
    <div class="row bg-green">

        <div class="col-lg-4 col-xs-6 advanced-search-label text-left">

            <div class="toggle-search-btn js-toggle-search" data-opened-text = "<?= Yii::t('app.exhibitor', 'Hide Search') ?> " data-closed-text = "<?= Yii::t('app.exhibitor', 'Show Search') ?> ">
                <span class="dropup js-caret-cont">
                    <span class="caret"></span>
                </span>
                <span class="js-label-text"><?= Yii::t('app.exhibitor', 'Hide Search') ?></span>
            </div>

        </div>
        <div class="col-lg-6 col-xs-6 text-right">

            <div class="summary "><b>{begin}-{count}</b> <?= Yii::t('app.exhibitor', 'of ') ?><b>{totalCount}</b> <?= Yii::t('app.exhibitor', 'results') ?></div>
        </div> 
        <div class="col-lg-2 col-xs-12 text-right">
            <fieldset>
                <?php
                echo $form->field($model, 'sort')->dropDownList(EventRegisterSearch::getSortList(), [
                    'prompt' => Yii::t('app.exhibitor', "- Sort by -"),
                    'id' => 'advanced-search-sort',
                    'data-field' => "eventregistersearch-sort",
                ])->label(false)->error(false);
                ?>
            </fieldset>
        </div>



    </div>
    <div class="row">
        <div class="col-xs-12 advanced-search js-advanced-search-cont open">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <?php
                    echo $form->field($model, 'country')->dropDownList(EventRegisterSearch::getCountryList(), [
                        'prompt' => Yii::t('app.exhibitor', "COUNTRY"),
                        'id' => 'hostess-country-filter',
                        'data-url' => Yii::$app->urlManager->createUrl("//hostess/get-cities-json"),
                            ]
                    )->label(false)->error(false);
                    ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <?php
                    echo $form->field($model, 'city')->dropDownList(
                            EventRegisterSearch::getCityList($model->country), [
                        'prompt' => Yii::t('app.exhibitor', "CITY"),
                        'id' => 'hostess-city-filter',
                        'data-url' => Yii::$app->urlManager->createUrl("//hostess/get-events-json"),
                            ]
                    )->label(false)->error(false);
                    ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <?php
                    $eventListData = EventRegisterSearch::getEventList($model->city);

                    echo $form->field($model, 'event')->dropDownList(
                            $eventListData, [
                        'prompt' => Yii::t('app.exhibitor', "EVENT"),
                        'id' => 'hostess-event-filter',
                            ]
                    )->label(false)->error(false);
                    ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 advanced-search-option ">
                    <?=
                            $form->field($model, 'languages')
                            ->widget(MultiSelect::classname(), [
                                "options" => [
                                    'id' => uniqid(),
                                    "class" => "hostess-languages-filter",
                                    'multiple' => "multiple",
                                ],
                                'data' => EventRegisterSearch::getLanguagesList(),
                                "clientOptions" =>
                                [
                                    "enableCaseInsensitiveFiltering" => true,
                                    "filterPlaceholder" => Yii::t('app.exhibitor', "LANGUAGE"),
                                    "nonSelectedText" => Yii::t('app.exhibitor', "LANGUAGES"),
                                    "enableFiltering" => true,
                                    "includeSelectAllOption" => true,
                                    'numberDisplayed' => 0,
                                    'buttonContainer' => '<div class="btn-group multi-cont"></div>'
                                ]
                            ])
                            ->label(false)->error(false);
                    ?>
                </div>
            </div>
            <div class="row">  
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 advanced-search-option ">
                    <?= $form->field($model, 'jobs')->dropDownList(EventRegisterSearch::getEventJobList(), ['prompt' => Yii::t('app.exhibitor', "JOB")])->label(false)->error(false); ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 advanced-search-option">
                    <?= $form->field($model, 'ages')->dropDownList(EventRegisterSearch::getYearList(), ['prompt' => Yii::t('app.exhibitor', "AGE")])->label(false)->error(false); ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 advanced-search-option ">
                    <?= $form->field($model, 'hair_colors')->dropDownList(EventRegisterSearch::getHairColorList(), ['prompt' => Yii::t('app.exhibitor', "HAIR COLOR")])->label(false)->error(false); ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 advanced-search-option ">
                    <?= $form->field($model, 'genders')->dropDownList(EventRegisterSearch::getGenderList(), ['prompt' => Yii::t('app.exhibitor', "GENDER")])->label(false)->error(false); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <?= $form->field($model, 'has_health_certificate', ['template' => '{input}{label}'])->widget(CheckboxX::classname(), ['pluginOptions' => ['threeState' => false, 'size' => 'xs']]); ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <?= $form->field($model, 'has_driving_licence', ['template' => '{input}{label}'])->widget(CheckboxX::classname(), ['pluginOptions' => ['threeState' => false, 'size' => 'xs']]); ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <?= $form->field($model, 'has_work_experience', ['template' => '{input}{label}'])->widget(CheckboxX::classname(), ['pluginOptions' => ['threeState' => false, 'size' => 'xs']]); ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <?= $form->field($model, 'has_mobile_app', ['template' => '{input}{label}'])->widget(CheckboxX::classname(), ['pluginOptions' => ['threeState' => false, 'size' => 'xs']]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-right">
                    <?= Html::button(Yii::t('app.exhibitor', "RESET"), ["class" => "search-hostess-btn btn btn-default js-btn-reset"]); ?>
                    <?= Html::button(Yii::t('app.exhibitor', "SEARCH"), ["class" => "search-hostess-btn btn btn-primary btn-green", "type" => "submit"]); ?>
                </div>
            </div>
        </div>        
    </div>
</div>

<?php
$form->end();
?>