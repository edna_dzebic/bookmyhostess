<div class="modal fade" id="booking-alert-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.exhibitor', 'Booking') ?></h3>
            </div>
            <div class="modal-body panel-body">
                <h4 class="js-booking-alert-message">

                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.exhibitor', 'Close Dialog') ?>
                </button>
            </div>           
        </div>
    </div>
</div>