<?php

use yii\widgets\ListView;
use frontend\models\Layout;

\frontend\assets\HostessCatalogAsset::register($this);

$this->title = "Online Messehostess buchen mit BOOKmyHOSTESS.com";
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => Layout::getMetaKeywords(Layout::SITE_HOSTESS_SEARCH)
        ], 'keywords');
$this->registerMetaTag([
    'name' => 'description',
    'content' => Layout::getMetaDescription(Layout::SITE_HOSTESS_SEARCH)
        ], 'description');
?>

<?= $this->render("//partials/_flash") ?>

<div class="row">
    <div class="col-lg-4">
        <div class="green-line"></div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-title">
        <?= Yii::t('app.exhibitor', 'FIND A HOSTESS') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-subtitle">
        <?= Yii::t('app.exhibitor', 'In just few steps to Your short term staff') ?>*

        <p>
            <br>
            <a href="https://www.youtube.com/watch?v=VAgAygIaQy0"  target="_blank" style="color:#0caa9d; font-weight:bold;font-size: 14px;">*<?= Yii::t('app.exhibitor', 'See Demo-Video') ?></a>
        <p>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

        <?php
        echo ListView::widget([
            'options' => [],
            'dataProvider' => $searchProvider,
            'itemOptions' => ['class' => 'margin-top-20 hostess-item col-lg-3 col-md-3 col-sm-3 col-xs-6'],
            'layout' => '{summary}<div class="row">{items}</div>',
            'summary' => $this->render("_search", ["model" => $searchModel]),
            'itemView' => function ($model, $key, $index, $widget)
    {
        return $this->render('_hostess_itemView', ['model' => $model, 'widget' => $widget, 'index' => $index]);
    },
        ]);
        ?>   
    </div>
</div>
<div class="row text-right margin-top-20">
    <div class="col-xs-12">
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $searchProvider->getPagination(),
            'options' => ['class' => 'pagination'],
            'maxButtonCount' => 5,
            'nextPageLabel' => '>',
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
            'prevPageLabel' => '<'
        ]);
        ?>
    </div>    
</div>

<div class="row text-justify margin-top-20">
    <div class="col-xs-12">
        <h2>BOOKmyHOSTESS.com: Ihre Plattform f&uuml;r Hostessen in Deutschland</h2>
        <p>Um ein Unternehmen auf einer Messe erfolgreich zu pr&auml;sentieren, sind Sie auf gute Hostessen in Deutschland angewiesen. M&ouml;chten Sie dabei als Unternehmen dauerhaft flexibel agieren, sind Sie bei BOOKmyHOSTESS.com richtig.</p>
        <p>BOOKmyHOSTESS.com ist eine online Plattform, die sich auf die Vermittlung von Hostessen in Dortmund sowie anderen St&auml;dten der Bundesrepublik spezialisiert hat. Stellen Sie unverbindlich und wann immer Sie wollen Ihre Anfrage bei uns.</p>
        <p>Innerhalb k&uuml;rzester Zeit erhalten Sie von den bei uns registrierten Hostessen eine Antwort. Die Kosten sind transparent und bei jeder Hostess auf dem Profil ausgewiesen.</p>
        <h3>Finden Sie die Hostess, die zu Ihnen passt</h3>
        <p>Sie k&ouml;nnen bei BOOKmyHOSTESS.com ganz in Ruhe beispielsweise f&uuml;r die Messe M&uuml;nchen die Hostess suchen, die Sie brauchen. Dabei haben Sie es in der Hand:</p>
        <p><strong>Entscheiden Sie selbst, ob Ihre Messehostess Englisch sprechen muss oder ob Sie sich vielleicht lieber eine Mitarbeiterin oder einen Mitarbeiter mit besonderem technischen Know-how w&uuml;nschen.</strong> Die Filter auf unserer Plattform helfen Ihnen die Auswahl einzuschr&auml;nken. Haben Sie die passenden Mitarbeiter gefunden, k&ouml;nnen Sie bei uns effizient und in Ruhe Ihre Messehostess buchen.</p>
        <h3>Faire und transparente Kosten von Anfang an</h3>
        <p>Wenn Sie bei uns eine Messehostess buchen, k&ouml;nnen Sie sich von Anfang an auf faire Kosten verlassen. Die Abwicklung Ihrer Anfrage und Buchung erfolgt ausschlie&szlig;lich online. Das spart hohe Verwaltungskosten, die viele traditionelle Agenturen an ihre Kunden weitergeben m&uuml;ssen.&nbsp;</p>
    </div>    
</div>
