<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;

\frontend\assets\EventViewAsset::register($this);

$this->title = $model->name;



$format = <<< SCRIPT
function format(state) {
                        
        if(state.disabled)
        {
            return state.text + '</br> <label class="label label-danger danger" data-toggle="tooltip" data-placement="top" title="'+state.title+'">'+state.title +'</label>';
        }
        
        return state.text;
        }
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);

?>
<?= $this->render("//partials/_flash") ?>

<div class="row">
    <div class="col-lg-4">
        <div class="green-line"></div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-title">
        <?= $model->name; ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-left">

        <?php if (Yii::$app->user->isGuest) : ?>
            <?= Html::a(Yii::t('app.hostess', 'Sign up'), Yii::$app->urlManager->createUrl(["event/signup-guest"]), ["class" => "btn btn-green"]) ?>
            <?php
        elseif (!Yii::$app->user->isGuest && (
                isset(Yii::$app->user->identity->hostessProfile) &&
                Yii::$app->user->identity->hostessProfile->isApproved() === false)):
            ?>


            <?= Html::a(Yii::t('app.hostess', 'Sign up'), Yii::$app->urlManager->createUrl(["event/signup-unapproved"]), ["class" => "btn btn-green"]) ?>

        <?php elseif (Yii::$app->user->identity->isHostess) : ?> 

            <?php if ($model->hostessAlreadyBooked) : ?>

                <?= Html::a(Yii::t('app.hostess', 'Booked'), Yii::$app->urlManager->createUrl(["/hostess-profile/current-booking"]), ["class" => "btn btn-danger"]) ?>

            <?php else : ?>
                <?php if ($model->hostessAlreadyRegistered) : ?>

                    <?php if ($model->hostessHasNewRequests) : ?> 
                        <?= Html::a(Yii::t('app.hostess', 'Reply to request'), Yii::$app->urlManager->createUrl(["/hostess-profile/requests"]), ["class" => "btn btn-danger"]) ?>

                    <?php elseif ($model->hostessHasAcceptedRequests): ?>
                        <?= Html::a(Yii::t('app.hostess', 'Already accepted'), Yii::$app->urlManager->createUrl(["/hostess-profile/requests"]), ["class" => "btn btn-green"]) ?>

                    <?php else: ?>
                        <?=
                        Html::button(Yii::t('app.hostess', 'Sign off'), [
                            'class' => 'btn btn-default js-event-withdraw',
                            'data-url' => Yii::$app->urlManager->createUrl(["//event/withdraw"]),
                            'data-id' => $model->id,
                            'data-name' => $model->name,
                        ]);
                        ?>
                    <?php endif; ?>
                <?php else : ?>
                     <?=
                                    Select2::widget([
                                        'name' => 'event',
                                        'data' => [
                                            Yii::t('app.hostess', "Select price") . "(" . $model->country->currency_label . ")" => $model->getPrices()
                                        ],
                                        'value' => '',
                                        'showToggleAll' => false,
                                        'hideSearch' => true,
                                        'options' => [
                                            'multiple' => false,
                                            'options' => $model->getOptionsForPriceDropdown(),
                                            'prompt' => Yii::t('app.hostess', "Sign up"),
                                            'class' => 'js-event-signup event-sign-up-dropdown',
                                            'data-url' => Yii::$app->urlManager->createUrl(["//event/signup"]),
                                            'data-id' => $model->id,
                                            'encode' => false
                                        ],
                                        'pluginOptions' => [
                                            'tags' => false,
                                            'allowClear' => false,
                                            'templateResult' => new JsExpression('format'),
                                            'templateSelection' => new JsExpression('format'),
                                            'escapeMarkup' => $escape,
                                        ],
                                    ]);
                                    ?>
                <?php endif; ?>
            <?php endif; ?>            
        <?php endif; ?>       
    </div>
</div> 
<br>
<div class="row event-view book-a-hostess-container">

    <div class="col-lg-12 text-left">

        <div>
            <p><?= Yii::t('app.hostess', 'Event category: {category}', ['category' => $model->type->translatedName]); ?></p>

            <p><?= Yii::t('app.hostess', 'Venue: {venue}', ['venue' => $model->venue]); ?></p>

            <p><?= Yii::t('app.hostess', 'Start date : {start_date}', ['start_date' => Yii::$app->util->formatDate($model->start_date)]); ?></p>

            <p><?= Yii::t('app.hostess', 'End date : {end_date}', ['end_date' => Yii::$app->util->formatDate($model->end_date)]); ?></p>
            <?php
            if (isset($model->city))
            {
                ?>
                <p><?= Yii::t('app.hostess', 'City: {city}', ['city' => $model->city->name]) ?></p>
                <?php
            }
            if (isset($model->country))
            {
                ?>
                <p><?= Yii::t('app.hostess', 'Country: {country}', ['country' => $model->country->name]); ?></p>
            <?php } ?>
        </div>
        <div class="row">
            <div class="spacer"></div>
        </div>
        <div class="row text-left">
            <div class="col-lg-12">
                <span class="title">
                    <?= Yii::t('app.hostess', 'Description'); ?>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?= $model->brief_description; ?>
            </div>
        </div>
        <div class="row">
            <div class="spacer"></div>
        </div>

        <?php
        if ($model->url != "")
        {
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <span class="title">
                        <?= Yii::t('app.hostess', 'Official event page'); ?>
                    </span>
                    <h4><?= Html::a($model->url, $model->url, ["target" => "_blank"]); ?></h4>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="bottom-buttons text-right">

                    <div class="clearfix"></div>
                    <?= Html::a(\Yii::t('app.hostess', 'Back'), $goBackUrl, ['id' => 'event-go-back', 'class' => 'btn btn-standard']); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render("//partials/_signoff_modal") ?>