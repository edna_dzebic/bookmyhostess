<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;

$format = <<< SCRIPT
function format(state) {
                        
        if(state.disabled)
        {
            return state.text + '</br> <label class="label label-danger danger" data-toggle="tooltip" data-placement="top" title="'+state.title+'">'+state.title +'</label>';
        }
        
        return state.text;
        }
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);


$viewUrlParams = ["//event/view", "id" => $model->id];
$searchData = $widget->view->context->searchData;

if (!empty($searchData['country_id']))
{
    $viewUrlParams['EventSearch[country_id]'] = $searchData['country_id'];
}
if (!empty($searchData['city_id']))
{
    $viewUrlParams['EventSearch[city_id]'] = $searchData['city_id'];
}
if (!empty($searchData['event']))
{
    $viewUrlParams['EventSearch[event]'] = $searchData['event'];
}
?>

<div class="event-item-content">
    <div class="row border-right">        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">

            <p class="title">
                <?= Html::a($model->name . ' <i class="glyphicon glyphicon-info-sign" title="' . Yii::t('app.hostess', 'More details') . '"></i>', \Yii::$app->urlManager->createUrl($viewUrlParams), ["class" => "green-text"]); ?>
            </p>

            <div class = "description">
                <p>
                    <?=
                    Yii::t('app.hostess', 'From : {date}', [
                        'date' => Yii::$app->util->formatDate($model->start_date)
                    ]);
                    ?>
                </p>

                <p>
                    <?=
                    Yii::t('app.hostess', 'To : {date}', [
                        'date' => Yii::$app->util->formatDate($model->end_date)
                    ]);
                    ?>
                </p>

                <p>
                    <?php
                    if (isset($model->country))
                    {
                        Yii::t('app.hostess', 'Country : {country}', [
                            'country' => $model->country->name
                        ]);
                    }
                    ?>
                </p>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
            <div class="bold">
                <?php
                if (isset($model->city))
                {
                    ?>
                    <?= $model->city->name;
                    ?>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <br>
            <div class="row">
                <div class="col-lg-6 col-md-4 col-sm-4 hidden-xs">

                </div>
                <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12 text-right">
                    <?php
                    if (
                            !\Yii::$app->user->isGuest && (
                            isset(\Yii::$app->user->identity->hostessProfile) &&
                            \Yii::$app->user->identity->hostessProfile->isApproved()))
                    {
                        ?>

                        <?php if (\Yii::$app->user->identity->isHostess) : ?>
                            <?php if ($model->hostessAlreadyBooked) : ?>

                                <?= Html::a(Yii::t('app.hostess', 'Booked'), Yii::$app->urlManager->createUrl(["/hostess-profile/current-booking"]), ["class" => "btn btn-danger"]) ?>

                            <?php else : ?>
                                <?php if ($model->hostessAlreadyRegistered) : ?>

                                    <?php if ($model->hostessHasNewRequests) : ?> 
                                        <?= Html::a(Yii::t('app.hostess', 'Reply to request'), Yii::$app->urlManager->createUrl(["/hostess-profile/requests"]), ["class" => "btn btn-danger"]) ?>

                                    <?php elseif ($model->hostessHasAcceptedRequests): ?>
                                        <?= Html::a(Yii::t('app.hostess', 'Already accepted'), Yii::$app->urlManager->createUrl(["/hostess-profile/requests"]), ["class" => "btn btn-green"]) ?>

                                    <?php else: ?>
                                        <?=
                                        Html::button(Yii::t('app.hostess', 'Sign off'), [
                                            'class' => 'btn btn-default js-event-withdraw',
                                            'data-url' => Yii::$app->urlManager->createUrl(["//event/withdraw"]),
                                            'data-id' => $model->id,
                                            'data-name' => $model->name,
                                        ]);
                                        ?>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <?=
                                    Select2::widget([
                                        'name' => 'event',
                                        'data' => [
                                            Yii::t('app.hostess', "Select price") . " " . "(" . $model->country->currency_label . ")" => $model->getPrices()
                                        ],
                                        'value' => '',
                                        'showToggleAll' => false,
                                        'hideSearch' => true,
                                        'options' => [
                                            'multiple' => false,
                                            'options' => $model->getOptionsForPriceDropdown(),
                                            'prompt' => Yii::t('app.hostess', "Sign up"),
                                            'class' => 'js-event-signup event-sign-up-dropdown',
                                            'data-url' => Yii::$app->urlManager->createUrl(["//event/signup"]),
                                            'data-id' => $model->id,
                                            'encode' => false
                                        ],
                                        'pluginOptions' => [
                                            'tags' => false,
                                            'allowClear' => false,
                                            'templateResult' => new JsExpression('format'),
                                            'templateSelection' => new JsExpression('format'),
                                            'escapeMarkup' => $escape,
                                        ],
                                    ]);
                                    ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php } ?>
                </div>
            </div> 
        </div>
    </div>
</div>