<?php

use yii\widgets\ListView;
use frontend\models\Layout;

\frontend\assets\EventCatalogAsset::register($this);

$this->title = "Messejobs und Promotionjobs mit BOOKmyHOSTESS.com finden";
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => Layout::getMetaKeywords(Layout::SITE_EVENT_SEARCH)
        ], 'keywords');
$this->registerMetaTag([
    'name' => 'description',
    'content' => Layout::getMetaDescription(Layout::SITE_EVENT_SEARCH)
        ], 'description');
?>

<div class="row">
    <div class="col-lg-4">
        <div class="green-line"></div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-title">
        <?= Yii::t('app.hostess', 'NEXT EVENTS') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-subtitle">
        <?= Yii::t('app.hostess', 'On this page you can find a list of Events and Tradeshows. You can filter the list by country and city. Furthermore, you can find out more details about event, and sign up for events you are interested in.') ?>

        <p>
            <br>
            <a href="https://www.youtube.com/watch?v=R-nLFr-uVno" target="_blank" style="color:#0caa9d; font-weight:bold;font-size: 14px;">*<?= Yii::t('app.exhibitor', 'See Demo-Video') ?></a>
        <p>
    </div>


</div>

<?= $this->render("//partials/_flash") ?>

<div class="row">
    <div class="col-lg-12">
        <?php
        echo ListView::widget([
            'options' => [],
            'dataProvider' => $dataProvider,
            'emptyText' => '<div class="text-center">' . Yii::t('app', 'No results') . '</div>',
            'itemOptions' => ['class' => 'margin-top-20 event-item col-lg-6 col-md-6 col-sm-6 col-xs-12'],
            'layout' => '{summary}<div class="row">{items}</div>',
            'summary' => $this->render("_search"),
            'itemView' => function ($model, $key, $index, $widget)
    {
        return $this->render('_event', ['model' => $model, 'widget' => $widget]);
    },
        ]);
        ?>
    </div>
</div>

<div class="row text-right margin-top-20">
    <div class="col-xs-12">
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'options' => ['class' => 'pagination'],
            'maxButtonCount' => 5,
            'nextPageLabel' => '>',
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
            'prevPageLabel' => '<'
        ]);
        ?>
    </div>    
</div>
<?= $this->render("//partials/_signoff_modal") ?>

<div class="row text-justify margin-top-20">
    <div class="col-xs-12">
        <h2>BOOKmyHOSTESS.com unterst&uuml;tzt dich bei der Suche nach Messejobs</h2>
        <p>Du m&ouml;chtest als Hostess Unternehmen dabei helfen, sich ansprechend einer breiten Zielgruppe zu pr&auml;sentieren? Mit BOOKmyHOSTESS.com ist genau das m&ouml;glich.</p>
        <p><strong>Als Plattform helfen wir dir bei der Suche nach neuen Messehostess Jobs.</strong> Wir bringen erfahrene Hostessen und suchende Unternehmen zusammen. Dabei kannst dubei BOOKmyHOSTESS.com nicht nur in Deutschland nach neuen Promotionjobs suchen. Auch in anderen L&auml;ndern Europas sind wir mit unserem Angebot bereits vertreten.</p>
        <h3>Verlasse dich auf eine transparente Abwicklung!</h3>
        <p>Bei uns entscheidest du selbst, f&uuml;r welche Messejobs du dich bewerben und wieviel du pro Stunde verdienen m&ouml;chtest. Nutze die Gelegenheit und erhalte die Anfragen von Unternehmen unterschiedlicher Branchen direkt auf dein Smartphone. Dabei findest du bei uns auch dann attraktive Jobs, wenn du beispielsweise kurzfristig Luft in deinem Kalender hast. Neben einer transparenten Abwicklung garantieren wir dir abwechslungsreiche Jobs und faire Honorare. Messehostess Jobs sind ideal <a title="Jobs f&uuml;r Studenten - Hostess" href="https://www.bookmyhostess.com/site/hostess" target="_blank" rel="noopener">Jobs f&uuml;r Studenten</a>, da du immer zeitliche befristete und planbare Arbeitszeiten hast.</p>
        <h3>Ergreife neue Chancen!</h3>
        <p>Mit BOOKmyHOSTESS.com kannst du dich als erfahrene Hostess einer gro&szlig;en Bandbreite an Unternehmen und Messeausstellern pr&auml;sentieren. Dabei erwarten dich immer wieder neue Herausforderungen, die du als <strong>Promotionjobs</strong> wahrnehmen kannst.</p>
        <p>Ergreife diese Chancen und kn&uuml;pfe wichtige neue Kontakte. Du findest bei uns nach der Registrierung sowohl kurzfristige Jobangebote als auch Anfragen f&uuml;r Messejobs zu einem sp&auml;teren Zeitpunkt. Unternehmen k&ouml;nnen bei BOOKmyHOSTESS.com ausstehende Messejobs bei Bedarf hinterlegen und k&ouml;nnen rechtzeitig mit der Suche nach motivierten Mitarbeitern beginnen.</p>
    </div>    
</div>