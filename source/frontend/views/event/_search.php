<?php

use frontend\models\EventRegisterSearch;
use frontend\models\EventSearch;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

$model = new EventSearch();
//load from request
$model->loadParamsForFilter(Yii::$app->request->get());
?>


<?php $form = ActiveForm::begin(["method" => "get", "action" => ["//event/index"], "id" => "event-search-form"]); ?>

<div class=" col-xs-12 hostess-filter">
    <div class="row bg-green">

        <div class="col-lg-4 col-xs-6 advanced-search-label text-left">

            <div class="toggle-search-btn js-toggle-search" data-opened-text = "<?= Yii::t('app.hostess', 'Hide Search') ?> " data-closed-text = "<?= Yii::t('app.hostess', 'Show Search') ?> ">
                <span class="dropup js-caret-cont">
                    <span class="caret"></span>
                </span>
                <span class="js-label-text"><?= Yii::t('app.hostess', 'Hide Search') ?></span>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6 text-right">

            <div class="summary "><b>{begin}-{count}</b> <?= Yii::t('app.hostess', 'of ') ?><b>{totalCount}</b> <?= Yii::t('app.hostess', 'results') ?></div>
        </div>
        <div class="col-lg-2 col-xs-12 text-right">
            <fieldset>
                <?php
                echo $form->field($model, 'sort')->dropDownList(EventSearch::getSortList(), [
                    'prompt' => Yii::t('app.hostess', "- Sort by -"),
                    'id' => 'advanced-search-sort',
                    'data-field' => "eventregistersearch-sort",
                ])->label(false)->error(false);
                ?>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 advanced-search js-advanced-search-cont open">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <?=
                    $form->field($model, 'country_id')->widget(Select2::classname(), [
                        'data' => EventRegisterSearch::getCountryList(),
                        'options' => [
                            'id' => 'event-country-filter',
                            'class' => ' form-control',
                            'placeholder' => Yii::t("app.hostess", "COUNTRY"),
                            'data-url' => Yii::$app->urlManager->createUrl("//hostess/get-cities-json"),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false)->error(false);
                    ?>

                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <?=
                    $form->field($model, 'city_id')->widget(Select2::classname(), [
                        'data' => EventRegisterSearch::getCityList($model->country_id),
                        'options' => [
                            'id' => 'event-city-filter',
                            'class' => ' form-control',
                            'placeholder' => Yii::t("app.hostess", "CITY"),
                            'data-url' => Yii::$app->urlManager->createUrl("//hostess/get-events-json"),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false)->error(false);
                    ?>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <?=
                    $form->field($model, 'event')->widget(Select2::classname(), [
                        'data' => EventSearch::getDataForTypeAhead($model->city_id, $model->country_id),
                        'options' => [
                            'id' => 'event-tt-filter',
                            'class' => 'event-name form-control',
                            'placeholder' => Yii::t("app.hostess", "Select event")
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false)->error(false);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-right">
                    <?= Html::button(Yii::t('app.hostess', "RESET"), ["class" => "search-hostess-btn btn btn-default js-btn-reset"]); ?>
                    <?= Html::button(Yii::t('app.hostess', "SEARCH"), ["class" => "search-hostess-btn btn btn-primary btn-green", "type" => "submit"]); ?>
                </div>
            </div>
        </div>        
    </div>
</div>

<?php
$form->end();
?>