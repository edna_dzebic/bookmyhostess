<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('app', 'Login');
?>
<div class="site-login">

    <div class="row">
        <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">

        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="green-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-title">
                    <?= Yii::t('app', 'LOGIN') ?>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12 bh-subtitle">
                    <?= Yii::t('app', 'Please fill out the following fields to login:') ?>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                    <div class="row">

                        <div class="col-xs-12">
                            <?= $form->field($model, 'username', ["template" => '{input}{error}'])->input("text", ["placeholder" => Yii::t('app', 'Username/Email*')])->label(false) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'password', ["template" => '{input}{error}'])->passwordInput(["placeholder" => Yii::t('app', 'Password*')])->label("false") ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <?= $form->field($model, 'rememberMe', ['template' => '{input}{label}'])->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right ">
                            <?= Html::a(Yii::t('app', 'Forgot your password?'), ['//site/request-password-reset'], ['class' => 'green-text']) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'LOGIN'), ['class' => 'btn btn-red full-width', 'name' => 'login-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>            
        </div>
        <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">

        </div>

    </div>


</div>
