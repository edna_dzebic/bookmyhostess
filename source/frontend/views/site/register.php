<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Free Registration');
?>
<div class="banner banner-image registration">

    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 register-form">
                <div class="row">
                    <div class="col-xs-12 title">
                        <?= Yii::t('app', 'FREE REGISTRATION') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-xs-8 subtitle">
                        
                    </div>
                </div>
                <div class="row margin-top-10">
                    <div class="col-xs-12">
                        <div class="row registration form-bg form-group-m-0">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-top-10">
                                <?=
                                Html::a(Yii::t('app', 'Register as exhibitor'), ["//exhibitor/register"], ["class" => "btn btn-red full-width btn-register"]);
                                ?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-top-10">
                                <?=
                                Html::a(Yii::t('app', 'Register as hostess'), ["//hostess-profile/register"], ["class" => "btn btn-green full-width btn-register"]);
                                ?>  
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>     
</div>