<?php

use yii\helpers\Html;
?>
<!--CONNECT BEGIN-->
<div class="connect content-section">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 margin-top-20">
                    <div class="row">
                        <div class="col-xs-12 white-title">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="green-line"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 bh-title white">
                                    <h2><?= Yii::t('app.hostess', 'CONNECT') ?></h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <a href="https://www.facebook.com/BOOKmyHOSTESS/" target="_blank">
                                        <?= Html::img(Yii::$app->urlManager->createAbsoluteUrl(["/images/fb.png"]), ["class" => "img-responsive"]) ?>
                                    </a>
                        </div>
                    </div>               
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 social-block  margin-top-20">
                    <div class="row">
                        <div class="col-xs-12 white-title">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="green-line"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 bh-title white">
                                    <h2><?= Yii::t('app.hostess', 'DOWNLOAD APP') ?></h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="<?= Yii::$app->settings->google_play ?>" target="_blank">
                                        <?= Html::img(Yii::$app->urlManager->createAbsoluteUrl(["/images/googleplay.png"]), ["class" => "img-responsive"]) ?>
                                    </a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="<?= Yii::$app->settings->app_store ?>" target="_blank">
                                        <?= Html::img(Yii::$app->urlManager->createAbsoluteUrl(["/images/appstore.png"]), ["class" => "img-responsive"]) ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
<!--CONNECT END-->
