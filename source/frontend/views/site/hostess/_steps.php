<div class="green-banner">
    <div class="container">
        <div class="row steps">           
            <div class="col">
                <div class="step-icon registration"></div>
                <?= Yii::t('app.hostess', '1. Registration') ?>
            </div>
            <div class="col">
                <div class="step-icon signup"></div>
                <?= Yii::t('app.hostess', '2. Event Signup') ?>
            </div>
            <div class="col">
                <div class="step-icon request"></div>
                <?= Yii::t('app.hostess', '3. Request') ?>
            </div>
            <div class="col">
                <div class="step-icon booking"></div>
                <?= Yii::t('app.hostess', '4. Booking') ?>
            </div>
            <div class="col">
                <div class="step-icon payment"></div>
                <?= Yii::t('app.hostess', '5. Payment') ?>
            </div>            
        </div>
    </div>
</div>