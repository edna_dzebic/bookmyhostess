<?php
$country = Yii::$app->params["pricing"][$countryName];
?>

<div class="margin-top-20 element col-xs-12 <?= $class ?>">
    <div class="row">
        <div class="col-lg-12 col-md-8 col-sm-8 col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed pricing-table">
                            <thead>
                                <tr>
                                    <td>
                                        <?= Yii::t('app.hostess', 'Booking service per day &amp; per hostess') ?><sup>1, 4</sup>
                                    </td>
                                    <td>
                                        <?= Yii::t('app.hostess', 'Staff per day') ?><sup>2, 4</sup>
                                    </td>
                                    <td>
                                        <?= Yii::t('app.hostess', 'Overtime hour') ?><sup>3, 4</sup>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                    <td>
                                        <?= Yii::$app->util->formatPrice($country["booking_fee"]) . " EUR" ?>
                                    </td>
                                    <td>
                                        <?= Yii::$app->util->formatPrice($country["min_price"]) . " - " . Yii::$app->util->formatPrice($country["max_price"]) . " " . $country ["currency"] ?>
                                    </td>
                                    <td>
                                        <?= Yii::$app->util->formatPrice($country["overtime_fee"]) . " " . $country ["currency"] ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>    
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <sup>1</sup> <?= Yii::t('app.hostess', 'Payment is due upon completion of the booking. The customer receives our invoice by e-mail.') ?>
                </div>

                <div class="col-xs-12">
                    <sup>2</sup> <?= Yii::t('app.hostess', 'The payment is due 7 days after the event and will be paid directly to the booked person after receipt of invoice.') ?>
                </div>

                <div class="col-xs-12">
                    <sup>3</sup> <?= Yii::t('app.hostess', 'Official trade show opening hours are considered as one working day. If a working day is longer than 9 hours over time fee applies for the rest of the time.') ?>
                </div>

                <div class="col-xs-12">
                    <sup>4</sup>
                    <?= Yii::t('app.hostess', 'For late bookings made {days} or less days before the fair, a surcharge of {percentage}% will apply.', ["days" => Yii::$app->settings->last_minute_days, "percentage" => Yii::$app->settings->last_minute_percentage]) ?>
                </div>  
            </div>
        </div>
    </div>
</div>
