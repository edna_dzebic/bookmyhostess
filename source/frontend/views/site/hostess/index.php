<?php
/* @var $this yii\web\View */

use frontend\models\Layout;

\frontend\assets\IndexAsset::register($this);

$this->title =  "Messehostess werden mit BOOKmyHOSTESS.com";
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => Layout::getMetaKeywords(Layout::SITE_HOSTESS_INDEX)
], 'keywords');
$this->registerMetaTag([
    'name' => 'description',
    'content' => Layout::getMetaDescription(Layout::SITE_HOSTESS_INDEX)
], 'description');

?>
<div class="banner banner-image home hostess">

    <div class="container">

        <div class="row">
            <div class="col-lg-1 hidden-md hidden-sm hidden-xs">
            </div>
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">                        
                        <h1 class="homepageHeaderCont">                            
                            <div class="homepageHeader"><?= Yii::t('app.hostess', 'FIND AN EVENT') ?></div>
                            <div class="homepageSubHeader"><?= Yii::t('app.hostess', 'And get hired.') ?></div>
                        </h1>    
                    </div>
                </div>
            </div>
            <div class="col-lg-1 hidden-md hidden-sm hidden-xs">
            </div>
        </div>
    </div>

    <div class="form-overlay">

        <div class="container">
            <?= $this->render("_form") ?>
        </div>     
    </div>
</div>


<?= $this->render("_steps") ?>

<?= $this->render("_about") ?>

<?= $this->render("//site/common/_partners") ?>

<?= $this->render("//site/common/_gallery") ?>

<?= $this->render("_pricing") ?>

<?= $this->render("_testimonials") ?>

<?= $this->render("//site/common/_featured_clients") ?>

<?= $this->render("//site/common/_recent_blog_posts") ?>

<?= $this->render("_contact", ["contactModel" => $contactModel]) ?>

<?= $this->render("_connect") ?>

<?= $this->render("_seo") ?>