<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = Yii::t('app', 'Reset password');
?>
<div class="site-request-password-reset">

    <div class="row">
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">

        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="green-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-title">
                    <?= Yii::t('app', 'Reset password') ?>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12 bh-subtitle">
                    <?= Yii::t('app', 'Please choose your new password:') ?>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                    <div class="row">

                        <div class="col-xs-12">
                            <?= $form->field($model, 'password') ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'password_repeat') ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'SAVE'), ['class' => 'btn btn-standard btn-primary btn-green full-width', 'name' => 'login-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>            
        </div>
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">

        </div>

    </div>

</div>
