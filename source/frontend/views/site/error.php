<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>

<div class="container" id="_error">

    <div class="row">
        <div class="col-lg-4">
            <div class="green-line"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 bh-title">
            <?= Yii::t('app', 'ERROR') ?>
        </div>
    </div>

    <div class="row ">

        <div class="col-lg-12 ">
            <p><?= Yii::t('app', 'Check the Web address you entered is correct.') ?></p>
            <p><?= Yii::t('app', 'Do not use bookmarks or favorites this, but try to access the page directly from the home page. If this page has been moved, please update your favorites / bookmarks.') ?></p>
            <p><?= Yii::t('app', 'If you want to open a specific page, click the "Back" button in your web browser and select another link from. Alternatively, you can enter a different search term combination to display relevant topics.') ?></p>

            <br>
            <p>
                <?= nl2br(Html::encode($message)) ?>
            </p>
        </div>
    </div>
</div>

