<div class="green-banner">
    <div class="container">
        <div class="row steps">           
            <div class="col">
                <div class="step-icon registration"></div>
                <?= Yii::t('app.exhibitor', '1. Registration') ?>
            </div>
            <div class="col">
                <div class="step-icon selection"></div>
                <?= Yii::t('app.exhibitor', '2. Selection') ?>
            </div>
            <div class="col">
                <div class="step-icon request"></div>
                <?= Yii::t('app.exhibitor', '3. Request') ?>
            </div>
            <div class="col">
                <div class="step-icon booking"></div>
                <?= Yii::t('app.exhibitor', '4. Booking') ?>
            </div>
            <div class="col">
                <div class="step-icon payment"></div>
                <?= Yii::t('app.exhibitor', '5. Payment') ?>
            </div>            
        </div>
    </div>
</div>