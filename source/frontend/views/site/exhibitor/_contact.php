<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
?>

<!--CONTACT BEGIN-->
<div class="container content-section exhibitor-contact" id="_contact">

    <div class="row">
        <div class="col-lg-4">
            <div class="green-line"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 bh-title">
            <h2><?= Yii::t('app.exhibitor', 'CONTACT') ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 bh-subtitle">
            <?= Yii::t('app.exhibitor', 'Contact us') ?>
            <br>
            <br>
            <?= Yii::t('app.exhibitor', 'We are happy to answer any questions you have or provide you with and estimate. Just send us a message using form below. Alternatively please call or send us an email directly.') ?>
        </div>
    </div>

    <?= $this->render("//partials/_flash") ?>


    <div class="row">


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-top-20">
            <?php $form = ActiveForm::begin(['method' => 'post', 'enableClientValidation' => false]); ?>
           
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12  col-xs-12 ">
                    <?=
                    $form->field($contactModel, 'name', ['inputOptions' => [
                            'placeholder' => $contactModel->getAttributeLabel('name') . '*',
                ]])->textInput(['maxlength' => 255, 'class' => 'form-control'])->label(false)
                    ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  col-xs-12 ">
                    <?=
                    $form->field($contactModel, 'email', ['inputOptions' => [
                            'placeholder' => $contactModel->getAttributeLabel('email') . '*',
                ]])->textInput([ 'maxlength' => 255, 'class' => 'form-control'])->label(false)
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12  col-xs-12 ">
                    <?=
                    $form->field($contactModel, 'url', ['inputOptions' => [
                            'placeholder' => $contactModel->getAttributeLabel('url'),
                ]])->textInput([ 'maxlength' => 255, 'class' => 'form-control'])->label(false)
                    ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12  col-xs-12 ">
                    <?=
                    $form->field($contactModel, 'subject', ['inputOptions' => [
                            'placeholder' => $contactModel->getAttributeLabel('subject'),
                ]])->textInput([ 'maxlength' => 255, 'class' => 'form-control'])->label(false)
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?=
                    $form->field($contactModel, 'message', [ 'inputOptions' => [
                            'placeholder' => $contactModel->getAttributeLabel('message') . '*',
                ]])->textArea([
                        'maxlength' => 255, 'class' => 'form-control', 'rows' => 6])->label(false)
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <?= $form->field($contactModel, 'dsgvo', ['template' => '<div class="col-xs-1">{input}</div><div class="col-xs-10">{label}</div><div class="col-xs-12">{error}</div>'])->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]])->label(Yii::t('app', 'I have read and accepted <a href="https://www.bookmyhostess.com/page/privacy">data protection declaration </a>. I agree that my form details for processing and answering this request will be stored, processed, transmitted and used and that BOOKmyHOSTESS.com can send me relevant, request-related information by post, e-mail or telephone. The declaration of consent can be revoked at any time in writing by e-mail to dataprotection@bookmyhostess.com with effect for the future. <br> Further information on data protection can be found at <a href="https://www.bookmyhostess.com/page/privacy">https://www.bookmyhostess.com/page/privacy</a>')); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6  col-xs-12 ">
                    <?= $form->field($contactModel, 'captcha')->hiddenInput()->label(false) ?>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <?= Html::submitButton(\Yii::t('app.exhibitor', 'SEND'), ['class' => "btn btn-primary btn-green btn-submit-contact full-width"]) ?>
                </div> 
            </div>
            <?php ActiveForm::end(); ?>
        </div>

        <?= $this->render("//site/common/_contact_map") ?>

    </div>
</div>


<!--CONTACT END-->  