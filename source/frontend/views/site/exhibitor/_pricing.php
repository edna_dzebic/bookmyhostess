<?php

use yii\helpers\Html;

$pricing = Yii::$app->params["pricing"];

?>


<!--PRICING BEGIN-->
<div class="container content-section" id="_pricing">

    <div class="row">
        <div class="col-lg-4">
            <div class="green-line"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 bh-title">
            <h2><?= Yii::t('app.exhibitor', 'PRICING') ?></h2>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-12 bh-subtitle">
            <?= Yii::t('app.exhibitor', 'Check out our simple and transparent price structure') ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 text-justify">
            <p> 

                <?= Yii::t('app.exhibitor', 'As an online business, we have limited maintenance and administration costs as well as low personnel expenditures relative to the traditional agencies.Our prices are transparent and the price structure uncomplicated.                            
                            ') ?>
            </p>
        </div>
    </div>


    <div class="row js-pricing-filter margin-top-20">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <div class="row">

                        <?php
                        $i = 0;
                        $class = '';
                        ?>
                        <?php
                        foreach ($pricing as $key => $item) :

                            if ($i == 0)
                            {
                                $i++;
                                $class = 'selected ';
                            } else
                            {
                                $class = '';
                            }
                            ?>
                            <div class="col-xs-6 col-lg-3 col-md-3 col-sm-3">
                                <button class="pricing-filter-item full-width <?= $class ?>" data-option-value=".<?= $key ?>">
                                    <?= Yii::t('app.countries', $item["name"]) ?>
                                </button>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row pricing_block margin-top-20">
        <div class="col-xs-12">
            <div class="row ">	

                <?php
                $i = 0;
                $class = 'elementFadeOut';
                ?>
                <?php
                foreach ($pricing as $key => $item) :
                    if ($i == 0)
                    {
                        $i++;
                        $class = 'active ';
                    } else
                    {
                        $class = 'elementFadeOut ';
                    }

                    $class = $class . $key;
                    ?>

                    <?= $this->render("_pricing_item", ["countryName" => $key, "class" => $class]) ?> 

                <?php endforeach; ?>
            </div>
        </div>
    </div>       
</div>