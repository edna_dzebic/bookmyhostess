<?php

use yii\bootstrap\Carousel;
?>
<!--TESTIMONIALS BEGIN-->
<div class="testimonials content-section">
    <div class="container">
        <div class="row">
            <div class="slide">
                <?=
                Carousel::widget([
                    'items' => \frontend\models\ExhibitorTestimonial::getAllForCarousel(),
                    'showIndicators' => true,
                    'controls' => false,
                    'options' => [
                        'interval' => 700
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
<!--TESTIMONIALS END-->    