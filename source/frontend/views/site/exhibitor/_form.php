<?php
/* @var $this yii\web\View */

use frontend\models\EventRegisterSearch;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$model = new EventRegisterSearch();
//load from request
$model->loadParamsForFilter(Yii::$app->request->get());
?>


<div class="row">
    <div class="col-lg-1 hidden-md hidden-sm hidden-xs">
    </div>
    <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">

        <div class="row form-bg form-group-m-0">

            <?php
            $form = ActiveForm::begin(["method" => "get", "action" => ["//hostess/index"], "id" => "simple-search-form"]);
            ?>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">                                                    
                <?=
                $form->field($model, 'country')->widget(Select2::classname(), [
                    'data' => EventRegisterSearch::getCountryList(),
                    'options' => [
                        'id' => 'hostess-country-filter',
                        'class' => 'form-control',
                        'placeholder' => Yii::t("app.exhibitor", "COUNTRY"),
                        'data-url' => Yii::$app->urlManager->createUrl("//hostess/get-cities-json"),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false)->error(false);
                ?>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'city')->widget(Select2::classname(), [
                    'data' => EventRegisterSearch::getCityList($model->country),
                    'options' => [
                        'id' => 'hostess-city-filter',
                        'class' => 'form-control',
                        'placeholder' => Yii::t("app.exhibitor", "CITY"),
                        'data-url' => Yii::$app->urlManager->createUrl("//hostess/get-events-json"),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false)->error(false);
                ?>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <?php
                $eventListData = EventRegisterSearch::getEventList($model->city);

                echo $form->field($model, 'event')->widget(Select2::classname(), [
                    'data' => $eventListData,
                    'options' => [
                        'id' => 'hostess-event-filter',
                        'class' => 'form-control',
                        'placeholder' => Yii::t("app.exhibitor", "EVENT")
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false)->error(false);
                ?>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

                <?= Html::button(Yii::t('app.exhibitor', "SEARCH"), ["class" => "search-hostess-btn btn btn-primary btn-green full-width", "type" => "submit"]); ?>

            </div>   
            <?php $form->end(); ?>
        </div>
    </div>
    <div class="col-lg-1 hidden-md hidden-sm hidden-xs">
    </div>
</div>