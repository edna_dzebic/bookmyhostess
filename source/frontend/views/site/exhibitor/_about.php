<!--ABOUT US BEGIN-->
<div class="about container content-section" id="_about">

    <div class="row">
        <div class="col-lg-4">
            <div class="green-line"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 bh-title">
            <h2><?= Yii::t('app.exhibitor', 'ABOUT BOOKmyHOSTESS.com') ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 bh-subtitle">
            <?= Yii::t('app.exhibitor', 'Who we are and why we do what we do') ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <p>
                <?= Yii::t('app.exhibitor', 'BOOKmyHOSTESS.com is a platform for online booking of exhibition staff, hostesses, models and promoters. Founded as the hostess agency in September 2013 by Adela Kadirić. BOOKmyHOSTESS.com has its headquarters in Cologne. The motivation for the creation of this website is to make an innovative platform and user-friendly application, booking of hostesses, promoters and models easier and faster was.') ?>
            </p>
            <p>
                <?= Yii::t('app.exhibitor', 'We have the whole booking process provided online to allow exhibitors to cost and time savings and improve communication with hostesses, models and promoters. Simultaneously, the agency BOOKmyHOSTESS.com offers our customers great choice of reliable and qualified hostesses at reasonable daily rates for personnel.') ?> <?= Yii::t('app.exhibitor', '<br> This <a href="https://www.youtube.com/watch?v=VAgAygIaQy0"  target="_blank" style="color:#0caa9d; font-weight:bold;">video</a> clearly explains the booking process.') ?> 
            </p>

            <p>
                <?= Yii::t('app.exhibitor', 'Our passion is a smart system to offer in order to achieve greater efficiency. We now have to gather extensive experience in the field of events and fairs. Among the many advantages offered by our online booking platform compared to traditional Hostessgenturen, we would emphasize the following four:') ?>
            </p>
        </div>
    </div>

    <div class="row about-icons">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 about-item" data-placement="bottom-right" data-text-content="js-fairvages-content" data-title="<?= Yii::t('app.exhibitor', 'Fair wages of exhibition personnel') ?>">
            <div class="about-icon fairvages  js-popover-icon"></div>
            <?= Yii::t('app.exhibitor', 'Fair wages <br> of exhibition personnel') ?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 about-item"  data-placement="bottom" data-text-content="js-motivated-content" data-title="<?= Yii::t('app.exhibitor', 'Motivated personnel for your event') ?>">
            <div class="about-icon motivated  js-popover-icon"></div>
            <?= Yii::t('app.exhibitor', 'Motivated personnel <br> for your event') ?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 about-item" data-placement="bottom" data-text-content="js-belowavgprices-content" data-title="<?= Yii::t('app.exhibitor', 'Below-average prices') ?>">
            <div class="about-icon belowavgprices  js-popover-icon"></div>
            <?= Yii::t('app.exhibitor', 'Below-average <br> prices') ?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 about-item"  data-placement="bottom-left" data-text-content="js-userfriendly-content" data-title="<?= Yii::t('app.exhibitor', 'User-friendly and time-saving booking system') ?>">
            <div class="about-icon userfriendly   js-popover-icon"></div>
            <?= Yii::t('app.exhibitor', 'User-friendly and <br> time-saving booking system') ?>
        </div>
    </div>

    <div class="row hidden js-popover-content">
        <div class="col-xs-12 js-fairvages-content">
            <?= Yii::t('app.exhibitor', 'Unlike traditional recruiting agencies, our hostesses, models and promoters determine their own daily rate. The fee of the exhibition staff depends solely on their own experience, language skills and expenditure Directions and overnight at
Fair site from. The final price for our customers is not distorted by high commissions and brokerage fees. It simply pays a fixed booking fee for the care and maintenance of the platform.') ?>   
        </div>
        <div class="col-xs-12 js-motivated-content">
            <?= Yii::t('app.exhibitor', 'Our system creates two strong incentives for high commitment and dedication of hostesses, promoters and Models: <br><br>1. Above average payment: the fact that our booking fee compared is relatively low and the hostess, as explained above, can set their daily fees higher than in a usual hostess agency or employment agency, work motivation and commitment is the hostesses, models and promoters from our database much higher.<br><br>2. Transparent Feedback System: Once the event exhibitor feedback on the profile of all fair hostess, promoter or Models can leave. This assessment is then visible to future exhibitors and ensures that hostesses be used more, are punctual, reliable and responsible. Our experience shows that the prospect of a good evaluation led the hostess to maintain generally larger rofessionalität and commitment.') ?>

        </div>
        <div class="col-xs-12 js-belowavgprices-content">
            <?= Yii::t('app.exhibitor', 'The total price consists of the daily rate of the hostesses and the booking fee. The former is set by the staff itself in a competitive environment, because to apply for an event several hostesses or models. The hostesses to each other in direct competition. To be booked and competitive in their
to be colleagues, our Promoters and Models retain a reasonable price level. The booking fee, the second part of the total price is compared with the market low, because we have an online Hostess Agency low staff, maintenance and administrative costs. These savings on one side and the fair daily rates on the other hand, allow us to offer competitive prices on the market.') ?>

        </div>
        <div class="col-xs-12 js-userfriendly-content">
            <?= Yii::t('app.exhibitor', 'Our system provides a large online database, immediate access to the profiles of the available hostesses and models allows. The customers of our hostess agency can start automatically and immediately after a brief registration to research the appropriate hostess, a model or promoter. This search can be limited by criteria such as price, language skills or age. Following a successful search the selected person can be requested and booked directly online. In BOOKmyHOSTESS.com the entire booking process is reduced to a few clicks and a few hours. It has never been easier and faster to find suitable and motivated Exhibition and promotion staff.') ?>
        </div>
    </div>
</div>
<!--ABOUT US END-->