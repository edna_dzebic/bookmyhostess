<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Thank you');
?>
<div class="banner banner-image thank-you">

    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 register-form">
                <div class="row">
                    <div class="col-xs-12 title">
                        <?= Yii::t('app', 'THANK YOU') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 subtitle">
                        <?= Yii::t('app', 'For using our system.'); ?>
                    </div>
                </div>
                <div class="row margin-top-10">
                    <div class="col-xs-12">
                        <?= \Yii::t('app', 'Email was sent to:'); ?> <?= preg_replace('/(?<=.).(?=.*@)/u', '*', $email); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>     
</div>