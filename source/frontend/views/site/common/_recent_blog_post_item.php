<?php

use yii\helpers\Html;

$image = \common\components\PhpThumb::Url([
            'src' => $model->image,
            'w' => 360,
            'h' => 270,
            'zc' => 'T',
            'q' => 90,
        ]);
?>

<div class="blog-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
    <a href="<?= \Yii::$app->urlManager->createUrl(["//blog/view", "id" => $model->id]); ?>">
        <?= Html::img($image, ["class" => "img-responsive", "alt" => $model->title]) ?>
        <div class="blog-content">
            <p class="title">
                <?= $model->title ?>
            </p>
            <div class="content">
                <?= $model->short_content ?>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> <p class="date"><?= Yii::$app->util->formatDate($model->date_created) ?></p>  </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><p class="text-right"><?= Yii::t('app', 'Continue reading') ?>...</p>   </div>
            </div>
        </div>
    </a>
</div>