
        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 margin-top-20">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2513.59946617742!2d6.837645951002067!3d50.94961997944695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47bf3a586a04f243%3A0x725119f56ee89f24!2sOttostra%C3%9Fe+7%2C+50859+K%C3%B6ln%2C+Germany!5e0!3m2!1sen!2sba!4v1462574464779" width="100%" height="308" frameborder="0" style="border:0;" allowfullscreen></iframe>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 margin-top-20">
            <div class="row address">
                <div class="col-lg-12 green-text">
                    BOOKmyHOSTESS.com                    
                </div>
                <div class="col-lg-12 margin-top-10">
                    Ottostr. 7
                    <br>
                    50859 <?= Yii::t('app', 'Cologne') ?>
                    <br>
                    <?= Yii::t('app', 'Germany') ?>
                </div>
                <div class="col-lg-12  margin-top-20">
                    Tel.: <a href="tel:004922344301397" class="green-text">+492234 4301397</a>
                    <br>
                    Fax: +492234 4301396
                </div>
                <div class="col-lg-12 margin-top-20 ">

                    <p class="visible-xs">
                    </p>

                    <a href="https://www.bookmyhostess.com" class="green-text">www.bookmyhostess.com</a>

                </div>
            </div>
        </div>