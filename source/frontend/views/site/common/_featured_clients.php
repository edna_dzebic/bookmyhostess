<?php

use yii\helpers\Html;
use frontend\models\Clients;

$gallery = Clients::getThumbnails();
?>

<!--FEATURED CLIENTS BEGIN-->
<div class="container content-section"  id="_clients">

    <div class="row">
        <div class="col-lg-4">
            <div class="green-line"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 bh-title">
            <h2><?= Yii::t('app', 'FEATURED CLIENTS') ?></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 bh-subtitle">
            <?= Yii::t('app', 'Some of the companies that have used our system') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="scrollbar">
                <div class="handle">
                    <div class="mousearea"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
            <div class="controls text-left">
                <button class="btn js-prev"> < </button>
            </div>
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
            <div class="frame" id="centered_clients" >
                <ul class="clearfix">
                    <?php foreach ($gallery as $image) : ?>
                        <li>
                            <div class="bg-client-image" style="background-image: url('<?= $image ?>')">
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

        </div>
        <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
            <div class="controls text-right">
                <button class="btn js-next"> > </button>
            </div>
        </div>
    </div>
</div>


<!--FEATURED CLIENTS END-->
