<?php

use frontend\models\Gallery;

$gallery = Gallery::getVisibleThumbnails();
$hidden = Gallery::getHiddenThumbnails();

$videoLink = Yii::$app->util->getVideoLink();
?>

<!--GALLERY BEGIN-->
<div class="container-fluid content-section" id="gallery">

    <div class="row">
        <div class="js-gallery-items col-lg-6 col-md-12 col-sm-12 col-xs-12 margin-top-10">
            <div class="row">
                <?php foreach ($gallery as $image) : ?>

                    <?= $this->render("_gallery_item", ["image" => $image]) ?>

                <?php endforeach; ?>   
            </div>

            <div class="row hidden">
                <?php foreach ($hidden as $image) : ?>

                    <?= $this->render("_gallery_item", ["image" => $image]) ?>

                <?php endforeach; ?>   
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 margin-top-10">
            <div class="row">
                <video controls crossorigin poster="<?= Yii::$app->urlManager->createAbsoluteUrl(["/images/videobg.png"]) ?>">
                    <!-- Video files -->
                    <source src="<?= Yii::$app->urlManager->createAbsoluteUrl([$videoLink]) ?>" type="video/mp4">

                    <!-- Fallback for browsers that don't support the <video> element -->
                    <a href="<?= Yii::$app->urlManager->createAbsoluteUrl([$videoLink]) ?>" download><?= Yii::t('app', 'Download') ?></a>
                </video> 
            </div>
        </div>
    </div>
</div>
<!--GALLERY END





