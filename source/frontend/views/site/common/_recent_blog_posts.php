<?php

use frontend\models\Blog;

$blogs = Blog::getTopThree();

foreach ($blogs as $blog)
{
    Yii::$app->jsonld->addBlogDoc($blog);
}
Yii::$app->jsonld->registerScripts();
?>
<!--RECENT BLOG POSTS BEGIN-->
<div class="blogs content-section">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-20">
                    <div class="row">
                        <div class="col-xs-12 blog-title white-title">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="green-line"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 bh-title white">
                                    <h2> <?= Yii::t('app', 'RECENT BLOG POSTS') ?></h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="row blog-post-items">
                                <?php foreach ($blogs as $blog) : ?>
                                    <?= $this->render("_recent_blog_post_item", ["model" => $blog]) ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>               
                </div>
            </div>
        </div>
    </div>   
</div>
<!--RECENT BLOG POSTS END-->
