<?php ?>

<div class="col-lg-3 col-sm-3 col-xs-6">
    <div class="folio-item">
        <div class="folio-image">
            <img class="img-responsive" src="<?= $image["thumb"] ?>" alt="<?= $image["alt"] ?>" title="<?= $image["title"] ?>">
        </div>
        <div class="overlay">
            <div class="overlay-content">
                <div class="overlay-text">

                    <div class="folio-overview">
                        <span class="folio-expand">
                            <a href="<?= $image["full"] ?>" data-lightbox="portfolio">
                                <i class="fa fa-search-plus"></i></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>