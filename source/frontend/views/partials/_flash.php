<?php

use kartik\widgets\Alert;
?>

<?php
if (
        ($flash = Yii::$app->session->getFlash('contact.success')) ||
        ($flash = Yii::$app->session->getFlash('success.booking')) ||
        ($flash = Yii::$app->session->getFlash('cart.success')) ||
        ($flash = Yii::$app->session->getFlash('payment.success')) ||
        ($flash = Yii::$app->session->getFlash('success'))
):
    ?>
    <div class = "row margin-top-20">
        <div class = "col-lg-12 text-center">

            <?=
            Alert::widget(
                    [
                        'type' => Alert::TYPE_SUCCESS,
                        'options' => ['class' => 'alert-success'],
                        'body' => $flash,
                        'showSeparator' => true,
                        'delay' => 0
                    ]
            );
            ?>
        </div>
    </div>
<?php endif; ?>

<?php
if (
        ($flash = Yii::$app->session->getFlash('contact.error')) ||
        ($flash = Yii::$app->session->getFlash('event.signup')) ||
        ($flash = Yii::$app->session->getFlash('error.booking')) ||
        ($flash = Yii::$app->session->getFlash('cart.error'))
) :
    ?>
    <div class="row margin-top-20">
        <div class="col-lg-12 text-center">
            <?=
            Alert::widget(
                    [
                        'type' => Alert::TYPE_DANGER,
                        'options' => ['class' => 'alert-danger'],
                        'body' => $flash,
                        'showSeparator' => true,
                        'delay' => 0
                    ]
            );
            ?>
        </div>
    </div>
<?php endif; ?>