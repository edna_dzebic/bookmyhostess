<table style="text-align:justify; ">
    <?php if ($model->getIsNormalInvoice()) : ?>
        <?php
        // if exhibitor is not German
        if (
                isset($model->userRequested->exhibitorProfile->country) &&
                Yii::$app->util->isGerman($model->userRequested->exhibitorProfile->country->id) == false
        ):
            ?>

            <?php
            // if is EU invoice type
            if ($model->userRequested->exhibitorProfile->country->getIsEuInvoice()):
                ?>

                <tr>
                    <td align="left" height="10">
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Yii::t('app.exhibitor', 'Reverse Charge - According to Article 194, 196 of Council Directive 2006/112/EEC, VAT liability rests with the service recipient {tax_id}.', ["tax_id" => $model->userRequested->exhibitorProfile->tax_number]) ?>
                    </td>
                </tr>
                <tr>
                    <td align="left" height="10">
                    </td>
                </tr>
                <?php
            // if third country - not Germany and not EU
            else :
                ?>
                <tr>
                    <td align="left" height="10">
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Yii::t('app.exhibitor', 'Not taxable in Germany.') ?>
                    </td>
                </tr>
            <?php endif; ?>
        <?php endif; ?>


        <?php if ($model->isPaypalPayment()): ?>
            <tr>
                <td>
                    <?= Yii::t('app.exhibitor', 'According to our General Terms and Conditions, payment is due immediately. The amount will be automatically collected from Your account. The PayPal transaction ID is: {paypalTransactionID}.', ["paypalTransactionID" => $model->paypal_transaction_id]) ?>
                </td> 
            </tr>
        <?php endif; ?>
        <?php if ($model->isInvoicePayment()): ?>
            <tr>
                <td>
                    <?= Yii::t('app.exhibitor', 'Please transfer the invoice amount with the corresponding invoice number within 7 days to our account.') ?>
                </td> 
            </tr>
        <?php endif; ?>

        <tr>
            <td align="left" height="10">
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('app.exhibitor', 'The invoice was issued electronically and is therefore valid without signature.') ?>
            </td>
        </tr>
    <?php endif; ?>
</table>