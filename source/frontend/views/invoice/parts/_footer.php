<table style="text-align:justify; ">   
    <tr>
        <td>
            <?= Yii::t('app.exhibitor', 'If you have any questions or comments, please contact us at +4922344301397.') ?>
        </td>
    </tr>
    <tr>
        <td align="left" height="10">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.exhibitor', 'Thank You for using BOOKmyHOSTESS.com! We are looking forward to serving You in the near future.') ?>
        </td>
    </tr>
    <tr>
        <td align="left" height="10">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.exhibitor', 'Best Regards, <br> Your BOOKmyHOSTESS.com - Team.') ?>
        </td>
    </tr>
</table>