<?php if ($model->getIsNormalInvoice()) : ?>

    <?php
    // if from Germany
    if (isset($model->userRequested->exhibitorProfile->country) &&
            Yii::$app->util->isGerman($model->userRequested->exhibitorProfile->country->id)):
        ?>

        <tr>
            <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                <b><?= Yii::t('app.exhibitor', 'Net amount') ?></b>
            </td>  
            <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                <?= Yii::$app->util->formatPrice($model->getTotalBookingPrice()) ?>
            </td>
        </tr>
        <tr>
            <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                <b><?= Yii::t('app.exhibitor', '+19% value added tax') ?></b>
            </td>   
            <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                <?= Yii::$app->util->formatPrice($model->getTaxAmount()) ?>
            </td>
        </tr>
        
        <tr>
            <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                <b><?= Yii::t('app.exhibitor', 'Gross amount') ?></b>
            </td>     
            <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                <b><?= Yii::$app->util->formatPrice($model->getGrossAmount()) ?></b>
            </td>
        </tr>

        <?= $this->render('_coupon_germany', ["model" => $model]) ?>

    <?php else: ?>

        <?= $this->render('_gross_total', ["model" => $model]) ?>

        <?= $this->render('_coupon_other', ["model" => $model]) ?>

    <?php endif; ?>

<?php else: ?>

    <?= $this->render('_receipt_total', ["model" => $model]) ?>

<?php endif; ?>