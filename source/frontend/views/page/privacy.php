<?php
$this->title = Yii::t('privacy', 'PRIVACY');
?>

<div class="row">
    <div class="col-xs-4">
        <div class="green-line"></div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-title">
        <?= Yii::t('privacy', 'PRIVACY') ?>
    </div>
</div>


<div class="row privacy-content">
    <div class="col-xs-12 text-justify">
        <p><b>Stand: 02.02.2021</b></p>
        <p>
            Die folgenden Datenschutzhinweise informieren Sie über Art und Umfang der Verarbeitung Ihrer personenbezogenen Daten im Rahmen der Nutzung unserer online Plattform. Personenbezogene Daten sind Informationen, die Ihrer Person direkt oder mittelbar zuzuordnen sind bzw. zugeordnet werden können. Als gesetzliche Grundlage für den Datenschutz dient insbesondere die Datenschutz-Grundverordnung (DSGVO).
        </p>        
        <p>Die nachfolgenden Datenschutzbestimmungen gelten f&uuml;r das Online-Angebot unter BOOKmyHOSTESS.com (&quot;Online-Angebot&quot;).</p>

        <p><b>1. Wer sind wir? (Verantwortlicher)</b></p>
        <p>Verantwortliche Stelle (auch: &bdquo;Verantwortlicher&ldquo;) ist BOOKmyHOSTESS.com gef&uuml;hrt von Adela Kadiric, Ottostr. 7, 50859, nachfolgend &bdquo;wir&ldquo; bzw. &bdquo;uns&ldquo;.</p>
        <p>Ausnahmen werden in diesen Datenschutzbestimmungen erl&auml;utert.</p>

        <p><b>2. Kontaktdaten unseres Datenschutzbeauftragten</b></p>
        <p>Sollten Sie Fragen zu dieser Datenschutzerkl&auml;rung oder generell zur Verarbeitung Ihrer Daten im Rahmen dieses Online-Angebots haben, wenden Sie sich bitte an unsere Datenschutzbeauftragte:</p>
        <p>Jasmina Babic, Ottostr. 7, 50859 K&ouml;ln
            <br>
            E-Mail: dataprotection@bookmyhostess.com
            <br>
            Tel.: +49 176 34618678
        </p>

        <p><b>3. Welche personenbezogenen Daten verarbeiten wir von Ihnen?</b></p>
        <p><b>Allgemeines</b></p>
        <p>Personenbezogene Daten sind alle Informationen, die sich auf eine identifizierte oder identifizierbare nat&uuml;rliche Person beziehen (z. B. Name, Fotos, Anschrift, Telefonnummer, Geburtsdatum oder E-Mail-Adresse).</p>
        <p>Wenn wir personenbezogene Daten verarbeiten bedeutet dies, dass wir diese z. B. erheben, speichern, nutzen, an andere &uuml;bermitteln oder l&ouml;schen.</p>
        <p>Grunds&auml;tzlich k&ouml;nnen Sie unser Online-Angebot ohne Angabe personenbezogener Daten nutzen. Die Nutzung bestimmter Services kann allerdings die Angabe personenbezogener Daten erfordern, z. B. eine Registrierung. Pflichtangaben sind regelm&auml;&szlig;ig mit einem * gekennzeichnet. Wenn Sie uns die hierf&uuml;r erforderlichen Daten nicht zur Verf&uuml;gung stellen m&ouml;chten, k&ouml;nnen Sie die entsprechenden Services leider nicht nutzen.</p>
        <p><b>Wof&uuml;r nutzen wir Ihre Daten und auf welcher Rechtsgrundlage?</b></p>

        <p>
            Nach Maßgabe des Art. 13 DSGVO teilen wir Ihnen die Rechtsgrundlagen unserer Datenverarbeitungen mit. Sofern die Rechtsgrundlage in der Datenschutzerklärung nicht genannt wird, gilt Folgendes:
            <br><br>
            Soweit wir für Verarbeitungsvorgänge personenbezogener Daten eine Einwilligung der betroffenen Person einholen, dient Art. 6 Abs. 1 lit. a DSGVO als Rechtsgrundlage.
            <br><br>
            Bei der Verarbeitung von personenbezogenen Daten, die zur Erfüllung eines Vertrages, dessen Vertragspartei die betroffene Person ist, erforderlich ist, dient Art. 6 Abs. 1 lit. b DSGVO als Rechtsgrundlage. Dies gilt auch für Verarbeitungsvorgänge, die zur Durchführung vorvertraglicher Maßnahmen erforderlich sind.
            <br><br>
            Soweit eine Verarbeitung personenbezogener Daten zur Erfüllung einer rechtlichen Verpflichtung erforderlich ist, der wir unterliegen, dient Art. 6 Abs. 1 lit. c DSGVO als Rechtsgrundlage.
            <br><br>
            Für den Fall, dass lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person eine Verarbeitung personenbezogener Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO als Rechtsgrundlage.
            <br><br>
            Ist die Verarbeitung zur Wahrung eines berechtigten Interesses unseres Unternehmens oder eines Dritten erforderlich und überwiegen die Interessen, Grundrechte und Grundfreiheiten des Betroffenen das erstgenannte Interesse nicht, so dient Art. 6 Abs. 1 lit. f DSGVO als Rechtsgrundlage für die Verarbeitung.<br>
        </p>
        <p></p>
        <p>Wir verarbeiten Ihre personenbezogenen Daten zu nachfolgenden Zwecken und auf Basis der genannten Rechtsgrundlagen. F&uuml;r den Fall, dass die Datenverarbeitung auf einer Interessenabw&auml;gung beruht, erl&auml;utern wir Ihnen auch unser berechtigtes Interesse, das wir mit der Verarbeitung verfolgen:</p>
        <p></p>

        <div class="table-responsive">

            <table class="table-bordered">
                <tbody>
                    <tr>
                        <td>
                            <p><b>Nr.</b></p>
                        </td>
                        <td>
                            <p><b>Zweck der Verarbeitung</b></p>
                        </td>
                        <td>
                            <p><b>Rechtsgrundlage der Verarbeitung sowie Darlegung des berechtigten Interesses, soweit relevant</b></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>1.</p>
                        </td>
                        <td>
                            <p>Bereitstellung dieses Online-Angebots und Vertragserf&uuml;llung gem&auml;&szlig; unseren AGB &nbsp;einschlie&szlig;lich Abrechnung.</p>
                        </td>
                        <td>
                            <p>Vertragserf&uuml;llung</p>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <p>2.</p>
                        </td>
                        <td>
                            <p>Personalisierung des Angebots.</p>
                        </td>
                        <td>
                            <p>Vertragserf&uuml;llung oder Einwilligung</p>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <p>3.</p>
                        </td>
                        <td>
                            <p>Analyse des Angebots zur Ermittlung von Nutzungsverhalten einschlie&szlig;lich Marktforschung und Reichweitenmessung.</p>
                        </td>
                        <td>
                            <p>Interessenabw&auml;gung; wir haben ein berechtigtes Interesse, das Nutzungsverhalten in unserem Online-Angebot zu analysieren um dies fortlaufend zu verbessern bzw. an die Interessen unserer Nutzer anpassen zu k&ouml;nnen.</p>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <p>4.</p>
                        </td>
                        <td>
                            <p>Erfassung und Auswertung von Nutzungsverhalten f&uuml;r interessenbasierte Werbung</p>
                        </td>
                        <td>
                            <p>Interessenabw&auml;gung; wir haben ein berechtigtes Interesse, interessenbasierte Werbung auszuspielen und unseren Nutzern Werbung zu bieten, die auf deren pers&ouml;nliche Interessen zugeschnitten ist.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>5.</p>
                        </td>
                        <td>
                            <p>Eigenwerbung im gesetzlich zul&auml;ssigen Umfang bzw. einwilligungsbasiert.</p>
                        </td>
                        <td>
                            <p>Einwilligung oder Interessenabw&auml;gung; wir haben ein berechtigtes Interesse an Direktmarketing, solange dies im Einklang mit datenschutz- und wettbewerbsrechtlichen Vorgaben erfolgt.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>6.</p>
                        </td>
                        <td>
                            <p>Durchf&uuml;hrung von Gewinnspielen gem&auml;&szlig; den jeweiligen Gewinnspielbedingungen.</p>
                        </td>
                        <td>
                            <p>Vertragserf&uuml;llung</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>7.</p>
                        </td>
                        <td>
                            <p>Personalisierte/individuelle Produktempfehlung</p>
                        </td>
                        <td>
                            <p>Interessenabw&auml;gung; wir haben ein berechtigtes Interesse, interessenbasierte Produktempfehlungen auszuspielen, um unseren Nutzern Werbung zu bieten, die auf deren pers&ouml;nliche Interessen zugeschnitten ist.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>8.</p>
                        </td>
                        <td>
                            <p>Einbindung von Social Plugins und Social Share-Funktionen.</p>
                        </td>
                        <td>
                            <p>Interessenabw&auml;gung; wir haben wir ein berechtigtes Interesse daran, auf den Wunsch unserer Nutzer, die ein Social Plugin aktiviert haben, deren Informationen mit dem entsprechenden sozialen Netzwerk zu teilen.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>9.</p>
                        </td>
                        <td>
                            <p>Ermittlung und ggf. Sperrung von Nutzern, die einen sogenannten Adblocker installiert haben und dadurch Werbung blockieren.</p>
                        </td>
                        <td>
                            <p>Interessenabw&auml;gung; wir haben ein berechtigtes Interesse daran, unsere ganz oder teilweise werbefinanzierten Angeboten nur Nutzern zur Verf&uuml;gung zu stellen, die Werbung nicht blockieren.</p>
                        </td>
                    </tr>
                    <tr >
                        <td>
                            <p>10.</p>
                        </td>
                        <td>
                            <p>Ermittlung von St&ouml;rungen und Gew&auml;hrleistung der Systemsicherheit einschlie&szlig;lich Aufdeckung und Nachverfolgung unzul&auml;ssiger Zugriffe und Zugriffsversuche auf unsere Web-Server</p>
                        </td>
                        <td>
                            <p>Erf&uuml;llung unserer rechtlichen Verpflichtungen im Bereich Datensicherheit sowie Interessenabw&auml;gung; wir haben ein berechtigtes Interesse an der Beseitigung von St&ouml;rungen, der Gew&auml;hrleistung der Systemsicherheit und der Aufdeckung und Nachverfolgung unzul&auml;ssiger Zugriffsversuche bzw. Zugriffe.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>11.</p>
                        </td>
                        <td>
                            <p>Wahrung und Verteidigung unserer Rechte</p>
                        </td>
                        <td>
                            <p>Interessenabw&auml;gung; wir haben ein berechtigtes Interesse an der Geltendmachung und Verteidigung unserer Rechte.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>12.</p>
                        </td>
                        <td>
                            <p>Messaging Dienste</p>
                        </td>
                        <td>
                            <p>Einwilligung</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <p></p>
        <p>Auf Anfrage k&ouml;nnen Sie von uns Informationen zu den von uns vorgenommenen Interessenabw&auml;gungen erhalten. Nutzen Sie hierf&uuml;r einfach die Angaben in Abschnitt Kontakt.</p>
        <p>Bitte beachten Sie Ihr Widerspruchsrecht bei der Verarbeitung von Daten zum Zwecke des Direktmarketings bzw. aus pers&ouml;nlichen Gr&uuml;nden (siehe Abschnitt Betroffenenrechte).</p>
        <p><b>Registrierung</b></p>
        <p>Sofern Sie Leistungen in Anspruch nehmen m&ouml;chten, die einen Vertragsschluss erfordern, werden wir Sie bitten, sich zu registrieren. Im Rahmen der Registrierung erheben wir die f&uuml;r die Vertragsbegr&uuml;ndung und -erf&uuml;llung erforderlichen personenbezogenen Daten (z. B. Vorname, Nachname, Geburtsdatum, E-Mail-Adresse, Fotos, ggf. Angaben zur gew&uuml;nschten Zahlungsart bzw. zum Kontoinhaber) sowie ggf. weitere Daten auf freiwilliger Basis. Pflichtangaben kennzeichnen wir mit einem *.</p>

        <p>
            <b>Kontaktformular</b>            
        </p>
        <p>
            Auf unserer Webseite befindet sich ein Kontaktformular, welches Sie für die elektronische Kontaktaufnahme mit uns nutzen können. Nehmen Sie diese Möglichkeit wahr, werden die in der Eingabemaske angegebenen Daten an uns übermittelt und gespeichert. Diese Daten sind:

        <ul>
            <li>Name,</li>
            <li>E-Mail-Adresse und</li>
            <li>ggf. Ihre personenbezogenen Angaben im Betreff oder Freitext.</li>
        </ul>
        </p>
        <p>
            Im Zeitpunkt der Absendung der Nachricht werden zudem folgende Daten gespeichert:
        <ul>
            <li>Datum und Uhrzeit des Absendens</li>
        </ul>
        </p>

        <p>
            Rechtsgrundlage für diese Datenverarbeitung ist Art. 6 Abs. 1 S. 1 lit. a DSGVO. Für die Verarbeitung dieser Daten wird im Rahmen des Absendevorgangs Ihre Einwilligung eingeholt und auf diese Datenschutzerklärung verwiesen. Ein Absenden des Kontaktformulars ohne diese Einwilligungserklärung ist nicht möglich. Alternativ ist eine Kontaktaufnahme über die bereitgestellte E-Mail-Adresse möglich. In diesem Fall werden die mit der E-Mail übermittelten personenbezogenen Daten des Nutzers gespeichert.
            <br>
            Wir benötigen die erhobenen Daten allein zur Beantwortung Ihrer Kontaktanfrage. Die Daten, die im Zeitpunkt der Absendung (z.B. Datum und Uhrzeit des Absendens) gespeichert werden dienen dazu, die Sicherheit unserer Systeme zu gewährleisten und den Missbrauch des Kontaktformulars zu verhindern. 
            <br>
            Rechtsgrundlage hierfür ist unser berechtigtes Interesse gem. Art. 6 Abs. 1 lit. f) DSGVO. Wir haben ein berechtigtes Interesse daran, einen Missbrauch unseres Kontaktformulars zu verhindern bzw. nachweisen zu können.
            <br>
            Ergibt sich aus der Kontaktanfrage ein Vertrags- oder Vertragsanbahnungsverhältnis, ist die Rechtsgrundlage für die weitere Datenverarbeitung Art. 6 Abs. 1 S. 1 lit. b DSGVO.
            <br>
            Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Für die personenbezogenen Daten aus der Eingabemaske des Kontaktformulars ist dies dann der Fall, wenn die jeweilige Konversation mit dem Nutzer beendet ist. Beendet ist die Konversation dann, wenn sich aus den Umständen entnehmen lässt, dass der betroffene Sachverhalt abschließend geklärt ist.
            <br>
            <br>
            <b>
                Sie haben die Möglichkeit, Ihre Einwilligung in die Verarbeitung und Speicherung Ihrer Daten jederzeit mit Wirkung für die Zukunft per E-Mail: dataprotection@bookmyhostess.com zu widerrufen. 
            </b>
        </p>

        <p><b>4. Wer bekommt Ihre personenbezogenen Daten und warum?</b></p>

        <p>
            Ihre personenbezogenen Daten werden von uns grundsätzlich nur dann an Dritte gegeben, soweit dies zur Vertragserfüllung erforderlich ist, wir oder der Dritte ein berechtigtes Interesse an der Weitergabe haben oder Ihre Einwilligung hierfür vorliegt. Sofern Daten an Dritte übermittelt werden, wird dies in diesen Datenschutzbestimmungen erläutert. 


            Darüber hinaus können Daten an Dritte übermittelt werden, soweit wir aufgrund gesetzlicher Bestimmungen oder durch vollstreckbare behördliche oder gerichtliche Anordnung hierzu verpflichtet sein sollten.

            Eine weitergehende Übermittlung der Daten findet nicht statt bzw. nur dann, wenn Sie dieser ausdrücklich zugestimmt haben.
        </p>
        <p>
            Grundlage für die Datenverarbeitung ist Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet.
        </p>
        <p>
            Empfänger von personenbezogenen Daten sind:
        <ul>
            <li> Kunden </li>
            <li> Bezahldiensleiter </li>
            <li> Mitarbeiter</li>
            <li> Steuerberater (Notwendige Angaben in den Rechnungen)</li>
            <li> Finanzamt (Notwendige Angaben in den Rechnungen)</li>
            <li> Dienstleister</li>
        </ul>
        </p>

        <p>
            Wir behalten uns vor, bei der Erhebung bzw. Verarbeitung von Daten Dienstleister einzusetzen. Dienstleister erhalten von uns nur die personenbezogenen Daten, die sie für ihre konkrete Tätigkeit benötigen. So kann z. B. Ihre E-Mail-Adresse an einen Dienstleister weitergeben werden, damit dieser Ihnen einen bestellten Newsletter ausliefern kann. Dienstleister können auch damit beauftragt werden, Serverkapazitäten zur Verfügung zu stellen. Dienstleister werden in der Regel als sogenannte Auftragsverarbeiter eingebunden, die personenbezogene Daten der Nutzer dieses Online-Angebots nur nach unseren Weisungen verarbeiten dürfen.
        </p>

        <p><b>5. Wann geben wir Daten in L&auml;nder, die nicht zum Europ&auml;ischen Wirtschaftsraum geh&ouml;ren?</b></p>
        <p>Wir geben personenbezogene Daten auch an Dritte, die ihren Sitz in Nicht-EWR-Ländern haben.  
            Die personenbezogenen Daten werden an alle Kunden aus den Drittländern nur dann übermittelt, wenn und soweit die Übermittlung zur Erfüllung eines Vertrages mit der betroffenen Person oder zum Abschluss oder zur Erfüllung eines Vertrages im Interesse der betroffenen Person erforderlich ist (Art. 49 Abs. 1 S.1 lit. b und c – g DS-GVO). </p>

        <p><b>6. Wie lange speichern wir Ihre Daten?</b></p>
        <p>Wir speichern Ihre Daten solange, wie dies zur Erbringung unseres Online-Angebots und der damit verbundenen Services erforderlich ist bzw. wir ein berechtigtes Interesse an der weiteren Speicherung haben. In allen anderen F&auml;llen l&ouml;schen wir Ihre personenbezogenen Daten mit Ausnahme solcher Daten, die wir zur Erf&uuml;llung gesetzlicher (z. B. steuer- oder handelsrechtlicher) Aufbewahrungsfristen weiter vorhalten m&uuml;ssen (z. B. Rechnungen).</p>
        <p>Daten, die einer Aufbewahrungsfrist unterliegen, sperren wir bis zum Ablauf der Frist.</p>

        <p><b>7. Verarbeitung von Daten durch Kreditinstitute und Zahlungsdienstleister; Forderungsmanagement</b></p>
        <p>Verarbeitung von Daten durch Kreditinstitute und Zahlungsdienstleister</p>
        <p>Wir setzen externe Zahlungsdienstleister ein. Je nachdem, welchen Zahlungsweg Sie im Bestellprozess ausw&auml;hlen, geben wir die zur Abwicklung von Zahlungen erhobenen Daten (z. B. Bankverbindung oder Kreditkartendaten) an das mit der Zahlung beauftragte Kreditinstitut oder an von uns beauftrage Zahlungsdienstleister weiter. Zum Teil erheben die Zahlungsdienstleister diese Daten auch selbst in eigener Verantwortung. Es gilt insoweit die Datenschutzerkl&auml;rung des jeweiligen Zahlungsdienstleisters.</p>
        <p>Entf&auml;llt der Speicherungszweck oder l&auml;uft eine vom Gesetzgeber vorgeschriebene Speicherfrist ab (z.B. 5 Jahre f&uuml;r Transaktionsdaten aufgrund von Geldw&auml;schebestimmungen), werden die personenbezogenen Daten routinem&auml;&szlig;ig und entsprechend den gesetzlichen Vorschriften gesperrt oder gel&ouml;scht.</p>
        <p>Forderungsmanagement</p>
        <p>Wir behalten uns vor, den Einzug von Forderungen durch Dienstleister vornehmen zu lassen, die diese als sogenannte Auftragsverarbeiter einziehen.</p>
        <p>Wir haben dar&uuml;ber hinaus ein berechtigtes Interesse, Forderungen an Dritte zu ver&auml;u&szlig;ern und in diesem Zusammenhang dem jeweiligen K&auml;ufer der Forderung die f&uuml;r den Forderungseinzug ben&ouml;tigten Daten zu &uuml;bermitteln. Beim Forderungseinzug werden K&auml;ufer von Forderungen in eigenem Namen t&auml;tig und verarbeiten die &uuml;bermittelten Daten in eigener Verantwortung. Es gelten insoweit die Datenschutzbestimmungen des jeweiligen Forderungsk&auml;ufers.</p>

        <p><b>8. Log-Dateien</b></p>
        <p>Bei jeder Nutzung des Internet werden von Ihrem Internet-Browser automatisch bestimmte Informationen &uuml;bermittelt und von uns in sogenannten Log-Dateien gespeichert.</p>
        <p>Die Log-Dateien werden von uns zur Ermittlung von St&ouml;rungen und Sicherheitsgr&uuml;nden (z. B. zur Aufkl&auml;rung von Angriffsversuchen) f&uuml;r 7 bis 14 Tage gespeichert und danach gel&ouml;scht. Log-Dateien, deren weitere Aufbewahrung zu Beweiszwecken erforderlich ist, sind von der L&ouml;schung ausgenommen bis zur endg&uuml;ltigen Kl&auml;rung des jeweiligen Vorfalls und k&ouml;nnen im Einzelfall an Ermittlungsbeh&ouml;rden weitergegeben werden.</p>
        <p>In den Log-Dateien werden insbesondere folgende Informationen gespeichert:</p>
        <ul>
            <li>IP-Adresse (Internetprotokoll-Adresse) des Endger&auml;ts, von dem aus auf das Online-Angebot zugriffen wird;</li>
            <li>Internetadresse der Website, von der aus das Online-Angebot aufgerufen wurde (sog. Herkunfts- oder Referrer-URL);</li>
            <li>Name des Service-Providers, &uuml;ber den der Zugriff auf das Online-Angebot erfolgt;</li>
            <li>Name der abgerufenen Dateien bzw. Informationen;</li>
            <li>Datum und Uhrzeit sowie Dauer des Abrufs;</li>
            <li>&uuml;bertragene Datenmenge;</li>
            <li>Betriebssystem und Informationen zum verwendeten Internet-Browser einschlie&szlig;lich installierter Add-Ons (z. B. f&uuml;r den Flash Player);</li>
            <li>http-Status-Code (z. B. &quot;Anfrage erfolgreich&quot; oder &quot;angeforderte Datei nicht gefunden&quot;);</li>
            <li>Log-Dateien werden dar&uuml;ber hinaus f&uuml;r die Web-Analyse verwendet.</li>
        </ul>

        <p><b>9. Cookies</b></p>
        <p><b>Was sind Cookies?</b></p>
        <p>Cookies sind kleine Textdateien, die beim Besuch einer Internetseite verschickt und im Browser des Nutzers gespeichert werden. Wird die entsprechende Internetseite erneut aufgerufen, sendet der Browser des Nutzers den Inhalt der Cookies zur&uuml;ck und erm&ouml;glicht so eine Wiedererkennung des Nutzers. Bestimmte Cookies werden nach Beendigung der Browser-Sitzung automatisch gel&ouml;scht (sogenannte Session Cookies), andere werden f&uuml;r eine vorgegebene Zeit bzw. dauerhaft im Browser des Nutzers gespeichert und l&ouml;schen sich danach selbst&auml;ndig (sogenannte tempor&auml;re oder permanente Cookies).</p>
        <p><b>Welche Daten werden in den Cookies gespeichert?</b></p>
        <p>In Cookies werden grunds&auml;tzlich keine personenbezogenen Daten gespeichert, sondern nur eine Online-Kennung.</p>
        <p><b>Wie k&ouml;nnen Sie die Verwendung von Cookies verhindern bzw. Cookies l&ouml;schen?</b></p>
        <p>Sie k&ouml;nnen die Speicherung von Cookies &uuml;ber Ihre Browser-Einstellungen deaktivieren und bereits gespeicherte Cookies jederzeit in Ihrem Browser l&ouml;schen Technikhinweise. Bitte beachten Sie jedoch, dass dieses Online-Angebot ohne Cookies m&ouml;glicherweise nicht oder nur noch eingeschr&auml;nkt funktioniert.</p>
        <p>Bitte beachten Sie weiterhin, dass Widerspr&uuml;che gegen die Erstellung von Nutzungsprofilen teilweise &uuml;ber einen sogenannten &quot;Opt-Out-Cookie&quot; funktionieren. Sollten Sie alle Cookies l&ouml;schen, findet ein Widerspruch daher evtl. keine Ber&uuml;cksichtigung mehr und muss von Ihnen erneut erhoben werden.</p>

        <p><b>Was f&uuml;r Cookies verwenden wir?</b></p>
        <p>Erforderliche Cookies</p>
        <p>
            Erforderliche Cookies helfen dabei, eine Website nutzbar zu machen, indem grundlegende Funktionen wie die Seitennavigation und der Zugriff auf sichere Bereiche der Website aktiviert werden. Die Website kann ohne diese Cookies nicht richtig funktionieren. In diese Kategorie fallen z. B. Cookies, die der Identifizierung bzw. Authentifizierung unserer Nutzer dienen.
        </p>

        <p>Präferenz-Cookies</p>
        <p>Präferenz-Cookies ermöglichen es einer Website, Informationen zu speichern, die das Verhalten oder Aussehen der Website anpassen, wie z.B. bevorzugte Sprache.</p>

        <p>Analyse-Cookies</p>
        <p>Mithilfe von Analyse-Cookies können Websitebesitzer die Interaktion von Besuchern mit Websites besser verstehen, indem sie Informationen anonym sammeln und melden.</p>

        <p>Chat-Cookies</p>
        <p>Diese Cookies werden verwendet, um festzustellen, ob Sie uns erlauben, unseren Chat-Dienst anzuzeigen oder nicht, und werden auch von unserem Chat-Dienstanbieter gesetzt. Unser Chat-Dienstleister ist Zopim. Wir verwenden das Chat-System, um Ihnen zu helfen, unser System besser zu nutzen. Diese Cookies sammeln keine persönlichen Daten.</p>

        <p>Tracking Cookies im Zusammenhang mit Social Plugins</p>
        <p>Die Einbindung von Social Plugins f&uuml;hrt regelm&auml;&szlig;ig dazu, dass die Anbieter der Plugins Cookies speichern, mit deren Hilfe sie Nutzungsprofile der Nutzer unseres Online-Angebots erstellen k&ouml;nnen.</p>        
        <p><b>Google Maps</b></p>
        <p>Diese Webseite verwendet Google Maps von Google Inc. zur Darstellung von interaktiven Karten. Google Maps ist ein Kartendienst von Google Inc., 1600 Amphitheatre Parkway, Mountain View, California 94043, USA. Durch Nutzung dieser Webseite erkl&auml;ren Sie sich mit der Erfassung, Bearbeitung sowie Nutzung der automatisiert erhobenen Daten durch Google Inc, deren Vertreter sowie Dritter einverstanden.</p>
        <p>Zweck und Umfang der Datenerhebung und die weitere Verarbeitung und Nutzung der Daten durch Google sowie Ihre diesbez&uuml;glichen Rechte und Einstellungsm&ouml;glichkeiten zum Schutz Ihrer Privatsph&auml;re entnehmen Sie bitte den Nutzungshinweisen von Google unter <a  href="https://www.google.com/url?q=https://www.google.com/intl/de_de/help/terms_maps.html&amp;sa=D&amp;ust=1552865124438000">https://www.google.com/intl/de_de/help/terms_maps.html</a>.</p>

        <p><b>10. Web-Analyse</b></p>
        <p>Wir ben&ouml;tigen statistische Informationen &uuml;ber die Nutzung unseres Online-Angebots um es nutzerfreundlicher gestalten, Reichweitenmessungen vornehmen und Marktforschung betreiben zu k&ouml;nnen.</p>
        <p>Zu diesem Zweck setzen wir die nachstehenden Analyse-Tools ein. Die von den Tools mithilfe von Analyse-Cookies oder durch Auswertung der Log-Dateien erstellten Nutzungsprofile werden nicht mit personenbezogenen Daten zusammengef&uuml;hrt.</p>
        <p>Die Anbieter der Tools verarbeiten Daten nur als Auftragsverarbeiter gem&auml;&szlig; unseren Weisungen und nicht zu eigenen Zwecken.</p>
        <p>Die Tools verwenden IP-Adressen der Nutzer entweder &uuml;berhaupt nicht oder k&uuml;rzen diese sofort nach der Erhebung.</p>
        <p>Sie finden zu jedem Tool Informationen zum jeweiligen Anbieter und dazu, wie Sie der Erhebung und Verarbeitung von Daten durch das Tool widersprechen k&ouml;nnen.</p>
        <p>Bei Tools, die den Widerspruch mittels Opt-Out-Cookies umsetzen ist zu beachten, dass die Opt-Out Funktion ger&auml;te- bzw. browserbezogen ist und grunds&auml;tzlich nur f&uuml;r das gerade verwendete Endger&auml;t bzw. den verwendeten Browser gilt. Wenn Sie mehrere Endger&auml;te bzw. Browser nutzen, m&uuml;ssen Sie das Opt-Out auf jedem einzelnen Endger&auml;t und in jedem verwendeten Browser setzen.</p>
        <p>Dar&uuml;ber hinaus k&ouml;nnen Sie die Bildung von Nutzungsprofilen zu Analysezwecken auch insgesamt verhindern, indem Sie generell die Verwendung von Cookies verhindern.</p>
        <p>Google Analytics</p>
        <p>Google Analytics wird von Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA (&quot;Google&quot;) bereitgestellt. Wir nutzen Google Analytics mit der von Google angebotenen Zusatzfunktion zur Anonymisierung von IP-Adressen: Hierbei wird die IP-Adresse von Google in der Regel bereits innerhalb der EU und nur in Ausnahmef&auml;llen erst in den USA gek&uuml;rzt und in jedem Fall nur in gek&uuml;rzter Form gespeichert.</p>
        <p>Sie k&ouml;nnen der Erhebung bzw. Auswertung Ihrer Daten durch dieses Tool widersprechen, indem Sie das unter dem folgenden Link verf&uuml;gbare Browser Plugin herunterladen und installieren: <a  href="https://www.google.com/url?q=http://tools.google.com/dlpage/gaoptout?hl%3Dde&amp;sa=D&amp;ust=1552865124439000">http://tools.google.com/dlpage/gaoptout?hl=de</a>.</p>
        <p><a  href="https://www.google.com/url?q=https://optout.ioam.de/&amp;sa=D&amp;ust=1552865124440000"></a></p>

        <p><b>11. Erfassung und Auswertung von Nutzungsverhalten f&uuml;r interessenbasierte Werbung (auch durch Dritte)</b></p>
        <p>Erfassung und Auswertung von Nutzungsverhalten f&uuml;r interessenbasierte Werbung (auch durch Dritte)</p>
        <p>Wir m&ouml;chten unseren Nutzern in diesem Online-Angebot auf ihre Interessen zugeschnittene Werbung bzw. besondere Angebote pr&auml;sentieren (&quot;interessenbasierte Werbung&quot;) und die H&auml;ufigkeit der Anzeige bestimmter Werbung begrenzen. Zu diesem Zweck setzen wir die nachstehend beschriebenen Tools ein.</p>
        <p>Die von den Tools mithilfe von Werbe-Cookies bzw. Werbe-Cookies Dritter, sogenannten Web Beacons (unsichtbare Grafiken, die auch Pixel oder Z&auml;hlpixel genannt werden) oder vergleichbaren Technologien erstellten Nutzungsprofile werden nicht mit personenbezogenen Daten zusammengef&uuml;hrt. Die Tools verarbeiten IP-Adressen der Nutzer entweder &uuml;berhaupt nicht oder k&uuml;rzen diese sofort nach der Erhebung.</p>
        <p>Verantwortlicher f&uuml;r die Verarbeitung von Daten im Zusammenhang mit den Tools ist der jeweilige Anbieter, soweit wir dies nicht anders angegeben haben. Die Anbieter der Tools geben Informationen zu den vorstehend benannten Zwecken ggf. auch an Dritte weiter.</p>
        <p>Sie finden bei jedem Tool Informationen zum Anbieter und dazu, wie Sie der Erhebung von Daten mittels dieses Tools widersprechen k&ouml;nnen. Bitte beachten Sie, dass Sie mit Ihrem Widerspruch nicht die Werbung abschalten. Ihr Widerspruch f&uuml;hrt nur dazu, Ihnen keine auf Ihr Nutzungsverhalten gest&uuml;tzte interessenbasierte Werbung angezeigt werden kann.</p>
        <p>Bei Tools, die mit Opt-Out-Cookies arbeiten, ist zu beachten, dass die Opt-Out Funktion ger&auml;te- bzw. browserbezogen ist und grunds&auml;tzlich nur f&uuml;r das gerade verwendete Endger&auml;t bzw. den verwendeten Browser gilt. Wenn Sie mehrere Endger&auml;te bzw. Browser nutzen, m&uuml;ssen Sie das Opt-Out auf jedem einzelnen Endger&auml;t und in jedem verwendeten Browser setzen.</p>
        <p>Dar&uuml;ber hinaus k&ouml;nnen Sie die Bildung von Nutzungsprofilen auch insgesamt verhindern, indem Sie generell die Verwendung von Cookies verhindern.</p>
        <p>Weitere Informationen zu interessenbasierter Werbung sind auf dem Verbraucherportal http://www.meine-cookies.org erh&auml;ltlich. &uuml;ber nachstehenden Link zu diesem Portal k&ouml;nnen Sie dar&uuml;ber hinaus den Stand der Aktivierung bez&uuml;glich Tools unterschiedlicher Anbieter einsehen und der Erhebung bzw. Auswertung Ihrer Daten durch diese Tools widersprechen: <a  href="https://www.google.com/url?q=http://www.meine-cookies.org/cookies_verwalten/praeferenzmanager-beta.html&amp;sa=D&amp;ust=1552865124441000">http://www.meine-cookies.org/cookies_verwalten/praeferenzmanager-beta.html.</a></p>
        <p><a  href="https://www.google.com/url?q=http://www.meine-cookies.org/cookies_verwalten/praeferenzmanager-beta.html&amp;sa=D&amp;ust=1552865124442000">Eine zentrale Widerspruchsm&ouml;glichkeit f&uuml;r verschiedene Tools insbesondere US-amerikanischer Anbieter ist auch unter folgendem Link erreichbar: </a><a  href="https://www.google.com/url?q=http://optout.networkadvertising.org/%23/&amp;sa=D&amp;ust=1552865124442000">http://optout.networkadvertising.org/#/</a></p>
        <p></p>
        <p>Google AdSense und Google Dynamic Remarketing</p>
        <p>Diese Website benutzt die Dienste Google AdSense und Google Remarketing zur Schaltung von Werbeanzeigen. Auch diese Dienste verwenden Cookies und Web Beacons um eine Analyse des Besucherverkehrs und der Benutzung der Website zu erm&ouml;glichen. Dadurch erm&ouml;glicht Google, dass kontextabh&auml;ngige Werbeanzeigen Dritter, basierend etwa auf Suchbegriffen in Suchmaschinen oder Schl&uuml;sselbegriffe des Webseiteninhalts, auf unseren Online-Angeboten platziert werden. Anbieter ist Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA (&quot;Google&quot;).</p>
        <p>Die durch Cookies und Web Beacons erzeugten Informationen &uuml;ber die Benutzung dieser Website (einschlie&szlig;lich Ihrer IP-Adresse) und Auslieferung von Werbeformaten werden an einen Server von Google in den USA &uuml;bertragen und dort gespeichert. Diese Informationen k&ouml;nnen von Google an Vertragspartner von Google weitergegeben werden. Google wird Ihre IP-Adresse jedoch nicht mit anderen von Ihnen gespeicherten Daten zusammenf&uuml;hren.</p>
        <p>Sie k&ouml;nnen die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht s&auml;mtliche Funktionen dieser Website voll umf&auml;nglich nutzen k&ouml;nnen. Durch die Nutzung dieser Website erkl&auml;ren Sie sich mit der Bearbeitung der &uuml;ber Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.</p>
        <p>Sie k&ouml;nnen zu jeder Zeit die Verwendung von Cookies f&uuml;r Werbezwecke durch Google auf <a  href="https://www.google.com/url?q=http://www.google.com/policies/technologies/ads/&amp;sa=D&amp;ust=1552865124443000">http://www.google.com/policies/technologies/ads/</a>&nbsp;deaktivieren.</p>
        <p>Google Adwords</p>
        <p>Wir nutzen das Online-Werbeprogramm &quot;Google AdWords&quot; und im Rahmen von Google AdWords das Conversion-Tracking. Das Google Conversion Tracking ist ein Analysedienst der Google Inc. (1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; &quot;Google&quot;). Wenn Sie auf eine von Google geschaltete Anzeige klicken, wird ein Cookie f&uuml;r das Conversion-Tracking auf Ihrem Rechner abgelegt. Diese Cookies verlieren nach 30 Tagen ihre G&uuml;ltigkeit, enthalten keine personenbezogenen Daten und dienen somit nicht der pers&ouml;nlichen Identifizierung.</p>
        <p>Wenn Sie bestimmte Internetseiten unserer Website besuchen und das Cookie noch nicht abgelaufen ist, k&ouml;nnen Google und wir erkennen, dass Sie auf die Anzeige geklickt haben und zu dieser Seite weitergeleitet wurden. Jeder Google AdWords-Kunde erh&auml;lt ein anderes Cookie. Somit besteht keine M&ouml;glichkeit, dass Cookies &uuml;ber die Websites von AdWords-Kunden nachverfolgt werden k&ouml;nnen.</p>
        <p>Die Informationen, die mithilfe des Conversion-Cookie eingeholten werden, dienen dazu, Conversion-Statistiken f&uuml;r AdWords-Kunden zu erstellen, die sich f&uuml;r Conversion-Tracking entschieden haben. Hierbei erfahren die Kunden die Gesamtanzahl der Nutzer, die auf ihre Anzeige geklickt haben und zu einer mit einem Conversion-Tracking-Tag versehenen Seite weitergeleitet wurden. Sie erhalten jedoch keine Informationen, mit denen sich Nutzer pers&ouml;nlich identifizieren lassen.</p>
        <p>Wenn Sie nicht am Tracking teilnehmen m&ouml;chten, k&ouml;nnen Sie dieser Nutzung widersprechen, indem Sie die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern (Deaktivierungsm&ouml;glichkeit). Sie werden sodann nicht in die Conversion-Tracking Statistiken aufgenommen. Weiterf&uuml;hrende Informationen sowie die Datenschutzerkl&auml;rung von Google finden Sie unter: <a  href="https://www.google.com/url?q=http://www.google.com/policies/technologies/ads/&amp;sa=D&amp;ust=1552865124443000">http://www.google.com/policies/technologies/ads/</a>, <a  href="https://www.google.com/url?q=http://www.google.de/policies/privacy/&amp;sa=D&amp;ust=1552865124444000">http://www.google.de/policies/privacy/</a></p>
        <p>Google personalisierte Werbung </p>
        <p>Um personalisierte Werbung von Google zu deaktivieren befolgen Sie diese Schritte: <a  href="https://www.google.com/url?q=https://support.google.com/ads/answer/2662922?hl%3Dde&amp;sa=D&amp;ust=1552865124444000">https://support.google.com/ads/answer/2662922?hl=de</a></p>        

        <p><b>12. Adblocker und Opt-Out-Cookies</b></p>
        <p>Wir weisen darauf hin, dass die Verwendung eines Adblocker die Funktionsweise von Opt-Out-Cookies beeintr&auml;chtigen kann. In bestimmten F&auml;llen kann es daher, trotz Speicherung eines Opt-Out-Cookies, dazu kommen, dass die entsprechenden Tools weiterhin Daten erheben. Sie k&ouml;nnen in diesem Fall die Funktionalit&auml;t wiederherstellen, indem Sie den Adblocker entsprechend konfigurieren oder deinstallieren.</p>

        <p><b>13. Social Plugins</b></p>
        <p>Dieses Online-Angebot verwendet Social Plugins (&quot;Plugins&quot;) folgender Anbieter:</p>
        <ul>
            <li>Plugins des sozialen Netzwerks Facebook; Facebook wird unter www.facebook.com von Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA, und unter www.facebook.de von Facebook Ireland Limited, Hanover Reach, 5-7 Hanover Quay, Dublin 2, Irland, betrieben (&quot;Facebook&quot;). </li>
            <li>Plugins von Twitter; Twitter wird betrieben von der Twitter Inc., 1355 Market St, Suite 900, San Francisco, CA 94103, USA (&quot;Twitter&quot;).</li>          
        </ul>
        <p>Die verschiedenen Anbieter von Plugins werden im Folgenden unter dem Begriff &quot;Plugin-Anbieter&quot; zusammengefasst. Auf unserer Internetpräsenz kommen nachfolgend benannte Social-Plug-ins zum Einsatz, um Ihnen die Möglichkeit zu geben, mit den sozialen Netzwerken und anderen Nutzern zu interagieren und so die Nutzung unserer Internetpräsenz für Sie interessanter zu gestalten und regelmäßig verbessern zu können. Ferner wird mit dem Einsatz der Social-Plug-ins der Zweck verfolgt, unsere Bekanntheit zu steigern. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. f DS-GVO, wobei sich unser berechtigtes Interesse aus vorbenannten Zwecken ergibt.</p>

        <p>
            Wir bieten Ihnen auf unserer Webseite die Möglichkeit der Nutzung von sog. „Social-Media-Buttons“ an. Es werden jedoch durch die Sharing-Plugins direkte Verbindungen zwischen Ihrem Browser und dem jeweiligen Netzwerk hergestellt. Der Besuch unserer Seiten kann dadurch von Ihrem Netzwerk Ihrem Benutzerkonto zugeordnet werden. Zum Schutze Ihrer Daten setzen wir bei der Implementierung auf die Lösung „Shariff“. Hierdurch werden diese Buttons auf der Webseite lediglich als Grafik eingebunden, die eine Verlinkung auf die entsprechende Webseite des Button-Anbieters enthält. Durch Anklicken der Grafik werden Sie somit zu den Diensten der jeweiligen Anbieter weitergeleitet. Erst dann werden Ihre Daten an die jeweiligen Anbieter gesendet. Die Plugins stellen selbständige Erweiterungen der Plugin-Anbieter dar. Wir haben daher keinen Einfluss auf den Umfang der von den Plugin-Anbietern über die Plugins erhobenen und gespeicherten Daten. Sofern Sie die Grafik nicht anklicken, findet keinerlei Austausch zwischen Ihnen und den Anbietern der Social-Media-Buttons statt. Wenn Sie nicht möchten, dass die Plugin-Anbieter die über dieses Online-Angebot erhobenen Daten erhalten und ggf. speichern bzw. weiterverwenden, sollten Sie die jeweiligen Plugins nicht verwenden. Weitere Informationen über Zweck und Umfang der Erhebung sowie die weitere Verarbeitung und Nutzung Ihrer Daten durch Plugin-Anbieter sowie zu Ihren Rechten und Einstellungsmöglichkeiten zum Schutz Ihrer Daten finden Sie in den Datenschutzhinweisen der jeweiligen Anbieter. 
        </p>

        <p>
            Mehr Informationen zur Shariff-Lösung finden Sie hier: <a target="blank" href="http://www.heise.de/ct/artikel/Shariff-Social-Media-Buttons-mit-Datenschutz-2467514.html">http://www.heise.de/ct/artikel/Shariff-Social-Media-Buttons-mit-Datenschutz-2467514.html</a>
        </p>

        <p>
            Zu den weiteren Informationen der von uns genannten Sozialen Netzwerke lesen Sie bitte deren Datenschutzerklärungen:
        </p>

        <p>
            Eine Übersicht über die Plugins von Facebook und deren Aussehen finden Sie hier: <a target="blank" href="http://developers.facebook.com/docs/plugins">http://developers.facebook.com/docs/plugins</a>. 
            <br>
            Informationen zum Datenschutz bei Facebook finden Sie hier: <a target="blank" href="http://www.facebook.com/policy.php">http://www.facebook.com/policy.php</a>.
        </p>

        <p>
            Eine Übersicht über die Plugins von Twitter und deren Aussehen finden Sie hier: <a target="blank" href="https://twitter.com/about/resources/buttons">https://twitter.com/about/resources/buttons</a>
            <br>
            Informationen zum Datenschutz bei Twitter finden Sie hier: <a target="blank" href="https://twitter.com/privacy">https://twitter.com/privacy</a>.
        </p>


        <p><b>14. Hotjar</b></p>
        <p>
            Wir nutzen <a href="http://www.hotjar.com">Hotjar</a>, um die Bedürfnisse unserer Nutzer besser zu verstehen und das Angebot auf dieser Website zu optimieren. &nbsp;Mithilfe der Technologie von Hotjar bekommen wir ein besseres Verständnis von den Erfahrungen unserer Nutzer (z.B. wieviel Zeit Nutzer auf welchen Seiten verbringen, welche Links sie anklicken, was sie mögen und was nicht etc.) und das hilft uns, unser Angebot am Feedback unserer Nutzer auszurichten. Hotjar arbeitet mit Cookies und anderen Technologien, um Informationen über das Verhalten unserer Nutzer und über ihre Endgeräte zu sammeln (insbesondere IP Adresse des Geräts (wird nur in anonymisierter Form erfasst und gespeichert), Bildschirmgröße, Gerätetyp (Unique Device Identifiers), Informationen &nbsp;über den verwendeten Browser, Standort (nur Land), zum Anzeigen unserer Website bevorzugte Sprache). Hotjar speichert diese Informationen in einem pseudonymisierten Nutzerprofil.&nbsp; Die Informationen werden weder von Hotjar noch von uns zur Identifizierung einzelner Nutzer verwendet oder mit weiteren Daten über einzelne Nutzer zusammengeführt. Rechtsgrundlage ist Art. 6 abs. 1 S. 1 lit. f DSGVO. Weitere Informationen finden Sie in Hotjars Datenschutzerklärung: <a href="https://www.hotjar.com/legal/policies/privacy">https://www.hotjar.com/legal/policies/privacy</a> </p>

        <p> Sie können der Speicherung eines Nutzerprofils und von Informationen über Ihren Besuch auf unserer Website durch Hotjar sowie dem Setzen von Hotjar Tracking Cookies auf anderen Websites über diesen Link : <a href="https://www.hotjar.com/legal/compliance/opt-out">https://www.hotjar.com/legal/compliance/opt-out</a></p>

        <p><b>15. Personalisierte/individuelle Produktempfehlung</b></p>
        <p>Wir erfassen Ger&auml;te- und Zugriffsdaten, die bei Ihrer Interaktion mit einem Newsletter anfallen. F&uuml;r diese Auswertung beinhalten die Newsletter Verweise auf Bilddateien, die auf unserem Webserver gespeichert sind. Wenn Sie einen Newsletter &ouml;ffnen, l&auml;dt Ihr E-Mail-Programm diese Bilddateien von unserem Webserver. Wir erfassen die dabei anfallenden Ger&auml;te- und Zugriffsdaten in pseudonymisierter Form unter einer zuf&auml;llig generierten Kennnummer (Newsletter-ID), die wir ohne Ihre Einwilligung nicht zur Ihrer Identifikation verwenden. So k&ouml;nnen wir nachvollziehen, ob und wann Sie welche Ausgaben eines Newsletters ge&ouml;ffnet haben. Die in den Newslettern enthaltenen Links enthalten ebenfalls Ihre Newsletter-ID, damit wir erfassen k&ouml;nnen, welche Inhalte Sie interessieren. Mit den so gewonnenen Daten erstellen wir ein Nutzerprofil zu Ihrer Newsletter-ID, um Newsletter-Inhalte entsprechend Ihren Interessen und Nutzungsgewohnheiten zu personalisieren und statistisch analysieren zu k&ouml;nnen, wie unsere Nutzer den Newsletter-Service verwenden. Diese Daten verkn&uuml;pfen wir mit den Daten, die wir im Rahmen der Nutzungsanalyse erfassen.</p>
        <p>Auf Grundlage Ihrer Interessen erhalten Sie von uns individuelle Produktempfehlungen per E-Mail oder Post, die wir aus Ihren bisherigen Einkaufsdaten und Interessensdaten ableiten. Wenn Sie von uns keine individuellen Produktempfehlungen mehr erhalten wollen, k&ouml;nnen Sie jederzeit widersprechen. Informationen hierzu finden Sie in Abschnitt: Ihr Recht auf Widerruf der Einwilligung.</p>

        <p><b>16. Einsatz von Zopim</b></p>
        <p>Wir verwenden Zopim, eine Live-Chat-Funktion der Zopim Technologies Pte Ltd, 1 Commonwealth Lane, 03-01, Singapore 149544 („Zopim“). Über die auf der Website bereitgestellte Live-Chat-Funktion können Sie in Echtzeit mit uns in Verbindung treten. Wir erheben, verarbeiten und nutzen die von Ihnen über die Live-Chat-Funktion mitgeteilten Informationen ausschliesslich für die Bearbeitung Ihres konkreten Anliegens. Die von Ihnen mitgeteilten Informationen werden in der Regel an einen Server von Zopim ausserhalb der EU übertragen und dort gespeichert. Zopim verwendet ausserdem Cookies, die eine Bereitstellung und Analyse der Benutzung der Live-Chat-Funktion durch Sie ermöglichen. Mithilfe der Cookies werden Ihre Besuche sowie anonymisierte Daten über die Nutzung der Website erfasst. Personenbezogene Daten werden dabei nicht gespeichert. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Zopim ausserhalb der EU übertragen und dort gespeichert. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen können. </p>
        <p>
            Weitere Informationen finden Sie unter der Zopim Datenschutzerklärung: <a href="https://www.zendesk.de/company/customers-partners/privacy-policy">
                https://www.zendesk.de/company/customers-partners/privacy-policy
            </a> </p>

        <p><b>17. Ihre Rechte (Rechte der betroffenen Person)</b></p>
        <p><b>Wie k&ouml;nnen Sie Ihre Rechte geltend machen?</b></p>
        <p>Bitte nutzen Sie zur Geltendmachung Ihrer Rechte die Angaben im Abschnitt Kontakt. Bitte stellen Sie dabei sicher, dass uns eine eindeutige Identifizierung Ihrer Person m&ouml;glich ist.</p>
        <p>Alternativ k&ouml;nnen Sie zur Berichtigung Ihrer bei der Registrierung angegebenen Daten oder f&uuml;r Ihren Werbewiderspruch auch die Einstellungsm&ouml;glichkeiten in Ihrem Benutzerkonto verwenden.</p>
        <p>Bitte beachten Sie, dass Ihre Daten zun&auml;chst nur gesperrt werden, sofern der L&ouml;schung Aufbewahrungsfristen entgegenstehen.</p>
        <p><b>Ihre Rechte auf Auskunft und Berichtigung</b></p>
        <p>Sie k&ouml;nnen verlangen, dass wir Ihnen best&auml;tigen, ob wir Sie betreffende personenbezogene Daten verarbeiten und Sie haben ein Recht auf Auskunft im Hinblick auf Ihre von uns verarbeiteten Daten. Sollten Ihre Daten unrichtig oder unvollst&auml;ndig sein, k&ouml;nnen Sie verlangen, dass Ihre Daten berichtigt bzw. vervollst&auml;ndigt werden. Wenn wir Ihre Daten an Dritte weitergegeben haben informieren wir diese &uuml;ber die Berichtigung, soweit dies gesetzlich vorgeschrieben ist.</p>
        <p><b>Ihr Recht auf L&ouml;schung</b></p>
        <p>Sie k&ouml;nnen, wenn die gesetzlichen Voraussetzungen vorliegen, von uns unverz&uuml;gliche L&ouml;schung Ihrer personenbezogenen Daten verlangen. Dies ist insbesondere der Fall wenn</p>
        <ul>
            <li>Ihre personenbezogenen Daten f&uuml;r die Zwecke, f&uuml;r die sie erhoben wurden, nicht l&auml;nger ben&ouml;tigt werden;</li>
            <li>die Rechtsgrundlage f&uuml;r die Verarbeitung ausschlie&szlig;lich Ihre Einwilligung war und Sie diese widerrufen haben;</li>
            <li>Sie der Verarbeitung zu werblichen Zwecken widersprochen haben (&quot;Werbewiderspruch&quot;);</li>
            <li>Sie einer Verarbeitung auf der Basis der Rechtsgrundlage Interessenabw&auml;gung aus pers&ouml;nlichen Gr&uuml;nden widersprochen haben und wir nicht nachweisen k&ouml;nnen, dass es vorrangige berechtigte Gr&uuml;nde f&uuml;r eine Verarbeitung gibt;</li>
            <li>Ihre personenbezogenen Daten unrechtm&auml;&szlig;ig verarbeitet wurden; oder</li>
            <li>Ihre personenbezogenen Daten gel&ouml;scht werden m&uuml;ssen, um gesetzlichen Anforderungen zu entsprechen.</li>
        </ul>
        <p>Wenn wir Ihre Daten an Dritte weitergegeben haben informieren wir diese &uuml;ber die L&ouml;schung, soweit dies gesetzlich vorgeschrieben ist.</p>
        <p>Bitte beachten Sie, dass Ihr L&ouml;schungsrecht Einschr&auml;nkungen unterliegt. Zum Beispiel m&uuml;ssen bzw. d&uuml;rfen wir keine Daten l&ouml;schen, die wir aufgrund gesetzlicher Aufbewahrungsfristen noch weiter vorhalten m&uuml;ssen. Auch Daten, die wir zur Geltendmachung, Aus&uuml;bung oder Verteidigung von Rechtsanspr&uuml;chen ben&ouml;tigen sind von Ihrem L&ouml;schungsrecht ausgenommen.</p>
        <p><b>Ihr Recht auf Einschr&auml;nkung der Verarbeitung</b></p>
        <p>Sie k&ouml;nnen, wenn die gesetzlichen Voraussetzungen vorliegen, von uns eine Einschr&auml;nkung der Verarbeitung verlangen. Dies ist insbesondere der Fall wenn</p>
        <ul>
            <li>die Richtigkeit Ihrer personenbezogenen Daten von Ihnen bestritten wird, und dann solange bis wir die M&ouml;glichkeit hatten, die Richtigkeit zu &uuml;berpr&uuml;fen;</li>
            <li>die Verarbeitung nicht rechtm&auml;&szlig;ig erfolgt und Sie statt der L&ouml;schung (siehe hierzu den vorigen Abschnitt) eine Einschr&auml;nkung der Nutzung verlangen;</li>
            <li>wir Ihre Daten nicht mehr f&uuml;r die Zwecke der Verarbeitung ben&ouml;tigen, Sie diese jedoch zur Geltendmachung, Aus&uuml;bung oder Verteidigung Ihrer Rechtsanspr&uuml;che brauchen;</li>
            <li>Sie Widerspruch aus pers&ouml;nlichen Gr&uuml;nden erhoben haben, und dann solange bis feststeht, ob Ihre Interessen &uuml;berwiegen.</li>
        </ul>
        <p>Wenn ein Recht auf Einschr&auml;nkung der Verarbeitung besteht markieren wir die betroffenen Daten um auf diese Weise sicherzustellen, dass diese nur noch in den engen Grenzen verarbeitet werden, die f&uuml;r solche eingeschr&auml;nkten Daten gelten (n&auml;mlich insbesondere zur Verteidigung von Rechtsanspr&uuml;chen oder mit Ihrer Einwilligung).</p>
        <p><b>Ihr Recht auf Daten&uuml;bertragbarkeit</b></p>
        <p>Sie haben das Recht, personenbezogene Daten, die Sie uns zur Vertragserf&uuml;llung oder auf Basis einer Einwilligung gegeben haben, in einem &uuml;bertragbaren Format zu erhalten. Sie k&ouml;nnen in diesem Fall auch verlangen, dass wir diese Daten direkt einem Dritten &uuml;bermitteln, soweit dies technisch machbar ist.</p>       

        <p><b>Ihr Beschwerderecht bei einer Aufsichtsbeh&ouml;rde</b></p>
        <p>Sie haben das Recht, eine Beschwerde bei einer Datenschutzbeh&ouml;rde einzureichen. Sie k&ouml;nnen sich dazu insbesondere an die Datenschutzbeh&ouml;rde wenden, die f&uuml;r Ihren Wohnort bzw. Ihr Bundesland zust&auml;ndig ist oder die f&uuml;r den Ort zust&auml;ndig ist, an dem die Verletzung des Datenschutzrechts stattgefunden hat. Alternativ k&ouml;nnen Sie sich auch an die f&uuml;r uns zust&auml;ndige Datenschutzbeh&ouml;rde wenden, dies ist:</p>
        <p>Landesbeauftragte f&uuml;r Datenschutz und Informationsfreiheit</p>
        <p>Nordrhein-Westfalen</p>
        <p>Kavalleriestr. 2-4</p>
        <p>D-40213 D&uuml;sseldorf</p>
        <p>poststelle@ldi.nrw.de</p>

        <div style="padding: 10px; background-color: lightgrey; color:black;">


            <p><b>Ihr Recht auf Widerruf der Einwilligung</b></p>
            <p><b>
                    Sofern Sie uns eine Einwilligung in die Verarbeitung Ihrer Daten erteilt haben, können Sie diese jederzeit per E-Mail: dataprotection@bookmyhostess.com mit Wirkung für die Zukunft widerrufen. Die Rechtmäßigkeit der Verarbeitung Ihrer Daten bis zum Widerruf bleibt hiervon unberührt.</b>
            </p>
        </div>
        <div style="padding: 10px; background-color: lightgrey; color:black;">
            <p>
                <b>
                    Information über Ihr Widerspruchsrecht nach Art. 21 DS-GVO  
                </b>
            </p>
            <p>
                <b>
                    Wenn die Datenverarbeitung auf Grundlage von Art. 6 Abs. 1 lit. e oder f DSGVO erfolgt, haben Sie jederzeit das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, gegen die Verarbeitung Ihrer personenbezogenen Daten Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling. Die jeweilige Rechtsgrundlage, auf denen eine Verarbeitung beruht, entnehmen Sie dieser Datenschutzerklärung. Wenn Sie Widerspruch einlegen, werden wir Ihre betroffenen personenbezogenen Daten nicht mehr verarbeiten, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen (Widerspruch nach Art. 21 Abs. 1 DSGVO).
                    Werden Ihre personenbezogenen Daten verarbeitet, um Direktwerbung zu betreiben, so haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung Sie betreffender personenbezogener Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht. Wenn Sie widersprechen, werden Ihre personenbezogenen Daten anschließend nicht mehr zum Zwecke der Direktwerbung verwendet (Widerspruch nach Art. 21 Abs. 2 DSGVO). Möchten Sie von Ihrem Widerspruchsrecht Gebrauch machen, genügt eine E-Mail an dataprotection@bookmyhostess.
                </b>
            </p>
        </div>
        <br>
        <p>
            <b>
                Wenn Sie eine Berichtigung, Sperrung, Löschung oder Auskunft über die zu Ihrer Person gespeicherten personenbezogenen Daten wünschen oder Fragen bzgl. der Erhebung, Verarbeitung oder Verwendung Ihrer personenbezogenen Daten haben oder erteilte Einwilligungen widerrufen möchten, wenden Sie sich bitte an folgende E-Mail-Adresse: dataprotection@bookmyhostess.com
            </b>
        </p>

        <p><b>18. Kontakt</b></p>
        <p>F&uuml;r Ausk&uuml;nfte und Anregungen zum Thema Datenschutz stehen wir bzw. unser Datenschutzbeauftragter Ihnen unter der E-Mail dataprotection@bookmyhostess.com  gerne zur Verf&uuml;gung.</p>
        <p>Wenn Sie mit uns in Kontakt treten m&ouml;chten, erreichen Sie uns im &uuml;brigen wie folgt:</p>
        <p>BOOKmyHOSTESS.com, Ottostr. 7, 50859 K&ouml;ln</p>
        <p><b>19. Anhang: Technikhinweise</b></p>
        <p>Technikhinweise zum L&ouml;schen von Cookies</p>
        <p>Internet Explorer:</p>
        <p>Anleitung unter</p>
        <p><a  href="https://www.google.com/url?q=http://windows.microsoft.com/de-de/internet-explorer/delete-manage-cookies%23ie%3Die-11-win-7&amp;sa=D&amp;ust=1552865124456000">http://windows.microsoft.com/de-de/internet-explorer/delete-manage-cookies#ie=ie-11-win-7</a></p>
        <p>Mozilla Firefox:</p>
        <p>Anleitung unter</p>
        <p><a  href="https://www.google.com/url?q=https://support.mozilla.org/de/kb/cookies-erlauben-und-ablehnen&amp;sa=D&amp;ust=1552865124457000">https://support.mozilla.org/de/kb/cookies-erlauben-und-ablehnen</a></p>
        <p>Google Chrome:</p>
        <p>Anleitung unter</p>
        <p><a  href="https://www.google.com/url?q=https://support.google.com/chrome/answer/95647&amp;sa=D&amp;ust=1552865124457000">https://support.google.com/chrome/answer/95647</a></p>
        <p>Safari:</p>
        <p>Anleitung unter</p>
        <p><a  href="https://www.google.com/url?q=http://help.apple.com/safari/mac/8.0/%23/sfri11471&amp;sa=D&amp;ust=1552865124458000">http://help.apple.com/safari/mac/8.0/#/sfri11471</a></p>
    </div>
</div>
