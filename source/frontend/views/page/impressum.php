<?php
$this->title = Yii::t('impressum', 'IMPRESSUM');
?>


<div class="row">
    <div class="col-xs-4">
        <div class="green-line"></div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-title">
        <?= Yii::t('impressum', 'IMPRESSUM') ?>
    </div>
</div>




<div class="row">


    <div class="col-xs-12 text-justify">

        <p>
            <b><?= Yii::t('impressum', 'Imprint') ?>:</b>
        </p>
        <p>
            <?= Yii::t('impressum', 'Responsible for the contents in accordance with § 5 TMG:') ?>
        </p>
        <p>
            Adela Kadirić
        </p>
        <p>
            Ottostr. 7, 50859 Cologne, Germany
        </p>       
        <p>
            Tel.:  +492234 4301397
        </p>
        <p>
            Fax:  +492234 4301396
        </p>
        <p>
            Email: info@bookmyhostess.com
        </p>

        <br>
        <p>
            <b><?= Yii::t('impressum', 'Disclaimer') ?></b>
        </p>

        <p>
            <b><?= Yii::t('impressum', 'Liability for the content of websites') ?></b>
        </p>
        <p>
            <?= Yii::t('impressum', 'This website serves to provide information. The contents were compiled with great care. For the accuracy, completeness and timeliness of content, we can not take any responsibility. Any liability for any damages or consequences arising from the use of such content, are hereby excluded (disclaimer).') ?>
        </p>

        <p>
            <b><?= Yii::t('impressum', 'Liability for links to external providers') ?></b>
        </p>
        <p>
            <?= Yii::t('impressum', 'Our site contains links to external websites, over which we have no control. We hereby dissociate ourselves from all contents of all linked pages on our homepage. The provider or operator is always responsible for the content of linked pages. We can not take any responsibility for their content. Upon notification of violations, we will remove such links immediately.
To this end, the proof must be provided that a violation has occurred. Claims for damages, especially the cost scores of lawyers can not be accepted, provided that we have not been notified in advance in writing in an appropriate form of the injured in the right party.
We appreciate your help in order to prevent the publication of illegal content. Please report violations of the law by email at info@bookmyhostess.com, so we can remove the link to that page.') ?>
        </p>
        <p>
            <b><?= Yii::t('impressum', 'Copyright') ?></b>            
        </p>
        <p>
            <?= Yii::t('impressum', 'All rights reserved. This website and its contents are protected by all existing and applicable intellectual property laws in the Federal Republic of Germany. 
All displayed emblems and logos, images and graphics of other companies are subject to the copyright of the respective licensors. They may not be copied, made publicly available or used otherwise without the consent of the licensor.

All displayed pictures, logos, texts, reports, scripts and programming routines, which are our developments or were rehashed by us, may not be copied, made publicly available or used otherwise without our consent.

Framing of this website is subject to the written consent of the licensor.') ?>
        </p>
    </div>
</div>
