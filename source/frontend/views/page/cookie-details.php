<?php
$this->title = Yii::t('app.cookie', 'Cookie Details');
?>

<input id="cookieDetailsPage" value = "1" type="hidden" name="cookieDetailsPage">

<div class="row">
    <div class="col-xs-4">
        <div class="green-line"></div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-title">
        <?= Yii::t('app.cookie', 'Cookie Details') ?>
    </div>
</div>


<div class="row">

    <div class="col-xs-12 text-justify">
        <p>
            <?= Yii::t('app.cookie', 'We use cookies that are necessary to enable you to use our website. We also use cookies (also from third-party providers) for statistical and analysis purposes as well as for marketing and personalizing our content and advertising, in order to evaluate the effectiveness of our advertising campaigns and to be able to offer you advertising beyond this page that is tailored to your interests and Corresponds to your surfing behavior. You can allow the use of all cookies by clicking "All agree". By clicking "Accept all", you voluntarily consent to the use of all cookies (revocable at any time). This also includes, for a limited time, your consent to data processing outside the EEA, such as in the USA (Art. 49 Para. 1 lit. a) GDPR), where the high European level of data protection does not exist, so that the data can be accessed by authorities to control - and may be subject to monitoring purposes against which neither effective legal remedies nor rights of data subjects can be enforced. Since we respect your right to data protection, you can decide not to allow certain types of cookies and to save your individual selection of cookies by clicking "Accept selection". Your consent is voluntary and can be withdrawn at any time. You can find more information about the cookies we use and how to exercise your right of withdrawal in our <a href="https://www.bookmyhostess.com/page/privacy">data protection declaration</a>. Information on the publisher of the page can be found in the <a href="https://www.bookmyhostess.com/page/impressum">imprint</a>. ') ?>
        </p>
        <p><?= Yii::t('app.cookie', 'You can decide which categories do you allow:') ?></p>
        <p >
            <input type="checkbox" disabled checked name="general" class="js-general-cookie"> <b><?= Yii::t('app.cookie', 'Necessary') ?></b>
            <br>
            <input type="checkbox" name="preferences" value="0" class="js-preferences-cookie"> <b><?= Yii::t('app.cookie', 'Preferences') ?></b>
            <br>
            <input type="checkbox" name="ga" value="0" class="js-ga-cookie"> <b><?= Yii::t('app.cookie', 'Analyse') ?></b>
            <br>
            <input type="checkbox" name="zopim" value="0" class="js-zopim-cookie"> <b><?= Yii::t('app.cookie', 'Chat') ?>

    </div>
    <div class="col-xs-12 text-right">
        <button id="cookie-modal-accept-all" type="button" class="btn btn-red js-btn-cookie-accept-all">
            <?= Yii::t('app.cookie', 'Accept all') ?>
        </button>
        <button id="cookie-modal-accept-selected" type="button" class="btn btn-default js-btn-cookie-accept-selected">
            <?= Yii::t('app.cookie', 'Accept selection') ?>
        </button>
    </div>
    <br>
    <div class="col-xs-12 margin-top-20">
        <p>
            <?= Yii::t('app.cookie', 'We and our partners are setting following cookies:') ?>
        </p>
    </div>
    <div class="col-xs-12 margin-top-10">
        <table class="table table-responsive table-striped table-bordered" style="font-weight: normal">
            <thead style="font-weight: bold">
                <tr>
                    <td>
                        <?= Yii::t('app.cookie', 'Cookie Name') ?>                     
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Host') ?>                     
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Category') ?> 
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Duration') ?> 
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Type') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Description') ?> 
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <b>PHPSESSID</b>
                    </td>
                    <td>
                        www.bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Necessary') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Session') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'PHP session cookie associated with embedded content from this domain.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>_csrf</b>
                    </td>
                    <td>
                        www.bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Necessary') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Session') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Provides protection against Cross-Site Request Forgery.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>cookieAgree</b>
                    </td>
                    <td>
                        www.bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Necessary') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Session') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Stores information if user agreed or not to cookie policy.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>anCookieAgree</b>
                    </td>
                    <td>
                        www.bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Necessary') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Session') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Stores information if user allows to save analytics services such as Google analytics or Hotjar.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>prefCookieAgree</b>
                    </td>
                    <td>
                        www.bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Necessary') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Session') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Stores information if user allows to save preferences cookies such as selected language.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>zopimCookieAgree</b>
                    </td>
                    <td>
                        www.bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Necessary') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Session') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Stores information if user allows to show Chat service.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>lang</b>
                    </td>
                    <td>
                        www.bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Preferences') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '3 Months') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Stores selected application language.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>_ga</b>
                    </td>
                    <td>
                        .bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Analyse') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '2 Years') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Contains a randomly generated user ID. Using this ID, Google Analytics can recognize returning users on this website and merge the data from previous visits. More about cookies from Google you can read here: <a href="https://policies.google.com/privacy">Google privacy policies</a>.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>_gid</b>
                    </td>
                    <td>
                        .bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Analyse') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '24 Hours') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Contains a randomly generated user ID. Using this ID, Google Analytics can recognize returning users on this website and merge the data from previous visits. More about cookies from Google you can read here: <a href="https://policies.google.com/privacy">Google privacy policies</a>.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>_gat</b>
                    </td>
                    <td>
                        .bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Analyse') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1 Minute') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1st Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Certain data is only sent to Google Analytics a maximum of once per minute. The cookie has a lifespan of one minute. As long as it is set, certain data transfers are prevented. More about cookies from Google you can read here: <a href="https://policies.google.com/privacy">Google privacy policies</a>.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>_hjid</b>
                    </td>
                    <td>
                        .bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Analyse') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1  Year') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '3rd Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Hotjar cookie that is set when the customer first lands on a page with the Hotjar script. It is used to persist the Hotjar User ID, unique to that site on the browser. This ensures that behavior in subsequent visits to the same site will be attributed to the same user ID. More about this cookie you can read here: <a href="https://help.hotjar.com/hc/en-us/articles/115011789248-Hotjar-Cookie-Information"> Hotjar cookie information</a>.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>ajs_anonymous_id</b>
                    </td>
                    <td>
                        .bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Analyse') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1  Year') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '3rd Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'This cookie is set by Segment as a randomly generated ID for anonymous visitors. More about this cookie you can read here: <a href="https://help.hotjar.com/hc/en-us/articles/115011789248-Hotjar-Cookie-Information"> Hotjar cookie information</a>.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>_hjTLDTest</b>
                    </td>
                    <td>
                        .bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Analyse') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Session') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '3rd Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'When the Hotjar script executes we try to determine the most generic cookie path we should use, instead of the page hostname. This is done so that cookies can be shared across subdomains (where applicable). To determine this, we try to store the _hjTLDTest cookie for different URL substring alternatives until it fails. After this check, the cookie is removed. More about this cookie you can read here: <a href="https://help.hotjar.com/hc/en-us/articles/115011789248-Hotjar-Cookie-Information"> Hotjar cookie information</a>.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>__zlcmid</b>
                    </td>
                    <td>
                        .bookmyhostess.com
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Chat') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '1  Year') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', '3rd Party') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Store unique user ID. More about Zopim cookies you can read here: <a href="https://support.zendesk.com/hc/en-us/articles/360022367393-Zendesk-In-Product-Cookie-Policy">Zendesk Cookie Policy</a>.') ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12 margin-top-10">
        <?= Yii::t('app.cookie', 'We categorize cookies as follows:') ?>
    </div>
    <div class="col-xs-12 margin-top-10">
        <table class="table table-responsive table-striped table-bordered" style="font-weight: normal">
            <thead style="font-weight: bold">
                <tr>
                    <td>
                        <?= Yii::t('app.cookie', 'Category') ?>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'Description') ?>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <b><?= Yii::t('app.cookie', 'Necessary') ?></b>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'These cookies are essential for our website and our features to work properly. Without these cookies, services such as customer login or booking of hostesses cannot work properly.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><?= Yii::t('app.cookie', 'Preferences') ?></b>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'These cookies enable us to improve the usability of website and to provide various features. Preference cookies store chose language, for example.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><?= Yii::t('app.cookie', 'Analyse') ?></b>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'These cookies collect information about how you use our website. Based on these information we can adapt the content of our website more specifically to your needs and improve our system. These cookies do not collect personal data. Tools which we use for this purpose are Google Analytics and Hotjar.') ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><?= Yii::t('app.cookie', 'Chat') ?></b>
                    </td>
                    <td>
                        <?= Yii::t('app.cookie', 'These cookies are used to know if you allow us to display our Chat service or not, and are as well set by our chat service provider. Our chat service provider is Zopim. We use chat system in order to help you to better use our system. These cookies do not collect any personal data.') ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
