<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use yii\jui\DatePicker;
use frontend\models\Gender;
use frontend\models\Language;

\frontend\assets\RegisterAsset::register($this);


$this->title = Yii::t('app.hostess', 'Free hostess registration');

$datePickerOptions = [
    'language' => 'en',
    'inline' => false,
    'value' => date('d.m.Y', strtotime("-18 years")),
    'options' => [
        'placeholder' => Yii::t('app.hostess', 'Birth date*'),
        'class' => 'form-control'
    ],
    'dateFormat' => 'dd.MM.yyyy', // ALWAYS READ DOCS FOR THESE TYPE OF CONTROLS!!!
    'clientOptions' => [
        'changeMonth' => true,
        'changeYear' => true,
        'maxDate' => date('d.m.Y', strtotime("-18 years")),
        'yearRange' => "1960" . ":" . date("Y")
    ],
];
?>

<!--REGISTER BEGIN-->

<?= $this->render("//partials/_flash") ?>

<div class="site-login">

    <div class="row">
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">

        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="green-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-title">
                    <?= Yii::t('app.hostess', 'FREE REGISTRATION') ?>
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12 bh-subtitle">
                    <?= Yii::t('app.hostess', 'Register for free as Hostess/Promoter so that clients can see your profile and book you') ?>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <?php $form = ActiveForm::begin(['id' => 'register-form']); ?>

                    <div class="row">

                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'gender')->dropDownList(Gender::getListForHostess(), ["prompt" => \Yii::t('app.hostess', '- Please select gender -')])->label(false) ?>
                        </div>

                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'lang')->dropDownList(Language::getPrefferedLanguageList(), ["prompt" => \Yii::t('app.hostess', '- Please select system language -')])->label(false) ?>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'first_name')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.hostess', 'First name*')])->label(false) ?>
                        </div>
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'last_name')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.hostess', 'Last name*')])->label(false) ?>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?=
                            $form->field($model, 'birth_date')->input("text", ["placeholder" => "birth date"])->widget(DatePicker::className(), $datePickerOptions)->label(false)
                            ?>
                        </div>
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.hostess', 'Email*')])->label(false) ?>
                        </div>


                    </div>


                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'username')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.hostess', 'Username*')])->label(false) ?>
                        </div>
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'password', ["template" => '<div class="password js-password">{input} <span class="glyphicon glyphicon-eye-close js-show-password"></span> </div>'])->passwordInput(['maxlength' => 50, 'placeholder' => Yii::t('app.hostess', 'Password*')])->label(false) ?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <?= $form->field($model, 'terms', ['template' => '<div class="col-xs-1">{input}</div><div class="col-xs-10">{label}</div><div class="col-xs-12">{error}</div>'])->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app.hostess', 'REGISTER'), ['class' => 'btn btn-red full-width', 'name' => 'register-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>   

            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 text-muted">        

                    <?= Yii::t('app.hostess', 'If you already have a profile, click on login button to login.') ?> 

                </div>
                <div class="col-lg-3 col-md-9 col-sm-9 col-xs-4">
                    <?= Html::a(Yii::t('app.hostess', 'Login'), Yii::$app->urlManager->createUrl(["site/login"]), ["class" => "btn btn-default pull-right"]) ?> 
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">

        </div>

    </div>


</div>
<!--REGISTER END-->