<?php

use yii\helpers\Html;
use yii\grid\GridView;

\frontend\assets\HostessProfileAsset::register($this);

use kartik\rating\StarRating;

$this->title = Yii::t('app.hostess', 'Past Bookings');

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'header' => Yii::t('app.hostess', 'Event'),
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($model, $index, $widget)
{
    return $model->event->name;
},
    ],
    [
        'header' => Yii::t('app.hostess', 'Period'),
        'format' => 'raw',
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDate($model->date_from) . "<br>-<br>" . Yii::$app->util->formatDate($model->date_to);
        },
    ],
    [
        'attribute' => 'days',
        'header' => Yii::t('app.hostess', "Days"),
        'value' => function ($model, $index, $widget)
        {
            return $model->getTotalBookingDays();
        }
    ],
    [
        'attribute' => 'daily_rate',
        'header' => Yii::t('app.hostess', "Daily rate"),
        'value' => function($model)
        {
            return Yii::$app->util->formatEventPrice($model->event, $model->price);
        }
    ],
    [
        'attribute' => 'payment_type',
        'header' => Yii::t('app.hostess', "Payment of the daily rate"),
        'value' => function($model)
        {
            return $model->getPaymentTypeLabel();
        }
    ],
    [
        'attribute' => 'dress_code',
        'header' => Yii::t('app.hostess', "Outfit"),
        'value' => function ($model, $index, $widget)
        {
            return $model->dressCode->translatedName;
        },
    ],
    [
        'attribute' => 'job',
        'header' => Yii::t('app.hostess', "Job"),
        'value' => function ($model, $index, $widget)
        {
            return $model->jobName;
        },
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.hostess', 'Review'),
        'value' => function ($model, $index, $widget)
        {
            if (isset($model->hostessReview))
            {
                if ($model->hostessReview->isPending)
                {
                    return '<div class="text-center full-width lbl label-accepted">' . Yii::t('app.hostess', 'Review requested') . '</div>';
                } else
                {
                    return
                            StarRating::widget([
                                'name' => 'rating',
                                'value' => $model->hostessReview->rating,
                                'pluginOptions' => [
                                    'readonly' => true,
                                    'showClear' => false,
                                    'showCaption' => false,
                                    'size' => 'xs'
                                ]
                    ]);
                }
            } else
            {
                return Html::a(Html::encode(\Yii::t('app.hostess', 'Ask for Review')), \Yii::$app->urlManager->createUrl(['//review/request']), ["class" => "btn btn-green js-ask-review full-width", 'data-id' => $model->id]);
            }
        },
            ],
        ];
        ?>

        <?= $this->render("profile_parts/_profile_menu", ["model" => $profileModel, "active_tab" => "past_booking"]); ?>
        <div class="row hostess-tab-content">
            <div class="col-xs-12">
                <?=
                GridView::widget(
                        [
                            'columns' => $columns,
                            'dataProvider' => $dataProvider,
                            'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                            'options' => [
                                'class' => 'grid-view table-responsive',
                            ],
                            'tableOptions' => [
                                'class' => 'table table-grey table-bordered',
                            ],
                            'layout' => "{items}",
                        ]
                );
                ?>
            </div>
            <div class="col-xs-12 text-right margin-top-20">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'options' => ['class' => 'pagination'],
                    'maxButtonCount' => 5,
                    'nextPageLabel' => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'prevPageLabel' => '<'
                ]);
                ?>
            </div>  
        </div>
        <?= $this->render("_review_modal") ?>