<?php

use yii\helpers\Html;
use yii\grid\GridView;

\frontend\assets\HostessProfileAsset::register($this);

$this->title = Yii::t('app.hostess', 'My trade shows');

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'event.name',
        'header' => Yii::t('app.hostess', "Event"),
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($data)
{
    return Html::a(
                    Html::encode($data["event"]["name"]), \Yii::$app->urlManager->createUrl(['//event/view', "id" => $data["event"]["id"]]), ["class" => "green-link bold"]
    );
}
    ],
    [
        'attribute' => 'event.start_date',
        'header' => Yii::t('app.hostess', "Start date"),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDate($model->event->start_date);
        },
    ],
    [
        'attribute' => 'event.end_date',
        'header' => Yii::t('app.hostess', "End date"),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDate($model->event->end_date);
        },
    ],
    [
        'attribute' => 'date_created',
        'header' => Yii::t('app.hostess', "Created At"),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDate($model->date_created);
        },
    ],
    [
        'attribute' => 'price',
        'header' => Yii::t('app.hostess', 'Daily rate'),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatEventPrice($model->event, $model->price);
        },
    ],
    [
        'class' => '\yii\grid\ActionColumn',
        'template' => '{delete}',
        'header' => Yii::t('app.hostess', 'Options'),
        'buttons' => [
            'delete' => function ($url, $model)
            {

                if (isset($model) && isset($model->event))
                {
                    if (!$model->event->hostessHasNewRequests && !$model->event->hostessHasAcceptedRequests)
                    {
                        return
                                Html::button(Yii::t('app.hostess', 'Sign off'), [
                                    'class' => 'btn btn-default full-width js-event-withdraw',
                                    'data-url' => Yii::$app->urlManager->createUrl(["//event/withdraw"]),
                                    'data-id' => $model->event_id,
                                    'data-name' => $model->event->name,
                        ]);
                    }
                }
                // TODO dodati status Confirmed ili Rejected
                return '';
            }
                ]
            ]
        ];
        ?>

        <?=
        $this->render("profile_parts/_profile_menu", [ "model" => $profileModel, "active_tab" => "trade_shows"]);
        ?>

        <div class="row hostess-tab-content">
            <div class="col-xs-12">
                <?=
                GridView::widget(
                        [
                            'columns' => $columns,
                            'dataProvider' => $dataProvider,
                            'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                            'options' => [
                                'class' => 'grid-view table-responsive ',
                            ],
                            'tableOptions' => [
                                'class' => 'table table-grey table-bordered',
                            ],
                            'layout' => "{items}",
                        ]
                );
                ?>
            </div>
            <div class="col-xs-12 text-right margin-top-20">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'options' => ['class' => 'pagination'],
                    'maxButtonCount' => 5,
                    'nextPageLabel' => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'prevPageLabel' => '<'
                ]);
                ?>
            </div>  
        </div>

        <?= $this->render("//partials/_signoff_modal") ?>