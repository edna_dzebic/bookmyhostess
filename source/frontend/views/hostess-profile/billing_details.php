<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app.hostess', 'Billing details');
?>

<!--BILLING DETAILS-->
<div class="site-login">
    <div class="row">
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="green-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-title">
                    <?= Yii::t('app.hostess', 'Address') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 margin-top-20">
                    <p><?= Yii::t('app.hostess', 'We are currently working on a new feature that will facilitate the invoicing for you. Very soon you will be able to generate an invoice in just few klicks. In order to enable the system to fill in as many fields as possible, we need to collect the following invoice information:') ?></p>
                </div>
            </div>
            <div class="row margin-top-20">
                <div class="col-xs-12">
                    <?php $form = ActiveForm::begin(['id' => 'form']); ?>

                    <div class="row">
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'phone')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.hostess', 'Phone*')]) ?>
                        </div>
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'tax_number')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.hostess', 'Tax number*')]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                            <?= $form->field($model, 'billing_address')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.hostess', 'Address*')]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'postal_code')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.hostess', 'Postal code*')]) ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'city')->textInput(['maxlength' => 255, 'placeholder' => Yii::t('app.hostess', 'City*')]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?=
                                    $form->field($model, 'country_id')
                                    ->dropDownList(\common\models\Country::getListForDropdown(), [ "prompt" => \Yii::t('app.hostess', '- Please select country -')])
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app.hostess', 'SAVE'), ['class' => 'btn btn-red full-width']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>            
        </div>
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
        </div>
    </div>
</div>
<!--BILLING DETAILS END-->

