<?php

use yii\helpers\Html;

?>
<ul class="row profile-images">
    <?php if (isset($model->images) && count($model->images) > 0) { ?>
        <?php foreach ($model->images as $image) { ?>
            <li class="col-lg-4">
                <a href="<?= Yii::getAlias("@web") . $image->getImagePath(); ?>" data-fancybox="images">
                    <div class="image img-responsive" style="
                        background-image: url('<?= $image->getCachedImage(53, 75); ?>');
                        ">
                    </div>
                </a>
            </li>
            <?php
        }
    }
    ?>
</ul>