<?php
\frontend\assets\HostessProfileAsset::register($this);

$this->title = Yii::t('app.hostess', 'Profile');

use yii\bootstrap\Progress;
?>


<?= $this->render("profile_parts/_profile_menu", ["model" => $model, "active_tab" => "profile"]); ?>

<div class="hostess-tab-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?= $this->render("profile_parts/_profile_sub_menu", ["active_tab" => $view]); ?>


            <div class="row">
                <div class="col-xs-12 margin-top-20">
                    <p class="green-form-title">
                        <?= Yii::t('app.hostess', 'Profile completeness:') ?> <span class="js-profile-completeness-info-icon"><i class="glyphicon glyphicon-question-sign red-glyphicon"></i></span>
                    </p>
                </div>
                <div class=" col-xs-12 margin-top-10">
                    <?=
                    Progress::widget([
                        'percent' => $model->profile_completeness_score,
                        'label' => $model->profile_completeness_score . "% / 100%",
                        'options' => [
                            'class' => 'js-profile-completeness-score-bar'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-xs-12">
                    <p class="green-text">
                        <i class="glyphicon glyphicon-info-sign"></i>
                        <?= Yii::t('app.hostess', 'You can set higher fees if your profile is completely filled out and the profile completeness is 100%') ?></p>
                </div>
            </div>

            <?= $this->render("profile_parts/$view", [ "model" => $model, "data" => $data]); ?> 
        </div>
    </div>
    <?= $this->render("profile_parts/_footer", ["model" => $model]) ?>
</div>

<?= $this->render('profile_parts/_profile_completeness_modal') ?>
<?= $this->render('profile_parts/_profile_reliability_modal') ?>
<?= $this->render('profile_parts/_approve_modal', ["model" => $model]) ?>