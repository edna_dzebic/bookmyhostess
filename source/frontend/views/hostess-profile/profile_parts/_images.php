<?php

use kartik\file\FileInput;
use yii\helpers\Html;

$defaultImage = $model->getDefaultImageModel(true);
?>


<div class="row">
    <div class="col-xs-12">
        <p class="green-form-title"><?= Yii::t('app.hostess', 'Images') ?></p>
    </div>     
</div>

<div class="row">
    <div class="col-lg-12">
        <?php if ($defaultImage !== null) { ?>
            <?= Html::img($defaultImage->getCachedImage(360, 400), ["class" => "img-responsive  carousel"]) ?>
        <?php } ?>
    </div>
</div>

<?php if (count($model->images) <= 0) : ?>
    <div class="row margin-top-10">
        <div class="col-lg-12">
            <p><?= Yii::t('app.hostess', 'Please upload your image!'); ?></p>
        </div>
    </div>
<?php endif; ?>

<div class="row margin-top-20">
    <div class="col-xs-6">
        <button class="btn btn-green full-width js-edit-gallery"><?= Yii::t('app.hostess', 'Edit Images') . " (" . count($model->images) . ")" ?></button>
    </div>
    <div class="col-xs-6">

        <div class="upload">
            <?php
            $imageForm = \yii\widgets\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'image-form']]);
            echo FileInput::widget([
                'model' => $userImagesModel,
                'attribute' => 'image_url',
                'pluginOptions' => [
                    'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'showPreview' => false,
                    'showCaption' => false,
                    'maxImageHeight' => '20',
                    'browseClass' => 'btn btn-standard btn-long btn-block',
                    'browseIcon' => '',
                    'browseLabel' => Yii::t('app.hostess', 'Upload images')
                ],
                'options' => ['accept' => 'image/*', 'onChange' => 'js:$("#image-form").submit()', 'multiple' => false]
            ]);
            ?>
            <?php $imageForm->end() ?>

        </div>
    </div>
</div>


<div class="modal fade" id="edit-images-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.hostess', 'Edit Images') ?></h3>
            </div>

            <div class="modal-body panel-body">
                <?php if (isset($model->images) && count($model->images) > 0) : ?>
                    <div class="profile-images row " id="profile-images">

                        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-2">
                            <div class="controls text-left">
                                <button class="btn js-prev">< </button>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-4 col-xs-8">
                            <div class="frame" id="centered" >
                                <ul class="clearfix">
                                    <?php foreach ($model->images as $image) : ?>
                                        <li>
                                            <div class="row item">
                                                <div class="col-xs-12">
                                                    <?= Html::img($image->getCachedImage(260, 300), ["class" => "img-responsive"]) ?>
                                                </div>                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <div class="btn-group">
                                                        <?= Html::a(\Yii::t('app.hostess', 'Set as profile picture'), ["//hostess-profile/set-image-as-default", "id" => $image->id], ["class" => "btn btn-green"]); ?>
                                                        <?= Html::a(\Yii::t('app.hostess', 'Delete'), ["//hostess-profile/delete-image", "id" => $image->id], ["class" => "btn btn-default"]); ?>
                                                    </div>     
                                                </div>
                                            </div> 
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-1  col-xs-2">
                            <div class="controls text-right">
                                <button class="btn js-next"> > </button>
                            </div>
                        </div>

                    </div>
                <?php endif; ?> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.hostess', 'Close Dialog') ?>
                </button>
            </div>           
        </div>
    </div>
</div>