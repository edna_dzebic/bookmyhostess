<?php

use yii\widgets\ActiveForm;
?>

<div>
    <?= Yii::t('app.hostess', 'You are about to change your email. <br> Your current email is <b>{oldEmail}</b>.', ["oldEmail" => $oldEmail]) ?>
</div>

<br>
<div class="text-center loading-content">
    <div>
        <?= Yii::t('app.hostess', 'Please wait...') ?>
    </div>
    <br>
    <div class="loader js-loader"></div>    
</div>

<br>

<?php
$form = ActiveForm::begin([
            "id" => "change-email-form",
            "action" => ["/hostess-profile/change-email"]]);

echo '<label>' . Yii::t('app.hostess', 'Please enter the new Email address:') . '</label><br>';
echo $form->field($model, 'email')->input("text")->label(false);

ActiveForm::end();
