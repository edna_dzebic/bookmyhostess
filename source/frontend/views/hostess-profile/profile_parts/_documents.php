<?php

use kartik\file\FileInput;
?>

<div class="row">

    <div class="col-xs-12">
        <p class="green-form-title"><?= Yii::t('app.hostess', 'Documents') ?>*</p>

    </div> 


    <div class="col-xs-12">
        <?php if (count($model->documents) > 0)
        {
            ?>
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <td style="text-align: center;"><?= Yii::t('app.hostess', 'Filename') ?></td>
                        <td style="text-align: center;"><?= Yii::t('app.hostess', 'Options') ?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($model->documents as $doc)
                    {
                        ?>
                        <tr>
                            <td style="text-align: center;"><?= (strlen($doc->real_filename) > 10) ? substr($doc->real_filename, 0, 10) . '...' : $doc->real_filename; ?></td>
                            <td style="text-align: center;">
                                <table style="text-align: center; width: 100%;">
                                    <tr style="text-align: center;">
                                        <td width="49%"  style="text-align: center;">
                                            <a target="_blank" class="btn btn-green full-width"
                                               href="<?= \Yii::$app->params['user_images_url'] . $doc->filename ?>"><?= Yii::t('app.hostess', 'Download') ?></a>
                                        </td>
                                        <td width="2%">

                                        </td>
                                        <td width="49%"  style="text-align: center;">
                                            <a class="btn btn-default full-width" href="<?= \Yii::$app->urlManager->createUrl(["//hostess-profile/delete-document", "id" => $doc->id]); ?>"><?= Yii::t('app.hostess', 'Delete') ?></a>

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr> 
            <?php } ?>
                </tbody>
            </table>
<?php } else
{
    ?>
            <div class="text-center">
                <p><?= Yii::t('app.hostess', 'No documents') ?></p>
            </div>
<?php } ?>
    </div>
    <div class="col-lg-12">


        <div class="upload">
            <?php
            $documentForm = \yii\widgets\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'document-form']]);

// A block file picker button with custom icon and label
            echo FileInput::widget([
                'model' => $userDocumentsModel,
                'attribute' => 'filename',
                'pluginOptions' => [
                    'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'showPreview' => false,
                    'showCaption' => false,
                    'browseClass' => 'btn btn-standard btn-long btn-block',
                    'browseIcon' => '',
                    'browseLabel' => Yii::t('app.hostess', 'Add documents')
                ],
                'options' => ['accept' => '.png,.jpg,.jpeg,.gif,.doc,.docx,.pdf,.xls,.xlsx,.odf', 'onChange' => 'js:$("#document-form").submit()', 'multiple' => false]
            ]);
            ?>
<?php $documentForm->end() ?>

        </div>
    </div>
    <div class="col-xs-12">
        <p class="text-muted">
            *<?= Yii::t('app.hostess', 'For example: trade licence, health certificate, testimonials...') ?>
        </p>
    </div>
</div>