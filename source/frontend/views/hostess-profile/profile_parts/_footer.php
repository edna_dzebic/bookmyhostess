<?php

use yii\helpers\Html;

/* @var $model frontend\models\HostessProfile */
$isProfileCompleted = $model->isProfileCompleted();
?>

<div class="row margin-top-40">
    <?php if ($model->isNotRequested()) : ?>
        <div class="col-xs-12">
            <?php
            $form = kartik\form\ActiveForm::begin();
            echo $form->errorSummary($model, ['header' => Yii::t('app.hostess', '<h3> Please correct following errors and click "Approve now": </h3>')]);
            $form->end();
            ?>
        </div>

        <div class="col-xs-12">
            <div class="alert alert-danger alert-important">
                <div class="row">

                    <?php if ($isProfileCompleted) : ?> 
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 margin-top-10">
                            <?= \Yii::t('app.hostess', 'Your profile is not approved yet. Click on "Approve now".'); ?>
                        </div>
                        <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12 margin-top-10 text-center">
                            <?= Html::a(Yii::t('app.hostess', 'Approve now'), \Yii::$app->urlManager->createUrl(["//hostess-profile/approve-me"]), ['class' => ' btn btn-red full-width btn-large-padding']);
                            ?>
                        </div>
                    <?php else: ?> 
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 margin-top-10">
                            <?= \Yii::t('app.hostess', 'Your profile is not approved yet. Please fill in all required fields and click on "Approve now".'); ?>
                        </div>
                        <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12 margin-top-10 text-center" title ="<?= Yii::t('app.hostess', 'Please fill in all required fields and click on Approve now!') ?>">
                            <?= Html::a(Yii::t('app.hostess', 'Approve now'), "#", ['class' => ' btn btn-red full-width btn-large-padding approval-disabled-btn js-disabled-approve-now']);
                            ?>
                        </div>

                    <?php endif; ?>
                </div>
            </div>
        </div>


        <?php if (!$isProfileCompleted && $model->canPostponeTradeLicense) : ?> 
            <div class="col-xs-12">
                <div class="alert alert-important">
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 margin-top-10">
                            <?= \Yii::t('app.hostess', 'You can request to postpone upload of trade license file and you will be able to send approve request after.'); ?>
                        </div>
                        <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12 margin-top-10 text-center" title ="<?= Yii::t('app.hostess', 'Postpone upload') ?>">
                            <?= Html::a(Yii::t('app.hostess', 'Postpone upload'), \Yii::$app->urlManager->createUrl(["//hostess-profile/extend-trade-license"]), ['class' => ' btn btn-default full-width btn-large-padding js-extend-trade-license']);
                            ?>
                        </div>
                    </div>
                </div>
            </div>    
        <?php endif; ?>

    <?php elseif ($model->isApproved()) : ?>
        <div class="col-xs-12 text-center">
            <div class="alert alert-success">
                <?= \Yii::t('app.hostess', 'Profile approved by administrators! '); ?>
            </div>
        </div>                    
    <?php elseif ($model->isRequested()) : ?>
        <div class="col-xs-12 text-center">
            <div class="alert alert-info">
                <?= \Yii::t('app.hostess', 'Profile sent for review! '); ?>
            </div>
        </div> 
    <?php endif; ?>
</div>


<?= $this->render("_trade_license_extend_modal") ?>