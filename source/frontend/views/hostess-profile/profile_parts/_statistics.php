<?php
$totalRequests = $model->getTotalRequestsCount();
$confirmed = $model->getConfirmedRequestsCount();
$rejected = $model->getRejectedRequestCount();
$booked = $model->getBookedRequestCount();
$notAnswered = $model->getNotAnsweredRequestCount();
$colors = $model->getRequestColors();

use yii\bootstrap\Progress;
?>

<div class="row statistics">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-label">
                <?= Yii::t('app.hostess', 'Reliability index:'); ?> <span class="js-profile-reliability-info-icon"><i class="glyphicon glyphicon-question-sign red-glyphicon"></i></span>
            </div> 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-value">
                <?=
                Progress::widget([
                    'percent' => $model->profile_reliability_score,
                    'label' => $model->profile_reliability_score . "% / 100%",
                    'options' => [
                        'class' => 'js-profile-reliability-score-bar'
                    ]
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-label">
                <?= Yii::t('app.hostess', 'Profile views:'); ?>
            </div> 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-value">
                <?= $model->profile_views ?> 
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-label"><?= Yii::t('app.hostess', 'Last login:'); ?></div> 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-value"><?= Yii::$app->util->formatDate($model->user->last_login) ?></div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-label"><?= Yii::t('app.hostess', 'Time to respond:'); ?></div> 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-value"> 
                <span class=" statistic-color <?= $colors["time"] ?>">
                    <?= $model->getTimeToRespond(); ?>
                </span> 
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-label"><?= Yii::t('app.hostess', 'Request:'); ?></div> 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-value"><?= $totalRequests ?></div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-label"><?= Yii::t('app.hostess', 'Booked:'); ?></div> 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6  attr-value">
                <div>
                    <span class="statistic-value"><?= $booked ?></span>
                    <span class="statistic-separator">/</span>
                    <span class="statistic-value"><?= $confirmed ?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-label"><?= Yii::t('app.hostess', 'Confirmed:'); ?></div> 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-value">
                <span class="statistic-color <?= $colors["confirmed"] ?> orange">
                    <span class="statistic-value"><?= $confirmed ?></span>
                    <span class="statistic-separator">/</span>
                    <span class="statistic-value"><?= $totalRequests ?></span>
                </span>
            </div>
        </div>
        <div class="row ">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-label"><?= Yii::t('app.hostess', 'Rejected:'); ?></div> 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-value">
                <span class="statistic-color <?= $colors["rejected"] ?> yellow">
                    <span class="statistic-value"><?= $rejected ?></span>
                    <span class="statistic-separator">/</span>
                    <span class="statistic-value"><?= $totalRequests ?></span>        
                </span>
            </div>
        </div>        
        <div class="row  ">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-label"><?= Yii::t('app.hostess', 'Not answered:'); ?></div> 
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 attr-value">
                <span class="statistic-color <?= $colors["notAnswered"] ?> red">
                    <span class="statistic-value"><?= $notAnswered ?></span>
                    <span class="statistic-separator">/</span>
                    <span class="statistic-value"><?= $totalRequests ?></span>
                </span>                
            </div>
        </div>
    </div>
</div>
