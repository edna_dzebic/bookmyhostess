<div class="modal fade" id="profile-completeness-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.hostess', 'Profile completeness') ?></h3>
            </div>
            <div class="modal-body panel-body">
                <?= Yii::t('app.hostess', 'The table below illustrates how and to what extent particular profile sections affect the completeness score.') ?>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Profile section') ?></td>
                            <td><?= Yii::t('app.hostess', 'Condition') ?></td>
                            <td><?= Yii::t('app.hostess', 'Percentage') ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Images') ?></td>
                            <td><?= Yii::t('app.hostess', 'Minimum 3') ?></td>
                            <td><?= Yii::$app->settings->pcs_images ?></td>
                        </tr> 
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Languages') ?></td>
                            <td><?= Yii::t('app.hostess', 'Minimum 2') ?></td>
                            <td><?= Yii::$app->settings->pcs_languages ?></td>
                        </tr> 
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Work experience') ?></td>
                            <td><?= Yii::t('app.hostess', 'Minimum 3') ?></td>
                            <td><?= Yii::$app->settings->pcs_work_experience ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Preferred cities') ?></td>
                            <td><?= Yii::t('app.hostess', 'Minimum 1') ?></td>
                            <td><?= Yii::$app->settings->pcs_preferred_cities ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Preferred jobs') ?></td>
                            <td><?= Yii::t('app.hostess', 'Minimum 1') ?></td>
                            <td><?= Yii::$app->settings->pcs_preferred_jobs ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Mobile App') ?></td>
                            <td><?= Yii::t('app.hostess', 'Installed') ?></td>
                            <td><?= Yii::$app->settings->pcs_mobile_app ?></td>
                        </tr> 
                        <tr>
                            <td><?= Yii::t('app.hostess', 'About me') ?></td>
                            <td><?= Yii::t('app.hostess', 'Completed') ?></td>
                            <td><?= Yii::$app->settings->pcs_summary ?></td>
                        </tr> 
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Education') ?></td>
                            <td><?= Yii::t('app.hostess', 'All completed') ?></td>
                            <td><?= Yii::$app->settings->pcs_education ?></td>
                        </tr> 
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Basic profile') ?></td>
                            <td><?= Yii::t('app.hostess', 'All completed') ?></td>
                            <td><?= Yii::$app->settings->pcs_basic_profile ?></td>
                        </tr>  
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.hostess', 'Close Dialog') ?>
                </button>
            </div>           
        </div>
    </div>
</div>