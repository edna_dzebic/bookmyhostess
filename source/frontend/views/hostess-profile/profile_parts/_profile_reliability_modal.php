<div class="modal fade" id="profile-reliability-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.hostess', 'Reliability index') ?></h3>
            </div>
            <div class="modal-body panel-body">
                <?= Yii::t('app.hostess', 'The table below illustrates how and to what extent particular profile sections affect the reliability score. This score directly affects the options for daily rates.') ?>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Option') ?></td>
                            <td><?= Yii::t('app.hostess', 'Condition') ?></td>
                            <td><?= Yii::t('app.hostess', 'Percentage') ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Profile completeness score') ?></td>
                            <td>
                                <table>
                                    <tr>
                                        <td><?= Yii::t('app.hostess', '100%') ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?= Yii::t('app.hostess', 'Less than {value}', ["value" => '100%']) ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <?= Yii::$app->settings->prs_profile_completeness ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>0</td>
                                    </tr>
                                </table>
                            </td>
                        </tr> 
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Bookings') ?></td>
                            <td>
                                <table>
                                    <tr><td><?= Yii::t('app.hostess', 'More than {value}', ["value" => Yii::$app->settings->prs_booked_count_min_cnt]) ?></td></tr>
                                    <tr><td>1</td></tr>
                                    <tr><td>0</td></tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <?= Yii::$app->settings->prs_booked_count ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?= 0.5 * Yii::$app->settings->prs_booked_count ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            0
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr> 
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Feedback') ?></td>
                            <td>
                                <table>
                                    <tr><td><?= Yii::t('app.hostess', 'More than {value}', ["value" => Yii::$app->settings->prs_review_count_min_cnt]) ?></td></tr>
                                    <tr><td>1</td></tr>
                                    <tr><td>0</td></tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <?= Yii::$app->settings->prs_review_count ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?= 0.5 * Yii::$app->settings->prs_review_count ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            0
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('app.hostess', 'Answering time') ?></td>
                            <td>
                                <table>
                                    <tr><td><?= Yii::t('app.hostess', 'Less than {value} hours', ["value" => Yii::$app->settings->prs_time_level_zero]) ?></td></tr>
                                    <tr><td><?= Yii::t('app.hostess', '{from} to {to} hours', ["from" => Yii::$app->settings->prs_time_level_zero, "to" => Yii::$app->settings->prs_time_level_one]) ?></td></tr>
                                    <tr><td><?= Yii::t('app.hostess', '{from} to {to} hours', ["from" => Yii::$app->settings->prs_time_level_one, "to" => Yii::$app->settings->prs_time_level_two]) ?></td></tr>
                                    <tr><td><?= Yii::t('app.hostess', '{from} to {to} hours', ["from" => Yii::$app->settings->prs_time_level_two, "to" => Yii::$app->settings->prs_time_level_three]) ?></td></tr>
                                    <tr><td><?= Yii::t('app.hostess', '{from} to {to} hours', ["from" => Yii::$app->settings->prs_time_level_three, "to" => Yii::$app->settings->prs_time_level_four]) ?></td></tr>
                                     <tr><td><?= Yii::t('app.hostess', '{from} to {to} hours', ["from" => Yii::$app->settings->prs_time_level_four, "to" => Yii::$app->settings->prs_time_level_five]) ?></td></tr>

                                    <tr><td><?= Yii::t('app.hostess', 'More than {value} hours', ["value" => Yii::$app->settings->prs_time_level_five]) ?></td></tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <?php foreach (Yii::$app->settings->getTimeLevelsWithCoefficients() as $level => $coeff) : ?>
                                        <tr>
                                            <td>
                                                <?= $coeff * Yii::$app->settings->prs_avg_answer_time_percent ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    <tr>
                                        <td>
                                            0
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('app.hostess', 'No. of not answered req.') ?></td>
                            <td>
                                <table>
                                    <tr><td><?= Yii::t('app.hostess', '0%') ?></td></tr>
                                    <tr><td><?= Yii::t('app.hostess', 'Less than {value}', ["value" => '5%']) ?></td></tr>
                                    <tr><td><?= Yii::t('app.hostess', 'More than {value}', ["value" => '5%']) ?></td></tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr><td><?= Yii::$app->settings->prs_no_answer_count ?></td></tr>
                                    <tr><td><?= 0.5 * Yii::$app->settings->prs_no_answer_count ?></td></tr>
                                    <tr><td>0</td></tr>
                                </table>
                            </td>                             
                        </tr> 
                        <tr>
                            <td><?= Yii::t('app.hostess', 'No. of rejected req.') ?></td>
                            <td>
                                <table>
                                    <tr><td><?= Yii::t('app.hostess', 'Less than {value}', ["value" => '5%']) ?></td></tr>
                                    <tr><td><?= Yii::t('app.hostess', 'Less than {value}', ["value" => '10%']) ?></td></tr>
                                    <tr><td><?= Yii::t('app.hostess', 'More than {value}', ["value" => '10%']) ?></td></tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr><td><?= Yii::$app->settings->prs_reject_count ?></td></tr>
                                    <tr><td><?= 0.5 * Yii::$app->settings->prs_reject_count ?></td></tr>
                                    <tr><td>0</td></tr>
                                </table>
                            </td>                             
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.hostess', 'Close Dialog') ?>
                </button>
            </div>           
        </div>
    </div>
</div>