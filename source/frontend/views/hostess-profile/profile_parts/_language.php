<?php

use yii\helpers\Html;
use kartik\editable\Editable;
use kartik\popover\PopoverX;
use yii\grid\GridView;

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'language.name',
        'value' => function ($model, $key, $index, $column)
        {
            return $model->language->getTranslatedName();
        },
        'header' => Yii::t('app.hostess', 'Language'),
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left']
    ],
    [
        'attribute' => 'level',
        'value' => function ($model, $key, $index, $column)
        {
            return $model->level->getTranslatedName();
        },
        'header' => Yii::t('app.hostess', 'Level'),
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left']
    ],
    [
        'class' => '\yii\grid\ActionColumn',
        'template' => '{delete}',
        'header' => Yii::t('app.hostess', 'Options'),
        'buttons' => [
            'delete' => function ($url, $model)
            {
                return Html::button(Yii::t('app.hostess', 'Delete'), [
                            'data-url' => \Yii::$app->urlManager->createUrl(["//hostess-profile/delete-language"]),
                            'data-id' => $model->id,
                            'data-name' => $model->language->getTranslatedName(),
                            'class' => 'btn btn-default js-delete-language'
                ]);
            }
                ]
            ]
        ];
        ?>


        <div class="hostess-content padding-top-30">

            <div class="row">
                <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6">
                    <?=
                    GridView::widget(
                            [
                                'columns' => $columns,
                                'dataProvider' => $data["languageDataProvider"],
                                'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                                'options' => [
                                    'class' => 'grid-view table-responsive',
                                ],
                                'tableOptions' => [
                                    'class' => 'table  table-green',
                                ],
                                'layout' => "{items}",
                            ]
                    );
                    ?>
                </div>

                <div class="col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?=
                        Editable::widget([
                            'id' => 'language-add',
                            'format' => Editable::FORMAT_BUTTON,
                            'name' => 'language_id',
                            'header' => Yii::t('app.hostess', 'Add language'),
                            'value' => '',
                            'placement' => PopoverX::ALIGN_TOP_LEFT,
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'preHeader' => '',
                            'buttonsTemplate' => '{submit}',
                            'submitButton' => ['icon' => Yii::t('app.hostess', 'Save'), 'class' => 'btn btn-green'],
                            'resetButton' => false,
                            'pluginOptions' => [
                            ],
                            'pluginEvents' => [
                                "editableSuccess" => "function(event, val, form, data) {  $(form)[0].reset();location.reload();  }",
                            ],
                            'data' => \frontend\models\Language::getAllTranslated(),
                            'options' => ['class' => 'form-control', 'prompt' => Yii::t('app.hostess', 'Select language...')],
                            'afterInput' => function ($form, $widget)
                    {
                        echo Html::dropDownList('level', null, \frontend\models\LanguageLevel::getAllTranslated(), ['class' => 'form-control', 'prompt' => Yii::t('app.hostess', 'Select level...')]);
                    },
                            'editableButtonOptions' => [
                                'class' => 'btn btn-green',
                                'label' => Yii::t('app.hostess', 'Add')
                            ],
                            'editableValueOptions' => ['class' => 'hidden'],
                            'containerOptions' => ['class' => 'pull-left']
                        ])
                        ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?php
                        echo \yii\widgets\LinkPager::widget([
                            'pagination' => $data["languageDataProvider"]->getPagination(),
                            'options' => ['class' => 'pagination'],
                            'maxButtonCount' => 5,
                            'nextPageLabel' => '>',
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>>',
                            'prevPageLabel' => '<'
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $this->render("_delete_lng_modal") ?>