<?php

use kartik\detail\DetailView;
?>
<div class="education hostess-content padding-top-30">
    <div class="row">
        <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
            <?php
            echo DetailView::widget([
                'model' => $model,
                'hAlign' => DetailView::ALIGN_LEFT,
                'vAlign' => DetailView::ALIGN_TOP,
                'condensed' => true,
                'hover' => true,
                'bordered' => false,
                'striped' => false,
                'mode' => DetailView::MODE_VIEW,
                'buttonContainer' => ['class' => 'pull-left margin-top-20'],
                'buttons1' => '{update}',
                'buttons2' => '{save}',
                'labelColOptions' => ['style' => 'width: 25%'],
                'saveOptions' => [
                    'class' => 'btn btn-standard ',
                    'label' => Yii::t('app.hostess', 'Save')
                ],
                'updateOptions' => [
                    'class' => 'btn btn-standard',
                    'label' => Yii::t('app.hostess', 'Edit')
                ],
                'panel' => [
                    'footer' => true,
                    'footerOptions' => [
                        'template' => '{buttons}'
                    ],
                    'heading' => false,
                ],
                'attributes' => [
                    'education_graduation',
                    'education_university',
                    'education_vocational_training',
                    'education_profession',
                    'education_special_knowledge',
                ]
            ]);
            ?>
        </div>
    </div>
</div>