<?php

use yii\helpers\Html;
use yii\grid\GridView;

\frontend\components\simpleDatePicker\SimpleDatePickerAsset::register($this);

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'start_date',
        'value' => function ($model, $key, $index, $widget)
        {
            return date('d.m.Y', strtotime($model->start_date));
        },
    ],
    [
        'attribute' => 'end_date',
        'value' => function ($model, $key, $index, $widget)
        {
            return date('d.m.Y', strtotime($model->end_date));
        },
    ],
    [
        'attribute' => 'event_name'
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.hostess', 'Description'),
        'headerOptions' => ["style" => "width : 20%;"],
        'value' => function ($model, $index, $widget)
{
    return $model->job_description;
},
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.hostess', 'Position'),
        'value' => function ($model, $index, $widget)
        {
            return $model->position;
        },
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.hostess', 'Options'),
        'value' => function ($model, $index, $widget)
        {
            return '' . Html::a(Yii::t('app.hostess', 'Edit'), \Yii::$app->urlManager->createUrl(["//hostess-profile/edit-work", "id" => $model->id]), [ 'title' => Yii::t('app.hostess', 'Update'), 'class' => 'btn btn-green js-edit-work', 'data-id' => $model->id]) . " " .
                    Html::button(Yii::t('app.hostess', 'Delete'), [
                        'data-name' => $model->job_description,
                        'class' => 'btn btn-default js-delete-work',
                        'data-id' => $model->id,
                        'data-url' => \Yii::$app->urlManager->createUrl(["//hostess-profile/delete-work"])]) .
                    '';
        },
            ],
        ];
        ?>

        <div class="row hostess-content padding-top-30">

            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
                <?=
                GridView::widget(
                        [
                            'columns' => $columns,
                            'dataProvider' => $data["workingExperienceProvider"],
                            'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                            'options' => [
                                'class' => 'grid-view table-responsive',
                            ],
                            'tableOptions' => [
                                'class' => 'table  table-green',
                            ],
                            'layout' => "{items}",
                        ]
                );
                ?>
            </div>

            <div class="col-xs-12  margin-top-20">
                <div class="row">                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">
                        <?= Html::button(Yii::t('app.hostess', 'Add'), [ 'class' => 'btn btn-green js-add-work']) ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
                        <?php
                        echo \yii\widgets\LinkPager::widget([
                            'pagination' => $data["workingExperienceProvider"]->getPagination(),
                            'options' => ['class' => 'pagination'],
                            'maxButtonCount' => 5,
                            'nextPageLabel' => '>',
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>>',
                            'prevPageLabel' => '<'
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $this->render("_work_exp_modal") ?>
        <?= $this->render("_add_work_exp_modal") ?>
        <?= $this->render("_delete_exp_modal") ?>