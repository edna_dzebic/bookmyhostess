<?php

use yii\helpers\Html;
use frontend\models\Request;
use yii\bootstrap\Tabs;

/* @var $model frontend\models\HostessProfile */

$count = Request::getHostessPendingCount();
$messageFromAdmin = $model->getAdminMessage();

/* @var $model frontend\models\HostessProfile */
$isProfileCompleted = $model->isProfileCompleted();
?>

<?= $this->render("//partials/_flash") ?>
<div class="row">
    <?php if ($model->isNotRequested()) : ?>
        <div class="col-xs-12">
            <div class="alert alert-danger alert-important">
                <div class="row">

                    <?php if ($isProfileCompleted) : ?> 
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 margin-top-10">
                            <?= \Yii::t('app.hostess', 'Your profile is not approved yet. Click on "Approve now".'); ?>
                        </div>
                        <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12 margin-top-10 text-center">
                            <?= Html::a(Yii::t('app.hostess', 'Approve now'), \Yii::$app->urlManager->createUrl(["//hostess-profile/approve-me"]), ['class' => ' btn btn-red full-width btn-large-padding']);
                            ?>
                        </div>
                    <?php else: ?> 
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 margin-top-10">
                            <?= \Yii::t('app.hostess', 'Your profile is not approved yet. Please fill in all required fields and click on "Approve now".'); ?>
                        </div>
                        <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12 margin-top-10 text-center" title ="<?= Yii::t('app.hostess', 'Please fill in all required fields and click on Approve now!') ?>">
                            <?= Html::a(Yii::t('app.hostess', 'Approve now'), "#", ['class' => ' btn btn-red full-width btn-large-padding approval-disabled-btn js-disabled-approve-now']);
                            ?>
                        </div>

                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <?php
            $form = kartik\form\ActiveForm::begin();
            echo $form->errorSummary($model, ['header' => Yii::t('app.hostess', '<h3> Please correct following errors and click "Approve now": </h3>')]);
            $form->end();
            ?>
        </div>

    <?php elseif ($model->isRequested()) : ?>
        <div class="col-xs-12 text-center">
            <div class="alert alert-info"><?= \Yii::t('app.hostess', 'Profile sent for review! '); ?></div>
        </div> 
    <?php endif; ?>
    
    <?php if ($model->isApproved() && !$model->isInstalledMobile()) : ?>
        <div class="col-xs-12">
            <div class="alert alert-danger alert-important">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 margin-top-10">
                        <?= \Yii::t('app.hostess', 'You have not installed mobile application yet. Users with mobile application are more likely to be booked. <br> Please choose version you prefer.'); ?>
                    </div>
                    <div class="col-lg-2 col-sm-6 col-md-6 col-xs-12 margin-top-10  text-center">
                        <a class="btn btn-red" href="<?= Yii::$app->settings->google_play ?>" target="_blank"><?= Yii::t('app.hostess', 'Android application') ?></a>
                    </div>
                    <div class="col-lg-2 col-sm-6 col-md-6 col-xs-12 margin-top-10  text-center">
                        <a class="btn btn-red" href="<?= Yii::$app->settings->app_store ?>" target="_blank"><?= Yii::t('app.hostess', 'iOS application') ?></a>
                    </div>
                </div>
            </div>
        </div> 
    <?php endif; ?>
</div>

<div class="row hostess-profile-submenu">

    <?php
    if ($messageFromAdmin)
    {
        ?>
        <?= $this->render("_hostess_message_from_admin", ["messageFromAdmin" => $messageFromAdmin]); ?>
    <?php } ?>

</div>

<div>
    <?=
    Tabs::widget([
        'navType' => 'nav-pills green-nav-pills profile-main-menu',
        'items' => [
            [
                'label' => Yii::t('app.hostess', 'Profile'),
                'url' => ["//hostess-profile/index"],
                'active' => $active_tab == "profile",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.hostess', 'My trade shows'),
                'url' => ["//hostess-profile/trade-shows"],
                'active' => $active_tab == "trade_shows",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                //"Anfragen"
                'label' => $count == 0 ? Yii::t('app.hostess', 'Requests') : Yii::t('app.hostess', 'Requests') . ' <div class="request-alert">' . $count . '</div>',
                'encode' => false,
                'url' => ["//hostess-profile/requests"],
                'active' => $active_tab == "requests",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                //  "Aktuelle Buchungen" 
                'label' => Yii::t('app.hostess', 'Current Bookings'),
                'url' => ["//hostess-profile/current-booking"],
                'active' => $active_tab == "current_booking",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                // "Vergangene Buchungen"
                'label' => Yii::t('app.hostess', 'Past Bookings'),
                'url' => ["//hostess-profile/past-booking"],
                'active' => $active_tab == "past_booking",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                // "Reviews"
                'label' => Yii::t('app.hostess', 'Reviews'),
                'url' => ["//hostess-profile/reviews"],
                'active' => $active_tab == "reviews",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ]
        ],
    ]);
    ?>
</div>