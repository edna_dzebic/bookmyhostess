<div class="modal fade" id="add-work-exp-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.hostess', 'Add work experience') ?></h3>
            </div>
            <div class="modal-body panel-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.hostess', 'Close Dialog') ?>
                </button>
                <button id="options-modal-confirm" type="button" class="btn btn-green js-btn-add-work">
                    <?= Yii::t('app.hostess', 'Save') ?>
                </button>
            </div>           
        </div>
    </div>
</div>