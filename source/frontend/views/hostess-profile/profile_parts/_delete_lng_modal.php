<div class="modal fade" id="delete-lng-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.hostess', 'Delete language') ?></h3>
            </div>
            <div class="modal-body panel-body">
                <h5 class="text-center">
                    <?= Yii::t('app.hostess', 'Are you sure you want to delete following language:') ?> 
                    <br> <b>&nbsp;<span class="js-lng-name"></span></b>&nbsp;?
                </h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.hostess', 'Close Dialog') ?>
                </button>
                <button id="options-modal-confirm" type="button" class="btn btn-green js-btn-delete-lng">
                    <?= Yii::t('app.hostess', 'Continue') ?>
                </button>
            </div>           
        </div>
    </div>
</div>