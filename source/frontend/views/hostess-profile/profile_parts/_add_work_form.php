<?php

use yii\widgets\ActiveForm;

frontend\components\simpleDatePicker\SimpleDatePickerAsset::register($this);
?>

<?php

$form = ActiveForm::begin(["id" => "add-work-form", "action" => ["/hostess-profile/add-work"]]);

echo '<label>' . Yii::t('app.hostess', 'Start Date') . '</label><br>';
echo $form->field($model, 'start_date')->input("hidden", [ 'id' => 'start_date'])->label(false);
echo '<label>' . Yii::t('app.hostess', 'End Date') . '</label><br>';
echo $form->field($model, 'end_date')->input("hidden", ['id' => 'end_date'])->label(false);
echo $form->field($model, 'event_name')->textInput();
echo $form->field($model, 'job_description')->textInput();
echo $form->field($model, 'position')->textInput();

ActiveForm::end();
