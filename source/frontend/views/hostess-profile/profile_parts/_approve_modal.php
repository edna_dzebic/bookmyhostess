<div class="modal fade" id="approve-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.hostess', 'Profile approval') ?></h3>
            </div>
            <div class="modal-body panel-body">
                <?= Yii::t('app.hostess', 'In order to activate your profile you have to fill in the missing information:') ?>
            </div>
            <div class="table-responsive">
                <?php
                $form = kartik\form\ActiveForm::begin();
                echo $form->errorSummary($model, ['header' => '']);
                $form->end();
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.hostess', 'Close Dialog') ?>
                </button>
            </div>           
        </div>
    </div>
</div>