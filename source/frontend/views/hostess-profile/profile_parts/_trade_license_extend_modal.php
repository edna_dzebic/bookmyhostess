<?php

use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

$startDate = date('d-m-Y');
$endDate = date("d-m-Y", strtotime("+1 month"));
$model = new \common\models\TradeLicenseExtend();
?>
<div class="modal fade trade-license" id="trade-license-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.hostess', 'Postpone trade license upload') ?></h3>
            </div>
            <div class="modal-body panel-body">
                <div>
                    <?= Yii::t('app.hostess', 'You have the option to postpone the upload of the trade licence. However, the time span must not be longer than one month. The document has to be uploaded by the first job otherweise your profile will be automatically deleted. ') ?>
                </div>
                <br>
                <div class="text-center loading-content">
                    <div>
                        <?= Yii::t('app.hostess', 'Please wait...') ?>
                    </div>
                    <br>
                    <div class="loader js-loader"></div>    
                </div>

                <br>

                <?php
                $form = ActiveForm::begin(["id" => "trade-license-form", "action" => ["/hostess-profile/extend-trade-license"]]);
                ?>


                <?php
                echo $form->field($model, 'date')->widget(DatePicker::classname(), [
                    'options' => [
                        'placeholder' => Yii::t('app.hostess', 'Please choose date ...'),
                        'language' => 'en'
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'language' => 'en',
                        'format' => 'dd-mm-yyyy',
                        'startDate' => $startDate,
                        'minDate' => $startDate,
                        'endDate' => $endDate,
                        'maxDate' => $endDate
                    ]
                ]);

                echo $form->field($model, 'reason')->textarea([
                    "placeholder" => Yii::t('app.hostess', 'Please enter reason why do you want to postpone upload of trade license ...'),
                    "rows" => 6
                ]);
                ?>



                <?php
                ActiveForm::end();
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.hostess', 'Close Dialog') ?>
                </button>
                <button id="options-modal-confirm" type="button" class="btn btn-green js-btn-trade-license">
                    <?= Yii::t('app.hostess', 'Save') ?>
                </button>
            </div>           
        </div>
    </div>
</div>