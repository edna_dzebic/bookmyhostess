<div class="col-xs-12 text-left">
    <div class="flash-message message-from-admin">
        <div class=" margin-top-20 alert alert-danger alert-dismissible" role="alert">

            <div class="row">
                <div class="col-xs-9">
                    <h2><?= Yii::t('app.hostess', 'Message from administrator') ?> </h2>
                </div>
                <div class="col-xs-3 text-right">
                    <a class="btn btn-danger" href="<?= Yii::$app->urlManager->createUrl("//hostess-profile/accept-admin-message") ?>">
                        <?= Yii::t('app.hostess', 'Close') ?>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <?= $messageFromAdmin ?>
                </div>
            </div>
        </div>
    </div>
</div>
