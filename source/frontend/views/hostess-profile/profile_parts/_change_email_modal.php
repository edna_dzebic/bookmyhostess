<div class="modal fade" id="change-email-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.hostess', 'Change Email') ?></h3>
            </div>
            <div class="modal-body panel-body">

            </div>
            <div class="modal-footer">

                <div class="row">
                    <div class="col-xs-12 text-center">

                        <?= Yii::t('app.hostess', 'Are you sure you want to change your Email?') ?>
                    </div>
                </div>

                <br>
                <div class="row">

                    <div class="col-lg-12 text-center">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <?= Yii::t('app.hostess', 'Cancel') ?>
                        </button>
                        <button id="options-modal-confirm" type="button" class="btn btn-green  js-btn-change-email">
                            <?= Yii::t('app.hostess', "Continue") ?>
                        </button>
                    </div>
                </div>
            </div>           
        </div>
    </div>
</div>