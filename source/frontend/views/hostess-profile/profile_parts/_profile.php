<?php

use kartik\detail\DetailView;
?>

<div class="row hostess-content">

    <div class="col-xs-12">
        <div class = 'row'>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 images-content  margin-top-20">
                <?= $this->render("_images", ["model" => $model, "userImagesModel" => $data["userImagesModel"]]) ?>
                <br>   
                <?= $this->render("_summary", ["model" => $model]) ?>
                <br>        
                <?= $this->render("_documents", ["model" => $model, "userDocumentsModel" => $data["userDocumentsModel"]]) ?>
            </div>
            <div class = "col-lg-4 col-md-4 col-sm-12 col-xs-12  margin-top-20">

                <div class="row">
                    <div class="col-xs-12">
                        <p class="green-form-title"><?= Yii::t('app.hostess', 'Details') ?></p>
                    </div>     
                </div>

                <div class = "row">


                    <div class="col-xs-12">

                        <?php
                        echo DetailView::widget([
                            'formOptions' => ['options' => ['enctype' => 'multipart/form-data', 'id' => 'basic-profile-form']],
                            'model' => $model,
                            'hAlign' => DetailView::ALIGN_LEFT,
                            'condensed' => true,
                            'hover' => true,
                            'bordered' => false,
                            'striped' => false,
                            'mode' => DetailView::MODE_VIEW,
                            'labelColOptions' => ['style' => 'width: 50%'],
                            'buttonContainer' => ['class' => 'pull-left margin-top-20'],
                            'buttons1' => '{update}',
                            'buttons2' => '{save}',
                            'saveOptions' => [
                                'class' => 'btn btn-standard ',
                                'label' => Yii::t('app.hostess', 'Save')
                            ],
                            'updateOptions' => [
                                'class' => 'btn btn-standard',
                                'label' => Yii::t('app.hostess', 'Edit')
                            ],
                            'panel' => [
                                'heading' => false,
                                'footer' => true,
                                'footerOptions' => [
                                    'template' => '{buttons}'
                                ]
                            ],
                            'attributes' => [
                                [
                                    'attribute' => 'hair_color_id',
                                    'label' => Yii::t('app.hostess', 'Hair Color'),
                                    'value' => isset($model->hairColor->name) == true ? Yii::t('app', $model->hairColor->name) : null,
                                    'type' => DetailView::INPUT_SELECT2,
                                    'widgetOptions' => [
                                        'hideSearch' => true,
                                        'class' => 'small',
                                        'data' => \frontend\models\HairColor::getAllTranslated(),
                                        'options' => [
                                            'placeholder' => Yii::t('app.hostess', 'Select ...')
                                        ],
                                    ],
                                ],
                                [
                                    'attribute' => 'height',
                                    'label' => Yii::t('app.hostess', 'Height'),
                                    'type' => DetailView::INPUT_TEXT,
                                    'fieldConfig' => [
                                        'template' => '{input} cm{error}{hint}'
                                    ],
                                    'options' => [
                                        'class' => 'form-control small inline text-center',
                                    ]
                                ],
                                [
                                    'attribute' => 'weight',
                                    'label' => Yii::t('app.hostess', 'Weight'),
                                    'type' => DetailView::INPUT_TEXT,
                                    'fieldConfig' => [
                                        'template' => '{input} kg{error}{hint}'
                                    ],
                                    'options' => [
                                        'class' => 'form-control small inline text-center',
                                    ]
                                ],
                                [
                                    'attribute' => 'waist_size',
                                    'label' => Yii::t('app.hostess', 'Waist size'),
                                    'type' => DetailView::INPUT_TEXT,
                                    'options' => [
                                        'class' => 'form-control small inline text-center',
                                    ]
                                ],
                                [
                                    'attribute' => 'shoe_size',
                                    'label' => Yii::t('app.hostess', 'Shoe size'),
                                    'type' => DetailView::INPUT_TEXT,
                                    'options' => [
                                        'class' => 'form-control small inline text-center',
                                    ]
                                ],
                                [
                                    'attribute' => 'has_tattoo',
                                    'label' => Yii::t('app.hostess', 'Visible Piercing / Tattoo'),
                                    'format' => 'raw',
                                    'value' => $model->has_tattoo ?
                                            Yii::t('app.hostess', 'Yes') :
                                            Yii::t('app.hostess', 'No'),
                                    'type' => DetailView::INPUT_SWITCH,
                                    'widgetOptions' => [
                                        'pluginOptions' => [
                                            'handleWidth' => 45,
                                            'onText' => Yii::t('app.hostess', 'Yes'),
                                            'offText' => Yii::t('app.hostess', 'No'),
                                        ]
                                    ]
                                ],
                                [
                                    'attribute' => 'gender',
                                    'label' => Yii::t('app.hostess', 'Gender'),
                                    'format' => 'raw',
                                    'value' => Yii::$app->util->isFemale($model->gender) ?
                                            Yii::t('app.hostess', 'Female') :
                                            Yii::t('app.hostess', 'Male'),
                                    'type' => DetailView::INPUT_SWITCH,
                                    'widgetOptions' => [
                                        'pluginOptions' => [
                                            'handleWidth' => 45,
                                            'onText' => Yii::t('app.hostess', 'Male'), // value is 1
                                            'offText' => Yii::t('app.hostess', 'Female'), // value is 0
                                        ]
                                    ]
                                ],
                                [
                                    'attribute' => 'has_car',
                                    'label' => Yii::t('app.hostess', 'Own a car'),
                                    'format' => 'raw',
                                    'value' => $model->has_car ?
                                            Yii::t('app.hostess', 'Yes') :
                                            Yii::t('app.hostess', 'No'),
                                    'type' => DetailView::INPUT_SWITCH,
                                    'widgetOptions' => [
                                        'pluginOptions' => [
                                            'handleWidth' => 45,
                                            'onText' => Yii::t('app.hostess', 'Yes'),
                                            'offText' => Yii::t('app.hostess', 'No'),
                                        ]
                                    ]
                                ],
                                [
                                    'attribute' => 'has_driving_licence',
                                    'label' => Yii::t('app.hostess', 'Driving License'),
                                    'format' => 'raw',
                                    'value' => $model->has_driving_licence ?
                                            Yii::t('app.hostess', 'Yes') :
                                            Yii::t('app.hostess', 'No'),
                                    'type' => DetailView::INPUT_SWITCH,
                                    'widgetOptions' => [
                                        'pluginOptions' => [
                                            'handleWidth' => 45,
                                            'onText' => Yii::t('app.hostess', 'Yes'),
                                            'offText' => Yii::t('app.hostess', 'No'),
                                        ]
                                    ]
                                ],
                                [
                                    'attribute' => 'has_health_certificate',
                                    'label' => Yii::t('app.hostess', 'Health Certificate'),
                                    'format' => 'raw',
                                    'value' => $model->has_health_certificate ?
                                            Yii::t('app.hostess', 'Yes') :
                                            Yii::t('app.hostess', 'No'),
                                    'type' => DetailView::INPUT_SWITCH,
                                    'widgetOptions' => [
                                        'pluginOptions' => [
                                            'handleWidth' => 45,
                                            'onText' => Yii::t('app.hostess', 'Yes'),
                                            'offText' => Yii::t('app.hostess', 'No'),
                                        ]
                                    ]
                                ]
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-top-20">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="green-form-title"><?= Yii::t('app.hostess', 'Statistics') ?></p>
                    </div>     
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?= $this->render("_statistics", ["model" => $model]) ?>
                    </div>     
                </div>
            </div>
        </div>
    </div>
</div>