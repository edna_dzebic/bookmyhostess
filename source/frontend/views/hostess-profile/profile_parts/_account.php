<?php

use yii\helpers\Html;
use kartik\editable\Editable;
use kartik\popover\PopoverX;
use frontend\models\Language;
?>
<div class="hostess-content padding-top-30">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-20">
            <div class="row ">
                <div class="col-xs-12">
                    <div class="row ">
                        <div class="col-xs-12">
                            <p class="green-form-title"><?= Yii::t('app.hostess', 'Details') ?></p>
                        </div>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3 attr-label">
                                    <?= Yii::t('app.hostess', 'Username:'); ?>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-10 col-xs-9 attr-value">
                                    <?= $model->user->username; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3 attr-label">
                                    <?= Yii::t('app.hostess', 'Email:'); ?>
                                </div>
                                <div class="col-lg-11 col-md-10 col-sm-10 col-xs-9 attr-value">
                                    <?= $model->user->email; ?>
                                </div>
                            </div>
                            <hr>
                        </div>                        
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row  margin-top-40">
                        <div class="col-xs-12  green-form-title">
                            <?= Yii::t('app.hostess', 'Change Email'); ?>
                        </div>
                        <div class="col-xs-12 attr-value">
                            <div class="row">
                                <div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
                                    <?= Yii::t('app.hostess', 'Change your email address') ?>
                                </div>
                                <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3 text-center">
                                    <?= Html::a(Yii::t('app.hostess', 'Change Email'), ['hostess-profile/change-email'], ["class" => "js-change-email green-text"]); ?>
                                </div>
                            </div>
                            <hr>
                        </div>                         
                    </div>
                    <div class="row  margin-top-20 ">
                        <div class="col-xs-12  green-form-title">
                            <?= Yii::t('app.hostess', 'System language'); ?>
                        </div>
                        <div class="col-xs-12 attr-value">
                            <div class="row">
                                <div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
                                    <?= Yii::t('app.hostess', 'Select your preferred system language') ?>
                                </div>
                                <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3 text-center">
                                    <?=
                                    Editable::widget([
                                        'id' => 'language-edit',
                                        'name' => 'lang',
                                        'header' => Yii::t('app.hostess', 'Select language'),
                                        'value' => $model->user->lang,
                                        'displayValue' => Yii::t('app.hostess', 'Change language'),
                                        'placement' => PopoverX::ALIGN_TOP_LEFT,
                                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                        'preHeader' => '',
                                        'buttonsTemplate' => '{submit}',
                                        'submitButton' => [
                                            'icon' => Yii::t('app.hostess', 'Save'),
                                            'class' => 'btn btn-green'
                                        ],
                                        'resetButton' => false,
                                        'pluginOptions' => [
                                        ],
                                        'pluginEvents' => [
                                            "editableSuccess" => "function(event, val, form, data) {  $(form)[0].reset();location.reload();  }",
                                        ],
                                        'data' => Language::getPrefferedLanguageList(),
                                        'options' => [
                                            'class' => 'form-control',
                                            'prompt' => Yii::t('app.hostess', 'Select language...')
                                        ]
                                    ])
                                    ?>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row  margin-top-20 ">
                        <div class="col-xs-12  green-form-title">
                            <?= Yii::t('app.hostess', 'Change password'); ?>
                        </div>
                        <div class="col-xs-12 attr-value">
                            <div class="row">
                                <div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
                                    <?= Yii::t('app.hostess', 'Choose a unique password to protect your account') ?>
                                </div>
                                <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3 text-center">
                                    <?= Html::a(Yii::t('app.hostess', 'Change password'), ['change-password'], ["class" => "green-text"]); ?>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row  margin-top-20 ">
                        <div class="col-xs-12  green-form-title">
                            <?= Yii::t('app.hostess', 'Delete profile'); ?>
                        </div>
                        <div class="col-xs-12 attr-value">
                            <div class="row">
                                <div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
                                    <?= Yii::t('app.hostess', 'Close your account if you wish') ?>
                                </div>
                                <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3 text-center">
                                    <?= Html::a(Yii::t('app.hostess', 'Delete profile'), ['delete-me'], ["class" => "green-text", "data-confirm" => \Yii::t('app.hostess', 'Are you sure you want to permanently delete your profile ?')]); ?>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render("_change_email_modal") ?>