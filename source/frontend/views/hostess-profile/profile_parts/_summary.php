<?php

use kartik\detail\DetailView;
?>


<div class="row">
    <div class="col-xs-12">
        <p class="green-form-title"><?= Yii::t('app.hostess', 'About me') ?></p>
    </div>  

    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 text-left about-me">
        <?php
        echo DetailView::widget([
            'model' => $model,
            'hAlign' => DetailView::ALIGN_LEFT,
            'condensed' => false,
            'hover' => false,
            'bordered' => false,
            'striped' => false,
            'mode' => DetailView::MODE_VIEW,
            'buttonContainer' => ['class' => 'pull-left margin-top-20'],
            'buttons1' => '{update}',
            'buttons2' => '{save}',
            'labelColOptions' => ['style' => 'width: 0%; padding : 0;'],
            'valueColOptions' => ['style' => 'width: 100%; padding : 0;', 'class' => 'summary-col'],
            'saveOptions' => [
                'class' => 'btn btn-standard ',
                'label' => Yii::t('app.hostess', 'Save')
            ],
            'updateOptions' => [
                'class' => 'btn btn-standard',
                'label' => Yii::t('app.hostess', 'Edit')
            ],
            'panel' => [
                'footer' => true,
                'footerOptions' => [
                    'template' => '{buttons}'
                ],
                'heading' => false,
            ],
            'attributes' => [

                [
                    'attribute' => 'summary',
                    'label' => false,
                    'type' => DetailView::INPUT_TEXTAREA,
                    'options' => ["rows" => 6]
                ]
            ]
        ]);
        ?>
    </div>
</div>
