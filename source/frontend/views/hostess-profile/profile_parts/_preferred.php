<?php

use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
?>

<div class=" hostess-content padding-top-30">
    <div class="row profile-table">
        <div class="col-xs-12">
            <p class="green-form-title"><?= Yii::t('app.hostess', 'Preferred cities') ?></p>
        </div>
        <?php $cityForm = kartik\form\ActiveForm::begin(); ?>
        <div class="col-lg-12">
            <?=
            Select2::widget([
                'name' => 'HostessPreferredCity[]',
                'data' => $data["cities"],
                'value' => ArrayHelper::map($model->preferredCities, 'id', 'city_id'),
                'size' => Select2::LARGE,
                'showToggleAll' => false,
                'options' => ['placeholder' => Yii::t('app.hostess', 'Start typing to add'), 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
           </div>


    <div class="row profile-table margin-top-20">
        <div class="col-xs-12">
            <p class="green-form-title"><?= Yii::t('app.hostess', 'Preferred jobs') ?></p>
        </div>
     
        <div class="col-xs-12">
            <?=
            Select2::widget([
                'name' => 'HostessPreferredJob[]',
                'data' => $data["eventJobs"],
                'value' => ArrayHelper::map($model->preferredJobs, 'id', 'event_job_id'),
                'size' => Select2::LARGE,
                'showToggleAll' => false,
                'options' => ['placeholder' => Yii::t('app.hostess', 'Click to add or remove your preferred jobs'), 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-xs-12 margin-top-20">
            <div class="pull-left kv-editable" id="hostesspastwork-start_date-cont">
                <input type="submit"
                       data-placement="left left-top"
                       class="btn btn-standard btn-small kv-editable-value"
                       id="preferred_jobs_save"
                       value="<?= Yii::t('app.hostess', 'save') ?>">
            </div>
        </div>
        <?php kartik\form\ActiveForm::end(); ?>
    </div>    
</div>
