<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
?>

<div>
    <?=
    Tabs::widget([
        'navType' => 'nav-pills green-nav-pills profile-submenu',
        'items' => [
            [
                'label' => Yii::t('app.hostess', 'Basic details'),
                'url' => ["//hostess-profile/index"],
                'active' => $active_tab == "_profile",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.hostess', 'Languages'),
                'url' => ["//hostess-profile/language"],
                'active' => $active_tab == "_language",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.hostess', 'Education'),
                'url' => ["//hostess-profile/education"],
                'active' => $active_tab == "_education",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.hostess', 'Work experience'),
                'url' => ["//hostess-profile/work-experience"],
                'active' => $active_tab == "_experience",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.hostess', 'Preferred jobs & cities'),
                'url' => ["//hostess-profile/preferred"],
                'active' => $active_tab == "_preferred",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.hostess', 'Account'),
                'url' => ["//hostess-profile/account"],
                'active' => $active_tab == "_account",
                'headerOptions' => ["class" => 'col-lg-2 col-md-4 col-sm-4 col-xs-6']
            ],
        ],
    ]);
    ?>
</div>