<?php
\frontend\assets\HostessProfileAsset::register($this);

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app.hostess', 'Answered Requests');

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'header' => Yii::t('app.hostess', 'Event'),
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($model, $index, $widget)
{
    return $model->event->name;
},
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.hostess', 'Period'),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDate($model->date_from) . "<br>-<br>" . Yii::$app->util->formatDate($model->date_to);
        },
    ],
    [
        'attribute' => 'days',
        'header' => Yii::t('app.hostess', "Days"),
        'value' => function ($model, $index, $widget)
        {
            return $model->getTotalBookingDays();
        },
    ],
    [
        'attribute' => 'daily_rate',
        'header' => Yii::t('app.hostess', "Daily rate"),
        'value' => function($model)
        {
            return Yii::$app->util->formatEventPrice($model->event, $model->price);
        }
    ],
    [
        'attribute' => 'payment_type',
        'header' => Yii::t('app.hostess', "Payment of the daily rate"),
        'value' => function($model)
        {
            return $model->getPaymentTypeLabel();
        }
    ],
    [
        'attribute' => 'dress_code',
        'header' => Yii::t('app.hostess', "Outfit"),
        'value' => function ($model, $index, $widget)
        {
            return $model->dressCode->translatedName;
        },
    ],
    [
        'attribute' => 'job',
        'header' => Yii::t('app.hostess', "Job"),
        'value' => function ($model, $index, $widget)
        {
            return $model->jobName;
        },
    ],
    [
        'attribute' => 'date_created',
        'header' => Yii::t('app.hostess', "Request sent"),
        'value' => function ($model, $index, $widget)
        {
            if ($model->date_created != '0000-00-00 00:00:00')
            {
                return Yii::$app->util->formatDateTime($model->date_created);
            } else
            {
                return '';
            }
        },
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.hostess', 'Status'),
        'value' => function ($data)
        {

            if ($data->isAccepted || $data->isPaymentPending)
            {
                return '<div class="text-center full-width lbl label-accepted">' . Yii::t('app.hostess', 'Accepted') . '</div>';
            }
            if ($data->isExWithdraw)
            {
                return '<div class="text-center full-width lbl label-accepted">' . Yii::t('app.hostess', 'Withdrawn') . '</div>';
            }
            if ($data->isExWithdrawAfterAccept)
            {
                return '<div class="text-center  full-width lbl label-accepted">' . Yii::t('app.hostess', 'Withdrawn') . '</div>';
            }
            if ($data->isHostessWithdrawAfterAccept)
            {
                return '<div class="text-center  full-width lbl label-rejected">' . Yii::t('app.hostess', 'Withdrawn') . '</div>';
            }
            if ($data->isRejected)
            {
                return '<div class="text-center full-width lbl label-rejected">' . Yii::t('app.hostess', 'Rejected') . '</div>';
            }

            return '';
        },
    ],
    [
        'class' => '\yii\grid\ActionColumn',
        'template' => '{delete}',
        'header' => Yii::t('app.hostess', 'Options'),
        'buttons' => [
            'delete' => function ($url, $model)
            {

                if ($model->completed == 0)
                {
                    if ($model->isAccepted)
                    {
                        return Html::a(Yii::t('app.hostess', 'Withdraw'), \Yii::$app->urlManager->createUrl(["//hostess-profile/withdraw-confirmation", "id" => $model->id]), [
                                    'data-confirm' => Yii::t('app.hostess', 'Do you really want to withdraw confirmation?'),
                                    'title' => \Yii::t('app.hostess', 'Withdraw confirmation'),
                                    'class' => 'btn btn-default full-width label-rejected js-withdraw',
                                    'data-id' => $model->id
                        ]);
                    }

                    if ($model->isNew)
                    {
                        return '' .
                                Html::a(Html::encode(\Yii::t('app.hostess', 'Accept')), \Yii::$app->urlManager->createUrl(['//hostess-profile/accept-request', "id" => $model->id]), ["class" => "btn btn-green full-width"]) .
                                '<br>' .
                                Html::a(Html::encode(\Yii::t('app.hostess', 'Reject')), \Yii::$app->urlManager->createUrl(['//hostess-profile/reject-request', "id" => $model->id]), ["class" => "btn btn-default full-width js-reject-request", 'data-id' => $model->id, "style" => "margin-top:5px;"]) .
                                '';
                    }
                }



                return '';
            }
                ]
            ]
        ];
        ?>

        <?= $this->render("profile_parts/_profile_menu", ["model" => $profileModel, "active_tab" => "requests"]); ?>
        <div class="row hostess-tab-content">
            <div class="col-xs-12">
                <?=
                GridView::widget(
                        [
                            'columns' => $columns,
                            'dataProvider' => $dataProvider,
                            'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                            'options' => [
                                'class' => 'grid-view table-responsive',
                            ],
                            'tableOptions' => [
                                'class' => 'table table-grey table-bordered',
                            ],
                            'layout' => "{items}",
                        ]
                );
                ?>
            </div>
            <div class="col-xs-12 text-right margin-top-20">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'options' => ['class' => 'pagination'],
                    'maxButtonCount' => 5,
                    'nextPageLabel' => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'prevPageLabel' => '<'
                ]);
                ?>
            </div>  

        </div>
        <?= $this->render("_withdraw_confirmation_modal") ?>


        <?= $this->render("_reject_request_modal") ?>