<?php
\frontend\assets\HostessProfileAsset::register($this);

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app.hostess', 'Open Requests');

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'event.name',
        'header' => Yii::t('app.hostess', "Event"),
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($data)
{
    return Html::a(
                    Html::encode($data["event"]["name"]), \Yii::$app->urlManager->createUrl(['//event/view', "id" => $data["event"]["id"]]), ["class" => "green-link bold"]
    );
}
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.hostess', 'Period'),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDate($model->date_from) . "<br>-<br>" . Yii::$app->util->formatDate($model->date_to);
        },
    ],
    [
        'attribute' => 'days',
        'header' => Yii::t('app.hostess', "Days"),
        'value' => function ($model, $index, $widget)
        {
            return $model->getTotalBookingDays();
        },
    ],
    [
        'attribute' => 'daily_rate',
        'header' => Yii::t('app.hostess', "Daily rate"),
        'value' => function($model)
        {
            return Yii::$app->util->formatEventPrice($model->event, $model->price);
        }
    ],
    [
        'attribute' => 'payment_type',
        'header' => Yii::t('app.hostess', "Payment of the daily rate"),
        'value' => function($model)
        {
            return $model->getPaymentTypeLabel();
        }
    ],
    [
        'attribute' => 'dress_code',
        'header' => Yii::t('app.hostess', "Outfit"),
        'value' => function ($model, $index, $widget)
        {
            return $model->dressCode->translatedName;
        },
    ],
    [
        'attribute' => 'job',
        'header' => Yii::t('app.hostess', "Job"),
        'value' => function ($model, $index, $widget)
        {
            return $model->jobName;
        },
    ],
    [
        'attribute' => 'date_created',
        'header' => Yii::t('app.hostess', "Request sent"),
        'value' => function ($model, $index, $widget)
        {
            if ($model->date_created != '0000-00-00 00:00:00')
            {
                return Yii::$app->util->formatDateTime($model->date_created);
            } else
            {
                return '';
            }
        },
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.hostess', 'Options'),
        'value' => function ($data)
        {
            return '' .
                    Html::a(Html::encode(\Yii::t('app.hostess', 'Accept')), \Yii::$app->urlManager->createUrl(['//hostess-profile/accept-request', "id" => $data["id"]]), ["class" => "btn btn-green"]) .
                    ' ' .
                    Html::a(Html::encode(\Yii::t('app.hostess', 'Reject')), \Yii::$app->urlManager->createUrl(['//hostess-profile/reject-request', "id" => $data["id"]]), ["class" => "btn btn-default js-reject-request", 'data-id' => $data["id"]]) .
                    '';
        },
            ],
        ];
        ?>

        <?= $this->render("profile_parts/_profile_menu", ["model" => $profileModel, "active_tab" => "current_request"]); ?>


        <div class="row hostess-tab-content">
            <div class="col-xs-12">
                <?=
                GridView::widget(
                        [
                            'columns' => $columns,
                            'dataProvider' => $dataProvider,
                            'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                            'options' => [
                                'class' => 'grid-view table-responsive',
                            ],
                            'tableOptions' => [
                                'class' => 'table  table-grey table-bordered',
                            ],
                            'layout' => "{items}",
                        ]
                );
                ?>
            </div>
            <div class="col-xs-12 text-right margin-top-20">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'options' => ['class' => 'pagination'],
                    'maxButtonCount' => 5,
                    'nextPageLabel' => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'prevPageLabel' => '<'
                ]);
                ?>
            </div>  

        </div>


        <?= $this->render("_reject_request_modal") ?>