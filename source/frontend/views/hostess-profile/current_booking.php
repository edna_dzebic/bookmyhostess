<?php

use yii\helpers\Html;
use yii\grid\GridView;

\frontend\assets\HostessProfileAsset::register($this);

$this->title = Yii::t('app.hostess', 'Current Bookings');

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'exhibitor',
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left'],
        'header' => Yii::t('app.hostess', "Client"),
        'value' => function ($model, $index, $widget)
{
    return $model->userRequested->fullName . "<br>" . $model->userRequested->email . "<br>" . $model->userRequested->phone_number . "<br>" .
            Html::a($model->userRequested->exhibitorProfile->website, \yii\helpers\Url::to($model->userRequested->exhibitorProfile->website), ['target' => "_blank", "class" => 'green-link']);
},
    ],
    [
        'attribute' => 'event.name',
        'header' => Yii::t('app.hostess', "Event"),
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($data)
{
    return Html::a(
                    Html::encode($data["event"]["name"]), \Yii::$app->urlManager->createUrl(['//event/view', "id" => $data["event"]["id"]]), ["class" => "green-link bold"]
    );
}
    ],
    [
        'header' => Yii::t('app.hostess', 'Period'),
        'format' => 'raw',
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDate($model->date_from) . "<br>-<br>" . Yii::$app->util->formatDate($model->date_to);
        },
    ],
    [
        'attribute' => 'days',
        'header' => Yii::t('app.hostess', "Days"),
        'value' => function ($model, $index, $widget)
        {
            return $model->getTotalBookingDays();
        }
    ],
    [
        'attribute' => 'daily_rate',
        'header' => Yii::t('app.hostess', "Daily rate"),
        'value' => function($model)
        {
            return Yii::$app->util->formatEventPrice($model->event, $model->price);
        }
    ],
    [
        'attribute' => 'payment_type',
        'header' => Yii::t('app.hostess', "Payment of the daily rate"),
        'value' => function($model)
        {
            return $model->getPaymentTypeLabel();
        }
    ],
    [
        'attribute' => 'dress_code',
        'header' => Yii::t('app.hostess', "Outfit"),
        'value' => function ($model, $index, $widget)
        {
            return $model->dressCode->translatedName;
        },
    ],
    [
        'attribute' => 'job',
        'header' => Yii::t('app.hostess', "Job"),
        'value' => function ($model, $index, $widget)
        {
            return $model->jobName;
        },
    ],
];
?>

        <?= $this->render("profile_parts/_profile_menu", ["model" => $profileModel, "active_tab" => "current_booking"]); ?>
<div class="row hostess-tab-content">
    <div class="col-xs-12">
        <?=
        GridView::widget(
                [
                    'columns' => $columns,
                    'dataProvider' => $dataProvider,
                    'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                    'options' => [
                        'class' => 'grid-view table-responsive',
                    ],
                    'tableOptions' => [
                        'class' => 'table  table-grey table-bordered',
                    ],
                    'layout' => "{items}",
                ]
        );
        ?>
    </div>
    <div class="col-xs-12 text-right margin-top-20">
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'options' => ['class' => 'pagination'],
            'maxButtonCount' => 5,
            'nextPageLabel' => '>',
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
            'prevPageLabel' => '<'
        ]);
        ?>
    </div>  
</div>
