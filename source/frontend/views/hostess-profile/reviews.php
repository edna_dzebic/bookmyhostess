<?php
use yii\widgets\ListView;

\frontend\assets\HostessProfileAsset::register($this);


$this->title = Yii::t('app.hostess', 'Reviews');

?>

<?= $this->render("profile_parts/_profile_menu", ["model" => $profileModel, "active_tab" => "reviews"]); ?>
<div class="row hostess-tab-content">

    <div class = "col-lg-12">

        <div class="row">
            <div class="col-xs-12">
                <?php
                echo ListView::widget([
                    'options' => [],
                    'emptyText' => '<div class="text-center">' . Yii::t('app.hostess', 'No ratings available. Customer reviews increase the reliability of your profile up to 10%. You have the possibility to request feedback from your customers in the profile. Go to "Past Bookings" and then "Ask for Review".') . '</div>',
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'margin-top-20 review-item col-lg-6 col-md-6 col-sm-6 col-xs-12'],
                    'layout' => '<div class="row">{items}</div>',
                    'itemView' => function ($model, $key, $index, $widget)
            {

                return $this->render('_review_item', ['model' => $model, 'widget' => $widget]);
            },
                ]);
                ?>  
            </div>
        </div>

        <div class="row text-right margin-top-20">
            <div class="col-xs-12">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'options' => ['class' => 'pagination'],
                    'maxButtonCount' => 5,
                    'nextPageLabel' => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'prevPageLabel' => '<'
                ]);
                ?>
            </div>    
        </div>

    </div>
</div>
