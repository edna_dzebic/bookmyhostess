<?php

use yii\widgets\ActiveForm;
?>

<div>
    <?= Yii::t('app.hostess', 'Request details:') ?>
</div>
<div class="table-responsive">
    <table class="table  table-grey table-bordered text-center">
        <thead>
            <tr class="text-center" style="text-align:center;">
                <th  style="text-align:center;"><?= Yii::t('app.hostess', "Event") ?></th>
                <th  style="text-align:center;"><?= Yii::t('app.hostess', 'Period') ?></th>
                <th  style="text-align:center;"><?= Yii::t('app.hostess', "Days") ?></th>
                <th  style="text-align:center;"><?= Yii::t('app.hostess', "Daily rate") ?></th>
                <th  style="text-align:center;"><?= Yii::t('app.hostess', "Outfit") ?></th>
                <th  style="text-align:center;"><?= Yii::t('app.hostess', "Job") ?></th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?= $model->event->name ?>
                </td>
                <td>
                    <?= Yii::$app->util->formatDate($model->date_from) . "-" . Yii::$app->util->formatDate($model->date_to) ?>
                </td>
                <td>
                    <?= $model->getTotalBookingDays() ?>
                </td>
                <td>
                    <?= Yii::$app->util->formatPrice($model->price) . " " . Yii::t('app.hostess', 'EUR') ?>
                </td>
                <td>
                    <?= $model->dressCode->translatedName ?>
                </td>
                <td>
                    <?= $model->jobName ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="text-center loading-content">
    <div>
        <?= Yii::t('app.hostess', 'Please wait...') ?>
    </div>
    <br>
    <div class="loader js-loader"></div>    
</div>


<?php
$form = ActiveForm::begin([
            "id" => "withdraw-confirmation-form",
            "action" => ["/hostess-profile/withdraw-confirmation", "id" => $model->id]]);


echo $form->field($model, 'hostess_withdraw_reason')
        ->dropDownList($model->getRejectionReasons(), ["prompt" => Yii::t('app.hostess', 'Please select reason')]);

ActiveForm::end();
