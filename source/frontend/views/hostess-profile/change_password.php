<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

\frontend\assets\HostessProfileAsset::register($this);

$this->title = Yii::t('app.hostess', 'Change password');
?>
<?= $this->render("profile_parts/_profile_menu", ["model" => $profileModel, "active_tab" => "profile"]); ?>

<div class="hostess-tab-content">
    <div class="row">

        <div class="col-xs-12">
            <?= $this->render("profile_parts/_profile_sub_menu", ["active_tab" => "_account"]); ?>

        </div>
    </div>
    <div class="row hostess-content  margin-top-30">
        <div class="col-xs-12">
            <p class="green-form-title"><?= Yii::t('app.hostess', 'Enter your new password:'); ?></p>
        </div>
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'confirm_password')->passwordInput() ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app.hostess', 'Save'), ['class' => 'btn btn-standard']) ?>
                <?= Html::a(Yii::t('app.hostess', 'Back'), ["/hostess-profile/account"], ['class' => 'btn btn-default']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

