<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

?>

<div>
    <?=
    Tabs::widget([
        'navType' => 'nav-pills green-nav-pills profile-submenu',
        'items' => [
            [
                'label' => Yii::t('app.exhibitor', 'Basic details'),
                'url' => ["//exhibitor/profile"],
                'active' => $active_tab == "profile",
                'headerOptions' => ["class" => 'col-lg-4 col-md-4 col-sm-4 col-xs-4']
            ],
            [
                'label' => Yii::t('app.exhibitor', 'Company'),
                'url' => ["//exhibitor/company"],
                'active' => $active_tab == "company",
                'headerOptions' => ["class" => 'col-lg-4 col-md-4 col-sm-4 col-xs-4']
            ],
            [
                'label' => Yii::t('app.exhibitor', 'Account'),
                'url' => ["//exhibitor/account"],
                'active' => $active_tab == "account",
                'headerOptions' => ["class" => 'col-lg-4 col-md-4 col-sm-4 col-xs-4']
            ]
        ],
    ]);
    ?>
</div>