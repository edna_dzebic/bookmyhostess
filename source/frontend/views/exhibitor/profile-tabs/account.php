<?php

use yii\helpers\Html;
use kartik\editable\Editable;
use kartik\popover\PopoverX;
use frontend\models\Language;
?>
<div class="profile-tables exhibitor-content">
    
    <?= $this->render("//partials/_flash") ?>
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-20">
            <div class="row ">
                <div class="col-xs-12">
                    <p class="green-form-title"><?= Yii::t('app.exhibitor', 'Details') ?></p>
                </div>

                <?php if (!empty($model->user->username)): ?>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3 attr-label">
                                <?= Yii::t('app.exhibitor', 'Username:'); ?>
                            </div>
                            <div class="col-lg-11 col-md-10 col-sm-10 col-xs-9 attr-value">
                                <?= $model->user->username; ?>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>

                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3 attr-label">
                            <?= Yii::t('app.exhibitor', 'Email:'); ?>
                        </div>
                        <div class="col-lg-11 col-md-10 col-sm-10 col-xs-9 attr-value">
                            <?= $model->user->email; ?>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="col-xs-12">

                    <div class="row  margin-top-20">
                        <div class="col-xs-12  green-form-title">
                            <?= Yii::t('app.exhibitor', 'Change Email'); ?>
                        </div>
                        <div class="col-xs-12 attr-value">
                            <div class="row">
                                <div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
                                    <?= Yii::t('app.exhibitor', 'Change Your email address') ?>
                                </div>
                                <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3 text-center">
                                    <?= Html::a(Yii::t('app.exhibitor', 'Change Email'), ['exhibitor/change-email'], ["class" => "js-change-email green-text"]); ?>
                                </div>
                            </div>                            
                            <hr>
                        </div>
                    </div>
                    <div class="row  margin-top-20">
                        <div class="col-xs-12  green-form-title">
                            <?= Yii::t('app.exhibitor', 'System language'); ?>
                        </div>
                        <div class="col-xs-12 attr-value">
                            <div class="row">
                                <div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
                                    <?= Yii::t('app.exhibitor', 'Select Your preferred system language') ?>
                                </div>
                                <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3 text-center">
                                    <?=
                                    Editable::widget([
                                        'id' => 'language-edit',
                                        'name' => 'lang',
                                        'header' => Yii::t('app.exhibitor', 'Select language'),
                                        'value' => $model->user->lang,
                                        'displayValue' => Yii::t('app.exhibitor', 'Change language'),
                                        'placement' => PopoverX::ALIGN_TOP_LEFT,
                                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                        'preHeader' => '',
                                        'buttonsTemplate' => '{submit}',
                                        'submitButton' => [
                                            'icon' => Yii::t('app.exhibitor', 'Save'),
                                            'class' => 'btn btn-green'
                                        ],
                                        'resetButton' => false,
                                        'pluginOptions' => [
                                        ],
                                        'pluginEvents' => [
                                            "editableSuccess" => "function(event, val, form, data) {  $(form)[0].reset();location.reload();  }",
                                        ],
                                        'data' => Language::getPrefferedLanguageList(),
                                        'options' => [
                                            'class' => 'form-control',
                                            'prompt' => Yii::t('app.exhibitor', 'Select language...')
                                        ]
                                    ])
                                    ?>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row  margin-top-20">
                        <div class="col-xs-12  green-form-title">
                            <?= Yii::t('app.exhibitor', 'Change password'); ?>
                        </div>
                        <div class="col-xs-12 attr-value">
                            <div class="row">
                                <div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
                                    <?= Yii::t('app.exhibitor', 'Choose a unique password to protect Your account') ?>
                                </div>
                                <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3 text-center">
                                    <?= Html::a(Yii::t('app.exhibitor', 'Change password'), ['change-password'], ["class" => "green-text"]); ?>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row  margin-top-20">
                        <div class="col-xs-12  green-form-title">
                            <?= Yii::t('app.exhibitor', 'Delete profile'); ?>
                        </div>
                        <div class="col-xs-12 attr-value">
                            <div class="row">
                                <div class="col-xs-12 col-lg-4 col-md-6 col-sm-6">
                                    <?= Yii::t('app.exhibitor', 'Close Your account if You wish') ?>
                                </div>
                                <div class="col-xs-12 col-lg-3 col-md-3 col-sm-3 text-center">
                                    <?= Html::a(Yii::t('app.exhibitor', 'Delete profile'), ['delete-me'], ["class" => "green-text", "data-confirm" => \Yii::t('app.exhibitor', 'Are You sure You want to permanently delete Your profile ?')]); ?>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render("_change_email_modal") ?>