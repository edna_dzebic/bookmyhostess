<?php

use kartik\detail\DetailView;
use frontend\models\Gender;
?>

<div class="row profile-tables exhibitor-content">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">

                <div class='row'>
                    <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6">
                        <?php
                        echo DetailView::widget([
                            'model' => $model->user,
                            'hAlign' => DetailView::ALIGN_LEFT,
                            'condensed' => true,
                            'hover' => true,
                            'bordered' => false,
                            'striped' => false,
                            'mode' => DetailView::MODE_VIEW,
                            'buttonContainer' => ['class' => 'pull-left margin-top-20'],
                            'buttons1' => '{update}',
                            'buttons2' => '{save}',
                            'labelColOptions' => ['style' => 'width: 30%'],
                            'saveOptions' => [
                                'class' => 'btn btn-standard',
                                'label' => Yii::t('app.exhibitor', 'Save')
                            ],
                            'updateOptions' => [
                                'class' => 'btn btn-standard  ',
                                'label' => Yii::t('app.exhibitor', 'Edit')],
                            'panel' => [
                                'heading' => false,
                                'footer' => true,
                                'footerOptions' => [
                                    'template' => '{buttons}'
                                ]
                            ],
                            'attributes' => [
                                [
                                    'attribute' => 'gender',
                                    'label' => Yii::t('app.exhibitor', 'Title'),
                                    'value' => Yii::$app->util->isFemale($model->user->gender) ? Yii::t('app.exhibitor', 'Ms') : Yii::t('app.exhibitor', 'Mr'),
                                    'type' => DetailView::INPUT_SELECT2,
                                    'widgetOptions' => [
                                        'data' => Gender::getList(),
                                        'options' => ['placeholder' => Yii::t('app.exhibitor', '- Please select title -')],
                                    ],
                                ],
                                'first_name',
                                'last_name',
                                'phone_number',
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>