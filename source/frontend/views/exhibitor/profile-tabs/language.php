<?php

use kartik\detail\DetailView;
?>


<div class="row profile-tables exhibitor-content">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6">
                <?php
                echo DetailView::widget([
                    'model' => $model,
                    'hAlign' => DetailView::ALIGN_LEFT,
                    'condensed' => true,
                    'hover' => true,
                    'bordered' => false,
                    'striped' => false,
                    'mode' => DetailView::MODE_VIEW,
                    'buttonContainer' => ['class' => 'pull-left margin-top-20'],
                    'notSetIfEmpty' => true,
                    'buttons1' => '{update}',
                    'buttons2' => '{save}',
                    'labelColOptions' => ['style' => 'width: 30%'],
                    'saveOptions' => [
                        'class' => 'btn btn-standard',
                        'label' => Yii::t('app.exhibitor', 'Save')
                    ],
                    'updateOptions' => [
                        'class' => 'btn btn-standard',
                        'label' => Yii::t('app.exhibitor', 'Edit')
                    ],
                    'panel' => [
                        'heading' => false,
                        'footer' => true,
                        'footerOptions' => [
                            'template' => '{buttons}'
                        ]
                    ],
                    'attributes' => [
                        [
                            'attribute' => 'lang',
                            'label' => Yii::t('app.exhibitor', 'Language'),
                            'value' => $model->getLangValue(),
                            'type' => DetailView::INPUT_SELECT2,
//                            'options' => ['style' => 'width:45%'],
                            'widgetOptions' => [
                                'data' => Yii::$app->params["user_languages"],
                                'options' => ['placeholder' => Yii::t('app.exhibitor', 'Select preferred language')],
                            ],
                        ],
                    ]
                ]);
                ?>
            </div>
        </div>

    </div>
</div>