<?php

use kartik\detail\DetailView;

$mode = DetailView::MODE_VIEW;

$isEdit = isset($data["edit"]) && $data["edit"] == 1 ? true : false;

if ($isEdit)
{
    $mode = DetailView::MODE_EDIT;
}
?>

<?php if ($isEdit) : ?>
    <div class="row profile-tables exhibitor-content">
        <div class="col-lg-12">
            <p><?= Yii::t('app.exhibitor', 'Please edit Your invoice details using form below:') ?></p>
        </div>
    </div>
<?php endif; ?>

<div class="row profile-tables exhibitor-content">
    <div class="col-lg-12">

        <div class="row">
            <div class="col-lg-12">
                <div class='row'>
                    <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6">
                        <?php
                        echo DetailView::widget([
                            'model' => $model,
                            'hAlign' => DetailView::ALIGN_LEFT,
                            'condensed' => true,
                            'hover' => true,
                            'bordered' => false,
                            'striped' => false,
                            'mode' => $mode,
                            'buttonContainer' => ['class' => 'pull-left margin-top-20'],
                            'buttons1' => '{update}',
                            'buttons2' => '{save}',
                            'labelColOptions' => ['style' => 'width: 30%'],
                            'saveOptions' => [
                                'class' => 'btn btn-standard',
                                'label' => Yii::t('app.exhibitor', 'Save')
                            ],
                            'updateOptions' => [
                                'class' => 'btn btn-standard',
                                'label' => Yii::t('app.exhibitor', 'Edit')
                            ],
                            'panel' => [
                                'heading' => false,
                                'footer' => true,
                                'footerOptions' => [
                                    'template' => '{buttons}'
                                ]
                            ],
                            'attributes' => [
                                'company_name',
                                'website',
                                'tax_number',
                                'home_number',
                                'street',
                                'postal_code',
                                'city',
                                [
                                    'attribute' => 'country_id',
                                    'label' => Yii::t('app.exhibitor', 'Country'),
                                    'value' => is_object($model->country) ? Yii::t('app', $model->country->name) : '',
                                    'type' => DetailView::INPUT_SELECT2,
                                    'widgetOptions' => [
                                        'data' => \common\models\Country::getListForDropdown(),
                                        'options' => ['placeholder' => Yii::t('app.exhibitor', '- Please select country -')],
                                    ],
                                ],
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>