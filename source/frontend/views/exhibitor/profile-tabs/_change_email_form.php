<?php

use yii\widgets\ActiveForm;
?>

<div>
    <?= Yii::t('app.exhibitor', 'You are about to change Your email. <br> Your current email is <b>{oldEmail}</b>.', ["oldEmail" => $oldEmail]) ?>
</div>

<br>
<div class="text-center loading-content">
    <div>
        <?= Yii::t('app.exhibitor', 'Please wait...') ?>
    </div>
    <br>
    <div class="loader js-loader"></div>    
</div>

<br>

<?php
$form = ActiveForm::begin([
            "id" => "change-email-form",
            "action" => ["/exhibitor/change-email"]]);

echo '<label>' . Yii::t('app.exhibitor', 'Please enter the new Email address:') . '</label><br>';
echo $form->field($model, 'email')->input("text")->label(false);

ActiveForm::end();
