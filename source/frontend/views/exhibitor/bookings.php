<?php

\frontend\assets\EventRequestAsset::register($this);
\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Bookings');
?>

<?= $this->render("tabs/_exhibitor_profile_menu", ["active_tab" => 'bookings']); ?>

<?= $this->render("tabs/bookings", ["model" => $model, "dataProvider" => $dataProvider]); ?>


<?= $this->render("_withdraw_request_modal") ?>