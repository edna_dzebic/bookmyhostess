<?php

\frontend\assets\EventRequestAsset::register($this);
\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Reviews');
?>

<?= $this->render("tabs/_exhibitor_profile_menu", ["active_tab" => 'reviews']); ?>

<?= $this->render("tabs/reviews", ["model" => $model, "dataProvider" => $dataProvider]); ?>
