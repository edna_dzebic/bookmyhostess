<?php

use yii\widgets\ActiveForm;
?>

<div>
    <?= Yii::t('app.exhibitor', 'Request details:') ?>
</div>
<div class="table-responsive">
    <table class="table  table-grey table-bordered text-center">
        <thead>
            <tr class="text-center" style="text-align:center;">
                <th  style="text-align:center;"><?= Yii::t("app", "Event") ?></th>
                <th  style="text-align:center;"><?= Yii::t('app.exhibitor', 'Period') ?></th>
                <th  style="text-align:center;"><?= Yii::t("app", "Days") ?></th>
                <th  style="text-align:center;"><?= Yii::t("app", "Daily rate") ?></th>
                <th  style="text-align:center;"><?= Yii::t("app", "Outfit") ?></th>
                <th  style="text-align:center;"><?= Yii::t("app", "Job") ?></th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?= $model->event->name ?>
                </td>
                <td>
                    <?= Yii::$app->util->formatDate($model->date_from) . "-" . Yii::$app->util->formatDate($model->date_to) ?>
                </td>
                <td>
                    <?= $model->getTotalBookingDays() ?>
                </td>
                <td>
                    <?= Yii::$app->util->formatPrice($model->price) . " " . Yii::t('app.exhibitor', 'EUR') ?>
                </td>
                <td>
                    <?= $model->dressCode->translatedName ?>
                </td>
                <td>
                    <?= $model->jobName ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="text-center loading-content">
    <div>
        <?= Yii::t('app.exhibitor', 'Please wait...') ?>
    </div>
    <br>
    <div class="loader js-loader"></div>    
</div>


<?php
$form = ActiveForm::begin([
            "id" => "withdraw-request-form",
            "action" => ["/exhibitor/withdraw-request", "id" => $model->id]]);


echo $form->field($model, 'ex_withdraw_reason')
        ->dropDownList($model->getExhibitorWithdrawReasons(), ["prompt" => Yii::t('app.exhibitor', 'Please select reason')])->label(Yii::t('app.exhibitor', 'Withdraw Reason'));

ActiveForm::end();
