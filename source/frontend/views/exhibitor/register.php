<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Language;
use frontend\models\Gender;
use frontend\models\ExhibitorProfile;
use kartik\checkbox\CheckboxX;

\frontend\assets\RegisterAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Free exhibitor registration');
?>

<!--REGISTER BEGIN-->
<div class="site-login">
    <div class="row">
        <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="green-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-title">
                    <?= Yii::t('app.exhibitor', 'FREE REGISTRATION') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-subtitle">
                    <?= Yii::t('app.exhibitor', 'Register for free as an exhibitor, to be able to see profiles and book selected persons') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php $form = ActiveForm::begin(['id' => 'register-form']); ?>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <?= $form->field($model, 'gender')->dropDownList(Gender::getList(), ["prompt" => \Yii::t('app.exhibitor', 'Title*')])->label(false) ?>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <?= $form->field($model, 'lang')->dropDownList(Language::getPrefferedLanguageList(), ["prompt" => \Yii::t('app.exhibitor', 'Preferred language*')])->label(false) ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'staff_demand_type')->dropDownList(ExhibitorProfile::getBookingStaffDemandTypesForDropdown(), ["prompt" => \Yii::t('app.exhibitor', 'Staff demand*')])->label(false) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'first_name')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.exhibitor', 'First name*')])->label(false) ?>
                        </div>
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'last_name')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.exhibitor', 'Last name*')])->label(false) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => 50, 'placeholder' => Yii::t('app.exhibitor', 'Email*')])->label(false) ?>
                        </div>
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <?= $form->field($model, 'password', ["template" => '<div class="password js-password">{input} <span class="glyphicon glyphicon-eye-close js-show-password"></span> {error} </div>'])->passwordInput(['maxlength' => 50, 'placeholder' => Yii::t('app.exhibitor', 'Password*')])->label(false) ?>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <?= $form->field($model, 'terms', ['template' => '<div class="col-xs-1">{input}</div><div class="col-xs-10">{label}</div><div class="col-xs-12">{error}</div>'])->widget(CheckboxX::classname(), ['autoLabel' => false, 'pluginOptions' => ['threeState' => false]]); ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app.exhibitor', 'REGISTER'), ['class' => 'btn btn-red full-width', 'name' => 'register-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>            
        </div>
        <div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        </div>
    </div>
</div>
<!--REGISTER END-->

