<div class="modal fade" id="withdraw-request-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.exhibitor', 'Withdraw request') ?></h3>
            </div>
            <div class="modal-body panel-body" style="min-height: 305px;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.exhibitor', 'Cancel') ?>
                </button>
                <button id="options-modal-confirm" type="button" class="btn btn-green js-btn-withdraw-request">
                    <?= Yii::t('app.exhibitor', 'Continue') ?>
                </button>
            </div>           
        </div>
    </div>
</div>