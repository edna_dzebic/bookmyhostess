<?php
\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Favorites');
?>

<?= $this->render("tabs/_exhibitor_profile_menu", ["active_tab" => 'favorites']); ?>

<?= $this->render("tabs/favorites", ["dataProvider" => $dataProvider]); ?>


