<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Change password');
?>

<?= $this->render("tabs/_exhibitor_profile_menu", ["active_tab" => 'profile']); ?>
<div class="exhibitor-tab-content">
    <div class="row">

        <div class="col-xs-12">
            <?= $this->render("profile-tabs/_exhibitor_profile_sub_menu", ["active_tab" => "account"]); ?>
        </div>
    </div>

    <div class="row exhibitor-content">
        <div class="col-xs-12">
            <p class="green-form-title"><?= Yii::t('app.exhibitor', 'Enter Your new password:'); ?></p>
        </div>
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

            <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control input-xlarge']) ?>
            <?= $form->field($model, 'confirm_password')->passwordInput(['class' => 'form-control input-xlarge']) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app.exhibitor', 'Save'), ['class' => 'btn btn-green']) ?>
                <?= Html::a(Yii::t('app.exhibitor', 'Back'), ['/exhibitor/account'],  ['class' => 'btn btn-default'])?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>




