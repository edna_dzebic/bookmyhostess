<?php

\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Invoices');
?>

<?= $this->render("tabs/_exhibitor_profile_menu", ["active_tab" => 'invoices']); ?>

<?= $this->render("tabs/invoices", ["dataProvider" => $dataProvider]); ?>


