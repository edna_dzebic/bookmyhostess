<?php
/* @var $cart \frontend\components\Cart */

use yii\helpers\Html;
use yii\grid\GridView;

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'user.username',
        'header' => Yii::t("app", "Username"),
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($data)
{
    return Html::a(
                    Html::encode($data["user"]["username"]), \Yii::$app->urlManager->createUrl(['//hostess/view', "id" => $data["user"]["id"]]), ["class" => "green-link"]
    );
}
    ],
    [
        'attribute' => 'event.name',
        'header' => Yii::t("app", "Event"),
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left']
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.exhibitor', 'Period'),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDate($model->date_from) . "<br>-<br>" . Yii::$app->util->formatDate($model->date_to);
        },
    ],
    [
        'attribute' => 'days',
        'header' => Yii::t("app", "Days"),
        'value' => function ($model, $index, $widget)
        {
            return $model->number_of_days;
        },
    ],
    [
        'attribute' => 'daily_rate',
        'header' => Yii::t("app", "Daily rate"),
        'value' => function($model)
        {
            return Yii::$app->util->formatEventPrice($model->event, $model->price);
        }
    ],
    [
        'attribute' => 'payment_type',
        'header' => Yii::t('app', "Payment of the daily rate"),
        'value' => function($model)
        {
            return $model->getPaymentTypeLabel();
        }
    ],
    [
        'attribute' => 'date_created',
        'header' => Yii::t('app.exhibitor', 'Date created'),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDateTime($model->date_created);
        },
    ],
    [
        'attribute' => 'date_responded',
        'header' => Yii::t('app.exhibitor', 'Date responded'),
        'value' => function ($model, $index, $widget)
        {
            if ($model->getIsResponded())
            {
                return Yii::$app->util->formatDateTime($model->date_responded);
            } else
            {
                return '';
            }
        },
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.exhibitor', 'Status'),
        'value' => function ($data) use ($cart)
        {

            if ($data->isNew)
            {
                return '<div class="text-center request-status">' . Yii::t('app.exhibitor', 'Pending') . '</div>';
            }
            if ($data->isAccepted)
            {
                $addClass = $cart->itemsContain($data->id) ? "hidden" : "";
                $removeClass = $cart->itemsContain($data->id) ? "" : "hidden";

                return "<span class='js-add-booking $addClass' data-id='$data->id'>" . Html::button(
                                "<i class='glyphicon glyphicon-unchecked'></i>&nbsp;" . Yii::t('app.exhibitor', 'Select'), ['class' => 'text-center btn btn-green full-width']
                        ) . "</span>"
                        . "<span class='js-remove-booking $removeClass' data-id='$data->id'>" . Html::button(
                                "<i class='glyphicon glyphicon-check'></i>&nbsp;" . Yii::t('app.exhibitor', 'Unselect'), ['class' => 'text-center btn btn-green full-width']
                        ) . "</span>";
            }
            if ($data->isRejected)
            {
                return '<div class="text-center request-status ">' . Yii::t('app.exhibitor', 'Rejected') . '</div>';
            }
            if ($data->isPaymentPending)
            {
                return '<div class="text-center request-status ">PayPal ' . Yii::t('app.exhibitor', 'Pending') . '</div>';
            }
            if ($data->isHostessWithdrawAfterAccept)
            {
                return '<div class="text-center request-status ">' . Yii::t('app.exhibitor', 'Withdrawn') . '</div>';
            }
            if ($data->isExWithdraw)
            {
                return '<div class="text-center request-status">' . Yii::t('app.exhibitor', 'Withdrawn') . '</div>';
            }
            if ($data->isExWithdrawAfterAccept)
            {
                return '<div class="text-center request-status">' . Yii::t('app.exhibitor', 'Withdrawn') . '</div>';
            }
            return '';
        },
            ],
            [
                'class' => '\yii\grid\ActionColumn',
                'template' => '{delete}',
                'header' => Yii::t('app.exhibitor', 'Options'),
                'buttons' => [
                    'delete' => function ($url, $model)
                    {
                        if ($model->isAccepted || $model->isNew && $model->completed == 0)
                        {
                            return Html::a(Yii::t('app.exhibitor', 'Withdraw'), \Yii::$app->urlManager->createUrl(["//exhibitor/withdraw-confirmation", "id" => $model->id]), [
                                        'data-confirm' => Yii::t('app.exhibitor', 'Do You really want to withdraw request?'),
                                        'title' => \Yii::t('app.exhibitor', 'Withdraw request'),
                                        'class' => 'btn btn-default full-width label-rejected js-withdraw-request',
                                        'data-id' => $model->id
                            ]);
                        }

                        return '';
                    }
                        ]
                    ]
                ];
                ?>

                <?= $this->render("//partials/_flash") ?>
                <div class="row exhibitor-tab-content">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-xs-6">
                            </div>
                            <div class="col-xs-6">

                                <?php
                                // dissabled is not spelling mistake! Please do not remove it
                                $class = $cart->getItemCount() > 0 ? "" : "dissabled";
                                $options = [
                                    'class' => 'btn btn-red btn-x-large btn-checkout pagination-aligned pull-right js-proceed-booking js-btn-ga-track ' . $class,
                                    'data-track-label' => 'Proceed to booking'
                                ];

                                echo Html::a(Yii::t('app.exhibitor', 'Proceed to booking'), ['/checkout/index'], $options)
                                ?>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12">
                                <?=
                                GridView::widget(
                                        [
                                            'columns' => $columns,
                                            'dataProvider' => $dataProvider,
                                            'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                                            'options' => [
                                                'class' => 'grid-view table-responsive',
                                            ],
                                            'tableOptions' => [
                                                'class' => 'table table-bordered',
                                            ],
                                            'layout' => "{items}",
                                        ]
                                );
                                ?>
                            </div>
                        </div>
                        <div class="row text-right margin-top-20">
                            <div class="col-xs-12">
                                <?php
                                echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $dataProvider->getPagination(),
                                    'options' => ['class' => 'pagination'],
                                    'maxButtonCount' => 5,
                                    'nextPageLabel' => '>',
                                    'firstPageLabel' => '<<',
                                    'lastPageLabel' => '>>',
                                    'prevPageLabel' => '<'
                                ]);
                                ?>
                            </div>  
                        </div>
                    </div>
                </div>


                <?= $this->render("_booking_modal") ?>