<?php

use kartik\rating\StarRating;
use yii\helpers\Html;

$pluginOptions = [
    'readonly' => true,
    'showClear' => false,
    'showCaption' => false,
    'size' => 'xs'
];
?>


<div class="review-item-content">
    <div class="row border-right">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <label class="green-text"> <b>  <?= $model->hostess->username ?></b></label>
                    <br>
                    <label class="text-default"><?= Yii::t('app.exhibitor', 'Event') ?> : <?= $model->event->name ?></label>
                </div>
            </div>                                   
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4  margin-top-10">
                    <?= Html::img($model->hostess->hostessProfile->getThumbnailImageUrl(360, 400), ["class" => "img-responsive"]) ?>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 margin-top-10">

                    <div class="row">
                        <div class="col-xs-12">
                            <label class="green-text"><?= Yii::t('app.exhibitor', 'Rating') ?></label>
                        </div>
                        <div class="col-xs-12">
                            <?=
                            StarRating::widget([
                                'name' => "rating",
                                'value' => $model->rating,
                                'pluginOptions' => $pluginOptions
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row margin-top-10 comment-section">
                <div class="col-xs-12">
                    <label class="green-text"><?= Yii::t('app.exhibitor', 'Comment') ?></label>
                </div>
                <div class="col-xs-12">
                    <?= $model->comment ?>
                </div>
            </div>
            <div class="row margin-top-10">

                <div class="col-xs-12 text-right">
                    <?= Yii::$app->util->formatDate($model->date_updated) ?>
                </div>                
            </div>

        </div>
    </div>
</div>

