<?php

use yii\widgets\ListView;

frontend\assets\ExhibitorFavoritesAsset::register($this);
?>

<?= $this->render("//partials/_flash") ?>
<div class = "row exhibitor-tab-content">
    <div class = "col-lg-12">

        <div class="row">
            <div class="col-xs-12">
                <?php
                echo ListView::widget([
                    'options' => [],
                    'emptyText' => '<div class="text-center">' . Yii::t('app', 'No results') . '</div>',
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'margin-top-20 review-item col-lg-6 col-md-6 col-sm-6 col-xs-12'],
                    'layout' => '<div class="row">{items}</div>',
                    'itemView' => function ($model, $key, $index, $widget)
            {

                return $this->render('_review_item', ['model' => $model, 'widget' => $widget]);
            },
                ]);
                ?>  
            </div>
        </div>

        <div class="row text-right margin-top-20">
            <div class="col-xs-12">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'options' => ['class' => 'pagination'],
                    'maxButtonCount' => 5,
                    'nextPageLabel' => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'prevPageLabel' => '<'
                ]);
                ?>
            </div>    
        </div>

    </div>
</div>