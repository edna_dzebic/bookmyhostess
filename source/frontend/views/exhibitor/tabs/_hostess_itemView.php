<?php

use yii\helpers\Html;

$defaultHostessImage = $model->hostess->hostessProfile->getDefaultImageModel(true);

$viewUrlParams = ["//hostess/view", "id" => $model->favorite_user_id];
?>

<?php

if ($defaultHostessImage !== null && !empty($defaultHostessImage->image_url)) :
    ?>
    <div class="row img-hostess">
        <div class="col-lg-12 relative">
            <?php echo Html::a('<div class="profile-image" style="background-image:url(' . $model->hostess->hostessProfile->getThumbnailImageUrl(220, 170) . ');"></div>', $viewUrlParams); ?>
            <div class="img-options">
                <span class="zoom-in">
                    <?=
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete-favorite', 'id' => $model['id']], [
                        'title' => Yii::t('app.exhibitor', 'Delete')
                    ])
                    ?>
                </span>
            </div>
        </div>
    </div>
    <div class="row margin-top-10">        
        <div class="col-lg-12 text-center hostess-name">
            <span class="username">  <a href="<?= Yii::$app->urlManager->createUrl($viewUrlParams) ?>"><?= $model->hostess->username; ?></a></span>
        </div>
    </div>
<?php endif; ?>
