<?php
/* @var $cart \frontend\components\Cart */

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\rating\StarRating;

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'user.username',
        'header' => Yii::t("app", "User"),
        'format' => 'raw',
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($model, $index, $widget)
{
    // if it is a past event, do not display data, only link to pdf profile
    if ($model->event->isPast)
    {
        return Html::a(
                        Html::encode($model->user->username), \Yii::$app->urlManager->createUrl(['//hostess/pdf', "id" => $model->user->id]), ["class" => "green-link"]
        );
    } else
    {
        // otherwise show hostess data
        return Html::a(
                        Html::encode($model->user->fullName), \Yii::$app->urlManager->createUrl(['//hostess/pdf', "id" => $model->user->id]), ["class" => "green-link"]
                ) .
                "<br>" . $model->user->email .
                "<br>" . $model->user->phone_number;
    }
}
    ],
    [
        'attribute' => 'event.name',
        'header' => Yii::t("app", "Event"),
        'headerOptions' => ['class' => 'text-left'],
        'contentOptions' => ['class' => 'text-left']
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.exhibitor', 'Period'),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDate($model->date_from) . "<br>-<br>" . Yii::$app->util->formatDate($model->date_to);
        },
    ],
    [
        'attribute' => 'days',
        'header' => Yii::t("app", "Days"),
        'value' => function ($model, $index, $widget)
        {
            return $model->getTotalBookingDays();
        },
    ],
    [
        'attribute' => 'daily_rate',
        'header' => Yii::t("app", "Daily rate"),
        'value' => function($model)
        {
            return Yii::$app->util->formatEventPrice($model->event, $model->price);
        }
    ],
    [
        'attribute' => 'payment_type',
        'header' => Yii::t('app', "Payment of the daily rate"),
        'value' => function($model)
        {
            return $model->getPaymentTypeLabel();
        }
    ],
    [
        'attribute' => 'date_created',
        'header' => Yii::t('app.exhibitor', 'Date created'),
        'value' => function ($model, $index, $widget)
        {
            return Yii::$app->util->formatDateTime($model->date_created);
        },
    ],
    [
        'attribute' => 'date_responded',
        'header' => Yii::t('app.exhibitor', 'Date responded'),
        'value' => function ($model, $index, $widget)
        {
            if ($model->date_responded != '0000-00-00 00:00:00')
            {
                return Yii::$app->util->formatDateTime($model->date_responded);
            } else
            {
                return '';
            }
        },
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.hostess', 'Review'),
        'value' => function ($data)
        {
            if ($data->event->isPast)
            {
                if (isset($data->hostessReview) && $data->hostessReview->isActive)
                {
                    return
                            StarRating::widget([
                                'name' => 'rating',
                                'value' => $data->hostessReview->rating,
                                'pluginOptions' => [
                                    'readonly' => true,
                                    'showClear' => false,
                                    'showCaption' => false,
                                    'size' => 'xs'
                                ]
                    ]);
                } else
                {
                    return Html::a(Html::encode(\Yii::t('app.exhibitor', 'Write Review')), \Yii::$app->urlManager->createUrl(['//exhibitor/create-review', "id" => $data["id"]]), ["class" => "btn btn-green full-width"]);
                }
            } else
            {
                return '-';
            }
        },
            ]
        ];
        ?>

        <?= $this->render("//partials/_flash") ?>
        <div class="row exhibitor-tab-content">
            <div class="col-lg-12">

                <div class="row">
                    <div class="col-xs-12">
                        <?=
                        GridView::widget(
                                [
                                    'columns' => $columns,
                                    'dataProvider' => $dataProvider,
                                    'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                                    'options' => [
                                        'class' => 'grid-view table-responsive',
                                    ],
                                    'tableOptions' => [
                                        'class' => 'table table-bordered',
                                    ],
                                    'layout' => "{items}",
                                ]
                        );
                        ?>
                    </div>
                </div>
                <div class="row text-right margin-top-20">
                    <div class="col-xs-12">
                        <?php
                        echo \yii\widgets\LinkPager::widget([
                            'pagination' => $dataProvider->getPagination(),
                            'options' => ['class' => 'pagination'],
                            'maxButtonCount' => 5,
                            'nextPageLabel' => '>',
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>>',
                            'prevPageLabel' => '<'
                        ]);
                        ?>
            </div>  
        </div>
    </div>
</div>
