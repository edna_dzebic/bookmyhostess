<?php

use yii\widgets\ListView;

frontend\assets\ExhibitorFavoritesAsset::register($this);
?>

<div class = "row exhibitor-tab-content">
    <div class = "col-lg-12">

        <div class="row">
            <div class="col-xs-12">
                <?php
                echo ListView::widget([
                    'options' => [],
                    'emptyText' => '<div class="text-center">' . Yii::t('app', 'No results') . '</div>',
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'margin-bottom-20 hostess-item col-lg-3 col-md-3 col-sm-3 col-xs-6'],
                    'layout' => '<div class="row">{items}</div>',
                    'itemView' => function ($model, $key, $index, $widget)
            {

                return $this->render('_hostess_itemView', ['model' => $model, 'widget' => $widget]);
            },
                ]);
                ?>  
            </div>
        </div>

        <div class="row text-right margin-top-20">
            <div class="col-xs-12">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'options' => ['class' => 'pagination'],
                    'maxButtonCount' => 5,
                    'nextPageLabel' => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'prevPageLabel' => '<'
                ]);
                ?>
            </div>    
        </div>

    </div>
</div>
