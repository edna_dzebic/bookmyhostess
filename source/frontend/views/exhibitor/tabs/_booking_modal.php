<div class="modal fade" id="booking-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('app.exhibitor', 'Booking Process') ?></h3>
            </div>
            <div class="modal-body panel-body">
                <?= Yii::t('app.exhibitor', 'Please select first the person you would like to book by clicking on the button "Select". The "Select" button is going to be enabled as soon as the requested people confirm your request. Thereupon continue with "Proceed to booking" to complete the booking process.') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('app.exhibitor', 'Close Dialog') ?>
                </button>
            </div>           
        </div>
    </div>
</div>