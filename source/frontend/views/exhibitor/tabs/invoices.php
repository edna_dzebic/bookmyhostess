<?php

use yii\grid\GridView;
use yii\helpers\Html;

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'header' => Yii::t('app.exhibitor', 'Number'),
        'attribute' => 'number',
        'value' => function ($data)
        {
            return $data->getRealIdText();
        },
    ],
    [
        'attribute' => 'date_paid',
        'header' => Yii::t('app.exhibitor', 'Date'),
        'value' => function ($data)
        {
            return \Yii::$app->util->formatDate($data["date_paid"]);
        },
    ],
    [
        'attribute' => 'amount',
        'value' => function ($data)
        {
            return \Yii::$app->util->formatPrice($data["amount"]) . " EUR";
        },
    ],
    [
        'header' => Yii::t('app.exhibitor', 'Payment type'),
        'attribute' => 'payment_type',
        'value' => function ($data)
        {
            return $data->getPaymentTypeLabel();
        },
    ],
    [
        'header' => Yii::t('app.exhibitor', 'Payment status'),
        'attribute' => 'status',
        'value' => function ($data)
        {
            return $data->getPaymentStatusLabel();
        },
    ],
    [
        'format' => 'raw',
        'header' => Yii::t('app.exhibitor', 'File'),
        'value' => function ($data)
        {
            if ($data["status"] === 0 || $data["filename"] == "" || $data["filename"] == null)
            {
                return '';
            } else
            {
                return Html::a(Html::encode(\Yii::t('app.exhibitor', "Download")), \Yii::$app->urlManager->createUrl([$data["filename"]]), ["class" => "btn btn-default full-width", "target" => "_blank"]);
            }
        },
            ],
        ];
        ?>

        <div class="row exhibitor-tab-content">
            <div class="col-xs-12">
                <?=
                GridView::widget(
                        [
                            'columns' => $columns,
                            'dataProvider' => $dataProvider,
                            'headerRowOptions' => ["class" => "text-center", 'style' => 'text-align:center;'],
                            'options' => [
                                'class' => 'grid-view table-responsive',
                            ],
                            'tableOptions' => [
                                'class' => 'table table-bordered',
                            ],
                            'layout' => "{items}",
                        ]
                );
                ?>
            </div>
            <div class="col-xs-12 text-right margin-top-20">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->getPagination(),
                    'options' => ['class' => 'pagination'],
                    'maxButtonCount' => 5,
                    'nextPageLabel' => '>',
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'prevPageLabel' => '<'
                ]);
                ?>
    </div>
</div>