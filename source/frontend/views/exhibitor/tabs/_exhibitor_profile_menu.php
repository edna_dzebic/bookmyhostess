<?php

use yii\helpers\Html;
use frontend\models\ExhibitorProfile;
use yii\bootstrap\Tabs;

$count = ExhibitorProfile::getNumberOfPendingRequests();
?>

<div>
    <?=
    Tabs::widget([
        'navType' => 'nav-pills green-nav-pills profile-main-menu',
        'items' => [
            [
                'label' => Yii::t('app.exhibitor', 'Profile'),
                'url' => ["//exhibitor/profile"],
                'active' => $active_tab == "profile",
                'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.exhibitor', 'Wishlist'),
                'url' => ["//exhibitor/favorites"],
                'active' => $active_tab == "favorites",
                'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
            ],
            [
                'label' => $count == 0 ? Yii::t('app.exhibitor', 'Requests') : Yii::t('app.exhibitor', 'Requests') . ' <div class="request-alert">' . $count . '</div>',
                'encode' => false,
                'url' => ["//exhibitor/event-request"],
                'active' => $active_tab == "requests",
                'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.exhibitor', 'Bookings'),
                'url' => ["//exhibitor/bookings"],
                'active' => $active_tab == "bookings",
                'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.exhibitor', 'Invoices'),
                'url' => ["//exhibitor/invoice"],
                'active' => $active_tab == "invoices",
                'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
            ],
            [
                'label' => Yii::t('app.exhibitor', 'Reviews'),
                'url' => ["//exhibitor/reviews"],
                'active' => $active_tab == "reviews",
                'headerOptions' => ["class" => 'col-lg-2 col-md-2 col-sm-4 col-xs-6']
            ]
        ],
    ]);
    ?>
</div>