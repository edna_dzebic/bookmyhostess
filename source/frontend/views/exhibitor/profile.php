<?php
\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Profile');
?>

<?= $this->render("tabs/_exhibitor_profile_menu", ["active_tab" => 'profile']); ?>

<?= $this->render("//partials/_flash") ?>

<div class="exhibitor-tab-content">
    <?= $this->render("profile-tabs/_exhibitor_profile_sub_menu", ["active_tab" => $view]); ?>

    <?= $this->render("profile-tabs/$view", ["model" => $model, "data" => $data]); ?>     
</div>



