<?php

\frontend\assets\EventRequestAsset::register($this);
\frontend\assets\ExhibitorProfileAsset::register($this);

$this->title = Yii::t('app.exhibitor', 'Requests');
?>

<?= $this->render("tabs/_exhibitor_profile_menu", ["active_tab" => 'requests']); ?>

<?= $this->render("tabs/requests", ["model" => $model, "cart" => $cart, "dataProvider" => $dataProvider]); ?>


<?= $this->render("_withdraw_request_modal") ?>