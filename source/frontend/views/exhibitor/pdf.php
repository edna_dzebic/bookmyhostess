<?php
/* @var $this yii\web\View */
/* @var $model \common\models\Invoice */
?>

<table style="width: 100%; font-size:10pt;">
    <tr>
        <td style="width: 70%"></td>
        <td style="width: 30%">
            <table style="width: 100%">
                <tr>
                    <td>
                        <img src='<?= Yii::$app->urlManager->createAbsoluteUrl(["images/pdf-logo.png"]) ?>' width="262px" height="91px">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 70%;">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <span  style="color:#0caa9d;">BOOKmyHOSTESS</span>  |  Ottostr 7. | 50859 <?= Yii::t('app', 'Cologne') ?> - <?= Yii::t('app', 'Germany') ?>
                                </td>                               
                            </tr>
                        </table>                        
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 30%;color:#0caa9d;padding-left: 20px;">
            BOOKmyHOSTESS.com
        </td>
    </tr>
    <tr>
        <td style="width: 70%">
            <table style="width: 100%;font-size:10pt;">
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td><b><?= $model->userRequested->exhibitorProfile->company_name ?></b></td>
                </tr>
                <tr>
                    <td><?= $model->userRequested->exhibitorProfile->street ?> &nbsp;  <?= $model->userRequested->exhibitorProfile->home_number ?> 
                </tr> 
                <tr>
                    <td><?= $model->userRequested->exhibitorProfile->postal_code ?> &nbsp;  <?= $model->userRequested->exhibitorProfile->city ?> 
                </tr>
                <tr>
                    <td><?= isset($model->userRequested->exhibitorProfile->country) ? Yii::t('app', $model->userRequested->exhibitorProfile->country->name) : ""; ?></td>
                </tr>  
                <tr>
                    <td><br><b><?= Yii::t('app.exhibitor', 'Tax number') ?></b> :&nbsp;<?= $model->userRequested->exhibitorProfile->tax_number ?></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>

                <?php if ($model->getIsNormalInvoice()) : ?>

                    <tr>
                        <td>
                            <?= Yii::t('app.exhibitor', 'Invoice Date:') ?>&nbsp;<?= Yii::$app->util->formatDate($model->date_created) ?> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= Yii::t('app.exhibitor', 'Invoice Number:') ?>&nbsp;<?= $model->getRealIdText(); ?>
                        </td>
                    </tr>   

                <?php else: ?>

                    <tr>
                        <td>
                            <?= Yii::t('app.exhibitor', 'Date:') ?>&nbsp;<?= Yii::$app->util->formatDate($model->date_created) ?> 
                        </td>
                    </tr>

                <?php endif; ?>
            </table>
        </td>
        <td style="width: 30%">
            <table style="width: 100%;font-size:12px;font-weight: bold;padding-left: 20px;">
                <tr>
                    <td> 
                        <?= Yii::t('app.exhibitor', 'Owner') ?>
                        <br>
                        Adela Kadiric
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        Ottostr 7.
                        <br>
                        50859 <?= Yii::t('app', 'Cologne') ?> - <?= Yii::t('app', 'Germany') ?>
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        M: +49 (0) 152/02 964 261
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        a.kadiric@bookmyhostess.com
                        <br>
                        www.BOOKmyHOSTESS.com
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        <?= Yii::t('app.exhibitor', 'Local tax office') ?> : Köln West
                        <br>
                        <?= Yii::t('app.exhibitor', 'Head office') ?> : Köln
                        <br>
                        <?= Yii::t('app.exhibitor', 'Tax number') ?> : DE 299791123
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        Sparkasse​​KölnBonn
                        <br>
                        IBAN:
                        <br>
                        DE56​3705 0198 1016 8636 96
                        <br>
                        BIC/SWIFT
                        <br>
                        COLSDE33XXX
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td align="left" height="20">
        </td>
    </tr>
</table>
<table align = "center">
    <?php if ($model->getIsNormalInvoice()) : ?>
        <tr>
            <td style="text-align: center;">
                <b style="text-align:center"><?= Yii::t('app.exhibitor', 'INVOICE') ?></b>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td style="text-align: center;">
                <b style="text-align:center"><?= Yii::t('app.exhibitor', 'RECEIPT') ?></b>
            </td>
        </tr>
    <?php endif; ?>
</table>
<table>
    <tr>
        <td align="left" height="20">
        </td>
    </tr>
</table>
<table style="">
    <tr>
        <td><?= Yii::t('app.exhibitor', 'Dear Madam/Sir,') ?><br/></td>
    </tr>
    <tr>
        <td align="left">
            <div style="text-align: justify">
                <?= Yii::t('app.exhibitor', 'Thank You very much for booking through BOOKmyHOSTESS.com. We value Your trust in our company,
                and we will do our best to meet Your service expectations. Following charges for our service will appear on Your account in the next days:') ?>
            </div>
        </td>
    </tr>
</table>
<table width='100%' style=" width: 100%;">
    <tr>
        <td align="center" style='alignment-baseline: middle;'>
            <div style=" padding:0px; width:100%; font-size:8pt;"><br/>

                <table cellspacing="0" style='border:1px solid black; min-width: 100%; width: 100%;'>
                    <tr style="font-weight: bold;">
                        <td valign='top' align='center' style='padding: 4px; border:1px solid black;'>#</td>
                        <td valign='top' align='center' style='padding: 4px; border:1px solid black;'><?= Yii::t('app.exhibitor', 'Description') ?></td>
                        <td valign='top' align='center' style='padding: 4px; border:1px solid black;'><?= Yii::t('app.exhibitor', 'Event name') ?></td>
                        <td valign='top' align='center' style='padding: 4px; border:1px solid black;'><?= Yii::t('app.exhibitor', 'Period') ?>
                        </td>
                        <td valign='top' align='center' style='padding: 4px; border:1px solid black;'><?= Yii::t('app.exhibitor', 'Days') ?></td>
                        <td valign='top' align='center' style='padding: 4px; border:1px solid black;'><?= Yii::t('app.exhibitor', 'Hostess name') ?></td>                      
                        <td valign='top' align='center' style='padding: 4px; border:1px solid black;'><?= Yii::t('app.exhibitor', 'Price (EUR)/<br/>Day') ?>
                        </td>
                        <td valign='top' align='center' style='padding: 4px; border:1px solid black;'><?= Yii::t('app.exhibitor', 'Amount <br/>(EUR)') ?><br/>
                        </td>
                    </tr>

                    <?php
                    $i = 0;
                    $total = 0;

                    foreach ($model->eventRequests as $request)
                    {
                        $total = $total + $request->total_booking_price;
                        ?>
                        <tr style='height:68px'>
                            <td style='height:68px; padding: 4px; border:1px solid black;' valign='top'
                                align='center'> <?= ++$i ?>
                            </td>
                            <td style='height:68px; padding: 4px; border:1px solid black;'  valign='top'
                                align='center'><?= Yii::t('app.exhibitor', 'Online booking of temporary <br/>trade fair staff') ?>
                            </td>
                            <td style='height:68px; padding: 4px; border:1px solid black;'valign='top'
                                align='center'><?= $request->event->name; ?>
                            </td>
                            <td style='height:68px; padding: 4px; border:1px solid black;' valign='top'
                                align='center'><?= Yii::$app->util->formatDate($request->date_from); ?>
                                <br>
                                - 
                                <br>
                                <?= Yii::$app->util->formatDate($request->date_to); ?>
                            </td>
                            <td style='height:68px; padding: 4px; border:1px solid black;'  valign='top'
                                align='center'><?= $request->number_of_days; ?>
                            </td>
                            <td style='height:68px; padding: 4px; border:1px solid black;'valign='top'
                                align='center'><?= $request->user->fullName; ?>
                            </td>                           
                            <td style='height:68px; padding: 4px; border:1px solid black;' valign='top'
                                align='center'><?= Yii::$app->util->formatPrice($request->booking_price_per_day); ?>
                            </td>
                            <td style='height:68px; padding: 4px; border:1px solid black;'  valign='top'
                                align='right'><?= Yii::$app->util->formatPrice($request->total_booking_price) ?>
                            </td>
                        </tr>
                    <?php } ?>


                    <?php if ($model->getIsNormalInvoice()) : ?>

                        <?php
                        // if from Germany
                        if (isset($model->userRequested->exhibitorProfile->country) &&
                                Yii::$app->util->isGerman($model->userRequested->exhibitorProfile->country->id)):
                            ?>

                            <tr>
                                <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                    <b><?= Yii::t('app.exhibitor', 'Net amount') ?></b>
                                </td>  
                                <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                                    <?= Yii::$app->util->formatPrice($model->getTotalBookingPrice()) ?>
                                </td>
                            </tr>
                            <tr>
                                <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                    <b><?= Yii::t('app.exhibitor', '+19% value added tax') ?></b>
                                </td>   
                                <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                                    <?= Yii::$app->util->formatPrice($model->getTaxAmount()) ?>
                                </td>
                            </tr>
                            <tr>
                                <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                    <b><?= Yii::t('app.exhibitor', 'Gross amount') ?></b>
                                </td>     
                                <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                                    <b><?= Yii::$app->util->formatPrice($model->getGrossAmount()) ?></b>
                                </td>
                            </tr>

                            <?php if (is_object($model->coupon)) : ?>


                                <tr>
                                    <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                        <b><?=
                                            Yii::t('app.exhibitor', 'Discount for coupon with code {couponCode} - {couponNettoAmount} EUR', [
                                                "couponCode" => $model->coupon->code,
                                                "couponNettoAmount" => Yii::$app->util->formatPrice($model->getCouponNettoAmount())
                                            ])
                                            ?>
                                        </b>
                                    </td>     
                                    <td style='height:30px;  padding:4px; border:1px solid black;' align="right">

                                    </td>
                                </tr>

                                <tr>
                                    <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                        <b><?=
                                            Yii::t('app.exhibitor', 'Coupon 19% VAT - {couponTaxAmount} EUR', [
                                                "couponTaxAmount" => Yii::$app->util->formatPrice($model->getCouponTaxAmount($model->getCouponGrossAmount()))
                                            ])
                                            ?>
                                        </b>
                                    </td>     
                                    <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                                        <b>- <?= Yii::$app->util->formatPrice($model->getCouponGrossAmount()) ?></b>

                                    </td>
                                </tr>
                                <tr>
                                    <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                        <b><?= Yii::t('app.exhibitor', 'Total amount with discount') ?></b>
                                    </td>     
                                    <td style='height:30px;  padding:4px; border:1px solid black;' align="right">

                                        <b><?= Yii::$app->util->formatPrice($model->getPayPalAmount()) ?></b>
                                    </td>
                                </tr>
                            <?php endif; ?>

                        <?php else: ?>

                            <tr>
                                <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                    <b><?= Yii::t('app.exhibitor', 'TOTAL') ?> </b>
                                </td>     
                                <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                                    <?= Yii::$app->util->formatPrice($model->getGrossAmount()) ?>
                                </td>
                            </tr>

                            <?php if (is_object($model->coupon)) : ?>


                                <tr>
                                    <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                        <b><?= Yii::t('app.exhibitor', 'Discount for coupon with code {couponCode}', ["couponCode" => $model->coupon->code]) ?></b>
                                    </td>     
                                    <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                                        - <?= Yii::$app->util->formatPrice($model->getCouponGrossAmount()) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                        <b><?= Yii::t('app.exhibitor', 'Total amount with discount') ?></b>
                                    </td>     
                                    <td style='height:30px;  padding:4px; border:1px solid black;' align="right">

                                        <b><?= Yii::$app->util->formatPrice($model->getPayPalAmount()) ?></b>
                                    </td>
                                </tr>
                            <?php endif; ?>

                        <?php endif; ?>

                    <?php else: ?>

                        <tr>
                            <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                                <b><?= Yii::t('app.exhibitor', 'TOTAL') ?> </b>
                            </td>     
                            <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                                <?= Yii::$app->util->formatPrice($model->getGrossAmount()) ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td align="left" height="20">
        </td>
    </tr>   
</table>
<table style=" text-align:justify; ">


    <?php if ($model->getIsNormalInvoice()) : ?>
        <?php
        // if exhibitor is not German
        if (
                isset($model->userRequested->exhibitorProfile->country) &&
                Yii::$app->util->isGerman($model->userRequested->exhibitorProfile->country->id) == false
        ):
            ?>

            <?php
            // if is EU invoice type
            if ($model->userRequested->exhibitorProfile->country->getIsEuInvoice()):
                ?>

                <tr>
                    <td align="left" height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Yii::t('app.exhibitor', 'Reverse Charge - According to Article 194, 196 of Council Directive 2006/112/EEC, VAT liability rests with the service recipient {tax_id}.', ["tax_id" => $model->userRequested->exhibitorProfile->tax_number]) ?>
                    </td>
                </tr>

                <?php
            // if third country - not Germany and not EU
            else :
                ?>
                <tr>
                    <td align="left" height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Yii::t('app.exhibitor', 'Not taxable in Germany.') ?>
                    </td>
                </tr>
            <?php endif; ?>
        <?php endif; ?>


        <?php if (!empty($model->paypal_transaction_id)): ?>
            <tr>
                <td>
                    <?= Yii::t('app.exhibitor', 'According to our General Terms and Conditions, payment is due immediately. The amount will be automatically collected from Your account. The PayPal transaction ID is: {paypalTransactionID}.', ["paypalTransactionID" => $model->paypal_transaction_id]) ?>
                </td> 
            </tr>
        <?php endif; ?>

        <tr>
            <td align="left" height="20">
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('app.exhibitor', 'The invoice was issued electronically and is therefore valid without signature.') ?>
            </td>
        </tr>

    <?php endif; ?>

    <tr>
        <td align="left" height="20">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.exhibitor', 'If you have any questions or comments, please contact us at +4922344301397.') ?>
        </td>
    </tr>
    <tr>
        <td align="left" height="10">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.exhibitor', 'Thank You for using BOOKmyHOSTESS.com! We are looking forward to serving You in the near future.') ?>
        </td>
    </tr>
    <tr>
        <td align="left" height="10">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.exhibitor', 'Best Regards, <br> Your BOOKmyHOSTESS.com - Team.') ?>
        </td>
    </tr>
</table>