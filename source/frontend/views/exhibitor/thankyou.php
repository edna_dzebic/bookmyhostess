<?php
$this->title = Yii::t('app.exhibitor', 'Thank You');
?>
<div class="banner banner-image registration-exhibitor">

    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 register-form">
                <div class="row">
                    <div class="col-xs-12 title">
                        <?= Yii::t('app.exhibitor', 'THANK YOU FOR REGISTERING') ?>
                    </div>
                </div>
                <div class="row margin-top-10">
                    <div class="col-xs-12">
                        <?= Yii::t('app.exhibitor', 'A verification e-mail has been sent to Your e-mail-account (please check the spam inbox too).'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>     
</div>