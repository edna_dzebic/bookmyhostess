<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <?= $this->render("_meta") ?>
        
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    </head>

    <body>
        <?php $this->beginBody() ?>
        <div class="wrap">

            <?= $this->render("_header") ?>

            <div class="container">

                <?= $content ?>
            </div>

        </div>


        <?= $this->render("_footer") ?>

        <?= $this->render("_scripts") ?>

        <?php $this->endBody() ?>


    </body>
</html>
<?php $this->endPage() ?>
