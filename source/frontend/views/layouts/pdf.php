<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link href='http://fonts.googleapis.com/css?family=Maven+Pro:700,500,900' rel='stylesheet' type='text/css'>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        @page {
            sheet-size: A3-L;
        }

        @page bigger {
            sheet-size: 420mm 370mm;
        }

        @page toc {
            sheet-size: A4;
        }

        h1.bigsection {
            page-break-before: always;
            page: bigger;
        }
    </style>
</head>
<body>

<?php $this->beginBody() ?>
<div class="row" style="width:70%;margin:0 auto;">
    <div class="col-lg-12">
        <?php echo $content; ?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
