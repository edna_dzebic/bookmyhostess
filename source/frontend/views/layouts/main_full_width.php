<?php

use frontend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta name='viewport' content='width=976'>

        <link href='http://fonts.googleapis.com/css?family=Maven+Pro:700,500,900' rel='stylesheet' type='text/css'>

        <?= $this->render("_meta") ?>

    </head>
    <body>
        <?php $this->beginBody(); ?>

        <div class="wrap">
            <?= $this->render("_header") ?>

            <div class="container-fluid">
                <?= $content ?>
            </div>

        </div>

        <?= $this->render("_footer") ?>

        <?= $this->render("_scripts") ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>