<?php

use yii\helpers\Html;
use frontend\models\Blog;

$blogs = Blog::getTopThree();
?>
<footer class="footer">
    <div class="container">           

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 member-of">
                <h3 class="title"><?= Yii::t('app', 'Member of:') ?></h3>
                <a href="https://webdecologne.de/" target="_blank" class="web-de-cologne">
                    <?= Html::img(Yii::$app->urlManager->createAbsoluteUrl(["/images/member_of/web-de-cologne.png"]), ["class" => "img-responsive", "alt" => "webdecologne.de"]) ?>    
                </a>
                <br>
                <a href="http://bosnien.ahk.de/" target="_blank">
                    <?= Html::img(Yii::$app->urlManager->createAbsoluteUrl(["/images/member_of/wirtschaftsverein-delegation.jpg"]), ["class" => "img-responsive", "alt" => "bosnien.ahk.de"]) ?>    
                </a>
                <br>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 news">
                <h3 class="title"><?= Yii::t('app', 'Latest news') ?></h3>
                <?php foreach ($blogs as $blog) : ?>
                    <p>
                        <span>
                            <?= Yii::$app->util->formatDate($blog->date_created) ?>
                        </span>
                        <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(["blog/view", "id" => $blog->id]) ?>" ><?= $blog->title ?></a>
                    </p>
                <?php endforeach; ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <h3 class="title"><?= Yii::t('app', 'Contact') ?></h3>
                <p>
                    <?= Yii::t('app', 'Please contact us for all questions with an email to  <a href="mailto:{email}" >{email}</a>, or by phone on <a href="tel:{phone}">{phoneText}</a>.', ["email" => "info@bookmyhostess.com", "phone" => "004922344301397", "phoneText" => "+492234 4301397"]) ?>
                </p>
                <div class=" social-links">
                    <a href="https://www.facebook.com/BookMyHostess/">
                        <i class="fa fa-facebook-square"></i>
                    </a>
                    <a href="https://twitter.com/bookmyhostess">
                        <i class="fa fa-twitter-square"></i>
                    </a>
                    <a href="https://www.instagram.com/bookmyhostess/">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="https://www.linkedin.com/company-beta/5194760/">
                        <i class="fa fa-linkedin-square"></i>
                    </a>
                    <a  href="https://www.xing.com/companies/bookmyhostess.com">
                        <i class="fa fa-xing-square"></i>
                    </a>
                    <a  href="https://www.youtube.com/channel/UCnsczMkf_WrUcZmXeGSnU4A">                    
                        <i class="fa fa-youtube-square"></i>
                    </a>
                </div>
            </div>
        </div>

        <div class="row margin-top-20">
            <div class="col-xs-12 links">

                <?= Html::a(Yii::t('app', "Impressum"), ["page/view", "seo" => "impressum"], ["class" => "link  menu-item"]); ?>

                <?= Html::a(Yii::t('app', "Privacy policy"), ["page/view", "seo" => "privacy"], ["class" => "link  menu-item"]); ?>

                <?= Html::a(Yii::t('app', "Terms and Conditions"), ["page/view", "seo" => "terms"], ["class" => "link  menu-item"]); ?>

                <?= Html::a(Yii::t('app', "Privacy Policy - Apps"), ["page/view", "seo" => "privacy_apps"], ["class" => "link  menu-item"]); ?>

                <?= Html::a(Yii::t('app', "FAQ Exhibitors"), ["page/view", "seo" => "faq-exhibitor"], ["class" => "link  menu-item"]); ?>

                <?= Html::a(Yii::t('app', "FAQ Hostesses"), ["page/view", "seo" => "faq-hostess"], ["class" => "link  menu-item"]); ?>
                
                <?= Html::a(Yii::t('app.cookie', "Cookie Details"), ["page/cookie-details", "seo" => "cookie-details"], ["class" => "link  menu-item"]); ?>

                <?= Html::a(Yii::t('app', "New features"), ["page/view", "seo" => "new-features"], ["class" => "link  menu-item"]); ?>

            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-xs-12 text-center">
                <span> &copy; <?= date('Y'); ?> <?= Yii::$app->name; ?> </span>
            </div>

        </div>
        <div class="row margin-top-10">
            <div class="col-xs-12 text-center">


                <?php if (Yii::$app->language == 'de'): ?>
                    Developed by <a href="https://www.watabo.com/de/startseite" target="_blank" class="developed-by">Watabo</a>
                <?php elseif (Yii::$app->language == 'en'): ?>
                    Developed by <a href="https://www.watabo.com/en/home" target="_blank" class="developed-by">Watabo</a>
                <?php endif; ?>

            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-xs-12 text-center">

            </div>
        </div>
    </div>
</footer>

<?php if (trim(Yii::$app->request->pathInfo) != 'page/cookie-details') : ?>
    <?= $this->render("_cookie_modal") ?>
<?php endif; ?> 
