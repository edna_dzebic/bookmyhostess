<?php

use yii\helpers\Html;
use frontend\models\Layout;
?>

<?= Html::csrfMetaTags() ?>

<meta charset="<?= Yii::$app->charset ?>">
<title><?= Html::encode($this->title) ?></title>

 <script type="text/javascript" src="https://secure.hiss3lark.com/js/186286.js" ></script>
 <noscript><img alt="" src="https://secure.hiss3lark.com/186286.png" style="display:none;" /></noscript>


<?php
if (is_null($this->metaTags) || false === array_key_exists('keywords', $this->metaTags))
{
    $this->registerMetaTag([
        'name' => 'keywords',
        'content' => Layout::getMetaKeywords()
            ], 'keywords');
}
if (is_null($this->metaTags) || false === array_key_exists('description', $this->metaTags))
{
    $this->registerMetaTag([
        'name' => 'description',
        'content' => Layout::getMetaDescription()
            ], 'description');
}
?>


<?php
Yii::$app->jsonld->addOrganizationDoc();

Yii::$app->jsonld->registerScripts();

$this->head()
?>  