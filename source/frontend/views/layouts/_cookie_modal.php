<div class="modal fade" id="cookie-confirmation-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><?= Yii::t('app.cookie', 'Cookie settings') ?></h3>
            </div>
            <div class="modal-body panel-body" style="min-height: 305px;">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <p class="cookie-text">
                            <?= Yii::t('app.cookie', 'We use cookies that are necessary to enable you to use our website. We also use cookies (also from third-party providers) for statistical and analysis purposes as well as for marketing and personalizing our content and advertising, in order to evaluate the effectiveness of our advertising campaigns and to be able to offer you advertising beyond this page that is tailored to your interests and Corresponds to your surfing behavior. You can allow the use of all cookies by clicking "All agree". By clicking "Accept all", you voluntarily consent to the use of all cookies (revocable at any time). This also includes, for a limited time, your consent to data processing outside the EEA, such as in the USA (Art. 49 Para. 1 lit. a) GDPR), where the high European level of data protection does not exist, so that the data can be accessed by authorities to control - and may be subject to monitoring purposes against which neither effective legal remedies nor rights of data subjects can be enforced. Since we respect your right to data protection, you can decide not to allow certain types of cookies and to save your individual selection of cookies by clicking "Accept selection". Your consent is voluntary and can be withdrawn at any time. You can find more information about the cookies we use and how to exercise your right of withdrawal in our <a href="https://www.bookmyhostess.com/page/privacy">data protection declaration</a> and under "More details". Information on the publisher of the page can be found in the <a href="https://www.bookmyhostess.com/page/impressum">imprint</a>. ') ?>
                        </p>
                        <p><?= Yii::t('app.cookie', 'You can decide which categories do you allow:') ?></p>
                        <p >
                            <input type="checkbox" disabled checked name="general" class="js-general-cookie"> <b><?= Yii::t('app.cookie', 'Necessary') ?></b>
                            <br>
                            <input type="checkbox" name="preferences" value="0" class="js-preferences-cookie"> <b><?= Yii::t('app.cookie', 'Preferences') ?></b>
                            <br>
                            <input type="checkbox" name="ga" value="0" class="js-ga-cookie"> <b><?= Yii::t('app.cookie', 'Analyse') ?></b>
                            <br>
                            <input type="checkbox" name="zopim" value="0" class="js-zopim-cookie"> <b><?= Yii::t('app.cookie', 'Chat') ?>
                        </p>
                    </div>               
                </div>
            </div>
            <div class="modal-footer">
                <button id="cookie-modal-accept-all" type="button" class="btn btn-red js-btn-cookie-accept-all">
                    <?= Yii::t('app.cookie', 'Accept all') ?>
                </button>
                <button id="cookie-modal-accept-selected" type="button" class="btn btn-default js-btn-cookie-accept-selected">
                    <?= Yii::t('app.cookie', 'Accept selection') ?>
                </button>

                <a class="btn btn-default" href="<?= Yii::$app->urlManager->createUrl(['/page/cookie-details']) ?>"><?= Yii::t('app.cookie', 'More details') ?></a>
            </div>           
        </div>
    </div>
</div>