<?php if (\Yii::$app->settings->include_google_analytics): ?>
    <input id="gaEnable" name = "gaEnable" value="1" type="hidden">
<?php else: ?>
    <input id="gaEnable" name = "gaEnable" value="0" type="hidden">
<?php endif; ?>

<?php if (\Yii::$app->settings->include_zopim): ?>
    <input id="zopimEnable" name = "zopimEnable" value="1" type="hidden">
    <?php else: ?>
    <input id="zopimEnable" name = "zopimEnable" value="0" type="hidden">
<?php endif; ?>

<?php if (\Yii::$app->settings->inlude_hotjar): ?>
    <input id="hotjarEnable" name = "hotjarEnable" value="1" type="hidden">
<?php else: ?>
    <input id="hotjarEnable" name = "hotjarEnable" value="0" type="hidden">
<?php endif; ?>