<?php

use yii\bootstrap\NavBar;
use yii\helpers\Html;
use kartik\widgets\Select2;

$brandUrl = "/site/index";

if (!Yii::$app->user->isGuest && Yii::$app->user->identity["role"] == \common\models\User::ROLE_HOSTESS)
{
    $brandUrl = "/site/hostess";
}
?>

<div class="header-container">

    <div class="header-top">
        <div class="container minor-padding">

            <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity["role"] == \common\models\User::ROLE_HOSTESS && \frontend\models\User::hasMessages()): ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sign-in text-right">
                        <?=
                        Html::a(
                                Yii::t('app', 'You have unread messages.'), ["/hostess-profile/index"], ["class" => "unread-message"]
                        )
                        ?>
                    </div>
                </div>
            <?php endif ?>

            <?php if (Yii::$app->params["maintenanceScheduled"]) : ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <?= Yii::t('app', 'System will be under maintenance tonight after 22:00 CET. Please finish all your work until this time.') ?>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row">

            
                <div class="col-xs-12 hidden-lg hidden-md hidden-sm text-center">
                    <p> <i class="fa fa-phone"></i> +492234 4301397 </p>
                </div>
                
                <div class="col-lg-2 col-md-3 col-sm-3 hidden-xs">
                    <i class="fa fa-phone"></i> +492234 4301397
                </div>
                
                <?php if (Yii::$app->user->isGuest): ?>

                    <div class="col-lg-9 col-md-7 col-sm-7 col-xs-8 sign-in">
                        <?= Html::a(Yii::t('app', "Login"), ["//site/login"]) ?>
                        |
                        <?= Html::a(Yii::t('app', 'Free registration'), ["/site/register"]) ?>
                    </div>

                <?php else: ?>

                    <div class="col-lg-9 col-md-7 col-sm-7 col-xs-8 sign-in">
                        <?php if (Yii::$app->user->identity["role"] == \common\models\User::ROLE_HOSTESS) : ?>
                            <?= Html::a(Yii::t('app', "Profile"), ["/hostess-profile/index"]) ?>     
                        <?php elseif (Yii::$app->user->identity["role"] == \common\models\User::ROLE_EXHIBITOR) : ?>
                            <?= Html::a(Yii::t('app', "Profile"), ["/exhibitor/profile"]) ?> 
                        <?php elseif (Yii::$app->user->identity["role"] == \common\models\User::ROLE_ADMIN) : ?>
                            <?= Html::a(Yii::t('app', "Administration"), Yii::$app->urlManagerBackend->createAbsoluteUrl(["site/index"])) ?>  
                        <?php endif ?>
                        |
                        <?= Html::a(Yii::t('app', 'Logout'), ["/site/logout"]) ?>
                    </div>
                <?php endif; ?>
                
                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4 text-right">
                    <?= Html:: beginForm('/site/set-lng', 'post'); ?> 

                    <?=
                    Select2::widget([
                        'id' => 'header-select',
                        'name' => 'lang',
                        'value' => Yii::$app->language,
                        'data' => Yii::$app->params["languages"],
                        'hideSearch' => true,
                        'pluginLoading' => true,
                        'options' => [
                            'onChange' => 'js:this.form.submit();'
                        ],
                    ]);
                    ?>

                    <?= Html::endForm(); ?>
                </div>   
            
            </div>

            
        </div>

    </div>
</div>
<?php
NavBar::begin([
    'brandLabel' => '',
    'brandUrl' => $brandUrl,
    'options' => [
        'class' => 'navbar navbar-default',
    ],
    'brandOptions' => [
        'class' => 'navbar-brand navbar-logo',
        'style' => 'background-image: url("' . Yii::$app->urlManager->createAbsoluteUrl(["images/logo.png"]) . '")'
    ]
]);
?>


<div class="navbar-nav navbar-right navbar-menu">    

    <?php if (Yii::$app->user->isGuest): ?>
        <?= Html::a(Yii::t('app', 'Home'), ['/'], ['class' => $this->context->getRoute() === 'site/index' ? 'active menu-item' : ' menu-item']) ?>
    <?php else: ?>

        <?php if (Yii::$app->user->identity["role"] == \common\models\User::ROLE_HOSTESS) : ?>
            <?= Html::a(Yii::t('app', 'Home'), ['/site/hostess'], ['class' => $this->context->getRoute() === 'site/hostess' ? 'active menu-item' : ' menu-item']) ?> 
        <?php else: ?>
            <?= Html::a(Yii::t('app', 'Home'), ['/'], ['class' => $this->context->getRoute() === 'site/index' ? 'active menu-item' : ' menu-item']) ?>
        <?php endif ?>

    <?php endif; ?>


    <?= Html::a(Yii::t('app', 'About'), ['/site/#about'], ['class' => $this->context->getRoute() === '/site/#about' ? 'active menu-item scroll' : 'scroll menu-item']) ?> 
    <?= Html::a(Yii::t('app', 'Pricing'), ['/site/#pricing'], ['class' => $this->context->getRoute() === '/site/#pricing' ? 'active menu-item scroll' : 'scroll menu-item']) ?> 
    <?= Html::a(Yii::t('app', 'Contact'), ['/site/#contact'], ['class' => $this->context->getRoute() === '/site/#contact' ? 'active menu-item scroll' : 'scroll menu-item']) ?> 
    <?= Html::a(Yii::t('app', 'Blog'), ['/blog'], ['class' => $this->context->getRoute() === 'blog/index' ? 'active menu-item' : 'menu-item']) ?> 




    <?php if (Yii::$app->user->isGuest): ?>
        <?= Html::a(Yii::t('app', "Hostess search"), ['/hostess/index'], ['class' => $this->context->getRoute() === 'hostess/index' ? 'active menu-item' : ' menu-item']) ?> 
        <?= Html::a(Yii::t('app', "I'm a hostess"), ['/site/hostess'], ['class' => $this->context->getRoute() === 'site/hostess' ? ' active menu-item' : 'menu-item']) ?> 

    <?php else : ?>
        <?php if (Yii::$app->user->identity["role"] == \common\models\User::ROLE_HOSTESS): ?>
            <?= Html::a(Yii::t('app', "Upcoming events"), ['/event/index'], ['class' => $this->context->getRoute() === 'event/index' ? 'active hostess-menu-item menu-item' : 'hostess-menu-item menu-item']) ?> 
        <?php else : ?>
            <?= Html::a(Yii::t('app', "Hostess search"), ['/hostess/index'], ['class' => $this->context->getRoute() === 'hostess/index' ? 'active hostess-menu-item menu-item' : 'hostess-menu-item menu-item']) ?> 
        <?php endif; ?>
    <?php endif; ?>
</div>

<?php NavBar::end(); ?>



