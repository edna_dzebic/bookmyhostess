<?php
$this->title = Yii::t('app', 'NEWS BLOG');

\frontend\assets\BlogAsset::register($this);
?>

<div class="row">
    <div class="col-lg-4">
        <div class="green-line"></div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-title">
        <?= Yii::t('app', 'NEWS BLOG') ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 bh-subtitle">
        <?= Yii::t('app', 'Who we are and why we do what we do') ?>
    </div>
</div>

<div class="row">
<?php foreach ($blogs as $blog): ?>

    <?= $this->render("_item", ["model" => $blog]) ?>

<?php endforeach; ?>    
</div>

