<?php

use yii\helpers\Html;

\frontend\assets\BlogAsset::register($this);

$title = "BOOKmyHOSTESS.com";
$desc = $model->title;

$shareUrl = Yii::$app->urlManager->createAbsoluteUrl(["blog/view", "id" => $model->id]);

$imgShareUrl = Yii::$app->urlManager->createAbsoluteUrl([$model->image]);

$this->registerMetaTag(['property' => 'og:title', 'content' => $title]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $desc]);
$this->registerMetaTag(['property' => 'og:url', 'content' => $shareUrl]);
$this->registerMetaTag(['property' => 'og:image', 'content' => $imgShareUrl]);
$this->registerMetaTag(['property' => 'og:type', 'content' => 'article']);

$this->registerMetaTag(['property' => 'twitter:image', 'content' => $imgShareUrl]);
$this->registerMetaTag(['property' => 'twitter:image:src', 'content' => $imgShareUrl]);
$this->registerMetaTag(['property' => 'twitter:card', 'content' => 'summary_large_image']);
$this->registerMetaTag(['property' => 'twitter:site', 'content' => "@bookmyhostess"]);
$this->registerMetaTag(['property' => 'twitter:creator', 'content' => "@bookmyhostess"]);
$this->registerMetaTag(['property' => 'twitter:url', 'content' => $shareUrl]);
$this->registerMetaTag(['property' => 'twitter:title', 'content' => $title]);
$this->registerMetaTag(['property' => 'twitter:description', 'content' => $desc]);
$this->registerMetaTag(['property' => 'twitter:domain', 'content' => "https://www.bookmyhostess.com"]);
?>
<div class="row blog-view">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <h1>
                    <?= $model->title ?>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <span><?=
                    Yii::t('app', 'Posted by: {username} on {date}', [
                        'username' => $model->created_by_username,
                        'date' => Yii::$app->util->formatDate($model->date_created)
                    ])
                    ?></span>
            </div>
        </div>
        <br/>
        <div class="row">           
            <div class="col-xs-12">
                <div class="shariff" data-services="[&quot;facebook&quot;,&quot;twitter&quot;]" data-lang="<?= Yii::$app->language ?>"></div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-12 short-content">
                <?= $model->short_content ?>
            </div> 
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-12">
                <?= Html::img(Yii::$app->urlManager->createAbsoluteUrl([$model->image]), ["class" => "img-responsive", "style" => "margin : 0 auto;"]) ?>
            </div> 
            <div class="col-xs-12 img-explanation text-center">
                <?=
                $model->image_explanation
                ?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-12 content">
                <?= $model->content ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12">
                <?= Html::a(Yii::t('app', "Go back to post list"), Yii::$app->urlManager->createUrl(["//blog/index"]), ["class" => "btn btn-standard"]); ?>
            </div>
        </div>
    </div>
</div>