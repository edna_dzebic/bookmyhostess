<?php

use yii\helpers\Html;

$image = \common\components\PhpThumb::Url([
            'src' => $model->image,
            'w' => 360,
            'h' => 270,
            'zc' => 'T',
            'q' => 90,
        ]);

?>

<div class="col-xs-12 blog-single-item">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <?= Html::img($image, ["class" => "img-responsive", "style" => "margin : 0 auto;"]) ?>
        </div> 
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="blog-title">
                        <?= $model->title ?>
                    </h3>
                </div>
                <div class="col-lg-12">
                    <p>
                        <?=
                        Yii::t('app', 'Posted by: {username} on {date}', [
                            'username' => $model->created_by_username,
                            'date' => Yii::$app->util->formatDate($model->date_created)
                        ])
                        ?>
                    </p>
                </div>
                <div class="col-lg-12 short-content">
                    <?= $model->short_content ?>
                </div>
                <div class="col-lg-12 text-right">
                    <?= Html::a(Yii::t('app', "Continue reading"), Yii::$app->urlManager->createUrl(["//blog/view", "id" => $model->id]), ["class" => "btn btn-standard btn-view-post"]) ?>
                </div>
            </div>
        </div>
    </div>
</div>