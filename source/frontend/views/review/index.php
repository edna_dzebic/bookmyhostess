<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;
use kartik\widgets\Alert;

$pluginOptions = [
    'step' => 0.5,
    'readonly' => false,
    'showClear' => false,
    'showCaption' => true,
    'size' => 'xs',
    'defaultCaption' => '{rating}',
    'clearCaption' => Yii::t('app.exhibitor', '0 stars'),
    'starCaptions' => [
        0 => Yii::t('app.exhibitor', "Extremely bad"),
        1 => Yii::t('app.exhibitor', "Very bad"),
        2 => Yii::t('app.exhibitor', "Bad"),
        3 => Yii::t('app.exhibitor', "Ok"),
        4 => Yii::t('app.exhibitor', "Good"),
        5 => Yii::t('app.exhibitor', "Excellent")
    ]
];

\frontend\assets\ReviewAsset::register($this);


$this->title = Yii::t('app.exhibitor', 'Review');
?>


<div class="hostess-request-form">
    <div class="row">
        <div class="col-lg-2 hidden-md hidden-sm hidden-xs">
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="green-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-title">
                    <?= Yii::t('app.exhibitor', 'REVIEW') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 bh-subtitle">
                    <?= Yii::t('app.exhibitor', 'Please review following users') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php
                    if (($flash = Yii::$app->session->getFlash('error')))
                    {
                        echo Alert::widget(
                                [
                                    'type' => Alert::TYPE_SUCCESS,
                                    'options' => ['class' => 'alert-danger'],
                                    'body' => $flash,
                                    'showSeparator' => true,
                                    'delay' => 0
                                ]
                        );
                    }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php $form = ActiveForm::begin(['id' => 'hostess-review-form']); ?>
                    <?php foreach ($models as $model) : ?>
                        <div class="row margin-top-20">
                            <div class="col-xs-12">
                                <div class="review-item">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="green-text"> <?= Yii::t('app.exhibitor', 'Event') ?>: <b> <?= $model->event->name . " ( " . Yii::$app->util->formatDate($model->eventRequest->date_from) . " - " . Yii::$app->util->formatDate($model->eventRequest->date_to) . " ) " ?></b></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label class="green-text"> <b>  <?= $model->hostess->username ?></b></label>
                                        </div>
                                    </div>                                   
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  margin-top-10">
                                            <?= Html::img($model->hostess->hostessProfile->getThumbnailImageUrl(360, 400), ["class" => "img-responsive"]) ?>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 margin-top-10">

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label><?= Yii::t('app.exhibitor', 'Rating') ?></label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <?=
                                                    StarRating::widget([
                                                        'name' => "Review[$model->id][rating]",
                                                        'value' => isset($post["Review"][$model->id]["rating"]) ? $post["Review"][$model->id]["rating"] : 5,
                                                        'pluginOptions' => $pluginOptions
                                                    ]);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label><?= Yii::t('app.exhibitor', 'Comment') ?></label>
                                                </div>
                                                <div class="col-xs-12">
                                                    <?= Html::textarea("Review[$model->id][comment]", isset($post["Review"][$model->id]["comment"]) ? $post["Review"][$model->id]["comment"] : '', ["class" => "form-control", "rows" => 6, "placeholder" => Yii::t('app.exhibitor', 'Please write your comment')]) ?>
                                                </div>
                                            </div>
                                            <?= Html::input("hidden", "Review[$model->id][event_id]", $model->event_id) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="row">
                        <div class="col-xs-12 margin-top-20">
                            <?= Html::submitButton(Yii::t('app.exhibitor', 'SAVE REVIEW'), ['class' => 'btn btn-red full-width']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-2 hidden-md hidden-sm hidden-xs">
        </div>
    </div>
</div>