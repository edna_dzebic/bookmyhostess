<?php

namespace frontend\models;

class Language extends \common\models\Language
{

    /**
     * @return array
     */
    public static function getAllTranslated()
    {
        $models = static::find()->all();

        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->getTranslatedName();
        }

        asort($result);

        return $result;
    }

}
