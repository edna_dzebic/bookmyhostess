<?php

namespace frontend\models;

use Yii;
use common\models\Email;

class NewsletterSubscriber extends \common\models\NewsletterSubscriber
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['status_id', 'updated_by', 'created_by'], 'integer'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    public function loadAjax($post)
    {

        if (empty($post["newsletterEmail"]))
        {
            return false;
        }

        $this->email = $post["newsletterEmail"];
        return true;
    }

    public function signUp()
    {

        $this->status_id = Yii::$app->status->active;

        if (static::findByEmail($this->email) == null)
        {
            if ($this->save())
            {
                return Email::sendNewsletterSubscribeEmail($this);
            }
        }

        return false;
    }

    public function unsubscribe()
    {
        $this->status_id = Yii::$app->status->deleted;
        return $this->update() !== false;
    }

    /**
     * Finds subscriber by id
     *
     */
    public static function findById($id)
    {

        $model = static::findOne([
                    'id' => $id,
                    'status_id' => Yii::$app->status->active
        ]);

        if ($model == null)
        {
            throw new \yii\web\NotFoundHttpException(Yii::t('app', 'ID is not valid.'));
        } else
        {
            return $model;
        }
    }

    public static function findByEmail($email)
    {
        return static::findOne([
                    'email' => $email,
                    'status_id' => Yii::$app->status->active
        ]);
    }

}
