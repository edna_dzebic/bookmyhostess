<?php

namespace frontend\models;

use Yii;

/**
 * Event class with functions for frontend
 */
class Event extends \common\models\Event
{

    /**
     * Get prices for event based on country pricing details
     * @return array
     */
    public function getPrices()
    {
        $prices = [];

        if (isset($this->country))
        {
            foreach ($this->country->countryPrices as $price)
            {
                $prices[$price->value] = $price->value;
            }
        }

        return $prices;
    }

    /**
     * Get pricing details for hostess for selected event
     * @return array
     */
    public function getOptionsForPriceDropdown()
    {
        $options = [];

        if (isset($this->country))
        {
            foreach ($this->country->countryPrices as $price)
            {
                $priceClasses = explode(',', $price->price_class);

                if (!in_array(Yii::$app->user->identity->hostessProfile->price_class, $priceClasses))
                {
                    $options[$price->value] = [
                        'disabled' => true,
                        "title" => Yii::t('app.hostess', 'Sorry. Your reliability index is too low')
                    ];
                }
            }

            // in case all prices will be disabled, allow at least one
            if (count($options) > 0 && count($options) === count($this->country->countryPrices))
            {
                unset($options[0]);
            }
        }

        return $options;
    }
}
