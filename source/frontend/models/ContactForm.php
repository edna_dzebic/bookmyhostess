<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Email;

/**
 * Contact form
 */
class ContactForm extends Model
{

    const CAPTCHA_VALUE = 'cr720bmh19';

    public $name;
    public $email;
    public $url;
    public $message;
    public $subject;
    public $captcha;
    public $dsgvo;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->captcha = self::CAPTCHA_VALUE;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['name', 'email', 'message'], 'required'],
            [['subject', 'url'], 'safe'],
            ['email', 'email'],
            [['name', 'email', 'message', 'subject', 'url', 'captcha', 'dsgvo'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            ['dsgvo', 'required', "message" => Yii::t('app', 'You have to accept Privacy Policy in order to continue.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'url' => Yii::t('app', 'Website'),
            'message' => Yii::t('app', 'Message'),
            'subject' => Yii::t('app', 'Subject'),
            'captcha' => Yii::t('app', 'Captcha'),
            'dsgvo' => Yii::t('app', 'I have read and accepted <a href="https://www.bookmyhostess.com/page/privacy">data protection declaration </a>. I agree that my form details for processing and answering this request will be stored, processed, transmitted and used and that BOOKmyHOSTESS.com can send me relevant, request-related information by post, e-mail or telephone. The declaration of consent can be revoked at any time in writing by e-mail to dataprotection@bookmyhostess.com with effect for the future. <br> Further information on data protection can be found at <a href="https://www.bookmyhostess.com/page/privacy">https://www.bookmyhostess.com/page/privacy</a>'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        if ($this->save())
        {
            return true;
            return Email::sendContactUsEmail($this);
        }

        return false;
    }

    /**
     * Validate form
     * @param array $attributeNames
     * @param boolean $clearErrors
     * @return boolean
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);
                
        if(empty($this->dsgvo))
        {
            $this->addError('dsgvo', Yii::t('app', 'You have to accept Privacy Policy in order to continue.'));
        }

        if (empty($this->captcha) || $this->captcha !== self::CAPTCHA_VALUE)
        {
            $this->addError('name', Yii::t('app', 'Invalid data!'));
        }

        return !$this->hasErrors();
    }

    private function save()
    {
        $model = new \common\models\ContactForm();

        $model->setAttributes($this->getAttributes());

        return $model->save();
    }
}
