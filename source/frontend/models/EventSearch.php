<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;

class EventSearch extends Model
{

    const SORT_KEY_DATE_UP = 'date_up';
    const SORT_KEY_DATE_DOWN = 'date_down';

    public $country_id = [];
    public $city_id = [];
    public $event;
    public $sort;
    public $pageSize = 20;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'city_id', 'event', 'sort'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public static function findSingleEvent($id)
    {
        $query = Event::find()
                ->with(["city.country.countryPrices", "type"])
                ->andWhere(["event.id" => $id])
                ->andWhere("event.end_date>=NOW()")
                ->andWhere(["event.status_id" => Yii::$app->status->active]);

        self::addHostessFilter($query);

        return $query->one();
    }

    /**
     * Main search for events
     * @param array $params
     * @return ActiveDataProvider
     */
    public function getSearchProvider($params = [])
    {
        $this->loadParamsForFilter($params);

        $query = Event::find()
                ->with(["city.country.countryPrices", "type"])
                ->joinWith(['city.country'])
                ->andWhere("event.end_date>=NOW()")
                ->andWhere(["event.status_id" => Yii::$app->status->active]);

        $query->andFilterWhere([
            'event.country_id' => $this->country_id,
            'event.city_id' => $this->city_id,
            'event.id' => $this->event
        ]);

        //$query->andFilterWhere(['like', 'event.name', $this->event]);

        self::addHostessFilter($query);

        $query->groupBy(["event.id"]);

        //sort
        $defaultOrder = [];
        switch ($this->sort)
        {
            case self::SORT_KEY_DATE_UP:
                $defaultOrder = ['event.start_date' => SORT_ASC, "event.name" => SORT_ASC];
                break;
            case self::SORT_KEY_DATE_DOWN:
                $defaultOrder = ['event.start_date' => SORT_DESC, "event.name" => SORT_ASC];
                break;
            default:
                $defaultOrder = ['event.start_date' => SORT_ASC, "event.name" => SORT_ASC];
        }

        $sort = new Sort([
            'attributes' => [
                'event.start_date',
                "event.name"
            ],
            'defaultOrder' => $defaultOrder
        ]);

        $query->orderBy($sort->orders);
        
        //die( $query->createCommand()->getRawSql());

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
            'sort' => $sort
        ]);

        return $dataProvider;
    }

    /**
     * Load params into model attributes
     * @param array $params
     */
    public function loadParamsForFilter($params)
    {
        if (!empty($params))
        {
            $this->load($params);
        } else
        {
            // TODO filter is not stored in session !!!
            $json = Yii::$app->session->get('event.filter');
            if (isset($json))
            {
                $get = json_decode($json, true);
                $this->load($get);
            }
        }
    }

    /**
     * TODO not used!?
     * Get age list for filter dropdown
     * @return array
     */
    public function getAgeList()
    {
        return range(18, 90);
    }

    /**
     * Returns list of event names as an array for some city or country
     * @param int|null $cityId
     * @param int|null $countryId
     * @return array
     */
    public static function getDataForTypeahead($cityId = null, $countryId = null)
    {
        $query = Event::find()
                ->with(["city.country.countryPrices", "type"])
                ->andWhere("DATE(event.end_date)>=DATE(NOW())")
                ->andWhere("event.status_id = " . Yii::$app->status->active)
                ->orderBy('event.name ASC');

        if (!empty($countryId))
        {
            $query->andWhere("event.country_id = " . $countryId);
        }

        if (!empty($cityId))
        {
            $query->andWhere("event.city_id = " . $cityId);
        }

        self::addHostessFilter($query);

        $query->groupBy(["event.id"]);

        $events = $query->all();

        $data = [];
        foreach ($events as $event)
        {
            $data[$event->id] = $event['name'];
        }

        return $data;
    }

    /**
     * 
     * @return array
     */
    public static function getSortList()
    {
        $data = [
            self::SORT_KEY_DATE_UP => Yii::t('app', 'Start Date Up'),
            self::SORT_KEY_DATE_DOWN => Yii::t('app', 'Start Date Down'),
        ];

        return $data;
    }

    private static function addHostessFilter(\yii\db\ActiveQuery &$query)
    {
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isHostess)
        {
            $query->innerJoin(
                    'hostess_country_mapping', "event.country_id = hostess_country_mapping.country_id"
            );

            $query->andWhere(["hostess_country_mapping.status_id" => Yii::$app->status->active]);
            $query->andWhere(["hostess_country_mapping.user_id" => Yii::$app->user->id]);
        }
    }

}
