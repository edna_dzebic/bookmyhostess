<?php

namespace frontend\models;

use Yii;
use common\models\Gallery as GalleryModel;

class Gallery extends \yii\base\Model
{

    /**
     * Get url for thumbnail image
     * @param int $w
     * @param int $h
     * @return string|null
     */
    public static function getThumbnailImageUrl($name, $w, $h)
    {
        return \common\components\PhpThumb::Url([
                    'src' => $name,
                    'w' => $w,
                    'h' => $h,
                    'zc' => 'T',
                    'q' => 90,
        ]);
    }

    public static function getHiddenImages()
    {
        return static::getQuery()
                        ->andWhere(["include_on_home" => 0])
                        ->all();
    }

    public static function getVisibleThumbnails()
    {
        $images = static::getVisibleImages();
        return static::returnThumbs($images);
    }

    public static function getHiddenThumbnails()
    {
        $images = static::getHiddenImages();
        return static::returnThumbs($images);
    }

    private static function returnThumbs($images)
    {
        $result = [];
        foreach ($images as $image)
        {
            $result[] = [
                'thumb' => static::getThumbnailImageUrl($image->path, 320, 300),
                'full' => $image->path,
                'title' => $image->title,
                'alt' => $image->alt
            ];
        }

        return $result;
    }

    private static function getVisibleImages()
    {
        return static::getQuery()
                        ->andWhere(["include_on_home" => 1])
                        ->limit(8)
                        ->all();
    }

    private static function getQuery()
    {
        return GalleryModel::find()
                        ->andWhere(["status_id" => Yii::$app->status->active])
                        ->orderBy([
                            "sort_order" => SORT_ASC,
                            "date_updated" => SORT_DESC,
                            "id" => SORT_DESC
        ]);
    }

}
