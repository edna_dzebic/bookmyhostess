<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use common\models\Coupon;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $created_at
 * @property string $amount
 * @property string $receiver_email
 * @property string $receiver_id
 * @property string $residence_country
 * @property string $test_ipn
 * @property string $transaction_subject
 * @property string $txn_id
 * @property string $txn_type
 * @property string $payer_email
 * @property string $payer_id
 * @property string $payer_status
 * @property string $first_name
 * @property string $last_name
 * @property string $address_city
 * @property string $address_country
 * @property string $address_state
 * @property string $address_status
 * @property string $address_country_code
 * @property string $address_name
 * @property string $address_street
 * @property string $address_zip
 * @property string $custom
 * @property string $handling_amount
 * @property string $item_name
 * @property string $item_number
 * @property string $mc_currency
 * @property string $mc_fee
 * @property string $mc_gross
 * @property string $payment_date
 * @property string $payment_fee
 * @property string $payment_gross
 * @property string $payment_status
 * @property string $payment_type
 * @property string $protection_eligibility
 * @property string $quantity
 * @property string $shipping
 * @property string $tax
 * @property string $notify_version
 * @property string $charset
 * @property string $verify_sign
 * @property string $coupon_code;
 * 
 * @property User $user
 * @property CreditTransaction[] $creditTransactions
 */
class Payment extends ActiveRecord
{

    private $coupon_code;
    private $coupon_model;
    private $user_model;
    private $exhibitor_model;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'coupon_code'], 'safe'],
            [['amount', 'user_id'], 'number'],
            [['user_id', 'amount', 'txn_id', 'mc_gross', 'mc_currency', 'payment_status', 'custom', 'business'], 'required'],
            [
                [
                    'business',
                    'receiver_email',
                    'receiver_id',
                    'residence_country',
                    'test_ipn',
                    'transaction_subject',
                    'txn_id',
                    'txn_type',
                    'payer_email',
                    'payer_id',
                    'payer_status',
                    'first_name',
                    'last_name',
                    'address_city',
                    'address_country',
                    'address_state',
                    'address_status',
                    'address_country_code',
                    'address_name',
                    'address_street',
                    'address_zip',
                    'custom',
                    'handling_amount',
                    'item_name',
                    'item_number',
                    'mc_currency',
                    'mc_fee',
                    'mc_gross',
                    'payment_date',
                    'payment_fee',
                    'payment_gross',
                    'payment_status',
                    'payment_type',
                    'protection_eligibility',
                    'quantity',
                    'shipping',
                    'tax',
                    'notify_version',
                    'charset',
                    'verify_sign'
                ], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'created_at' => Yii::t('admin', 'Created At'),
            'amount' => Yii::t('admin', 'Amount'),
            'receiver_email' => Yii::t('admin', 'Receiver Email'),
            'receiver_id' => Yii::t('admin', 'Receiver ID'),
            'residence_country' => Yii::t('admin', 'Residence Country'),
            'test_ipn' => Yii::t('admin', 'Test Ipn'),
            'transaction_subject' => Yii::t('admin', 'Transaction Subject'),
            'txn_id' => Yii::t('admin', 'Txn ID'),
            'txn_type' => Yii::t('admin', 'Txn Type'),
            'payer_email' => Yii::t('admin', 'Payer Email'),
            'payer_id' => Yii::t('admin', 'Payer ID'),
            'payer_status' => Yii::t('admin', 'Payer Status'),
            'first_name' => Yii::t('admin', 'First Name'),
            'last_name' => Yii::t('admin', 'Last Name'),
            'address_city' => Yii::t('admin', 'Address City'),
            'address_country' => Yii::t('admin', 'Address Country'),
            'address_state' => Yii::t('admin', 'Address State'),
            'address_status' => Yii::t('admin', 'Address Status'),
            'address_country_code' => Yii::t('admin', 'Address Country Code'),
            'address_name' => Yii::t('admin', 'Address Name'),
            'address_street' => Yii::t('admin', 'Address Street'),
            'address_zip' => Yii::t('admin', 'Address Zip'),
            'custom' => Yii::t('admin', 'Custom'),
            'handling_amount' => Yii::t('admin', 'Handling Amount'),
            'item_name' => Yii::t('admin', 'Item Name'),
            'item_number' => Yii::t('admin', 'Item Number'),
            'mc_currency' => Yii::t('admin', 'Mc Currency'),
            'mc_fee' => Yii::t('admin', 'Mc Fee'),
            'mc_gross' => Yii::t('admin', 'Mc Gross'),
            'payment_date' => Yii::t('admin', 'Payment Date'),
            'payment_fee' => Yii::t('admin', 'Payment Fee'),
            'payment_gross' => Yii::t('admin', 'Payment Gross'),
            'payment_status' => Yii::t('admin', 'Payment Status'),
            'payment_type' => Yii::t('admin', 'Payment of the daily rate'),
            'protection_eligibility' => Yii::t('admin', 'Protection Eligibility'),
            'quantity' => Yii::t('admin', 'Quantity'),
            'shipping' => Yii::t('admin', 'Shipping'),
            'tax' => Yii::t('admin', 'Tax'),
            'notify_version' => Yii::t('admin', 'Notify Version'),
            'charset' => Yii::t('admin', 'Charset'),
            'verify_sign' => Yii::t('admin', 'Verify Sign'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => function()
        {
            return date("Y-m-d H:i:s");
        }
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function processPayment($post)
    {
        $status = $post["payment_status"];

        if ($this->isPending($status))
        {
            return false;
        }
        if ($this->isDenied($status))
        {
            return $this->processDeniedPayment($post);
        }
        if ($this->isCompleted($status))
        {
            return $this->processCompletePayment($post);
        }

        return false;
    }

    /**
     * @return \common\models\ExhibitorProfile
     */
    public function getExhibitorModel()
    {
        return $this->exhibitor_model;
    }

    /**
     * @return \common\models\User
     */
    public function getUserModel()
    {
        return $this->user_model;
    }

    /**
     * @return \common\models\Coupon
     */
    public function getCoupon()
    {
        return $this->coupon_model;
    }

    private function processCompletePayment($post)
    {

        if ($this->verify($post))
        {
            $paymentPost = ["Payment" => $post];
            if ($this->load($paymentPost))
            {
                $this->setCoupon($post);

                $model = static::findOne([
                            "txn_id" => $this->txn_id,
                            "payment_status" => $this->payment_status
                ]);
                if (!isset($model))
                {
                    $this->setAmount();
                    if ($this->save())
                    {
                        return true;
                    } else
                    {
                        Yii::error("UNABLE TO SAVE PAYMENT");
                        Yii::error(var_export($this->errors, true));
                    }
                } else
                {
                    Yii::error("PAYMENT ALREADY PROCESSED");
                }
            } else
            {
                Yii::error("UNABLE TO LOAD POST");
            }
        } else
        {
            Yii::error("POST NOT OK");
        }

        return false;
    }

    private function processDeniedPayment($post)
    {
        Yii::warning("PROCESS DENIED PAYMENT");
        Email::sendPaypalDeniedTransactionEmail($post);
        return false;
    }

    private function isCompleted($status)
    {
        return $status == "Completed";
    }

    private function isDenied($status)
    {
        return $status == "Denied";
    }

    private function isPending($status)
    {
        return $status == "Pending";
    }

    public function verify($post)
    {
        $custom = isset($post["custom"]) ? $post["custom"] : null;
        $business = isset($post["business"]) ? $post["business"] : null;
        $mc_gross = isset($post["mc_gross"]) ? $post["mc_gross"] : null;
        $currency = isset($post["mc_currency"]) ? $post["mc_currency"] : null;

        return $this->verifyCustomAndUser($custom) &&
                $this->verifyBusiness($business) &&
                $this->verifyMcGross($mc_gross) &&
                $this->verifyCurrency($currency);
    }

    private function verifyCustomAndUser($custom)
    {
        $parts = explode('_', $custom);

        if (count($parts) != 2)
        {
            return false;
        }

        $user_id = $parts[1];

        if (($userModel = $this->findUser($user_id)) != null)
        {
            $this->user_model = $userModel;
            $this->exhibitor_model = $userModel->exhibitorProfile;
            $this->user_id = $userModel->id;
            $paypal_custom = $parts[0];
            return sha1(Yii::$app->params["paypal"]["custom"]) == $paypal_custom;
        } else
        {
            Yii::error("Cannot find user for Paypal!");
        }

        return false;
    }

    private function verifyBusiness($business)
    {
        return Yii::$app->params["paypal"]["business"] == $business;
    }

    private function verifyMcGross($mc_gross)
    {

        if (is_numeric($mc_gross))
        {
            return $mc_gross > 0;
        }

        return false;
    }

    private function findUser($id)
    {
        return User::findOne(["id" => $id, "role" => User::ROLE_EXHIBITOR]);
    }

    private function verifyCurrency($currency)
    {
        return Yii::$app->params["paypal"]["currency_code"] == $currency;
    }

    private function setAmount()
    {
        $this->amount = $this->mc_gross;
    }

    private function setCoupon($post)
    {
        if (isset($post["option_name1"]) && isset($post["option_selection1"]))
        {
            $this->coupon_code = $post["option_selection1"];

            $coupon_model = Coupon::findOne(["code" => $this->coupon_code]);

            if (isset($coupon_model))
            {
                Yii::info("Coupon model found for code " . $this->coupon_code);
                $this->coupon_model = $coupon_model;
            } else
            {
                Yii::info("Coupon model not found for code " . $this->coupon_code);
            }
        } else
        {
            Yii::info("Option name 1 not in post request!");
            $this->coupon_code = "";
            $this->coupon_model = null;
        }
    }

}
