<?php

namespace frontend\models;

use common\models\City;
use common\models\Country;
use common\models\Email;
use common\models\Event;
use common\models\EventJob;
use common\models\HairColor;
use common\models\Language;
use frontend\models\Gender;
use Yii;
use yii\base\Model;

/**
 * Hostess Request Form
 */
class HostessRequestForm extends Model
{

    public $additional_description;
    private $filter;
    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['additional_description'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'additional_description' => Yii::t('app.exhibitor', 'Additional Description')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        return Email::sendHostessSearchRequestEmail($this);
    }

    public function setFilter($newValue)
    {
        $this->filter = $newValue;
    }

    public function setUser($newValue)
    {
        $this->user = $newValue;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getFilter()
    {
        $filter = [];

        foreach ($this->filter as $key => $value)
        {
            if ($key == 'sort') continue;
            if ($value !== '')
            {
                switch ($key)
                {
                    case 'languages':
                        $values = Language::find()->select('name')->where(['in', 'id', $value])->asArray()->all();
                        $value = implode(', ', array_column($values, 'name'));
                        break;
                    case 'country':
                        $model = Country::findOne($value);
                        $value = $model->name;
                        break;
                    case 'city':
                        $model = City::findOne($value);
                        $value = $model->name;
                        break;
                    case 'event':
                        $model = Event::findOne($value);
                        $value = $model->name;
                        break;
                    case 'jobs':
                        $model = EventJob::findOne($value);
                        $value = $model->name;
                        break;
                    case 'hair_colors':
                        $model = HairColor::findOne($value);
                        $value = $model->name;
                        break;
                    case 'genders':
                        $value = Gender::getListForHostess()[$value == 1 ? 1 : 0];
                        break;
                    case 'has_work_experience':
                    case 'has_health_certificate':
                    case 'has_driving_licence':
                    case 'trained':
                    case 'has_mobile_app':
                        $value = $value == 1 ? Yii::t('admin', 'Yes') : Yii::t('admin', 'No');
                }
            } else
            {
                $value = Yii::t('admin', 'Not set');
            }
            $filter[$key] = $value;
        }

        return $filter;
    }

    public function getFilterTranslations()
    {
        return [
            'ages' => Yii::t('admin', 'Ages'),
            'languages' => Yii::t('admin', 'Languages'),
            'country' => Yii::t('admin', 'Country'),
            'city' => Yii::t('admin', 'City'),
            'event' => Yii::t('admin', 'Event'),
            'jobs' => Yii::t('admin', 'Job'),
            'hair_colors' => Yii::t('admin', 'Hair Color'),
            'genders' => Yii::t('admin', 'Gender'),
            'has_health_certificate' => Yii::t('admin', 'Has health certificate'),
            'has_driving_licence' => Yii::t('admin', 'Has driving licence'),
            'trained' => Yii::t('admin', 'Is trained'),
            'has_mobile_app' => Yii::t('admin', 'Has mobile app'),
            'has_work_experience' => Yii::t('admin', 'Has work experience')
        ];
    }
}
