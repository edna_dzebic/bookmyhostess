<?php

namespace frontend\models;

use Yii;
use common\models\Email;

class HostessReview extends \common\models\HostessReview
{

    public static function getExhibitorDataProvider()
    {
        $query = static::find()->joinWith(["hostess.hostessProfile"]);

        $query->andWhere([static::tableName() . ".exhibitor_id" => Yii::$app->user->id]);
        $query->andWhere([static::tableName() . ".status_id" => Yii::$app->status->active]);
        $query->orderBy([static::tableName() . ".date_updated" => SORT_DESC]);

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 4],
        ]);

        return $dataProvider;
    }

    public static function getHostessDataProvider()
    {
        $query = static::find()->joinWith(["hostess.hostessProfile"]);

        $query->andWhere([static::tableName() . ".hostess_id" => Yii::$app->user->id]);
        $query->andWhere([static::tableName() . ".status_id" => Yii::$app->status->active]);
        $query->orderBy([static::tableName() . ".date_updated" => SORT_DESC]);

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 4],
        ]);

        return $dataProvider;
    }

    public static function getDataProviderForUser($user_id)
    {
        $query = static::find()->joinWith(["hostess.hostessProfile"]);

        $query->andWhere([static::tableName() . ".hostess_id" => $user_id]);
        $query->andWhere([static::tableName() . ".status_id" => Yii::$app->status->active]);
        $query->orderBy([static::tableName() . ".date_updated" => SORT_DESC]);

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    public function sendRequestEmailToExhibitor()
    {
        return Email::sendHostessReviewRequestEmailToExhibitor($this);
    }

}
