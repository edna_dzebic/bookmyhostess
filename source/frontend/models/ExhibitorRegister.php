<?php

namespace frontend\models;

use common\models\ExhibitorProfile;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class ExhibitorRegister extends Model
{

    public $email;
    public $password;
    public $first_name;
    public $last_name;
    public $lang;
    public $gender;
    public $email_validation_secret;
    public $staff_demand_type;
    public $terms;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'email', 'first_name', 'last_name', 'staff_demand_type'], 'required', 'message' => Yii::t('app', 'This field cannot be blank.')],
            ['password', 'string', 'min' => 6, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            [['lang', 'gender'], 'required', 'message' => Yii::t('app', 'This field cannot be blank.')],
            ['terms', 'required', "message" => Yii::t('app', 'You have to accept Terms&Conditions and Privacy Policy in order to continue.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'terms' => Yii::t('app', 'I have read and accept the Terms & Conditions & Privacy Policy.'),
            'password' => Yii::t('app', 'Password'),
            'first_name' => Yii::t('app', 'First name'),
            'last_name' => Yii::t('app', 'Last name'),
            'lang' => Yii::t('app', 'Preferred system language'),
            'gender' => Yii::t('app', 'Title'),
            'staff_demand_type' => Yii::t('app', 'Staff demand')
        ];
    }

    /**
     * Signs user up.
     *
     * @return true|false depending on status of save
     */
    public function signup()
    {
        $this->setLanguage();

        if ($this->validate())
        {
            $transaction = Yii::$app->db->beginTransaction();

            $user = $this->saveUser();

            if (is_object($user) && $this->saveProfile($user) !== false)
            {
                $transaction->commit();
                return true;
            }

            $transaction->rollBack();
        }

        return false;
    }

    /**
     * Get full name
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     * Returns validation link for activation that is sent to user email
     */
    public function getValidationLink($refhos = null, $event = null, $cp = null)
    {
        return \yii\helpers\Url::to(["//exhibitor/activate",
                    "id" => $this->email_validation_secret,
                    'ref' => $refhos,
                    "event" => $event,
                    "cp" => $cp,
                    "lang" => $this->lang
                        ], true);
    }

    /**
     * Set chosen language to session and to the app
     */
    private function setLanguage()
    {
        Yii::$app->session->set('lang', $this->lang);
        Yii::$app->language = Yii::$app->session->get('lang');
    }

    /**
     * Sets validation secret
     * @return string
     */
    private function generateEmailValidationSecret()
    {
        $code = sha1(microtime(true) . $this->email);
        $this->email_validation_secret = $code;
        return $code;
    }

    /**
     * Save user to DB using data from form
     * @return User
     */
    private function saveUser()
    {
        $user = new User();
        $user->setAttributes($this->getAttributes());

        $user->role = User::ROLE_EXHIBITOR;
        $user->username = $this->email;
        $user->email = $this->email;
        $user->status_id = Yii::$app->status->pending;
        $user->email_validation_secret = $this->generateEmailValidationSecret();
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->save() !== false)
        {
            return $user;
        }

        $this->addErrors($user->getErrors());
        return null;
    }

    /**
     * Save exhibitor profile using data from form and from user
     * @param User $user
     * @return boolean
     */
    private function saveProfile($user)
    {
        $model = new ExhibitorProfile();

        // by default all exhibitor have normal booking type and price
        $model->booking_type = ExhibitorProfile::BOOKING_TYPE_REGULAR;
        $model->booking_price_discount = 0;
        $model->booking_price_per_day = Yii::$app->settings->booking_price;
        $model->is_invoice_payment_allowed = false;

        $model->user_id = $user->id;
        $model->staff_demand_type = $this->staff_demand_type;
        $model->status_id = Yii::$app->status->pending;

        if ($model->save() !== false)
        {
            return true;
        }

        $this->addErrors($model->getErrors());
        return false;
    }

}
