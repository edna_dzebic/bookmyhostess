<?php

namespace frontend\models;

class HairColor extends \common\models\HairColor
{

    /**
     * Get list of mapped hair colors from id to translated names
     * @return array
     */
    public static function getAllTranslated()
    {
        $models = static::find()->all();

        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->getTranslatedName();
        }

        asort($result);

        return $result;
    }

}
