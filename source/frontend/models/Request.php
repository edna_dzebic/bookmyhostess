<?php

namespace frontend\models;

use Yii;
use common\models\RequestJob;
use common\models\Email;
use yii\data\ActiveDataProvider;

class Request extends \common\models\Request
{

    // DO NOT USE THIS ANYMORE!! USED ONLY FOR MOBILE APP!!!
    const STATUS_ACCEPT = 1;
    const STATUS_REJECT = 2;
    const STATUS_EXPIRED = 3;
    const STATUS_NEW = 4;
    const STATUS_PAYPAL_PENDING = 5;
    const STATUS_EXPIRED_HOSTESS = 6;
    const STATUS_EX_WITHDRAW = 7;
    const STATUS_EX_WITHDRAW_AFTER_ACCEPT = 8;
    const STATUS_H_WITHDRAW_AFTER_ACCEPT = 9;

    /**
     * Get past bookings for hostess
     * @return ActiveDataProvider
     */
    public static function getPastBookingForHostess()
    {
        $provider = new ActiveDataProvider([
            'query' => Request::find()
                    ->joinWith(["event"])
                    ->joinWith(["userRequested"])
                    ->leftJoin('hostess_review', 'hostess_review.event_request_id = request.id')
                    ->andWhere("event.end_date < NOW()")
                    ->andWhere(["event.status_id" => Yii::$app->status->active])
                    ->andWhere(["request.user_id" => Yii::$app->user->id])
                    ->andWhere(["request.status_id" => Yii::$app->status->request_accept])
                    ->andWhere(["request.completed" => 1])
                    ->orderBy('request.id desc'),
            'pagination' => ['pageSize' => 10],
        ]);

        return $provider;
    }

    /**
     * Get current bookings for hostess
     * @return ActiveDataProvider
     */
    public static function getCurrentBookingForHostess()
    {
        $provider = new ActiveDataProvider([
            'query' => static::find()
                    ->joinWith(["event"])
                    ->joinWith(["userRequested"])
                    ->andWhere("event.end_date >= NOW()")
                    ->andWhere(["event.status_id" => Yii::$app->status->active])
                    ->andWhere(["request.user_id" => Yii::$app->user->id])
                    ->andWhere(["request.status_id" => Yii::$app->status->request_accept])
                    ->andWhere(["request.completed" => 1])
                    ->orderBy('request.id desc'),
            'pagination' => ['pageSize' => 10],
        ]);

        return $provider;
    }

    /**
     * Helper method to map new status to old ones!!!
     * Should be deleted after mobile app is updated with new statuses!!!
     * @return int
     */
    public function getOldStatus()
    {
        switch ($this->status_id)
        {
            case Yii::$app->status->request_new:
                return self::STATUS_NEW;
            case Yii::$app->status->request_accept:
                return self::STATUS_ACCEPT;
            case Yii::$app->status->request_reject:
                return self::STATUS_REJECT;
            case Yii::$app->status->request_expired:
                return self::STATUS_EXPIRED;
            case Yii::$app->status->request_paypal_pending:
                return self::STATUS_PAYPAL_PENDING;
            case Yii::$app->status->request_expired_hostess:
                return self::STATUS_EXPIRED_HOSTESS;
            case Yii::$app->status->request_exhibitor_withdraw:
                return self::STATUS_EX_WITHDRAW;
            case Yii::$app->status->request_exhibitor_withdraw_after_accept:
                return self::STATUS_EX_WITHDRAW_AFTER_ACCEPT;
            case Yii::$app->status->request_hostess_withdraw_after_accept:
                return self::STATUS_H_WITHDRAW_AFTER_ACCEPT;
        }
    }

    /**
     * Hostess rejects request
     * @param array $post
     * @return boolean
     */
    public function reject($post)
    {
        $this->load($post);

        if (empty($this->reason))
        {
            $this->addError("reason", Yii::t('app.hostess', 'Rejection reason is required!'));
            return false;
        }

        $this->status_id = Yii::$app->status->request_reject;
        $this->date_responded = date('Y-m-d H:i:s');
        $this->date_rejected = date('Y-m-d H:i:s');

        if (!$this->update(false))
        {
            Yii::error("Failed to update model under hostess reject request");
        } else
        {
            if (Email::sendRejectRequestEmailToExibitor($this))
            {
                return Email::sendAfterRejectEmailToHostess($this->user);
            }
        }

        return false;
    }

    /**
     * Hostess withdraw request after confirmation
     * @param array $post
     * @return boolean
     */
    public function hostessWithdrawConfirmation($post)
    {
        $lang = Yii::$app->language;


        $this->load($post);

        if (empty($this->hostess_withdraw_reason))
        {
            $this->addError("hostess_withdraw_reason", Yii::t('app.hostess', 'Withdraw reason is required!'));
            return false;
        }

        $this->status_id = Yii::$app->status->request_hostess_withdraw_after_accept;
        $this->hostess_withdraw_date = date("Y-m-d");

        if (!$this->update(false))
        {
            Yii::error("Failed to update model under hostess withdraw confirmation");
        } else
        {
            if (Email::sendHostessWithdrawConfirmationEmailToExibitor($this))
            {
                Yii::$app->language = $lang;
                return true;
            }
        }

        return false;
    }

    /**
     * Withdraw request ( exhibitor withdraw previosly sent request on his profile)
     * @param array $post
     * @return boolean
     */
    public function exhibitorWithdraw($post)
    {
        $lang = Yii::$app->language;

        $this->load($post);

        if (empty($this->ex_withdraw_reason))
        {
            $this->addError("ex_withdraw_reason", Yii::t('app.exhibitor', 'Withdraw reason is required!'));
            return false;
        }

        if ($this->status_id == Yii::$app->status->request_new)
        {
            $this->status_id = Yii::$app->status->request_exhibitor_withdraw;
        } else
        {
            $this->status_id = Yii::$app->status->request_exhibitor_withdraw_after_accept;
        }

        $this->ex_withdraw_date = date("Y-m-d");

        if (!$this->update(false))
        {
            Yii::error("Failed to update model under exhibitor withdraw request");
        } else
        {
            if (Email::sendExhibitorWithdrawEmailToHostess($this))
            {
                Yii::$app->language = $lang;
                return true;
            }
        }

        return false;
    }

    /**
     * Get requests for exhibitor profile
     * @return \common\models\ActiveDataProvider
     */
    public static function getExhibitorBookingsDataProvider()
    {
        $query = static::find()
                ->joinWith(["user", "event"])
                ->andWhere(["requested_user_id" => \Yii::$app->user->id])
                ->andWhere("request.completed = 1")
                ->orderBy('request.id desc');

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10]
        ]);
    }

    /**
     * Get past and current requests
     * @return ActiveDataProvider
     */
    public static function getHostessRequestsDataProvider()
    {
        $query = static::find()
                ->joinWith(["event"])
                ->joinWith(["userRequested"])
                ->andWhere(["request.user_id" => Yii::$app->user->id])
                ->andWhere(["<>", 'request.status_id', Yii::$app->status->request_expired])
                ->andWhere(["<>", 'request.status_id', Yii::$app->status->request_expired_hostess])
                ->orderBy('request.id desc');

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10],
        ]);
    }

    /**
     * Create request for given registration model
     * @param \frontend\models\Register $eventRegisterModel
     * @param string|null $afflIdentification
     * @return boolean
     */
    public function createRequest(Register $eventRegisterModel, $afflIdentification)
    {
        $exhibitor = Yii::$app->user;
        $event = $eventRegisterModel->event;

        $this->completed = self::NOT_COMPLETED;
        $this->requested_user_id = $exhibitor->id;
        $this->user_id = $eventRegisterModel->user_id;
        $this->event_id = $eventRegisterModel->event_id;
        $this->status_id = Yii::$app->status->request_new;

        $this->payment_type = Yii::$app->request->get("payment_type");
        $this->event_dress_code = Yii::$app->request->get("event_dress_code");
        $this->date_from = date('Y-m-d H:i:s', strtotime(Yii::$app->request->get("date_from")));
        $this->date_to = date('Y-m-d H:i:s', strtotime(Yii::$app->request->get("date_to")));
        $this->days_to_event = Yii::$app->util->getDaysToEvent($event);
        $this->number_of_days = $this->calculateBookingDays();

        // hostess original price is what she chose when applying to event
        $this->hostess_original_price = $eventRegisterModel->price;
        $this->price = $this->hostess_original_price;

        // last minute by default false
        $this->is_last_minute = false;
        $this->last_minute_percentage = 0;

        // system price based on profile of exhibitor
        $this->original_booking_price_per_day = Yii::$app->user->identity->exhibitorProfile->getBookingPrice($eventRegisterModel->event_id);
        $this->booking_price_per_day = $this->original_booking_price_per_day;

        // set last minute prices and data
        if (Yii::$app->util->isRequestLastMinute($event))
        {
            $this->is_last_minute = true;
            $this->last_minute_percentage = Yii::$app->settings->last_minute_percentage;

            // hostess price per day with last minute surcharge
            $this->price = Yii::$app->util->getLastMinutePrice($this->hostess_original_price);
            $this->booking_price_per_day = Yii::$app->util->getLastMinutePrice($this->original_booking_price_per_day);
        }

        $this->total_booking_price = $this->calculateBookingPrice();

        $this->affiliate_id = $this->getAffiliateId($afflIdentification);
        
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->save())
        {
            $event_jobs = Yii::$app->request->get("event_job");
            foreach ($event_jobs as $index => $id)
            {
                $eventJobModel = new RequestJob();
                $eventJobModel->event_request_id = $this->id;
                $eventJobModel->event_job = $id;
                if (!$eventJobModel->save())
                {
                    $transaction->rollBack();
                    return false;
                }
            }

            $transaction->commit();

            Email::sendBookRequestToHostess($this);

            $this->sendFirebaseNotification();

            return true;
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * How many requests logged in hostess has which are still not answered
     * @return integer
     */
    public static function getHostessPendingCount()
    {
        return static::find()
                        ->andWhere(['request.user_id' => Yii::$app->user->id])
                        ->andWhere(['request.status_id' => Yii::$app->status->request_new])
                        ->count();
    }

    /**
     * Send notification to hostess if sending enabled in settings
     */
    public function sendFirebaseNotification()
    {
        if (Yii::$app->settings->send_firebase_notification)
        {
            $this->sendAndroidNotification();
            $this->sendIOSNotification();
        }
    }

    /**
     * Check if request can be created for given hostess for selected event
     * from logged in exhibitor.
     * It can be created if there are no previos requests with specific status
     * 
     * @param integer $eventId
     * @param integer $hostessId
     * @param integer $exhibitorId
     * @return boolean
     */
    public static function canBeCreated($eventId, $hostessId, $exhibitorId)
    {
        $exists = static::find()
                ->andWhere([
                    "request.event_id" => $eventId,
                    "request.user_id" => $hostessId,
                    'request.requested_user_id' => $exhibitorId
                ])
                ->andWhere(["<>", 'request.status_id', Yii::$app->status->request_expired])
                ->andWhere(['<>', 'request.status_id', Yii::$app->status->request_expired_hostess])
                ->andWhere(["<>", 'request.status_id', Yii::$app->status->request_reject])
                ->andWhere(["<>", 'request.status_id', Yii::$app->status->request_exhibitor_withdraw])
                ->andWhere(["<>", 'request.status_id', Yii::$app->status->request_exhibitor_withdraw_after_accept])
                ->andWhere(["<>", 'request.status_id', Yii::$app->status->request_hostess_withdraw_after_accept])
                ->exists();

        return $exists === false;
    }

    /**
     * Check if hostess is already booked for selected event
     * @param integer $eventId
     * @param integer $hostessId
     * @return boolean
     */
    public static function hostessAlreadybooked($eventId, $hostessId)
    {
        $exists = static::find()
                ->andWhere([
                    "request.event_id" => $eventId,
                    "request.user_id" => $hostessId,
                    'request.completed' => 1
                ])
                ->exists();

        return $exists;
    }

    /**
     * Check if hostess already rejected request from exhibitor for given event
     * 
     * @param integer $eventId
     * @param integer $hostessId
     * @param integer $exhibitorId
     * @return boolean
     */
    private static function getRejectRequestExists($eventId, $hostessId, $exhibitorId)
    {
        return Request::find()
                        ->andWhere([
                            "request.event_id" => $eventId,
                            "request.user_id" => $hostessId,
                            'request.requested_user_id' => $exhibitorId
                        ])
                        ->andWhere(['request.status_id' => Yii::$app->status->request_reject])
                        ->exists();
    }

    /**
     * Check if hostess already accepted request from exhibitor for given event
     * 
     * @param integer $eventId
     * @param integer $hostessId
     * @param integer $exhibitorId
     * @return boolean
     */
    private static function getAcceptedRequestsExists($eventId, $hostessId, $exhibitorId)
    {
        return Request::find()
                        ->andWhere([
                            "request.event_id" => $eventId,
                            "request.user_id" => $hostessId,
                            'request.requested_user_id' => $exhibitorId
                        ])
                        ->andWhere(['request.status_id' => Yii::$app->status->request_accept])
                        ->andWhere("request.completed IS NULL")
                        ->exists();
    }

    /**
     * Check if request from exhibitor for given event and given hostess exists
     * 
     * @param integer $eventId
     * @param integer $hostessId
     * @param integer $exhibitorId
     * @return boolean
     */
    private static function getPendingRequestsExists($eventId, $hostessId, $exhibitorId)
    {
        return Request::find()
                        ->andWhere([
                            "request.event_id" => $eventId,
                            "request.user_id" => $hostessId,
                            'request.requested_user_id' => $exhibitorId
                        ])
                        ->andWhere(['request.status_id' => Yii::$app->status->request_new])
                        ->andWhere("request.completed IS NULL")
                        ->exists();
    }

    /**
     * This method builds additional display data for hostess profile in case 
     * there is already request between logged in exhibitor and hostess
     * 
     * @param integer $eventId
     * @param integer $hostessId
     * @param integer $exhibitorId
     * @return array
     */
    public static function getNotificationDataForExhibitor($eventId, $hostessId, $exhibitorId)
    {
        $notificationData = [
            'show' => false,
            'color' => 'red',
            'message' => ''
        ];

        $pendingRequestExists = static::getPendingRequestsExists($eventId, $hostessId, $exhibitorId);
        $acceptedRequestExists = static::getAcceptedRequestsExists($eventId, $hostessId, $exhibitorId);

        if ($pendingRequestExists || $acceptedRequestExists)
        {
            $notificationData["show"] = true;
            $notificationData["color"] = 'red';
            $notificationData["message"] = Yii::t('app.exhibitor', 'You are waiting for the answer from this person.');

            if ($acceptedRequestExists)
            {
                $notificationData["message"] = Yii::t('app.exhibitor', 'This person has already confirmed your request.');
            }
        } else
        {
            $rejectRequestExists = static::getRejectRequestExists($eventId, $hostessId, $exhibitorId);

            if ($rejectRequestExists)
            {
                $notificationData["show"] = true;
                $notificationData["color"] = 'red';
                $notificationData["message"] = Yii::t('app.exhibitor', 'This person has once rejected Your request.');
            }
        }

        return $notificationData;
    }

    /**
     * Send notification about request to hostess ( for android )
     */
    private function sendAndroidNotification()
    {

        $tokens = $this->user->androidTokens;

        if (!empty($tokens))
        {
            // before sending notification change language so it is sent in selected language
            Yii::$app->language = $this->user->lang;

            $data = [
                'sound' => 'default',
                'click_action' => 'OPEN_NOTIFICATION',
                'title' => Yii::t('app.hostess', '{event} - Booking request', ["event" => $this->event->name]),
                'body' => Yii::t('app.hostess', 'Period: {period} ({days} day(s))!', [
                    "period" => Yii::$app->formatter->asDate($this->date_from) . " - " . Yii::$app->formatter->asDate($this->date_to),
                    "days" => $this->getTotalBookingDays()
                ]),
                'request_id' => $this->id,
                'notification_type' => 'new_request'
            ];

            Yii::$app->firebase->sendDataNotification($tokens, $data);

            // return language to currently logged user for other messages
            Yii::$app->language = $this->userRequested->lang;
        }
    }

    /**
     * Send notification to hostes about request ( iOS )
     */
    private function sendIOSNotification()
    {
        $tokens = $this->user->IOSTokens;

        if (!empty($tokens))
        {
            // before sending notification change language so it is sent in selected language
            Yii::$app->language = $this->user->lang;

            $data = [
                'sound' => 'default',
                'click_action' => 'OPEN_NOTIFICATION',
                'request_id' => $this->id,
                'notification_type' => 'new_request'
            ];

            $notification = [
                'title' => Yii::t('app.hostess', '{event} - Booking request', ["event" => $this->event->name]),
                'body' => Yii::t('app.hostess', 'Period: {period} ({days} day(s))!', [
                    "period" => Yii::$app->formatter->asDate($this->date_from) . " - " . Yii::$app->formatter->asDate($this->date_to),
                    "days" => $this->getTotalBookingDays()
            ])];

            Yii::$app->firebase->sendIOSDataNotification($tokens, $data, $notification);

            // return language to currently logged user for other messages
            Yii::$app->language = $this->userRequested->lang;
        }
    }

    /**
     * 
     * @param string|null $identification
     * @return integer|null
     */
    private function getAffiliateId($identification = null)
    {
        if (is_null($identification))
        {
            return null;
        }

        $affiliate = \common\models\Affiliate::findOne([
                    'identification' => $identification
        ]);

        if (is_object($affiliate))
        {
            return $affiliate->id;
        }

        return null;
    }

}
