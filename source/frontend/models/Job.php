<?php

namespace frontend\models;

class Job extends \common\models\Job
{

    /**
     * Return mapped list from id to translated name of jobs
     * @return array
     */
    public static function getAllTranslated()
    {
        $models = static::find()->orderBy(["order" => SORT_ASC])->all();

        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->getTranslatedName();
        }

        return $result;
    }

}
