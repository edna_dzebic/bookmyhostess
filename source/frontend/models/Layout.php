<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class Layout extends Model
{

    const SITE_EXHIBITOR_INDEX = 'exhibitor-index';
    const SITE_HOSTESS_INDEX = 'hostess-index';
    const SITE_HOSTESS_SEARCH = 'hostess-search';
    const SITE_EVENT_SEARCH = 'event-search';

    public static function getMetaDescription($site = "")
    {
        switch ($site)
        {
            case self::SITE_EXHIBITOR_INDEX:
                return "Sie brauchen eine Hostess für Ihre Messe in Köln oder Düsseldorf? Dann vertrauen Sie auf den flexiblen Service von BOOKmyHOSTESS.com.";
            case self::SITE_HOSTESS_INDEX:
                return "Du möchtest Messehostess werden? Dann baue auf BOOKmyHOSTESS.com. Wir bringen Unternehmen und die Hostess für Messe-Veranstaltungen zusammen.";
            case self::SITE_HOSTESS_SEARCH:
                return "Bequem, schnell und effizient können Sie mit BOOKmyHOSTESS.com Ihre Messehostess buchen. Suchen Sie bei uns Hostessen in ganz Deutschland.";
            case self::SITE_EVENT_SEARCH:
                return "Suche flexibel mit BOOKmyHOSTESS.com Messe- und Promotionsjobs in ganz Deutschland. Stelle dich über unsere Plattform zahlreichen Messeausstellern Unternehmen und Hostess-Agenturen vor.";
            default :
                return Yii::$app->settings->meta_description;
        }
    }

    public static function getMetaKeywords($site = "")
    {
        switch ($site)
        {
            case self::SITE_EXHIBITOR_INDEX:
                return "hostess, hostess agentur köln, hostess agenturen, hostess agentur düsseldorf";
            case self::SITE_HOSTESS_INDEX:
                return "messehostess werden, hostess messe, jobs für studenten";
            case self::SITE_HOSTESS_SEARCH:
                return "messehostess buchen, messehostess englisch, hostessen in dortmund, hostessen deutschland, messe münchen hostess";
            case self::SITE_EVENT_SEARCH:
                return "messehostess jobs, messejobs, promotionjobs";
            default :
                return Yii::$app->settings->meta_keywords;
        }
    }

}
