<?php

namespace frontend\models;

use Yii;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;

class Register extends \common\models\Register
{

    public static function getForHostess()
    {
        $provider = new ActiveDataProvider(
                [
            'query' => static::find()
                    ->joinWith("event")
                    ->andWhere([
                        "register.user_id" => Yii::$app->user->id
                    ])
                    ->andWhere("event.end_date>=NOW()")
                    ->orderBy("event.start_date")
            ,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);

        return $provider;
    }

    /**
     * Return eventAvailibiltiy as dataprovider
     * @param int $userID
     * @return arrays
     */
    public static function getAllForUser($userID)
    {
        $models = static::find()
                ->joinWith("event")
                ->andWhere(["register.user_id" => $userID])
                ->andWhere("event.end_date>=NOW()")
                ->orderBy("event.start_date")
                ->all();

        $eventAvailability = new ArrayDataProvider(
                [
            'allModels' => $models,
            'pagination' => [
                'pageSize' => 10
            ]
                ]
        );

        $availabilityData = [];
        foreach ($models as $model)
        {
            $availabilityData[$model->event->id] = $model->event->name;
        }

        return [$eventAvailability, $availabilityData];
    }

    /**
     * Return eventAvailibility as array
     * @param int $userID
     * @return array
     */
    public static function getAllForHostess($userID)
    {
        $eventAvailability = static::find()
                ->joinWith("event")
                ->andWhere(["register.user_id" => $userID])
                ->andWhere("event.end_date>=NOW()")
                ->orderBy("event.start_date")
                ->all();

        $availabilityData = [];
        foreach ($eventAvailability as $ev)
        {
            $availabilityData[$ev->event->id] = $ev->event->name;
        }

        return [$eventAvailability, $availabilityData];
    }

}
