<?php

namespace frontend\models;

use Yii;

class Partners extends \yii\base\Model
{

    public static function getThumbnails()
    {
        $partners = [
            [
                'link' => 'https://www.messe-friedrichshafen.de/',
                'logo' => Yii::$app->urlManager->createAbsoluteUrl(['/images/partners/messe-friedrichshafen.png']),
                'name' => 'MESSE FRIEDRICHSHAFEN GmbH'
            ],
            [
                'link' => 'https://www.boe-messe.de/',
                'logo' => Yii::$app->urlManager->createAbsoluteUrl(['/images/partners/messe-boe.jpg']),
                'name' => 'BOE INTERNATIONAL - DIE INTERNATIONALE FACHMESSE FÜR ERLEBNISMARKETING'
            ]
        ];

        return $partners;
    }

}
