<?php

namespace frontend\models;

class LanguageLevel extends \common\models\LanguageLevel
{

    /**
     * @return array
     */
    public static function getAllTranslated()
    {
        $models = static::find()->all();

        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->getTranslatedName();
        }

        asort($result);

        return $result;
    }

}
