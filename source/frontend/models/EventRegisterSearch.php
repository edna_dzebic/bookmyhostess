<?php

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use common\models\Register;
use common\models\Event;

/**
 * Class EventRegisterSearch used for listing all hostesses
 */
class EventRegisterSearch extends \yii\base\Model
{

    const SORT_KEY_PRICE_UP = 'price_up';
    const SORT_KEY_PRICE_DOWN = 'price_down';
    const SORT_KEY_PROFILE_RELIABILITY_UP = 'profile_reliability_score_up';
    const SORT_KEY_PROFILE_RELIABILITY_DOWN = 'profile_reliability_score_down';

    public $country;
    public $city;
    public $event;
    public $sort;
    public $pageSize = 20;
    public $languages;
    public $hair_colors;
    public $genders;
    public $ages;
    public $jobs;
    public $has_health_certificate;
    public $has_driving_licence;
    public $has_trade_licence;
    public $has_work_experience;
    public $has_tattoo;
    public $has_car;
    public $has_mobile_app;
    public $image_path;

    public function rules()
    {
        return [
            [['country', 'city', 'event', 'sort', 'languages', 'hair_colors', 'ages', 'jobs', 'genders', 'has_health_certificate', 'has_driving_licence', 'has_work_experience', 'has_tattoo', 'has_car', 'has_mobile_app'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'has_work_experience' => Yii::t('app', 'With work experience'),
            'birth_date' => Yii::t('app', 'Age'),
            'height' => Yii::t('app', 'Height'),
            'weight' => Yii::t('app', 'Weight'),
            'waist_size' => Yii::t('app', 'Waist Size'),
            'shoe_size' => Yii::t('app', 'Shoe Size'),
            'has_tattoo' => Yii::t('app', 'Has Tattoo'),
            'has_car' => Yii::t('app', 'Has Car'),
            'has_driving_licence' => Yii::t('app', 'Has Driving Licence'),
            'has_trade_licence' => Yii::t('app', 'Has Trade Licence'),
            'has_health_certificate' => Yii::t('app', 'Has Health Certificate'),
            'education_graduation' => Yii::t('app', 'Graduation'),
            'education_university' => Yii::t('app', 'University'),
            'education_vocational_training' => Yii::t('app', 'Vocational Training'),
            'education_profession' => Yii::t('app', 'Profession'),
            'education_special_knowledge' => Yii::t('app', 'Special Knowledge'),
            'has_mobile_app' => Yii::t('app', 'Has Mobile App')
        ];
    }

    /**
     * Main search for hostess profiles
     * @param array $params
     * @return ActiveDataProvider
     */
    public function getSearchProvider($params = [])
    {        
        list($query, $sort) = $this->createQuery($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
            'sort' => $sort
        ]);

        return $dataProvider;
    }

    /**
     * Load params into model attributes
     * @param array $params
     */
    public function loadParamsForFilter($params)
    {
        if (!empty($params))
        {
            $this->load($params);
        } else
        {
            $json = Yii::$app->session->get('hostess.filter');
            if (isset($json))
            {                
                $this->load($json);
            }
        }
    }

    /**
     * Save params or param into session for later
     * @param mixed $params
     */
    public function saveParamsForFilter($params)
    {
        Yii::$app->session->set('EVENT_REGISTER_SEARCH_DATA', $params);
    }

    /**
     * Gets params or param from session
     */
    public function getParamsForFilterFromSession()
    {
        Yii::$app->session->get('EVENT_REGISTER_SEARCH_DATA');
    }

    /**
     * Returns list of countries as id=>name array
     * @return array
     */
    public static function getCountryList()
    {
        //get from available countries
        $data = [];
        $events = Event::find()
                ->joinWith(['country'])
                ->innerJoin('country_price', 'country_price.country_id=country.id')
                ->select(['event.country_id', 'country.name'])
                ->andWhere(['event.status_id' => Yii::$app->status->active])
                ->andWhere("DATE(event.end_date)>=DATE(NOW())")
                ->orderBy('country.name ASC')
                ->distinct(true)
                ->all();

        foreach ($events as $event)
        {
            //countries
            if (!isset($data[$event->country_id]))
            {
                $data[$event->country_id] = $event->country->getTranslatedName();
            }
        }

        asort($data);

        return $data;
    }

    /**
     * Returns list of cities as id=>name array for some country id
     * @param int|null $countryId
     * @return array
     */
    public static function getCityList($countryId = null)
    {
        //get from available countries
        $data = [];

        $query = Event::find()
                ->joinWith(['city'])
                ->select(['event.city_id', 'city.name'])
                ->andWhere(['event.status_id' => Yii::$app->status->active])
                ->andWhere("DATE(event.end_date)>=DATE(NOW())")
                ->orderBy('city.name ASC')
                ->distinct(true);

        $query->andFilterWhere(["event.country_id" => $countryId]);

        $events = $query->all();

        foreach ($events as $event)
        {
            //cities
            if (!isset($data[$event->city_id]))
            {
                $data[$event->city_id] = $event->city->getTranslatedName();
            }
        }

        asort($data); // needed because of translations

        return $data;
    }

    /**
     * Returns list of cities as id=>name array for some country id
     * @param int|null $countryId
     * @return array
     */
    public static function getCityListJson($countryId = null)
    {

        $data = self::getCityList($countryId);

        $result = [];
        foreach ($data as $id => $name)
        {
            $result[] = [
                'id' => $id,
                'name' => $name
            ];
        }


        return $result;
    }

    /**
     * Returns list of events as id=>name array for some city or country
     * @param int|null $cityId
     * @param int|null $countryId
     * @return array
     */
    public static function getEventList($cityId = null, $countryId = null)
    {
        $query = Event::find()
                ->andWhere("DATE(end_date)>=DATE(NOW())")
                ->andWhere("status_id = " . Yii::$app->status->active)
                ->orderBy('name ASC');

        if (!empty($countryId))
        {
            $query->andWhere("country_id = " . $countryId);
        }

        if (!empty($cityId))
        {
            $query->andWhere("city_id = " . $cityId);
        }

        $events = $query->all();

        $result = [];

        foreach ($events as $event)
        {
            $result[$event->id] = $event->name;
        }

        return $result;
    }

    /**
     * Returns list of events as id=>name array for some city or country
     * @param int|null $cityId
     * @param int|null $countryId
     * @return array
     */
    public static function getEventListJson($cityId = null, $countryId = null)
    {
        $query = Event::find()
                ->andWhere("DATE(end_date)>=DATE(NOW())")
                ->andWhere("status_id = " . Yii::$app->status->active)
                ->orderBy('name ASC');

        if (!empty($countryId))
        {
            $query->andWhere("country_id = " . $countryId);
        }

        if (!empty($cityId))
        {
            $query->andWhere("city_id = " . $cityId);
        }

        $events = $query->all();

        $result = [];

        foreach ($events as $event)
        {
            $result[] = [
                "id" => $event->id,
                "name" => $event->name
            ];
        }

        return $result;
    }

    /**
     * Return list of translated languages for filter
     * @return array
     */
    public static function getLanguagesList()
    {
        return Language::getAllTranslated();
    }

    /**
     * Return gender list for filter
     * @return array
     */
    public static function getGenderList()
    {

        $data = [
            Yii::t('app', 'Female'),
            Yii::t('app', 'Male'),
        ];

        return $data;
    }

    /**
     * Return transalted list of jobs for filter
     * @return array
     */
    public static function getEventJobList()
    {
        return Job::getAllTranslated();
    }

    /**
     * Return translated list of hair colors for filter
     * @return array
     */
    public static function getHairColorList()
    {
        return HairColor::getAllTranslated();
    }

    /**
     * Return list of years for filter
     * @return array
     */
    public static function getYearList()
    {
        return [
            "18,21" => "18-21",
            "21,24" => "21-24",
            "24,27" => "24-27",
            "27,30" => "27-30",
            "30,33" => "30-33",
            "33,36" => "33-36",
            "36,99" => ">36"
        ];
    }

    /**
     * Return sort options
     * @return array
     */
    public static function getSortList()
    {
        return [
            self::SORT_KEY_PRICE_UP => Yii::t('app', 'Price up'),
            self::SORT_KEY_PRICE_DOWN => Yii::t('app', 'Price down'),
            self::SORT_KEY_PROFILE_RELIABILITY_UP => Yii::t('app', 'Lowest reliability'),
            self::SORT_KEY_PROFILE_RELIABILITY_DOWN => Yii::t('app', 'Highest reliability')
        ];
    }

    /**
     * 
     * @param int $id
     * @return Register
     */
    public function getNextModel($id)
    {
        list($query, $sort) = $this->createQuery([]);

        $models = $query->all();

        $prev = null;
        $found = null;
        foreach ($models as $model)
        {
            if (isset($prev))
            {
                if ($prev->user_id == $id)
                {
                    $found = $model;
                    break;
                }
            }

            $prev = $model;
        }

        return $found;
    }

    /**
     * 
     * @param int $id
     * @return Register
     */
    public function getPrevModel($id)
    {
        list($query, $sort) = $this->createQuery([]);

        $models = $query->all();

        $found = null;
        foreach ($models as $model)
        {
            if ($model->user_id != $id)
            {
                $found = $model;
            } else
            {
                break;
            }
        }

        return $found;
    }

    /**
     * Find any model which is signed up for event, which is not for user with id $id
     * @param int $id
     * @param int $eventID
     * @return Register
     */
    public function getNextAfterNotFound($id, $eventID)
    {
        $eventModel = Event::findOne(["id" => $eventID]);

        $this->event = $eventModel->id;
        $this->country = $eventModel->country_id;
        $this->city = $eventModel->city_id;

        list($query, $sort) = $this->createQuery([]);

        $query->andWhere(["<>", "hostess_profile.user_id", $id]);

        $models = $query->all();

        $count = count($models);

        if ($count > 0)
        {
            return $models[0];
        }

        return null;
    }

    /**
     * 
     * @param int $id
     * @param int $eventID
     * @return Register
     */
    public function getNextAfterBooking($id, $eventID)
    {
        list($query, $sort) = $this->createQuery([]);

        if (empty($this->event))
        {
            $eventModel = Event::findOne(["id" => $eventID]);

            $this->event = $eventModel->id;
            $this->country = $eventModel->country_id;
            $this->city = $eventModel->city_id;

            $query->andFilterWhere([
                'event.country_id' => $eventModel->country_id,
                'event.city_id' => $eventModel->city_id,
                'event.id' => $eventModel->id
            ]);
        }

        $models = $query->all();

        $prev = null;
        $found = null;
        foreach ($models as $model)
        {
            if (isset($prev))
            {
                if ($prev->user_id == $id)
                {
                    $found = $model;
                    break;
                }
            }

            $prev = $model;
        }

        return $found;
    }

    /**
     * Prepare base query
     * @param array $params
     * @return list Query and Sort
     */
    private function createQuery($params)
    {
        //load request params into attributes
        $this->loadParamsForFilter($params);

        //query all
        $query = Register::find()
                ->innerJoin('user', 'register.user_id = user.id')
                ->innerJoin('hostess_profile', 'register.user_id = hostess_profile.user_id')
                ->innerJoin('event', 'event.id = register.event_id')
                ->innerJoin('country', 'event.country_id = country.id')
                ->innerJoin('city', 'event.city_id = city.id')
                ->innerJoin('event_job', 'event_job.event_id = event.id')
                ->innerJoin('user_images', 'hostess_profile.user_id = user_images.user_id')
                ->andWhere([
                    'user.status_id' => Yii::$app->status->active,
                    'event.status_id' => Yii::$app->status->active,
                    'hostess_profile.admin_approved' => \common\models\HostessProfile::APPROVAL_APPROVED,
                    'user.role' => User::ROLE_HOSTESS,
                ])
                ->andWhere("user_images.id IS NOT NULL AND DATE(event.end_date)>=DATE(NOW())");

        $query->andFilterWhere([
            'event.country_id' => $this->country,
            'event.city_id' => $this->city,
            'event.id' => $this->event,
            'hostess_profile.hair_color_id' => $this->hair_colors,
            'hostess_profile.gender' => $this->genders,
            'event_job.job_id' => $this->jobs,
            'hostess_profile.has_tattoo' => $this->has_tattoo,
            'hostess_profile.has_car' => $this->has_car,
            'hostess_profile.has_trade_licence' => $this->has_trade_licence,
            'hostess_profile.has_mobile_app' => $this->has_mobile_app,
        ]);

        if (!empty($this->has_driving_licence))
        {
            $query->andWhere(['hostess_profile.has_driving_licence' => $this->has_driving_licence]);
        }

        if (!empty($this->languages))
        {
            $query->innerJoin('hostess_language', 'hostess_language.user_id = hostess_profile.user_id');

            $query->andWhere('hostess_language.language_id IN ( ' . implode(", ", $this->languages) . ')');
            $query->groupBy('hostess_language.user_id');

            $query->having('COUNT(DISTINCT hostess_language.language_id) >= ' . count($this->languages));
        }

        if (!empty($this->has_health_certificate))
        {
            $query->andWhere(['hostess_profile.has_health_certificate' => $this->has_health_certificate]);
        }

        if (!empty($this->has_work_experience))
        {
            $query->innerJoin("hostess_past_work", "hostess_profile.user_id=hostess_past_work.user_id");
        }

        if ($this->ages != '')
        {
            $ageLimits = explode(",", $this->ages);
            if (count($ageLimits) === 2)
            {
                $ageFrom = $ageLimits[0];
                $ageTo = $ageLimits[1];

                $birthdayFrom = date('Y-m-d', strtotime('-' . $ageTo . ' years'));
                $birthdayTo = date('Y-m-d', strtotime('-' . $ageFrom . ' years')); //TODO: does 24-27 need to include 27-years?

                $query->andWhere('hostess_profile.birth_date > :birthday_from AND hostess_profile.birth_date <= :birthday_to', [':birthday_from' => $birthdayFrom, ':birthday_to' => $birthdayTo]);
            }
        }

        $query->groupBy(["hostess_profile.id", "register.user_id"]);

        //sort
        $defaultOrder = [];

        if (!isset($this->sort) || empty($this->sort))
        {
            $this->sort = self::SORT_KEY_PROFILE_RELIABILITY_DOWN;
        }

        switch ($this->sort)
        {
            case self::SORT_KEY_PRICE_UP:
                $defaultOrder = [
                    'register.price' => SORT_ASC,
                    'hostess_profile.user_id' => SORT_DESC
                ];
                break;
            case self::SORT_KEY_PRICE_DOWN:
                $defaultOrder = [
                    'register.price' => SORT_DESC,
                    'hostess_profile.user_id' => SORT_DESC
                ];
                break;
            case self::SORT_KEY_PROFILE_RELIABILITY_UP:
                $defaultOrder = [
                    'hostess_profile.profile_reliability_score' => SORT_ASC,
                    'hostess_profile.user_id' => SORT_DESC
                ];
                break;
            case self::SORT_KEY_PROFILE_RELIABILITY_DOWN:
                $defaultOrder = [
                    'hostess_profile.profile_reliability_score' => SORT_DESC,
                    'hostess_profile.user_id' => SORT_DESC
                ];
                break;
            default:
                $defaultOrder = [
                    'hostess_profile.profile_reliability_score' => SORT_DESC,
                    'hostess_profile.user_id' => SORT_DESC
                ];
        }

        $sort = new Sort([
            'attributes' => [
                'register.price',
                'hostess_profile.profile_reliability_score',
                'hostess_profile.user_id'
            ],
            'defaultOrder' => $defaultOrder
        ]);

        $query->orderBy($sort->orders);

        return [$query, $sort];
    }

}
