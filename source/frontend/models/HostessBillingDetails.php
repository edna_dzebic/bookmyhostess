<?php

namespace frontend\models;

use yii\base\Model;
use Yii;

class HostessBillingDetails extends Model
{

    /**
     * @var \common\models\HostessProfile 
     */
    public $model;
    public $phone;
    public $tax_number;
    public $billing_address;
    public $postal_code;
    public $city;
    public $country_id;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->setAttributes($this->model->getAttributes());
        $this->phone = $this->model->user->phone_number;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['phone', 'tax_number',
                    'billing_address', 'postal_code',
                    'city', 'country_id'
                ], 'required'],
            ['phone', 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => Yii::t('app', 'Phone number'),
            'tax_number' => Yii::t('app', 'Tax number'),
            'billing_address' => Yii::t('app', 'Address'),
            'postal_code' => Yii::t('app', 'Postal code'),
            'city' => Yii::t('app', 'City'),
            'country_id' => Yii::t('app', 'Country')
        ];
    }
    
    public function attributeHints()
    {
        return [
            'tax_number' => Yii::t('app.hostess', 'If not available, please enter "XXX"')
        ];
    }

    /**
     * Save billing details.
     *
     * @return true/false
     */
    public function saveDetails($model, $data)
    {
        if ($this->load($data) && $this->validate())
        {
            $transaction = Yii::$app->db->beginTransaction();

            $model->user->phone_number = $this->phone;
            $this->model->setAttributes($this->getAttributes());

            if ($model->user->update(false) !== false && $model->save(false) !== false)
            {
                $this->insertMappings();
                $transaction->commit();
                return true;
            }

            $this->addErrors($this->model->getErrors());
            $this->addErrors($this->model->user->getErrors());

            $transaction->rollBack();
        }

        return false;
    }

    private function insertMappings()
    {
        $countryId = $this->model->country_id;
        $country = \common\models\Country::findOne(["id" => $countryId]);

        if (is_object($country))
        {
            $mappings = $country->sourceMappings;

            $tableName = \common\models\HostessCountryMapping::tableName();

            $attributes = [
                'user_id',
                'country_id',
                'created_by',
                'updated_by',
                'date_created',
                'date_updated',
                'status_id'
            ];
            $values = [];

            // insert country of origin by default
            $values[] = [
                $this->model->user_id,
                $countryId,
                $this->model->user_id,
                $this->model->user_id,
                date("Y-m-d H:i:s"),
                date("Y-m-d H:i:s"),
                Yii::$app->status->active
            ];

            // add all mappings for country of origin
            foreach ($mappings as $mapping)
            {
                $values[] = [
                    $this->model->user_id,
                    $mapping->target_country_id,
                    $this->model->user_id,
                    $this->model->user_id,
                    date("Y-m-d H:i:s"),
                    date("Y-m-d H:i:s"),
                    Yii::$app->status->active
                ];
            }


            Yii::$app->db->createCommand()
                    ->batchInsert($tableName, $attributes, $values)
                    ->execute();
        }
    }

}
