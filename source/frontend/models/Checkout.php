<?php

namespace frontend\models;

use Yii;

class Checkout
{

    const MAX_STEPS = 4;

    public static function getAllSteps()
    {
        $steps = [
            'step-1' => [
                'label' => Yii::t('app.exhibitor', 'Requests overview'),
                'index' => 1,
                'url' => Yii::$app->urlManager->createUrl(["checkout/index"])
            ],
            'step-2' => [
                'label' => Yii::t('app.exhibitor', 'Invoice recipient'),
                'index' => 2,
                'url' => Yii::$app->urlManager->createUrl(["checkout/invoice-details"]),
            ],
            'step-3' => [
                'label' => Yii::t('app.exhibitor', 'Method of payment'),
                'index' => 3,
                'url' => Yii::$app->urlManager->createUrl(["checkout/payment"]),
            ],
            'step-4' => [
                'label' => Yii::t('app.exhibitor', 'Final overview'),
                'index' => 4,
                'url' => Yii::$app->urlManager->createUrl(["checkout/final"]),
            ]
        ];

        return $steps;
    }

    public function getNumberOfSteps()
    {
        
    }

    public static function getDisplayedPaymentTypes(ExhibitorProfile $profileModel)
    {
        $payments = [];

        if ($profileModel->is_invoice_payment_allowed)
        {
            $payments[] = [
                'value' => Invoice::PAYMENT_TYPE_INVOICE,
                'label' => Yii::t('app.exhibitor', 'Payment upon invoice'),
                'description' => Yii::t('app.exhibitor', 'You receive an invoice which you pay by bank transfer within 7 days after the booking.')
            ];
        }

        $payments[] = [
            'value' => Invoice::PAYMENT_TYPE_PAYPAL,
            'label' => Yii::t('app.exhibitor', 'PayPal'),
            'description' => Yii::t('app.exhibitor', 'After completion of the booking process, you will be forwarded to PayPal for payment processing.')
        ];

        return $payments;
    }

    /**
     * 
     * @param string $paymentType
     * @return string
     */
    public static function getPaymentLabel($paymentType)
    {

        if ($paymentType === Invoice::PAYMENT_TYPE_INVOICE)
        {
            return Yii::t('app.exhibitor', 'Payment upon invoice');
        }

          if ($paymentType === Invoice::PAYMENT_TYPE_RECEIPT)
        {
            return Yii::t('app.exhibitor', 'Receipt');
        }
        
        return Yii::t('app.exhibitor', 'PayPal');
    }

}
