<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Blog as BlogModel;

class Blog extends Model
{

    public static function getModel($id)
    {
        $data = static::getData();

        $filtered = array_filter($data, static function($value) use($id)
        {
            return $value["id"] === (int) $id;
        });

        if (count($filtered) >= 1)
        {
            $data = current($filtered);
            $model = new BlogModel();
            $model->setData($data);
            return $model;
        }

        return null;
    }

    public static function getTopThree()
    {
        $data = array_slice(static::getData(), 0, 3);
        $result = [];

        foreach ($data as $blog)
        {
            $model = new BlogModel();
            $model->setData($blog);
            $result[] = $model;
        }

        return $result;
    }

    public static function getAll()
    {
        $data = static::getData();
        $result = [];

        foreach ($data as $blog)
        {
            $model = new BlogModel();
            $model->setData($blog);
            $result[] = $model;
        }

        return $result;
    }

    private static function getData()
    {
        return [
            static::getBlog9(),
            static::getBlog8(),
            static::getBlog7(),
            static::getBlog6(),
            static::getBlog5(),
            static::getBlog4(),
            static::getBlog3(),
            static::getBlog2(),
            static::getBlog1(),
        ];
    }

     private static function getBlog9()
    {
        return [
            "id" => 9,
            "title" => "BOOKmyHOSTESS.com zieht positive Bilanz der BoE Dortmund",
            "short_content" => "<p>Zum ersten Mal war das Team von BOOKmyHOSTESS.com Kooperationspartner der BoE Dortmund. Im Rahmen dieser Kooperation präsentierte sich das Unternehmen auf dem Branchenevent als Aussteller und konnte damit die Funktionen, aber auch die Leistungen des eigenen Services potenziellen Kunden vorstellen. Damit gelang BOOKmyHOSTESS.com ein großer Schritt nach vorn. </p>",
            "date_created" => "2019-01-18",
            "created_by_username" => "Admin",
            "image" => "/images/blog/boe.jpg",
            "image_explanation" => "Vortrag im MICE-Forum auf der Messe Best of Events 2019 in den Messehallen Dortmund.",
            "content" => '
                <p>
Die BoE Dortmund gehört zu den Branchenevents schlechthin. In diesem Jahr präsentierte sich die Best of Events größer denn je. Insgesamt 10.900 Fachbesucher wurden in den sechs Ausstellungshallen gezählt. Damit konnte der Event seine strategische Weiterentwicklung untermauern.                 
</p>
<p>
BOOKmyHOSTESS.com war einer von insgesamt 650 Ausstellern. Allein im Bereich der Aussteller gelang den Veranstaltern ein überragender Zuwachs von insgesamt 18 Prozent.  Das Forum für Eventmanager, Marketer und PR-Spezialisten konnte dabei den Weg für neue Kooperationen ebnen. Auch das Team von BOOKmyHOSTESS.com zieht ein positives Fazit zur Veranstaltung. Obwohl dem Team nur 12 Tage Vorbereitung blieben, konnten die Chancen der Messe genutzt werden. 
</p>
<p>
Auch im kommenden Jahr wird die BoE Dortmund wieder zahlreiche Besucher in die Dortmunder Messehallen locken. Die Zeichen, dass der Veranstaltung dabei ein erneuter Wachstumskurs gelingt, stehen zweifelsohne gut. In diesem Jahr führten die Initiatoren bereits während der Messe eine Befragung durch, um sich ein Bild vom Feedback der Fachbesucher zu machen. Dabei war dieses überwiegend positiv und gab der Zukunftsorientierung der Veranstalter Recht. 
</p>
                ',
        ];
    }
    
    
    private static function getBlog8()
    {
          return [
            "id" => 8,
            "title" => "BOOKmyHOSTESS.com geht Kooperation mit Messe Friedrichshafen ein",
            "short_content" => "<p>Das Team von BOOKmyHOSTESS.com stellt alle Zeichen weiterhin auf Wachstumskurs und erreicht damit sowohl regional, aber auch überregional große Erfolge. Noch im Dezember vergangenen Jahres konnten wir uns über einen weiteren Meilenstein freuen. Die Messe Friedrichshafen hat die Kooperation mit BOOKmyHOSTESS.com bestätigt.</p>",
            "date_created" => "2019-01-08",
            "created_by_username" => "Admin",
            "image" => "/images/blog/Messe-Friedrichshafen.jpg",
            "image_explanation" => "Foto: Messe Friedrichshafen",
            "content" => '
                <p>
                    Die Partnerschaft zwischen der Messe Friedrichshafen und BOOKmyHOSTESS.com bietet uns die Möglichkeit, unseren Service einem noch breiteren Publikum zu präsentieren. Davon profitiert langfristig vor allem die Bekanntheit der Marke. So wird das Unternehmen als offizieller Partner der Messe aufgeführt. Aussteller, die auf einer Messe in Friedrichshafen teilnehmen, werden nun bereits vor der Anreise auf den Service von BOOKmyHOSTESS.com hingewiesen.             
                </p>
                <p>
                Von der neuen Partnerschaft profitieren bei weitem nicht nur die kooperierenden Unternehmen, sondern auch die Aussteller, die ihre Angebote in Friedrichshafen präsentieren möchten. Steht den ausstellenden Unternehmen noch keine Hostess kurz vor Veranstaltungsbeginn zur Verfügung, kann diese unverbindlich über unsere Plattform gesucht werden. Wir bringen Hostessen und Aussteller schnell, unkompliziert und fair zusammen.
                </p>
                <p>
                Unternehmen, die sich auf einer Messe ihrer Zielgruppe, aber auch möglichen Geschäftspartnern präsentieren möchten, sind auf motiviertes und ebenso kompetentes Personal angewiesen. Hier knüpft das Angebot von BOOKmyHOSTESS.com an. Nur fünf Schritte trennen Sie von der Anmeldung bis zur Buchung geeigneter Hostessen. 
                </p>
                ',
        ];
    }
    
    private static function getBlog7()
    {
        return [
            "id" => 7,
            "title" => "Hostessen für Messen in Italien",
            "short_content" => "<p>Für Messen in Italien können Aussteller, Agenturen und Veranstalter ab sofort auf BOOKmyHOSTESS.com temporäre Mitarbeiter buchen. Zahlreiche Hostessen haben sich bereits registriert und stellen sich mit ihren Profilen, Erfahrungen und Honorarangaben vor.</p>",
            "date_created" => "2018-05-28",
            "created_by_username" => "Admin",
            "image" => "/images/blog/Hostessen-fuer-Messen-in-Italien.jpg",
            "image_explanation" => "Die Buchung einer Hostess für eine Messe in Italien hat sich in der Praxis bereits bewährt. Auf der Xylexpo in Mailand war am Stand eines Ausstellers aus den USA eine Hostess tätig, die er über BOOKmyHOSTESS.com gebucht hat.",
            "content" => '<p>
                    BOOKmyHOSTESS.com wird internationaler und ermöglicht ab sofort auch die digitale Buchung von Messepersonal in Mailand, Bologna und anderen italienischen Städten. Die Plattform steht jetzt in Deutsch, Englisch und Italienisch zur Verfügung. Sowohl die Nutzer, die Messepersonal suchen, als auch die Hostessen verwenden die Plattform in der Sprache ihrer Wahl.
                </p>
                <p>
                „Wir haben in letzter Zeit viel positives Feedback erhalten, wie einfach und schnell eine Buchung auf BOOKmyHOSTESS.com ist. Daher gehen wir jetzt einen wichtigen Schritt in Richtung Internationalisierung und ermöglichen auch die Buchung von Personal für Messen in Italien“, sagt Adela Kadiric, die Gründerin der Online-Plattform.
                </p>
                <p>
                Die Messewirtschaft ist in jeder Hinsicht international: deutsche Unternehmen sind regelmäßig Aussteller auf Messen im Ausland, und ausländische Unternehmen sind auf Messen in Deutschland vertreten. Daher bietet BOOKmyHOSTESS.com eine bequeme Lösung, um international Messepersonal zu buchen. Die Digitalisierung bietet hier eine große Chance. Auf einer Online-Plattform kommen Angebot und Nachfrage auf internationaler Ebene ganz einfach zusammen.
                </p>
                <p>
                Um die italienischen Hostessen und Nutzer zu betreuen, steht in Italien eine muttersprachliche Mitarbeiterin als Ansprechpartnerin zur Verfügung. Sie unterstützt sowohl die Hostessen, als auch die Kunden bei Fragen bezüglich der Registrierung oder des Ablaufs der Buchung.
                </p>
                ',
        ];
    }

    private static function getBlog6()
    {
        return [
            "id" => 6,
            "title" => "Kundenreferenz: Sailing Blue auf der boot 2018",
            "short_content" => "<p>
            „Ich bin immer rundum zufrieden mit den Hostessen, die ich über BOOKmyHOSTESS.com buche. Das gilt auch für Janine, die dieses Jahr auf der boot einen tollen Job gemacht hat“, resümiert Periklis Gkikas seine Erfahrung mit der Hostess, die ihn 10 Tage lang auf der Messe in Düsseldorf unterstützt hat.
                    </p>",
            "date_created" => "2018-02-18",
            "created_by_username" => "Admin",
            "image" => "/images/blog/Messe-Duesseldorf-Boot-2018.jpg",
            "image_explanation" => "„Ich bin immer rundum zufrieden mit den Hostessen, die ich über BOOKmyHOSTESS.com buche. Das gilt auch für Janine, die dieses Jahr auf der boot einen tollen Job gemacht hat“, resümiert Periklis Gkikas seine Erfahrung mit der Hostess, die ihn 10 Tage lang auf der Messe in Düsseldorf unterstützt hat.",
            "content" => ' <p>
                    Für die boot in Düsseldorf war die Hostess, die der griechische Unternehmer zunächst gebucht hatte, kurzfristig verhindert. Doch die Mitarbeiter von BOOKmyHOSTESS.com lassen ihre Kunden nicht im Stich. „Ein Anruf genügte, und alles hat dann problemlos geklappt. Auf jeden Fall werde ich auch in Zukunft die Online-Plattform nutzen, um Hostessen zu finden“, sagt Gkikas.
                </p>
                <p>
                    An seinen Messeständen benötigt Gkikas eine Hostess, die die Besucher freundlich begrüßt und ihnen Broschüren aushändigt. „Das erste Mal hat mich vor allem der Preis überzeugt. Außerdem sind die Suche, Anfrage und Buchung über die Plattform wirklich einfach“, erklärt Gkikas, der bereits zum x. Mal über BOOKmyHOSTESS.com gebucht hat.
                </p>
                <p>
                    Der Kapitän ist regelmäßig Aussteller auf Messen in Deutschland, um für Sailing Blue zu werben. Das Unternehmen vermietet Yachten und Segelboote. Für Interessierte ohne Segelerfahrung bietet es Touren zu den griechischen Inseln an.
                </p>
                <br>
                <img class="img-responsive" src="http://localhost:8002//images/blog/SailingBlue.jpg" alt="" style="margin : 0 auto;">                 <div class="img-explanation text-center">
                Periklis Gkikas bucht regelmäßig eine Messehostess über BOOKmyHOSTESS.com und wird auch künftig die Online-Plattform nutzen.
            </div>',
        ];
    }

    private static function getBlog5()
    {
        return [
            "id" => 5,
            "title" => "Vielbucher erhalten Bonus-Paket",
            "short_content" => "<p>
                    Event-Agenturen und Unternehmen, die regelmäßig externe Mitarbeiter benötigen, können sich ab sofort als Vielbucher registrieren. Daraufhin vereinbart BOOKmyHOSTESS.com mit ihnen kundenspezifische Gebühren. Die Bonus-Pakete starten mit einer befristeten Testphase ohne Buchungsgebühr. 
                </p>",
            "date_created" => "2017-09-11",
            "created_by_username" => "Admin",
            "image" => "/images/blog/Teamfoto.jpg",
            "image_explanation" => "Für  Agenturen, Unternehmen und Veranstalter,die regelmäßig Messepersonal buchen, führt die Online-Plattform kundenspezifische Gebühren ein",
            "content" => " <p>
                   Die Buchung von Hostessen, Models und Promotern erfolgt auf BOOKmyHOSTESS.com ausschließlich online. Daher kann die Plattform die Buchungsgebühr niedrig halten. Sie beträgt regulär 25 Euro, um einen externen Mitarbeiter für einen Tag zu buchen. Mit den neuen Bonus-Paketen für Vielbucher sind die Kosten noch niedriger.
                </p>
                <p>
                  Von einem Bonus-Paket können beispielsweise Event-Agenturen profitieren, die verschiedene Unternehmen bei Messeauftritten oder anderen Veranstaltungen unterstützen und dafür regelmäßig externe Mitarbeiter buchen. Außerdem erhalten es Marketing-Profis in Unternehmen, die im Laufe eines Jahres auf mehreren Messen Aussteller sind und dafür Messepersonal benötigen. Mit Veranstaltern von Messen, Konferenzen und anderen großen Events vereinbart BOOKmyHOSTESS.com individuelle Kooperationen.
                </p>
                <p>
                    „Die Kostenvorteile, die wir als Online-Plattform im Vergleich zu einer Agentur haben, geben wir gern an unsere Kunden weiter und berechnen daher sehr niedrige Buchungsgebühren“, sagt Adela Kadiric, die Gründerin der Online-Plattform BOOKmyHOSTESS.com. „Die reguläre Gebühr ist bereits so niedrig, dass die Hostessen von einem höheren Tagessatz profitieren und daher besonders engagiert und motiviert sind. Mit den neuen Bonus-Paketen für Vielbucher sind die Buchungen für unsere Kunden noch günstiger, und unsere Plattform wird auch für Hostessen attraktiver, da sie häufiger gebucht werden.“
                </p>",
        ];
    }

    private static function getBlog4()
    {
        return [
            "id" => 4,
            "title" => "dmexco: Besuchen Sie uns im Start-up Village in Halle 5.2",
            "short_content" => "<p>
                    BOOKmyHOSTESS.com stellt auf der dmexco 2017 seine Online-Plattform vor. Im Start-up Village in Halle 5.2 Gang S Nr. U-02 erfahren die Besucher, wie sie mit wenigen Klicks externes Personal suchen und buchen können. Marketing-Profis in Unternehmen und Event-Agenturen erhalten einen Gutschein für die nächste Buchung.
                </p>",
            "date_created" => "2017-09-10",
            "created_by_username" => "Admin",
            "image" => "/images/blog/DmExco.jpg",
            "image_explanation" => "Für  Agenturen, Unternehmen und Veranstalter,die regelmäßig Messepersonal buchen, führt die Online-Plattform kundenspezifische Gebühren ein",
            "content" => '<p>
                    BOOKmyHOSTESS.com präsentiert eine Demo der Online-Plattform, die alle Anwendungen für Nutzer und Hostessen zeigt. Die Besucher erfahren beispielsweise, wie ein registrierter Nutzer auf der Plattform mit wenigen Klicks Hostessen auswählen und dann kostenfrei anfragen kann. Hostessen, die über die Hostessen-App verfügen, antworten innerhalb weniger Minuten auf die Anfragen, und der Nutzer kann die Buchung online abschließen.
                </p>
                <p>
                    „Bei Ihrem Besuch an unserem Messestand beantworten wir Ihnen gern alle Fragen zu unserer Plattform. Sie werden überrascht sein, wie schnell und einfach Sie damit Hostessen und Promoter buchen können“, sagt Adela Kadiric, Gründerin und Geschäftsführerin von BOOKmyHOSTESS.com. „Außerdem erhalten Sie von uns einen Gutschein im Wert von 25 Euro, den Sie dann bei der nächsten Buchung einlösen können.“
                </p>
                <p>Die internationale Fachmesse für digitales Marketing findet dieses Jahr am 13. und 14. September in Köln statt. Weitere Informationen zur dmexco finden Sie unter <a href="http://www.dmexco.de">www.dmexco.de</a>.</p>',
        ];
    }

    private static function getBlog3()
    {
        return [
            "id" => 3,
            "title" => "Die Gründerin im Interview",
            "short_content" => 'Über ihre Motivation, die Online-Plattform BOOKmyHOSTESS.com  zu gründen, sprach Adela Kadiric mit den Alumni Services der WiSo-Fakultät Köln. Außerdem erzählt sie, warum ihr die Arbeit mit Hostessen, Kunden und Geschäftspartnern so viel Spaß macht.
                    Das ganze interview lesen Sie unter : 
                    <a href="https://www.wiso.uni-koeln.de/praxis/wisoalumni/wiso-alumni-im-interview/adela-kadiric/" target="_blank">https://www.wiso.uni-koeln.de/praxis/wisoalumni/wiso-alumni-im-interview/adela-kadiric/</a>',
            "date_created" => "2017-06-27",
            "created_by_username" => "Admin",
            "image" => "/images/blog/fb2b8752ea291bd70338bf7dc7.jpg",
            "image_explanation" => "",
            "content" => '',
        ];
    }

    private static function getBlog2()
    {
        return [
            "id" => 2,
            "title" => "Unsere kostenlose Hostessen-App ist da!",
            "short_content" => '<p>
                    Wer auf BOOKmyHOSTESS.com externe Mitarbeiter sucht, erhält jetzt noch schneller Antwort auf seine Anfragen. Das ermöglicht die neue, kostenlose Hostessen-App für Hostessen, Models und Promoter. Erste Erfahrungen zeigen, dass Unternehmen und Agenturen dadurch in weniger als 15 Minuten Personal finden und buchen können.
                </p>',
            "date_created" => "2017-05-01",
            "created_by_username" => "Admin",
            "image" => "/images/blog/fb2b8752ea291bd70228bf7dc7e.jpg",
            "image_explanation" => "Die neue Hostessen-App steht auf BOOKmyHOSTESS.com kostenlos zum Herunterladen zur Verfügung. Sie informiert die Hostessen jederzeit über eine Anfrage, und diese können umgehend zusagen.",
            "content" => '<p>
                    Auf BOOKmyHOSTESS.com finden Agenturen und Unternehmen mehrere hundert geprüfte Profile von Hostessen, Models und Promotern. Diese geben unter anderem an, auf welchen Messen sie zu welchem Tagessatz arbeiten möchten und welche Erfahrungen sie haben. Bei Interesse fragen registrierte Nutzer die Hostess gebührenfrei an, indem sie in einem Online-Formular Angaben wie die erforderliche Kleidung und Tätigkeit anklicken. Bisher erhielten die Hostessen diese Anfragen ausschließlich per Email. Mit der Hostessen-App sind sie schneller informiert und können zügiger antworten. 
                </p>
                <p>
                    „Bei der Suche nach externen Mitarbeitern muss es meistens schnell gehen. Daher profitieren von der Hostessen-App nicht nur die Hostessen, sondern auch die Aussteller“, ist die Gründerin Adela Kadirić überzeugt. „Bei uns erfolgen Suche, Auswahl, Anfrage und Buchung ausschließlich online, so dass für Unternehmen und Agenturen nur eine niedrige Vermittlungsgebühr anfällt. Wir wollen die digitalen Möglichkeiten ausschöpfen, damit es für alle Beteiligten möglichst reibungslos funktioniert. Mit der neuen App führen wir diesen Ansatz konsequent fort.“
                </p>
                <p>
                    Die Hostessen-App steht kostenlos für den Google Play Store und für den Apple Store jeweils in Deutsch und Englisch zur Verfügung. Bei einer Anfrage zeigt die App den Hostessen folgende Informationen an: Messe, Einsatzzeitraum, Tagessatz, Art der Tätigkeit und erforderliche Kleidung. Die Hostess kann als Antwort zwischen „Zusagen“ und „Absagen“ auswählen. Der registrierte Nutzer, der die Anfrage gestellt hat, erhält sofort die Antwort.
                </p>',
        ];
    }

    private static function getBlog1()
    {
        return [
            "id" => 1,
            "title" => "30 Promoter überreichten Besuchern der K-Messe die aktuellen Nachrichten",
            "short_content" => '<p>
                    Internationale Experten der Kunststoffindustrie trafen sich im Oktober 2016 in den 19 Hallen der Messe Düsseldorf. Der Veranstalter erwartete mehr als 200.000 Besucher. Damit sie jeden Tag mit aktuellen Informationen rund um die Messe versorgt werden konnten, setzten die Verlage der beiden offiziellen  Messezeitungen auf erfahrene Promoter.
                </p>',
            "date_created" => "2016-10-27",
            "created_by_username" => "Admin",
            "image" => "/images/blog/fb2b8752ea291bd70118bf7dc7.jpg",
            "image_explanation" => "Auf der K-Messe 2016 verteilten freundliche Promoter die beiden Messezeitungen. Adela Kadiric von BOOKmyHOSTESS.com und Jutta Illhardt (hintere Reihe 1. und 2. von links) blicken zufrieden zurück auf acht Messetage.",
            "content" => ' <p>
                    Die Messezeitungen K- AKTUELL und Plastics News berichteten über neue Produkte und Anwendungen, die mehr als 3.000 Aussteller vom 19. bis 26. Oktober auf der Kunststoffmesse präsentierten. Die Leser erfuhren außerdem, wann und wo Veranstaltungen stattfanden, und erhielten umfangreiche Eindrücke vom Messegeschehen. Die beiden Verlage, vertreten durch illhardt medien support, engagierten 30 Promoter, die die beiden Messezeitungen tagesaktuell an die Besucher verteilten. Sie waren an allen acht Messetagen im Einsatz. Gebucht wurden sie bei BOOKmyHOSTESS.com.
                </p>
                <p>
                    „Die Verlage benötigten Promoter, die  aktiv und freundlich  auf die Besucher zugehen und ihnen die beiden Messezeitungen anbieten. Keine leichte Aufgabe, denn Messebesucher haben es in der Regel eilig, und der Promoter hat wenig Zeit, um Kontakt aufzunehmen. Das ist dem Team aber außerordentlich gut gelungen“, erklärt Jutta Illhardt. Die Verlags- und Medienberaterin hat die beiden Verlage auf der K-Messe unterstützt und die Buchung der Promoter über BOOKmyHOSTESS.com empfohlen.
                </p>
                <p>
                    „Im vergangen Jahr haben wir auf einer Fachmesse bereits sehr positive Erfahrungen mit BOOKmyHOSTESS.com und den vermittelten Promotern gemacht.  Da lag es nahe, die Agentur für die  Kunststoffmesse in Düsseldorf wieder zu engagieren. Wie sich zeigte, zu recht: Die Promoter haben während der acht Messetage gute Arbeit geleistet und waren sehr engagiert bei der Sache“, resümiert Illhardt.
                </p>',
        ];
    }

}
