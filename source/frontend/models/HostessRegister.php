<?php

namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class HostessRegister extends Model
{

    public $username;
    public $email;
    public $password;
    public $first_name;
    public $last_name;
    public $terms;
    public $birth_date;
    public $email_validation_secret;
    public $lang;
    public $gender;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'email', 'first_name', 'last_name'], 'required', 'message' => Yii::t('app', 'This field cannot be blank.')],
            ['username', 'unique', 'targetClass' => '\common\models\User'],
            ['email', 'unique', 'targetClass' => '\common\models\User'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['username', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            [['lang', 'gender'], 'required', 'message' => Yii::t('app', 'This field cannot be blank.')],
            [['birth_date'], 'required', 'message' => Yii::t('app', 'This field cannot be blank.')],
            ['birth_date', 'checkYearRule'],
            ['username', 'checkUsername'],
            ['terms', 'required', "message" => Yii::t('app', 'You have to accept Terms&Conditions and Privacy Policy in order to continue.')]
        ];
    }

    public function attributeHints()
    {
        return [
            'username' => Yii::t('app.hostess', 'Username should not be: your first name, last name, or email.')
        ];
    }

    /**
     * Validate year ( only above 18 is allowed )
     * @param string $attribute
     * @param array $params
     */
    public function checkYearRule($attribute, $params)
    {
        $birthDate = strtotime($this->birth_date);
        $limit = strtotime('now -18 year');

        if ($birthDate > $limit)
        {
            $this->addError($attribute, Yii::t('app', 'You must be at least 18 years old to register on this site'));
        }
    }

    public function checkUsername($attribute, $params)
    {
        $username = trim($this->username);
        $username = preg_replace('/\s+/', '', $username);
        
        if (!empty($this->first_name))
        {
            if (strpos($username, $this->first_name) !== false)
            {
                $this->addError($attribute, Yii::t('app.hostess', 'Username should not be: your first name, last name, or email.'));
            }
        }
        if (!empty($this->last_name))
        {
            if (strpos($username, $this->last_name) !== false)
            {
                $this->addError($attribute, Yii::t('app.hostess', 'Username should not be: your first name, last name, or email.'));
            }
        }

        $fullName = $this->first_name . $this->last_name;

        if (!empty($fullName))
        {
            if (strpos($username, $fullName) !== false)
            {
                $this->addError($attribute, Yii::t('app.hostess', 'Username should not be: your first name, last name, or email.'));
            }
        }

        if (!empty($this->email))
        {
            if (strpos($username, $this->email) !== false)
            {
                $this->addError($attribute, Yii::t('app.hostess', 'Username should not be: your first name, last name, or email.'));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'terms' => Yii::t('app', 'I have read and accept the Terms & Conditions & Privacy Policy.'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'first_name' => Yii::t('app', 'First name'),
            'last_name' => Yii::t('app', 'Last name'),
            'birth_date' => Yii::t('app', 'Birth date'),
            'lang' => Yii::t('app', 'Preferred system language')
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        $this->setLanguage();

        if ($this->validate())
        {
            $user = new User();
            $user->setAttributes($this->getAttributes());
            $user->role = User::ROLE_HOSTESS;
            $user->birth_date = date('Y-m-d H:i:s', strtotime($this->birth_date));
            $user->status_id = Yii::$app->status->pending;
            $user->email_validation_secret = $this->generateEmailValidationSecret();
            $user->setPassword($this->password);
            $user->generateAuthKey();

            $transaction = Yii::$app->db->beginTransaction();

            // false because of phone number
            if ($user->save(false))
            {
                $model = new \common\models\HostessProfile();

                $model->user_id = $user->id;
                $model->birth_date = $user->birth_date;
                $model->date_created = $user->date_created;
                $model->gender = $this->gender;
                $model->status_id = Yii::$app->status->pending;

                if ($model->save(false))
                {
                    $transaction->commit();
                    return true;
                }
            }

            $transaction->rollBack();
        }

        return null;
    }

    /**
     * Sets validation secret
     * @return string
     */
    public function generateEmailValidationSecret()
    {
        $code = sha1(microtime(true) . $this->email);
        $this->email_validation_secret = $code;
        return $code;
    }

    /**
     * Return full name
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     * Returns validation link for activation that is sent to user email
     */
    public function getValidationLink()
    {
        return \yii\helpers\Url::to([
                    "//hostess-profile/activate",
                    "id" => $this->email_validation_secret,
                    "lang" => $this->lang
                        ], true);
    }

    /**
     * Save chosen language to session and set it to app
     */
    private function setLanguage()
    {
        Yii::$app->session->set('lang', $this->lang);
        Yii::$app->language = Yii::$app->session->get('lang');
    }

}
