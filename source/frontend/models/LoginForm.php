<?php

namespace frontend\models;

class LoginForm extends \common\models\LoginForm
{

    /**
     * Checks user password, if its old user and was not logged in before
     * send them email verification again
     */
    public function requiresNewPassword()
    {
        $model = User::find()
                ->andWhere(["username" => $this->username])
                ->one();

        if (is_object($model) && $model->request_password_reset === 1)
        {
            $this->_user = $model;
            return true;
        }

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null)
        {
            $this->_user = User::findByUsernameOrEmail($this->username);
        }

        return $this->_user;
    }

}
