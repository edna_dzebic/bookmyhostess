<?php

namespace frontend\models;

class UserImages extends \common\models\UserImages
{

    public function upload()
    {
        $this->user_id = \Yii::$app->user->id;

        return parent::saveImage();
    }

}
