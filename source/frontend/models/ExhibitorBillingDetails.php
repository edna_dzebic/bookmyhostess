<?php

namespace frontend\models;

use yii\base\Model;
use Yii;

class ExhibitorBillingDetails extends Model
{

    public $phone;
    public $billing_tax_number;
    public $billing_street;
    public $billing_company_name;
    public $billing_home_number;
    public $billing_postal_code;
    public $billing_city;
    public $billing_country;
    public $email_validation_secret;
    public $website;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['phone', 'billing_tax_number', 'billing_street', 'billing_company_name', 'billing_home_number', 'billing_postal_code', 'billing_city', 'billing_country'], 'required'
            ],
            [
                ['website'], 'safe'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => Yii::t('app', 'Phone number'),
            'billing_tax_number' => Yii::t('app', 'Billing tax number'),
            'billing_street' => Yii::t('app', 'Billing street'),
            'billing_company_name' => Yii::t('app', 'Billing company name'),
            'billing_home_number' => Yii::t('app', 'Billing house number'),
            'billing_postal_code' => Yii::t('app', 'Billing postal code'),
            'billing_city' => Yii::t('app', 'Billing city'),
            'billing_country' => Yii::t('app', 'Billing country'),
            'website' => Yii::t('app', 'Website')
        ];
    }

    /**
     * Save billing details.
     * 
     * @param \frontend\models\ExhibitorProfile $model
     * @param array $data
     * @return boolean
     */
    public function saveDetails(ExhibitorProfile $model, $data)
    {
        if ($this->load($data) && $this->validate())
        {
            $country = \common\models\Country::findOne([
                        "id" => $this->billing_country
            ]);

            if (!is_object($country))
            {
                $this->addError("billing_country");
                return false;
            }

            $model->user->phone_number = $this->phone;
            $model->country_id = $this->billing_country;
            $model->company_name = $this->billing_company_name;
            $model->tax_number = $this->billing_tax_number;
            $model->street = $this->billing_street;
            $model->home_number = $this->billing_home_number;
            $model->postal_code = $this->billing_postal_code;
            $model->city = $this->billing_city;
            $model->website = $this->website;
            $model->is_invoice_payment_allowed = $country->is_invoice_payment_allowed;

            $transaction = Yii::$app->db->beginTransaction();

            if ($model->user->update(false) !== false &&
                    $model->update(false) !== false)
            {
                $transaction->commit();
                return true;
            }

            $transaction->rollBack();
        }

        return false;
    }

    /**
     * 
     * @param \frontend\models\ExhibitorProfile $model
     */
    public function loadProfileValues(ExhibitorProfile $model)
    {
        $this->phone = $model->user->phone_number;
        $this->billing_country = $model->country_id;
        $this->billing_company_name = $model->company_name;
        $this->billing_tax_number = $model->tax_number;
        $this->billing_street = $model->street;
        $this->billing_home_number = $model->home_number;
        $this->billing_postal_code = $model->postal_code;
        $this->billing_city = $model->city;
        $this->website = $model->website;
    }

}
