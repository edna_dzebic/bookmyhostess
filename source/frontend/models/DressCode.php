<?php

namespace frontend\models;

class DressCode extends \common\models\DressCode
{

    /**
     * Get translated list mapped from id to name
     * @return array
     */
    public static function getAllTranslated()
    {
        $models = static::find()->all();

        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->getTranslatedName();
        }

        return $result;
    }

}
