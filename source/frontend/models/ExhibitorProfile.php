<?php

namespace frontend\models;

use Yii;
use common\models\Request;
use common\models\Email;

class ExhibitorProfile extends \common\models\ExhibitorProfile
{

    /**
     * Returns number of event_requests of current logged in user for which hostess responded
     * Takes not expired events only
     * @return int
     */
    public static function getNumberOfPendingRequests()
    {
        $count = Request::find()
                ->joinWith(["userRequested.exhibitorProfile"])
                ->andWhere(["request.requested_user_id" => \Yii::$app->user->id])
                ->andWhere("request.date_responded is not null and DATE(request.date_to)>=DATE(NOW()) and request.completed is null")
                ->andWhere(['request.status_id' => [
                        Yii::$app->status->request_accept,
                        Yii::$app->status->request_reject
                    ]
                ])
                ->andWhere("DATE_FORMAT(request.date_responded,'%Y-%m-%d %H:%i:%s') >=  DATE_FORMAT(exhibitor_profile.last_profile_view,'%Y-%m-%d %H:%i:%s')")
                ->count();


        return $count;
    }

    /**
     * Get selected preferred language value
     * @return string
     */
    public function getLangValue()
    {
        if (isset($this->user) && isset($this->user->lang))
        {
            if (isset(Yii::$app->params['user_languages'][$this->user->lang]))
            {
                return Yii::$app->params['user_languages'][$this->user->lang];
            }
        }
        return '';
    }

    /**
     * Save values from profile to user
     */
    public function saveValuesToUser()
    {
        $this->user->lang = $this->lang;
        $this->user->gender = $this->gender;
        $this->user->update(false);

        Yii::$app->session->set('lang', $this->lang);
        Yii::$app->language = Yii::$app->session->get('lang');
    }

    /**
     * Change email
     * @param array $data
     * @param string $oldEmail
     * @return boolean
     */
    public function changeEmail($data, $oldEmail)
    {
        if ($this->user->load($data))
        {
            if ($this->user->email === $oldEmail)
            {
                $this->user->addError('email', Yii::t('app.exhibitor', 'Please use different Email.'));
            } else
            {
                if ($this->user->update())
                {
                    if (Email::sendChangeEmailToExhibitor($this->user))
                    {
                        return true;
                    } else
                    {
                        $this->user->addError('email', Yii::t('app.exhibitor', 'Error while saving email.'));
                    }
                } else
                {
                    $this->user->addError('email', Yii::t('app.exhibitor', 'Error while saving email.'));
                }
            }
        }
        return false;
    }

}
