<?php

namespace frontend\models;

use Yii;
use yii\data\ArrayDataProvider;
use common\models\UserMessages;
use common\models\HostessPreferredCity;
use common\models\HostessPreferredJob;
use common\models\Email;

class HostessProfile extends \common\models\HostessProfile
{

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'hair_color_id' => Yii::t('app.hostess', 'Hair Color'),
            'height' => Yii::t('app.hostess', 'Height'),
            'weight' => Yii::t('app.hostess', 'Weight'),
            'waist_size' => Yii::t('app.hostess', 'Waist Size'),
            'shoe_size' => Yii::t('app.hostess', 'Shoe Size'),
            'education_graduation' => Yii::t('app.hostess', 'Graduation'),
            'education_university' => Yii::t('app.hostess', 'University'),
            'education_vocational_training' => Yii::t('app.hostess', 'Vocational Training'),
            'education_profession' => Yii::t('app.hostess', 'Profession'),
            'education_special_knowledge' => Yii::t('app.hostess', 'Special Knowledge')                ]
        );
    }
    
    public function attributeHints()
    {
        return array_merge(
                parent::attributeHints(), [            
            'education_special_knowledge' => Yii::t('app.hostess', 'E.g: riding, dancing, photography')                ]
        );
    }

    /**
     * Get colors for display
     * @return array
     */
    public function getRequestColors()
    {
        $total = $this->getTotalRequestsCount();
        $confirmed = $this->getConfirmedRequestsCount();
        $notAnswered = $this->getNotAnsweredRequestCount();
        $rejected = $this->getRejectedRequestCount();
        $time = $this->getTimeToRespondValue();

        $result = [
            "confirmed" => $this->getConfirmedColor($total, $confirmed),
            "notAnswered" => $this->getColor($total, $notAnswered),
            "rejected" => $this->getColor($total, $rejected),
            "time" => $this->getTimeToRespondColor($time)
        ];

        return $result;
    }

    /**
     * Removes existing preferred jobs an adds new ones
     * @param array $ids
     */
    public function savePreferredJobs($ids)
    {
        HostessPreferredJob::deleteAll(["user_id" => (int) $this->user_id]);

        if (is_array($ids))
        {
            foreach ($ids as $id)
            {
                $model = new HostessPreferredJob();
                $model->user_id = $this->user_id;
                $model->event_job_id = $id;
                $model->save();
            }
        }

        $this->recalculateProfileCompleteness();
    }

    /**
     * Removes existin preferred cities an adds new ones
     * @param array $ids
     */
    public function savePreferredCities($ids)
    {
        HostessPreferredCity::deleteAll(["user_id" => (int) $this->user_id]);

        if (is_array($ids))
        {
            foreach ($ids as $id)
            {
                $model = new HostessPreferredCity();
                $model->user_id = $this->user_id;
                $model->city_id = $id;
                $model->save();
            }
        }

        $this->recalculateProfileCompleteness();
    }

    /**
     * Get past work as array data provider
     *
     * @return ArrayDataProvider
     */
    public function getPastWorkProvider()
    {
        if (isset($this->pastWork) && count($this->pastWork) > 0)
        {
            $pastWorkModels = $this->pastWork;
        } else
        {
            $pastWorkModels = [];
        }

        return new ArrayDataProvider(
                [
            'allModels' => $pastWorkModels,
            'pagination' => false
                ]
        );
    }

    /**
     * Return message from admin if exists
     * @return boolean
     */
    public function getAdminMessage()
    {
        $model = UserMessages::find()
                ->andWhere(["to_user_id" => Yii::$app->user->id, "accepted" => 0])
                ->one();

        if ($model)
        {
            return $model->message;
        }

        return false;
    }

    /**
     * Set values from user
     */
    public function setValuesFromUser()
    {
        if (isset($this->user) && isset($this->user->lang))
        {
            $this->lang = $this->user->lang;
        }
    }

    /**
     * Save values from profile to user model
     */
    public function saveValuesToUser()
    {
        $this->user->lang = $this->lang;
        $this->user->gender = $this->gender;
        $this->user->update(false);

        Yii::$app->session->set('lang', $this->lang);
        Yii::$app->language = Yii::$app->session->get('lang');
    }

    /**
     * Is hostess allowed to request to postpone trade license
     * @return boolean
     */
    public function getCanPostponeTradeLicense()
    {
        $hasActiveRequest = TradeLicenseExtend::find()
                ->andWhere(["hostess_profile_id" => $this->id])
                ->andWhere(["status_id" => Yii::$app->status->active])
                ->exists();

        return $hasActiveRequest ? false : true;
    }

    /**
     * Does hostess has valid request to postpone upload of license
     * @return boolean
     */
    public function getHasRequestedPostponeTradeLicense()
    {
        $hasActiveRequest = TradeLicenseExtend::find()
                ->andWhere(["hostess_profile_id" => $this->id])
                ->andWhere(["status_id" => Yii::$app->status->active])
                ->andWhere([">=", "DATE(date)", date("Y-m-d")])
                ->exists();

        return $hasActiveRequest ? true : false;
    }

    /**
     * Change email
     * @param array $data
     * @param string $oldEmail
     * @return boolean
     */
    public function changeEmail($data, $oldEmail)
    {
        if ($this->user->load($data))
        {
            if ($this->user->email === $oldEmail)
            {
                $this->user->addError('email', Yii::t('app.hostess', 'Please use different Email.'));
            } else
            {
                if ($this->user->update())
                {
                    if (Email::sendChangeEmailToHostess($this->user))
                    {
                        return true;
                    } else
                    {
                        $this->user->addError('email', Yii::t('app.hostess', 'Error while saving email.'));
                    }
                } else
                {
                    $this->user->addError('email', Yii::t('app.hostess', 'Error while saving email.'));
                }
            }
        }
        return false;
    }

    /**
     * Create base profile for hostess
     * @param integer $id
     */
    public static function createBaseProfile($id)
    {
        $model = HostessProfile::find()
                ->andWhere(["user_id" => $id])
                ->one();

        if (!is_object($model))
        {
            $model = new HostessProfile();
            $model->user_id = $id;
        }

        $model->status_id = Yii::$app->status->active;
        $model->save(false);
    }

    /**
     * Returns boolean is profile completed or ready for admin approval
     */
    public function isProfileCompleted()
    {
        $this->scenario = 'profile-complete';

        $this->validateImages();
        $this->validateDocuments();
        $this->validate(null, false);
        $this->validateLanguages();

        if (!$this->hasErrors())
        {
            return true;
        }

        return false;
    }

    /**
     * Checks do user have any images and adds error if not
     * @return boolean
     */
    private function validateImages()
    {
        if (isset($this->images) && count($this->images) > 0)
        {
            return true;
        }

        $this->addError('images', Yii::t('app', 'Profile images missing! You need to upload your images!'));
    }

    /**
     * Checks do user have any images and adds error if not
     * @return boolean
     */
    private function validateDocuments()
    {
        if (
                (isset($this->documents) && count($this->documents) > 0) ||
                ($this->getHasRequestedPostponeTradeLicense() === true)
        )
        {
            return true;
        }

        $this->addError('documents', Yii::t('app.hostess', 'Trade licence file is missing, upload licence under Documents section or request to postpone upload of trade license file!'));
    }

    /**
     * Validate entered languages
     * @return boolean
     */
    private function validateLanguages()
    {
        if (isset($this->languages) && count($this->languages) > 1)
        {
            $nativeLevel = LanguageLevel::findOne(["name" => "Native"]);

            if (isset($nativeLevel))
            {
                foreach ($this->languages as $lang)
                {
                    if ($lang->level->id === $nativeLevel->id)
                    {
                        return true;
                    }
                }
            }
        }

        $this->addError('languages', Yii::t('app', 'Languages missing! You need to set mother language and at least one more.'));
    }

    /**
     * Get color based on input values
     * @param integer $total
     * @param integer $value
     * @return string
     */
    private function getColor($total, $value)
    {
        $res = $total > 0 ? $value / $total : 0;


        if ($res >= 0.75)
        {
            return "red";
        }
        if ($res >= 0.50)
        {
            return "orange";
        }
        if ($res >= 0.25)
        {
            return "yellow";
        }

        return "green";
    }

    /**
     * Get color for confirmed requests number for display
     * @param integer $total
     * @param integer $value
     * @return string
     */
    private function getConfirmedColor($total, $value)
    {
        $res = $total > 0 ? $value / $total : 1;

        if ($res >= 0.75)
        {
            return "green";
        }
        if ($res >= 0.50)
        {
            return "yellow";
        }
        if ($res >= 0.25)
        {
            return "orange";
        }

        return "red";
    }

    /**
     * Get color for time to respond in statistics part
     * @param integer $value
     * @return string
     */
    private function getTimeToRespondColor($value)
    {
        if ($value <= 4)
        {
            return "green";
        }
        if ($value <= 8)
        {
            return "yellow";
        }
        if ($value <= 12)
        {
            return "orange";
        }

        return "red";
    }

}
