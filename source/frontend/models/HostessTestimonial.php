<?php

namespace frontend\models;

use Yii;

class HostessTestimonial extends \common\models\HostessTestimonial
{

    public static function getAllForCarousel()
    {

        $models = static::find()
                ->orderBy(['date_created' => SORT_DESC])
                ->all();

        $result = [];

        foreach ($models as $model)
        {
            $result[] = [
                'caption' => static::createHtml($model),
                'content' => ''
            ];
        }

        return $result;
    }

    private static function createHtml($model)
    {
        $caption = $model->text . '</br><small>' . $model->source . '</small>';

        return $caption;
    }

}
