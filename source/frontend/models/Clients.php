<?php

namespace frontend\models;

use Yii;

class Clients extends \yii\base\Model {

    public static function getThumbnails() {
        $logos = [
            'gpcmedical.jpg',
            'healthybalance.jpg',
            'jas.jpg',
            'logo_jaehn.png',
            'plasticnews.jpg',
            'varibike.jpg'
        ];

        $result = [];
        foreach ($logos as $logo) {

            $url = "/images/featured_clients/" . $logo;

            $result[] = Yii::$app->urlManager->createAbsoluteUrl([$url]);
        }

        return $result;
    }
}
