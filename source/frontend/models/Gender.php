<?php

namespace frontend\models;

use Yii;

class Gender extends \yii\base\Model {

    public static function getList() {
        return [
            Yii::t('app', 'Ms'),
            Yii::t('app', 'Mr'),
        ];
    }

    public static function getListForHostess() {
        return [
            Yii::t('app', 'Female'),
            Yii::t('app', 'Male'),
        ];
    }

}
