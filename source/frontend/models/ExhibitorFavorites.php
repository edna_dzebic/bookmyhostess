<?php

namespace frontend\models;

use Yii;

class ExhibitorFavorites extends \common\models\ExhibitorFavorites
{

    /**
     * Get data provider for exhibitor
     * @return \yii\data\ActiveDataProvider
     */
    public static function getDataProvider()
    {
        $query = static::find()->joinWith(["hostess.hostessProfile"]);

        $query->andWhere([static::tableName() . ".user_id" => Yii::$app->user->id]);
        $query->andWhere(["user.status_id" => Yii::$app->status->active]);

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 12],
        ]);

        return $dataProvider;
    }

}
