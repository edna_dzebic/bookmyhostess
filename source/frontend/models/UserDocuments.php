<?php

namespace frontend\models;

class UserDocuments extends \common\models\UserDocuments
{

    public function upload()
    {
        $this->user_id = \Yii::$app->user->id;

        return parent::saveDocument();
    }

}
