<?php

namespace frontend\models;

use Yii;
use common\models\UserMessages;

class User extends \common\models\User
{

    public $confirm_password;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
                parent::rules(), [
            [['confirm_password'], 'compare', 'compareAttribute' => 'password', 'on' => 'change-password'],
            [['password', 'confirm_password'], 'required', 'on' => 'change-password']
                ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'password' => Yii::t('app', 'Password'),
            'confirm_password' => Yii::t('app', 'Confirm Password'),
                ]
        );
    }

    public static function hasMessages()
    {
        $model = UserMessages::find()
                ->andWhere(["to_user_id" => \Yii::$app->user->id, "accepted" => 0])
                ->count();

        if ($model)
        {
            return $model;
        }

        return false;
    }

    /**
     * List of allowed roles for login
     *
     * @return array
     */
    protected static function allowedRoles()
    {
        return [
            static::ROLE_EXHIBITOR,
            static::ROLE_HOSTESS,
            static::ROLE_ADMIN,
            static::ROLE_SYS_ADMIN
        ];
    }

}
