<?php

namespace frontend\models;

use Yii;
use common\models\Email;

class TradeLicenseExtend extends \common\models\TradeLicenseExtend
{

    public function sendRequest($data)
    {
        if ($this->load($data))
        {
            $this->date = date("Y-m-d", strtotime($this->date));

            $this->status_id = Yii::$app->status->active;

            if ($this->save())
            {
                Email::sendHostessPostponeTradeLicenseRequestToAdmin($this);
                return true;
            }
        }

        return false;
    }

}
