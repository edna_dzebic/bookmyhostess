<?php

namespace frontend\controllers;

use Yii;
use kartik\mpdf\Pdf;
use common\models\Invoice;
use common\models\Email;
use common\models\Register;
use common\models\Request;
use frontend\components\Paypal;
use frontend\components\Cart;
use frontend\models\Payment;

class PaypalController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;

    public function actionIpn()
    {
        $post = Yii::$app->request->post();
        
        $res = (new Paypal())->ipn($post);
        
        if (strcmp($res, "VERIFIED") == 0)
        {
            return $this->processVerifiedPayment($post);
        } else if (strcmp($res, "INVALID") == 0)
        {
            return $this->processInvalidPayment($post);
        }
        
        return '';
    }

    public function actionComplete()
    {
        //remove everything from cart       
        Yii::$app->session->remove(Cart::CART_SESSION_KEY);

        Yii::$app->session->setFlash('payment.success', Yii::t('app.exhibitor', 'Your transaction was successful and will appear on the list of your invoices after processing. <br> Thank you for your patience and understanding.'), true);
        
        return $this->redirect(\Yii::$app->urlManager->createUrl(["//exhibitor/bookings"]));
    }

    private function processVerifiedPayment($post)
    {
        
        // TODO TRANSACTIONS
        Yii::info("VERIFIED PAYMENT");
        $status = isset($post["payment_status"]) ? $post["payment_status"] : null;

        if (isset($status))
        {
            $paymentModel = new Payment();
            if ($paymentModel->processPayment($post))
            {
                Yii::info("Process payment");

                $eventReqIds = explode(",", $post["item_number"]);

                $coupon = $paymentModel->getCoupon();

                if (isset($coupon))
                {
                    Yii::info("Coupon code is " . $coupon->code);
                }

                $invoice = $this->createInvoice($post, $paymentModel, $coupon);

                if ($invoice !== null)
                {
                    foreach ($eventReqIds as $eventReqId)
                    {
                        if ($eventReqId != "" && $eventReqId != ",")
                        {

                            $eventRequest = Request::find()
                                    ->andWhere([ "id" => $eventReqId])
                                    ->one();

                            if ($eventRequest->completed === Request::COMPLETED)
                            {
                                continue;
                            }

                            $eventRequest->invoice_id = $invoice->id;
                            $eventRequest->completed = Request::COMPLETED;
                            $eventRequest->update(false);

                            Email::sendInvoiceEmailToHostess($eventRequest);

                            // remove hostess from this event since she is now booked
                            $eventRegisterModel = Register::find()
                                    ->andWhere([
                                        "user_id" => $eventRequest->user_id,
                                        "event_id" => $eventRequest->event_id])
                                    ->one();

                            if (is_object($eventRegisterModel))
                            {
                                $eventRegisterModel->delete();
                            } else
                            {
                                Yii::error("EVENT REGISTER MODEL NOT FOUND.");
                            }
                                                        
                            $eventRequest->user->hostessProfile->recalculateProfileReliability();
                        }
                    }

                    $invoiceFileName = $this->createInvoiceFile($invoice->id);
                    $invoice->sendNotificationEmails($invoiceFileName);

                    $invoice->fillHistoryData();
                }
            } else
            {
                Yii::info("Process Payment returned false!");
            }

            return true; // return actually anything to paypal
        }
        
        return $this->processInvalidPayment($post);
    }

    private function processInvalidPayment($post)
    {
        Yii::error("INVALID PAYMENT");
        
        return Email::sendPaypalInvalidTransactionEmail($post);
    }

    /**
     * Creates PDF invoice and returns its file name
     * @param type $invoiceId
     * @return string
     */
    private function createInvoiceFile($invoiceId)
    {
        define('_MPDF_TTFONTDATAPATH', Yii::getAlias('@runtime/mpdf'));

        $model = Invoice::find()
                ->andWhere(["id" => $invoiceId])
                ->one();

        Yii::$app->language = $model->userRequested->lang;

        if (is_object($model))
        {
            $content = $this->renderPartial('//invoice/pdf', ["model" => $model]);
            $fileName = $model->filename;
            $pdf = new Pdf([
                'marginTop' => 9,
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_FILE,
                'content' => $content,
                'filename' => $fileName,
                'cssFile' => 'css/invoice_pdf.css',
                'cssInline' => file_get_contents(Yii::getAlias('@frontend/assets/css/invoice_pdf.css')),
                'options' => ['title' => \Yii::$app->name],
            ]);

            // return the pdf output as per the destination setting
            $pdf->render();
            return $fileName;
        }
    }

    /**
     * 
     * @param array $post
     * @param \frontend\models\Payment $payment
     * @param \common\models\Coupon $coupon
     * @return Invoice
     */
    private function createInvoice($post, $payment, $coupon = null)
    {

        $invoice = new Invoice();
        $invoice->amount = $post['mc_gross'];

        $invoice->status = Invoice::STATUS_PAID;
        $invoice->date_paid = date('Y-m-d H:i:s');
        $invoice->paypal_transaction_id = $post['txn_id'];
        $invoice->date_created = $invoice->date_paid;
        $invoice->requested_user_id = $payment->user_id;
        $invoice->exhibitor_booking_type = $payment->getExhibitorModel()->booking_type;
        $invoice->payment_type = Invoice::PAYMENT_TYPE_PAYPAL;
        $invoice->is_reminder_allowed = 0;
        
        $invoice->setNewRealId();

        if (is_object($coupon))
        {
            $invoice->coupon_amount = $coupon->amount;
            $invoice->coupon_code = $coupon->code;
            $invoice->coupon_id = $coupon->id;
            $invoice->coupon_type = $coupon->coupon_type;
        }

        if ($invoice->save())
        {
            $invoice->filename = 'invoice/invoice_' . $invoice->id . '.pdf';

            if ($invoice->update() !== false)
            {
                return $invoice;
            }
        }
        
        Email::sendPayPalFailedEmail(json_encode($invoice->getErrors()));
        
        return null;
    }

}

/**
 * Example return
 * {
  "mc_gross":"50.00",
  "protection_eligibility":"Eligible",
  "address_status":"unconfirmed",
  "payer_id":"3CYSPSXZLKUHG",
  "tax":"0.00",
  "address_street":"Ottostr. 7,",
  "payment_date":"12:57:06 May 31, 2015 PDT",
  "payment_status":"Pending",
  "charset":"windows-1252",
  "address_zip":"",
  "first_name":"semir",
  "address_country_code":"BA",
  "address_name":"Adela Kadiric",
  "notify_version":"3.8",
  "custom":"93775727ec976dee4cadefaeb52ce545dbaf2726",
  "payer_status":"verified",
  "business":"trokdy@gmail.com",
  "address_country":"Bosnia and Herzegovina",
  "address_city":"n\/a",
  "quantity":"1",
  "verify_sign":"ANtiIz5HDF9uXhDqnNqZkeyQ.XPmAO1KTi23Di2AZ4wOqnhF7RfDtJqq",
  "payer_email":"semir.h@leftor.ba",
  "txn_id":"6J872391RW098953U",
  "payment_type":"instant",
  "last_name":"hodzic",
  "address_state":"",
  "receiver_email":"trokdy@gmail.com",
  "receiver_id":"YFTJQFU6EMCVC",
  "pending_reason":"multi_currency",
  "txn_type":"web_accept",
  "item_name":"BookMyHostess.com booking",
  "mc_currency":"EUR",
  "item_number":"20,",
  "residence_country":"US",
  "test_ipn":"1",
  "handling_amount":"0.00",
  "transaction_subject":"93775727ec976dee4cadefaeb52ce545dbaf2726",
  "payment_gross":"",
  "shipping":"0.00",
  "ipn_track_id":"41cbf8ef2fce6"
  }
 */