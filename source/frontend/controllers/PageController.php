<?php

namespace frontend\controllers;

use frontend\components\FrontController;

class PageController extends FrontController {

    public function actionView($seo) {
        return $this->render($seo);
    }
}
