<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontController;
use frontend\components\Cart;
use yii\filters\AccessControl;
use common\models\Email;
use common\models\Invoice;
use common\models\Request;
use common\models\Register;
use frontend\models\ExhibitorProfile;
use frontend\models\Checkout;
use yii\web\BadRequestHttpException;
use kartik\mpdf\Pdf;

class CheckoutController extends FrontController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $profileModel = $this->getProfileModel();

        $cartInstance = Cart::getInstance();

        $models = $cartInstance->getAllModels();

        if (count($models) == 0)
        {
            return $this->redirect(["/checkout/empty"]);
        }

        $coupon = $cartInstance->getCoupon();
        $bookingAmount = $cartInstance->getTotalBookingPrice();
        $taxAmount = $cartInstance->getTaxAmount($profileModel);
        $grossAmount = $cartInstance->getGrossAmount($profileModel);
        $payPalAmount = $cartInstance->getPayPalAmount($profileModel);
        $ids = $cartInstance->getRequestIds();

        $couponGrossAmount = $cartInstance->getCouponGrossAmount($profileModel);

        return $this->render('checkout', [
                    "steps" => Checkout::getAllSteps(),
                    "hideNavigation" => false,
                    "activeStep" => "1",
                    "view" => "items",
                    "data" => [
                        "models" => $models,
                        "profileModel" => $profileModel,
                        "coupon" => $coupon,
                        'bookingAmount' => $bookingAmount,
                        'taxAmount' => $taxAmount,
                        'grossAmount' => $grossAmount,
                        'payPalAmount' => $payPalAmount,
                        'couponNettoAmount' => $cartInstance->getCouponNettoAmount($profileModel),
                        'couponGrossAmount' => $couponGrossAmount,
                        'couponTaxAmount' => $cartInstance->getCouponTaxAmount($couponGrossAmount),
                        'ids' => $ids
                    ]
        ]);
    }

    public function actionInvoiceDetails()
    {
        $profileModel = $this->getProfileModel();

        $cartInstance = Cart::getInstance();

        $models = $cartInstance->getAllModels();
        if (count($models) == 0)
        {
            return $this->redirect(["/checkout/empty"]);
        }

        $profileModelCompleted = $profileModel->isProfileCompleted();
        $formModel = new \frontend\models\ExhibitorBillingDetails();
        $formModel->loadProfileValues($profileModel);

        if ($profileModelCompleted === false && Yii::$app->request->isPost)
        {
            if ($formModel->saveDetails($profileModel, Yii::$app->request->post()))
            {
                return $this->redirect(["/checkout/payment"]);
            }
        }

        return $this->render('checkout', [
                    "steps" => Checkout::getAllSteps(),
                    "view" => "invoice_data",
                    "hideNavigation" => false,
                    "activeStep" => "2",
                    "data" => [
                        "profileModel" => $profileModel,
                        "profileModelCompleted" => $profileModelCompleted,
                        "formModel" => $formModel
                    ]
        ]);
    }

    public function actionPayment()
    {
        $profileModel = $this->getProfileModel();

        $cartInstance = Cart::getInstance();

        $models = $cartInstance->getAllModels();
        if (count($models) == 0)
        {
            return $this->redirect(["/checkout/empty"]);
        }

        $profileModelCompleted = $profileModel->isProfileCompleted();
        if ($profileModelCompleted === false)
        {
            return $this->redirect(["/checkout/invoice-details"]);
        }
        
        $payPalAmount = $cartInstance->getPayPalAmount($profileModel);
        if($payPalAmount == 0)
        {
            $paymentType = Invoice::PAYMENT_TYPE_RECEIPT;
            if ($cartInstance->setPaymentType($profileModel, $paymentType))
            {
                return $this->redirect(["/checkout/final"]);
            } else
            {
                Yii::$app->session->setFlash('cart.error', $cartInstance->errorMessage, true);
            }
        }

        if (Yii::$app->request->isPost)
        {
            $paymentType = Yii::$app->request->post('payment_type');

            if ($cartInstance->setPaymentType($profileModel, $paymentType))
            {
                return $this->redirect(["/checkout/final"]);
            } else
            {
                Yii::$app->session->setFlash('cart.error', $cartInstance->errorMessage, true);
            }
        }

        $allowedPayments = Checkout::getDisplayedPaymentTypes($profileModel);

        $selectedPayment = $cartInstance->getPaymentType();

        return $this->render('checkout', [
                    "steps" => Checkout::getAllSteps(),
                    "hideNavigation" => false,
                    "activeStep" => "3",
                    "view" => "payment",
                    "data" => [
                        'allowedPayments' => $allowedPayments,
                        'selectedPayment' => $selectedPayment
                    ]
        ]);
    }

    public function actionFinal()
    {
        $profileModel = $this->getProfileModel();

        $cartInstance = Cart::getInstance();

        $models = $cartInstance->getAllModels();
        if (count($models) == 0)
        {
            return $this->redirect(["/checkout/empty"]);
        }

        $profileModelCompleted = $profileModel->isProfileCompleted();
        if ($profileModelCompleted === false)
        {
            return $this->redirect(["/checkout/invoice-details"]);
        }

        $selectedPayment = $cartInstance->getPaymentType();
        if (empty($selectedPayment))
        {
            return $this->redirect(["/checkout/payment"]);
        }

        $coupon = $cartInstance->getCoupon();
        $bookingAmount = $cartInstance->getTotalBookingPrice();
        $taxAmount = $cartInstance->getTaxAmount($profileModel);
        $grossAmount = $cartInstance->getGrossAmount($profileModel);
        $payPalAmount = $cartInstance->getPayPalAmount($profileModel);
        $ids = $cartInstance->getRequestIds();

        $couponGrossAmount = $cartInstance->getCouponGrossAmount($profileModel);

        $applyPaypalForm = $payPalAmount > 0 &&
                $cartInstance->getPaymentType() === \frontend\models\Invoice::PAYMENT_TYPE_PAYPAL;

        return $this->render('checkout', [
                    "steps" => Checkout::getAllSteps(),
                    "hideNavigation" => false,
                    "activeStep" => "4",
                    "view" => "final",
                    "data" => [
                        "models" => $models,
                        "profileModel" => $profileModel,
                        "coupon" => $coupon,
                        'bookingAmount' => $bookingAmount,
                        'taxAmount' => $taxAmount,
                        'grossAmount' => $grossAmount,
                        'payPalAmount' => $payPalAmount,
                        'couponNettoAmount' => $cartInstance->getCouponNettoAmount($profileModel),
                        'couponGrossAmount' => $couponGrossAmount,
                        'couponTaxAmount' => $cartInstance->getCouponTaxAmount($couponGrossAmount),
                        'ids' => $ids,
                        "paymentMethod" => $cartInstance->getPaymentType(),
                        "paymentMethodLabel" => Checkout::getPaymentLabel($cartInstance->getPaymentType()),
                        "applyPaypalForm" => $applyPaypalForm
                    ]
        ]);
    }

    public function actionApplyCoupon()
    {
        $profileModel = $this->getProfileModel();

        $cartInstance = Cart::getInstance();

        if (Yii::$app->request->isPost)
        {
            $code = Yii::$app->request->post('coupon');

            if ($cartInstance->setCoupon($code))
            {
                Yii::$app->session->setFlash('cart.success', Yii::t('app.exhibitor', 'Coupon successfully applied!'), true);
            } else
            {
                Yii::$app->session->setFlash('cart.error', $cartInstance->errorMessage, true);
            }
        }

        return $this->redirect(["/checkout/index"]);
    }

    public function actionCompleteBooking()
    {
        $profileModel = $this->getProfileModel();

        $cartInstance = Cart::getInstance();

        $models = $cartInstance->getAllModels();
        if (count($models) == 0)
        {
            return $this->redirect(["/checkout/empty"]);
        }

        $profileModelCompleted = $profileModel->isProfileCompleted();
        if ($profileModelCompleted === false)
        {
            return $this->redirect(["/checkout/invoice-details"]);
        }

        $selectedPayment = $cartInstance->getPaymentType();
        if (empty($selectedPayment) || $selectedPayment === Invoice::PAYMENT_TYPE_PAYPAL)
        {
            Yii::$app->session->setFlash('cart.error', Yii::t('app.exhibitor', 'Payment not valid!'), true);
            return $this->redirect(["/checkout/payment"]);
        }

        $payPalAmount = $cartInstance->getPayPalAmount($profileModel);

        $transaction = Yii::$app->db->beginTransaction();

        $invoice = $this->createInvoice($cartInstance, $profileModel, $payPalAmount);

        if ($invoice !== null)
        {
            $models = $cartInstance->getAllModels();

            foreach ($models as $eventRequest)
            {
                $eventRequest->invoice_id = $invoice->id;
                $eventRequest->completed = Request::COMPLETED;

                if ($eventRequest->update(false) !== false)
                {
                    Email::sendInvoiceEmailToHostess($eventRequest);

                    $eventRequest->user->hostessProfile->recalculateProfileReliability();

                    // remove hostess from this event since she is now booked
                    $eventRegisterModel = Register::find()
                            ->andWhere([
                                "register.user_id" => $eventRequest->user_id,
                                "register.event_id" => $eventRequest->event_id])
                            ->one();

                    if (is_object($eventRegisterModel))
                    {
                        $eventRegisterModel->delete();
                    } else
                    {
                        Yii::error("EVENT REGISTER MODEL NOT FOUND.");
                    }
                }
            }

            $invoiceFileName = $this->createInvoiceFile($invoice->id);
            $invoice->sendNotificationEmails($invoiceFileName);

            Yii::$app->session->remove(Cart::CART_SESSION_KEY);

            $transaction->commit();

            //remove everything from cart       
            Yii::$app->session->remove(Cart::CART_SESSION_KEY);

            $invoice->fillHistoryData();

            return $this->redirect(["/checkout/success", "invoiceId" => $invoice->id]);
        } else
        {
            $transaction->rollBack();
            Yii::$app->session->setFlash('cart.error', Yii::t('app.exhibitor', 'An error occured while processing Your request. Please notify our administrators. <br> Thank you for patiance and understanding!'), true);
            return $this->redirect(["/checkout/index"]);
        }
    }

    public function actionSuccess($invoiceId)
    {
        $profileModel = $this->getProfileModel();
        $invoice = Invoice::findOne(["id" => $invoiceId]);

        if ($invoice->requested_user_id !== $profileModel->user_id)
        {
            throw new \yii\web\ForbiddenHttpException(Yii::t('app.exhibitor', 'You are not allowed to see selected invoice!'));
        }

        $invoiceLink = Yii::$app->urlManager->createUrl([$invoice->filename]);

        return $this->render('checkout', [
                    "steps" => Checkout::getAllSteps(),
                    "activeStep" => 0,
                    "hideNavigation" => true,
                    "view" => "success",
                    "data" => [
                        "invoiceLink" => $invoiceLink
                    ]
        ]);
    }

    public function actionEmpty()
    {
        return $this->render('checkout', [
                    "steps" => Checkout::getAllSteps(),
                    "activeStep" => 1,
                    "hideNavigation" => false,
                    "view" => "items_empty",
                    "data" => []
        ]);
    }

    /**
     * 
     * @param \frontend\components\Cart $cartInstance
     * @param \common\models\ExhibitorProfile $profileModel
     * @param decimal $payPalAmount
     * @return Invoice
     */
    private function createInvoice($cartInstance, $profileModel, $payPalAmount)
    {
        $coupon = $cartInstance->getCoupon();

        $invoice = new Invoice();
        $invoice->amount = $payPalAmount;

        $invoice->date_paid = date('Y-m-d H:i:s');
        $invoice->paypal_transaction_id = NULL;
        $invoice->date_created = $invoice->date_paid;
        $invoice->requested_user_id = $profileModel->user_id;
        $invoice->exhibitor_booking_type = $profileModel->booking_type;
        $invoice->is_reminder_allowed = 0;
        
        if ($invoice->getIsNormalInvoice())
        {
            $invoice->setNewRealId();
            $invoice->payment_type = $cartInstance->getPaymentType();
            $invoice->status = Invoice::STATUS_NOT_PAID;
            $invoice->is_reminder_allowed = 1;
            
        } else
        {
            $invoice->payment_type = Invoice::PAYMENT_TYPE_RECEIPT;
            $invoice->status = Invoice::STATUS_PAID;
        }

        if (is_object($coupon))
        {
            $invoice->coupon_amount = $coupon->amount;
            $invoice->coupon_code = $coupon->code;
            $invoice->coupon_id = $coupon->id;
            $invoice->coupon_type = $coupon->coupon_type;
        }

        if ($invoice->save())
        {
            $invoice->filename = $invoice->getIsNormalInvoice() ? 'invoice/invoice_' . $invoice->id . '.pdf' : 'receipt/receipt_' . $invoice->id . '.pdf';

            if ($invoice->update() !== false)
            {
                return $invoice;
            }
        }

        Email::sendCompleteBookingFailedEmail(json_encode($invoice->getErrors()));
        return null;
    }

    /**
     * Creates PDF invoice and returns its file name
     * @param type $invoiceId
     * @return string
     */
    private function createInvoiceFile($invoiceId)
    {
        define('_MPDF_TTFONTDATAPATH', Yii::getAlias('@runtime/mpdf'));

        $model = Invoice::findOne(["id" => $invoiceId]);

        Yii::$app->language = $model->userRequested->lang;

        $content = $this->renderPartial('//invoice/pdf', ["model" => $model]);
        $fileName = $model->filename;
        $pdf = new Pdf([
            'marginTop' => 9,
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_FILE,
            'content' => $content,
            'filename' => $fileName,
            'cssFile' => 'css/invoice_pdf.css',
            'cssInline' => file_get_contents(Yii::getAlias('@frontend/assets/css/invoice_pdf.css')),
            'options' => ['title' => \Yii::$app->name],
        ]);

        // return the pdf output as per the destination setting
        $pdf->render();
        return $fileName;
    }

    /**
     * @return \frontend\models\ExhibitorProfile
     * @throws BadRequestHttpException
     */
    private function getProfileModel()
    {
        if (Yii::$app->user->isGuest || Yii::$app->user->identity->getIsExhibitor() === false)
        {
            throw new BadRequestHttpException(\Yii::t('app.exhibitor', 'An error occured. Please do not repeat this request.'));
        }

        $model = ExhibitorProfile::find()
                ->joinWith(["user"])
                ->andWhere(["user.id" => \Yii::$app->user->id])
                ->one();

        if ($model === null || $model === false)
        {
            throw new BadRequestHttpException(\Yii::t('app.exhibitor', 'An error occured. Please do not repeat this request.'));
        }


        return $model;
    }

}
