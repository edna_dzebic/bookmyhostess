<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontController;
use frontend\models\Event;
use frontend\models\Register;
use frontend\models\EventSearch;
use frontend\models\HostessProfile;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class EventController extends FrontController
{

    /**
     * Search parametri, trebaju kod listanja evenata
     * @var array
     */
    public $searchData = [];

    public function actionIndex()
    {

        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->getIsHostess())
        {
            $hostessProfileModel = HostessProfile::find()
                    ->joinWith(["user"])
                    ->andWhere(["user.id" => Yii::$app->user->id])
                    ->one();

            if (!$hostessProfileModel->isAddressComplete())
            {
                return $this->redirect(["hostess-profile/billing-details"]);
            }
        }

        $model = new EventSearch();
        $dataProvider = $model->getSearchProvider(Yii::$app->request->get());
        $this->searchData = [
            'country_id' => $model->country_id,
            'city_id' => $model->city_id,
            'event' => $model->event,
        ];

        Yii::$app->session->set('event.backUrl', Yii::$app->request->url);

        return $this->render("index", ["model" => $model, "dataProvider" => $dataProvider]);
    }

    public function actionView($id)
    {
        $model = EventSearch::findSingleEvent($id);

        if (!isset($model))
        {
            throw new NotFoundHttpException(Yii::t('app', 'You are not allowed to access this page.Please do not repeat this request.'));
        }

        $goBackUrl = Yii::$app->session->get('event.backUrl', '');

        if (empty($goBackUrl))
        {
            $goBackUrl = Yii::$app->urlManager->createUrl(["//event/index"]);
        }

        return $this->render("view", ["model" => $model, 'goBackUrl' => $goBackUrl]);
    }

    /**
     * Ajax post signup
     */
    public function actionSignup()
    {
        if (Yii::$app->user->isGuest)
        {
            throw new BadRequestHttpException("An error occured. Please do not repeat this request.");
        }

        $request = Yii::$app->request;

        $event = Event::find()
                ->andWhere(["event.id" => (int) $request->post('event_id')])
                ->andWhere(["event.status_id" => Yii::$app->status->active])
                ->one();

        if (!isset($event))
        {
            throw new NotFoundHttpException(Yii::t('app', 'You are not allowed to access this page.Please do not repeat this request.'));
        }

        if ($request->isAjax && $request->isPost)
        {
            $eventId = (int) $request->post('event_id');
            $price = $request->post('price');

            $model = Register::findOne([
                        "register.user_id" => Yii::$app->user->id,
                        "register.event_id" => $eventId]
            );

            if (!is_object($model))
            {
                $model = new Register();
                $model->user_id = Yii::$app->user->id;
                $model->event_id = $eventId;
                $model->price = $price;
                $model->save();
                Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'You are signed up for event'));
            }
        }

        Yii::$app->end();
    }

    /**
     * Ajax post withdraw
     */
    public function actionWithdraw()
    {
        if (Yii::$app->user->isGuest)
        {
            throw new BadRequestHttpException("An error occured. Please do not repeat this request.");
        }

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost)
        {
            $eventId = (int) $request->post('event_id');

            $model = Register::find()
                    ->joinWith(['event'])
                    ->andWhere(["register.event_id" => $eventId])
                    ->andWhere(["register.user_id" => Yii::$app->user->id])
                    ->one();

            if (is_object($model) && is_object($model->event))
            {
                if (!$model->event->getHostessHasNewRequests() && !$model->event->getHostessHasAcceptedRequests())
                {
                    $model->delete();
                    Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'You are signed off from event'));
                }
            }
        }

        Yii::$app->end();
    }

    /**
     * Retruns JSON encoded event start and end dates
     * -added to return dress codes
     * @param int $event_id
     * @return JSON
     */
    public function actionGetEventDetailsForBooking()
    {

        if (Yii::$app->user->isGuest)
        {
            throw new BadRequestHttpException(\Yii::t('app', 'An error occured. Please do not repeat this request.'));
        }

        $request = Yii::$app->request;
        if ($request->isAjax)
        {
            $eventId = (int) $request->get('event_id');

            $model = Event::find()
                    ->with(["eventDressCodes", "eventJobs"])
                    ->andWhere(["event.id" => $eventId])
                    ->one();

            if (isset($model))
            {
                $eventJobs = [];
                foreach ($model->eventJobs as $job)
                {
                    $eventJobs[] = $job['job_id'];
                }

                $eventDressCodes = [];
                foreach ($model->eventDressCodes as $code)
                {
                    $eventDressCodes[] = $code['event_dress_code_id'];
                }
                
                $startDate = $model->start_date;
                $today = date("Y-m-d");
                
                if($startDate < $today)
                {
                    $startDate = $today;
                }

                echo \yii\helpers\Json::encode([
                    "start_date" => date('d-m-Y', strtotime($startDate)),
                    "end_date" => date('d-m-Y', strtotime($model->end_date)),
                    "event_jobs" => $eventJobs,
                    "event_dress_codes" => $eventDressCodes,
                ]);
            }

            Yii::$app->end();
        }
    }

    public function actionSignupGuest()
    {
        Yii::$app->session->setFlash('event.signup', Yii::t('app', 'Only registered and logged in users with completed profile can signup for events.'));
        return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(["hostess-profile/register"]));
    }

    public function actionSignupUnapproved()
    {

        Yii::$app->session->setFlash('event.signup', Yii::t('app', 'Only registered and logged in users with completed profile can signup for events.'));
        return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(["hostess-profile"]));
    }

}
