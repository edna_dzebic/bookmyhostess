<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontController;
use frontend\components\Cart;
use yii\filters\AccessControl;
use common\models\Email;
use common\models\Invoice;
use frontend\models\Request;
use frontend\models\User;
use frontend\models\ExhibitorFavorites;
use frontend\models\ExhibitorRegister;
use frontend\models\ExhibitorProfile;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\helpers\Json;
use frontend\models\HostessReview;

class ExhibitorController extends FrontController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['register', 'thank-you', 'activate'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                            'profile',
                            'company',
                            'account',
                            'change-password',
                            'event-request',
                            'delete-me',
                            'favorites',
                            'invoice',
                            'delete-favorite',
                            'add-favorite',
                            'book',
                            'add-booking',
                            'remove-booking',
                            'change-email',
                            'withdraw-request',
                            'bookings',
                            'reviews',
                            'create-review'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionRegister($refhos = null, $ev = null, $cp = null)
    {

        $model = new ExhibitorRegister();
        if ($model->load(Yii::$app->request->post()) && $model->signup())
        {
            //send registration email
            $lang = $model->lang;
            $showLangMessage = false;
            if (isset($lang) && !in_array($lang, array_keys(Yii::$app->params["languages"])))
            {
                $showLangMessage = true;
            }

            Email::sendExibitorRegistrationCreatedEmail($model, $showLangMessage, $refhos, $ev, $cp);

            //redirect to thank you page
            return $this->redirect(['thank-you']);
        }

        return $this->render("register", ["model" => $model]);
    }

    /**
     * Return only thank you page
     * @return mixed
     */
    public function actionThankYou($lang = null)
    {
        $this->layout = "simple_bg";
        return $this->render("thankyou");
    }

    public function actionActivate($id, $ref = null, $event = null, $cp = null)
    {
        $model = User::find()
                ->andWhere([
                    "user.email_validation_secret" => $id,
                    "user.role" => User::ROLE_EXHIBITOR
                ])
                ->one();

        if (is_object($model))
        {
            $model->status_id = Yii::$app->status->active;
            $model->activated_at = date("Y-m-d H:i:s");
            ExhibitorProfile::createBaseProfile($model->id);
            $model->update(false);
        }

        $this->redirect(\Yii::$app->urlManager->createUrl(["//site/login",
                    "ref" => $ref,
                    "ev" => $event,
                    "cp" => $cp
        ]));
    }

    /**
     * Outputs exhibitor profile
     * @return type
     */
    public function actionProfile()
    {
        $model = $this->getProfileModel();

        if (Yii::$app->request->isPost)
        {
            if ($model->user->load(Yii::$app->request->post()) && $model->user->validate())
            {
                $model->user->save();
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate())
            {
                $model->saveValuesToUser();
                $model->save();
            }

            return $this->refresh();
        }

        $model->setValuesFromUser();

        return $this->render("profile", ["model" => $model, "view" => "profile", "data" => []]);
    }

    /**
     * Outputs exhibitor profile
     * @return type
     */
    public function actionCompany($ref = null, $edit = null)
    {
        $model = $this->getProfileModel();

        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate())
            {
                $model->save();
            }

            if (isset($ref))
            {
                return $this->redirect([$ref]);
            }

            return $this->refresh();
        }

        return $this->render("profile", [
                    "model" => $model,
                    "view" => "company",
                    "data" => ["edit" => $edit]
        ]);
    }

    /**
     * Outputs exhibitor profile
     * @return type
     */
    public function actionAccount()
    {
        $model = $this->getProfileModel();

        //saving editables
        if (Yii::$app->request->post('hasEditable') == 1)
        {
            $model->lang = Yii::$app->request->post('lang');

            $model->saveValuesToUser();

            return Json::encode(['output' => Yii::t('app.exhibitor', 'Change language'), 'message' => '']);
        }

        $model->setValuesFromUser();

        return $this->render("profile", ["model" => $model, "view" => "account", "data" => []]);
    }

    public function actionChangePassword()
    {
        $model = User::find()
                ->andWhere(["user.id" => \Yii::$app->user->id])
                ->one();
       
        if (!is_object($model))
        {
            throw new BadRequestHttpException(\Yii::t('app.exhibitor', 'An error occured. Please do not repeat this request.'));
        }

        $model->setScenario('change-password');

        if (\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if (isset(\Yii::$app->request->post()['User']['password']))
        {
            $model->password = \Yii::$app->request->post()['User']['password'];
            $model->confirm_password = \Yii::$app->request->post()['User']['confirm_password'];
            if ($model->validate())
            {
                $model->setPassword(\Yii::$app->request->post()['User']['password']);
                $model->generateAuthKey();
                $model->save();
                return $this->redirect(\Yii::$app->urlManager->createUrl(["//exhibitor/profile"]));
            }
        }

        return $this->render("change_password", ["model" => $model]);
    }

    public function actionEventRequest()
    {
        $model = $this->getProfileModel();
        $model->last_profile_view = date('Y-m-d H:i:s');
        $model->update(false);

        $query = Request::find()
                ->joinWith(["user", "event"])
                ->andWhere(["request.requested_user_id" => Yii::$app->user->id])
                ->andWhere(
                        ['<>', 'request.status_id', Yii::$app->status->request_expired]
                )
                ->andWhere(
                        ['<>', 'request.status_id', Yii::$app->status->request_expired_hostess]
                )
                ->andWhere("request.completed is null or request.completed = 0")
                ->andWhere("DATE(event.end_date)>= DATE(NOW())")
                ->orderBy('request.id desc');

        return $this->render('event_request', [
                    "model" => $model,
                    "cart" => Cart::getInstance(),
                    'dataProvider' => new \yii\data\ActiveDataProvider([
                        'query' => $query,
                        'pagination' => ['pageSize' => 10],
                            ]),
        ]);
    }

    /**
     * Action for profile delete function
     */
    public function actionDeleteMe()
    {
        $model = User::find()
                ->andWhere(["id" => \Yii::$app->user->id])
                ->one();

        if (!is_object($model))
        {
            throw new BadRequestHttpException(\Yii::t('app.exhibitor', 'An error occured. Please do not repeat this request.'));
        }

        $model->status_id = Yii::$app->status->deleted;
        $model->save();
        \Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionFavorites()
    {
        $model = $this->getProfileModel();

        return $this->render('favorites', [
                    "model" => $model,
                    'dataProvider' => ExhibitorFavorites::getDataProvider(),
        ]);
    }

    /**
     *
     * Show list of paid/unpaid invoices
     *
     */
    public function actionInvoice()
    {
        // only to check if user is logged in and exhibitor
        $model = $this->getProfileModel();

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Invoice::find()
                    ->joinWith([
                        'userRequested'
                    ])
                    ->andWhere([
                        "user.id" => $model->user_id
                    ])
                    ->orderBy('invoice.id desc'),
            'pagination' => ['pageSize' => 10],
        ]);

        return $this->render('invoice', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteFavorite($id)
    {
        $profileModel = $this->getProfileModel();

        $model = ExhibitorFavorites::find()
                ->andWhere(["exhibitor_favorites.id" => $id])
                ->andWhere(["exhibitor_favorites.user_id" => $profileModel->user_id])
                ->one();

        if (!is_object($model))
        {
            throw new BadRequestHttpException(\Yii::t('app.exhibitor', 'An error occured. Please do not repeat this request.'));
        }

        $model->delete();
        $this->redirect(\Yii::$app->urlManager->createUrl(["//exhibitor/favorites"]));
    }

    /**
     * Adds hostess to exhibitor favorite table
     * @param int $id
     */
    public function actionAddFavorite($id)
    {
        if (Yii::$app->user->isGuest || Yii::$app->user->identity->getIsExhibitor() === false)
        {
            throw new BadRequestHttpException(\Yii::t('app.exhibitor', 'An error occured. Please do not repeat this request.'));
        }

        $model = new ExhibitorFavorites();
        $model->user_id = \Yii::$app->user->id;
        $model->favorite_user_id = (int) $id;
        $model->save();

        $this->redirect(\Yii::$app->urlManager->createUrl(["//exhibitor/favorites"]));
    }


    /**
     * @return string
     */
    public function actionBook()
    {
        $profileModel = $this->getProfileModel();

        $models = Cart::getInstance()->getAllModels();

        if (count($models) > 0)
        {
            if ($profileModel->isProfileCompleted() === false)
            {
                return $this->redirect("billing-details");
            }

            return $this->redirect(["checkout/index"]);
        }

        $this->redirect(\Yii::$app->urlManager->createUrl(["//exhibitor/event-request"]));
    }

    /**
     * Add event request with $id to "cart"
     * @param $id
     *
     * @return string
     * @throws HttpException
     */
    public function actionAddBooking($id)
    {
        $cart = Cart::getInstance();

        if (!$cart->add($id))
        {
            throw new HttpException(400, $cart->errorMessage);
        }

        // TODO: proper success report
        return json_encode(["cartCount" => $cart->getItemCount()]);
    }

    /**
     * Remove event request with $id from cart
     *
     * @param $id
     *
     * @return string
     */
    public function actionRemoveBooking($id)
    {
        $cart = Cart::getInstance();

        $cart->remove($id);

        // TODO: proper error/success report
        return json_encode(["cartCount" => $cart->getItemCount()]);
    }

    public function actionChangeEmail()
    {
        $profileModel = $this->getProfileModel();

        $oldEmail = $profileModel->user->email;

        $data = [
            'status_code' => 0,
            'view' => ''
        ];

        if (Yii::$app->request->isPost)
        {
            if ($profileModel->changeEmail(Yii::$app->request->post(), $oldEmail))
            {
                Yii::$app->session->setFlash('success', Yii::t('app.exhibitor', 'You have successfully changed Your email!'), true);
                $data["status_code"] = 1;
            }
        }

        unset($profileModel->user->email);
        $data["view"] = $this->renderPartial("profile-tabs/_change_email_form", [
            "model" => $profileModel->user,
            "oldEmail" => $oldEmail
        ]);

        return Json::encode($data);
    }

    public function actionWithdrawRequest($id)
    {
        $profileModel = $this->getProfileModel();

        $model = Request::find()
                ->andWhere(["request.id" => $id])
                ->andWhere(['request.requested_user_id' => $profileModel->user_id])
                ->one();

        if (!is_object($model))
        {
            throw new BadRequestHttpException(\Yii::t('app.exhibitor', 'An error occured. Please do not repeat this request.'));
        }

        $data = [
            'status_code' => 0,
            'view' => ''
        ];

        if (Yii::$app->request->isPost)
        {
            if ($model->exhibitorWithdraw(Yii::$app->request->post()))
            {
                Yii::$app->session->setFlash('success', Yii::t('app.exhibitor', 'You have withdrawn request!'), true);
                $data["status_code"] = 1;
            }
        }

        $data["view"] = $this->renderPartial("_withdraw_request_form", ["model" => $model]);

        return Json::encode($data);
    }

    public function actionBookings()
    {
        $model = $this->getProfileModel();

        return $this->render('bookings', [
                    "model" => $model,
                    'dataProvider' => Request::getExhibitorBookingsDataProvider(),
        ]);
    }

    public function actionReviews()
    {
        $model = $this->getProfileModel();

        return $this->render('reviews', [
                    "model" => $model,
                    "dataProvider" => \frontend\models\HostessReview::getExhibitorDataProvider()
        ]);
    }

    public function actionCreateReview($id)
    {
        $model = $this->getProfileModel();

        // get event request
        $eventRequest = Request::findOne([
                    "id" => $id,
                    "requested_user_id" => $model->user_id
        ]);

        if (!$eventRequest)
        {
            throw new NotFoundHttpException(Yii::t('app.exhibitor', 'Event request not found.'));
        }

        $reviewModel = HostessReview::find()->where(["event_request_id" => $id])->one();

        if (!isset($reviewModel))
        {
            // generate new group code
            $groupCode = strtotime("now") . rand(1000, 9999);

            // create new hostess review
            $model = new HostessReview([
                'event_request_id' => $eventRequest->id,
                'hostess_id' => $eventRequest->user_id,
                'exhibitor_id' => $eventRequest->requested_user_id,
                'event_id' => $eventRequest->event_id,
                'group_code' => $groupCode,
                'review_requested' => 1,
                'status_id' => Yii::$app->status->pending
            ]);

            if ($model->save())
            {
                return $this->redirect(['/review', "code" => $model->group_code]);
            } else
            {
                Yii::$app->session->setFlash('error', Yii::t('app.exhibitor', 'An error occured. Please try again later.'), true);
            }
        } else
        {
            if ($reviewModel->getIsPending())
            {
                return $this->redirect(['/review', "code" => $reviewModel->group_code]);
            } else
            {
                Yii::$app->session->setFlash('error', Yii::t('app.exhibitor', 'The review already exists.'), true);
            }
        }


        return $this->redirect(['bookings']);
    }

    private function getProfileModel()
    {

        if (Yii::$app->user->isGuest || Yii::$app->user->identity->getIsExhibitor() === false)
        {
            throw new BadRequestHttpException(\Yii::t('app.exhibitor', 'An error occured. Please do not repeat this request.'));
        }

        $model = ExhibitorProfile::find()
                ->joinWith(["user"])
                ->andWhere(["user.id" => \Yii::$app->user->id])
                ->one();

        if (!is_object($model))
        {
            throw new BadRequestHttpException(\Yii::t('app.exhibitor', 'An error occured. Please do not repeat this request.'));
        }


        return $model;
    }

}
