<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Cart;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;
use common\models\User;
use common\models\Email;
use frontend\components\FrontController;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends FrontController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Display start page
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = "full";

        $contactModel = new ContactForm();

        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->getIsHostess())
        {
            return $this->redirect(["hostess"]);
        }

        if ($contactModel->load(Yii::$app->request->post()) && $contactModel->validate())
        {
            if ($contactModel->sendEmail())
            {
                Yii::$app->session->setFlash('contact.success', Yii::t('app.exhibitor', 'Thank you for contacting us. We will respond to you as soon as possible.'));
                return $this->redirect(["/site/index/#contact"]);
            } else
            {
                Yii::$app->session->setFlash('contact.error', Yii::t('app.exhibitor', 'There was an error sending email.'));
            }
        }

        return $this->render('exhibitor/index', ["contactModel" => $contactModel]);
    }

    /**
     * Display hostess start page
     * @return mixed
     */
    public function actionHostess()
    {
        $this->layout = "hostess";

        $contactModel = new ContactForm();

        if ($contactModel->load(Yii::$app->request->post()) && $contactModel->validate())
        {
            if ($contactModel->sendEmail())
            {
                Yii::$app->session->setFlash('contact.success', Yii::t('app.hostess', 'Thank you for contacting us. We will respond to you as soon as possible.'));
                return $this->redirect(["/site/hostess/#contact"]);
            } else
            {
                Yii::$app->session->setFlash('contact.error', Yii::t('app.hostess', 'There was an error sending email.'));
            }
        }

        return $this->render('hostess/index', ["contactModel" => $contactModel]);
    }

    /**
     * Login
     * @param string $ref
     * @param string $return
     * @param string $ev
     * @param string $cp 
     * @return mixed
     */
    public function actionLogin($ref = null, $return = '', $ev = null, $cp = null)
    {
        if (!Yii::$app->user->isGuest)
        {
            $this->setLanguage();
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->requiresNewPassword())
            {
                return $this->handleNewPasswordRequired($model);
            }

            if ($model->login())
            {
                $this->setLanguage();

                if (Yii::$app->user->identity->getIsAdmin())
                {
                    return $this->redirect(Yii::$app->urlManager->createUrl(["//site/index"]));
                }

                if (Yii::$app->user->identity->getIsHostess())
                {
                    return $this->loginHostess($model);
                }

                if (Yii::$app->user->identity->getIsExhibitor())
                {
                    return $this->loginExhibitor($ref, $return, $ev, $cp);
                }
            }
        }

        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout
     * @return mixed
     */
    public function actionLogout()
    {
        $lng = Yii::$app->language;

        Yii::$app->user->logout();

        Yii::$app->session->set('lang', $lng);
        Yii::$app->language = Yii::$app->session->get('lang');

        return $this->goHome();
    }

    /**
     * Display form to request password reset
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $user = User::findOne([
                        'status_id' => Yii::$app->status->active,
                        'email' => $model->email,
            ]);

            $code = sha1(microtime(true) . $user->username);
            $user->email_validation_secret = $code;
            $user->save(false);

            Email::sendRequestPasswordResetEmail($user);

            return $this->redirect(["thank-you", 'id' => $user->id]);
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Return only thank you page
     * @return type
     */
    public function actionThankYou($id)
    {
        $this->layout = "simple_bg";

        $user = User::findOne([
                    'id' => $id,
        ]);

        return $this->render("thankyou", ["email" => $user->email, 'user' => $user]);
    }

    /**
     * Display form to input new passwords. Token comes from link in email
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try
        {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e)
        {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword())
        {
            return $this->redirect(Yii::$app->urlManager->createUrl(["//site/login"]));
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    /**
     * Display registration page
     * @return mixed
     */
    public function actionRegister()
    {
        $this->layout = "simple_bg";

        return $this->render('register');
    }

    /**
     * Action to change language.
     * The FrontController will do the job
     * @return type
     */
    public function actionSetLng()
    {
        $responseCookies = Yii::$app->response->cookies;

        $lngCookieAcceptedExists = in_array(self::LNG_COOKIE_ACCEPTED, array_keys($_COOKIE));
        $lngCookieAccepted = false;

        if ($lngCookieAcceptedExists)
        {
            $lngCookieAccepted = $_COOKIE[self::LNG_COOKIE_ACCEPTED] == true;
        }
       
        if ($lngCookieAccepted)
        {
            $responseCookies->add(new \yii\web\Cookie([
                'name' => self::LNG_COOKIE_NAME,
                'value' => Yii::$app->language,
                'expire' => strtotime('+3 months')
            ]));
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Set language based on logged in user
     */
    private function setLanguage()
    {
        $lang = Yii::$app->user->identity->lang;

        if (empty($lang))
        {
            $lang = 'de';
        }

        Yii::$app->session->set('lang', Yii::$app->user->identity->lang);
        Yii::$app->language = Yii::$app->session->get('lang');
    }

    /**
     * Handles "change password required" scenario for old users
     * @param LoginForm $model
     * @return mixed
     */
    private function handleNewPasswordRequired(LoginForm $model)
    {
        $code = sha1(microtime(true) . $model->username);
        $model->setNewEmailValidationSecret($code);

        Email::sendRequestPasswordResetEmail($model->getUserModel());

        return $this->redirect(["thank-you", 'id' => $model->getUserID()]);
    }

    /**
     * Handles login for hostesses
     * @param LoginForm $model
     * @return type
     */
    private function loginHostess(LoginForm $model)
    {
        $userModel = $model->getUserModel();
        $profileModel = $userModel->hostessProfile;

        if (!$profileModel->isAddressComplete())
        {
            return $this->redirect(["hostess-profile/billing-details"]);
        }

        if ($profileModel->admin_approved == \common\models\HostessProfile::APPROVAL_APPROVED)
        {
            if ($profileModel->getNotAnsweredRequestCount() > 0)
            {
                return $this->redirect(Yii::$app->urlManager->createUrl(["//hostess-profile/requests"]));
            }
            return $this->redirect(Yii::$app->urlManager->createUrl(["//site/index"]));
        }

        return $this->redirect(Yii::$app->urlManager->createUrl(["//hostess-profile/index"]));
    }

    /**
     * Handle login for exhibitor
     * @return mixed
     */
    private function loginExhibitor($ref, $return, $ev, $cp = null)
    {
        if (isset($cp))
        {
            $this->setCoupon($cp);
        }

        if (isset($ref))
        {
            return $this->redirect(Yii::$app->urlManager->createUrl(["//hostess/view", 'id' => $ref, "event" => $ev]));
        }

        if (!empty($return))
        {
            $return = urldecode($return);
            return $this->redirect(Yii::$app->urlManager->createUrl([$return]));
        }

        return $this->redirect(Yii::$app->urlManager->createUrl(["//site/index"]));
    }

    private function setCoupon($couponCode)
    {
        $cartInstance = Cart::getInstance();
        $cartInstance->setCoupon($couponCode);
    }

}
