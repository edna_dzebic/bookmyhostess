<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontController;
use common\models\EventRegister;
use common\models\EventRequest;
use common\models\EventRequestJob;
use kartik\mpdf\Pdf;
use common\models\Invoice;

/**
 * Test controller
 */
class TestController extends FrontController
{

    public function actionJasmin()
    {
        $registers = EventRegister::find()
                ->where(["user_id" => 3961])
                ->all();


        $rand = rand(0, count($registers) - 1);

        $eventRegisterModel = $registers[$rand];

        $eventRequestModel = new EventRequest();

        $this->saveRequest($eventRequestModel, $eventRegisterModel);

        return die(\yii\helpers\VarDumper::dump($eventRequestModel->id, 10, true));
    }

    /**
     * Creates PDF invoice and returns its file name
     * @param type $invoiceId
     * @return string
     */
    public function actionCreate($invoiceId)
    {

        define('_MPDF_TTFONTDATAPATH', Yii::getAlias('@runtime/mpdf'));

        $model = Invoice::find()->andWhere(["invoice.id" => $invoiceId])->one();

        Yii::$app->language = $model->userRequested->lang;


        $content = $this->renderPartial('//invoice/pdf', ["model" => $model]);
        $fileName = "invoice_" . $model->id;
        $pdf = new Pdf([
            'marginTop' => 9,
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_DOWNLOAD,
            'content' => $content,
            'filename' => $fileName,
            'cssFile' => 'css/invoice_pdf.css',
            'cssInline' => file_get_contents(Yii::getAlias('@frontend/assets/css/invoice_pdf.css')),
            'options' => ['title' => \Yii::$app->name],
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    private function saveRequest(&$model, $eventRegisterModel)
    {

        $model->price = $eventRegisterModel->price;
        $model->user_id = 3961;
        $model->event_id = $eventRegisterModel->event_id;
        $model->requested_user_id = 3521;
        $model->date_created = date('Y-m-d H:i:s');

        $model->date_from = date('Y-m-d H:i:s', strtotime($eventRegisterModel->event->start_date));
        $model->date_to = date('Y-m-d H:i:s', strtotime($eventRegisterModel->event->end_date));


        $dressCodes = $eventRegisterModel->event->eventToDressCodes;
        $rand = rand(0, count($dressCodes) - 1);
        $dressCodeId = $dressCodes[$rand]->event_dress_code_id;
        $model->event_dress_code = $dressCodeId;

        $model->status = EventRequest::STATUS_NEW;

        $transaction = Yii::$app->db->beginTransaction();

        if ($model->save())
        {
            $event_jobs = $eventRegisterModel->event->eventToJobs;

            $rand = rand(0, count($event_jobs) - 1);

            $eventJobModel = new EventRequestJob();
            $eventJobModel->event_request_id = $model->id;
            $eventJobModel->event_job = $event_jobs[$rand]->job_id;

            if (!$eventJobModel->save())
            {
                $transaction->rollBack();
                return false;
            }

            $model->sendFirebaseNotification();


            $transaction->commit();
            return true;
        }

        $transaction->rollBack();
        return false;
    }

}
