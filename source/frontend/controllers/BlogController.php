<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontController;
use frontend\models\Blog;

class BlogController extends FrontController
{

    public function actionIndex()
    {
        $blogs = Blog::getAll();
        return $this->render("index", ["blogs" => $blogs]);
    }

    public function actionView($id)
    {
        $model = Blog::getModel($id);

        if (is_object($model))
        {
            return $this->render("view", ["model" => $model]);
        }

        throw new \yii\web\NotFoundHttpException();
    }

}
