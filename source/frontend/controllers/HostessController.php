<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Cart;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use kartik\mpdf\Pdf;
use yii\helpers\Json;
use frontend\components\FrontController;
use frontend\models\Request;
use frontend\models\Job;
use frontend\models\Register;
use frontend\models\DressCode;
use frontend\models\HostessProfile;
use frontend\models\HostessRequestForm;
use frontend\models\EventRegisterSearch;
use frontend\models\Event;

class HostessController extends FrontController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Search parameters, used when listing events
     * @var array
     */
    public $searchData = [];

    public function actionIndex()
    {
        $model = new EventRegisterSearch();
        $searchProvider = $model->getSearchProvider(Yii::$app->request->get());

        $this->setCoupon();

        $this->searchData = [
            'country' => $model->country,
            'city' => $model->city,
            'event' => $model->event,
        ];

        Yii::$app->session->set('hostess.backUrl', Yii::$app->request->url);
        Yii::$app->session->set('hostess.filter', Yii::$app->request->get());

        // If no results and logged as exhibitor, redirect to hostess request form
        if ($searchProvider->getTotalCount() == 0 &&
                !Yii::$app->user->isGuest &&
                Yii::$app->user->identity->isExhibitor)
        {
            return $this->redirect(['hostess/request-form']);
        }

        return $this->render("index", [
                    "searchProvider" => $searchProvider,
                    "searchModel" => $model
        ]);
    }

    /**
     * Shows form when there are no results for hostess/index search
     */
    public function actionRequestForm()
    {
        // Is not logged in as exhibitor
        if (Yii::$app->user->isGuest || !Yii::$app->user->identity->isExhibitor)
        {
            return $this->redirect(['hostess/index']);
        }

        // If there are no filters set, redirect to hostess index
        if (!isset(Yii::$app->session->get('hostess.filter')['EventRegisterSearch']))
        {
            return $this->redirect(['hostess/index']);
        }

        // If filter results are greater than zero, redirect back to search
        $model = new EventRegisterSearch();
        $searchProvider = $model->getSearchProvider(Yii::$app->session->get('hostess.filter'));
        if ($searchProvider->getTotalCount() > 0)
        {
            return $this->redirect(['hostess/index']);
        }

        $model = new HostessRequestForm();
        $model->filter = Yii::$app->session->get('hostess.filter')['EventRegisterSearch'];
        $model->user = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post()) && $model->sendEmail())
        {
            Yii::$app->session->remove('hostess.backUrl');
            Yii::$app->session->remove('hostess.filter');
            Yii::$app->session->setFlash('success', Yii::t('app.exhibitor', 'You have successfully sent a hostess search request.'), true);
            return $this->redirect(['hostess/index']);
        }

        return $this->render("request_form", ["model" => $model]);
    }

    /**
     * Action used for link from emails ( to load hostesses for specific event )
     * @param integer $id
     * @return mixed
     */
    public function actionForEvent($id = null)
    {
        $model = new EventRegisterSearch();

        $model->event = $id;

        $searchProvider = $model->getSearchProvider(Yii::$app->request->get());

        $this->searchData = [
            'country' => $model->country,
            'city' => $model->city,
            'event' => $model->event,
        ];

        $filter = [
            'EventRegisterSearch' => [
                'country' => $model->country,
                'city' => $model->city,
                'event' => $id,
            ]
        ];

        Yii::$app->session->set('hostess.backUrl', Yii::$app->request->url);
        Yii::$app->session->set('hostess.filter', $filter);

        return $this->render("index", [
                    "searchProvider" => $searchProvider,
                    "searchModel" => $model
        ]);
    }

    /**
     * Shows hostess profile page
     * @param int $id
     * @return type
     * @throws \yii\web\NotFoundHttpException
     * @throws BadRequestHttpException
     */
    public function actionView($id, $event = null)
    {
        /* @var $model HostessProfile */
        $model = $this->findModel($id);

        $get = Yii::$app->request->get();

        $couponCode = $this->setCoupon();

        if (isset($get["EventRegisterSearch"]) && isset($get["EventRegisterSearch"]["event"]))
        {
            $event = $get["EventRegisterSearch"]["event"];
        }

        if (!is_object($model) && isset($event))
        {
            return $this->tryRedirectAfterNotFound($id, $event, $couponCode);
        }

        if (Yii::$app->user->isGuest)
        {
            return $this->render("view_nr", [
                        "model" => $model,
                        "event" => $event,
                        "cp" => $couponCode
            ]);
        }

        if (Yii::$app->user->identity->isHostess && ($model->user_id != Yii::$app->user->id))
        {
            throw new NotFoundHttpException(Yii::t('app.exhibitor', 'No access to the profiles of other users. Please do not repeat this request.'));
        }

        $startDate = '';
        $endDate = '';
        $eventReg = null;
        $eventModel = null;

        if (!empty($event))
        {
            $eventModel = Event::findOne(["id" => $event]);

            if (!is_object($eventModel))
            {
                throw new NotFoundHttpException(Yii::t('app.exhibitor', 'Event not found'));
            }
        }

        if (is_object($eventModel))
        {
            $eventReg = Register::findOne(["user_id" => $id, "event_id" => $event]);

            if (is_object($eventReg))
            {
                $eventStartDate = $eventModel->start_date;
                $eventEndDate = $eventModel->end_date;

                $startDate = Yii::$app->session->get("booking.start_date", $eventReg->event->start_date);
                $endDate = Yii::$app->session->get("booking.end_date", $eventReg->event->end_date);

                if ($eventStartDate > $startDate)
                {
                    $startDate = $eventStartDate;
                }
                if ($endDate > $eventEndDate)
                {
                    $endDate = $eventEndDate;
                }
                if ($endDate < $startDate)
                {
                    $startDate = $eventStartDate;
                    $endDate = $eventEndDate;
                }

                $today = date("Y-m-d");
                if ($startDate < $today)
                {
                    $startDate = $today;
                }
            } else
            {
                return $this->tryRedirectAfterNotFound($id, $event);
            }
        }

        list($eventAvailability, $availabilityData) = Register::getAllForUser($id);

        $goBackUrl = Yii::$app->session->get('hostess.backUrl', Yii::$app->urlManager->createUrl(["//hostess/index"]));

        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->getIsExhibitor())
        {
            $model->profile_views = $model->profile_views + 1;
            $model->update(false);
        }

        $eventRegisterSearchModel = new EventRegisterSearch();
        $nextModel = $eventRegisterSearchModel->getNextModel($id);
        $prevModel = $eventRegisterSearchModel->getPrevModel($id);
        $notificationData = Request::getNotificationDataForExhibitor($event, $id, Yii::$app->user->id);

        $isLastMinute = false;
        $lastMinutePriceAddition = 0;
        if (is_object($eventModel))
        {
            $isLastMinute = Yii::$app->util->isRequestLastMinute($eventModel);

            if (is_object($eventReg))
            {
                $lastMinutePriceAddition = Yii::$app->util->getLastMinutePriceAddition($eventReg->price);
            }
        }

        return $this->render("view_r", [
                    "model" => $model,
                    "eventJobs" => Job::getAllTranslated(),
                    "eventDressCodes" => DressCode::getAllTranslated(),
                    'eventAvailability' => $eventAvailability,
                    'availabilityData' => $availabilityData,
                    'goBackUrl' => $goBackUrl,
                    'pastWork' => $model->getPastWorkProvider(),
                    "event" => $event,
                    "eventReg" => $eventReg,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'dressCode' => Yii::$app->session->get('booking.dress_code', ''),
                    'selectedJobs' => Yii::$app->session->get("booking.jobs", []),
                    'selectedPaymentType' => Yii::$app->session->get("booking.payment_type", Yii::$app->util->businessLicence),
                    'notificationData' => $notificationData,
                    "prev" => $prevModel,
                    "next" => $nextModel,
                    "reviews" => \frontend\models\HostessReview::getDataProviderForUser($id),
                    'isLastMinute' => $isLastMinute,
                    'lastMinutePriceAddition' => $lastMinutePriceAddition
        ]);
    }

    /**
     * Downloads hostess profile PDF
     * @param int $id
     * @throws \yii\web\NotFoundHttpException
     * @throws BadRequestHttpException
     */
    public function actionPdf($id, $event = null)
    {
        if (Yii::$app->user->isGuest)
        {
            throw new ForbiddenHttpException(Yii::t('app.exhibitor', 'No access to this page.'));
        }

        $model = HostessProfile::find()
                ->joinWith(["user"])
                ->andWhere([
                    "user.id" => $id,
                    "user.status_id" => Yii::$app->status->active
                ])
                ->one();

        if (!is_object($model))
        {
            throw new NotFoundHttpException(Yii::t('app.exhibitor', 'Hostess profile does not exists . Please do not repeat this request.'));
        }

        if (($model->user_id != Yii::$app->user->id) && Yii::$app->user->identity->isHostess)
        {
            throw new NotFoundHttpException(Yii::t('app.exhibitor', 'No access to the profiles of other users. Please do not repeat this request.'));
        }

        $this->layout = "pdf";

        list($eventAvailability, $availabilityData) = Register::getAllForHostess($id);

        $eventReg = null;
        $eventModel = null;
        $isLastMinute = false;

        if (isset($event))
        {
            $eventModel = Event::findOne(["id" => $event]);

            if (!is_object($eventModel))
            {
                throw new NotFoundHttpException(Yii::t('app.exhibitor', 'Event not found'));
            }

            $isLastMinute = Yii::$app->util->isRequestLastMinute($eventModel);
            $eventReg = Register::findOne(["user_id" => $id, "event_id" => $event]);
        }

        $content = $this->renderPartial("pdf", [
            "model" => $model,
            'eventAvailability' => $eventAvailability,
            'eventReg' => $eventReg,
            'isLastMinute' => $isLastMinute
        ]);

        $pdf = new Pdf([
            'filename' => Yii::t('app.exhibitor', 'User') . ' - ' . $model->user->username . ".pdf",
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::FORMAT_LETTER,
            // stream to browser inline
            'destination' => Pdf::DEST_DOWNLOAD,
            // your html content input
            'content' => $content,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'cssInline' => file_get_contents(Yii::getAlias('@frontend/assets/css/pdf_profile.css')),
            'options' => ['title' => Yii::t('app.exhibitor', 'User') . ' - ' . $model->user->username . " | " . Yii::$app->name],
            'marginTop' => 30,
            'marginBottom' => 30,
            'methods' => [
                'SetHTMLFooter' => ['<div class="foot-div green-text"><div class="pull-left">{PAGENO}</div><div class="pull-right">&copy;&nbsp;www.BOOKmyHOSTESS.com</div></div>']
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Book a hostess
     * -add record to db
     * -send notifications
     * -show thank you page
     *
     */
    public function actionBook()
    {
        $hostesId = Yii::$app->request->get("hostess_id");
        $eventId = Yii::$app->request->get("event");

        $eventRegisterModel = Register::findOne([
                    "event_id" => $eventId,
                    "user_id" => $hostesId
        ]);

        $eventModel = Event::findOne(["id" => $eventId]);

        if (!is_object($eventModel))
        {
            throw new NotFoundHttpException(Yii::t('app.exhibitor', 'Event not found'));
        }

        if (is_object($eventRegisterModel) === false)
        {
            Yii::$app->session->setFlash('error', Yii::t('app.exhibitor', 'User is not available for this event.'));
            return $this->redirect(["view", "id" => $hostesId, "event" => $eventId]);
        }

        $alreadyBooked = Request::hostessAlreadybooked($eventId, $hostesId);

        if ($alreadyBooked === true)
        {
            Yii::$app->session->setFlash('error.booking', Yii::t('app.exhibitor', 'User already booked for this event.'));
            return $this->redirect(["view", "id" => $hostesId, "event" => $eventId]);
        }

        $canBeCreated = Request::canBeCreated($eventId, $hostesId, Yii::$app->user->id);

        if ($canBeCreated === false)
        {
            Yii::$app->session->setFlash('error.booking', Yii::t('app.exhibitor', 'Request already sent to this person.'));
            return $this->redirect(["view", "id" => $hostesId, "event" => $eventId]);
        }

        $oldAppLang = Yii::$app->language;

        $affl = null;
        if (Yii::$app->session->has('affl'))
        {
            $affl = Yii::$app->session->get('affl');
        }

        $formModel = new Request();
        if ($formModel->createRequest($eventRegisterModel, $affl))
        {
            Yii::$app->language = $oldAppLang;

            Yii::$app->session->set("booking.jobs", Yii::$app->request->get("event_job"));
            Yii::$app->session->set("booking.dress_code", $formModel->event_dress_code);
            Yii::$app->session->set("booking.start_date", $formModel->date_from);
            Yii::$app->session->set("booking.end_date", $formModel->date_to);
            Yii::$app->session->set("booking.payment_type", $formModel->payment_type);

            $eventRegSearchModel = new EventRegisterSearch();
            $nextModel = $eventRegSearchModel->getNextAfterBooking($hostesId, $eventId);

            Yii::$app->session->setFlash('success.booking', Yii::t('app.exhibitor', 'Your request has successfully been send. You can continue to send non binding requests.'));

            $goBackUrl = Yii::$app->session->get('hostess.backUrl', Yii::$app->urlManager->createUrl(["//hostess/index"]));
            if (is_object($nextModel))
            {
                $goBackUrl = Yii::$app->urlManager->createUrl(["//hostess/view", "id" => $nextModel->user->id, "event" => $eventId]);
            }

            return $this->redirect($goBackUrl);
        }

        Yii::$app->session->setFlash('error.booking', Yii::t('app.exhibitor', 'Sorry, an error occured. Please try again later.'));
        return $this->redirect(["view", "id" => $hostesId, "event" => $eventId]);
    }

    //FILTER AJAX
    //used on hostess search and event search
    /**
     * Get all cities for selected country as json
     */
    public function actionGetCitiesJson()
    {
        $request = Yii::$app->request;
        if ($request->isAjax)
        {
            $countryId = $request->get('country_id');

            $cities = EventRegisterSearch::getCityListJson($countryId);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $cities = Json::encode($cities);
            return $cities;
        }
    }

    /**
     * Get all cities for selected city as json
     */
    public function actionGetEventsJson()
    {
        $request = Yii::$app->request;
        if ($request->isAjax)
        {
            $cityId = $request->get('city_id');

            $events = EventRegisterSearch::getEventListjson($cityId);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $events = Json::encode($events);
            return $events;
        }
    }

    public function actionNotFound()
    {
        return $this->render("not_found");
    }

    private function tryRedirectAfterNotFound($id, $event, $couponCode = null)
    {
        $evRegisterModel = new EventRegisterSearch();
        $redirectModel = $evRegisterModel->getNextAfterNotFound($id, $event);

        if (is_object($redirectModel))
        {
            return $this->redirect(["view",
                        "id" => $redirectModel->user->id,
                        "event" => $event,
                        "cp" => $couponCode
            ]);
        }

        throw new NotFoundHttpException(Yii::t('app.exhibitor', 'Hostess profile does not exists . Please do not repeat this request.'));
    }

    private function findModel($id)
    {
        return HostessProfile::find()
                        ->joinWith(["user"])
                        ->andWhere([
                            "user.id" => $id,
                            "user.status_id" => Yii::$app->status->active
                        ])
                        ->one();
    }

    private function setCoupon()
    {
        $code = Yii::$app->request->get('cp', null);

        $cartInstance = Cart::getInstance();

        if (!isset($code))
        {
            $code = $cartInstance->getCouponCode();
        }

        if (isset($code))
        {
            $cartInstance->setCoupon($code);
        }

        return $code;
    }

}
