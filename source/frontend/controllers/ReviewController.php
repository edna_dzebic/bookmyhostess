<?php

namespace frontend\controllers;

use Yii;
use frontend\components\FrontController;
use frontend\models\HostessReview;

class ReviewController extends FrontController
{

    public function actionIndex($code)
    {
        if (Yii::$app->user->isGuest)
        {
            $return = "review/index?code=" . $code;
            $return = urlencode($return);
            return $this->redirect(["site/login", 'return' => $return]);
        }

        if (Yii::$app->user->identity->getIsExhibitor() === false)
        {
            throw new \yii\web\ForbiddenHttpException(Yii::t('app.exhibitor', 'You are not allowed to access this page!'));
        }

        $models = $this->getModels($code);

        $post = Yii::$app->request->post();
        if (Yii::$app->request->isPost)
        {
            // used to see if validation is successful
            $valid = true;
            foreach ($models as $model)
            {
                // get review for hostess
                $review = $post['Review'][$model->id];

                // Set values for model
                $model->scenario = 'update';
                $model->rating = $review['rating'];
                $model->comment = $review['comment'];

                // Validate model
                $valid &= $model->validate();
            }

            // if valid, save models, set success flash and redirect to exhibitor profile/reviews
            if ($valid)
            {
                foreach ($models as $model)
                {
                    $model->status_id = Yii::$app->status->active;
                    if ($model->save() !== false)
                    {
                        $model->hostess->hostessProfile->recalculateRating();
                    }
                }
                Yii::$app->session->setFlash('success', Yii::t('app.exhibitor', 'Thank you for reviews.'), true);
                return $this->redirect(['exhibitor/reviews']);
            } else
            {
                // If not valid, set error message
                Yii::$app->session->setFlash('error', Yii::t('app.exhibitor', 'Please enter comments for all profiles shorter than 255 characters.'), true);
            }
        }

        return $this->render("index", ["models" => $models, "post" => $post]);
    }

    private function getModels($code)
    {
        $models = HostessReview::find()
                ->andWhere([
                    "group_code" => $code,
                    "exhibitor_id" => Yii::$app->user->id,
                    "status_id" => Yii::$app->status->pending
                ])
                ->all();

        if (count($models) != 0)
        {
            return $models;
        }

        throw new \yii\web\NotFoundHttpException(Yii::t('app.exhibitor', 'Reviews not found!'));
    }

}
