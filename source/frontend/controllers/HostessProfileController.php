<?php

namespace frontend\controllers;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use frontend\components\FrontController;
use common\models\UserMessages;
use common\models\HostessLanguage;
use common\models\Email;
use common\models\HostessPastWork;
use frontend\models\HostessProfile;
use frontend\models\UserDocuments;
use frontend\models\UserImages;
use frontend\models\TradeLicenseExtend;
use frontend\models\City;
use frontend\models\User;
use frontend\models\Job;
use frontend\models\Register;
use frontend\models\Request;
use frontend\models\HostessRegister;
use frontend\models\HostessBillingDetails;
use frontend\models\HostessReview;

class HostessProfileController extends FrontController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['register', 'thank-you', 'activate'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [
                            'index',
                            'language',
                            'education',
                            'work-experience',
                            'preferred',
                            'account',
                            'billing-details',
                            'accept-admin-message',
                            'change-password',
                            'requests',
                            'requests',
                            'delete-language',
                            'delete-work',
                            'edit-work',
                            'add-work',
                            'delete-me',
                            'change-email',
                            'set-image-as-default',
                            'accept-request',
                            'reject-request',
                            'approve-me',
                            'trade-shows',
                            'trade-show-remove',
                            'current-booking',
                            'past-booking',
                            'delete-image',
                            'delete-document',
                            'extend-trade-license',
                            'reviews',
                            'withdraw-confirmation',
                            'ask-review'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Outputs hostess profile
     * @return type
     */
    public function actionIndex()
    {
        $model = $this->getProfileModel();

        if (!$model->isAddressComplete())
        {
            return $this->redirect(["billing-details"]);
        }

        list($userImagesModel, $uploadedImg) = $this->saveHostessProfileImages();

        list($userDocumentsModel, $uploadedDocs) = $this->saveHostessDocuments($model);

        if ($uploadedDocs || $uploadedImg)
        {
            return $this->refresh();
        }

        //saving profile
        if ($model->load(Yii::$app->request->post()))
        {

            if ($model->validate())
            {
                $model->birth_date = date("Y-m-d H:i:s", strtotime($model->birth_date));
            }

            // do not run validation because of education part
            // approve will take care of validation for itself so no harm is done
            $model->save(false);

            $model->recalculateProfileCompleteness();

            return $this->redirect(["index"]);
        }

        return $this->render("profile", [
                    "model" => $model,
                    "userImagesModel" => $userImagesModel,
                    "userDocumentsModel" => $userDocumentsModel,
                    "view" => "_profile",
                    "data" => [
                        'userImagesModel' => $userImagesModel,
                        "userDocumentsModel" => $userDocumentsModel,
                    ]
        ]);
    }

    public function actionRegister()
    {
        $model = new HostessRegister();
        if ($model->load(Yii::$app->request->post()) && $model->signup())
        {

            $lang = $model->lang;
            $showLangMessage = false;
            if (isset($lang) && !in_array($lang, array_keys(Yii::$app->params["languages"])))
            {
                $showLangMessage = true;
            }

            Email::sendHostessRegistrationCreatedEmail($model, $showLangMessage);

            return $this->redirect(['thank-you']);
        }

        return $this->render("register", ["model" => $model]);
    }

    /**
     * Used on hostess profile
     * Accepts admin messages
     *
     * @return boolean
     */
    public function actionAcceptAdminMessage()
    {
        $profileModel = $this->getProfileModel();
        UserMessages::updateAll(['accepted' => 1], ["to_user_id" => $profileModel->user_id, 'accepted' => 0]);
        return $this->redirect(["index"]);
    }

    public function actionChangePassword()
    {

        $profileModel = $this->getProfileModel();

        $model = User::find()->andWhere(["id" => $profileModel->user_id])->one();

        if ($model === null || $model === false)
        {
            throw new BadRequestHttpException(Yii::t('app.hostess', 'An error occured. Please do not repeat this request.'));
        }

        $model->setScenario('change-password');

        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if (isset(Yii::$app->request->post()['User']['password']))
        {
            $model->password = Yii::$app->request->post()['User']['password'];
            $model->confirm_password = Yii::$app->request->post()['User']['confirm_password'];
            if ($model->validate())
            {
                $model->setPassword(Yii::$app->request->post()['User']['password']);
                $model->generateAuthKey();
                $model->save();
                $this->redirect(["index"]);
            }
        }

        return $this->render("change_password", ["model" => $model, "profileModel" => $profileModel]);
    }

    public function actionRequests()
    {
        $profileModel = $this->getProfileModel();

        return $this->render('requests', [
                    "profileModel" => $profileModel,
                    'dataProvider' => Request::getHostessRequestsDataProvider(),
        ]);
    }

    /**
     * Return only thank you page
     * @return type
     */
    public function actionThankYou()
    {
        $this->layout = "simple_bg";

        return $this->render("thankyou");
    }

    public function actionDeleteLanguage($id)
    {
        $profileModel = $this->getProfileModel();

        $model = HostessLanguage::find()
                ->andWhere(["user_id" => $profileModel->user_id, "id" => $id])
                ->one();

        if (isset($model))
        {
            $model->delete();
            $profileModel->recalculateProfileCompleteness();
            return $this->redirect(["language"]);
        }
        throw new NotFoundHttpException(Yii::t('app.hostess', 'An error occured. Please do not repeat this request again.'));
    }

    public function actionDeleteWork($id)
    {
        $profileModel = $this->getProfileModel();

        $model = HostessPastWork::find()
                ->andWhere(["user_id" => $profileModel->user_id, "id" => $id])
                ->one();

        if (isset($model))
        {
            $model->delete();
            $profileModel->recalculateProfileCompleteness();
            return $this->redirect(["work-experience"]);
        }
        throw new NotFoundHttpException(Yii::t('app.hostess', 'An error occured. Please do not repeat this request again.'));
    }

    public function actionEditWork($id)
    {
        $profileModel = $this->getProfileModel();

        $model = HostessPastWork::find()
                ->andWhere(["user_id" => $profileModel->user_id, "id" => $id])
                ->one();

        $data = [
            'status_code' => 0,
            'view' => ''
        ];

        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->save())
            {
                $data["status_code"] = 1;
            }
        }

        $data["view"] = $this->renderPartial("profile_parts/_edit_work_form", ["model" => $model]);

        return Json::encode($data);
    }

    public function actionAddWork()
    {
        $profileModel = $this->getProfileModel();

        $model = new HostessPastWork();
        $model->user_id = Yii::$app->user->id;

        $data = [
            'status_code' => 0,
            'view' => ''
        ];

        if (Yii::$app->request->isPost)
        {
            if (isset($model) && $model->load(Yii::$app->request->post()) && $model->save())
            {
                $profileModel->recalculateProfileCompleteness();
                $data["status_code"] = 1;
            }
        }

        $data["view"] = $this->renderPartial("profile_parts/_add_work_form", ["model" => $model]);

        return Json::encode($data);
    }

    public function actionDeleteMe()
    {
        $profileModel = $this->getProfileModel();

        $model = User::find()
                ->andWhere(["id" => $profileModel->user_id])
                ->one();

        $model->status_id = Yii::$app->status->deleted;
        $model->save();
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Sets current user choosen image as default one for their profile pages
     * @param type $id
     * @return type
     */
    public function actionSetImageAsDefault($id)
    {
        $profileModel = $this->getProfileModel();

        $models = UserImages::find()
                ->andWhere([
                    "user_id" => $profileModel->user_id
                ])
                ->all();

        foreach ($models as $model)
        {
            if ($model->id == (int) $id)
            {
                $model->is_default = 1;
            } else
            {
                $model->is_default = 0;
            }

            $model->save(false);
        }
        Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'The change of the profile picture has been saved successfully.'), true);

        return $this->redirect(["index"]);
    }

    /**
     * Hostess accepting request sent from exhibitor
     *
     * @param int $id
     */
    public function actionAcceptRequest($id)
    {
        $profileModel = $this->getProfileModel();

        $model = Request::find()
                ->andWhere(["request.id" => $id])
                ->one();

        if (is_object($model))
        {
            $model->status_id = Yii::$app->status->request_accept;
            $model->date_responded = date('Y-m-d H:i:s');

            $model->update(false);

            Email::sendAcceptRequestEmailToExibitor($model);

            $profileModel->recalculateProfileCompleteness();
        } else
        {
            Yii::error('Failed to update model under hostess accept request');
            throw new NotFoundHttpException(Yii::t('app.hostess', 'The specified model cannot be found.'));
        }
        return $this->redirect(["requests"]);
    }

    public function actionActivate($id)
    {
        $model = User::find()
                ->andWhere([
                    "email_validation_secret" => $id,
                    "role" => User::ROLE_HOSTESS
                ])
                ->one();

        if (isset($model->id))
        {
            $model->status_id = Yii::$app->status->active;
            $model->activated_at = date("Y-m-d H:i:s");
            HostessProfile::createBaseProfile($model->id);
            $model->update(false);
        }
        return $this->redirect(["/site/login"]);
    }

    public function actionTradeShows()
    {
        $profileModel = $this->getProfileModel();

        return $this->render('trade_shows', [
                    "profileModel" => $profileModel,
                    'dataProvider' => Register::getForHostess(),
        ]);
    }

    public function actionTradeShowRemove($id)
    {
        $profileModel = $this->getProfileModel();

        $model = Register::find()
                        ->joinWith(['event'])
                        ->andWhere([
                            "id" => $id,
                            "user_id" => $profileModel->user_id
                        ])->one();

        if (isset($model) && isset($model->event))
        {
            if (!$model->event->hostessHasNewRequests && !$model->event->hostessHasAcceptedRequests)
            {
                $model->delete();
            }
        }

        return $this->redirect(["trade-shows"]);
    }

    public function actionCurrentBooking()
    {
        $profileModel = $this->getProfileModel();

        return $this->render('current_booking', [
                    "profileModel" => $profileModel,
                    'dataProvider' => Request::getCurrentBookingForHostess(),
        ]);
    }

    public function actionPastBooking()
    {
        $profileModel = $this->getProfileModel();

        return $this->render('past_booking', [
                    "profileModel" => $profileModel,
                    'dataProvider' => Request::getPastBookingForHostess(),
        ]);
    }

    public function actionDeleteImage($id)
    {
        $profileModel = $this->getProfileModel();
        $userImage = UserImages::find()
                ->andWhere([
                    "id" => $id,
                    "user_id" => $profileModel->user_id
                ])
                ->one();

        if (isset($userImage))
        {
            if ($userImage->delete())
            {
                Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'Selected file has been successfully deleted.'), true);
                $profileModel->recalculateProfileCompleteness();
            } else
            {
                Yii::$app->session->setFlash('error', Yii::t('app.hostess', 'Selected file cannot be deleted.'), true);
            }
        }

        return $this->redirect(["index"]);
    }

    public function actionDeleteDocument($id)
    {
        $profileModel = $this->getProfileModel();
        $model = UserDocuments::find()
                ->andWhere(["id" => $id, "user_id" => $profileModel->user_id])
                ->one();

        if (is_object($model))
        {
            if ($model->delete() !== false)
            {
                Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'Selected file has been successfully deleted.'), true);
            } else
            {
                Yii::$app->session->setFlash('error', Yii::t('app.hostess', 'Selected file cannot be deleted.'), true);
            }
        }

        return $this->redirect(["index"]);
    }

    private function saveHostessProfileImages()
    {
        $profileModel = $this->getProfileModel();
        $model = new UserImages();
        $model->scenario = 'upload';

        $uploaded = false;
        if (Yii::$app->request->isPost && isset(Yii::$app->request->post()['UserImages']))
        {
            $model->image_url = UploadedFile::getInstance($model, 'image_url');
            if ($model->upload())
            {
                Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'Selected file has been successfully uploaded.'), true);

                $uploaded = true;

                $profileModel->recalculateProfileCompleteness();
            }
        }

        return [$model, $uploaded];
    }

    private function saveHostessDocuments($profileModel)
    {
        $model = new UserDocuments();
        $model->scenario = 'upload';
        $uploaded = false;
        if (Yii::$app->request->isPost && isset(Yii::$app->request->post()['UserDocuments']))
        {
            $model->filename = UploadedFile::getInstance($model, 'filename');
            $model->real_filename = $model->filename;
            if ($model->upload())
            {
                $profileModel->has_trade_licence = 1;
                $profileModel->update(false);
                $uploaded = true;
                Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'Selected file has been successfully uploaded.'), true);
            }
        }

        return [$model, $uploaded];
    }

    public function actionApproveMe()
    {
        $model = $this->getProfileModel();

        //if wants to approve profile
        if ($model->isProfileCompleted() && $model->isApproved() === false)
        {
            $model->admin_approved = \common\models\HostessProfile::APPROVAL_REQUESTED;

            $model->save(false);
        }

        return $this->redirect("index");
    }

    /**
     * Outputs hostess profile
     * @return type
     */
    public function actionLanguage()
    {
        $model = $this->getProfileModel();

        $hostessToLanguageModel = new HostessLanguage();

        //saving editables
        if (Yii::$app->request->post('hasEditable') == 1)
        {
            //saving languages
            if (Yii::$app->request->post('level') != "" && Yii::$app->request->post('language_id') != "")
            {

                $nLangModel = HostessLanguage::findOne(["user_id" => Yii::$app->user->id, "language_id" => Yii::$app->request->post('language_id')]);

                if (!isset($nLangModel))
                {
                    $nLangModel = new HostessLanguage();
                    $nLangModel->user_id = Yii::$app->user->id;
                    $nLangModel->level_id = Yii::$app->request->post('level');
                    $nLangModel->language_id = Yii::$app->request->post('language_id');
                    if ($nLangModel->save())
                    {
                        echo Json::encode(['output' => 'saved', 'message' => '']);
                        $model->recalculateProfileCompleteness();
                        return;
                    }
                } else
                {
                    echo Json::encode(['output' => 'not-saved', 'message' => Yii::t('app.hostess', 'Please choose another language. Chosen language already exists in your list.')]);
                    return;
                }
            }
        }

        $languageDataProvider = new ActiveDataProvider([
            'query' => HostessLanguage::find()
                    ->with(["level"])
                    ->andWhere(["user_id" => Yii::$app->user->id])
                    ->orderBy('id desc'),
            'pagination' => ['pageSize' => 5],
        ]);

        return $this->render("profile", [
                    "model" => $model,
                    "view" => "_language",
                    "data" => [
                        "hostessToLanguageModel" => $hostessToLanguageModel,
                        "languageDataProvider" => $languageDataProvider,
                    ]
        ]);
    }

    /**
     * Outputs hostess profile
     * @return type
     */
    public function actionEducation()
    {
        $model = $this->getProfileModel();

        //saving profile
        if ($model->load(Yii::$app->request->post()))
        {
            // do not run validation because of education part
            // approve will take care of validation for itself so no harm is done
            $model->save(false);

            Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'Data saved!'), true);

            $model->recalculateProfileCompleteness();

            return $this->refresh();
        }

        return $this->render("profile", [
                    "model" => $model,
                    "view" => "_education",
                    "data" => []
        ]);
    }

    /**
     * Outputs hostess profile
     * @return type
     */
    public function actionWorkExperience()
    {
        $model = $this->getProfileModel();
        //saving editables
        if (Yii::$app->request->post('hasEditable') == 1)
        {
            $wxModel = new HostessPastWork();
            $wxModel->user_id = Yii::$app->user->id;
            if ($wxModel->load(Yii::$app->request->post()) && $wxModel->validate())
            {
                //convert dates just in case 
                $wxModel->start_date = date("Y-m-d H:i:s", strtotime($wxModel->start_date));
                $wxModel->end_date = date("Y-m-d H:i:s", strtotime($wxModel->end_date));

                if ($wxModel->save())
                {
                    echo Json::encode(['output' => 'saved', 'message' => '']);
                    $model->recalculateProfileCompleteness();
                    return;
                }
            }

            echo Json::encode(['output' => 'not-saved', 'message' => Yii::t('app.hostess', 'Validation error.  Please check all input fields.')]);
            return;
        }

        $workingExperienceProvider = new ActiveDataProvider([
            'query' => HostessPastWork::find()
                    ->andWhere(["user_id" => Yii::$app->user->id])
                    ->orderBy(["start_date" => SORT_DESC]),
            'pagination' => ['pageSize' => 5],
        ]);

        $workExpModel = new HostessPastWork();

        return $this->render("profile", [
                    "model" => $model,
                    "view" => "_experience",
                    "data" => [
                        "workingExperienceProvider" => $workingExperienceProvider,
                        "workExpModel" => $workExpModel,
                    ]
        ]);
    }

    /**
     * Outputs hostess profile
     * @return type
     */
    public function actionPreferred()
    {
        $model = $this->getProfileModel();

        if (Yii::$app->request->isPost)
        {
            $model->savePreferredJobs(Yii::$app->request->post("HostessPreferredJob"));
            $model->savePreferredCities(Yii::$app->request->post("HostessPreferredCity"));
            return $this->refresh();
        }

        $eventJobs = Job::getAllTranslated();
        $cities = City::getAllTranslated();

        return $this->render("profile", [
                    "model" => $model,
                    "view" => "_preferred",
                    "data" => [
                        "eventJobs" => $eventJobs,
                        "cities" => $cities
                    ]
        ]);
    }

    public function actionReviews()
    {
        $profileModel = $this->getProfileModel();

        return $this->render('reviews', [
                    "profileModel" => $profileModel,
                    'dataProvider' => \frontend\models\HostessReview::getHostessDataProvider(),
        ]);
    }

    /**
     * Outputs hostess profile
     * @return type
     */
    public function actionAccount()
    {
        $model = $this->getProfileModel();

        //saving editables
        if (Yii::$app->request->post('hasEditable') == 1)
        {
            $model->lang = Yii::$app->request->post('lang');

            $model->saveValuesToUser();

            return Json::encode(['output' => Yii::t('app.hostess', 'Change language'), 'message' => '']);
        }

        $model->setValuesFromUser();

        return $this->render("profile", [
                    "model" => $model,
                    "view" => "_account",
                    "data" => []
        ]);
    }

    public function actionRejectRequest($id)
    {
        $profileModel = $this->getProfileModel();

        $model = Request::find()
                ->andWhere(["id" => $id, "user_id" => $profileModel->user_id])
                ->one();

        $data = [
            'status_code' => 0,
            'view' => ''
        ];

        if (Yii::$app->request->isPost)
        {
            if ($model->reject(Yii::$app->request->post()))
            {
                Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'You have rejected request!'), true);
                $data["status_code"] = 1;

                $profileModel->recalculateProfileCompleteness();
            }
        }

        $data["view"] = $this->renderPartial("_reject_request_form", ["model" => $model]);

        return Json::encode($data);
    }

    public function actionWithdrawConfirmation($id)
    {
        $profileModel = $this->getProfileModel();

        $model = Request::find()
                ->andWhere(["id" => $id, "user_id" => $profileModel->user_id])
                ->one();

        $data = [
            'status_code' => 0,
            'view' => ''
        ];

        if (Yii::$app->request->isPost)
        {
            if ($model->hostessWithdrawConfirmation(Yii::$app->request->post()))
            {
                Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'You have withdrawn confirmation!'), true);
                $data["status_code"] = 1;

                $profileModel->recalculateProfileCompleteness();
            }
        }

        $data["view"] = $this->renderPartial("_withdraw_confirmation_form", ["model" => $model]);

        return Json::encode($data);
    }

    public function actionChangeEmail()
    {
        // just to make sure user is logged in and hostess
        $profileModel = $this->getProfileModel();

        $oldEmail = $profileModel->user->email;

        $data = [
            'status_code' => 0,
            'view' => ''
        ];

        if (Yii::$app->request->isPost)
        {
            if ($profileModel->changeEmail(Yii::$app->request->post(), $oldEmail))
            {
                Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'You have successfully changed your email!'), true);
                $data["status_code"] = 1;
            }
        }

        unset($profileModel->user->email);
        $data["view"] = $this->renderPartial("profile_parts/_change_email_form", ["model" => $profileModel->user, "oldEmail" => $oldEmail]);

        return Json::encode($data);
    }

    public function actionExtendTradeLicense()
    {
        $profileModel = $this->getProfileModel();

        if ($profileModel->getCanPostponeTradeLicense() === false)
        {
            throw new ForbiddenHttpException(Yii::t('app.hostess', 'You cannot request postpone of trade license!'));
        }

        $model = new TradeLicenseExtend();
        $model->hostess_profile_id = $profileModel->id;

        $data = [
            'status_code' => 0
        ];

        if (Yii::$app->request->isPost)
        {
            if ($model->sendRequest(Yii::$app->request->post()))
            {
                $data["status_code"] = 1;
            }
        }

        return Json::encode($data);
    }

    public function actionBillingDetails()
    {
        $profileModel = $this->getProfileModel();

        if ($profileModel->isAddressComplete())
        {
            return $this->redirect(["index"]);
        }

        $model = new HostessBillingDetails(["model" => $profileModel]);

        if (Yii::$app->request->isPost && $model->saveDetails($profileModel, Yii::$app->request->post()))
        {
            $profileModel->recalculateProfileCompleteness();
            return $this->redirect(["index"]);
        }


        return $this->render("billing_details", ["model" => $model]);
    }

    public function actionAskReview($id)
    {
        $profileModel = $this->getProfileModel();

        // get event request
        $eventRequest = Request::findOne([
                    "id" => $id,
                    "user_id" => $profileModel->user_id
        ]);

        if (!is_object($eventRequest))
        {
            throw new NotFoundHttpException(Yii::t('app.hostess', 'Event request not found.'));
        }

        $reviewModel = HostessReview::find()
                ->andWhere(["event_request_id" => $id])
                ->one();

        $data = [
            'status_code' => 0,
            'view' => '',
            'message' => ''
        ];

        if (isset($reviewModel))
        {
            $data["message"] = Yii::t('app.hostess', "Review already sent");
        } else
        {
            if (Yii::$app->request->isPost)
            {
                // generate new group code
                $groupCode = strtotime("now") . rand(1000, 9999);

                // create new hostess review
                $model = new HostessReview([
                    'event_request_id' => $eventRequest->id,
                    'hostess_id' => $eventRequest->user_id,
                    'exhibitor_id' => $eventRequest->requested_user_id,
                    'event_id' => $eventRequest->event_id,
                    'group_code' => $groupCode,
                    'review_requested' => 1,
                    'status_id' => Yii::$app->status->pending
                ]);


                $transaction = Yii::$app->db->beginTransaction();
                if ($model->save())
                {
                    $oldLng = Yii::$app->language;
                    if ($model->sendRequestEmailToExhibitor())
                    {
                        $transaction->commit();
                        Yii::$app->language = $oldLng;
                        Yii::$app->session->setFlash('success', Yii::t('app.hostess', 'You have successfully sent review request!'), true);
                        $data["status_code"] = 1;
                    }
                } else
                {
                    $data["message"] = Yii::t('app.hostess', 'An error occured. Unable to send review!');
                    $transaction->rollBack();
                }
            }
        }

        $data["view"] = $this->renderPartial("_review_form", ["model" => $eventRequest]);

        return Json::encode($data);
    }

    private function getProfileModel()
    {
        if (Yii::$app->user->isGuest || Yii::$app->user->identity->getIsHostess() === false)
        {
            throw new BadRequestHttpException(\Yii::t('app.hostess', 'An error occured. Please do not repeat this request.'));
        }

        $model = HostessProfile::find()
                ->joinWith(["user"])
                ->andWhere(["user.id" => Yii::$app->user->id])
                ->one();

        if (!is_object($model))
        {
            throw new BadRequestHttpException(Yii::t('app.hostess', 'An error occured while loading hostess profile. Please do not repeat this request.'));
        }

        return $model;
    }

}
