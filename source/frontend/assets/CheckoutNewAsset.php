<?php

namespace frontend\assets;

class CheckoutNewAsset extends BaseAssetBundle
{

    protected $scripts = [        
    ];
    protected $styles = [
        'css/checkout-new'
    ];
    
    public function init()
    {
        parent::init();
    }

}
