<?php

namespace frontend\assets;

class EventRequestAsset extends BaseAssetBundle
{

    protected $scripts = [
        'js/analytics/events/ProceedToCheckoutEvent',
        'js/analytics/listeners/ProceedToCheckoutEventListener',
        'js/event-request-cart',
        'js/exhibitor-profile'
    ];
  
    public function init()
    {
        parent::init();
    }

}
