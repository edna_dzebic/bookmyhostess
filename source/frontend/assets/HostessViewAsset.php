<?php

namespace frontend\assets;

class HostessViewAsset extends BaseAssetBundle
{

    public $js = [
        'js/plugins.min.js',
        'js/sly.min.js'
    ];
    protected $styles = [
        'css/hostess-view',
    ];
    protected $scripts = [
        'js/hostess-view'
    ];
    protected $additionalDepends = [
        'frontend\assets\FancyBoxAsset'
    ];

    public function init()
    {
        parent::init();
    }

}
