<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class HostessProfileAsset extends BaseAssetBundle
{

    public $js = [
        'js/plugins.min.js',
        'js/sly.min.js'
    ];
    protected $styles = [
        'css/hostess-profile',
    ];
    protected $scripts = [
        'js/hostess-profile'
    ];
    protected $additionalDepends = [
        'frontend\assets\FancyBoxAsset'
    ];

    public function init()
    {
        parent::init();
    }

}
