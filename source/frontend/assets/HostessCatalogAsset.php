<?php

namespace frontend\assets;

class HostessCatalogAsset extends BaseAssetBundle
{

    protected $styles = [
        'css/hostess-catalog'
    ];
    protected $scripts = [
        'js/hostess-icon'
    ];
    protected $additionalDepends = [
        'frontend\assets\FancyBoxAsset'
    ];

    public function init()
    {
        parent::init();
    }

}
