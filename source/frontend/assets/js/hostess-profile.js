"use strict";

var App = App || {};

$(document).ready(function () {

    App.HostessProfile.init();
});

App.HostessProfile = {
    modalImage: "#edit-images-modal",
    modalProfileCompleteness: "#profile-completeness-modal",
    modalProfileReliability: "#profile-reliability-modal",
    modalApprove: "#approve-modal",
    slyInitialized: false,
    sly: null,
    init: function () {
        var self = this;
        $(".js-edit-gallery").click(function () {
            self.editImages();
        });

        $(".js-profile-completeness-info-icon").click(function () {
            self.profileCompleteness();
        });
        
        $(".js-profile-reliability-info-icon").click(function () {
            self.profileReliability();
        });
        
         $(".js-disabled-approve-now").click(function () {
            self.profileApproval();
        });

        $('.js-delete-language').on('click', self.deleteLanguage);
        $('.js-delete-work').on('click', self.deleteWork);
    },
    profileReliability: function () {
        $(this.modalProfileReliability).modal();
    },
    profileCompleteness: function () {
        $(this.modalProfileCompleteness).modal();
    },
    profileApproval: function () {
        $(this.modalApprove).modal();
    },
    editImages: function () {
        var self = this;

        $(this.modalImage).modal();

        self.setSly();

        $(this.modalImage).on('hidden.bs.modal', function (e) {
            self.sly.destroy();
        });
        $(this.modalImage).on('shown.bs.modal', function (e) {
            self.sly.init();
        });
    },
    setSly: function ()
    {
        var self = this;

        if ($('#profile-images').length == 0)
        {
            return;
        }

        var $wrap = $('#profile-images');

        var $frame = $('#centered', $wrap);

        self.sly = new Sly($frame, {
            horizontal: 1,
            itemNav: 'centered',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 1,
            scrollBy: 1,
            speed: 300,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1
        });

        $(".js-next", $wrap).on("click", function ()
        {
            self.sly.next();
        });

        $(".js-prev", $wrap).on("click", function ()
        {
            self.sly.prev();
        });
    },
    deleteLanguage: function () {
        App.log('Delete language');
        var $el = $(this);
        var url = $el.data('url');
        var id = $el.data('id');
        var name = $el.data('name');
        App.log(url, id);

        var $modal = $("#delete-lng-modal");

        $modal.on('hidden.bs.modal', function (e) {
            $(".js-lng-name", $modal).html("");
        });

        $modal.modal();
        $(".js-lng-name", $modal).html(name);
        $(".js-btn-delete-lng", $modal).unbind("click").on("click", function () {
            $.get(url, {id: id}, function () {
                App.log('Delete successful');
                App.Helpers.reload();
            });
        });
    },
    deleteWork: function () {
        App.log('Delete language');
        var $el = $(this);
        var url = $el.data('url');
        var id = $el.data('id');
        var name = $el.data('name');
        App.log(url, id);

        var $modal = $("#delete-work-modal");

        $modal.on('hidden.bs.modal', function (e) {
            $(".js-work-name", $modal).html("");
        });

        $modal.modal();
        $(".js-work-name", $modal).html(name);
        $(".js-btn-delete-work", $modal).unbind("click").on("click", function () {
            $.get(url, {id: id}, function () {
                App.log('Delete successful');
                App.Helpers.reload();
            });
        });
    }
};
