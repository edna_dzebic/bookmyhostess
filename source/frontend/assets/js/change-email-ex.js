$(window).load(function () {
    bindChangeEmail();
});


function changeEmailBtnSubmit($modal)
{
    $(".js-btn-change-email", $modal).unbind("click").on("click", function (e)
    {
        $(".loading-content", $modal).show();

        var $btn = $(".js-btn-change-email", $modal);

        $btn.attr("disabled", true);

        $("#change-email-form").hide();

        e.preventDefault();
        $.ajax({
            url: "/exhibitor/change-email",
            data: $("#change-email-form").serialize(),
            method: 'POST',
            dataType: "json",
            success: function (data) {

                $(".loading-content", $modal).hide();
                $btn.attr("disabled", false);
                $("#change-email-form").show();

                var status = parseInt(data["status_code"]);
                if (status === 1)
                {
                    $modal.modal("hide");
                    $modal.find(".modal-body").html('');
                    location.reload();
                }
                else
                {
                    $modal.find(".modal-body").html(data["view"]);
                }
            }}
        );
        return false;
    });
}


/**
 * 
 */
function getChangeEmail()
{
    $.ajax({
        url: "/exhibitor/change-email",
        method: 'GET',
        dataType: "json",
        success: function (data) {
            var $modal = $("#change-email-modal");
            $modal.find(".modal-body").html(data["view"]);

            changeEmailBtnSubmit($modal);

            $modal.modal();
        }
    });
}

/**
 * Bind click event on change email link
 */
function bindChangeEmail()
{
    $(".js-change-email").unbind("click").on("click", function (e) {
        getChangeEmail();
        e.preventDefault();
        return false;
    });
}
