"use strict";

var App = App || {};

$(document).ready(function () {
    App.about.init();
    App.sly.init();
    App.video.init();
});

App.about = {
    init: function () {
        var windows_width = $(window).width();
        var width = 400;

        if (windows_width < 768)
        {
            width = 270;
        }

        if (windows_width < 360)
        {
            width = 220;
        }

        var settings = {
            trigger: 'click',
            container: $(".about-icons"),
            title: '',
            content: '',
            width: width,
            height: 'auto',
            multi: true,
            closeable: true,
            delay: 0,
            padding: true,
            backdrop: false
        };

        $(".js-popover-icon").on("click", function (e) {

            var $parent = $(this).parent();

            if ($($parent).hasClass("active") === false)
            {
                $(".about-item").removeClass("active");
                $($parent).addClass("active");

                var $target = $($parent).attr("data-text-content");
                var placement = $($parent).attr("data-placement");
                var title = $($parent).attr("data-title");

                var content = $("." + $target).html();


                var largeSettings = {
                    content: content,
                    title: title,
                    placement: placement
                };

                $('.webui-popover').hide();
                $($parent)
                        .webuiPopover('destroy')
                        .webuiPopover($.extend({}, settings, largeSettings))
                        .on("hidden.webui.popover", function () {
                            $(this).removeClass("active");
                        });
            }
        });
    }
};

App.sly = {
    init: function () {
        App.sly.initSpecific('_clients');
        App.sly.initSpecific('_partners');
    },
    initSpecific: function (id) {
        var $wrap = $('#'+id);
        var $frame = $('#centered'+id, $wrap);

        var sly = new Sly($frame, {
            horizontal: 1,
            itemNav: 'centered',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 1,
            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            speed: 300,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1
        }).init();


        $(".js-next", $wrap).on("click", function ()
        {
            sly.next();
        });
        $(".js-prev", $wrap).on("click", function ()
        {
            sly.prev();
        });
    }
};

App.video = {
    init: function ()
    {
        $(window).resize(function () {
            App.video.resize();
        });

        App.video.resize();
        plyr.setup();
    },
    resize: function () {
        if ($(window).width() >= 1000) {
            var $height = $(".js-gallery-items").height();

            $('video').css('min-height', $height - 1);
            $('video').css('max-height', $height - 1);
        }
        else {
            $('video').css('min-height', 'auto');
            $('video').css('max-height', 300);
        }
    }
};
