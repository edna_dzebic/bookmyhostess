"use strict";

/*
 *  jQuery OwlCarousel v1.3.3
 *
 *  Copyright (c) 2013 Bartosz Wojciechowski
 *  http://www.owlgraphic.com/owlcarousel/
 *
 *  Licensed under MIT
 *
 */

/*JS Lint helpers: */
/*global dragMove: false, dragEnd: false, $, jQuery, alert, window, document */
/*jslint nomen: true, continue:true */

if (typeof Object.create !== "function") {
    Object.create = function (obj) {
        function F() {
        }
        F.prototype = obj;
        return new F();
    };
}
(function ($, window, document) {

    $.fn.ownCarousel = function (options) {
        return this.each(function () {
            var base = this;
            var ownOptions = {};
            if (options.progressBar != false) {
                ownOptions = {
                    afterInit: progressBar,
                    afterMove: moved,
                    startDragging: pauseOnDragging
                };

            }

            base.$elem = $(this);
            base.options = $.extend({}, $.fn.owlCarousel.options, ownOptions, base.$elem.data(), options);

            base.$elem.owlCarousel(base.options);


            var time = 7; // time one slide is active

            var $progressBar; // progress bar container
            var $bar; // the actual bar that is moving
            var $elem; // the slider element
            var isPause; // if the bar is paused ( it pauses on mouseover )
            var tick; // interval
            var percentTime; // the bar progress in percent (used to change width, from 0 to 100% )


            //Init progressBar where elem is $("#owl-demo")
            function progressBar(elem) {
                $elem = elem;
                //build progress bar elements
                buildProgressBar();
                //start counting
                start();
            }

            //create div#progressBar and div#bar then appent to the outer wrapper
            function buildProgressBar() {
                $progressBar = $("<div>", {
                    id: "progressBar"
                });

                $bar = $("<div>", {
                    id: "bar"
                });

                $progressBar.append($bar).appendTo($elem.find(".owl-wrapper-outer"));
            }

            function start() {
                //reset timer
                percentTime = 0;
                isPause = false;
                //run interval every 0.01 second
                tick = setInterval(interval, 10);
            }
            ;

            function interval() {
                if (isPause === false) {
                    percentTime += 1 / time;
                    $bar.css({
                        width: percentTime + "%"
                    });
                    //if percentTime is equal or greater than 103 
                    //(it switches instantly on 100, and 105 is too slow)
                    if (percentTime >= 103) {
                        //slide to next item 
                        $elem.trigger('owl.next');
                    }
                }
            }

            function pauseOnDragging() {
                isPause = true;
            }

            //moved callback
            function moved() {
                //clear interval
                clearTimeout(tick);
                //start again
                start();
            }

            if (typeof ($elem) !== "undefined") {
                $elem.on('mouseover', function () {
                    isPause = true;
                });
                $elem.on('mouseout', function () {
                    isPause = false;
                });
            }


        });
    };

    $.fn.ownCarousel.options = {
        items: 5,
        itemsCustom: false,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 2],
        itemsTabletSmall: false,
        itemsMobile: [479, 1],
        //
        singleItem: false,
        itemsScaleUp: false,
        slideSpeed: 200,
        paginationSpeed: 800,
        rewindSpeed: 1000,
        //
        autoPlay: false,
        stopOnHover: false,
        navigation: false,
        navigationText: ["prev", "next"],
        rewindNav: true,
        scrollPerPage: false,
        pagination: true,
        paginationNumbers: false,
        //
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
        baseClass: "owl-carousel",
        theme: "owl-theme",
        //
        lazyLoad: false,
        lazyFollow: true,
        lazyEffect: "fade",
        //
        autoHeight: false,
        //
        jsonPath: false,
        jsonSuccess: false,
        //
        dragBeforeAnimFinish: true,
        //
        mouseDrag: true,
        touchDrag: true,
        //
        addClassActive: false,
        transitionStyle: false,
        //
        beforeUpdate: false,
        afterUpdate: false,
        beforeInit: false,
        afterInit: false,
        beforeMove: false,
        afterMove: false,
        afterAction: false,
        startDragging: false,
        afterLazyLoad: false
    };
}(jQuery, window, document));