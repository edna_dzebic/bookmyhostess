"use strict";

var Main = Main || {};

$(document).ready(function () {

    Main.ExhibitorProfile.init();
});

Main.ExhibitorProfile = {
    init: function () {
        var self = this;
        $(".js-withdraw-request").click(function (e) {
            var id = $(this).attr("data-id");
            self.withdrawRequest(id);
            e.preventDefault();
            return false;
        });        
    },
    withdrawRequest: function (id)
    {

        var self = this;
        $.ajax({
            url: "/exhibitor/withdraw-request",
            data: {id: id},
            method: 'GET',
            dataType: "json",
            success: function (data) {
                var $modal = $("#withdraw-request-modal");
                $modal.find(".modal-body").html(data["view"]);
                self.withdrawConfirmationBtnSubmit(id, $modal);
                $modal.modal();
            }
        });

    },
    withdrawConfirmationBtnSubmit: function (id, $modal) {
        var $btn = $(".js-btn-withdraw-request", $modal);

        $btn.unbind("click").on("click", function (e)
        {
            var $form = $("#withdraw-request-form", $modal);
            var $loading = $(".loading-content", $modal);
            $loading.show();
            $btn.attr("disabled", true);
            $form.hide();
            e.preventDefault();
            $.ajax({
                url: "/exhibitor/withdraw-request?id=" + id,
                data: $form.serialize(),
                method: 'POST',
                dataType: "json",
                success: function (data) {
                    $loading.hide();
                    $btn.attr("disabled", false);
                    $form.show();
                    var status = parseInt(data["status_code"]);
                    if (status === 1)
                    {
                        $modal.modal("hide");
                        $modal.find(".modal-body").html('');
                        location.reload();
                    }
                    else
                    {
                        $modal.find(".modal-body").html(data["view"]);
                    }
                }}
            );
            return false;
        });
    }
};
