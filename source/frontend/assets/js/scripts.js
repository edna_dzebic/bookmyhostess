"use strict";

var App = App || {};

$(document).ready(function () {
    App.googleAnalytics.init();
    App.zopim.init();
    App.hotjar.init();
});

App.googleAnalytics = {
    init: function () {

        var allowed = $("#gaEnable").val() == 1;
        if (allowed == false)
        {
            return;
        }

        var index = document.cookie.indexOf(" anCookieAgree=1");

        if (index != -1)
        {
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', "<?= \Yii::$app->settings->google_analytics_tracking_id; ?>", 'auto');

            ga('set', 'anonymizeIp', true);
            ga('set', 'forceSSL', true);

            ga('send', 'pageview');
        }
    }
};

App.zopim = {
    init: function () {
        var allowed = $("#zopimEnable").val() == 1;
        if (allowed == false)
        {
            return;
        }

        var index = document.cookie.indexOf(" zopimCookieAgree=1");

        if (index != -1) {

            window.$zopim || (function (d, s) {
                var z = function (c) {
                    z._.push(c)
                }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0];
                z.set = function (o) {
                    z.set.
                            _.push(o)
                };
                z._ = [];
                z.set._ = [];
                $.async = !0;
                $.setAttribute("charset", "utf-8");
                $.src = "//v2.zopim.com/?3r5nu5A2oaKBAIxReimbvHPwkEA8Bkou";
                z.t = +new Date;
                $.type = "text/javascript";
                e.parentNode.insertBefore($, e)
            })(document, "script");
        }
    }
};

App.hotjar = {
    init: function () {

        var allowed = $("#hotjarEnable").val() == 1;
        if (allowed == false)
        {
            return;
        }

        var index = document.cookie.indexOf(" anCookieAgree=1");

        if (index != -1)
        {

            (function (h, o, t, j, a, r) {
                h.hj = h.hj || function () {
                    (h.hj.q = h.hj.q || []).push(arguments)
                };
                h._hjSettings = {hjid: 194381, hjsv: 5};
                a = o.getElementsByTagName('head')[0];
                r = o.createElement('script');
                r.async = 1;
                r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
                a.appendChild(r);
            })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');

        }
    }
};