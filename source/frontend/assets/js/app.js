/**
 * Main application
 */

var App = (function () {
    'use strict';
    /**
     * debug mod
     */
    var APP_DEBUG = false;
    /**
     * Main config
     */
    var config = {
        log: {
            showTime: true,
            showStack: false
        }
    };
    /**
     * Glavna log funkcija, prima proizvoljan broj parametara
     */
    function log() {
        //ništa ne prikazuj ako nije debug mod
        if (!APP_DEBUG) {
            return;
        }

        var args = [].slice.apply(arguments);
        //ako treba debug stack
        if (config.log.showStack) {
            var stack = (new Error()).stack.split(/\n/);
            // Chrome includes a single "Error" line, FF doesn't.
            if (stack[0].indexOf('Error') === 0) {
                stack = stack.slice(1);
            }
            args = args.concat([' ... ' + stack[1].trim()]);
        }

        //ako treba, dodaj i vrijeme
        if (config.log.showTime) {
            args = [new Date().toISOString() + ' - '].concat(args);
        }

        return console.debug.apply(console, args);
    }

    /**
     * 
     * @param {string} param
     * @returns {mixed}
     */
    function getConfig(param) {
        if (typeof param === 'undefined') {
            return config;
        } else {
            return config[param];
        }
    }

    return {
        getConfig: getConfig,
        log: log,
        isDebug: function () {
            return APP_DEBUG;
        },
        init: function () {
            this.log('Initializing app...');
            this.Hostess.init(); // this is for exhibitors
            this.Event.init(); // this is for hostesses
        }
    };
}());

//Module for helper functions
App.Helpers = (function () {
    'use strict';
    /**
     * redirection to some url
     * @param {string} url
     */
    function redirect(url) {
        url = url || '';
        App.log('Redirecting to "' + url + '"');
        try {
            window.location = url;
        } catch (e) {
            App.log(e);
        }
    }

    /**
     * Reload current page
     */
    function reload() {
        App.log('Reloading page...');
        window.location.reload();
    }

    /**
     * Get current URI
     * @returns {String}
     */
    function getRequestUri() {
        return window.location.protocol + '//' + window.location.hostname + window.location.pathname;
    }

    return {
        redirect: redirect,
        reload: reload,
        getRequestUri: getRequestUri
    };
}());

//Module for hostess
App.Hostess = (function () {
    'use strict';
    /**
     * Main filter for country
     */
    function onFilterChangeCountry() {
        App.log('Filter Country changed: ' + this.value);
        var $el = $(this);
        var val = $el.val();
        var url = $el.data('url');
        //get cities as json and append to dropdown
        $.getJSON(url, {country_id: val}, function (data) {
            //check results
            App.log('Ajax success!');
            App.log(data);
            //clear city and event
            var $city = $('#hostess-city-filter');
            var $event = $('#hostess-event-filter');
            var cityNeutral = $city.find('option:first'); //maybe use .find('option[value=""]')
            var eventNeutral = $event.find('option:first'); //maybe use .find('option[value=""]')

            //clear
            $city.empty().append(cityNeutral);
            $event.empty().append(eventNeutral);
            var cityOptions = [];
            data = JSON.parse(data);
            $.each(data, function (i, val) {
                cityOptions.push('<option value="' + val["id"] + '">' + val["name"] + '</option>');
            });
            $city.append(cityOptions);
        });
    }

    /**
     * Main filter for city
     */
    function onFilterChangeCity() {
        App.log('Filter City changed: ' + this.value);
        var $el = $(this);
        var val = $el.val();
        var url = $el.data('url');
        //get cities as json and append to dropdown
        $.getJSON(url, {city_id: val}, function (data) {
            //check results
            App.log('Ajax success!');
            App.log(data);
            //clear city and event
            var $event = $('#hostess-event-filter');
            var eventNeutral = $event.find('option:first'); //maybe use .find('option[value=""]')

            //clear
            $event.empty().append(eventNeutral);
            data = JSON.parse(data);
            var eventOptions = [];
            $.each(data, function (i, val) {
                eventOptions.push('<option value="' + val["id"] + '">' + val["name"] + '</option>');
            });
            $event.append(eventOptions);
        });
    }

    /**
     * Main filter for event
     */
    function onFilterChangeEvent() {
        App.log('Filter Event changed: ' + this.value);
        var $el = $(this);
        //submit form
        $el.closest('form').submit();
    }

    /**
     * Sort dropdown on advanced search
     */
    function onFilterChangeSort()
    {
        App.log('Sort changed: ' + this.value);
        $('#advanced-search-form').submit();
    }

    /**
     * Checkboxes on hostess profile
     */
    function onProfileCheckDresscode()
    {
        App.log('Check dress code: ' + this.value);
        $('#hostess-book-dresscodes').find(':checkbox:checked').not(this).prop('checked', false);
    }

    /**
     * Hostess profile submit form / send request
     */
    function onBookFormSubmit()
    {
        App.log('Hostess book form submit');
        //simple validate
        if (
                $('.js-select-payment-type').val() == null ||
                $('.js-select-event').val() == null ||
                $("#hostess-book-date").val() == "" ||
                $("#hostess-book-date-2").val() == "" ||
                $("#event-jobs").val() == null ||
                $('.js-select-dresscode').val() == null ||
                $('.js-select-dresscode').val() == ""
                )
        {

            var message = $('#hostess-book-form').data("alert-message");
            var $modal = $("#booking-alert-modal");
            $modal.on("hidden.bs.modal", function () {
                $(".js-booking-alert-message").html("");
            });
            $(".js-booking-alert-message").html(message);
            $modal.modal();

            return false;
        }

        return true;
    }

    function setDatePickerDate(startDate, endDate, forceSetValues)
    {
        var $datepickerFrom = $('#hostess-book-date');
        var $datepickerTo = $('#hostess-book-date-2');
        $datepickerFrom.kvDatepicker('setStartDate', startDate);
        $datepickerFrom.kvDatepicker('setMinDate', startDate);
        $datepickerFrom.kvDatepicker('setEndDate', endDate);
        $datepickerFrom.kvDatepicker('setMaxDate', endDate);
        $datepickerTo.kvDatepicker('setMinDate', startDate);
        $datepickerTo.kvDatepicker('setStartDate', startDate);
        $datepickerTo.kvDatepicker('setEndDate', endDate);
        $datepickerTo.kvDatepicker('setMaxDate', endDate);
        if ($datepickerFrom.val() === '' || forceSetValues)
        {
            $datepickerFrom.val(startDate);
            $datepickerFrom.kvDatepicker('setDate', startDate);
        }

        if ($datepickerTo.val() === '' || forceSetValues)
        {
            $datepickerTo.val(endDate);
            $datepickerTo.kvDatepicker('setDate', endDate);
        }

    }

    /**
     * Hostess profile change event dropdown
     */
    function onProfileSelectEvent()
    {
        App.log('Event changed: ' + this.value);
        var $el = $(this);
        var val = $el.val();
        var url = $el.data('url');
        //get event details
        if (val !== '') {
            //get ajax data
            $.getJSON(url, {event_id: val}, function (data) {
                //check results
                App.log('Ajax success!');
                App.log(data);
                setDatePickerDate(data.start_date, data.end_date, true);
                $('.hostess-date-range-picker').show();
                var $eventJobs = $('.hostess-book-event-job');
                $eventJobs.prop('disabled', false);
                $eventJobs.each(function (i, el) {
                    var val = parseInt($(el).val());
                    if ($.inArray(val, data.event_jobs) === -1) {
                        $(el).prop('checked', false).prop('disabled', true);
                    }
                });
                var $eventDressCodes = $('.hostess-book-dress-code');
                $eventDressCodes.prop('checked', false).prop('disabled', false);
                $eventDressCodes.each(function (i, el) {
                    var val = parseInt($(el).val());
                    if ($.inArray(val, data.event_dress_codes) === -1) {
                        $(el).prop('disabled', true);
                    }
                });
            });
        } else {
            //neutral
            $('.hostess-date-range-picker').hide();
            $('.hostess-book-event-job').prop('checked', false).prop('disabled', false);
            $('.hostess-book-dress-code').prop('checked', false).prop('disabled', false);
        }
    }

    function setEventAndDatesOnProfileLoad()
    {

        var $el = $('#hostess-book-event-list');
        var val = $el.val();
        var url = $el.data('url');
        //get event details
        if (val !== '') {
            //get ajax data
            $.getJSON(url, {event_id: val}, function (data) {
                //check results
                App.log('Ajax success!');
                App.log(data);
                setDatePickerDate(data.start_date, data.end_date, false);
                $('.hostess-date-range-picker').show();
                var $eventJobs = $('.hostess-book-event-job');
                $eventJobs.prop('disabled', false);
                $eventJobs.each(function (i, el) {
                    var val = parseInt($(el).val());
                    if ($.inArray(val, data.event_jobs) === -1) {
                        $(el).prop('checked', false).prop('disabled', true);
                    }
                });
                var $eventDressCodes = $('.hostess-book-dress-code');
                $eventDressCodes.prop('checked', false).prop('disabled', false);
                $eventDressCodes.each(function (i, el) {
                    var val = parseInt($(el).val());
                    if ($.inArray(val, data.event_dress_codes) === -1) {
                        $(el).prop('disabled', true);
                    }
                });
            });
        } else {
            //neutral
            $('.hostess-date-range-picker').hide();
            $('.hostess-book-event-job').prop('checked', false).prop('disabled', false);
            $('.hostess-book-dress-code').prop('checked', false).prop('disabled', false);
        }
    }

    /**
     * Since widget for date dropdowns is not loaded manually set date dropdowns
     * * @param {modal dialog} $modal -> work experience modal dialog
     */
    function editWorkModalSetDates($modal)
    {
        var options = {
            'className': 'form-control small inline xs-select',
            'minYear': 1950
        };
        $('#start_date', $modal).dropdate(options);
        $('#end_date', $modal).dropdate(options);
    }

    /**
     * Bind click event for work experience update form
     * @param {int} id -> work experience model id
     * @param {modal} $modal -> modal dialog with form for work experience edit
     */
    function editWorkBtnSubmit(id, $modal)
    {
        $(".js-btn-edit-work", $modal).unbind("click").on("click", function (e)
        {
            e.preventDefault();
            $.ajax({
                url: "/hostess-profile/edit-work?id=" + id,
                data: $("#edit-work-form").serialize(),
                method: 'POST',
                dataType: "json",
                success: function (data) {

                    var status = parseInt(data["status_code"]);
                    if (status === 1)
                    {
                        $modal.modal("hide");
                        $modal.find(".modal-body").html('');
                        location.reload();
                    }
                    else
                    {
                        $modal.find(".modal-body").html(data["view"]);
                        editWorkModalSetDates($modal);
                    }
                }}
            );
            return false;
        });
    }

    /**
     * Bind click event for work experience update form
     * @param {int} id -> work experience model id
     * @param {modal} $modal -> modal dialog with form for work experience edit
     */
    function addWorkBtnSubmit($modal)
    {
        $(".js-btn-add-work", $modal).unbind("click").on("click", function (e)
        {
            e.preventDefault();
            $.ajax({
                url: "/hostess-profile/add-work",
                data: $("#add-work-form").serialize(),
                method: 'POST',
                dataType: "json",
                success: function (data) {

                    var status = parseInt(data["status_code"]);
                    if (status === 1)
                    {
                        $modal.modal("hide");
                        $modal.find(".modal-body").html('');
                        location.reload();
                    }
                    else
                    {
                        $modal.find(".modal-body").html(data["view"]);
                        editWorkModalSetDates($modal);
                    }
                }}
            );
            return false;
        });
    }

    function rejectRequestBtnSubmit(id, $modal)
    {
        $(".js-btn-confirm-reject", $modal).unbind("click").on("click", function (e)
        {
            $(".loading-content", $modal).show();
            var $btn = $(".js-btn-confirm-reject", $modal);
            $btn.attr("disabled", true);
            $("#reject-request-form").hide();
            e.preventDefault();
            $.ajax({
                url: "/hostess-profile/reject-request?id=" + id,
                data: $("#reject-request-form").serialize(),
                method: 'POST',
                dataType: "json",
                success: function (data) {

                    $(".loading-content", $modal).hide();
                    $btn.attr("disabled", false);
                    $("#reject-request-form").show();
                    var status = parseInt(data["status_code"]);
                    if (status === 1)
                    {
                        $modal.modal("hide");
                        $modal.find(".modal-body").html('');
                        location.reload();
                    }
                    else
                    {
                        $modal.find(".modal-body").html(data["view"]);
                    }
                }}
            );
            return false;
        });
    }

    function withdrawConfirmationBtnSubmit(id, $modal)
    {
        var $btn = $(".js-btn-withdraw-confirmation", $modal);

        $btn.unbind("click").on("click", function (e)
        {
            var $form = $("#withdraw-confirmation-form");
            var $loading = $(".loading-content", $modal);
            $loading.show();
            $btn.attr("disabled", true);
            $form.hide();
            e.preventDefault();
            $.ajax({
                url: "/hostess-profile/withdraw-confirmation?id=" + id,
                data: $form.serialize(),
                method: 'POST',
                dataType: "json",
                success: function (data) {
                    $loading.hide();
                    $btn.attr("disabled", false);
                    $form.show();
                    var status = parseInt(data["status_code"]);
                    if (status === 1)
                    {
                        $modal.modal("hide");
                        $modal.find(".modal-body").html('');
                        location.reload();
                    }
                    else
                    {
                        $modal.find(".modal-body").html(data["view"]);
                    }
                }}
            );
            return false;
        });
    }

    function changeEmailBtnSubmit($modal)
    {
        $(".js-btn-change-email", $modal).unbind("click").on("click", function (e)
        {
            $(".loading-content", $modal).show();
            var $btn = $(".js-btn-change-email", $modal);
            $btn.attr("disabled", true);
            $("#change-email-form").hide();
            e.preventDefault();
            $.ajax({
                url: "/hostess-profile/change-email",
                data: $("#change-email-form").serialize(),
                method: 'POST',
                dataType: "json",
                success: function (data) {

                    $(".loading-content", $modal).hide();
                    $btn.attr("disabled", false);
                    $("#change-email-form").show();
                    var status = parseInt(data["status_code"]);
                    if (status === 1)
                    {
                        $modal.modal("hide");
                        $modal.find(".modal-body").html('');
                        location.reload();
                    }
                    else
                    {
                        $modal.find(".modal-body").html(data["view"]);
                    }
                },
                error: function (data)
                {
                    location.reload();
                }
            }
            );
            return false;
        });
    }

    function askReviewBtnSubmit(id, $modal)
    {
        $(".js-btn-ask-review", $modal).unbind("click").on("click", function (e)
        {
            var $loading = $(".loading-content", $modal);
            var $btn = $(".js-btn-ask-review", $modal);

            $loading.show();

            $btn.attr("disabled", true);

            e.preventDefault();

            $.ajax({
                url: "/hostess-profile/ask-review?id=" + id,
                data: {"id": id},
                method: 'POST',
                dataType: "json",
                success: function (data) {

                    $(".loading-content", $modal).hide();
                    $btn.attr("disabled", false);

                    var status = parseInt(data["status_code"]);
                    if (status === 1)
                    {
                        $modal.modal("hide");
                        $modal.find(".modal-body").html('');
                        location.reload();
                    }
                    else
                    {
                        $modal.find(".modal-body").html(data["view"]);
                    }
                }}
            );
            return false;
        });
    }

    function extendTradeLicenseBtnSubmit($modal)
    {

        var $btn = $(".js-btn-trade-license", $modal);
        var $form = $("#trade-license-form");
        var $loading = $(".loading-content", $modal);
        $btn.unbind("click").on("click", function (e)
        {
            var data = $form.data("yiiActiveForm");
            $.each(data.attributes, function () {
                this.status = 3;
            });
            $form.yiiActiveForm("validate", false);
            if ($("#tradelicenseextend-date").val() === '' ||
                    $("#tradelicenseextend-reason").val() === '')
            {
                return;
            }

            $loading.show();
            $btn.attr("disabled", true);
            $form.hide();
            e.preventDefault();
            $.ajax({
                url: "/hostess-profile/extend-trade-license",
                data: $form.serialize(),
                method: 'POST',
                dataType: "json",
                success: function (data) {

                    $loading.hide();
                    $btn.attr("disabled", false);
                    $form.show();
                    var status = parseInt(data["status_code"]);
                    if (status === 1)
                    {
                        $modal.modal("hide");
                        location.reload();
                    }
                }}
            );
            return false;
        });
    }

    /**
     * Get modal body content with form for editing of work experience
     * Show modal and bind event for form submit
     * @param {int} id -> work experience model id
     */
    function editWork(id)
    {
        $.ajax({
            url: "/hostess-profile/edit-work",
            data: {id: id},
            method: 'GET',
            dataType: "json",
            success: function (data) {
                var $modal = $("#work-exp-modal");
                $modal.find(".modal-body").html(data["view"]);
                editWorkModalSetDates($modal);
                editWorkBtnSubmit(id, $modal);
                $modal.modal();
            }
        });
    }

    /**
     * Get modal body content with form for editing of work experience
     * Show modal and bind event for form submit
     * @param {int} id -> work experience model id
     */
    function addWork()
    {
        $.ajax({
            url: "/hostess-profile/add-work",
            method: 'GET',
            dataType: "json",
            success: function (data) {
                var $modal = $("#add-work-exp-modal");
                $modal.find(".modal-body").html(data["view"]);

                if (data["status_code"] === 2)
                {
                    $(".js-btn-add-work").hide();
                }
                else {
                    editWorkModalSetDates($modal);
                    addWorkBtnSubmit($modal);
                }

                $modal.modal();
            }
        });
    }

    /**
     * 
     */
    function getRejectRequest(id)
    {
        $.ajax({
            url: "/hostess-profile/reject-request",
            data: {id: id},
            method: 'GET',
            dataType: "json",
            success: function (data) {
                var $modal = $("#reject-request-modal");
                $modal.find(".modal-body").html(data["view"]);
                rejectRequestBtnSubmit(id, $modal);
                $modal.modal();
            }
        });
    }

    /**
     * 
     */
    function getWithdrawConfirmation(id)
    {
        $.ajax({
            url: "/hostess-profile/withdraw-confirmation",
            data: {id: id},
            method: 'GET',
            dataType: "json",
            success: function (data) {
                var $modal = $("#withdraw-confirmation-modal");
                $modal.find(".modal-body").html(data["view"]);
                withdrawConfirmationBtnSubmit(id, $modal);
                $modal.modal();
            }
        });
    }

    /**
     * 
     */
    function getChangeEmail()
    {
        $.ajax({
            url: "/hostess-profile/change-email",
            method: 'GET',
            dataType: "json",
            success: function (data) {
                var $modal = $("#change-email-modal");
                $modal.find(".modal-body").html(data["view"]);
                changeEmailBtnSubmit($modal);
                $modal.modal();
            }
        });
    }

    /**
     * 
     */
    function getTradeLicense()
    {

        var $modal = $("#trade-license-modal");
        extendTradeLicenseBtnSubmit($modal);
        $modal.on('shown.bs.modal', function () {
            $("#trade-license-form")[0].reset();
        });
        $modal.on('hidden.bs.modal', function () {
            $("#trade-license-form")[0].reset();
        });
        $modal.modal();
    }

    /**
     * 
     */
    function getAskReview(id)
    {
        $.ajax({
            url: "/hostess-profile/ask-review",
            method: 'GET',
            data: {id: id},
            dataType: "json",
            success: function (data) {
                var $modal = $("#ask-review-modal");
                $modal.find(".modal-body").html(data["view"]);
                askReviewBtnSubmit(id, $modal);
                $modal.modal();
            }
        });
    }

    /**
     *  Bind click event on pencil icon in working expirience gridview
     */
    function bindEditWorkEvent()
    {
        $(".js-edit-work").unbind("click").on("click", function (e) {
            var id = $(this).attr("data-id");
            editWork(id);
            e.preventDefault();
            return false;
        });
    }

    /**
     *  Bind click event on pencil icon in working expirience gridview
     */
    function bindAddWorkEvent()
    {
        $(".js-add-work").unbind("click").on("click", function (e) {
            addWork();
            e.preventDefault();
            return false;
        });
    }

    /**
     * Bind click event on red reject request button
     */
    function bindRejectRequest()
    {
        $(".js-reject-request").unbind("click").on("click", function (e) {
            var id = $(this).attr("data-id");
            getRejectRequest(id);
            e.preventDefault();
            return false;
        });
    }

    /**
     * Bind click event on red widraw confirmation button
     */
    function bindWidrawConfirmation()
    {
        $(".js-withdraw").unbind("click").on("click", function (e) {
            var id = $(this).attr("data-id");
            getWithdrawConfirmation(id);
            e.preventDefault();
            return false;
        });
    }

    /**
     * Bind click event on change email link
     */
    function bindChangeEmail()
    {
        $(".js-change-email").unbind("click").on("click", function (e) {
            getChangeEmail();
            e.preventDefault();
            return false;
        });
    }

    /**
     * Bind click event on extend trade license link
     */
    function bindTradeLicense()
    {
        $(".js-extend-trade-license").unbind("click").on("click", function (e) {
            getTradeLicense();
            e.preventDefault();
            return false;
        });
    }

    /**
     * Bind click event on ask for review button
     */
    function bindAskReview()
    {
        $(".js-ask-review").unbind("click").on("click", function (e) {
            var id = $(this).attr("data-id");
            getAskReview(id);
            e.preventDefault();
            return false;
        });
    }

    return {
        init: function () {
            App.log('Initializing Hostess module...');
            //filter events
            $('#hostess-country-filter').on('change', onFilterChangeCountry);
            $('#hostess-city-filter').on('change', onFilterChangeCity);
            $('#simple-search-form #hostess-event-filter').on('change', onFilterChangeEvent);
            $('#advanced-search-sort').on('change', onFilterChangeSort);
            $('#hostess-book-dresscodes').on('click', 'input', onProfileCheckDresscode);
            $('#hostess-book-event-list').on('change', onProfileSelectEvent);
            $('#hostess-book-form').on('submit', onBookFormSubmit);
//            $('#hostess-book-event-list').trigger("change");
            setEventAndDatesOnProfileLoad();
            /**
             * Bind everything related for work experience editing             
             */
            bindEditWorkEvent();
            /**
             * Bind add work button click event handler
             */
            bindAddWorkEvent();
            /**
             * Bind reject request button click event handler
             */
            bindRejectRequest();
            /**
             * Bind withdraw confirmation button click event handler
             */
            bindWidrawConfirmation();
            /**
             * Bind change email button click event handler
             */
            bindChangeEmail();
            /**
             * Bind postpone trade license upload button click event handler
             */
            bindTradeLicense();
            /***
             * Bind ask for review button click
             */
            bindAskReview();
        }
    };
}());

//Module for event
App.Event = (function () {
    'use strict';
    /**
     * Event filter for country
     */
    function onFilterChangeCountry() {
        App.log('Filter Country changed: ' + this.value);
        var $el = $(this);
        var val = $el.val();
        var url = $el.data('url');
        //get cities as json and append to dropdown
        $.getJSON(url, {country_id: val}, function (data) {
            //check results
            App.log('Ajax success!');
            App.log(data);
            //clear city and event
            var $city = $('#event-city-filter');
            var $event = $('#event-tt-filter');
            var cityNeutral = $city.find('option:first');

            //clear
            $city.empty().append(cityNeutral);
            $event.val('');
            data = JSON.parse(data);
            var cityOptions = [];
            $.each(data, function (i, val) {
                cityOptions.push('<option value="' + val["id"] + '">' + val["name"] + '</option>');
            });
            $city.append(cityOptions);
        });
    }

    /**
     * Event filter for city
     */
    function onFilterChangeCity() {

        App.log('Filter City changed: ' + this.value);
        var $el = $(this);
        var val = $el.val();
        var url = $el.data('url');
        //get cities as json and append to dropdown
        $.getJSON(url, {city_id: val}, function (data) {
            //check results
            App.log('Ajax success!');
            App.log(data);
            //clear city and event
            var $event = $('#event-tt-filter');
            var eventNeutral = $event.find('option:first');

            //clear
            $event.empty().append(eventNeutral);
            data = JSON.parse(data);
            var eventOptions = [];
            $.each(data, function (i, val) {
                eventOptions.push('<option value="' + val["id"] + '">' + val["name"] + '</option>');
            });
            $event.append(eventOptions);
        });
    }

    /**
     * Signup on event
     */
    function onSignUp() {
        App.log('Signup for event with value ' + this.value);
        var $el = $(this);
        var val = $el.val();
        var url = $el.data('url');
        var eventId = $el.data('id');
        App.log(url, eventId);
        if (val !== '') {
            $.post(url, {event_id: eventId, price: val}, function () {
                App.log('Sing up successful');
                App.Helpers.reload();
            });
        }
    }

    /**
     * Signup on event
     */
    function onWithdraw() {
        App.log('Withdraw from event');
        var $el = $(this);
        var url = $el.data('url');
        var eventId = $el.data('id');
        var eventName = $el.data('name');
        App.log(url, eventId, eventName);

        var $modal = $("#sign-off-modal");

        $modal.on('hidden.bs.modal', function (e) {
            $(".js-event-name", $modal).html('');
        });

        $modal.modal();

        $(".js-event-name", $modal).html(eventName);
        $(".js-btn-signoff", $modal).unbind("click").on("click", function () {
            $.post(url, {event_id: eventId}, function () {
                App.log('Withdraw successful');
                App.Helpers.reload();
            });
        });
    }

    /**
     * Sort dropdown on advanced search
     */
    function onFilterChangeSort()
    {
        App.log('Sort changed: ' + this.value);
        $('#event-search-form').submit();
    }


    return {
        init: function () {
            App.log('Initializing Event module...');
            //filter events
            $('#event-country-filter').on('change', onFilterChangeCountry);
            $('#event-city-filter').on('change', onFilterChangeCity);
            $('.js-event-signup').on('change', onSignUp);
            $('.js-event-withdraw').on('click', onWithdraw);
            $('#advanced-search-sort').on('change', onFilterChangeSort);
        }
    };
}());

/**
 * Initialize app
 */
App.log('App defined, waiting for DOM to load...');

$(document).ready(function () {

    // if hash present in url scroll to selected section
    if (window.location.hash != "")
    {
        var elem = $('#_' + window.location.hash.replace('#', ''));

        if (typeof (elem) !== "undefined" && $(elem).length > 0)
        {
            setTimeout(
                    function () {
                        $('html,body').animate({scrollTop: $(elem).offset().top - 20}, 1500);
                    }, 500);
        }
    }

    // click on menu item scroll to selected section
    $(".scroll").click(function (event) {
        var elem = $('#_' + this.hash.replace('#', ''));
        if (typeof (elem) !== "undefined" && $(elem).length > 0)
        {
            event.preventDefault();
            $('html,body').animate({scrollTop: $(elem).offset().top - 20}, 1500);
        }

    });

    $('.js-btn-reset').click(function () {


        var form = $(this)[0].form;
        $(form)
                .find(':radio, :checkbox').removeAttr('checked').end()
                .find('input, textarea, :text, select, :hidden').val('');
        if ($('.js-multi-select').length !== 0)
        {
            $('.js-multi-select').multiselect('refresh');
        }



        $(form).submit();
        return false;
    });

    $(".js-toggle-search").click(function () {

        var $searcCont = $(".js-advanced-search-cont");
        if ($searcCont.hasClass("open"))
        {
            $searcCont.removeClass("open");
            $searcCont.slideUp();
            $(".js-caret-cont").removeClass("dropup").addClass("dropdown");
            $(".js-label-text").html($(this).attr("data-closed-text"));
        }
        else {
            $searcCont.slideDown();
            $searcCont.addClass("open");
            $(".js-caret-cont").removeClass("dropdown").addClass("dropup");
            $(".js-label-text").html($(this).attr("data-opened-text"));
        }


    });

    App.log('DOM is ready');
    App.init();
    App.log('APP initialized');

});
