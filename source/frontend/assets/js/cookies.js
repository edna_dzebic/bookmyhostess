"use strict";

var App = App || {};

$(document).ready(function () {
    App.Cookie.init();
});


App.Cookie = (function () {
    'use strict';

    var cookieName = 'cookieAgree';
    var anCookieName = 'anCookieAgree';
    var prefCookieName = 'prefCookieAgree';
    var zopimCookieName = 'zopimCookieAgree';

    var $modal = $("#cookie-confirmation-modal");

    function init() {

        var isCookieDetailsPage = $("#cookieDetailsPage").length != 0;
        console.log(isCookieDetailsPage);

        var cookieAgree = Cookies.get(cookieName);

        if (typeof (cookieAgree) == "undefined" || cookieAgree == 0)
        {
            if (isCookieDetailsPage == false) {
                $modal.modal();
            }
        }

        $(".js-btn-cookie-accept-all").unbind("click").on("click", function (e) {
            e.preventDefault();

            setCookie(cookieName, 1);
            setCookie(anCookieName, 1);
            setCookie(prefCookieName, 1);
            setCookie(zopimCookieName, 1);

            if (isCookieDetailsPage == false) {
                closeModal($modal);
            }

            location.reload();
        });

        $(".js-btn-cookie-accept-selected").unbind("click").on("click", function (e) {
            e.preventDefault();

            setCookie(cookieName, 1);

            var gaAccepted = $(".js-ga-cookie").is(':checked');
            if (gaAccepted)
            {
                setCookie(anCookieName, 1);
            } else {
                setCookie(anCookieName, 0);
            }

            var prefAccepted = $(".js-preferences-cookie").is(':checked');
            if (prefAccepted)
            {
                setCookie(prefCookieName, 1);
            }
            else {
                setCookie(prefCookieName, 0);
            }

            var zopimAccepted = $(".js-zopim-cookie").is(':checked');
            if (zopimAccepted)
            {
                setCookie(zopimCookieName, 1);
            }
            else {
                setCookie(zopimCookieName, 0);
            }

            if (isCookieDetailsPage == false) {
                closeModal($modal);
            }

            location.reload();
        });
    }

    function isConstentAgreed() {
        var cookieAgree = Cookies.get(cookieName);
        if (typeof (cookieAgree) == "undefined" || cookieAgree == 0)
        {
            return false;
        }

        return true;
    }

    function setCookie(cookieName, value)
    {
        Cookies.set(cookieName, value);
    }

    function closeModal($modal)
    {
        $modal.modal("hide");
    }

    return {
        init: function () {
            init();
        },
        isAgreed: function () {
            return isConstentAgreed();
        }
    };

}());