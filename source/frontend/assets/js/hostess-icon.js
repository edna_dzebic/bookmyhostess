"use strict";

var Main = Main || {};

$(document).ready(function () {


    setIconPosition();

    $(window).resize(function () {
        setIconPosition();
    });

});

function setIconPosition() {
    var image = $(".profile-image");

    var marginLeft = image.css("margin-left");

    var left = 15 + parseInt(marginLeft) + $(".profile-image").width() - 38;

    $(".zoom-in").css("left", left);

    $(".has-mobile .mobile").css("left", left);
    $(".has-mobile .zoom-in").css("left", left - 38);
}