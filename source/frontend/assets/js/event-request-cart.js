"use strict";

var App = App || {};

$(document).ready(function () {
    App.Booking.BindEvents();
});

App.Booking = {
    BindEvents: function () {
        var _self = this;
        $(".js-add-booking").on("click", function () {
            var elem = $(this);
            _self.AddToCart(elem, elem.attr("data-id"));
        });

        $(".js-remove-booking").on("click", function () {
            var elem = $(this);
            _self.RemoveFromCart(elem, elem.attr("data-id"));
        });


        $(".js-proceed-booking").on("click", function (e) {
            if ($(".js-proceed-booking").hasClass("dissabled"))
            {
                // show dialog
                var $modal = $("#booking-modal");
                $modal.modal();

                e.stopImmediatePropagation();
                e.preventDefault();
                return false;
            }
        });

        var $widthLast = $("td:nth-last-child(1)").width();
        var $widthPreLast = $("td:nth-last-child(2)").width();

        $(".js-proceed-booking").width($widthLast + $widthPreLast);
    },
    AddToCart: function (elem, id) {
        $.ajax({
            method: "GET",
            url: "/exhibitor/add-booking",
            data: {id: id},
            success: function (data) {

                elem.addClass("hidden");
                elem.parent().find(".js-remove-booking").removeClass("hidden");

                data = JSON.parse(data);

                if (parseInt(data.cartCount) > 0)
                {
                    $(".js-proceed-booking").removeClass("dissabled");
                }
                else {
                    $(".js-proceed-booking").addClass("dissabled");
                }
            },
            error: function (data) {
                alert(data.responseText);
            }
        });
    },
    RemoveFromCart: function (elem, id) {
        $.ajax({
            method: "GET",
            url: "/exhibitor/remove-booking",
            data: {id: id},
            success: function (data) {
                elem.addClass("hidden");
                elem.parent().find(".js-add-booking").removeClass("hidden");

                data = JSON.parse(data);

                if (parseInt(data.cartCount) > 0)
                {
                    $(".js-proceed-booking").removeClass("dissabled");
                }
                else {
                    $(".js-proceed-booking").addClass("dissabled");
                }
            },
            error: function (data) {
                alert(data.responseText);
            }
        });
    }
};