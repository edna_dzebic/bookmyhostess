"use strict";

var App = App || {};

$(document).ready(function () {

    App.HostessView.init();
});


App.HostessView = {
    slyInitialized: false,
    sly: null,
    init: function () {
        $(".js-tooltip").tooltip();

        $(".loading-content").fadeOut(2000, function () {
            $(".js-hostes-view-profile").fadeIn(500, function () {
                $('.carousel').carousel({
                    interval: 5000
                });
                App.HostessView.initSly();
            });
        });

        App.HostessView.initInfoIcons();
    },
    initSly: function () {

        var self = this;

        if ($('#profile-images').length == 0)
        {
            return;
        }

        var $wrap = $('#profile-images');

        var $frame = $('#centered', $wrap);

        self.sly = new Sly($frame, {
            horizontal: 1,
            itemNav: 'centered',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 1,
            scrollBy: 1,
            speed: 300,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1
        }).init();

        self.slyInitialized = true;

        $(".js-next", $wrap).on("click", function ()
        {
            self.sly.next();
        });

        $(".js-prev", $wrap).on("click", function ()
        {
            self.sly.prev();
        });
    },
    initInfoIcons: function () {
        $(".js-daily-rate-info-icon").on("click", function ()
        {
            $("#price-modal").modal();
        });
        $(".js-surcharge-info-icon").on("click", function ()
        {
            $("#last-minute-modal").modal();
        });
        $(".js-mobile-app-info-icon").on("click", function ()
        {
            $("#mobile-app-modal").modal();
        });
        $(".js-reliability-score-info-icon").on("click", function ()
        {
            $("#reliability-modal").modal();
        });

        $(".js-overall-rating-info-icon").on("click", function ()
        {
            $(".js-reviews-tab>a").click();

            var href = $(".js-reviews-tab>a").attr("href");

            $('html, body').animate({
                scrollTop: $(href).offset().top - 50
            }, 2000);

        });

    }
};