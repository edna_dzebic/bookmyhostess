(function () {

    'use strict';

    ProceedToCheckoutEvent.prototype = {
        eventName: 'ProceedToCheckoutEvent',
        eventCategory: 'Proceed-to-booking',
        eventAction: '',
        eventLabel: ''
    };

    function ProceedToCheckoutEvent(action, label) {
        this.eventAction = action;
        this.eventLabel = label;
    }

    window.ProceedToCheckoutEvent = ProceedToCheckoutEvent;
})();