$(document).ready(function () {

    var labels = {
        dissabled: "Click on disabled proceed to checkout button",
        enabled: "Click on enabled proceed to checkout button"
    };

    $(".js-proceed-booking").on("click", function (e) {

        if ($(".js-proceed-booking").hasClass("dissabled"))
        {
            GT.Track(new ProceedToCheckoutEvent("click", labels.dissabled));
        }
        else {
            GT.Track(new ProceedToCheckoutEvent("click", labels.enabled));
        }
    });
});