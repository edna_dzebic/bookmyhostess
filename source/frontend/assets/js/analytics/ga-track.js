"use strict";

var GT = GT || {};

GT.Track = function (event) {
    ga('send', {
        hitType: 'event',
        eventCategory: event.eventCategory,
        eventAction: event.eventAction,
        eventLabel: event.eventLabel
    });
};