"use strict";

var App = App || {};

$(document).ready(function () {
    App.newsLetter.init();
});

App.newsLetter = {
    inProgress: false,
    init: function () {
        this.inProgress = false;
        $("#newsletter-subcribe").attr("disabled", false);
        $("#newsletter-subcribe").on("click", function () {
            App.newsLetter.submit();
        });
    },
    submit: function () {
        if (this.inProgress === false) {
            this.inProgress = true;
            $("#newsletter-subcribe").attr("disabled", true);
            $.ajax({
                url: '/newsletter/subscribe',
                method: 'POST',
                data: $("#newsletter-subscribe-form").serialize(),
                success: function (data) {
                    $(".js-newsletter-column").replaceWith(data);
                    App.newsLetter.init();
                },
                error: function (error) {
                    App.newsLetter.init();
                }
            });
        }
    }
};



