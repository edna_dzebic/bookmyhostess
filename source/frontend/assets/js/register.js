'use strict';

$(window).load(function () {

    var isPassword = true;

    $(".js-password .js-show-password").click(function (e) {

        if (isPassword) {
            $(".js-password input").attr('type', 'text');
            isPassword = false;
            $(this).removeClass("glyphicon-eye-close").addClass("glyphicon-eye-open");
        }
        else {
            $(".js-password input").attr('type', 'password');
            isPassword = true;
            $(this).removeClass("glyphicon-eye-open").addClass("glyphicon-eye-close");
        }

        e.preventDefault();

    });

});




