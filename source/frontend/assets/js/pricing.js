'use strict';

$(window).load(function () {

    var $container = $('#_pricing');

    var $optionSet = $('.js-pricing-filter');
    var $optionLinks = $optionSet.find('.pricing-filter-item');

    $optionLinks.click(function () {
        var $this = $(this);
       
        if ($this.hasClass('selected')) {
            return false;
        }
        
        $optionSet.find('.selected').removeClass('selected');       
        
        $this.addClass('selected');

        var $value = $this.attr('data-option-value');

        $(".element.active").fadeOut(400, function () {
            $(this).removeClass("active");
            $container.find($value).fadeIn(400, function () {
                $(this).addClass("active");
            });
        });

        return false;
    });

});




