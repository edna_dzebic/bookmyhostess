<?php

namespace frontend\assets;

class ReviewAsset extends BaseAssetBundle
{

    protected $styles = [
        'css/review',
    ];
       
    public function init()
    {
        parent::init();
    }

}
