<?php

namespace frontend\assets;

class IndexAsset extends BaseAssetBundle
{

    public $css = [
        'css/lightbox.min.css',
        'css/jquery.webui-popover.min.css'
    ];
    public $js = [
        'js/plugins.min.js',
        'js/sly.min.js',
        'js/jquery.webui-popover.min.js',
        'js/lightbox.min.js',
        'js/rangetouch.min.js',
        'js/plyr.min.js'
    ];
    protected $styles = [
        'css/plyr',
        'css/pricing',
        'css/gallery',
        'css/horizontal',
        'css/webui-popover',
        'css/index',
    ];
    protected $scripts = [
        'js/pricing',
        'js/index',
    ];

    public function init()
    {
        parent::init();
    }

}
