<?php

namespace frontend\assets;

class EventCatalogAsset extends BaseAssetBundle
{

    protected $styles = [
        'css/event'
    ];
   
    public function init()
    {
        parent::init();
    }

}
