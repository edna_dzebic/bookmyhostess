<?php

namespace frontend\assets;

class RegisterAsset extends BaseAssetBundle {

    protected $styles = [
        'css/register',
    ];
    protected $scripts = [
        'js/register'
    ];
       
    public function init()
    {
        parent::init();
    }

}
