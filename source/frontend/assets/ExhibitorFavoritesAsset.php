<?php

namespace frontend\assets;

class ExhibitorFavoritesAsset extends BaseAssetBundle
{

    protected $scripts = [
        'js/hostess-icon'
    ];
   
    public function init()
    {
        parent::init();
    }

}
