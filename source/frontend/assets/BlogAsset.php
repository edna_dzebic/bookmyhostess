<?php

namespace frontend\assets;

class BlogAsset extends BaseAssetBundle
{
    protected $styles = [
        'css/blog'
    ];
  
    public function init()
    {
        parent::init();
    }

}
