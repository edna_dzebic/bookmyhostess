<?php

namespace frontend\assets;

class FancyBoxAsset extends BaseAssetBundle {

    public $css = [
        'css/jquery.fancybox.min.css',
    ];
    public $js = [
        'js/jquery.fancybox.min.js'
    ];
       
    public function init()
    {
        parent::init();
    }

}
