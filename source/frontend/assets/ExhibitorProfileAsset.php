<?php

namespace frontend\assets;

class ExhibitorProfileAsset extends BaseAssetBundle
{

    protected $scripts = [
        'js/change-email-ex'
    ];
    protected $styles = [
        'css/exhibitor-profile'
    ];
    
    public function init()
    {
        parent::init();
    }

}
