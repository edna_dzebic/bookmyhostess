<?php

namespace frontend\assets;

class EventViewAsset extends BaseAssetBundle
{
    protected $styles = [
        'css/event-view'
    ];
  
    public function init()
    {
        parent::init();
    }

}
