<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class BaseAssetBundle extends AssetBundle
{

    const EXT_JS = ".js";
    const EXT_CSS = ".css";
    const MIN_EXT_JS = ".min.js";
    const MIN_EXT_CSS = ".min.css";

    public $sourcePath = '@frontend/assets/';
    public $js = [];
    public $css = [];
    protected $scripts = [];
    protected $styles = [];
    protected $additionalDepends = [];
    public $depends = [
        'frontend\assets\AppAsset'
    ];

    public function init()
    {
        parent::init();

        if (!YII_DEBUG)
        {
            $this->js = array_merge($this->js, $this->getMinifiedJs());
            $this->css = array_merge($this->css, $this->getMinifiedCss());
        } else
        {
            $this->js = array_merge($this->js, $this->getRegularJs());
            $this->css = array_merge($this->css, $this->getRegularCss());
        }

        $this->depends = array_merge($this->depends, $this->additionalDepends);
    }

    private function getMinifiedJs()
    {
        return $this->getScripts(self::MIN_EXT_JS);
    }

    private function getRegularJs()
    {
        return $this->getScripts(self::EXT_JS);
    }

    private function getScripts($ext)
    {
        return $this->getFiles($this->scripts, $ext);
    }

    private function getMinifiedCss()
    {
        return $this->getStyles(self::MIN_EXT_CSS);
    }

    private function getRegularCss()
    {
        return $this->getStyles(self::EXT_CSS);
    }

    private function getStyles($ext)
    {
        return $this->getFiles($this->styles, $ext);
    }

    private function getFiles($haystack, $ext)
    {
        $result = [];

        foreach ($haystack as $value)
        {
            $result[] = $this->addExt($value, $ext);
        }

        return $result;
    }

    private function addExt($value, $ext)
    {
        return $value . $ext;
    }

}
