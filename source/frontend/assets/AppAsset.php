<?php

namespace frontend\assets;

class AppAsset extends BaseAssetBundle
{

    protected $scripts = [
        'js/js-cookie',
        'js/app',
        'js/cookies',
        'js/scripts',
        'js/newsletter',
        'js/analytics/ga-track'
    ];
    protected $styles = [
        'css/font',
        'css/normalize',
        'css/buttons',
        'css/helpers',
        'css/pagination',
        'css/form',
        'css/footer',
        'css/header',
        'css/site',
        'css/main',
        'css/mobile'
    ];
    public $css = [
        'css/font-awesome.min.css',
        'css/jquery-ui.min.css',
        'css/shariff.complete.min.css'
    ];
    public $js = [
        'js/jquery-ui.min.js',
        'js/shariff.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];

    public function init()
    {
        parent::init();
    }

}
