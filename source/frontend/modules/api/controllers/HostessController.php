<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use frontend\modules\api\filters\auth\CompositeAuth;
use yii\web\Response;

class HostessController extends \frontend\modules\api\components\Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter' => [
                'class' => VerbFilter::className(),
                'actions' => $this->verbs(),
            ],
            'authenticator' => [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    [
                        'class' => \frontend\modules\api\filters\auth\ApiAuth::className(),
                    ],
                    [
                        'class' => \frontend\modules\api\filters\auth\UserAuth::className(),
                        'optional' => ['login']
                    ]
                ]
            ]
        ];
    }

    public function actionLogin() {
        return $this->getApi()->login($this->getData());
    }

    public function actionLogout() {
        return $this->getApi()->logout($this->getData());
    }

    public function actionNewRequests() {
        return $this->getApi()->getNewRequestList();
    }

    public function actionPastBookings() {
        return $this->getApi()->getPastBookings();
    }

    public function actionBookings() {
        return $this->getApi()->getBookings();
    }

    public function actionRequests() {
        return $this->getApi()->getRequestList();
    }

    public function actionView() {
        return $this->getApi()->details($this->getData());
    }

    public function actionAccept() {
        return $this->getApi()->accept($this->getData());
    }

    public function actionReject() {
        return $this->getApi()->reject($this->getData());
    }

    public function actionUpdateToken() {        
        return $this->getApi()->updateToken($this->getData());
    }

    /**
     * @return \app\modules\api\models\Hostess
     */
    private function getApi() {
        return new \frontend\modules\api\models\Hostess();
    }

}
