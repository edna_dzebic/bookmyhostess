<?php

namespace frontend\modules\api\filters\auth;

use Yii;

class UserAuth extends AuthMethod {

    /**
     * @var string the parameter name for passing the access token
     */
    public $tokenParam = 'auth_key';

    /**
     * @inheritdoc
     */
    public function authenticate($action, $user, $request, $response) {

        if ($this->isOptional($action)) {
            return true;
        }

        $accessToken = $request->getHeaders()->get($this->tokenParam);

        if (!isset($accessToken)) {
            $accessToken = $request->post($this->tokenParam);
        }

        if (is_string($accessToken)) {
            $identity = $user->loginByAccessToken($accessToken, get_class($this));
            if ($identity !== null) {
                return true;
            }
        }

        $this->handleFailure($response);
    }

    public function handleFailure($response) {
        throw new \frontend\modules\api\exception\ApiException(422, Yii::t('app', 'Incorrect username or password'));
    }

    /**
     * Checks, whether authentication is optional for the given action.
     *
     * @param Action $action
     * @return boolean
     * @see optional
     * @since 2.0.7
     */
    protected function isOptional($action) {
        $id = $this->getActionId($action);
        return in_array($id, $this->optional, true);
    }

}
