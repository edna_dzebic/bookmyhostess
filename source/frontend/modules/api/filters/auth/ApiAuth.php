<?php

namespace frontend\modules\api\filters\auth;

use Yii;

class ApiAuth extends AuthMethod {

    /**
     * @var string the parameter name for passing the access token
     */
    public $tokenParam = 'api_auth';

    /**
     * @inheritdoc
     */
    public function authenticate($action, $user, $request, $response) {
        $accessToken = $request->getHeaders()->get($this->tokenParam);

        if (!isset($accessToken)) {
            $accessToken = $request->post($this->tokenParam);
        }

        if (is_string($accessToken)) {

            if ($accessToken === Yii::$app->params["api_auth_key"]) {
                return true;
            }
        }

        $this->handleFailure($response);
    }

}
