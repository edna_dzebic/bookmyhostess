<?php

namespace frontend\modules\api\components;

use Yii;

class Controller extends \yii\rest\Controller {

    private $langParam = "x-language";

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function beforeAction($action) {

        $lang = Yii::$app->request->getHeaders()->get($this->langParam);


        if (!isset($lang)) {
            $lang = Yii::$app->request->post($this->langParam);
        }

        if (isset($lang)) {
            Yii::$app->language = $lang;
        }

        return parent::beforeAction($action);
    }

    protected function getData() {
        return Yii::$app->request->isPost ? Yii::$app->request->post() : Yii::$app->request->get();
    }

}
