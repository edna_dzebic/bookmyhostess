<?php

namespace frontend\modules\api\components;

class ErrorHandler extends \yii\web\ErrorHandler {

    /**
     * Converts an exception into an array.
     * @param \Exception $exception the exception being converted
     * @return array the array representation of the exception.
     */
    protected function convertExceptionToArray($exception) {
        if (!YII_DEBUG && !$exception instanceof \yii\base\UserException && !$exception instanceof  \yii\web\HttpException) {
            $exception = new HttpException(500, Yii::t('app', 'There was an error at the server.'));
        }
        $array = [
            'error' => $exception->getMessage(),
            'error_code' => $exception->getCode()
        ];

        if ($exception instanceof \frontend\modules\api\exception\ApiException) {
            $array["error_messages"] = $exception->messages;
            $array["status"] =  $exception->statusCode;
        }

//        if (YII_DEBUG) {
//            $array['type'] = get_class($exception);
//            if (!$exception instanceof UserException) {
//                $array['file'] = $exception->getFile();
//                $array['line'] = $exception->getLine();
//                $array['stack-trace'] = explode("\n", $exception->getTraceAsString());
//                if ($exception instanceof \yii\db\Exception) {
//                    $array['error-info'] = $exception->errorInfo;
//                }
//            }
//        }
        if (($prev = $exception->getPrevious()) !== null) {
            $array['previous'] = $this->convertExceptionToArray($prev);
        }
        return $array;
    }

}
