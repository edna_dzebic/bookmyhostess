<?php

namespace frontend\modules\api;

use Yii;

class Module extends \yii\base\Module {

    public function init() {
        parent::init();
        \Yii::configure($this, [
            'components' => [
                'errorHandler' => [
                    'class' => '\frontend\modules\api\components\ErrorHandler',
                ]
            ]
        ]);

        $errorHandler = new \frontend\modules\api\components\ErrorHandler();

        Yii::$app->set('errorHandler', $errorHandler);
        $errorHandler->register();
    }

}
