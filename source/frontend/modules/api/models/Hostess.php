<?php

namespace frontend\modules\api\models;

use Yii;
use common\models\Email;
use common\models\NotificationToken;
use common\models\User;
use frontend\models\Request;
use common\models\Event;

class Hostess extends Base
{

    /**
     * @param array $params
     *
     * @return string
     */
    public function login($params)
    {
        $data = $this->getData($params);


        $username = $this->getParam($data, 'username', null);
        $password = $this->getParam($data, 'password', null);

        $notification_token = $this->getParam($data, 'notification_token', null);

        if ($username == null || $password == null || $notification_token == null)
        {
            throw new \yii\web\BadRequestHttpException("Missing required parameters!");
        }

        $user = User::findHostessByUsername($username);

        if ($user)
        {
            if ($user->validatePassword($password))
            {

                $os_type = $this->getParam($data, 'os_type', NotificationToken::ANDROID_TYPE);


                $notificationModel = $this->insertNotificationToken($user, $notification_token, $os_type);

                if (!is_object($notificationModel))
                {
                    throw new \frontend\modules\api\exception\ApiException(403, "Notification token is required!");
                }

                $data = [
                    'id' => $user->id,
                    'username' => $user->username,
                    'auth_key' => $user->auth_key,
                    'notification_token_id' => $notificationModel->id
                ];

                return $this->formatOutput($data);
            }
        }

        throw new \frontend\modules\api\exception\ApiException(401, "Incorrect username or password!");
    }

    /**
     * Logout user
     * @param array $params
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function logout($params)
    {
        $data = $this->getData($params);
        $notification_token_id = $this->getParam($data, 'notification_token_id');

        if ($notification_token_id == null)
        {
            throw new \yii\web\BadRequestHttpException("Missing required parameters!");
        }

        $model = NotificationToken::findOne(
                        [
                            "id" => $notification_token_id
                        ]
        );

        if (!is_object($model))
        {
            throw new \yii\web\NotFoundHttpException("Model not found");
        }

        $model->delete();
        return $this->formatOutput(["message" => Yii::t('app.hostess', 'Token deleted!')]);
    }

    /**
     * Get new requests
     * @return array
     */
    public function getNewRequestList()
    {
        $requests = Request::find()
                ->joinWith(["event"])
                ->joinWith(["userRequested"])
                ->andWhere(["request.user_id" => Yii::$app->user->identity->id])
                ->andWhere(['request.status_id' => [
                        Yii::$app->status->request_new
            ]])
                ->andWhere("DATE(event.end_date)>= DATE(NOW())")
                ->orderBy('request.date_created desc')
                ->all();

        $result = [];

        foreach ($requests as $model)
        {
            $result[] = $this->formatRequestModel($model);
        }

        return $this->formatOutput($result);
    }

    /**
     * Get current bookings
     * @return array
     */
    public function getBookings()
    {
        $requests = Request::find()
                ->joinWith(["event"])                
                ->joinWith(["userRequested"])
                ->andWhere(["request.user_id" => Yii::$app->user->identity->id])
                ->andWhere(['request.completed' => 1])
                ->andWhere('request.completed IS NOT NULL')
                ->andWhere("DATE(event.end_date)>= DATE(NOW())")
                ->orderBy('event.start_date asc')
                ->all();

        $result = [];

        foreach ($requests as $model)
        {
            $result[] = $this->formatRequestModel($model);
        }

        return $this->formatOutput($result);
    }

    /**
     * Get past bookings for user
     * @return array
     */
    public function getPastBookings()
    {
        $requests = Request::find()
                ->joinWith(["event"])                
                ->joinWith(["userRequested"])
                ->andWhere(["request.user_id" => Yii::$app->user->identity->id])
                ->andWhere(['request.completed' => 1])
                ->andWhere('request.completed IS NOT NULL')
                ->andWhere("DATE(event.end_date)< DATE(NOW())")
                ->orderBy('event.start_date asc')
                ->all();

        $result = [];

        foreach ($requests as $model)
        {
            $result[] = $this->formatRequestModel($model);
        }

        return $this->formatOutput($result);
    }

    /**
     * Display list of requests for user
     * @return array
     */
    public function getRequestList()
    {
        $requests = Request::find()
                ->joinWith(["event"])
                ->joinWith(["userRequested"])
                ->andWhere(["request.user_id" => Yii::$app->user->identity->id])
                ->andWhere(['request.status_id' => [
                        Yii::$app->status->request_accept,
                        Yii::$app->status->request_reject
                    ]
                ])
                ->andWhere('request.completed IS NULL OR request.completed = 0')
                ->orderBy('request.date_created desc')
                ->all();

        $result = [];

        foreach ($requests as $model)
        {
            $result[] = $this->formatRequestModel($model);
        }

        return $this->formatOutput($result);
    }

    /**
     * Detail view of request
     * @param array $params
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function details($params)
    {
        $data = $this->getData($params);
        $id = $this->getParam($data, 'id');

        if ($id == null)
        {
            throw new \yii\web\BadRequestHttpException("Missing required parameters!");
        }

        $model = Request::findOne(
                        [
                            "request.id" => $id,
                            "request.user_id" => Yii::$app->user->identity->id
                        ]
        );

        if (!is_object($model))
        {
            throw new \yii\web\NotFoundHttpException("Model not found");
        }

        $result = $this->formatRequestViewModel($model);

        return $this->formatOutput($result);
    }

    /**
     * Accept request
     * @param array $params
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \frontend\modules\api\exception\ApiException
     */
    public function accept($params)
    {
        $lang = Yii::$app->language;

        $data = $this->getData($params);
        $id = $this->getParam($data, 'id');

        if ($id == null)
        {
            throw new \yii\web\BadRequestHttpException("Missing required parameters!");
        }

        $model = Request::findOne(
                        [
                            "request.id" => $id,
                            "request.user_id" => Yii::$app->user->identity->id,
                            "request.status_id" => Yii::$app->status->request_new
                        ]
        );

        if (!is_object($model))
        {
            throw new \yii\web\NotFoundHttpException("Model not found");
        }

        $model->status_id = Yii::$app->status->request_accept;
        $model->date_responded = date('Y-m-d H:i:s');

        if (!$model->update(false))
        {
            throw new \frontend\modules\api\exception\ApiException(422, "Error while updating model", $model->getErrors());
        } else
        {
            if (is_object($model->user) && is_object($model->user->hostessProfile))
            {
                $model->user->hostessProfile->recalculateProfileCompleteness();
            }

            Email::sendAcceptRequestEmailToExibitor($model);
        }

        Yii::$app->language = $lang;
        return $this->formatOutput(["message" => Yii::t('app.hostess', 'Accepted')]);
    }

    /**
     * Reject request
     * @param array $params
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \frontend\modules\api\exception\ApiException
     */
    public function reject($params)
    {
        $lang = Yii::$app->language;

        $data = $this->getData($params);
        $id = $this->getParam($data, 'id');

        if ($id == null)
        {
            throw new \yii\web\BadRequestHttpException("Missing required parameters!");
        }

        $model = Request::findOne(
                        [
                            "request.id" => $id,
                            "request.user_id" => Yii::$app->user->identity->id,
                            "request.status_id" => Yii::$app->status->request_new
                        ]
        );

        if (!is_object($model))
        {
            throw new \yii\web\NotFoundHttpException("Model not found");
        }

        $model->reason = "other";
        $model->status_id = Yii::$app->status->request_reject;
        $model->date_responded = date('Y-m-d H:i:s');
        $model->date_rejected = date('Y-m-d H:i:s');

        if (!$model->update(false))
        {
            throw new \frontend\modules\api\exception\ApiException(422, "Error while updating model", $model->getErrors());
        } else
        {
            if (is_object($model->user) && is_object($model->user->hostessProfile))
            {
                $model->user->hostessProfile->recalculateProfileCompleteness();
            }

            Email::sendRejectRequestEmailToExibitor($model);
            Email::sendAfterRejectEmailToHostess($model->user);
        }


        Yii::$app->language = $lang;
        return $this->formatOutput(["message" => Yii::t('app.hostess', 'Rejected')]);
    }

    /**
     * Update notification token for user
     * @param array $params
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \frontend\modules\api\exception\ApiException
     */
    public function updateToken($params)
    {
        $data = $this->getData($params);
        $notification_token_id = $this->getParam($data, 'notification_token_id');
        $token = $this->getParam($data, 'token');

        if ($notification_token_id == null || $token == null)
        {
            throw new \yii\web\BadRequestHttpException("Missing required parameters!");
        }

        $model = NotificationToken::findOne(
                        [
                            "user_id" => Yii::$app->user->identity->id,
                            "id" => $notification_token_id
                        ]
        );

        if (!is_object($model))
        {
            throw new \yii\web\NotFoundHttpException("Model not found");
        }

        $model->token = $token;
        if ($model->update() === false)
        {
            throw new \frontend\modules\api\exception\ApiException(422, "Error while updating model", $model->getErrors());
        }

        return $this->formatOutput(["message" => Yii::t('app.hostess', 'Token updated!')]);
    }

    /**
     * Inser notification token
     * @param User $user
     * @param string $token
     * @param string $os_type
     * @return NotificationToken
     */
    private function insertNotificationToken(User $user, $token, $os_type)
    {
        $model = NotificationToken::findOne(
                        [
                            "user_id" => $user->id,
                            "token" => $token
                        ]
        );

        if (!is_object($model))
        {
            $model = new NotificationToken();
            $model->user_id = $user->id;
            $model->token = $token;
            $model->os_type = $os_type;

            if ($model->save() === false)
            {
                return null;
            }
        }

        if (is_object($model->user) && is_object($model->user->hostessProfile))
        {
            $model->user->hostessProfile->has_mobile_app = true;

            // do not run validation, God knows what might fail
            $model->user->hostessProfile->update(false);

            $model->user->hostessProfile->recalculateProfileCompleteness();
        }

        return $model;
    }

    /**
     * Format request for list view
     * @param Request $model
     * @return type
     */
    private function formatRequestModel(Request $model)
    {
        $data = [
            'id' => $model->id,
            'status' => $model->getOldStatus(),
            'event_name' => $model->event->name,
            'event_city' => $model->event->city->name,
            'event_venue' => $model->event->venue,
            'days' => $model->getTotalBookingDays(),
            'date_created' => gmdate('Y-m-d\TH:i:s\Z', strtotime($model->date_created)),
            'date_from' => Yii::$app->util->formatDate($model->date_from),
            'date_to' => Yii::$app->util->formatDate($model->date_to),
            'daily_rate' => Yii::$app->util->formatEventPrice($model->event, $model->price),
            'outfit' => $model->dressCode->translatedName,
            'job_name' => $model->jobName,
            'completed' => $model->completed === 1 ? 1 : 0,
            'date_responded' => Yii::$app->util->formatDateTime($model->date_responded)
        ];

        return $data;
    }

    /**
     * Format request data for detail view
     * @param Request $model
     * @return array
     */
    private function formatRequestViewModel(Request $model)
    {

        $showClientData = false;
        if ($model->status_id == Yii::$app->status->request_accept && $model->completed === 1 && $model->event->end_date >= date("Y-m-d"))
        {
            $showClientData = true;
        }

        $data = [
            'request' => [
                'id' => $model->id,
                'status' => $model->getOldStatus(),
                'days' => $model->getTotalBookingDays(),
                'date_created' => gmdate('Y-m-d\TH:i:s\Z', strtotime($model->date_created)),
                'date_from' => Yii::$app->util->formatDate($model->date_from),
                'date_to' => Yii::$app->util->formatDate($model->date_to),
                'daily_rate' => Yii::$app->util->formatEventPrice($model->event, $model->price),
                'outfit' => $model->dressCode->getTranslatedName(),
                'job_name' => $model->getJobName(),
                'completed' => $model->completed === 1 ? 1 : 0,
                'date_responded' => Yii::$app->util->formatDateTime($model->date_responded),
                'show_client_data' => $showClientData,
                'payment_type' => $model->getPaymentTypeLabel()
            ],
            'event' => $this->formatEventModel($model->event),
            'exhibitor' => $this->formatExhibitorModel($model->userRequested)
        ];

        return $data;
    }

    /**
     * 
     * @param \common\models\Event $model
     */
    private function formatEventModel(Event $model)
    {
        $data = [
            'name' => $model->name,
            'description' => strip_tags($model->brief_description),
            'category' => $model->type->getTranslatedName(),
            'venue' => $model->venue,
            'url' => $model->url,
            'city' => isset($model->city) ? $model->city->getTranslatedName() : '',
            'country' => isset($model->country) ? $model->country->getTranslatedName() : '',
            'start_date' => Yii::$app->util->formatDate($model->start_date),
            'end_date' => Yii::$app->util->formatDate($model->end_date),
        ];

        return $data;
    }

    /**
     * 
     * @param \common\models\User $model
     */
    private function formatExhibitorModel(User $model = null)
    {
        $data = [
            'first_name' => $model ? $model->first_name : '',
            'last_name' => $model ? $model->last_name : '',
            'full_name' => $model ? $model->fullName : '',
            'phone' => $model ? $model->phone_number : '',
            'email' => $model ? $model->email : ''
        ];

        return $data;
    }

}
