<?php

namespace frontend\modules\api\models;

use Yii;
use yii\helpers\Json;

class Base
{

    /**
     * Get value with $key from $array if it's set.
     * Return $default instead.
     *
     * @param array $array
     * @param string $key
     * @param mixed $default
     *
     * @return mixed
     */
    protected function getParam($array, $key, $default = 0)
    {
        if (isset($array[$key]))
        {
            return $array[$key];
        }

        return $default;
    }

    /**
     * Was planned if later additional things should be added to all responses
     * @param array $data
     * @return array
     */
    protected function formatOutput($data)
    {
        return $data;
    }

    /**
     * Log error
     * @param string $error
     */
    protected function logError($error)
    {
        Yii::error($error, 'API');
    }

    /**
     * Log info
     * @param string $info
     */
    protected function logInfo($info)
    {
        Yii::info($info, 'API');
    }

    /**
     * Log warning
     * @param string $warning
     */
    protected function logWarning($warning)
    {
        Yii::info($warning, 'API');
    }

    /**
     *
     * @param string $json
     *
     * @return array
     */
    protected function decodeJson($json)
    {
        return Json::decode($json, true);
    }

    /**
     * @param array $params
     *
     * @return array
     */
    protected function getDataAndSetLanguage($params)
    {
        $data = $this->decodeJson($params['data']);
        $this->setLanguage($data); /// TODO: this does not belong here
        return $data;
    }

    /**
     * @param $data
     *
     * @return void
     */
    protected function setLanguage($data)
    {
        if (isset($data["lang"]))
        {
            Yii::$app->language = $data["lang"];
        } else if (Yii::$app->user->isGuest === false)
        {
            Yii::$app->language = Yii::$app->user->lang;
        }
    }

    /**
     * @param array $params
     *
     * @return array
     */
    protected function getData($params)
    {
        return $this->decodeJson($params['data']);
    }

}
