<?php

namespace frontend\modules\api\exception;

class ApiException extends \yii\web\HttpException {

    public $messages = [];

    public function __construct($status, $message, $messages = []) {
        parent::__construct($status, $message);

        $this->messages = $messages;
    }

}
