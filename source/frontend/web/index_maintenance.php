<?php
if (in_array(@$_SERVER['REMOTE_ADDR'], [
            // DO NOT TOUCH UNDER
            '127.0.0.1', '::1',
            '64.4.248.8', '64.4.249.8', '173.0.84.40',
            '173.0.84.8', '173.0.88.40', '173.0.88.8',
            '173.0.92.8', '173.0.93.8', '66.211.170.66',
            '173.0.81.1', '173.0.81.0/24', '173.0.81.33',
            '173.0.82.75', '173.0.82.91', '173.0.88.210',
            '173.0.89.210', '66.211.171.211', '173.0.82.126',
            '173.0.82.101', '173.0.82.75', '173.0.82.77'
        ])) :
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'dev');



    require(__DIR__ . '/../bmh_app/vendor/autoload.php');
    require(__DIR__ . '/../bmh_app/vendor/yiisoft/yii2/Yii.php');
    require(__DIR__ . '/../bmh_app/common/config/bootstrap.php');
    require(__DIR__ . '/../bmh_app/frontend/config/bootstrap.php');

    $config = yii\helpers\ArrayHelper::merge(
                    require(__DIR__ . '/../bmh_app/common/config/main.php'),
                    //
                    require(__DIR__ . '/../bmh_app/common/config/main-local.php'),
                    //
                    require(__DIR__ . '/../bmh_app/frontend/config/main.php'),
                    //
                    require(__DIR__ . '/../bmh_app/frontend/config/main-local.php')
    );

    $application = new yii\web\Application($config);
    $application->run();
    ?>

<?php else : ?>
    <!doctype html>
    <title>Site Maintenance</title>
    <style>
        body { text-align: center; padding: 150px; }
        h1 { font-size: 50px; }
        body { font: 20px Arial, sans-serif; color: #333; }
        article { display: block; text-align: left; width: 650px; margin: 0 auto; }
        a { color: #dc8100; text-decoration: none; }
        a:hover { color: #333; text-decoration: none; }
    </style>

    <article>
        <h1>We&rsquo;ll be back soon!</h1>
        <div>
            <p>We are currently undergoing scheduled maintanance.</p> 
            <p>The website will be offline until Monday, October 15<sup>th</sup>.</p>
            <p>Thank you for your patience.</p>
            <p>&mdash; BOOKmyHOSTESS.COM - Team.</p>
        </div>
    </article>
<?php endif; ?>