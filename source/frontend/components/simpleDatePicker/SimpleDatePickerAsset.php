<?php

namespace frontend\components\simpleDatePicker;

use yii\web\AssetBundle;

class SimpleDatePickerAsset extends AssetBundle {

    public $sourcePath = '@frontend/components/simpleDatePicker/assets/';
    public $js = [
        'js/date.format.js',
        'js/jquery-dropdate.js'
    ];

}
