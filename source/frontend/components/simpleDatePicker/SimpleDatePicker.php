<?php

namespace frontend\components\simpleDatePicker;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

class SimpleDatePicker extends InputWidget {

    public $clientOptions = [];

    /**
     * Initializes the widget.
     */
    public function init() {

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run() {
        if ($this->hasModel()) {
            echo Html::activeTextInput($this->model, $this->attribute);
        } else {
            echo Html::textInput($this->name, $this->value);
        }
        $this->registerPlugin();
    }

    /**
     * Registers MultiSelect Bootstrap plugin and the related events
     */
    protected function registerPlugin() {
        $view = $this->getView();

        SimpleDatePickerAsset::register($view);

        $id = $this->options['id'];

        $options = $this->clientOptions !== false && !empty($this->clientOptions) ? Json::encode($this->clientOptions) : '';

        $js = "jQuery('#$id').dropdate($options);";
        $view->registerJs($js);
    }

}
