<?php

namespace frontend\components;

use Yii;
use yii\base\Object;
use yii\helpers\Html;
use ML\JsonLD\JsonLD;
use common\models\Blog;

class JsonLDHelper extends Object
{

    public function addOrganizationDoc()
    {
        $doc = [
            "@type" => "http://schema.org/Organization",
            "http://schema.org/email" => "info@bookmyhostess.com",
            "http://schema.org/telephone" => '004915202964261',
            "http://schema.org/faxNumber" => '004922349799839',
            "http://schema.org/foundingLocation" => (object) [
                "@type" => "http://schema.org/Place",
                "http://schema.org/address" => (object) [
                    "@type" => "http://schema.org/PostalAddress",
                    "http://schema.org/addressCountry" => Yii::t('app', 'Germany'),
                    "http://schema.org/addressRegion" => Yii::t('app', 'North Rhine-Westphalia'),
                    "http://schema.org/addressLocality" => Yii::t('app', 'Cologne'),
                    "http://schema.org/postalCode" => 50859,
                    "http://schema.org/streetAddress" => "Ottostr. 7",
                ],
            ],
            "http://schema.org/founder" => (object) [
                "@type" => "http://schema.org/Person",
                "http://schema.org/name" => "Adela Kadiric",
            ],
            "http://schema.org/address" => (object) [
                "@type" => "http://schema.org/PostalAddress",
                "http://schema.org/addressCountry" => Yii::t('app', 'Germany'),
                "http://schema.org/addressRegion" => Yii::t('app', 'North Rhine-Westphalia'),
                "http://schema.org/addressLocality" => Yii::t('app', 'Cologne'),
                "http://schema.org/postalCode" => 50859,
                "http://schema.org/streetAddress" => "Ottostr. 7",
            ],
            "http://schema.org/logo" => (object) [
                "@type" => "http://schema.org/ImageObject",
                "http://schema.org/contentUrl" => Yii::$app->urlManager->createAbsoluteUrl(["images/logo.png"]),
                "http://schema.org/caption" => Yii::t('app', 'BOOKmyHOSTESS'),
                "http://schema.org/description" => Yii::t('app', 'BOOKmyHOSTESS logo'),
                "http://schema.org/width" => (object) [
                    "@type" => "http://schema.org/QuantitativeValue",
                    "http://schema.org/value" => 237
                ],
                "http://schema.org/height" => (object) [
                    "@type" => "http://schema.org/QuantitativeValue",
                    "http://schema.org/value" => 75
                ]
            ],
            "http://schema.org/legalName" => "BookMyHostess.com",
            "http://schema.org/taxID" => "DE 299791123",
            "http://schema.org/url" => "https://www.bookmyhostess.com/"
        ];

        $this->add($doc);
    }

    public function addBlogDoc(Blog $model)
    {
        $doc = [
            "@type" => "http://schema.org/BlogPosting",
            "http://schema.org/mainEntityOfPage" => (object) [
                "@type" => "http://schema.org/WebPage",
                "@id" => Yii::$app->urlManager->createAbsoluteUrl(["//blog/view", "id" => $model->id]),
            ],
            "http://schema.org/headline" => $model->title,
            "http://schema.org/description" => $model->short_content,
            "http://schema.org/articleBody" => $model->content,
            "http://schema.org/datePublished" => $model->date_created,
            "http://schema.org/dateCreated" => $model->date_created,
            "http://schema.org/dateModified" => $model->date_created,
            "http://schema.org/image" => (object) [
                "@type" => "http://schema.org/ImageObject",
                "http://schema.org/url" => Yii::$app->urlManager->createAbsoluteUrl($model->image),
                "http://schema.org/caption" => $model->title
            ],
            "http://schema.org/inLanguage" => (object) [
                "@type" => "http://schema.org/Language",
                "http://schema.org/name" => "German"
            ],
            "http://schema.org/publisher" => (object) [
                "@type" => "http://schema.org/Organization",
                "http://schema.org/name" => "BookMyHostess.com",
                "http://schema.org/logo" => (object) [
                    "@type" => "http://schema.org/ImageObject",
                    "http://schema.org/url" => Yii::$app->urlManager->createAbsoluteUrl(["images/logo.png"]),
                    "http://schema.org/caption" => Yii::t('app', 'BOOKmyHOSTESS'),
                    "http://schema.org/description" => Yii::t('app', 'BOOKmyHOSTESS logo'),
                    "http://schema.org/width" => (object) [
                        "@type" => "http://schema.org/QuantitativeValue",
                        "http://schema.org/value" => 237
                    ],
                    "http://schema.org/height" => (object) [
                        "@type" => "http://schema.org/QuantitativeValue",
                        "http://schema.org/value" => 75
                    ]
                ],
            ],
            "http://schema.org/author" => (object) [
                "@type" => "http://schema.org/Organization",
                "http://schema.org/name" => "BookMyHostess.com",
            ]
        ];

        $this->add($doc);
    }

    public function add($doc)
    {
        $context = (object) [
                    "@context" => (object) ["@vocab" => "http://schema.org/"]
        ];

        $compacted = JsonLD::compact((object) $doc, $context);

        $view = Yii::$app->getView();
        $view->params['jsonld'][] = json_encode(
                $compacted, JSON_PRETTY_PRINT |
                JSON_UNESCAPED_SLASHES |
                JSON_UNESCAPED_UNICODE
        );
    }

    public function registerScripts()
    {
        $view = Yii::$app->getView();

        if (isset($view->params['jsonld']))
        {
            foreach ($view->params['jsonld'] as $jsonld)
            {
                echo Html::script($jsonld, ['type' => 'application/ld+json']);
            }
        }
    }

}
