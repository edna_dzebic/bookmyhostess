<?php

namespace frontend\components;

use yii\web\Controller;
use Yii;

class FrontController extends Controller
{

    const LNG_COOKIE_ACCEPTED = 'prefCookieAgree';
    const LNG_COOKIE_NAME = 'lang';
    const AFFL_FLAG = 'bmhaffl';
    const LNG_FLAG = 'lang';

    public function beforeAction($action)
    {
        $this->checkLanguage();

        $this->checkAffiliate();

        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        Yii::$app->auditLog->log($this->id, Yii::$app->request->get('id'), $action->id);

        return $result;
    }

    private function checkLanguage()
    {
        $lng = Yii::$app->language;

        $lngCookie = Yii::$app->request->cookies->getValue(self::LNG_COOKIE_NAME, false);

        $allowedLngs = array_keys(Yii::$app->params["languages"]);
        if ($lngCookie && in_array($lngCookie, $allowedLngs))
        {
            Yii::$app->language = $lngCookie;
        }

        if (isset(Yii::$app->request->post()[self::LNG_FLAG]) || isset(Yii::$app->request->get()[self::LNG_FLAG]))
        {
            if (isset(Yii::$app->request->post()[self::LNG_FLAG]))
            {
                $lng = Yii::$app->request->post()[self::LNG_FLAG];
            } else
            {
                $lng = Yii::$app->request->get()[self::LNG_FLAG];
            }

            Yii::$app->language = $lng;
        }

        Yii::$app->session->set(self::LNG_FLAG, Yii::$app->language);
    }

    private function checkAffiliate()
    {
        $cookies = Yii::$app->response->cookies;

        if (isset(Yii::$app->request->post()[self::AFFL_FLAG]) || isset(Yii::$app->request->get()[self::AFFL_FLAG]))
        {
            if (isset(Yii::$app->request->post()[self::AFFL_FLAG]))
            {
                Yii::$app->session->set(self::AFFL_FLAG, Yii::$app->request->post()[self::AFFL_FLAG]);
            } else
            {
                Yii::$app->session->set(self::AFFL_FLAG, Yii::$app->request->get()[self::AFFL_FLAG]);
            }

            // add a new cookie to the response to be sent
            $cookies->add(new \yii\web\Cookie([
                'name' => self::AFFL_FLAG,
                'value' => Yii::$app->session->get(self::AFFL_FLAG),
                'expire' => strtotime('+1 year')
            ]));
        } else if ($cookies->has(self::AFFL_FLAG))
        {
            Yii::$app->session->set(self::AFFL_FLAG, $cookies->get(self::AFFL_FLAG)->value);
        }
    }

}
