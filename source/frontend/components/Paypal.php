<?php

namespace frontend\components;

use Yii;

class Paypal extends \yii\base\Model {

    public function ipn($post) {
        $req = $this->buildIpnRequest($post);
        return $this->curl($req);
    }

    private function buildIpnRequest($post) {
        $req = 'cmd=_notify-validate';
        foreach ($post as $key => $value) {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }

        return $req;
    }

    private function curl($req) {
        Yii::info("IN CURL 26");
        $ch = curl_init(Yii::$app->params["paypal"]["url"]);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        if (!($res = curl_exec($ch))) {
            Yii::error(curl_error($ch));
            curl_close($ch);
            Yii::info("IN CURL 38");
            return;
        }
        curl_close($ch);
        Yii::info("IN CURL 43");
        return $res;
    }

}

//IPN message decoded to params	
//[
//    'mc_gross' => '11.00'
//    'protection_eligibility' => 'Ineligible'
//    'payer_id' => 'payer id here'
//    'tax' => '0.00'
//    'payment_date' => '09:26:09 Jan 27, 2016 PST'
//    'payment_status' => 'Completed'
//    'charset' => 'windows-1252'
//    'first_name' => 'Kudces'
//    'mc_fee' => '0.67'
//    'notify_version' => '3.8'
//    'custom' => ''
//    'payer_status' => 'verified'
//    'business' => 'business email here'
//    'quantity' => '1'
//    'verify_sign' => 'verify sign here'
//    'payer_email' => 'payer email here'
//    'txn_id' => '1JU39549YV476112A'
//    'payment_type' => 'instant'
//    'btn_id' => '3264604'
//    'last_name' => 'Klijent'
//    'receiver_email' => 'receiver email here'
//    'payment_fee' => ''
//    'shipping_discount' => '0.00'
//    'insurance_amount' => '0.00'
//    'receiver_id' => 'receiver id here'
//    'txn_type' => 'web_accept'
//    'item_name' => 'charge_credit'
//    'discount' => '0.00'
//    'mc_currency' => 'EUR'
//    'item_number' => ''
//    'residence_country' => 'US'
//    'test_ipn' => '1'
//    'handling_amount' => '0.00'
//    'shipping_method' => 'Default'
//    'transaction_subject' => ''
//    'payment_gross' => ''
//    'shipping' => '0.00'
//    'ipn_track_id' => '32b4296578023'
//]