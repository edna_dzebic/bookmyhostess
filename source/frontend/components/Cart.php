<?php

namespace frontend\components;

use Yii;
use common\models\Invoice;
use common\models\Request;
use common\models\Coupon;
use common\models\ExhibitorProfile;
use yii\helpers\ArrayHelper;

/**
 * Model that handles all cart based operations
 */
class Cart
{

    const CART_SESSION_KEY = "cart";

    /**
     * Reason last action did not succeed
     *
     * @var string
     */
    public $errorMessage = "";

    /**
     * Items in the cart
     * structure :
     *  [
     *      [
     *          'model' => frontend\models\Item
     *      ]
     * ];
     *
     * @var array
     */
    private $_items = [];
    private $_coupon_model = null;
    private $_coupon_code = "";
    private $_payment_type = "";

    /**
     * Array of ids. Used when serializing/unserializing the object
     * @var array
     */
    private $_ids = [];

    /**
     * Private so no instances can be created outside of the class.
     *
     * @see getInstance()
     */
    private function __construct()
    {
        
    }

    /**
     * Returns the instance of this class.
     *
     * @return Cart The cart instance.
     */
    public static function getInstance()
    {
        /* @var $model Cart */
        $model = Yii::$app->session->get(static::CART_SESSION_KEY, false);

        if ($model === false)
        {
            $model = new self();
            Yii::$app->session->set(static::CART_SESSION_KEY, serialize($model));
        } else
        {
            $model = unserialize($model);
            $model->save();
        }

        return $model;
    }

    /**
     * Set selected payment type
     * @param string $type
     */
    public function setPaymentType(ExhibitorProfile $profile, $type)
    {
        if ($this->validatePaymentType($profile, $type))
        {
            $this->_payment_type = $type;
            $this->save();
            return true;
        } else
        {
            $this->errorMessage = $this->getPaymentNotAllowedMessage();
            return false;
        }
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->_payment_type;
    }

    /**
     * Set coupon
     * @param string $code
     * @return boolean
     */
    public function setCoupon($code)
    {
        $model = $this->findCouponModel($code);
        if ($this->validateCoupon($model))
        {
            $this->_coupon_code = $model->code;
            $this->_coupon_model = $model;
            $this->save();
            return true;
        }

        return false;
    }

    /**
     * @return \common\models\Coupon
     */
    public function getCoupon()
    {
        return $this->_coupon_model;
    }

    /**
     * @return string
     */
    public function getCouponCode()
    {
        return $this->_coupon_code;
    }

    /**
     * @return number of items in cart
     */
    public function getItemCount()
    {
        return count($this->_items);
    }

    /**
     * Add new model to items (if it exists)
     *
     * @param $id
     *
     * @return bool
     */
    public function add($id)
    {
        if (!$this->itemsContain($id))
        {
            /* @var $item EventRequest */
            $item = Request::find()
                    ->andWhere(['request.id' => $id])
                    ->andWhere(['request.requested_user_id' => Yii::$app->user->id])
                    ->andWhere(['request.status_id' => Yii::$app->status->request_accept])
                    ->one();

            if ($item)
            {
                $alreadyBooked = Request::find()
                        ->andWhere(['request.event_id' => $item->event_id])
                        ->andWhere(['request.user_id' => $item->user_id])
                        ->andWhere(['request.completed' => Request::COMPLETED])
                        ->exists();

                if (!$alreadyBooked)
                {
                    $this->addNewItem($item);
                    $this->save();

                    return true;
                } else
                {
                    $this->errorMessage = $this->getAlreadyBookedMessage();

                    return false;
                }
            } else
            {
                $this->errorMessage = $this->getRequestNotFoundMessage();

                return false;
            }
        }

        return true;
    }

    /**
     * Remove model with $id from items
     *
     * @param $id
     */
    public function remove($id)
    {
        if ($this->itemsContain($id))
        {
            $this->removeItem($id);
            $this->save();
        }
    }

    /**
     * @return array
     */
    public function getAllModels()
    {
        return ArrayHelper::getColumn($this->_items, 'model');
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function itemsContain($id)
    {
        return $this->indexOf($id) >= 0;
    }

    /**
     * Get selected IDs
     * @return string
     */
    public function getRequestIds()
    {
        $models = $this->getAllModels();
        $ids = [];
        foreach ($models as $model)
        {
            $ids[] = $model->id;
        }
        return implode(",", $ids);
    }

    /**
     * This is netto amount for all requests
     * @return type
     */
    public function getTotalBookingPrice()
    {
        $models = $this->getAllModels();
        $total = 0.0;

        foreach ($models as $model)
        {
            $total += $model->total_booking_price;
        }

        return $total;
    }

    /**
     * Tax amount for invoice ( only Germany 19% )
     * @param type $profileModel
     * @return real
     */
    public function getTaxAmount($profileModel)
    {
        $total = $this->getTotalBookingPrice();
        if (Yii::$app->util->isGerman($profileModel->country_id))
        {
            return 0.19 * $total;
        }

        return 0.0;
    }

    /**
     * This is brutto amount
     *
     * @param type $profileModel
     * @return type
     */
    public function getGrossAmount($profileModel)
    {
        return $this->getTotalBookingPrice() + $this->getTaxAmount($profileModel);
    }

    /**
     * This is how much exhibitors should pay after deducting coupon
     * 
     * @param type $profileModel
     * @return type
     */
    public function getPayPalAmount($profileModel)
    {
        $total = $this->getGrossAmount($profileModel);

        if (isset($this->_coupon_model))
        {
            $total = Yii::$app->util->calculateDiscountFromCoupon($total, $this->_coupon_model);
        }

        return $total;
    }

    /**
     * @param  $profileModel
     * @return float
     */
    public function getCouponNettoAmount($profileModel)
    {
        $gross = $this->getCouponGrossAmount($profileModel);
        $tax = $this->getCouponTaxAmount($gross);
        return $gross - $tax;
    }

    /**
     * @param type $profileModel
     * @return float
     */
    public function getCouponGrossAmount($profileModel)
    {
        return $this->getGrossAmount($profileModel) - $this->getPayPalAmount($profileModel);
    }

    /**
     * @param type $value
     * @return float
     */
    public function getCouponTaxAmount($value)
    {
        return $value * 19 / 119;
    }

    /**
     * Magic function that is called when serializing the model
     * Used to replace the model with it's id to reduce the serialized string length
     *
     * Private so it can only be serialized from inside the class itself
     *
     * @return array of attributes to serialize
     */
    private function __sleep()
    {
        $ids = [];

        foreach ($this->_items as $item)
        {
            $ids[] = $item['model']->id;
        }

        $this->_ids = $ids;

        return ['_ids', '_coupon_code', '_payment_type'];
    }

    /**
     * Magic function that is called when unserializing the model
     * Used to convert the ids that were set in __sleep to models
     *
     */
    private function __wakeup()
    {
        $items = [];
        $ids = [];

        foreach ($this->_ids as $id)
        {
            $ids[] = $id;
        }

        $itemModels = Request::find()->andWhere(['id' => $ids])->all();

        foreach ($this->_ids as $id)
        {
            $tempItem['model'] = $this->findModelById($itemModels, $id);

            if ($tempItem['model'] !== null)
            {
                $items[] = $tempItem;
            }
        }

        if (!empty($this->_coupon_code))
        {
            $couponModel = $this->findCouponModel($this->_coupon_code);
            if ($this->validateCoupon($couponModel))
            {
                $this->_coupon_model = $couponModel;
            } else
            {
                $this->_coupon_code = "";
            }
        }

        $this->_items = $items;
    }

    /**
     * Save the current serialized model into session
     */
    private function save()
    {
        Yii::$app->session->set(static::CART_SESSION_KEY, serialize($this));
    }

    /**
     * @param $models
     * @param $id
     *
     * @return object|null
     */
    private function findModelById($models, $id)
    {
        foreach ($models as $model)
        {
            if ($model->id === $id)
            {
                return $model;
            }
        }

        return null;
    }

    /**
     * @param $item
     */
    private function addNewItem($item)
    {
        $this->_items[] = [
            'model' => $item
        ];
    }

    /**
     * @param $id
     */
    private function removeItem($id)
    {
        unset($this->_items[$this->indexOf($id)]);
    }

    /**
     * @param $id
     *
     * @return int
     */
    private function indexOf($id)
    {
        $count = count($this->_items);
        for ($i = 0; $i < $count; $i++)
        {
            if ($this->_items[$i]['model']->id == $id)
            {
                return $i;
            }
        }

        return -1;
    }

    /**
     * @return string
     */
    private function getAlreadyBookedMessage()
    {
        return Yii::t('app.exhibitor', 'Already booked');
    }

    /**
     * @return string
     */
    private function getRequestNotFoundMessage()
    {
        return Yii::t('app.exhibitor', 'Request not found');
    }

    /**
     * @return string
     */
    private function getCouponNotYetActiveMessage()
    {
        return Yii::t('app.exhibitor', 'The coupon is still not valid. Please check dates.');
    }

    /**
     * @return string
     */
    private function getCouponExpiredMessage()
    {
        return Yii::t('app.exhibitor', 'The coupon is already expired. Please check dates.');
    }

    /**
     * @return string
     */
    private function getCouponNotFoundMessage()
    {
        return Yii::t('app.exhibitor', 'The coupon does not exist!');
    }

    /**
     * @return string
     */
    private function getPaymentNotAllowedMessage()
    {
        return Yii::t('app.exhibitor', 'Chosen payment type not allowed!');
    }

    /**
     * @return string
     */
    private function getCouponAlreadyUsed()
    {
        return Yii::t('app.exhibitor', 'You have already used this coupon!');
    }

    /**
     * Validate coupon
     * @param \common\models\Coupon $coupon
     * @return boolean
     */
    private function validateCoupon($coupon = null)
    {
        if (isset($coupon))
        {
            // if coupon is already used by user
            $invoice = Invoice::findOne([
                        "requested_user_id" => Yii::$app->user->id,
                        "coupon_code" => $coupon->code
            ]);

            if (is_object($invoice))
            {
                $this->errorMessage = $this->getCouponAlreadyUsed();
                return false;
            }

            if (isset($coupon->date_from))
            {
                // if coupon has not yet started
                if (date("Y-m-d", strtotime($coupon->date_from)) > date("Y-m-d"))
                {
                    $this->errorMessage = $this->getCouponNotYetActiveMessage();
                    return false;
                }
            }

            if (isset($coupon->date_to))
            {
                // if coupon end date is past
                if (date("Y-m-d", strtotime($coupon->date_to)) < date("Y-m-d"))
                {
                    $this->errorMessage = $this->getCouponExpiredMessage();
                    return false;
                }
            }

            return true;
        } else
        {
            $this->errorMessage = $this->getCouponNotFoundMessage();
            return false;
        }
    }

    /**
     * @param ExhibitorProfile $profileModel
     * @param string $payment
     * @return boolean
     */
    private function validatePaymentType(ExhibitorProfile $profileModel, $payment)
    {
        return in_array($payment, $profileModel->getAllowedPaymentsForExhibitor());
    }

    /**
     * Find coupon by code
     * @param string $code
     * @return \common\models\Coupon
     */
    private function findCouponModel($code)
    {
        return Coupon::findOne(["code" => $code]);
    }

}
