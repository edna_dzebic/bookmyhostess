<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'),
        //
        require(__DIR__ . '/../../common/config/params-local.php'),
        //
        require(__DIR__ . '/params.php'),
        //
        require(__DIR__ . '/params-local.php')
        //
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'api' => [
            'class' => '\frontend\modules\api\Module'
        ],
    ],
    'components' => [
        'jsonld' => [
            'class' => '\frontend\components\JsonLDHelper'
        ],
        'user' => [
            'identityClass' => 'common\models\User', // User must implement the IdentityInterface
            'enableAutoLogin' => true,
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'forceCopy' => true
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => array(
                'find-hostess' => 'hostess/index',
                'upcoming-events' => 'event/index',
                'page/<seo>' => 'page/view',
                'login' => 'site/login',
                'logout' => 'site/logout',
                'blog' => 'blog/index',
                'register' => 'site/register',
                'contact' => 'site/contact',
                'hostess/event-request' => 'hostess/requests',
                'hostess/current-request' => 'hostess/requests',
                'hostess/past-request' => 'hostess/requests',
                'exhibitor/requests' => 'exhibitor/event-request',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
