<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'tasC8-5qU_mr9uifTDAgWhES3nb6Xi27',
        ]
    ],
];

if (YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1','2a02:27b0:420b:9830:2216:d8ff:feb6:ffc', '77.189.145.151']
    ];
}

return $config;