<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "register".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $event_id
 * @property integer $price
 * @property string $date_created
 * @property integer $status_id
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property Event $event
 * @property User $user
 */
class Register extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'register';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'event_id', 'price'], 'required'],
            [['user_id', 'event_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'userFullName' => Yii::t('admin', 'User'),
            'userUserName' => Yii::t('admin', 'Username'),
            'userFirstName' => Yii::t('admin', 'First name'),
            'userLastName' => Yii::t('admin', 'Last name'),
            'userEmail' => Yii::t('admin', 'Email'),
            'userPhone' => Yii::t('admin', 'Phone'),
            'event_id' => Yii::t('admin', 'Event'),
            'eventName' => Yii::t('admin', 'Event'),
            'price' => Yii::t('admin', 'Price')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessProfile()
    {
        return $this->hasOne(HostessProfile::className(), ['user_id' => 'user_id']);
    }

    /**
     * 
     * @return string
     */
    public function getEventName()
    {
        if (is_object($this->event))
        {
            return $this->event->name;
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getUserFullName()
    {
        if (is_object($this->user))
        {
            return $this->user->getFullName();
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getUserUserName()
    {
        if (is_object($this->user))
        {
            return $this->user->username;
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getUserFirstName()
    {
        if (is_object($this->user))
        {
            return $this->user->first_name;
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getUserLastName()
    {
        if (is_object($this->user))
        {
            return $this->user->last_name;
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getUserEmail()
    {
        if (is_object($this->user))
        {
            return $this->user->email;
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getUserPhone()
    {
        if (is_object($this->user))
        {
            return $this->user->phone_number;
        }

        return '';
    }

    /**
     * Return eventAvailibility as array
     * @param int $userID
     * @return array
     */
    public static function getAllForHostess($userID)
    {
        $eventAvailability = static::find()
                ->joinWith("event")
                ->andWhere(["register.user_id" => $userID])
                ->andWhere("event.end_date>=NOW()")
                ->orderBy("event.start_date")
                ->all();

        $availabilityData = [];
        foreach ($eventAvailability as $ev)
        {
            $availabilityData[$ev->event->id] = $ev->event->name;
        }

        return [$eventAvailability, $availabilityData];
    }

}
