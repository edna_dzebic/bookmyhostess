<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "request_job".
 *
 * @property integer $id
 * @property integer $event_request_id
 * @property integer $event_job
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property Job $eventJob
 * @property Request $eventRequest
 */
class RequestJob extends \common\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_job';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_request_id', 'event_job'], 'required'],
            [['event_request_id', 'event_job', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'event_request_id' => Yii::t('admin', 'Event Request ID'),
                'event_job' => Yii::t('admin', 'Event Job')
            ]
        );
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'event_job']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'event_request_id']);
    }


}
