<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "gallery".
 *
 * @property integer $id
 * @property string $path
 * @property integer $include_on_home
 * @property integer $sort_order
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $date_created
 * @property string $date_updated
 * @property integer $status_id
 * @property string $file_name
 * @property string $alt
 * @property string $title
 *
 * @property Status $status
 * @property User $createdBy
 * @property User $updatedBy
 */
class Gallery extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name', 'path'], 'unique'],
            [['file_name', 'alt', 'path', 'title'], 'required'],
            [['path', 'file_name', 'alt'], 'string'],
            [['include_on_home', 'sort_order', 'created_by', 'updated_by', 'status_id'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['path'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'alt' => Yii::t('admin', 'Alt'),
            'title' => Yii::t('admin', 'Title'),
            'file_name' => Yii::t('admin', 'Filename'),
            'path' => Yii::t('admin', 'Path'),
            'include_on_home' => Yii::t('admin', 'Include On Home'),
            'sort_order' => Yii::t('admin', 'Sort Order')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

}
