<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "user".
 *
 * @property integer $id
 * @property string $old_user_id
 * @property string $role
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $date_created
 * @property string $date_updated
 * @property string $birth_date
 * @property string $last_login
 * @property integer $status_id
 * @property string $email_validation_secret
 * @property integer $request_password_reset
 * @property string $lang
 * @property string $gender
 * @property string $activated_at
 * @property integer $updated_by
 * 
 * @property ExhibitorProfile $exhibitorProfile
 * @property NotificationToken[] $notificationTokens
 * @property HostessProfile $hostessProfile
 */
class User extends \common\components\Model implements \yii\web\IdentityInterface
{

    //fixed user roles 
    const ROLE_HOSTESS = "hostes";
    const ROLE_EXHIBITOR = "exhibitor";
    const ROLE_ADMIN = "admin";
    const ROLE_SYS_ADMIN = "sys_admin";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'username'], 'required'],
            [['email'], 'email'],
            [['email', 'username'], 'unique'],
            [['role'], 'required'],
            [['date_created', 'date_updated', 'birth_date', 'last_login', 'activated_at'], 'safe'],
            [['status_id', 'request_password_reset'], 'integer'],
            [['old_user_id', 'username', 'password_hash', 'password_reset_token', 'auth_key', 'email', 'lang', 'gender'], 'string', 'max' => 255],
            [['role', 'first_name', 'last_name', 'phone_number'], 'string', 'max' => 50],
            [['email_validation_secret'], 'string', 'max' => 150],
            [['old_user_id', 'username', 'password_hash', 'password_reset_token', 'auth_key', 'email', 'lang', 'gender', 'role', 'first_name', 'last_name', 'phone_number', 'email_validation_secret'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'old_user_id' => Yii::t('admin', 'Old User ID'),
            'role' => Yii::t('admin', 'Role'),
            'username' => Yii::t('admin', 'Username'),
            'password_hash' => Yii::t('admin', 'Password Hash'),
            'password_reset_token' => Yii::t('admin', 'Password Reset Token'),
            'auth_key' => Yii::t('admin', 'Auth Key'),
            'email' => Yii::t('admin', 'Email'),
            'first_name' => Yii::t('admin', 'First Name'),
            'last_name' => Yii::t('admin', 'Last Name'),
            'phone_number' => Yii::t('admin', 'Phone Number'),
            'birth_date' => Yii::t('admin', 'Birth Date'),
            'last_login' => Yii::t('admin', 'Last Login'),
            'email_validation_secret' => Yii::t('admin', 'Email Validation Secret'),
            'request_password_reset' => Yii::t('admin', 'Request Password Reset'),
            'lang' => Yii::t('admin', 'Language'),
            'gender' => Yii::t('admin', 'Gender'),
            'activated_at' => Yii::t('admin', 'Activated At')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessProfile()
    {
        return $this->hasOne(HostessProfile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExhibitorProfile()
    {
        return $this->hasOne(ExhibitorProfile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationTokens()
    {
        return $this->hasMany(NotificationToken::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status_id' => Yii::$app->status->active]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(["auth_key" => $token, 'status_id' => Yii::$app->status->active]);
    }

    /**
     * @inheritdoc
     */
    public static function findHostessByAccessToken($token, $type = null)
    {
        return static::findOne(["auth_key" => $token, 'status_id' => Yii::$app->status->active, 'role' => self::ROLE_HOSTESS]);
    }

    /**
     * Find hostess by username
     * @param string $username
     * @return User
     */
    public static function findHostessByUsername($username)
    {
        return static::findOne([
                    'username' => $username,
                    'status_id' => Yii::$app->status->active,
                    'role' => self::ROLE_HOSTESS
        ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token))
        {
            return null;
        }

        return static::findOne(
                        [
                            'password_reset_token' => $token,
                            'status_id' => Yii::$app->status->active,
                        ]
        );
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token))
        {
            return false;
        }

        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Get role
     * @return string
     */
    public function getUserRole()
    {
        return $this->role;
    }

    /**
     * Return url for password reset
     * @return string
     */
    public function getPasswordResetLink()
    {
        return \yii\helpers\Url::to(["//site/reset-password", "token" => $this->email_validation_secret, "lang" => Yii::$app->language], true);
    }

    /**
     * Check if the user has the role hostes
     * @return boolean
     */
    public function getIsHostess()
    {
        return $this->role === static::ROLE_HOSTESS;
    }

    /**
     * Check if the user has the role exhibitor (or admin)
     * Needed because the admin should be able to do everything the exhibitor can on frontend
     * Without you would have to add multiple checks to every if statement
     * @return boolean
     */
    public function getIsExhibitor()
    {
        return $this->role === static::ROLE_EXHIBITOR || $this->role === static::ROLE_ADMIN;
    }

    /**
     * Is user administrator
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->role === static::ROLE_ADMIN;
    }

    /**
     * Full name
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getLangValue()
    {

        if (isset($this->lang))
        {
            if (isset(Yii::$app->params['languages'][$this->lang]))
            {
                return Yii::$app->params['languages'][$this->lang];
            }
        }
        return '';
    }

    // not sure if this is used anywhere, do not delete!
    public function getTokens()
    {
        $notificationTokens = $this->notificationTokens;

        $result = [];

        if (!empty($notificationTokens))
        {
            foreach ($notificationTokens as $token)
            {
                $result[] = $token->token;
            }
        }

        return $result;
    }

    /**
     * Get list of iOS tokens for user
     * @return array
     */
    public function getIOSTokens()
    {
        return $this->getFilteredTokens(NotificationToken::IOS_TYPE);
    }

    /**
     * Get list of andorid tokens for user
     * @return array
     */
    public function getAndroidTokens()
    {
        return $this->getFilteredTokens(NotificationToken::ANDROID_TYPE);
    }

    /**
     * @inheritdoc
     */
    public static function findByUsername($username)
    {
        return static::findByUsernameOrEmail($username);
    }

    /**
     * Find user that match the username or email, and add a role and status filter.
     * The roles are determined by the allowedRoles function
     * Should only be used by functions from the identity interface
     * 
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findByUsernameOrEmail($value)
    {
        return static::find()
                        ->andWhere(['username' => $value])
                        ->orWhere(['email' => $value])
                        ->andWhere(['status_id' => Yii::$app->status->active])
                        ->andWhere(['role' => static::allowedRoles()])
                        ->one();
    }

    /**
     * Get status label
     * @return string
     */
    public function getStatusLabel()
    {
        return is_object($this->status) ? $this->status->label : "";
    }

    /**
     * Return list of filtered tokens by type
     * @param string $type
     * @return array
     */
    private function getFilteredTokens($type)
    {
        $notificationTokens = $this->notificationTokens;

        $result = [];

        if (!empty($notificationTokens))
        {
            foreach ($notificationTokens as $token)
            {
                if ($token->os_type == $type)
                {
                    $result[] = $token->token;
                }
            }
        }

        return $result;
    }

    /**
     * By default allowed roles should be empty
     * @return array
     */
    protected static function allowedRoles()
    {
        return [];
    }

    /**
     * Activate profile
     * @return boolean
     */
    public function activate()
    {
        $this->activated_at = date("Y-m-d H:i:s");
        $this->status_id = Yii::$app->status->active;

        return $this->update() !== false;
    }

}
