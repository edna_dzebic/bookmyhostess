<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the generated model class for table "status".
 *
 * @property integer $id
 * @property string $name
 * @property string $label
 * @property string $updated_by
 * @property string $updated_at
 *
 * @property User[] $users
 * @property User $updatedBy
 */
class Status extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => $this->getBeforeInsertAttrib(),
                    ActiveRecord::EVENT_BEFORE_UPDATE => $this->getBeforeUpdateAttrib(),
                ],
                'value' => function ()
        {
            return $this->getCurrentDateTime();
        }
            ],
            'blameable' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'createdByAttribute' => false,
                'updatedByAttribute' => "updated_by",
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_updated', 'updated_by'], 'safe'],
            [['updated_by'], 'integer'],
            [['name', 'label'], 'required'],
            [['name', 'label'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['label', 'name'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'name' => Yii::t('admin', 'Name'),
            'label' => Yii::t('admin', 'Label')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

}
