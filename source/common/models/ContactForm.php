<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "contact_form".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $url
 * @property string $message
 * @property string $subject
 * @property string $captcha
 * @property integer $dsgvo
 */
class ContactForm extends \common\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_form';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'message', 'captcha', 'dsgvo'], 'required'],
            [['message'], 'string'],
            [['dsgvo'], 'integer'],
            [['name', 'email', 'url', 'subject', 'captcha'], 'string', 'max' => 255],
            [['message'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            [['name', 'email', 'url', 'subject', 'captcha'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'id' => Yii::t('app', 'ID'),
                'name' => Yii::t('app', 'Name'),
                'email' => Yii::t('app', 'Email'),
                'url' => Yii::t('app', 'Url'),
                'message' => Yii::t('app', 'Message'),
                'subject' => Yii::t('app', 'Subject'),
                'captcha' => Yii::t('app', 'Captcha'),
                'dsgvo' => Yii::t('app', 'Dsgvo'),
            ]
        );
    }



}
