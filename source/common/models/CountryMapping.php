<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "country_mapping".
 *
 * @property integer $id
 * @property integer $source_country_id
 * @property integer $target_country_id
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Country $targetCountry
 * @property User $createdBy
 * @property Country $sourceCountry
 * @property Status $status
 * @property User $updatedBy
 */
class CountryMapping extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country_mapping';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_country_id', 'target_country_id'], 'required'],
            [['source_country_id', 'target_country_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'source_country_id' => Yii::t('admin', 'Source Country'),
            'target_country_id' => Yii::t('admin', 'Target Country'),
            'sourceCountryName' => Yii::t('admin', 'Source Country'),
            'targetCountryName' => Yii::t('admin', 'Target Country'),
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTargetCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'target_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'source_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return string
     */
    public function getSourceCountryName()
    {
        return $this->sourceCountry->name;
    }

    /**
     * @return string
     */
    public function getTargetCountryName()
    {
        return $this->targetCountry->name;
    }

}
