<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "trade_license_extend".
 *
 * @property integer $id
 * @property integer $hostess_profile_id
 * @property string $date
 * @property string $reason
 * @property string $date_created
 * @property string $date_updated
 * @property integer $status_id
 * @property integer $updated_by
 *
 * @property Status $status
 * @property HostessProfile $hostessProfile
 * @property User $updatedBy
 */
class TradeLicenseExtend extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trade_license_extend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hostess_profile_id', 'date', 'reason'], 'required'],
            [['hostess_profile_id', 'status_id', 'updated_by'], 'integer'],
            [['date', 'date_created', 'date_updated'], 'safe'],
            [['reason'], 'string'],
            [['reason'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'hostess_profile_id' => Yii::t('admin', 'Hostess Profile'),
            'date' => Yii::t('admin', 'Date'),
            'reason' => Yii::t('admin', 'Reason')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessProfile()
    {
        return $this->hasOne(HostessProfile::className(), ['id' => 'hostess_profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

}
