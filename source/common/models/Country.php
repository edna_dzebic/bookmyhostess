<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "country".
 *
 * @property integer $id
 * @property string $name
 * @property integer $invoice_type
 * @property string $currency_label
 * @property string $currency_symbol
 * @property string $booking_price
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $is_eu
 * @property integer $is_invoice_payment_allowed
 *
 * @property City[] $cities
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property CountryPrice[] $countryPrices
 * @property Event[] $events
 * @property ExhibitorProfile[] $exhibitorProfiles
 * @property HostessProfile[] $hostessProfiles
 * @property User[] $users
 * @property CountryMapping[] $sourceMappings
 * @property CountryMapping[] $targetMappings
 * @property HostessCountryMapping[] $hostessCountryMappings
 */
class Country extends \common\components\Model
{

    const INVOICE_EU = 1;
    const INVOICE_DE = 2;
    const INVOCE_OTHER = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'currency_label', 'currency_symbol', 'booking_price', 'is_eu', 'is_invoice_payment_allowed'], 'required'],
            [['invoice_type', 'status_id', 'created_by', 'updated_by', 'is_eu'], 'integer'],
            [['booking_price'], 'number'],
            [['date_created', 'date_updated'], 'safe'],
            [['name', 'currency_label', 'currency_symbol'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'name' => Yii::t('admin', 'Name'),
            'translatedName' => Yii::t('admin', 'Name'),
            'invoice_type' => Yii::t('admin', 'Invoice Type'),
            'currency_label' => Yii::t('admin', 'Currency Label'),
            'currency_symbol' => Yii::t('admin', 'Currency Symbol'),
            'booking_price' => Yii::t('admin', 'Booking Price'),
            'is_eu' => Yii::t('admin', 'Is EU member'),
            'is_invoice_payment_allowed' => Yii::t('admin', 'Is payment on invoice allowed')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountryPrices()
    {
        return $this->hasMany(CountryPrice::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExhibitorProfiles()
    {
        return $this->hasMany(ExhibitorProfile::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessProfiles()
    {
        return $this->hasMany(HostessProfile::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTargetMappings()
    {
        return $this->hasMany(CountryMapping::className(), ['target_country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceMappings()
    {
        return $this->hasMany(CountryMapping::className(), ['source_country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessCountryMappings()
    {
        return $this->hasMany(HostessCountryMapping::className(), ['country_id' => 'id']);
    }

    /**
     * Get mapped translated list of countries
     * @return array
     */
    public static function getListForDropdown()
    {
        $models = static::find()->all();

        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->getTranslatedName();
        }

        asort($result);

        return $result;
    }

    /**
     * Is invoice type Other ( not EU and not Germany )
     * @return boolean
     */
    public function getIsOtherInvoice()
    {
        return $this->invoice_type == static::INVOCE_OTHER;
    }

    /**
     * Is invoice type Germany
     * @return boolean
     */
    public function getIsDeInvoice()
    {
        return $this->invoice_type == static::INVOICE_DE;
    }

    /**
     * Is invoice type EU
     * @return boolean
     */
    public function getIsEuInvoice()
    {
        return $this->invoice_type == static::INVOICE_EU;
    }

    /**
     * Get translated name
     * @return string
     */
    public function getTranslatedName()
    {
        return Yii::t('app.countries', $this->name);
    }

}
