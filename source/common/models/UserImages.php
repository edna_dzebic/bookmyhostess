<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "user_images".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $image_url
 * @property string $name
 * @property integer $is_default
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property User $user
 */
class UserImages extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'image_url'], 'required'],
            [['user_id', 'is_default', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['image_url'], 'image', 'on' => 'upload', 'maxSize' => 1024 * 1024 * 5, 'skipOnEmpty' => false, 'tooBig' => Yii::t('app.hostess', 'The file "{file}" is too big. Its size cannot exceed 5MB.')],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'image_url' => Yii::t('admin', 'Image Url'),
            'name' => Yii::t('admin', 'Name'),
            'is_default' => Yii::t('admin', 'Is Default')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Returns cached image of hostess 
     * Used on views for easy listing 
     * 
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getCachedImage($width, $height)
    {
        return \common\components\PhpThumb::Url([
                    'src' => $this->getImagePath(true),
                    'w' => $width,
                    'h' => $height,
                    'zc' => 'T',
                    'q' => 90,
        ]);
    }

    /**
     * Retruns full path to image, can be easily used on views
     * 
     * Set failSafe to true:
     * If image is not available at all, will return default image of system like "No image found" etc
     * @return mixed String if image is found or failsafe is on, false if image is not available and failsafe is off
     */
    public function getImagePath($fullPath = false, $failSafe = null)
    {
        if ($this->image_url != null)
        {
            return \Yii::$app->params['user_images_url'] . $this->image_url;
        }

        if ($failSafe === true)
        {
            $baseUrl = $fullPath != false ? \Yii::$app->urlManagerFrontend->baseUrl : '';
            return $baseUrl . \Yii::$app->params['default_image'];
        }

        return false;
    }

    public function saveImage()
    {
        if ($this->validate())
        {
            $filename = sha1(microtime()) . '.' . $this->image_url->extension;
            $path = Yii::$app->params['uploads_save_path'] . $filename;
            $this->image_url->saveAs($path);
            $this->image_url = $filename;

            // false because Yii validation of images
            return $this->save(false);
        }

        return false;
    }

}
