<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "hostess_past_work".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $start_date
 * @property string $end_date
 * @property string $event_name
 * @property string $job_description
 * @property string $position
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property User $user
 */
class HostessPastWork extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hostess_past_work';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['start_date', 'end_date', 'date_created', 'date_updated'], 'safe'],
            [['event_name', 'job_description', 'position'], 'string', 'max' => 255],
            [['event_name', 'job_description', 'position'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            ['start_date', 'compare', 'compareAttribute' => 'end_date', 'operator' => '<=', 'enableClientValidation' => false],
            [['job_description', 'position'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'start_date' => Yii::t('admin', 'Start Date'),
            'end_date' => Yii::t('admin', 'End Date'),
            'event_name' => Yii::t('admin', 'Event Name'),
            'job_description' => Yii::t('admin', 'Job Description'),
            'position' => Yii::t('admin', 'Position')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
