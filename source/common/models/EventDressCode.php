<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "event_dress_code".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $event_dress_code_id
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property Event $event
 * @property DressCode $eventDressCode
 */
class EventDressCode extends \common\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_dress_code';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'event_dress_code_id'], 'required'],
            [['event_id', 'event_dress_code_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['event_dress_code_id'], 'exist', 'skipOnError' => true, 'targetClass' => DressCode::className(), 'targetAttribute' => ['event_dress_code_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'event_id' => Yii::t('admin', 'Event'),
                'event_dress_code_id' => Yii::t('admin', 'Event Dress Code')
            ]
        );
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDressCode()
    {
        return $this->hasOne(DressCode::className(), ['id' => 'event_dress_code_id']);
    }


}
