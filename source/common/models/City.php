<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "city".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property Country $country
 * @property Event[] $events
 * @property HostessPreferredCity[] $hostessPreferredCities
 * @property User[] $users
 */
class City extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name'], 'required'],
            [['country_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'country_id' => Yii::t('admin', 'Country'),
            'countryName' => Yii::t('admin', 'Country'),
            'translatedCountryName' => Yii::t('admin', 'Country'),
            'name' => Yii::t('admin', 'Name'),
            'translatedName' => Yii::t('admin', 'Name')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessPreferredCities()
    {
        return $this->hasMany(HostessPreferredCity::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['city_id' => 'id']);
    }

    /**
     * Get country name
     * @return string
     */
    public function getCountryName()
    {
        return is_object($this->country) ? $this->country->name : '';
    }

    /**
     * Get translated name
     * @return string
     */
    public function getTranslatedName()
    {
        return Yii::t('app.cities', $this->name);
    }

    /**
     * Get translated country name
     * @return string
     */
    public function getTranslatedCountryName()
    {
        return is_object($this->country) ? $this->country->getTranslatedName() : '';
    }

}
