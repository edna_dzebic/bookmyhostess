<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invoice".
 *
 * @property integer $id
 * @property integer $requested_user_id
 * @property double $amount
 * @property integer $status
 * @property string $date_created
 * @property string $filename
 * @property string $coupon_code
 * @property double $coupon_amount
 * @property integer $coupon_id
 * @property integer $coupon_type
 * @property integer $is_exhibitor_german
 * @property integer $invoice_type
 * @property integer $exhibitor_booking_type
 * @property string $language
 * @property string $exhibitor_company_name
 * @property string $exhibitor_street
 * @property string $exhibitor_postal_code
 * @property string $exhibitor_city
 * @property string $exhibitor_country
 * @property string $exhibitor_tax_number
 * @propery double $coupon_netto_amount
 * @propery double $coupon_tax_amount
 * @propery double $coupon_gross_amount
 * @property string $payment_type 
 * @property double $net_amount
 * @property double $tax_amount
 * @property double $gross_amount
 * @property double $total_price_with_discount 
 * @property integer $is_reminder_allowed
 * 
 * @property Request[] $eventRequests
 * @property InvoiceItem[] $invoiceItems
 * @property InvoiceReminder[] $invoiceReminders
 * @property Coupon $coupon
 * @property User $userRequested
 */
class Invoice extends \yii\db\ActiveRecord
{

    const STATUS_PAID = 1;
    const STATUS_NOT_PAID = 2;
    const PAYMENT_TYPE_INVOICE = 'invoice';
    const PAYMENT_TYPE_PAYPAL = 'paypal';
    const PAYMENT_TYPE_RECEIPT = 'receipt';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requested_user_id', 'date_created', 'exhibitor_booking_type', 'payment_type'], 'required'],
            [['requested_user_id', 'status', 'coupon_id', 'is_reminder_allowed'], 'integer'],
            [['date_created', 'paypal_transaction_id', 'user_id', 'requested_user_id', 'amount', 'real_invoice_id', 'filename', 'coupon_code', 'coupon_id', 'coupon_amount'], 'safe'],
            [['coupon_type', 'is_exhibitor_german', 'is_exhibitor_eu', 'exhibitor_company_name', 'exhibitor_street', 'exhibitor_postal_code', 'exhibitor_city', 'exhibitor_country', 'exhibitor_tax_number', 'net_amount', 'tax_amount', 'gross_amount', 'total_price_with_discount', 'language'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'requested_user_id' => Yii::t('admin', 'Exhibitor'),
            'exhibitorFullName' => Yii::t('admin', 'Exhibitor'),
            'amount' => Yii::t('admin', 'Amount'),
            'status' => Yii::t('admin', 'Payment Status'),
            'date_created' => Yii::t('admin', 'Date created'),
            'date_paid' => Yii::t('admin', 'Payment Date'),
            'number' => Yii::t('admin', 'Invoice Number'),
            'filename' => Yii::t('admin', 'File'),
            'coupon_id' => Yii::t('admin', 'Coupon'),
            'coupon_code' => Yii::t('admin', 'Coupon Code'),
            'real_invoice_id' => Yii::t('admin', 'Real number'),
            'realNumberLabel' => Yii::t('admin', 'Number'),
            'paypal_transaction_id' => Yii::t('admin', 'Paypal Transaction'),
            'coupon_amount' => Yii::t('admin', 'Coupon Amount'),
            'coupon_type' => Yii::t('admin', 'Coupon Type'),
            'couponTypeLabel' => Yii::t('admin', 'Coupon Type'),
            'invoice_type' => Yii::t('admin', 'Invoice Type'),
            'invoiceTypeLabel' => Yii::t('admin', 'Invoice Type'),
            'payment_type' => Yii::t('admin', 'Payment Type'),
            'paymentTypeLabel' => Yii::t('admin', 'Payment Type'),
            'paymentStatusLabel' => Yii::t('admin', 'Payment Status'),
            'is_exhibitor_german' => Yii::t('admin', 'Is client in Germany'),
            'isExhibitorInGermanyLabel' => Yii::t('admin', 'Is client in Germany'),
            'is_reminder_allowed' => Yii::t('admin', 'Is reminder allowed'),
            'isReminderAllowedLabel' => Yii::t('admin', 'Is reminder allowed'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoupon()
    {
        return $this->hasOne(Coupon::className(), ['id' => 'coupon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRequests()
    {
        return $this->hasMany(Request::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceItems()
    {
        return $this->hasMany(InvoiceItem::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceReminders()
    {
        return $this->hasMany(InvoiceReminder::className(), ['invoice_id' => 'id']);
    }

    /**
     * Exhibitor
     * @return \yii\db\ActiveQuery
     */
    public function getUserRequested()
    {
        return $this->hasOne(User::className(), ['id' => 'requested_user_id']);
    }

    /**
     * Get number of invoices for currrent year
     * @return int
     */
    public function getThisYearCount()
    {
        return Invoice::find()
                        ->where("YEAR(date_created)=YEAR(CURDATE())")
                        ->andWhere("exhibitor_booking_type <> " . ExhibitorProfile::BOOKING_TYPE_FREE)
                        ->count();
    }

    /**
     * Calculate real number of invoice
     */
    public function setNewRealId()
    {
        $count = $this->getThisYearCount();

        $this->real_invoice_id = $count + Yii::$app->settings->invoice_offset + Yii::$app->settings->manual_invoice_count + 1;
    }

    /**
     * Format real number for pdf invoice
     * @return string
     */
    public function getRealIdText()
    {
        $date1 = $this->date_created;
        $date2 = "2020-01-21";
        $dateTimestamp1 = strtotime($date1);
        $dateTimestamp2 = strtotime($date2);

        /**
         * From 20.01.2020. the "REAL INVOICE ID" is obsolete!
         * Therefore, usage of the ID is required!
         * 
         */
        if ($dateTimestamp1 > $dateTimestamp2)
        {
            return $this->id . '/' . date("Y", strtotime($this->date_created));
        }

        if ($this->getIsNormalInvoice())
        {
            return $this->real_invoice_id . '/' . date("Y", strtotime($this->date_created));
        }

        return '';
    }

    /**
     * Send notification email to exhibitor
     * @param string $invoiceFileName
     */
    public function sendNotificationEmails($invoiceFileName)
    {
        Email::sendInvoiceEmailToExibitor($this, $invoiceFileName);
    }

    /**
     * Calculate total booking price
     * @return double
     */
    public function getTotalBookingPrice()
    {
        $models = $this->eventRequests;
        $total = 0.0;

        foreach ($models as $model)
        {
            $total += $model->total_booking_price;
        }

        return $total;
    }

    /**
     * Calculate invoice tax amount
     * @return double
     */
    public function getTaxAmount()
    {
        $profileModel = $this->userRequested->exhibitorProfile;
        $total = $this->getTotalBookingPrice();

        if (Yii::$app->util->isGerman($profileModel->country_id))
        {
            return 0.19 * $total;
        }

        return 0.0;
    }

    /**
     * Calculate invoice gross amount
     * @return double
     */
    public function getGrossAmount()
    {
        $profileModel = $this->userRequested->exhibitorProfile;
        return $this->getTotalBookingPrice() + $this->getTaxAmount($profileModel);
    }

    /**
     * Calculate final amount which should be paid
     * @return double
     */
    public function getPayPalAmount()
    {
        $total = $this->getGrossAmount();

        if (isset($this->coupon))
        {
            $total = Yii::$app->util->calculateDiscountFromCoupon($total, $this->coupon);
        }

        return $total;
    }

    /**
     * Calculate coupon netto value
     * @return double
     */
    public function getCouponNettoAmount()
    {
        $gross = $this->getCouponGrossAmount();
        $tax = $this->getCouponTaxAmount($gross);
        return $gross - $tax;
    }

    /**
     * Calculate coupon gross amount
     * @return double
     */
    public function getCouponGrossAmount()
    {
        return $this->getGrossAmount() - $this->getPayPalAmount();
    }

    /**
     * Calculate tax amount for invoice value
     * @param double $value
     * @return double
     */
    public function getCouponTaxAmount($value)
    {
        return $value * 19 / 119;
    }

    public function fillHistoryData()
    {
        $requests = $this->eventRequests;

        foreach ($requests as $request)
        {
            $invoiceItem = new InvoiceItem();

            $invoiceItem->invoice_id = $this->id;
            $invoiceItem->event_name = $request->event->name;
            $invoiceItem->number_of_days = $request->number_of_days;
            $invoiceItem->from_date = $request->date_from;
            $invoiceItem->to_date = $request->date_to;
            $invoiceItem->hostess_name = $request->user->fullName;
            $invoiceItem->booking_price_per_day = $request->booking_price_per_day;
            $invoiceItem->amount = $request->total_booking_price;
            $invoiceItem->is_last_minute = $request->is_last_minute;
            
            $invoiceItem->save(false);
        }

        $this->net_amount = $this->getTotalBookingPrice();
        $this->tax_amount = $this->getTaxAmount();
        $this->gross_amount = $this->getGrossAmount();
        $this->total_price_with_discount = $this->getPayPalAmount();

        if (isset($this->coupon_id))
        {
            $this->coupon_gross_amount = $this->getCouponGrossAmount();
            $this->coupon_netto_amount = $this->getCouponNettoAmount();
            $this->coupon_tax_amount = $this->getCouponTaxAmount($this->coupon_gross_amount);
        }

        $exhibitor = $this->userRequested;
        $exhibitorProfile = $exhibitor->exhibitorProfile;

        $this->language = $exhibitor->lang;

        $this->exhibitor_company_name = $exhibitorProfile->company_name;
        $this->exhibitor_street = $exhibitorProfile->street;
        $this->exhibitor_postal_code = $exhibitorProfile->postal_code;
        $this->exhibitor_city = $exhibitorProfile->city;
        $this->exhibitor_tax_number = $exhibitorProfile->tax_number;

        $country = $exhibitorProfile->country;

        if (isset($country))
        {
            $this->exhibitor_country = $country->name;

            $this->is_exhibitor_german = Yii::$app->util->isGerman($country->id);

            $this->invoice_type = $country->invoice_type;
        }

        $this->update(false);
    }

    /**
     * 
     * @return string
     */
    public function getCouponAmountLabel()
    {
        if ($this->coupon_type == Coupon::TYPE_FIX)
        {
            return "-" . $this->coupon_amount;
        }
        if ($this->coupon_type == Coupon::TYPE_PERECENTAGE)
        {
            return "-" . $this->coupon_amount . " %";
        }

        return '';
    }

    public function getPaymentTypeLabel()
    {
        if ($this->payment_type == self::PAYMENT_TYPE_PAYPAL)
        {
            return Yii::t('app.exhibitor', 'PayPal');
        } elseif ($this->payment_type == self::PAYMENT_TYPE_INVOICE)
        {
            return Yii::t('app.exhibitor', 'Invoice');
        } elseif ($this->payment_type == self::PAYMENT_TYPE_RECEIPT)
        {
            return Yii::t('app.exhibitor', 'Receipt');
        }

        return '';
    }

    public function getPaymentStatusLabel()
    {
        if ($this->status == self::STATUS_NOT_PAID)
        {
            return Yii::t('app.exhibitor', 'Not paid');
        }

        return Yii::t('app.exhibitor', 'Paid');
    }

    /**
     * 
     * @return boolean
     */
    public function getIsInvoiceTypeEu()
    {
        return $this->invoice_type == Country::INVOICE_EU;
    }

    /**
     * 
     * @return boolean
     */
    public function getIsInvoiceTypeDe()
    {
        return $this->invoice_type == Country::INVOICE_DE;
    }

    /**
     * 
     * @return boolean
     */
    public function getIsNormalInvoice()
    {
        return $this->amount > 0;
    }

    /**
     * 
     * @return string
     */
    public function getExhibitorFullName()
    {
        if (is_object($this->userRequested))
        {
            return $this->userRequested->getFullName();
        }

        return '';
    }

    public function isPaypalPayment()
    {
        return $this->payment_type === self::PAYMENT_TYPE_PAYPAL;
    }

    public function isInvoicePayment()
    {
        return $this->payment_type === self::PAYMENT_TYPE_INVOICE;
    }

    public function isReminderEnabled()
    {
        return $this->is_reminder_allowed == 1;
    }

    public function isReminderDisabled()
    {
        return $this->is_reminder_allowed == 0;
    }

    public function getIsReminderAllowedLabel()
    {
        if ($this->is_reminder_allowed)
        {
            return Yii::t('admin', 'Yes');
        }

        return Yii::t('admin', 'No');
    }

    public function getIsExhibitorInGermanyLabel()
    {
        if ($this->is_exhibitor_german)
        {
            return Yii::t('admin', 'Yes');
        }

        return Yii::t('admin', 'No');
    }

}
