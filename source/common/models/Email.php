<?php

namespace common\models;

use Yii;

class Email
{

    /**
     * Send test email
     * @return boolean
     */
    public static function sendKronTestEmail()
    {
        return static::sendEmail('testing', [], Yii::$app->settings->email_from, [
                    Yii::$app->settings->developer_email
                        ], "BMH KRON TEST");
    }

    /**
     * Send email to exhibitor after profile approved by admin
     * @param User $model
     */
    public static function sendActivateByAdminEmailToExhibitor($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $data = ['model' => $model];
        $view = 'exhibitor/activated_by_admin';
        $subject = Yii::t('mail.exhibitor', 'BOOKmyHOSTESS.com Profile');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to hostess to activate profile
     * @param \common\models\User $model
     * @return boolean
     */
    public static function sendActivateReminderEmailToHostess($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $validationLink = Yii::$app->urlManagerFrontend->createAbsoluteUrl(["//hostess-profile/activate", "id" => $model->email_validation_secret, "lang" => Yii::$app->language]);

        $data = ['model' => $model, "validationLink" => $validationLink];
        $view = 'hostess/activate_reminder';
        $subject = Yii::t('mail.hostess', 'Registration at BOOKmyHOSTESS.com');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to hostess after she rejects request
     * @param \common\models\User $model
     * @return boolean
     */
    public static function sendAfterRejectEmailToHostess($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $data = ['model' => $model];
        $view = 'hostess/after_reject';
        $subject = Yii::t('mail.hostess', 'Request Rejection at BOOKmyHOSTESS.com');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send notification that email has been changed to hostess
     * @param \common\models\User $model
     * @return boolean
     */
    public static function sendChangeEmailToHostess($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $data = ['model' => $model];
        $view = 'hostess/change_email_old';
        $subject = Yii::t('mail.hostess', 'Email address BOOKmyHOSTESS.com');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        if (static::sendHostessEmail($view, $data, $from, $to, $subject))
        {
            $view_new = 'hostess/change_email_new';
            return static::sendHostessEmail($view_new, $data, $from, $to, $subject);
        }

        return false;
    }

    /**
     * Send notification that email has been changed to exhibitor
     * @param \common\models\User $model
     * @return boolean
     */
    public static function sendChangeEmailToExhibitor($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $data = ['model' => $model];
        $view = 'exhibitor/change_email_old';
        $subject = Yii::t('mail.exhibitor', 'Email address BOOKmyHOSTESS.com');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        if (static::sendExhibitorEmail($view, $data, $from, $to, $subject))
        {
            $view_new = 'exhibitor/change_email_new';
            return static::sendExhibitorEmail($view_new, $data, $from, $to, $subject);
        }

        return false;
    }

    /**
     * Send reminder to activate profile to exhibitor
     * @param \common\models\User $model
     * @return boolean
     */
    public static function sendActivateReminderEmailToExhibitor($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $validationLink = Yii::$app->urlManagerFrontend->createAbsoluteUrl(["//exhibitor/activate", "id" => $model->email_validation_secret, "lang" => Yii::$app->language]);

        $data = ['model' => $model, "validationLink" => $validationLink];
        $view = 'exhibitor/activate_reminder';
        $subject = Yii::t('mail.exhibitor', 'Registration at BOOKmyHOSTESS.com');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send reminder to send requests to exhibitor
     * @param \common\models\User $model
     * @return boolean
     */
    public static function sendRequestReminderEmailToExhibitor($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $data = ['model' => $model];
        $view = 'exhibitor/request_reminder';
        $subject = Yii::t('mail.exhibitor', 'Hostess booking');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send reminder to signup for events to hostess
     * @param \common\models\User $model
     * @return boolean
     */
    public static function sendEventRegisterReminderEmailToHostess($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $data = ['model' => $model];
        $view = 'hostess/register_reminder';
        $subject = Yii::t('mail.hostess', 'Your event availability');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send reminder to send approve request to hostess
     * @param \common\models\User $model
     * @return boolean
     */
    public static function sendApproveReminderEmailToHostess($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $data = ['model' => $model];
        $view = 'hostess/approve_reminder';
        $subject = Yii::t('mail.hostess', 'Profile not approved');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send reminder to respond on request
     * @param \common\models\Request $model
     * @return boolean
     */
    public static function sendRespondToRequestReminderToHostess($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->user->lang;

        $view = 'hostess/response_reminder';
        $subject = Yii::t('mail.hostess', '{event} - Booking request', ["event" => $model->event->name]);

        $data = ['model' => $model];
        $from = Yii::$app->settings->email_from;
        $to = $model->user->email;


        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send notification to hostess that request is deleted
     * @param \common\model\Request $model
     * @return boolean
     */
    public static function sendRequestDeletedNotificationToHostess($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->user->lang;

        $view = 'hostess/request_deleted';
        $subject = Yii::t('mail.hostess', '{event} - Booking request', ["event" => $model->event->name]);

        $data = ['model' => $model];
        $from = Yii::$app->settings->email_from;
        $to = $model->user->email;


        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * This function is used to send email to user about his registration.
     * 
     * @param \frontend\models\HostessRegister $model
     * @return boolean
     */
    public static function sendHostessRegistrationCreatedEmail($model, $showLangMessage)
    {
        $data = ['model' => $model, 'showLangMessage' => $showLangMessage];

        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        $view = 'hostess/register';
        $subject = Yii::t('mail.hostess', 'E-Mail verification');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to admin that hostess requests postpone of trade license
     * @param \frontend\models\TradeLicenseExten $model
     * @return boolean
     */
    public static function sendHostessPostponeTradeLicenseRequestToAdmin($model)
    {
        $view = 'admin/postpone_trade_license_request';
        $data = ['model' => $model];
        $subject = Yii::t('admin', 'Postpone trade license request');
        return static::sendAdminEmail($view, $data, $subject);
    }

    /**
     * Send email notification that profile is approved
     * @param \common\model\HostessProfile $model
     * @return boolean
     */
    public static function sendHostessProfileApprovedEmail($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->user->lang;

        $view = 'hostess/profile_approved';
        $data = ['model' => $model];
        $subject = Yii::t('mail.hostess', 'BOOKmyHOSTESS.com profile approved');
        $from = Yii::$app->settings->email_from;
        $to = $model->user->email;


        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send hostess not approved email
     * @param \common\models\HostessProfile $model
     * @param \common\models\UserMessages $message
     * @return boolean
     */
    public static function sendHostessProfileNotApprovedEmail(HostessProfile $model, UserMessages $message)
    {
        /**
         * TODO
         * In all emails pass language and use it in Yii::t.
         * No need to change app language for this purpose
         */
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->user->lang;

        $view = 'hostess/profile_not_approved';
        $data = ['model' => $model, 'message' => $message->message];
        $subject = Yii::t('mail.hostess', 'BOOKmyHOSTESS.com profile not approved');
        $from = Yii::$app->settings->email_from;
        $to = $model->user->email;


        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * This function is used to send email to user about his registration.
     * 
     * @param \frontend\models\ExhibitorRegister $model
     * @return boolean
     */
    public static function sendExibitorRegistrationCreatedEmail($model, $showLangMessage, $refhos = null, $event = null, $cp = null)
    {

        $link = $model->getValidationLink($refhos, $event, $cp);

        $data = [
            'model' => $model,
            'showLangMessage' => $showLangMessage,
            'link' => $link
        ];

        Yii::$app->language = $model->lang;

        $view = 'exhibitor/register';
        $subject = Yii::t('mail.exhibitor', 'E-Mail verification');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * This function is used to send email to user when he forgets password.
     * 
     * @param \common\models\User model
     * @return boolean
     */
    public static function sendRequestPasswordResetEmail($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->lang;

        if ($model->isHostess)
        {
            $view = 'hostess/password_reset';
            $data = ['model' => $model];
            $subject = Yii::t('mail.hostess', 'Password reset');
            $from = Yii::$app->settings->email_from;
            $to = $model->email;

            return static::sendHostessEmail($view, $data, $from, $to, $subject);
        } else
        {
            $view = 'exhibitor/password_reset';
            $data = ['model' => $model];
            $subject = Yii::t('mail.exhibitor', 'Password reset');
            $from = Yii::$app->settings->email_from;
            $to = $model->email;

            return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
        }
    }

    /**
     * Send subscribe for newsletter confirmation
     * @param \frontend\models\NewsletterSubscriber $model
     * @return boolean
     */
    public static function sendNewsletterSubscribeEmail($model)
    {
        $view = 'user/newsletter_subscribe';
        $data = ['model' => $model];
        $subject = Yii::t('mail', 'BOOKmyHOSTESS Nesletter');
        $from = Yii::$app->settings->email_from;
        $to = $model->email;

        return static::sendEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send contact form email
     * @param \frontend\models\ContactForm $model
     * @return boolean
     */
    public static function sendContactUsEmail($model)
    {
        // emails for admin always in English
        Yii::$app->language = "en";

        Yii::$app->mailer->htmlLayout = 'layouts/html';

        $to = Yii::$app->settings->contact_email;
        $view = 'admin/contact_us';
        $data = ['model' => $model];
        $subject = Yii::t('admin', 'Contact form message:') . " " . $model->subject;
        $from = [$model->email => $model->name];

        return static::sendEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to developer that checkout has failed
     * @param array $content
     */
    public static function sendCompleteBookingFailedEmail($content)
    {
        $view = 'checkout/fail';
        $data = ['content' => $content];
        $subject = Yii::t('admin', 'BMH BOOKING FAILED');
        static::sendDevEmail($view, $data, $subject);
    }

    /**
     * Send email to developer that paypal transaction is failed
     * @param array $content
     */
    public static function sendPayPalFailedEmail($content)
    {
        $view = 'paypal/fail';
        $data = ['content' => $content];
        $subject = Yii::t('admin', 'BMH PAYPAL FAILED');
        static::sendDevEmail($view, $data, $subject);
    }

    /**
     * Send email to developer that paypal transaction is invalid
     * @param array $post
     */
    public static function sendPaypalInvalidTransactionEmail($post)
    {

        $view = 'paypal/paypal_invalid_transaction';
        $data = ['content' => $post];
        $subject = Yii::t('admin', 'Paypal Invalid Transaction');
        static::sendDevEmail($view, $data, $subject);
    }

    /**
     * Send email to developer that paypal transaction is denied
     * @param array $post
     */
    public static function sendPaypalDeniedTransactionEmail($post)
    {

        $view = 'paypal/paypal_denied_transaction';
        $data = ['content' => $post];
        $subject = Yii::t('admin', 'Paypal Denied Transaction');
        static::sendDevEmail($view, $data, $subject);
    }

    /**
     * Notify hostess about new request
     * @param \common\models\Request $model 
     */
    public static function sendBookRequestToHostess($model)
    {
        $data = ['model' => $model];

        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->user->lang;

        $view = 'hostess/book';
        $subject = Yii::t('mail.hostess', '{event} - Booking request', ["event" => $model->event->name]);
        $from = Yii::$app->settings->email_from;
        $to = $model->user->email;

        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * After hostess accepts request, notify exhibitor
     * @param \common\models\Request $model 
     */
    public static function sendAcceptRequestEmailToExibitor($model)
    {
        $data = ['model' => $model];

        Yii::$app->language = $model->userRequested->lang;

        $view = 'exhibitor/acceptRequest';
        $subject = Yii::t('mail.exhibitor', '{event} - Request accepted', ["event" => $model->event->name]);
        $from = Yii::$app->settings->email_from;
        $to = $model->userRequested->email;

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * After hostess rejects request notify exhibitor
     * @param \common\models\Request $model 
     */
    public static function sendRejectRequestEmailToExibitor($model)
    {
        $data = ['model' => $model];

        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->userRequested->lang;

        $view = 'exhibitor/rejectRequest';
        $subject = Yii::t('mail.exhibitor', '{event} - Request not confirmed', ["event" => $model->event->name]);
        $from = Yii::$app->settings->email_from;
        $to = $model->userRequested->email;

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * After hostess withdraw confirmed request, notify exhibitor
     * @param \common\models\Request $model 
     */
    public static function sendHostessWithdrawConfirmationEmailToExibitor($model)
    {
        $data = ['model' => $model];

        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->userRequested->lang;

        $view = 'exhibitor/hostess_withdraw_confirmation';
        $subject = Yii::t('mail.exhibitor', '{event} - Confirmation withdrawn', ["event" => $model->event->name]);
        $from = Yii::$app->settings->email_from;
        $to = $model->userRequested->email;


        return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to hostess after exhibitor withdraw request
     * @param \common\models\Request $model 
     */
    public static function sendExhibitorWithdrawEmailToHostess($model)
    {
        // SEND EMAIL TO HOSTESS
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->user->lang;

        $view = 'hostess/exhibitor_withdraw_confirmation';
        $data = ['model' => $model];
        $subject = Yii::t('mail.hostess', '{event} - Request withdrawn', ["event" => $model->event->name]);

        $from = Yii::$app->settings->email_from;
        $to = $model->user->email;


        return static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send first reminder to exhibitor
     * @param \common\models\Invoice $model
     * @return boolean
     */
    public static function sendInvoiceFirstReminderEmailToExibitor($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->userRequested->lang;

        $view = 'invoice/first_reminder';
        $data = ['model' => $model];

        $subject = Yii::t('mail.exhibitor', 'Payment reminder ');

        $from = Yii::$app->settings->email_from;
        $to = $model->userRequested->email;

        $attachments = [
            Yii::$app->params['invoice_save_path'] . $model->filename
        ];

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject, $attachments);
    }

    /**
     * Send second reminder to exhibitor
     * @param \common\models\Invoice $model
     * @return boolean
     */
    public static function sendInvoiceSecondReminderEmailToExibitor($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->userRequested->lang;

        $view = 'invoice/second_reminder';
        $data = ['model' => $model];

        $subject = Yii::t('mail.exhibitor', 'Payment warning');

        $from = Yii::$app->settings->email_from;
        $to = $model->userRequested->email;

        $attachments = [
            Yii::$app->params['invoice_save_path'] . $model->filename
        ];

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject, $attachments);
    }

    /**
     * Send invoice and contract to exhibitor
     * @param \common\models\Invoice $model
     * @param string $invoiceFileName
     * @return boolean
     */
    public static function sendInvoiceEmailToExibitor($model, $invoiceFileName)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->userRequested->lang;

        $view = 'invoice/exhibitor';
        $data = ['model' => $model];

        if ($model->getIsNormalInvoice())
        {
            $subject = Yii::t('mail.exhibitor', 'Contact Details and INVOICE');
        } else
        {
            $subject = Yii::t('mail.exhibitor', 'Contact Details and RECEIPT');
        }

        $from = Yii::$app->settings->email_from;
        $to = $model->userRequested->email;

        $contractFileEn = Yii::$app->params['contracts_path']['exhibitor'] .
                Yii::$app->params['exhibitor_contract_files']["en"];

        $attachments = [
            $contractFileEn,
            Yii::$app->params['invoice_save_path'] . $invoiceFileName
        ];

        // if translated !!!
        if (in_array($model->userRequested->lang, array_keys(Yii::$app->params['languages'])))
        {
            $contractFile = Yii::$app->params['contracts_path']['exhibitor'] .
                    Yii::$app->params['exhibitor_contract_files'][$model->userRequested->lang];
            $attachments[] = $contractFile;
        }

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject, $attachments);
    }

    /**
     * Send invoice to hostess with contract
     * @param \common\models\Request $model 
     */
    public static function sendInvoiceEmailToHostess($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->user->lang;

        $view = 'invoice/hostess';
        $data = ['model' => $model];
        $subject = Yii::t('mail.hostess', '{event} - Booking', ["event" => $model->event->name]);

        $from = Yii::$app->settings->email_from;
        $to = $model->user->email;

        $attachments = [];

        $englishInvoice = Yii::$app->params['invoices_templates_path']["hostess"] .
                Yii::$app->params['hostess_invoice_files']['en'];

        $englishContract = Yii::$app->params['contracts_path']['hostess'] .
                Yii::$app->params['hostess_contract_files']["en"];

        $attachments[] = $englishInvoice;
        $attachments[] = $englishContract;

        if ($model->user->lang !== "en" && in_array($model->user->lang, array_keys(Yii::$app->params['languages'])))
        {
            $attachments[] = Yii::$app->params['invoices_templates_path']["hostess"] .
                    Yii::$app->params['hostess_invoice_files'][$model->user->lang];

            $attachments[] = Yii::$app->params['contracts_path']['hostess'] .
                    Yii::$app->params['hostess_contract_files'][$model->user->lang];
        }

        return static::sendHostessEmail($view, $data, $from, $to, $subject, $attachments);
    }

    /**
     * Notify exhibitor to complete booking request
     * @param \common\models\Request $model 
     */
    public static function sendBookNotifyEmailToExhibitor($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->userRequested->lang;

        $view = 'exhibitor/book_notify';
        $subject = Yii::t('mail.exhibitor', '{event} - Booking reminder', ["event" => $model->event->name]);

        $data = ['model' => $model];
        $from = Yii::$app->settings->email_from;
        $to = $model->userRequested->email;


        static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to exhibitor that his request is deleted
     * @param \common\models\Request $model 
     */
    public static function sendBookDeletedEmailToExhibitor($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->userRequested->lang;

        $view = 'exhibitor/book_deleted';
        $subject = Yii::t('mail.exhibitor', '{event} - Booking request expired', ["event" => $model->event->name]);

        $data = ['model' => $model];
        $from = Yii::$app->settings->email_from;
        $to = $model->userRequested->email;


        static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to hostess that booking request is deleted
     * @param \common\models\Request $model 
     */
    public static function sendBookDeletedEmailToHostess($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->user->lang;

        $view = 'hostess/book_deleted';
        $subject = Yii::t('mail.hostess', '{event} - Booking completed', ["event" => $model->event->name]);

        $data = ['model' => $model];
        $from = Yii::$app->settings->email_from;
        $to = $model->user->email;


        static::sendHostessEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to admin after exhibitor fills in form for search when search result is empty
     * @param \frontend\models\HostessRequestForm $model
     * @return boolean
     */
    public static function sendHostessSearchRequestEmail($model)
    {
        $view = 'admin/hostess_request';
        $data = ['model' => $model];
        $subject = Yii::t('admin', 'New request for hostess search');
        return static::sendAdminEmail($view, $data, $subject);
    }

    /**
     * Send email for review to exhibitor
     * @param \common\models\Request $model
     * @param string $groupCode
     * @return boolean
     */
    public static function sendHostessReviewEmailToExhibitor($model, $groupCode)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->userRequested->lang;

        $reviewLink = Yii::$app->urlManagerFrontend->createAbsoluteUrl(["//review/index", "code" => $groupCode]);

        $view = 'exhibitor/hostess_review';
        $data = [
            'model' => $model,
            'reviewLink' => $reviewLink
        ];
        $subject = Yii::t('mail.exhibitor', 'How satisfied were you with your staff?');
        $from = Yii::$app->settings->email_from;
        $to = $model->userRequested->email;

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * @param \common\models\HostessReview $model
     * @return boolean TRUE|FALSE is email sent successfully
     */
    public static function sendHostessReviewRequestEmailToExhibitor($model)
    {
        // before sending email change language so email is sent in selected language
        Yii::$app->language = $model->exhibitor->lang;

        $reviewLink = Yii::$app->urlManagerFrontend->createAbsoluteUrl(["//review/index", "code" => $model->group_code]);

        $view = 'exhibitor/hostess_review_request';
        $data = [
            'model' => $model,
            'reviewLink' => $reviewLink
        ];
        $subject = Yii::t('mail.exhibitor', '{hostess} asks for your feedback', [
                    "hostess" => $model->hostess->fullName
        ]);
        $from = Yii::$app->settings->email_from;
        $to = $model->exhibitor->email;

        return static::sendExhibitorEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to administrator email
     * @param string $view
     * @param array $data
     * @param string $subject
     * @param string $from
     * @return boolean
     */
    private static function sendAdminEmail($view, $data, $subject, $from = "")
    {
        if (empty($from))
        {
            $from = Yii::$app->settings->email_from;
        }

        Yii::$app->mailer->htmlLayout = 'layouts/html';
        $to = Yii::$app->settings->admin_email;

        // emails for admin always in English
        Yii::$app->language = "en";
        return static::sendEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email on developer email
     * @param string $view
     * @param array $data
     * @param string $subject
     * @param string $from
     * @return boolean
     */
    private static function sendDevEmail($view, $data, $subject, $from = "")
    {
        if (empty($from))
        {
            $from = Yii::$app->settings->email_from;
        }

        Yii::$app->mailer->htmlLayout = 'layouts/html';
        $to = Yii::$app->settings->developer_email;

        // emails for admin always in English
        Yii::$app->language = "en";
        return static::sendEmail($view, $data, $from, $to, $subject);
    }

    /**
     * Send email to hostess using specific layout
     * @param string $view
     * @param array $data
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param array $attacments
     * @return boolean
     */
    private static function sendHostessEmail($view, $data, $from, $to, $subject, $attacments = [])
    {
        Yii::$app->mailer->htmlLayout = 'layouts/hostess';
        return static::sendEmail($view, $data, $from, $to, $subject, $attacments);
    }

    /**
     * Send email to exhibitor using specific layout
     * @param string $view
     * @param array $data
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param array $attacments
     * @return boolean
     */
    private static function sendExhibitorEmail($view, $data, $from, $to, $subject, $attacments = [])
    {
        Yii::$app->mailer->htmlLayout = 'layouts/exhibitor';
        return static::sendEmail($view, $data, $from, $to, $subject, $attacments);
    }

    /**
     * Send email using given template and data
     * @param string $view
     * @param array $data
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param array $attacments
     * @return boolean
     */
    private static function sendEmail($view, $data, $from, $to, $subject, $attacments = [])
    {
        $email = Yii::$app->mailer->compose($view, $data)
                ->setFrom($from)
                ->setTo($to)
                ->setSubject($subject);

        foreach ($attacments as $attacment)
        {
            $email->attach($attacment);
        }

        return $email->send();
    }

}
