<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "exhibitor_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $country_id
 * @property string $company_name
 * @property string $tax_number
 * @property string $street
 * @property string $home_number
 * @property string $postal_code
 * @property string $date_created
 * @property string $city
 * @property string $last_profile_view
 * @property integer $review_email_sent
 * @property integer $booking_type
 * @property string $booking_price_per_day
 * @property string $booking_price_discount
 * @property integer $staff_demand_type
 * @property string $website
 * @property integer $status_id
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $is_invoice_payment_allowed
 *
 * @property User $updatedBy
 * @property User $createdBy
 * @property Status $status
 * @property Country $country
 * @property User $user
 */
class ExhibitorProfile extends \common\components\Model
{

    /**
     * booking types
     */
    const BOOKING_TYPE_FREE = 0; // total free booking
    const BOOKING_TYPE_REGULAR = 1; // booking normal of 35 EUR per day
    const BOOKING_TYPE_SPECIAL = 2; // special price for booking defined by admin

    /**
     * staff demand types
     */
    const STAFF_DEMAND_TYPE_NORMAL = 0;
    const STAFF_DEMAND_TYPE_SPECIAL = 1;
    const STAFF_DEMAND_TYPE_COOPERATION = 2;
    const STAFF_DEMAND_TYPE_FIVE_PER_YEAR = 3;

    public $lang;
    public $gender;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exhibitor_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'booking_type', 'staff_demand_type', 'is_invoice_payment_allowed'], 'required'],
            [['user_id', 'country_id', 'review_email_sent', 'booking_type', 'staff_demand_type', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'last_profile_view', 'date_updated'], 'safe'],
            [['booking_price_per_day', 'booking_price_discount'], 'number'],
            [['company_name', 'street', 'city', 'website'], 'string', 'max' => 255],
            [['tax_number', 'home_number', 'postal_code'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User ID'),
            'country_id' => Yii::t('admin', 'Country'),
            'countryName' => Yii::t('admin', 'Country'),
            'company_name' => Yii::t('admin', 'Company Name'),
            'tax_number' => Yii::t('admin', 'Tax Number'),
            'street' => Yii::t('admin', 'Street'),
            'home_number' => Yii::t('admin', 'House Number'),
            'postal_code' => Yii::t('admin', 'Postal Code'),
            'date_created' => Yii::t('admin', 'Date Created'),
            'city' => Yii::t('admin', 'City'),
            'last_profile_view' => Yii::t('admin', 'Last Profile View'),
            'review_email_sent' => Yii::t('admin', 'Review Email Sent'),
            'booking_type' => Yii::t('admin', 'Booking Type'),
            'bookingTypeLabel' => Yii::t('admin', 'Booking Type'),
            'booking_price_per_day' => Yii::t('admin', 'Booking Price Per Day'),
            'staff_demand_type' => Yii::t('admin', 'Staff Demand Type'),
            'staffDemandTypeLabel' => Yii::t('admin', 'Staff Demand Type'),
            'website' => Yii::t('admin', 'Website'),
            'is_invoice_payment_allowed' => Yii::t('admin', 'Is payment on invoice allowed')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Attribute hints
     * @return array
     */
    public function attributeHints()
    {
        return [
            'website' => Yii::t('app', 'Please enter with http or https ( http://website.domain or https://website.domain )')
        ];
    }

    /**
     * Create base emtpy exhibitor profile model
     * @param integer $id
     */
    public static function createBaseProfile($id)
    {
        $model = static::findOne(["user_id" => $id]);

        if (!is_object($model))
        {
            $model = new ExhibitorProfile();
            $model->user_id = $id;
            $model->save(false);
        }
    }

    /**
     * Save values from profile form to user
     */
    public function setValuesFromUser()
    {

        if (isset($this->user) && isset($this->user->lang))
        {
            $this->lang = $this->user->lang;
            $this->gender = $this->user->gender;
        }
    }

    /**
     * Used to validate completeness, in order to know can invoice be generated.
     * Used both on frontend and backend
     * @return boolean
     */
    public function isProfileCompleted()
    {
        if (
                !empty($this->company_name) &&
                !empty($this->tax_number) &&
                !empty($this->street) &&
                !empty($this->country_id) &&
                !empty($this->city) &&
                !empty($this->home_number)
        )
        {
            return true;
        }
        return false;
    }

    /**
     * Get list of staff demand types
     * @return array
     */
    public static function getBookingStaffDemandTypesForDropdown()
    {
        return [
            static::STAFF_DEMAND_TYPE_NORMAL => Yii::t('app', 'Up to 5 people monthly'),
            static::STAFF_DEMAND_TYPE_SPECIAL => Yii::t('app', 'More than 5 people monthly'),
            static::STAFF_DEMAND_TYPE_FIVE_PER_YEAR => Yii::t('app', 'Up to 5 people per year'),
            static::STAFF_DEMAND_TYPE_COOPERATION => Yii::t('app', 'Interested in a cooperation'),
        ];
    }

    /**
     * Get staff demand type label
     * @return string
     */
    public function getStaffDemandTypeLabel()
    {
        if ($this->staff_demand_type == static::STAFF_DEMAND_TYPE_COOPERATION)
        {
            return Yii::t('app', 'Interested in a cooperation');
        } else if ($this->staff_demand_type == static::STAFF_DEMAND_TYPE_SPECIAL)
        {
            return Yii::t('app', 'More than 5 people monthly');
        } else if ($this->staff_demand_type == static::STAFF_DEMAND_TYPE_FIVE_PER_YEAR)
        {
            return Yii::t('app', 'Up to 5 people per year');
        } else
        {
            return Yii::t('app', 'Up to 5 people monthly');
        }
    }

    /**
     * Get booking type label
     * @return string
     */
    public function getBookingTypeLabel()
    {
        if ($this->booking_type == static::BOOKING_TYPE_FREE)
        {
            return Yii::t('admin', 'Free');
        }
        if ($this->booking_type == static::BOOKING_TYPE_REGULAR)
        {
            return Yii::t('admin', 'Normal');
        }
        if ($this->booking_type == static::BOOKING_TYPE_SPECIAL)
        {
            return Yii::t('admin', 'Special');
        }
    }

    public function getBookingPrice($eventID)
    {
        /**
         * 1. If bookingtype == free return 0
         * 2. If regular, return for specific country
         * 3. If special, apply discount to normal booking price for that country
         */
        if ($this->booking_type == self::BOOKING_TYPE_FREE)
        {
            return 0;
        }

        $eventModel = Event::findOne(["id" => $eventID]);

        if (is_object($eventModel) && is_object($eventModel->country))
        {
            $originalPrice = $eventModel->country->booking_price;

            if ($this->booking_type == self::BOOKING_TYPE_REGULAR)
            {
                return $originalPrice;
            }

            if ($this->booking_type == self::BOOKING_TYPE_SPECIAL)
            {
                return $this->calculatePrice($originalPrice);
            }
        }

        // last possible fallback
        return Yii::$app->settings->booking_price;
    }

    /**
     * @param \common\models\ExhibitorProfile $profileModel
     */
    public function getAllowedPaymentsForExhibitor()
    {
        $payments = [
            Invoice::PAYMENT_TYPE_PAYPAL,
            Invoice::PAYMENT_TYPE_RECEIPT
        ];

        if ($this->is_invoice_payment_allowed)
        {
            $payments[] = Invoice::PAYMENT_TYPE_INVOICE;
        }

        return $payments;
    }

    /**
     * @param double $originalPrice
     * @return double
     */
    private function calculatePrice($originalPrice)
    {
        $discount = $this->booking_price_discount;

        if ($discount > 0)
        {
            $discount = ( $discount / 100 ) * $originalPrice;
            return $originalPrice - $discount;
        }

        return $originalPrice;
    }

}
