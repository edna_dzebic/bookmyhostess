<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "invoice_reminder".
 *
 * @property integer $id
 * @property integer $invoice_id
 * @property integer $flag
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Invoice $invoice
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 */
class InvoiceReminder extends \common\components\Model
{

    const FLAG_FIRST_REMINDER = 1;
    const FLAG_SECOND_REMINDER = 2;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_reminder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'flag'], 'required'],
            [['invoice_id', 'flag', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'invoice_id' => Yii::t('admin', 'Invoice ID'),
            'flag' => Yii::t('admin', 'Flag')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

}
