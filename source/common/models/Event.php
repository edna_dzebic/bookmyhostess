<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "event".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $city_id
 * @property integer $type_id
 * @property string $url
 * @property string $name
 * @property string $venue
 * @property string $brief_description
 * @property string $start_date
 * @property string $end_date
 * @property integer $status_id
 * @property string $date_updated
 * @property string $date_created
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $parent_id
 *
 * @property Type $type
 * @property City $city
 * @property Country $country
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property EventDressCode[] $eventDressCodes
 * @property EventJob[] $eventJobs
 * @property HostessReview[] $hostessReviews
 * @property Register[] $registers
 * @property Request[] $requests
 */
class Event extends \common\components\Model
{

    public $alreadyRegistered;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'city_id', 'type_id', 'name', 'venue', 'start_date', 'end_date'], 'required'],
            [['country_id', 'city_id', 'type_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['start_date', 'end_date', 'date_updated', 'date_created', 'parent_id'], 'safe'],
            [['url', 'name', 'venue'], 'string', 'max' => 150],
            [['brief_description'], 'string', 'max' => 1450],
            [['url', 'venue'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            ['start_date', 'compare', 'compareAttribute' => 'end_date', 'operator' => '<=', 'enableClientValidation' => false],
            ['url', 'url']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'country_id' => Yii::t('admin', 'Country'),
            'countryName' => Yii::t('admin', 'Country'),
            'translatedCountryName' => Yii::t('admin', 'Country'),
            'city_id' => Yii::t('admin', 'City'),
            'cityName' => Yii::t('admin', 'City'),
            'translatedCityName' => Yii::t('admin', 'City'),
            'type_id' => Yii::t('admin', 'Type'),
            'typeName' => Yii::t('admin', 'Type'),
            'translatedTypeName' => Yii::t('admin', 'Type'),
            'url' => Yii::t('admin', 'Url'),
            'name' => Yii::t('admin', 'Name'),
            'venue' => Yii::t('admin', 'Venue'),
            'brief_description' => Yii::t('admin', 'Brief Description'),
            'start_date' => Yii::t('admin', 'Start Date'),
            'end_date' => Yii::t('admin', 'End Date'),
            'parent_id' => Yii::t('admin', 'Parent event')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDressCodes()
    {
        return $this->hasMany(EventDressCode::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventJobs()
    {
        return $this->hasMany(EventJob::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessReviews()
    {
        return $this->hasMany(HostessReview::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegisters()
    {
        return $this->hasMany(Register::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['event_id' => 'id']);
    }

    /**
     * Get city name
     * @return string
     */
    public function getCityName()
    {
        return is_object($this->city) ? $this->city->name : '';
    }

    /**
     * Get translated city name
     * @return string
     */
    public function getTranslatedCityName()
    {
        return is_object($this->city) ? $this->city->getTranslatedName() : '';
    }

    /**
     * Get country name
     * @return string
     */
    public function getCountryName()
    {
        return is_object($this->country) ? $this->country->name : '';
    }

    /**
     * Get translated country name
     * @return string
     */
    public function getTranslatedCountryName()
    {
        return is_object($this->country) ? $this->country->getTranslatedName() : '';
    }

    /**
     * Get event type name
     * @return string
     */
    public function getTypeName()
    {
        return is_object($this->type) ? $this->type->name : '';
    }

    /**
     * Get translated type name
     * @return string
     */
    public function getTranslatedTypeName()
    {
        return is_object($this->type) ? $this->type->getTranslatedName() : '';
    }

    /**
     * Is hostess already booked for this event
     * @return boolean
     */
    public function getHostessAlreadyBooked()
    {
        return Request::find()
                        ->andWhere(["request.user_id" => Yii::$app->user->id])
                        ->andWhere(["request.completed" => 1])
                        ->andWhere(["request.event_id" => $this->id])
                        ->exists();
    }

    /**
     * Does hostess have new requests for this event
     * @return boolean
     */
    public function getHostessHasNewRequests()
    {
        return Request::find()
                        ->andWhere(["request.user_id" => Yii::$app->user->id])
                        ->andWhere(["request.event_id" => $this->id])
                        ->andWhere(["request.status_id" => [Yii::$app->status->request_new]])
                        ->exists();
    }

    /**
     * Does hostess have accepted requests for this event
     * @return boolean
     */
    public function getHostessHasAcceptedRequests()
    {
        return Request::find()
                        ->andWhere(["request.user_id" => Yii::$app->user->id])
                        ->andWhere(["request.event_id" => $this->id])
                        ->andWhere(["request.status_id" => [Yii::$app->status->request_accept]])
                        ->exists();
    }

    /**
     * Is hostess already registered for this event
     * @return boolean
     */
    public function getHostessAlreadyRegistered()
    {
        return Register::find()
                        ->andWhere(["user_id" => Yii::$app->user->id])
                        ->andWhere(["event_id" => $this->id])
                        ->exists();
    }

    /**
     * 
     * @return boolean
     */
    public function getIsPast()
    {
        return date("Y-m-d", strtotime($this->end_date)) < date("Y-m-d");
    }

    public function getDisplayName()
    {
        if (strlen($this->name) > 55)
        {
            return substr($this->name, 0, 55) . "...";
        }

        return $this->name;
    }

}
