<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "country_price".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $value
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status_id
 * @property string $price_class
 *
 * @property Status $status
 * @property Country $country
 * @property User $createdBy
 * @property User $updatedBy
 */
class CountryPrice extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'value', 'price_class'], 'required'],
            [['country_id', 'created_by', 'updated_by', 'status_id'], 'integer'],
            [['value'], 'number'],
            [['date_created', 'date_updated'], 'safe'],
            [['price_class'], 'string', 'max' => 255],
            [['price_class'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'value' => Yii::t('admin', 'Value'),
            'price_class' => Yii::t('admin', 'Price Class'),
            'country_id' => Yii::t('admin', 'Country'),
            'countryName' => Yii::t('admin', 'Country'),
            'translatedCountryName' => Yii::t('admin', 'Country')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Get country name
     * @return string
     */
    public function getCountryName()
    {
        return is_object($this->country) ? $this->country->name : '';
    }

    /**
     * Get translated country name
     * @return string
     */
    public function getTranslatedCountryName()
    {
        return is_object($this->country) ? $this->country->getTranslatedName() : '';
    }

}
