<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "exhibitor_testimonial".
 *
 * @property integer $id
 * @property string $text
 * @property string $source
 * @property integer $language_id
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Language $language
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 */
class ExhibitorTestimonial extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exhibitor_testimonial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'source', 'language_id'], 'required'],
            [['text'], 'string'],
            [['language_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['source'], 'string', 'max' => 255],
            [['text'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            [['source'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'text' => Yii::t('admin', 'Text'),
            'source' => Yii::t('admin', 'Source'),
            'language_id' => Yii::t('admin', 'Language')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

}
