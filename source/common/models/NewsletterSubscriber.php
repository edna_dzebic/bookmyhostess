<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "newsletter_subscriber".
 *
 * @property integer $id
 * @property string $email
 * @property string $date_created
 * @property string $date_updated
 * @property integer $status_id
 * @property integer $updated_by
 * @property integer $created_by
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 */
class NewsletterSubscriber extends \common\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsletter_subscriber';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['status_id', 'updated_by', 'created_by'], 'integer'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'email' => Yii::t('admin', 'Email')
            ]
        );
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }


}
