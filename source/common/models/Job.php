<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "job".
 *
 * @property integer $id
 * @property string $name
 * @property integer $order
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property EventJob[] $eventJobs
 * @property HostessPreferredJob[] $hostessPreferredJobs
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property RequestJob[] $requestJobs
 */
class Job extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['order', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'name' => Yii::t('admin', 'Name'),
            'order' => Yii::t('admin', 'Order')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventJobs()
    {
        return $this->hasMany(EventJob::className(), ['job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessPreferredJobs()
    {
        return $this->hasMany(HostessPreferredJob::className(), ['event_job_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestJobs()
    {
        return $this->hasMany(RequestJob::className(), ['event_job' => 'id']);
    }

    /**
     * Get translated name
     * @return string
     */
    public function getTranslatedName()
    {
        return Yii::t('app.jobs', $this->name);
    }

}
