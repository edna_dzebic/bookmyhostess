<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "user_documents".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $filename
 * @property string $real_filename
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property User $user
 */
class UserDocuments extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'filename', 'real_filename'], 'required'],
            [['user_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['filename'], 'file', 'on' => 'upload', 'maxSize' => 1024 * 1024 * 5, 'extensions' => 'png, jpg, jpeg, gif, doc, docx, pdf, xls, xlsx, odf', 'tooBig' => Yii::t('app.hostess', 'The file "{file}" is too big. Its size cannot exceed 5MB.'), "checkExtensionByMimeType" => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'filename' => Yii::t('admin', 'Filename'),
            'real_filename' => Yii::t('admin', 'Real Filename')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Make filename shorter if needed
     * @return string
     */
    public function getShortFileName()
    {
        if (strlen($this->real_filename) > 40)
        {
            return substr($this->real_filename, 0, 40) . '...';
        }

        return $this->real_filename;
    }

    /**
     * Get download link for file
     * @return string
     */
    public function getDownloadLink()
    {
        return Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/uploads/{$this->filename}"]);
    }

    public function saveDocument()
    {
        if ($this->validate())
        {
            $filename = sha1(microtime()) . '.' . $this->filename->extension;
            $path = Yii::$app->params['uploads_save_path'] . $filename;
            $this->filename->saveAs($path);
            $this->real_filename = $this->filename;
            $this->filename = $filename;

            // false because Yii validation of files
            return $this->save(false);
        }

        return false;
    }

}
