<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "notification_token".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property string $os_type
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class NotificationToken extends \yii\db\ActiveRecord
{

    const ANDROID_TYPE = "android";
    const IOS_TYPE = "iOS";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['token', 'os_type'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'token' => 'Token',
            'os_type' => 'OS type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function ()
        {
            return date("Y-m-d H:i:s");
        }
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
