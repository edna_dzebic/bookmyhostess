<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "event_job".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $job_id
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property Event $event
 * @property Job $job
 */
class EventJob extends \common\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_job';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'job_id'], 'required'],
            [['event_id', 'job_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'event_id' => Yii::t('admin', 'Event'),
                'job_id' => Yii::t('admin', 'Job')
            ]
        );
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }


}
