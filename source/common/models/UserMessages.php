<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "user_messages".
 *
 * @property integer $id
 * @property integer $to_user_id
 * @property integer $from_user_id
 * @property string $subject
 * @property string $message
 * @property string $date_created
 * @property integer $accepted
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status_id
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property User $toUser
 * @property User $fromUser
 */
class UserMessages extends \common\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_messages';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_user_id', 'from_user_id', 'subject', 'message'], 'required'],
            [['to_user_id', 'from_user_id', 'accepted', 'created_by', 'updated_by', 'status_id'], 'integer'],
            [['message'], 'string'],
            [['date_created', 'date_updated'], 'safe'],
            [['subject'], 'string', 'max' => 50],
            [['message', 'subject'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'to_user_id' => Yii::t('admin', 'To User ID'),
                'from_user_id' => Yii::t('admin', 'From User ID'),
                'subject' => Yii::t('admin', 'Subject'),
                'message' => Yii::t('admin', 'Message'),
                'accepted' => Yii::t('admin', 'Accepted')
            ]
        );
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }


}
