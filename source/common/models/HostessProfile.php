<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "hostess_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $profile_views
 * @property integer $admin_approved
 * @property string $birth_date
 * @property integer $hair_color_id
 * @property integer $height
 * @property integer $weight
 * @property integer $waist_size
 * @property integer $shoe_size
 * @property integer $has_tattoo
 * @property integer $has_car
 * @property double $cached_rating
 * @property integer $has_driving_licence
 * @property integer $has_trade_licence
 * @property integer $has_health_certificate
 * @property integer $is_trained
 * @property string $education_graduation
 * @property string $education_university
 * @property string $education_vocational_training
 * @property string $education_profession
 * @property string $education_special_knowledge
 * @property string $billing_address
 * @property string $tax_number
 * @property string $date_created
 * @property integer $gender
 * @property string $approved_at
 * @property integer $has_mobile_app
 * @property string $summary
 * @property string $postal_code
 * @property string $city
 * @property integer $country_id
 * @property integer $price_class
 * @property double $profile_completeness_score
 * @property double $profile_reliability_score
 * @property string $date_updated
 * @property integer $updated_by
 * @property integer $status_id
 * @property integer $num_rejections_after_booking
 *
 * @property Country $country
 * @property Status $status
 * @property User $updatedBy
 * @property User $user
 * @property TradeLicenseExtend[] $tradeLicenseExtends
 * @property HostessCountryMapping[] $hostessCountryMappings
 */
class HostessProfile extends \common\components\Model
{

    const APPROVAL_NOT_REQUESTED = 0;
    const APPROVAL_APPROVED = 1;
    const APPROVAL_REQUESTED = 2;

    private $_defaultImageModel;
    public $lang;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hostess_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'profile_views', 'admin_approved', 'hair_color_id', 'height', 'weight', 'waist_size', 'shoe_size', 'has_tattoo', 'has_car', 'has_driving_licence', 'has_trade_licence', 'has_health_certificate', 'is_trained', 'gender', 'has_mobile_app', 'country_id', 'price_class', 'updated_by', 'status_id', 'num_rejections_after_booking'], 'integer'],
            [['birth_date', 'date_created', 'approved_at', 'date_updated'], 'safe'],
            [['cached_rating', 'profile_completeness_score', 'profile_reliability_score'], 'number'],
            [['education_graduation', 'education_university', 'education_vocational_training', 'education_profession', 'education_special_knowledge'], 'string', 'max' => 100],
            [['billing_address', 'tax_number', 'summary', 'postal_code', 'city'], 'string', 'max' => 255],
            [['education_graduation', 'education_university', 'education_vocational_training', 'education_profession', 'education_special_knowledge', 'billing_address', 'tax_number', 'summary', 'postal_code', 'city'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            [['user_id', 'date_created', 'hair_color_id', 'height', 'weight', 'waist_size', 'shoe_size', 'has_tattoo', 'has_car', 'has_driving_licence', 'has_health_certificate', 'postal_code', 'city', 'country_id', 'tax_number', 'billing_address'], 'required', 'on' => 'profile-complete']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User ID'),
            'profile_views' => Yii::t('admin', 'Profile Views'),
            'admin_approved' => Yii::t('admin', 'Admin Approved'),
            'birth_date' => Yii::t('admin', 'Birth Date'),
            'hair_color_id' => Yii::t('admin', 'Hair Color'),
            'height' => Yii::t('admin', 'Height'),
            'weight' => Yii::t('admin', 'Weight'),
            'waist_size' => Yii::t('admin', 'Waist Size'),
            'shoe_size' => Yii::t('admin', 'Shoe Size'),
            'has_tattoo' => Yii::t('admin', 'Has Tattoo'),
            'has_car' => Yii::t('admin', 'Has Car'),
            'cached_rating' => Yii::t('admin', 'Cached Rating'),
            'has_driving_licence' => Yii::t('admin', 'Has Driving Licence'),
            'has_trade_licence' => Yii::t('admin', 'Has Trade Licence'),
            'has_health_certificate' => Yii::t('admin', 'Has Health Certificate'),
            'is_trained' => Yii::t('admin', 'Is Trained'),
            'education_graduation' => Yii::t('admin', 'Education Graduation'),
            'education_university' => Yii::t('admin', 'Education University'),
            'education_vocational_training' => Yii::t('admin', 'Education Vocational Training'),
            'education_profession' => Yii::t('admin', 'Education Profession'),
            'education_special_knowledge' => Yii::t('admin', 'Education Special Knowledge'),
            'billing_address' => Yii::t('admin', 'Billing Address'),
            'tax_number' => Yii::t('admin', 'Tax Number'),
            'gender' => Yii::t('admin', 'Gender'),
            'approved_at' => Yii::t('admin', 'Approved'),
            'has_mobile_admin' => Yii::t('admin', 'Has Mobile App'),
            'summary' => Yii::t('admin', 'Summary'),
            'postal_code' => Yii::t('admin', 'Postal Code'),
            'city' => Yii::t('admin', 'City'),
            'country_id' => Yii::t('admin', 'Country ID'),
            'price_class' => Yii::t('admin', 'Price Class'),
            'profile_completeness_score' => Yii::t('admin', 'Profile Completeness Score'),
            'profile_reliability_score' => Yii::t('admin', 'Profile Reliability Score'),
            'approvalStatusLabel' => Yii::t('admin', 'Approval status')
                ]
        );
    }

    /**
     * Attribute hints
     * @return array
     */
    public function attributeHints()
    {
        return [
            'summary' => Yii::t('app', 'Max. 250 characters'),
            'price_class' => Yii::t('admin', 'Defines which prices can hostess choose. Enter class from 1 to 3 with comma between. Do not put comma after last number. Example: 1,2,3')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTradeLicenseExtends()
    {
        return $this->hasMany(TradeLicenseExtend::className(), ['hostess_profile_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(HostessReview::className(), ['hostess_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages()
    {
        return $this->hasMany(HostessLanguage::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPastWork()
    {
        return $this->hasMany(HostessPastWork::className(), ['user_id' => 'user_id'])->orderBy(['start_date' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(UserImages::className(), ['user_id' => 'user_id'])
                        ->orderBy([
                            "user_images.is_default" => SORT_DESC,
                            "user_images.id" => SORT_DESC
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(UserDocuments::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTradeLicenseExtend()
    {
        return $this->hasMany(TradeLicenseExtend::className(), ['hostess_profile_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['id' => 'event_id'])
                        ->viaTable("register er", ["user_id" => "user_id"]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEvent()
    {
        return $this->hasMany(Event::className(), ['id' => 'event_id'])
                        ->viaTable("register er", ["user_id" => "user_id"]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreferredJobs()
    {
        return $this->hasMany(HostessPreferredJob::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreferredCities()
    {
        return $this->hasMany(HostessPreferredCity::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHairColor()
    {
        return $this->hasOne(HairColor::className(), ['id' => 'hair_color_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessCountryMappings()
    {
        return $this->hasMany(HostessCountryMapping::className(), ['user_id' => 'user_id']);
    }

    /**
     * Is hostess installed mobible app
     * @return boolean
     */
    public function isInstalledMobile()
    {
        return $this->has_mobile_app === 1;
    }

    /**
     * Is profile approved
     * @return bollean
     */
    public function isApproved()
    {
        return $this->admin_approved == static::APPROVAL_APPROVED;
    }

    /**
     * Is provile approve request sent
     * @return boolean
     */
    public function isRequested()
    {
        return $this->admin_approved == static::APPROVAL_REQUESTED;
    }

    /**
     * Is profile approve request not sent
     * @return boolean
     */
    public function isNotRequested()
    {
        return $this->admin_approved == static::APPROVAL_NOT_REQUESTED;
    }

    /**
     * Calculate average time to respond on requests from exhibitors
     * Rounds result
     * @return string
     */
    public function getTimeToRespond()
    {
        $hours = $this->getTimeToRespondValue();

        return $hours . ' ' . \Yii::t('app', 'h');
    }

    /**
     * Return number of hours to respond on request
     * @return integer
     */
    public function getTimeToRespondValue()
    {
        $command = Yii::$app->db->createCommand("SELECT AVG(TIMESTAMPDIFF(SECOND, r.date_created, r.date_responded)) 
from request r
where 
r.date_responded is not null 
and 
r.date_responded <> '0000-00-00 00:00:00'
	and r.user_id= " . $this->user_id);

        $delaySeconds = $command->queryScalar();

        $minutes = $delaySeconds / 60;
        $hours = round($minutes / 60, 2);

        return $hours;
    }

    /**
     * Returns total requests count
     * @return string
     */
    public function getTotalRequestsCount()
    {
        return Request::find()->andWhere(["user_id" => $this->user_id])->count();
    }

    /**
     * Returns total confirmed requests count
     * @return string
     */
    public function getConfirmedRequestsCount()
    {
        // confirmed and expired because expired status is set when request was accepted and it expires after exhibitor not payed for several days
        // paypal pending because when request is sent to paypal to be paid, it can only be request which was previously accepted
        return Request::find()->andWhere([
                            "user_id" => $this->user_id,
                            "status_id" => [
                                Yii::$app->status->request_accept,
                                Yii::$app->status->request_expired,
                                Yii::$app->status->request_paypal_pending,
                                Yii::$app->status->request_exhibitor_withdraw_after_accept
                            ]
                                ]
                        )
                        ->count();
    }

    /**
     * Returns total rejected requests count
     * @return string
     */
    public function getRejectedRequestCount()
    {
        return Request::find()->andWhere([
                    "user_id" => $this->user_id,
                    "status_id" => [
                        Yii::$app->status->request_reject,
                        Yii::$app->status->request_hostess_withdraw_after_accept
                    ]
                ])->count();
    }

    /**
     * Return total count of not answered requests
     * @return string
     */
    public function getNotAnsweredRequestCount()
    {
        return Request::find()->andWhere([
                    "user_id" => $this->user_id,
                    "status_id" => [
                        Yii::$app->status->request_new,
                        Yii::$app->status->request_expired_hostess
                    ]
                ])->count();
    }

    /**
     * Get list of languages mapped to a level in translated form
     * @return array
     */
    public function getLanguageList()
    {
        $result = [];

        if (isset($this->languages))
        {
            foreach ($this->languages as $object)
            {
                $result[$object->language->getTranslatedName()] = $object->level->getTranslatedName();
            }
        }

        return $result;
    }

    /**
     * returns current hostess age integer
     */
    public function getCurrentAge()
    {
        return date_diff(date_create($this->birth_date), date_create('now'))->y;
    }

    /**
     * Returns default image model for current hostess profile model
     * @param boolean $returnFirstIfNotFound
     * @return UserImages|null
     */
    public function getDefaultImageModel($returnFirstIfNotFound = true)
    {
        //if cached, return it
        if ($this->_defaultImageModel !== null)
        {
            return $this->_defaultImageModel;
        }

        if (isset($this->images) && count($this->images) > 0)
        {
            foreach ($this->images as $image)
            {
                //get default if exists
                if ($image->is_default == 1)
                {
                    $this->_defaultImageModel = $image;
                    break;
                }
            }

            //if no default found, get first image
            if ($this->_defaultImageModel === null && $returnFirstIfNotFound)
            {
                $this->_defaultImageModel = $this->images[0];
            }
        }

        return $this->_defaultImageModel;
    }

    /**
     * Returns cached image of hostess of default image
     * Used on views for easy listing 
     * 
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getCachedImage($width, $height)
    {
        $model = $this->getDefaultImageModel(true);
        if ($model)
        {
            return $model->getCachedImage($width, $height);
        }
        return false;
    }

    /**
     * Get url for thumbnail image
     * @param int $w
     * @param int $h
     * @return string|null
     */
    public function getThumbnailImageUrl($w, $h)
    {
        $defaultImage = $this->getDefaultImageModel(true);

        //if no image found, return null
        if ($defaultImage === null)
        {
            return null;
        }

        return \common\components\PhpThumb::Url([
                    'src' => Yii::$app->params['user_images_url'] . $defaultImage->image_url,
                    'w' => $w,
                    'h' => $h,
                    'zc' => 'T',
                    'q' => 90,
        ]);
    }

    public function getLangValue()
    {
        if (isset($this->user) && isset($this->user->lang))
        {
            if (isset(Yii::$app->params['user_languages'][$this->user->lang]))
            {
                return Yii::$app->params['user_languages'][$this->user->lang];
            }
        }
        return '';
    }

    /**
     * Recalcuate profile completeness index
     * @return boolean
     */
    public function recalculateProfileCompleteness()
    {
        $score = 0;

        if (isset($this->images) &&
                count($this->images) >= Yii::$app->settings->pcs_images_min_cnt)
        {
            $score = $score + Yii::$app->settings->pcs_images;
        }

        if ($this->hasCompletedBasicProfile())
        {
            $score = $score + Yii::$app->settings->pcs_basic_profile;
        }

        if (isset($this->languages) &&
                count($this->languages) >= Yii::$app->settings->pcs_languages_min_cnt)
        {
            $score = $score + Yii::$app->settings->pcs_languages;
        }

        if (!empty($this->summary))
        {
            $score = $score + Yii::$app->settings->pcs_summary;
        }

        if (isset($this->pastWork) &&
                count($this->pastWork) >= Yii::$app->settings->pcs_past_work_min_cnt)
        {
            $score = $score + Yii::$app->settings->pcs_work_experience;
        }

        if ($this->hasCompleteEducation())
        {
            $score = $score + Yii::$app->settings->pcs_education;
        }

        if ($this->isInstalledMobile())
        {
            $score = $score + Yii::$app->settings->pcs_mobile_app;
        }

        if (isset($this->preferredCities) &&
                count($this->preferredCities) >= Yii::$app->settings->pcs_preferred_cities_min_cnt)
        {
            $score = $score + Yii::$app->settings->pcs_preferred_cities;
        }

        if (isset($this->preferredJobs) &&
                count($this->preferredJobs) >= Yii::$app->settings->pcs_preferred_jobs_min_cnt)
        {
            $score = $score + Yii::$app->settings->pcs_preferred_jobs;
        }

        $this->profile_completeness_score = $score;

        return $this->recalculateProfileReliability();
    }

    /**
     * Recalculate profile reliability index
     * @return boolean
     */
    public function recalculateProfileReliability()
    {
        $time = $this->getTimeToRespond();
        $total = $this->getTotalRequestsCount();
        $notAnswered = $this->getNotAnsweredRequestCount();
        $rejected = $this->getRejectedRequestCount();
        $bookedCount = $this->getBookedRequestCount();
        $reviewCount = HostessReview::find()
                        ->andWhere(["hostess_id" => $this->user_id])
                        ->andWhere(["status_id" => Yii::$app->status->active])->count();

        $score = 0;

        if ($this->profile_completeness_score == 100)
        {
            $score = $score + Yii::$app->settings->prs_profile_completeness;
        }

        if ($bookedCount == Yii::$app->settings->prs_booked_count_min_cnt)
        {
            $score = $score + 0.5 * Yii::$app->settings->prs_booked_count;
        }
        if ($bookedCount > Yii::$app->settings->prs_booked_count_min_cnt)
        {
            $score = $score + Yii::$app->settings->prs_booked_count;
        }

        if ($reviewCount == Yii::$app->settings->prs_review_count_min_cnt)
        {
            $score = $score + 0.5 * Yii::$app->settings->prs_review_count;
        }
        if ($reviewCount > Yii::$app->settings->prs_review_count_min_cnt)
        {
            $score = $score + Yii::$app->settings->prs_review_count;
        }


        if ($total > 0)
        {
            foreach (Yii::$app->settings->getTimeLevelsWithCoefficients() as $level => $coeff)
            {
                if ($time <= $level)
                {
                    $score = $score + $coeff * Yii::$app->settings->prs_avg_answer_time_percent;
                    break;
                }
            }

            if ($notAnswered == 0)
            {
                $score = $score + Yii::$app->settings->prs_no_answer_count;
            } else
            {
                $notAnsweredRatio = ($notAnswered / $total) * 100;
                if ($notAnsweredRatio <= 5)
                {
                    $score = $score + 0.5 * Yii::$app->settings->prs_no_answer_count;
                }
            }

            $rejectedRatio = ($rejected / $total) * 100;

            if ($rejectedRatio <= 5)
            {
                $score = $score + Yii::$app->settings->prs_reject_count;
            }
            if ($rejectedRatio > 5 && $rejectedRatio <= 10)
            {
                $score = $score + 0.5 * Yii::$app->settings->prs_reject_count;
            }
        }

        $score = $score - ($this->num_rejections_after_booking * Yii::$app->settings->prs_rejections_after_booking);

        $this->profile_reliability_score = $score;

        $this->recalculatePriceClass();

        return $this->save(false);
    }

    private function recalculatePriceClass()
    {
        $time = $this->getTimeToRespond();
        $total = $this->getTotalRequestsCount();
        $notAnswered = $this->getNotAnsweredRequestCount();
        $rejected = $this->getRejectedRequestCount();
        $bookedCount = $this->getBookedRequestCount();
        $reviewCount = HostessReview::find()
                        ->andWhere(["hostess_id" => $this->user_id])
                        ->andWhere(["status_id" => Yii::$app->status->active])->count();

        $score = $this->profile_completeness_score;

        if ($score == 100)
        {
            $classCandidate = 1;
        } elseif ($score >= 80)
        {
            $classCandidate = 2;
        } else
        {
            $classCandidate = 3;
        }

        $calculatedClass = 3;
        switch ($classCandidate)
        {
            case 1:
                $tempSum = 0;
                if ($bookedCount >= 3)
                {
                    $tempSum++;
                }
                if ($reviewCount >= 3)
                {
                    $tempSum++;
                }
                if ($time <= 4)
                {
                    $tempSum++;
                }

                if ($total > 0)
                {
                    if ($notAnswered == 0)
                    {
                        $tempSum++;
                    }

                    $rejectedRatio = ($rejected / $total) * 100;
                    if ($rejectedRatio <= 5)
                    {
                        $tempSum++;
                    }
                }

                if ($tempSum >= 5)
                {
                    $calculatedClass = 1;
                } else
                {
                    $calculatedClass = 2;
                }

                break;
            case 2;
                $tempSum = 0;
                if ($bookedCount >= 1)
                {
                    $tempSum++;
                }
                if ($reviewCount >= 1)
                {
                    $tempSum++;
                }
                if ($time <= 24)
                {
                    $tempSum++;
                }

                if ($total > 0)
                {
                    $notAnsweredRatio = ($notAnswered / $total) * 100;
                    if ($notAnsweredRatio <= 5)
                    {
                        $tempSum++;
                    }
                    $rejectedRatio = ($rejected / $total) * 100;
                    if ($rejectedRatio <= 10)
                    {
                        $tempSum++;
                    }
                }

                if ($tempSum >= 5)
                {
                    $calculatedClass = 2;
                } else
                {
                    $calculatedClass = 3;
                }
                break;
            default:
                $calculatedClass = 3;
                break;
        }

        $this->price_class = $calculatedClass;
        return $this->save(false);
    }

    /**
     * Did hostess entered all values to education
     * @return boolean
     */
    private function hasCompleteEducation()
    {
        if (
                !empty($this->education_graduation) ||
                !empty($this->education_university) ||
                !empty($this->education_special_knowledge) ||
                !empty($this->education_vocational_training) ||
                !empty($this->education_profession)
        )
        {
            return true;
        }

        return false;
    }

    /**
     * Is hostess profile model valid
     * @return boolean
     */
    public function isValid()
    {
        if (count($this->getErrors()) > 0)
        {
            return false;
        }
        return true;
    }

    /**
     * Returns total booked requests count
     * @return string
     */
    public function getBookedRequestCount()
    {
        return Request::find()
                        ->andWhere(["user_id" => $this->user_id, "completed" => 1])
                        ->count();
    }

    /**
     * Is hostess address complete
     * @return boolean
     */
    public function isAddressComplete()
    {
        if (empty($this->billing_address) ||
                empty($this->tax_number) ||
                empty($this->city) ||
                empty($this->postal_code) ||
                !isset($this->country_id) ||
                !isset($this->user) ||
                empty($this->user->phone_number)
        )
        {
            return false;
        }

        return true;
    }

    /**
     * Recalculate rating after saving review
     * @return boolean
     */
    public function recalculateRating()
    {
        // Prepare query
        $query = HostessReview::find()
                ->andWhere(["hostess_id" => $this->user_id])
                ->andWhere(["status_id" => Yii::$app->status->active]);

        // Get data for calculation
        $sumRating = $query->sum("rating");
        $count = $query->count();

        // Update cached query
        $this->cached_rating = $count > 0 ? $sumRating / $count : 0;

        $this->save();

        return $this->recalculateProfileReliability();
    }

    /**
     * Does hostess have completed basic profile
     * @return boolean
     */
    private function hasCompletedBasicProfile()
    {
        // fields from registration form are not validated because they are required there
        if (
                isset($this->hair_color_id) &&
                isset($this->has_car) &&
                isset($this->has_driving_licence) &&
                isset($this->height) &&
                isset($this->weight) &&
                isset($this->waist_size) &&
                isset($this->shoe_size) &&
                isset($this->has_tattoo) &&
                isset($this->has_health_certificate) &&
                isset($this->billing_address) &&
                isset($this->tax_number) &&
                isset($this->postal_code) &&
                isset($this->city) &&
                isset($this->country_id)
        )
        {
            return true;
        }

        return false;
    }

    /**
     * Planned for future
     * @return boolean
     */
    public function isPremium()
    {
        if ($this->is_trained === 1)
        {
            return true;
        }
        return false;
    }

    /**
     * Get approval label status for admin
     * @return string
     */
    public function getApprovalStatusLabel()
    {
        if ($this->isApproved())
        {
            return Yii::t('admin', 'Approved');
        }

        if ($this->isRequested())
        {
            return Yii::t('admin', 'Requested');
        }

        if ($this->isNotRequested())
        {
            return Yii::t('admin', 'Not Requested');
        }
    }

    /**
     * Return imploded list of preferred jobs with translation
     * @return string
     */
    public function getTranslatedPreferredJobs()
    {
        $result = [];

        if (!empty($this->preferredJobs))
        {
            foreach ($this->preferredJobs as $job)
            {
                $result[] = $job->eventJob->getTranslatedName();
            }
        }

        return implode(", ", $result);
    }

    /**
     * Activate profile
     * @return boolean
     */
    public function activate()
    {
        $this->status_id = Yii::$app->status->active;

        return $this->update() !== false;
    }

}
