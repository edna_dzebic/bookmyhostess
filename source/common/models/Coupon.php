<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "coupon".
 *
 * @property integer $id
 * @property string $code
 * @property string $date_from
 * @property string $date_to
 * @property string $amount
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $coupon_type
 * @property integer $status_id
 * @property string $title
 * @property string $bar_code
 *
 * @property Status $status
 * @property User $createdBy
 * @property User $updatedBy
 * @property Invoice[] $invoices
 */
class Coupon extends \common\components\Model
{

    const TYPE_FIX = 0;
    const TYPE_PERECENTAGE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'amount', 'bar_code'], 'required'],
            [['date_from', 'date_to', 'date_created', 'date_updated'], 'safe'],
            [['amount'], 'number'],
            [['created_by', 'updated_by', 'coupon_type', 'status_id'], 'integer'],
            [['code', 'title', 'bar_code'], 'string', 'max' => 255],
            [['code', 'title', 'bar_code'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            ['code', 'unique'],
            ['bar_code', 'unique'],
            ['date_from', 'compare', 'compareAttribute' => 'date_to', 'operator' => '<', 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'code' => Yii::t('admin', 'Code'),
            'date_from' => Yii::t('admin', 'Date From'),
            'date_to' => Yii::t('admin', 'Date To'),
            'amount' => Yii::t('admin', 'Amount'),
            'coupon_type' => Yii::t('admin', 'Coupon Type'),
            'typeLabel' => Yii::t('admin', 'Coupon Type'),
            'title' => Yii::t('admin', 'Title'),
            'bar_code' => Yii::t('admin', 'Bar Code'),
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['coupon_id' => 'id']);
    }

    /**
     * Is type fixed amounnt
     * @return boolean
     */
    public function getIsFixed()
    {
        return $this->coupon_type == static::TYPE_FIX;
    }

    /**
     * Is type percentage amount
     * @return boolean
     */
    public function getIsPercentage()
    {
        return $this->coupon_type == static::TYPE_PERECENTAGE;
    }

    /**
     * Get type label for display
     * @return string
     */
    public function getTypeLabel()
    {
        if ($this->isFixed)
        {
            return Yii::t('admin', 'Fixed amount');
        }
        if ($this->isPercentage)
        {
            return Yii::t('admin', 'Percentage amount');
        }

        return '';
    }

    /**
     * Get amount label for display
     * @return string
     */
    public function getAmountLabel()
    {
        if ($this->isFixed)
        {
            return "-" . $this->amount;
        }
        if ($this->isPercentage)
        {
            return "-" . $this->amount . " %";
        }
        return '';
    }

}
