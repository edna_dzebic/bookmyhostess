<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "affiliate".
 *
 * @property integer $id
 * @property string $identification
 * @property integer $provision
 * @property string $name
 * @property string $description
 * @property string $tax_number
 * @property string $street
 * @property string $zip
 * @property string $city
 * @property string $phone
 * @property string $email
 * @property string $web
 * @property string $fax
 * @property string $contact
 * @property integer $country_id
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Country $country
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property Request[] $requests
 */
class Affiliate extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'affiliate';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'sluggable' => [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'name',
                'slugAttribute' => 'identification',
                'ensureUnique' => true,
                'immutable' => true
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['identification'], 'unique'],
            [['identification', 'name', 'provision'], 'required'],
            [['country_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['identification', 'name', 'description', 'tax_number', 'street', 'zip', 'city', 'phone', 'email', 'web', 'fax', 'contact'], 'string', 'max' => 255],
            [['identification', 'name', 'description', 'tax_number', 'street', 'zip', 'city', 'phone', 'email', 'web', 'fax', 'contact'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'identification' => Yii::t('admin', 'Identification'),
            'name' => Yii::t('admin', 'Name'),
            'provision' => Yii::t('admin', 'Provision'),
            'description' => Yii::t('admin', 'Description'),
            'tax_number' => Yii::t('admin', 'Tax Number'),
            'street' => Yii::t('admin', 'Street'),
            'zip' => Yii::t('admin', 'Zip'),
            'city' => Yii::t('admin', 'City'),
            'phone' => Yii::t('admin', 'Phone'),
            'email' => Yii::t('admin', 'Email'),
            'web' => Yii::t('admin', 'Web'),
            'fax' => Yii::t('admin', 'Fax'),
            'contact' => Yii::t('admin', 'Contact Person'),
            'country_id' => Yii::t('admin', 'Country ID'),
            'countryName' => Yii::t('admin', 'Country'),
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['affiliate_id' => 'id'])
                        ->orderBy(['request.date_created' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Request::className(), ['affiliate_id' => 'id'])
                        ->andWhere(['request.completed' => 1])
                        ->orderBy(['request.date_created' => SORT_DESC]);
    }

    /**
     * Get country name
     * @return string
     */
    public function getCountryName()
    {
        return is_object($this->country) ? $this->country->name : '';
    }

}
