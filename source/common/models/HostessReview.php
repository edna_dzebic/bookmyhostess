<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "hostess_review".
 *
 * @property integer $id
 * @property integer $hostess_id
 * @property integer $exhibitor_id
 * @property integer $event_id
 * @property integer $event_request_id
 * @property string $group_code
 * @property string $rating
 * @property string $comment
 * @property integer $review_requested
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status_id
 *
 * @property User $createdBy
 * @property Event $event
 * @property Request $eventRequest
 * @property User $exhibitor
 * @property User $hostess
 * @property Status $status
 * @property User $updatedBy
 */
class HostessReview extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hostess_review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hostess_id', 'exhibitor_id', 'event_id', 'group_code'], 'required'],
            [['rating'], 'required', 'on' => 'update'],
            [['comment'], 'required', 'on' => 'update'],
            [['hostess_id', 'exhibitor_id', 'event_id', 'event_request_id', 'review_requested', 'created_by', 'updated_by', 'status_id'], 'integer'],
            [['rating'], 'number'],
            [['date_created', 'date_updated'], 'safe'],
            [['group_code', 'comment'], 'string', 'max' => 255],
            [['group_code', 'comment'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'hostess_id' => Yii::t('admin', 'Hostess'),
            'exhibitor_id' => Yii::t('admin', 'Exhibitor'),
            'event_id' => Yii::t('admin', 'Event'),
            'event_request_id' => Yii::t('admin', 'Event Request'),
            'group_code' => Yii::t('admin', 'Group Code'),
            'rating' => Yii::t('admin', 'Rating'),
            'comment' => Yii::t('admin', 'Comment'),
            'review_requested' => Yii::t('admin', 'Review Requested')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'event_request_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExhibitor()
    {
        return $this->hasOne(User::className(), ['id' => 'exhibitor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostess()
    {
        return $this->hasOne(User::className(), ['id' => 'hostess_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Is review in pending state
     * @return boolean
     */
    public function getIsPending()
    {
        return $this->status_id == Yii::$app->status->pending;
    }

    /**
     * Is review in active state
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->status_id == Yii::$app->status->active;
    }

    /**
     * 
     * @return string
     */
    public function getEventName()
    {
        if (is_object($this->event))
        {
            return $this->event->name;
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getHostessFullName()
    {
        if (is_object($this->hostess))
        {
            return $this->hostess->getFullName();
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getExhibitorFullName()
    {
        if (is_object($this->exhibitor))
        {
            return $this->exhibitor->getFullName();
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getStatusLabel()
    {
        if ($this->status_id === Yii::$app->status->pending)
        {
            return Yii::t('admin', 'Pending');
        }
        if ($this->status_id === Yii::$app->status->active)
        {
            return Yii::t('admin', 'Active');
        }

        return Yii::t('admin', 'Deleted');
    }

}
