<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "language".
 *
 * @property integer $id
 * @property string $name
 * @property integer $frontend_selectable
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property HostessLanguage[] $hostessLanguages
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 */
class Language extends \common\components\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['frontend_selectable', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'name' => Yii::t('admin', 'Name'),
            'frontend_selectable' => Yii::t('admin', 'Frontend Selectable')
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessLanguages()
    {
        return $this->hasMany(HostessLanguage::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Get translated name
     * @return string
     */
    public function getTranslatedName()
    {
        return Yii::t('app.languages', $this->name);
    }

    /**
     * Get list of languages for preferred system language
     * @return array
     */
    public static function getPrefferedLanguageList()
    {
        return [
            'en' => Yii::t('app.languages', 'English'),
            'de' => Yii::t('app.languages', 'German'),
            'es' => Yii::t('app.languages', 'Spanish'),
            'tr' => Yii::t('app.languages', 'Turkish'),
            'bs' => Yii::t('app.languages', 'Bosnian'),
            'it' => Yii::t('app.languages', 'Italian'),
            'fr' => Yii::t('app.languages', 'French'),
            'ru' => Yii::t('app.languages', 'Russian'),
            'pl' => Yii::t('app.languages', 'Polish'),
            'pt' => Yii::t('app.languages', 'Portuguese')
        ];
    }

}
