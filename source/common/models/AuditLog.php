<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "audit_log".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $page
 * @property string $page_item_id
 * @property string $action
 * @property string $ip
 * @property string $url
 * @property string $get
 * @property string $post
 * @property string $message
 * @property string $date_created
 *
 * @property User $user
 */
class AuditLog extends \common\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audit_log';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['page_item_id', 'url', 'get', 'post', 'message'], 'string'],
            [['date_created'], 'safe'],
            [['page', 'action', 'ip'], 'string', 'max' => 255],
            [['page_item_id', 'url', 'get', 'post', 'message'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            [['page', 'action', 'ip'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'user_id' => Yii::t('admin', 'User ID'),
                'page' => Yii::t('admin', 'Page'),
                'page_item_id' => Yii::t('admin', 'Page Item ID'),
                'action' => Yii::t('admin', 'Action'),
                'ip' => Yii::t('admin', 'Ip'),
                'url' => Yii::t('admin', 'Url'),
                'get' => Yii::t('admin', 'Get'),
                'post' => Yii::t('admin', 'Post'),
                'message' => Yii::t('admin', 'Message')
            ]
        );
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


}
