<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "hostess_language".
 *
 * @property integer $id
 * @property integer $language_id
 * @property integer $user_id
 * @property integer $level_id
 * @property integer $status_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property LanguageLevel $level
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property Language $language
 * @property User $user
 */
class HostessLanguage extends \common\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hostess_language';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'user_id', 'level_id'], 'required'],
            [['language_id', 'user_id', 'level_id', 'status_id', 'created_by', 'updated_by'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'language_id' => Yii::t('admin', 'Language'),
                'user_id' => Yii::t('admin', 'User'),
                'level_id' => Yii::t('admin', 'Level')
            ]
        );
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(LanguageLevel::className(), ['id' => 'level_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


}
