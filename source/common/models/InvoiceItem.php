<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "invoice_item".
 *
 * @property integer $id
 * @property integer $invoice_id
 * @property string $event_name
 * @property integer $number_of_days
 * @property string $booking_price_per_day
 * @property string $amount
 * @property string $from_date
 * @property string $to_date
 * @property string $hostess_name
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status_id
 * @property integer $is_last_minute
 *
 * @property User $createdBy
 * @property Status $status
 * @property User $updatedBy
 * @property Invoice $invoice
 */
class InvoiceItem extends \common\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_item';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id'], 'required'],
            [['invoice_id', 'number_of_days', 'created_by', 'updated_by', 'status_id', 'is_last_minute'], 'integer'],
            [['booking_price_per_day', 'amount'], 'number'],
            [['from_date', 'to_date', 'date_created', 'date_updated'], 'safe'],
            [['event_name', 'hostess_name'], 'string', 'max' => 255],
            [['event_name', 'hostess_name'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'invoice_id' => Yii::t('admin', 'Invoice ID'),
                'event_name' => Yii::t('admin', 'Event Name'),
                'number_of_days' => Yii::t('admin', 'Number Of Days'),
                'booking_price_per_day' => Yii::t('admin', 'Booking Price Per Day'),
                'amount' => Yii::t('admin', 'Amount'),
                'from_date' => Yii::t('admin', 'From Date'),
                'to_date' => Yii::t('admin', 'To Date'),
                'hostess_name' => Yii::t('admin', 'Hostess Name'),
                'is_last_minute' => Yii::t('admin', 'Is last minute')
            ]
        );
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }


}
