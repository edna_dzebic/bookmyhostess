<?php

namespace common\models;

use Yii;

/**
 * This is the generated model class for table "request".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $user_id
 * @property integer $requested_user_id
 * @property integer $event_dress_code
 * @property integer $completed
 * @property integer $invoice_id
 * @property integer $status_id
 * @property string $note
 * @property string $date_created
 * @property string $date_from
 * @property string $date_to
 * @property string $date_responded
 * @property decimal $price
 * @property decimal $hostess_original_price
 * @property string $reason
 * @property string $date_rejected
 * @property string $ex_withdraw_reason
 * @property string $ex_withdraw_date
 * @property string $hostess_withdraw_reason
 * @property string $hostess_withdraw_date
 * @property integer $number_of_days
 * @property decimal $booking_price_per_day
 * @property decimal $original_booking_price_per_day
 * @property decimal $total_booking_price
 * @property string  $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $days_to_event
 * @property boolean $is_last_minute
 * @property integer $last_minute_percentage
 * @property integer $payment_type
 * @property integer $affiliate_id
 *
 * @property HostessReview[] $hostessReviews
 * @property User $createdBy
 * @property Affiliate $affiliate
 * @property Status $status
 * @property User $updatedBy
 * @property Event $event
 * @property DressCode $eventDressCode
 * @property User $user
 * @property Invoice $invoice
 * @property User $requestedUser
 * @property User $userRequested
 * @property RequestJob[] $requestJobs
 */
class Request extends \common\components\Model
{

    // completion status
    const NOT_COMPLETED = 0;
    const COMPLETED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'user_id', 'requested_user_id', 'event_dress_code', 'number_of_days', 'booking_price_per_day', 'total_booking_price', 'price', 'payment_type', 'hostess_original_price', 'original_booking_price_per_day', 'days_to_event', 'is_last_minute', 'last_minute_percentage'], 'required'],
            [['event_id', 'user_id', 'requested_user_id', 'event_dress_code', 'completed', 'invoice_id', 'status_id', 'number_of_days', 'created_by', 'updated_by', 'affiliate_id'], 'integer'],
            [['date_created', 'date_from', 'date_to', 'date_responded', 'date_rejected', 'hostess_withdraw_reason', 'hostess_withdraw_date', 'date_updated'], 'safe'],
            [['booking_price_per_day', 'total_booking_price', 'price'], 'number'],
            [['note'], 'string', 'max' => 50],
            [['reason', 'ex_withdraw_reason', 'ex_withdraw_date'], 'string', 'max' => 255],
            [['note', 'reason', 'ex_withdraw_reason', 'ex_withdraw_date'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            ['date_from', 'compare', 'compareAttribute' => 'date_to', 'operator' => '<=', 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'event_id' => Yii::t('admin', 'Event'),
            'eventName' => Yii::t('admin', 'Event'),
            'user_id' => Yii::t('admin', 'Hostess'),
            'hostessFullName' => Yii::t('admin', 'Hostess'),
            'hostessEmail' => Yii::t('admin', 'Hostess Email'),
            'hostessPhone' => Yii::t('admin', 'Hostess Phone'),
            'exhibitorFullName' => Yii::t('admin', 'Exhibitor'),
            'exhibitorEmail' => Yii::t('admin', 'Exhibitor Email'),
            'completedDate' => Yii::t('admin', 'Booking date'),
            'requested_user_id' => Yii::t('admin', 'Exhibitor'),
            'event_dress_code' => Yii::t('admin', 'Dress Code'),
            'completed' => Yii::t('admin', 'Completed'),
            'completedLabel' => Yii::t('admin', 'Completed'),
            'invoice_id' => Yii::t('admin', 'Invoice'),
            'status_id' => Yii::t('admin', 'Status'),
            'note' => Yii::t('admin', 'Note'),
            'date_from' => Yii::t('admin', 'Date From'),
            'date_to' => Yii::t('admin', 'Date To'),
            'date_responded' => Yii::t('admin', 'Date Responded'),
            'price' => Yii::t('admin', 'Price'),
            'reason' => Yii::t('admin', 'Reason'),
            'date_rejected' => Yii::t('admin', 'Date Rejected'),
            'ex_withdraw_reason' => Yii::t('admin', 'Exhibitor Withdraw Reason'),
            'ex_withdraw_date' => Yii::t('admin', 'Exhibitor Withdraw Date'),
            'hostess_withdraw_reason' => Yii::t('admin', 'Hostess Withdraw Reason'),
            'hostess_withdraw_date' => Yii::t('admin', 'Hostess Withdraw Date'),
            'number_of_days' => Yii::t('admin', 'Number Of Days'),
            'booking_price_per_day' => Yii::t('admin', 'Booking Price Per Day'),
            'total_booking_price' => Yii::t('admin', 'Total Booking Price'),
            'jobName' => Yii::t('admin', 'Jobs'),
            'normalizedJobName' => Yii::t('admin', 'Jobs'),
            'dressCodeName' => Yii::t('admin', 'Dress Code'),
            'normalizedDressCodeName' => Yii::t('admin', 'Dress Code'),
            'payment_type' => Yii::t('admin', 'Payment of the daily rate'),
            'paymentTypeLabel' => Yii::t('admin', 'Payment of the daily rate'),
            'is_last_minute' => Yii::t('admin', 'Is last minute'),
            'lastMinuteLabel' => Yii::t('admin', 'Is last minute'),
            'affiliateName' => Yii::t('admin', 'Affiliate'),
                ]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessReview()
    {
        return $this->hasOne(HostessReview::className(), ['event_request_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAffiliate()
    {
        return $this->hasOne(Affiliate::className(), ['id' => 'affiliate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDressCode()
    {
        return $this->hasOne(DressCode::className(), ['id' => 'event_dress_code']);
    }

    /**
     * For backward compatibility!
     * @return \yii\db\ActiveQuery
     */
    public function getDressCode()
    {
        return $this->getEventDressCode();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'requested_user_id']);
    }

    /**
     * Do not delete. Used for backwards compatibility
     * @return \yii\db\ActiveQuery
     */
    public function getUserRequested()
    {
        return $this->getRequestedUser();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestJobs()
    {
        return $this->hasMany(RequestJob::className(), ['event_request_id' => 'id']);
    }

    /**
     * @return float
     */
    public function calculateBookingPrice()
    {
        return round($this->number_of_days * $this->booking_price_per_day, 2);
    }

    public function calculateBookingDays()
    {
        $zone = new \DateTimeZone('UTC');
        $start = new \DateTime($this->date_from, $zone);
        $end = new \DateTime($this->date_to, $zone);

        $start->setTimeZone($zone);
        $end->setTimeZone($zone);

        $diff = $end->diff($start)->format("%a") + 1;
        return $diff;
    }

    public function getTotalBookingDays()
    {
        return $this->number_of_days;
    }

    public function getIsNew()
    {
        return $this->status_id == Yii::$app->status->request_new;
    }

    public function getIsRejected()
    {
        return $this->status_id == Yii::$app->status->request_reject;
    }

    public function getIsHostessWithdrawAfterAccept()
    {
        return $this->status_id == Yii::$app->status->request_hostess_withdraw_after_accept;
    }

    public function getIsExWithdraw()
    {
        return $this->status_id == Yii::$app->status->request_exhibitor_withdraw;
    }

    public function getIsExWithdrawAfterAccept()
    {
        return $this->status_id == Yii::$app->status->request_exhibitor_withdraw_after_accept;
    }

    public function getIsAccepted()
    {
        return $this->status_id == Yii::$app->status->request_accept;
    }

    public function getIsPaymentPending()
    {
        return $this->status_id == Yii::$app->status->request_paypal_pending;
    }

    /**
     * TODO !!!
     * @return array
     */
    public function getRejectionReasons()
    {
        return [
            "payment_type" => Yii::t('app.hostess', 'Payment of the daily rate'),
            "accepted_different_offer" => Yii::t('app.hostess', 'I have accepted different offer'),
            "dress_code" => Yii::t('app.hostess', 'I am not satisfied with suggested dress code'),
            "not_good_offer" => Yii::t('app.hostess', 'I dont like the offer anymore'),
            "not_available" => Yii::t('app.hostess', 'I am not available during the days listed in offer'),
            "other" => Yii::t('app.hostess', 'Other'),
        ];
    }

    /**
     * TODO !!!
     * @return string
     */
    public function getRejectionText()
    {
        if (!(empty($this->reason)))
        {
            $reasons = $this->getRejectionReasons();
            return isset($reasons[$this->reason]) ?
                    $reasons[$this->reason] : Yii::t('app', 'Other');
        }

        return Yii::t('app', 'Other');
    }

    /**
     * 
     * @return string
     */
    public function getWithdrawText()
    {
        if (!(empty($this->hostess_withdraw_reason)))
        {
            $reasons = $this->getRejectionReasons();
            return isset($reasons[$this->hostess_withdraw_reason]) ?
                    $reasons[$this->hostess_withdraw_reason] : Yii::t('app', 'Other');
        }

        return Yii::t('app', 'Other');
    }

    /**
     * TODO !!!!!!
     * @return array
     */
    public function getExhibitorWithdrawReasons()
    {
        return [
            "another_hostess_confirmed_earlier" => Yii::t('app.exhibitor', 'I have booked another user'),
            "made_mistake" => Yii::t('app.exhibitor', 'I made a mistake'),
            "changed_mind" => Yii::t('app.exhibitor', 'I have changed my mind'),
            "change_dates" => Yii::t('app.exhibitor', "I'd like to change the dates"),
            "other" => Yii::t('app.exhibitor', 'Other Reason'),
        ];
    }

    /**
     * TODO !!!!!!
     * @return string
     */
    public function getExhibitorWithdrawText()
    {
        if (!(empty($this->ex_withdraw_reason)))
        {
            $reasons = $this->getExhibitorWithdrawReasons();
            return isset($reasons[$this->ex_withdraw_reason]) ?
                    $reasons[$this->ex_withdraw_reason] : Yii::t('app', 'Other Reason');
        }

        return Yii::t('app', 'Other Reason');
    }

    /**
     * 
     * @return boolean
     */
    public function getIsResponded()
    {
        // Stupid, but...
        if ($this->date_responded !== '0000-00-00 00:00:00')
        {
            return true;
        }

        return false;
    }

    /**
     * 
     * @return string
     */
    public function getCompletedLabel()
    {
        if ($this->completed === 1)
        {
            return Yii::t('admin', 'Yes');
        }

        return Yii::t('admin', 'No');
    }

    /**
     * 
     * @return string
     */
    public function getLastminuteLabel()
    {
        if ($this->is_last_minute === 1)
        {
            return Yii::t('admin', 'Yes');
        }

        return Yii::t('admin', 'No');
    }

    /**
     * 
     * @return string
     */
    public function getStatusLabel()
    {
        $text = '';

        switch ($this->status_id)
        {
            case Yii::$app->status->request_accept:
                $text = Yii::t('admin', 'Hostess accepted');
                break;
            case Yii::$app->status->request_reject:
                $text = Yii::t('admin', 'Hostess rejected');
                break;
            case Yii::$app->status->request_expired:
                $text = Yii::t('admin', 'Expired: exhibitor');
                break;
            case Yii::$app->status->request_new:
                $text = Yii::t('admin', 'New');
                break;
            case Yii::$app->status->request_paypal_pending:
                $text = Yii::t('admin', 'Waiting Paypal Info');
                break;
            case Yii::$app->status->request_expired_hostess:
                $text = Yii::t('admin', 'Expired: hostess');
                break;
            case Yii::$app->status->request_exhibitor_withdraw:
                $text = Yii::t('admin', 'Withdraw: exhibitor');
                break;
            case Yii::$app->status->request_exhibitor_withdraw_after_accept:
                $text = Yii::t('admin', 'Withdraw: exhibitor after accept');
                break;
            case Yii::$app->status->request_hostess_withdraw_after_accept:
                $text = Yii::t('admin', 'Withdraw: hostess after accept');
                break;
            default :
                $text = '';
                break;
        }

        return $text;
    }

    /**
     * Get event name
     * @return string
     */
    public function getEventName()
    {
        if (is_object($this->event))
        {
            return $this->event->name;
        }

        return '';
    }

    /**
     * Get hostess full name
     * @return string
     */
    public function getHostessFullName()
    {
        if (is_object($this->user))
        {
            return $this->user->getFullName();
        }

        return '';
    }

    /**
     * Get hostess email
     * @return string
     */
    public function getHostessEmail()
    {
        if (is_object($this->user))
        {
            return $this->user->email;
        }

        return '';
    }

    /**
     * Get hostess phone number
     * @return string
     */
    public function getHostessPhone()
    {
        if (is_object($this->user))
        {
            return $this->user->phone_number;
        }

        return '';
    }

    /**
     * Get exhibitor full name
     * @return string
     */
    public function getExhibitorFullName()
    {
        if (is_object($this->requestedUser))
        {
            return $this->requestedUser->getFullName();
        }

        return '';
    }

    /**
     * Get exhibitor email
     * @return string
     */
    public function getExhibitorEmail()
    {
        if (is_object($this->requestedUser))
        {
            return $this->requestedUser->email;
        }

        return '';
    }

    /**
     * Returs all jobs as imploded sring with translation
     * @return string
     */
    public function getJobName()
    {
        $eventJobs = $this->requestJobs;
        $result = [];

        foreach ($eventJobs as $eventJob)
        {
            $result[] = $eventJob->eventJob->getTranslatedName();
        }

        return implode(', ', $result);
    }

    /**
     * Return translated dress code name
     * @return string
     */
    public function getDressCodeName()
    {
        if (is_object($this->eventDressCode))
        {
            return $this->eventDressCode->getTranslatedName();
        }
        return '';
    }

    /**
     * 
     * @return string
     */
    public function getNormalizedDressCodeName()
    {
        if (is_object($this->eventDressCode))
        {
            return $this->eventDressCode->name;
        }
        return '';
    }

    /**
     * Returs all jobs as imploded sring
     * @return string
     */
    public function getNormalizedJobName()
    {
        $eventJobs = $this->requestJobs;
        $result = [];

        foreach ($eventJobs as $eventJob)
        {
            $result[] = $eventJob->eventJob->name;
        }

        return implode(', ', $result);
    }

    public function getReviewForHostessExists()
    {
        $model = HostessReview::findOne(
                        [
                            "hostess_id" => Yii::$app->user->id,
                            "event_request_id" => $this->id
        ]);

        if (is_object($model))
        {
            return true;
        }
    }

    /**
     * Get payment type label for display
     * @return string
     */
    public function getPaymentTypeLabel()
    {
        if ($this->payment_type === Yii::$app->util->shortTermEmployment)
        {
            return Yii::t('app', 'On income tax card');
        }

        return Yii::t('app', 'On account (trade licence)');
    }

    /**
     * Get completed date
     * @return string
     */
    public function getCompletedDate()
    {
        if (is_object($this->invoice))
        {
            return $this->invoice->date_created;
        }

        return '';
    }

    /**
     * 
     * @return string
     */
    public function getAffiliateName()
    {
        if (is_object($this->affiliate))
        {
            return $this->affiliate->name;
        }

        return '';
    }

}
