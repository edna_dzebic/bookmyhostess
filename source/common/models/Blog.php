<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Blog extends Model
{

    public $id;
    public $title;
    public $short_content;
    public $created_by_username;
    public $date_created;
    public $image;
    public $image_explanation;
    public $content;

    public function setData($data)
    {
        $this->id = $data["id"];
        $this->title = $data["title"];
        $this->short_content = $data["short_content"];
        $this->created_by_username = $data["created_by_username"];
        $this->date_created = $data["date_created"];
        $this->image = $data["image"];
        $this->image_explanation = $data["image_explanation"];
        $this->content = $data["content"];
    }

}
