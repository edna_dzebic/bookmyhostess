<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Account section' => '',
    'As a client, you now can list our profiles without need to return to search every time. <br> We are trying to provide you as much as we can features to make booking process faster and easier. <br> We hope you like this new feature.' => '',
    'As a client, you now can send non-binding requests to users, but you do not have to fill request form every time. <br> System will remember data from your first request, and you can send new request to next profile with just one click' => '',
    'As a user, you can postpone trade license file upload. <br> Please use this feature carefully, since this file is still required, and your profile will be deleted if you do not send it until selected period expired.' => '',
    'Chat feature' => '',
    'Client registration changed' => '',
    'Discount coupons' => '',
    'Email change' => '',
    'February 2017' => '',
    'File size' => '',
    'From now on, users are obligated to input mother language and at lease one more before profile approval.' => '',
    'From now, every user can write short summary about himself. <br> This summary is displayed on user\'s profile so clients can read it. <br> Please use this field to describe yourself in few lines the best you can.' => '',
    'Gallery' => '',
    'Invoice detais form for users' => '',
    'January 2017' => '',
    'March 2017' => '',
    'My bookings' => '',
    'New features' => '',
    'New information on checkout page' => '',
    'No results for profile search' => '',
    'No results for your search? No problem! <br> From now on, as a client, leave us a message which profiles exactly are you looking for.<br> We will do our best to find you suitable users.' => '',
    'Please take a look at our new photos in <i>Gallery</i> section of our home page. <br> Our users are booked at various trade shows accross Europe, and we are trying to show you our users at work.' => '',
    'Post-reject email' => '',
    'Postpone trade license file upload' => '',
    'Previous and next profile' => '',
    'Rejection reasons' => '',
    'Remembering booking data' => '',
    'Since we are developing invoicing feature for our users, we are obligated to gather invoice details. <br> All users are required to do so upon first login in system. <br> This must be done only once. <br> Please stay tuned about invoicing feature on our <i>New Features</i> page.' => '',
    'Statistics' => '',
    'System is now sending information e-mail to our users after user rejects request. <br> This e-mail informs user about possibility to withdraw from events, in case user is no longer available. <br> This way, we want to reduce number of rejections, and also inform users how negatively this affects their statistic.' => '',
    'This feature provides our users possiblity to change email used by system for communication and messages. <br> Please use e-mail address you use frequently, since our notifications are very important and you do not want to miss them.' => '',
    'This option gives possibility to select reason why user is rejecting new request. <br> This gives more information to client, but as well as to our administrator. <br> Keep in mind that rejection affects users statistic negatively.' => '',
    'This option gives possibility to withdraw newly sent, as well as already accepted requests. <br> This feature is available both for clients and users. <br> In case you have sent request by mistake, or changed your mind, or simply want to change dates, please withdraw current request and simply send new one. <br> If your are a user, and can no longer work, you can also withdraw your confirmation.' => '',
    'Users language' => '',
    'Users profile summary' => '',
    'Users registration' => '',
    'We are happy to inform you that from now on we are using chat feature. <br> Our administrators will try to be online as much as they can. <br> Please do not hasitate to contact us using chat in case you have any inquires.' => '',
    'We are happy to inform you that we have developed an iOS application, designed only for our users. <br> From now, users can use both android and iOS application and react much faster an all requests. <br> Users with installed mobile application are more likely to be booked, so we recommend you to install version you prefer.' => '',
    'We are happy to inform you that we have developed new feature, designed especially for our clients. <br> From now one, you wil be able to user special discount code at checkout. <br> This means you will be able to save money with our gift codes. <br> Please check your e-mail regulary because you never know when we will send you this code.' => '',
    'We are now displaying all invoce deatils to our clients on checkout page. <br> Please check all details since these informations are very important and must be correct in order to have valid invoice.' => '',
    'We have added new feature to our clients profile. <br> This feature provides clients history of all past bookings with information about users they have bindingly booked for upcoming events.' => '',
    'We have increased file size for users images as well as for documents. <br> Please keep in mind that it is still limited in size, and please try to upload as small as possible files.' => '',
    'We have redesign account section on both client and users profiles. display on users profile. <br> We hope you like it.' => '',
    'We have redesign our registration process for our clients. <br> Registration form no longer requires any invoice fields. These fields will be asked only before first booking. <br> Registration process is now much faster and easier.' => '',
    'We have redesign registration process for our users. <br> From now on, you do not have to give your tax information upon registration. <br> All invoice details will be asked after you register. <br> Registration process is now much faster and easier.' => '',
    'We have redesign stats display on users profile. <br> We are trying to make it easier for our clients to see which are our best profiles, so we made stats colorful. <br> We hope you like it.' => '',
    'Withdraw option' => '',
    'iOS application' => '',
];
