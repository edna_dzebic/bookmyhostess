<?php

use yii\helpers\Url;
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>

<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model->userRequested]) ?>


    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'One working day ago the hostess with the username <b>{hostess}</b>  has confirmed Your booking request for the event <b>{event_name}</b>. She has scheduled the time for this event and is still waiting for the final booking. ', ['event_name' => $model->event->name, 'hostess' => $model->user->username]) ?>         
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td style="color:red; font-style: italic;">
            <?= Yii::t('mail.exhibitor', 'Please complete the booking process within one working day because the confirmation of the hostess will expire after this period. If the person you have booked has canceled or can not come for any reason, we will arrange a replacement for you immediately and free of charge.') ?>         
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/exhibitor/requests", "lang" => Yii::$app->language]) ?>" target='_blank'><?= Yii::t('mail.exhibitor', 'SEE REQUEST') ?></a>
            <br>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'Thank You in advance for Your booking!') ?>            
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
</table>

<?php $this->endBody() ?>
<?php $this->endPage() ?>
