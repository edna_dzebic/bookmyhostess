<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'You have recently successfully registered at BOOKmyHOSTESS.com but not yet sent a non-binding booking request.') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'Is there a specific reason for it? Have You not found a suitable person? Is there anything we can help You with?') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td><?= Yii::t('mail.exhibitor', 'Do not hesitate to contact us if You have any inquiries.') ?></td>
    </tr>
    <tr height="10"><td></td></tr>

</table>