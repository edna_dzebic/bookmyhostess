<?php if (Yii::$app->util->isMale($model->gender)) : ?>
    <tr>
        <td><?= Yii::t('mail.exhibitor', 'Dear Mr. {user},', ["user" => $model->fullName]) ?></td>
    </tr>
<?php else : ?> 
    <tr>
        <td><?= Yii::t('mail.exhibitor', 'Dear Ms. {user},', ["user" => $model->fullName]) ?></td>
    </tr>
<?php endif; ?>
<tr style="height: 10%;"><td><br></td></tr>

