<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'You have started the free registration at BOOKmyHOSTESS.com but it is not completed yet. The E-mail-verification is pending. Please verify now Your e-mail-address.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px 20px;background-color: #0caa9d; color:white; text-decoration: none;border-radius: 5px;" href="<?= $validationLink ?>" target='_blank'><?= Yii::t('mail.exhibitor', 'Verify e-mail-address') ?></a>
            <br>
        </td>

    </tr>
    <tr height="10"><td></td></tr>
</table>