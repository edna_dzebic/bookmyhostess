<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model->userRequested]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'The hostess with the username <b>{hostess}</b> has not confirmed Your booking request for the event <b>{event}</b>. <br> Reason: <b>{reason} </b> <br><br> However, there are more available people for this event. <br> Please send a new request if You have sent just one or if all Your requests were rejected.', ["hostess" => $model->user->username, "event" => $model->event->name, "reason" => $model->getRejectionText()]) ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/hostess/for-event", "id" => $model->event->id, "lang" => Yii::$app->language]) ?>" target='_blank'><?= Yii::t('mail.exhibitor', 'SEND NEW REQUESTS') ?></a>
            <br>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td><?= Yii::t('mail.exhibitor', 'Do not hesitate to contact us if You have any inquiries.') ?></td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
</table>