<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model->userRequested]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'You have recently booked short term staff for the fair {event} through BOOKmyHOSTESS.com. <br> We would like to thank you for your trust and hope that you were satisfied. 
<br><br> We are constantly working on our improvement. We would like to make it easier for future clients to find a motivated person on our platform. <br> For this endeavor your feedback will be incredibly valuable.<br> Please take a few minutes of your time to let us know if the chosen person has completed given tasks to your satisfaction.<br> Is there something she or he should improve?', [
                "event" => $model->event->name])
            ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= $reviewLink ?>" target='_blank'><?= Yii::t('mail.exhibitor', 'Take the survey') ?></a>
            <br>
            <br>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td><?= Yii::t('mail.exhibitor', 'Thank you in advance for your contribution!') ?></td>
    </tr>
</table>
