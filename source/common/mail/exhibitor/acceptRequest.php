<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model->userRequested]) ?>


    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'Your booking request has been confirmed.') ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>

    <tr>
        <td>
            <table>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Hostess username:') ?>
                    </td>
                    <td>
                        <?= $model->user->username ?>
                    </td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Her rate:') ?>
                    </td>
                    <td>
                        <?= Yii::$app->util->formatEventPrice($model->event, $model->price) ?>
                    </td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Payment of the daily rate:') ?>
                    </td>
                    <td>
                        <?= $model->getPaymentTypeLabel() ?>
                    </td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Event Name:') ?>
                    </td>
                    <td>
                        <?= $model->event->name; ?>
                    </td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'City:') ?>
                    </td>
                    <td> <?= $model->event->city->name; ?></td>
                </tr>
                <tr>
                    <td style="width:45%"><?= Yii::t('mail.exhibitor', 'Date:') ?></td>
                    <td> <b><?= Yii::$app->util->formatDate($model->date_from) ?> - <?= Yii::$app->util->formatDate($model->date_to); ?></b></td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Job description:') ?>
                    </td>
                    <td> <?= $model->jobName; ?></td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Outfit:') ?>
                    </td>
                    <td><?= $model->dressCode->translatedName ?></td>
                </tr>                
            </table>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'System booking charge: {price} EUR/day.', ["price" => Yii::$app->util->formatPrice($model->booking_price_per_day)]) ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td style="color:red; font-style: italic;">
            <?= Yii::t('mail.exhibitor', 'Please complete the booking process as soon as possible as the preferred hostess could be booked by an other client. If the person you have booked has canceled or can not come for any reason, we will arrange a replacement for you immediately and free of charge.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/exhibitor/requests", "lang" => Yii::$app->language]) ?>" target='_blank'><?= Yii::t('mail.exhibitor', 'SEE REQUEST') ?></a>
            <br>
        </td>

    </tr>
    <tr height="10"><td></td></tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td><?= Yii::t('mail.exhibitor', 'Do not hesitate to contact us if You have any inquiries.') ?></td>
    </tr>
    <tr height="10"><td></td></tr>
</table>