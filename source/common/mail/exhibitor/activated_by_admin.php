<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'You registered on {date} as an exhibitor on our platform <a href="https://www.bookmyhostess.com">BOOKmyHOSTESS.com</a>.', ["date" => Yii::$app->util->formatDate($model->exhibitorProfile->date_created)]) ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?=
            Yii::t('mail.exhibitor', 'However, the registration failed because you made a mistake typing the e-mail address. This error is now fixed and you can <a href="{loginLink}">log in</a> with your e-mail address and the password you have assigned in order to view the <a href="{hostessesLink}">profiles of the available hostesses</a> for the desired trade fair.', [
                "loginLink" => Yii::$app->urlManagerFrontend->createAbsoluteUrl(["site/login", "lang" => $model->lang]),
                "hostessesLink" => Yii::$app->urlManagerFrontend->createAbsoluteUrl(["hostess/"])
            ])
            ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td><?= Yii::t('mail.exhibitor', 'Do not hesitate to contact us if You have any inquiries.') ?></td>
    </tr>
    <tr height="10"><td></td></tr>

</table>