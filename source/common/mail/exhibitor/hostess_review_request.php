<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model->eventRequest->userRequested]) ?>

    <?php if (Yii::$app->util->isMale($model->hostess->gender)) : ?>
        <tr>
            <td>
                <?=
                Yii::t('mail.exhibitor', 'You have booked {host} for the fair {event} through BOOKmyHOSTESS.com. <br> We would like to thank you for your trust and hope that you were satisfied.<br><br> {host} now asks you for a feedback.<br> This would make it easier for him to get more interesting assignments and improve on the basis of your rating.', [
                    "host" => $model->hostess->fullName,
                    "event" => $model->event->name])
                ?>
            </td>
        </tr>
        <tr height="10"><td><br></td></tr>
        <tr>
            <td>
                <?= Yii::t('mail.exhibitor', 'Where do you find his strengths? What could he do better?') ?>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td>
                <?=
                Yii::t('mail.exhibitor', 'You have booked {hostess} for the fair {event} through BOOKmyHOSTESS.com. <br> We would like to thank you for your trust and hope that you were satisfied. <br><br> {hostess} now asks you for a feedback.<br>This would make it easier for her to get more interesting assignments and improve on the basis of your rating.', [
                    "hostess" => $model->hostess->fullName,
                    "event" => $model->event->name])
                ?>
            </td>
        </tr>
        <tr height="10"><td><br></td></tr>
        <tr>
            <td>
                <?= Yii::t('mail.exhibitor', 'Where do you find her strengths? What could she do better?') ?>
            </td>
        </tr>
    <?php endif; ?>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td style="text-align: center;" width="50%">
            <br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= $reviewLink ?>" target='_blank'><?= Yii::t('mail.exhibitor', 'TO ASSESSMENT') ?></a>
            <br>
            <br>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td><?= Yii::t('mail.exhibitor', 'Thank you in advance for your contribution!') ?></td>
    </tr>
</table>
