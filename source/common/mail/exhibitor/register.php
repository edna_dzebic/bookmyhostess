<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model]) ?>
    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'Thank You for Your registration at BOOKmyHOSTESS.com.') ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'Your email on our platform is: <b>{email}</b> .', ["email" => $model->email]) ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'To complete Your registration on our platform please verify Your email address now.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px 20px;background-color: #0caa9d; color:white; text-decoration: none;border-radius: 5px;" href="<?= $link ?>" target='_blank'><?= Yii::t('mail.exhibitor', 'VERIFY MY E-Mail') ?></a>
            <br>
        </td>
    </tr>
     <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'Our short <a href="https://www.youtube.com/watch?v=VAgAygIaQy0" style="color:#0caa9d; font-weight:bold;">Demo-Video</a> shows how the booking is performed.') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <?php if ($showLangMessage) : ?>
        <tr height="20"><td></td></tr>
        <tr>
            <td>
                Please note: <br> 
                At this moment the default language of our system is English. The translation into Your preferred language is coming soon.
            </td>
        </tr>
    <?php endif; ?> 
</table>