<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model->userRequested]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'The hostess with the username <b>{hostess}</b> has withdrawn Your booking request for the following event:', ["hostess" => $model->user->username]) ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>

    <tr>
        <td>
            <table>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Event Name:') ?>
                    </td>
                    <td>
                        <?= $model->event->name; ?>
                    </td>
                </tr>

                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'City:') ?>
                    </td>
                    <td> <?= $model->event->city->name; ?></td>
                </tr>
                <tr>
                    <td style="width:45%"><?= Yii::t('mail.exhibitor', 'Date:') ?></td>
                    <td> <b><?= Yii::$app->util->formatDate($model->date_from) ?> - <?= Yii::$app->util->formatDate($model->date_to); ?></b></td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Job description:') ?>
                    </td>
                    <td> <?= $model->jobName; ?></td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Outfit:') ?>
                    </td>
                    <td><?= $model->dressCode->translatedName ?></td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Daily rate:') ?>
                    </td>
                    <td>
                        <?= Yii::$app->util->formatPrice($model->price) . " " . Yii::t('mail.exhibitor', 'EUR/day') ?>
                    </td>
                </tr>
                <tr>
                    <td style="width:45%">
                        <?= Yii::t('mail.exhibitor', 'Payment of the daily rate:') ?>
                    </td>
                    <td>
                        <?= $model->getPaymentTypeLabel() ?>
                    </td>
                </tr>
                <tr style="height: 10%;"><td></td></tr>
                <tr style="height: 10%;"><td></td></tr>
                <tr style="height: 10%;"><td></td></tr>
                <tr style="height: 10%;"><td></td></tr>
                <tr>
                    <td style="width:45%">
                        <b><?= Yii::t('mail.exhibitor', 'Reason:') ?></b>
                    </td>
                    <td>
                        <b><?= $model->getWithdrawText(); ?></b>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
</table>