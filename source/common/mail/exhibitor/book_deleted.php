<?php

use yii\helpers\Html;
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>

<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model->userRequested]) ?>


    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'Your booking request to <b>{hostess}</b> for the event <b>{event_name}</b> has expired after two working days.', ['event_name' => $model->event->name, 'hostess' => $model->user->username]) ?>         
        </td>
    </tr> 
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.exhibitor', 'If You are still in need of a host or hostess You can send new non-binding booking requests.') ?>         
        </td>
    </tr>  
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/hostess/for-event", "id" => $model->event->id, "lang" => Yii::$app->language]) ?> " target='_blank'><?= Yii::t('mail.exhibitor', 'SEND NEW REQUESTS') ?></a>
            <br>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td><?= Yii::t('mail.exhibitor', "If we can be of any more assistance, please don't hesitate to contact us.") ?></td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
</table>

<?php $this->endBody() ?>
<?php $this->endPage() ?>
