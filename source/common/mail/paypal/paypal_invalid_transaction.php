<?php

use yii\helpers\Html;
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>

<table style="width: 100%">
    <tr>
        <td><?= Yii::t('admin', "Dear admin,") ?></td>
    </tr>
    <tr style="height: 10%;">
        <td></td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('admin', 'Invalid paypal transaction has been registered.') ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('admin', 'Data is: ') ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= \yii\helpers\VarDumper::dump($content, 10, false); ?>
        </td>
    </tr>
    <tr style="height: 10%;">
        <td></td>
    </tr>
    <tr>
    </tr>
    <tr style="height: 10%;">
        <td></td>
    </tr>   
</table>

<?php $this->endBody() ?>
<?php $this->endPage() ?>
