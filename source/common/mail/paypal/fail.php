<?php

use yii\helpers\Html;
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>

<table style="width: 100%">
    <tr>
        <td><?= Yii::t('admin', 'Dear admin,') ?></td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('admin', 'PAYPAL TRANSACTION FAILED. CONTACT DEVELOPERS IMMEDIATELY! ') ?> .          
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <table style="width: 100%;">
                <tr>
                    <td style="color:#f15a24; width:20%;">
                        <?= Yii::t('admin', 'Errors:') ?>
                    </td> 
                    <td>
                        <?= $content ?> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?php $this->endBody() ?>
<?php $this->endPage() ?>
