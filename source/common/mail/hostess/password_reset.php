<?php

use yii\helpers\Html;
?>
<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model]) ?>
 
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'To complete your password reset procedure please click on link below.') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= $model->getPasswordResetLink() ?>" target='_blank'><?= Yii::t('mail.hostess', 'RESET PASSWORD') ?></a>
            <br>
            <br>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Your username on our platform is: <b>{username}</b>.', ["username" => $model->username]) ?>
        </td>
    </tr>
</table>