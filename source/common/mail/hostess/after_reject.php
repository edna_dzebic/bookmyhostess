<?php
$profileModel = $model->hostessProfile;
$totalRequests = $profileModel->getTotalRequestsCount();
$confirmed = $profileModel->getConfirmedRequestsCount();
$rejected = $profileModel->getRejectedRequestCount();
$booked = $profileModel->getBookedRequestCount();
$notAnswered = $profileModel->getNotAnsweredRequestCount();
?>

<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model]) ?>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'You have just rejected a booking request at BOOKmyHOSTESS.com. This is your updated profile statistics:') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <table>

                <tr>
                    <td><?= Yii::t('mail.hostess', 'Time to respond:'); ?></td>
                    <td> 
                        <?= $profileModel->getTimeToRespond(); ?>

                    </td>
                </tr>
                <tr>
                    <td><?= Yii::t('mail.hostess', 'Request:'); ?></td>
                    <td><?= $totalRequests ?></td>
                </tr>
                <tr>
                    <td>
                        <?= Yii::t('mail.hostess', 'Booked:'); ?>
                    </td>
                    <td>
                        <?= $booked ?>
                        /
                        <?= $confirmed ?>
                    </td>
                </tr>
                <tr>
                    <td><?= Yii::t('mail.hostess', 'Confirmed:'); ?></td>
                    <td>

                        <?= $confirmed ?>
                        /
                        <?= $totalRequests ?>

                    </td>
                </tr>
                <tr>
                    <td><?= Yii::t('mail.hostess', 'Rejected:'); ?></td>
                    <td>
                        <?= $rejected ?>
                        /
                        <?= $totalRequests ?>        

                    </td>
                </tr>       
                <tr>
                    <td><?= Yii::t('mail.hostess', 'Not answered:'); ?></td>
                    <td>
                        <?= $notAnswered ?>
                        /
                        <?= $totalRequests ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Each denial affects negatively your statistics and therewith the rating of your profile. <br> In order to prevent further downgrade of your profile, reduce the number of the cancellations by signing off the event list:') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/hostess-profile/trade-shows", "lang" => Yii::$app->language]) ?>" target='_blank'><?= Yii::t('mail.hostess', 'SIGN OFF') ?></a>
            <br>
        </td>

    </tr>
    <tr height="10"><td></td></tr>
</table>