<?php if (Yii::$app->util->isMale($model->gender)) : ?>
    <tr>
        <td><?= Yii::t('mail.hostess', 'Dear {host},', ["host" => $model->first_name]) ?></td>
    </tr>
<?php else : ?> 
    <tr>
        <td><?= Yii::t('mail.hostess', 'Dear {hostess},', ["hostess" => $model->first_name]) ?></td>
    </tr>  
<?php endif; ?>
<tr style="height: 10%;"><td><br></td></tr>