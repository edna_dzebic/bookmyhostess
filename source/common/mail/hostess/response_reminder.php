
<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model->user]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'One working day ago you have received the following booking request: ') ?>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width: 100%">
                <tr>
                    <td style="width: 20%">
                        <?= Yii::t('mail.hostess', 'Event name:') ?>
                    </td>
                    <td>
                        <?= $model->event->name; ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%">
                        <?= Yii::t('mail.hostess', 'City:') ?>
                    </td>
                    <td> <?= $model->event->city->name; ?></td>
                </tr>
                <tr>
                    <td style="width: 20%"><?= Yii::t('mail.hostess', 'Date:') ?></td>
                    <td> <b><?= date('d.m.Y', strtotime($model->date_from)) ?> - <?= date('d.m.Y', strtotime($model->date_to)); ?></b></td>
                </tr>
                <tr>
                    <td style="width: 20%">
                        <?= Yii::t('mail.hostess', 'Job description:') ?>
                    </td>
                    <td> <?= $model->jobName; ?></td>
                </tr>
                <tr>
                    <td style="width: 20%">
                        <?= Yii::t('mail.hostess', 'Outfit:') ?>
                    </td>
                    <td><?= $model->dressCode->translatedName; ?></td>
                </tr>
                <tr>
                    <td  style="width: 20%">
                        <?= Yii::t('mail.hostess', 'Daily rate:') ?>
                    </td>
                    <td>
                        <?= Yii::$app->util->formatEventPrice($model->event, $model->price) ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'The client is still awaiting your answer and the system counts the time you need to respond. Please answer immediately to this request.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white; text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/hostess-profile/requests", "id" => $model->event->id, "lang" => Yii::$app->language]) ?>" target='_blank'><?= Yii::t('mail.hostess', 'CONFIRM/REJECT REQUEST') ?></a>
            <br>
        </td>

    </tr>
    <tr height="10"><td></td></tr>
</table>