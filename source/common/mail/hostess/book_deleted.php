<?php

use yii\helpers\Html;
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>

<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model->user]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'The client has completed the booking process for the event  <b>{event_name}</b>. You have not been booked this time but you are still available for other clients.', ['event_name' => $model->event->name]) ?>         
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'To improve your booking rate please consider that the clients prefer profiles with the following properties:') ?>         
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            &nbsp;&nbsp;-&nbsp;<?= Yii::t('mail.hostess', 'professional pictures') ?>         
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;&nbsp;-&nbsp;<?= Yii::t('mail.hostess', 'good statistics (low reject-rate, short average time to answer)') ?>         
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;&nbsp;-&nbsp;<?= Yii::t('mail.hostess', 'completeness of the profile') ?>         
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;&nbsp;-&nbsp;<?= Yii::t('mail.hostess', 'lower daily rates') ?>         
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'We know from our experience that the booking rate increases after an improvement of the mentioned profile features.') ?>         
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'We wish you good luck for the next time.') ?>         
        </td>
    </tr>
</table>

<?php $this->endBody() ?>
<?php $this->endPage() ?>
