<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model]) ?>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Recently you have changed an email address at BOOKmyHOSTESS.com.') ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Your new email on our platform is: <b>{email}</b> .', ["email" => $model->email]) ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Your new email will be used for all system notifications, requests and messages.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'In case you changed your email by mistake, or want to return your old email, please change it at your profile.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px 20px;background-color: #0caa9d; color:white; text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManager->createAbsoluteUrl(["/hostess-profile/account"]) ?>" target='_blank'><?= Yii::t('mail.hostess', 'Profile') ?></a>
            <br>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
</table>