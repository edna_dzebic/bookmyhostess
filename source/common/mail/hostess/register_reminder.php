<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model]) ?>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Your profile at BOOKmyHOSTESS.com has recently been approved, but yet you have not signed up for any event. The potential clients can’t see your profile and they can’t book your service.') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Haven’t you found at least one interesting event in our system? Did you know that you can apply for a job six months in advance? The earlier you register for an event, the higher is the probability of being booked. We would therefore recommend that you sign up on time.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/upcoming-events", "lang" => Yii::$app->language]) ?>" target='_blank'><?= Yii::t('mail.hostess', 'EVENT SIGN UP') ?></a>
            <br>
        </td>

    </tr>
    <tr height="10"><td></td></tr>
</table>