<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model]) ?>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'You have initiated the registration at BOOKmyHOSTESS.com. To access your profile you only need to verify your E-Mail-address. Please complete the registration now.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px 20px;background-color: #0caa9d; color:white; text-decoration: none;border-radius: 5px;" href="<?= $validationLink ?>" target='_blank'><?= Yii::t('mail.hostess', 'VERIFY E-Mail') ?></a>
            <br>
        </td>

    </tr>
    <tr height="10"><td></td></tr>
</table>