<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model->user]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Two working days ago you have received a booking request for the event <b>{event_name}</b> through BOOKmyHOSTESS.com. You have not answered to this request therefore your statistics on the profile have worsened.', ['event_name' => $model->event->name]) ?>         
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'In the future, to avoid another deterioration of your profile statistics, we recommend to react faster on client’s inquiries or to withdraw the registration if you are no longer available for an event. ') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white; text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/site/login", "lang" => Yii::$app->language]) ?>" target='_blank'><?= Yii::t('mail.hostess', 'Withdraw from events') ?></a>
            <br>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'In case you are not interested any more in new job offers please delete your profile.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr> 
    <tr>
        <td style="text-align: center;" width="50%">
            <br>
            <a style="padding: 10px;background-color: #0caa9d; color:white; text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/site/login"]) ?>" target='_blank'><?= Yii::t('mail.hostess', 'Delete profile') ?></a>
            <br>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
</table>