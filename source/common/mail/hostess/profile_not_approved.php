<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model->user]) ?>

    <tr><td ><?= Yii::t('mail.hostess', 'Your profile on www.bookmyhostess.com has not been approved! This is the message from our administrator:') ?></td></tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <b><?= $message ?></b>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Please revise your profile and send a new approval request.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/site/login", "lang" => Yii::$app->language]) ?>" target='_blank'><?= Yii::t('mail.hostess', 'LOG IN') ?></a>
            <br>
        </td>

    </tr>
    <tr height="10"><td></td></tr>
</table>
