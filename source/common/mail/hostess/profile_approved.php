<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model->user]) ?>

    <tr><td ><?= Yii::t('mail.hostess', 'Your profile on www.bookmyhostess.com has been approved! You can now apply for jobs in our database and sign up for varios events.') ?></td></tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/upcoming-events", "lang" => Yii::$app->language]) ?>" target='_blank'><?= Yii::t('mail.hostess', 'EVENT SIGN UP') ?></a>
            <br>
        </td>

    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td>
            <br><br>
            <?= Yii::t('mail.hostess', 'Our short <a href="https://www.youtube.com/watch?v=R-nLFr-uVno" style="color:#0caa9d; font-weight:bold;">Demo-Video</a> shows how you can apply for jobs in our system.') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'We wish you a lot of success!') ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
</table>