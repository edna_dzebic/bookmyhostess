<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model]) ?>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Thank you for your registration at BOOKmyHOSTESS.com.') ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'Your username on our platform is: <b>{username}</b> .', ["username" => $model->username]) ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'To complete your registration on our platform please verify your email address now.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>
        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px 20px;background-color: #0caa9d; color:white; text-decoration: none;border-radius: 5px;" href="<?= $model->getValidationLink() ?>" target='_blank'><?= Yii::t('mail.hostess', 'VERIFY E-Mail') ?></a>
            <br>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <?php if ($showLangMessage) : ?>
        <tr height="20"><td></td></tr>
        <tr>
            <td>
                Please note: <br> 
                At this moment the default language of our system is English. The translation into your preferred language is coming soon.
            </td
        </tr>
    <?php endif; ?> 
</table>