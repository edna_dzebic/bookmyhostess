
<table style="width: 100%">

    <?= $this->render("_dear", ["model" => $model->user]) ?>

    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'The client has withdrawn the booking request for the following event:') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>
        <td>
            <table style="width: 100%">
                <tr>
                    <td style="width: 20%">
                        <?= Yii::t('mail.hostess', 'Event name:') ?>
                    </td>
                    <td>
                        <?= $model->event->name; ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%">
                        <?= Yii::t('mail.hostess', 'City:') ?>
                    </td>
                    <td> <?= $model->event->city->name; ?></td>
                </tr>
                <tr>
                    <td style="width: 20%"><?= Yii::t('mail.hostess', 'Date:') ?></td>
                    <td> <b><?= Yii::$app->util->formatDate($model->date_from) ?> - <?= Yii::$app->util->formatDate($model->date_to); ?></b></td>
                </tr>
                <tr>
                    <td style="width: 20%">
                        <?= Yii::t('mail.hostess', 'Job description:') ?>
                    </td>
                    <td> <?= $model->jobName; ?></td>
                </tr>
                <tr>
                    <td style="width: 20%">
                        <?= Yii::t('mail.hostess', 'Outfit:') ?>
                    </td>
                    <td><?= $model->dressCode->translatedName ?></td>
                </tr>
                <tr>
                    <td  style="width: 20%">
                        <?= Yii::t('mail.hostess', 'Daily rate:') ?>
                    </td>
                    <td><?= Yii::$app->util->formatPrice($model->price) . " " . Yii::t('mail.hostess', 'EUR/day') ?></td>
                </tr>
                <tr>
                    <td style="width:20%">
                        <?= Yii::t('mail.exhibitor', 'Payment of the daily rate:') ?>
                    </td>
                    <td>
                        <?= $model->getPaymentTypeLabel() ?>
                    </td>
                </tr>
                <tr height="10"><td></td></tr>
                <tr height="10"><td></td></tr>
                <tr height="10"><td></td></tr>
                <tr>
                    <td style="width: 20%">
                        <b><?= Yii::t('mail.hostess', 'Reason:') ?></b>
                    </td>
                    <td>
                        <b><?= $model->getExhibitorWithdrawText() ?></b>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
</table>