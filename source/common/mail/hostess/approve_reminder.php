<table style="width: 100%">
    <?= $this->render("_dear", ["model" => $model]) ?>
    <tr>
        <td>
            <?= Yii::t('mail.hostess', 'To be able to apply for various vacancies at BOOKmyHOSTESS.com your profile has to be completed and approved by our administrator. Please fill in the empty fields in your profile and click on “approve”.') ?>
        </td>
    </tr>
    <tr height="10"><td><br></td></tr>
    <tr>

        <td style="text-align: center;" width="50%">
            <br><br>
            <a style="padding: 10px;background-color: #0caa9d; color:white;text-decoration: none;border-radius: 5px;" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/site/login"]) ?>" target='_blank'><?= Yii::t('mail.hostess', 'COMPLETE PROFILE') ?></a>
            <br>
        </td>

    </tr>
    <tr height="10"><td></td></tr>
</table>