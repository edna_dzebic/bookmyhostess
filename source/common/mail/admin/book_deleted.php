<table style="width: 100%">
    <tr>
        <td><?= Yii::t('admin', 'Dear admin,') ?></td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('admin', 'Booking for event <b>{event_name}</b> for hostess <b>{hostess}</b> has been deleted because exhibitor <b>{exibitor}</b>  waited longer that five days to complete booking. ', ['event_name' => $model->event->name, 'hostess' => $model->user->username, 'exibitor' => $model->userRequested->username]) ?>         
        </td>
    </tr>   
</table>
