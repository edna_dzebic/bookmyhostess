<table style="width: 100%">
    <tr>
        <td><?= Yii::t('admin', 'Dear admin,') ?></td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('admin', 'You have new request for hostess from user {0}, {1}, {2}', [$model->user->fullName, $model->user->username,  $model->user->email]) ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('admin', 'These are the search parameters.') ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <table style="width: 100%;">
                <?php foreach ($model->filter as $key=>$value) : ?>
                <tr>
                    <th align="left" style="width:200px;">
                        <strong><?= $model->getFilterTranslations()[$key] ?></strong>
                    </th>
                    <td>
                        <?= $value ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <strong><?= Yii::t('admin', 'Additional description') ?></strong>
        </td>
    </tr>
    <tr>
        <td>
            <?= $model->additional_description ?>
        </td>
    </tr>
</table>
