<?php

use yii\helpers\Html;
?>
<table style="width: 100%">
    <tr>
        <td><?= Yii::t('admin', 'Dear admin,') ?></td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('admin', 'You have new postpone requst from hostess on site ') . Yii::$app->name ?> .          
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <table style="width: 100%;">       
                <tr>
                    <td style=" width:20%;">
                        <?= Yii::t('admin', 'Hostess ID:') ?>
                    </td> 
                    <td>
                        <?= $model->hostessProfile->user->id ?> 
                    </td>
                </tr>
                <tr>
                    <td style=" width:20%;">
                        <?= Yii::t('admin', 'Username:') ?>
                    </td> 
                    <td>
                        <?= $model->hostessProfile->user->username ?> 
                    </td>
                </tr>
                <tr>
                    <td style=" width:20%;">
                        <?= Yii::t('admin', 'Full name:') ?>
                    </td> 
                    <td>
                        <?= $model->hostessProfile->user->first_name . " " . $model->hostessProfile->user->last_name ?> 
                    </td>
                </tr>
                <tr>
                    <td style=" width:20%;">
                        <?= Yii::t('admin', 'Requested postpone until:') ?>
                    </td> 
                    <td>
                        <?= $model->date ?> 
                    </td>
                </tr>
                <tr>
                    <td style=" width:20%;">
                        <?= Yii::t('admin', 'Reason for request:') ?>
                    </td> 
                    <td>
                        <?= $model->reason ?> 
                    </td>
                </tr>
                <tr>
                    <td style=" width:20%;">
                        <?= Yii::t('admin', 'Click on following link to see profile:') ?>
                    </td> 
                    <td>
                        <?= Html::a(Yii::t('admin', 'LINK'), Yii::$app->urlManagerBackend->createUrl(["hostess/verify", "id" => $model->hostessProfile->user->id])) ?> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
