<table style="width: 100%">
    <tr>
        <td><?= Yii::t('admin', 'Dear admin,') ?></td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('admin', 'You have new exhibitor on site ') . Yii::$app->name ?> .          
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <table style="width: 100%;"> 
                <tr>
                    <td style="width:20%;">
                        <?= Yii::t('admin', 'Username:') ?>
                    </td> 
                    <td>
                        <?= $model->username ?> 
                    </td>
                </tr>
                <tr>
                    <td style="width:20%;">
                        <?= Yii::t('admin', 'Full name:') ?>
                    </td> 
                    <td>
                        <?= $model->first_name . " " . $model->last_name ?> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
