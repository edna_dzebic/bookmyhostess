<table style="width: 100%">
    <tr>
        <td><?= Yii::t('admin', 'Dear admin,') ?></td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('admin', 'Hostess denied book request on site ') . Yii::$app->name ?> .          
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <table style="width: 100%;">              
                <tr>
                    <td style="width:20%;">
                        <?= Yii::t('admin', 'Hostess full name:') ?>
                    </td> 
                    <td>
                        <?= $model->user->first_name . " " . $model->user->last_name ?> 
                    </td>
                </tr>
                <tr>
                    <td style="width:20%;">
                        <?= Yii::t('admin', 'Exhibitor full name:') ?>
                    </td> 
                    <td>
                        <?= $model->userRequested->first_name . " " . $model->userRequested->last_name ?> 
                    </td>
                </tr>
                <tr>
                    <td style="width:20%;">
                        <?= Yii::t('admin', 'Reason:') ?>
                    </td> 
                    <td>
                        <?= $model->getRejectionText() ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border=1 width=400px>
                <tr>
                    <td><?= Yii::t('admin', 'Event name:') ?></td><td> <?= $model->event->name; ?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('admin', 'City:') ?></td><td> <?= $model->event->city->name; ?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('admin', 'Dates:') ?></td><td> <b><?= date('d.m.Y', strtotime($model->date_from)) ?></b> to <b><?= date('d.m.Y', strtotime($model->date_to)); ?></b></td>
                </tr>
                <tr>
                    <td><?= Yii::t('admin', 'Job description:') ?></td><td> <?= $model->jobName; ?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('admin', 'Outfit:') ?></td><td><?= $model->dressCode->translatedName; ?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('admin', 'Daily rate') ?></td>
                    <td>
                        <?= Yii::$app->util->formatEventPrice($model->event, $model->price) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Yii::t('admin', 'Payment of the daily rate:') ?>
                    </td>
                    <td>
                        <?= $model->getPaymentTypeLabel() ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
