<?php


$unsubscribeLink = Yii::$app->urlManagerFrontend->createAbsoluteUrl(['newsletter/unsubscribe', 'id' => $model->id, "lang" => Yii::$app->language]);
?>

<table style="width: 100%">
    <tr>
        <td><?= Yii::t('mail', 'Dear user') . "," ?></td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail', 'Thank you for choosing BOOKmyHOSTESS.com.') ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail', 'Newsletter subscription is registered for following email: <b>{email}</b> .', ["email" => $model->email]) ?>
        </td>
    </tr>
    <tr style="height: 10%;"><td></td></tr>
    <tr>
        <td>
            <?= Yii::t('mail', 'In case you want to cancel your subscription, click on link below:') ?>
        </td>
    </tr>
    <tr height="10"><td></td></tr>
    <tr>

        <td>
            <br>
            <a style="" href="<?= $unsubscribeLink ?>" target='_blank'><?= Yii::t('mail', 'UNSUBSCRIBE') ?></a>
            <br>
        </td>

    </tr>
    <tr height="10"><td></td></tr>
</table>
