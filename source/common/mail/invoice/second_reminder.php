<?php
/* @var $model \common\models\Invoice */
?>

<table cellpadding='0' cellspacing='0' style="width: 100%;">
    <tr>
        <td style='min-height:500px;'>
            <table cellpadding='0' cellspacing='0' border=0  align='center' style= 'width: 100%;font-family:Arial, Helvetica, sans-serif; font-size:14px;'>
                <tr align=left>
                    <td>
                        <table cellpadding='0' cellspacing='0' border=0 style='width: 100%;font-family:Arial, Helvetica, sans-serif; font-size:12px; color: black;'>

                            <?= $this->render("../exhibitor/_dear", ["model" => $model->userRequested]) ?>

                            <tr>
                                <td>
                                    <?=
                                    Yii::t('mail.exhibitor', 'On {date} You have booked personnel through our platform <a href="https://www.bookmyhostess.com." style="color: #0CAA9D">BOOKmyHOSTESS.com</a>.<br> Our records show that we haven’t yet received the payment despite a payment reminder.', [
                                        'date' => Yii::$app->util->formatDate($model->date_created)
                                    ])
                                    ?>
                            </tr>
                            <tr><td style="height: 10%;"><br></td></tr>
                            <tr>
                                <td>
                                    <?= Yii::t('mail.exhibitor', 'Please understand that if we do not receive payment to the specified account within three business days, we must hand over the case to a collection agency.') ?>
                                </td>
                            </tr>
                            <tr><td style="height: 10%;"><br></td></tr>
                            <tr>
                                <td>
                                    <?= Yii::t('mail.exhibitor', 'If You have already initiated the payment, please consider this reminder to be irrelevant.<br>Please let me know if You have any questions regarding the bill or the payment.') ?>
                                </td>
                            </tr>                          
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
