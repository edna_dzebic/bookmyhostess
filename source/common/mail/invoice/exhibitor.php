<?php
/* @var $model \common\models\Invoice */
?>

<table cellpadding='0' cellspacing='0' style="width: 100%;">
    <tr>
        <td style='min-height:500px;'>
            <table cellpadding='0' cellspacing='0' border=0  align='center' style= 'width: 100%;font-family:Arial, Helvetica, sans-serif; font-size:14px;'>
                <tr align=left>
                    <td>
                        <table cellpadding='0' cellspacing='0' border=0 style= 'width: 100%;font-family:Arial, Helvetica, sans-serif; font-size:12px;'>

                            <?= $this->render("../exhibitor/_dear", ["model" => $model->userRequested]) ?>                            

                            <tr height=15>
                                <td><?= Yii::t('mail.exhibitor', 'Thank You for choosing BOOKmyHOSTESS.com! You have bindingly booked:') ?></td>
                            </tr>
                            <tr>
                                <td>

                                    <table style=" width: 100%;">
                                        <tr>
                                            <td align="center" style='alignment-baseline: middle;'>
                                                <div style=" padding:0px; width:100%; font-size:10pt;"><br/>

                                                    <table cellspacing="0" style='border:1px solid black; min-width: 100%; width: 100%;'>
                                                        <tr style="font-weight: bold;">
                                                            <td valign='top' align='center' style='padding: 5px; border:1px solid black;'>#</td>                                                           
                                                            <td valign='top' align='center' style='padding: 5px; border:1px solid black;'><?= Yii::t('app', 'Username') ?></td>                      
                                                            <td valign='top' align='center' style='padding: 5px; border:1px solid black;'><?= Yii::t('app', 'Name') ?></td>                      
                                                            <td valign='top' align='center' style='padding: 5px; border:1px solid black;'><?= Yii::t('app', 'Email') ?></td>                      
                                                            <td valign='top' align='center' style='padding: 5px; border:1px solid black;'><?= Yii::t('app', 'Phone') ?></td> 
                                                            <td valign='top' align='center' style='padding: 5px; border:1px solid black;'><?= Yii::t('app', 'Event') ?>
                                                            </td>
                                                            <td valign='top' align='center' style='padding: 5px; border:1px solid black;'><?= Yii::t('app', 'Price /<br>Day') ?>
                                                            </td>
                                                            <td valign='top' align='center' style='padding: 5px; border:1px solid black;'><?= Yii::t('app', 'Payment of the daily rate') ?>
                                                            </td>
                                                        </tr>
                                                        <?php $i = 0; ?>
                                                        <?php foreach ($model->eventRequests as $request) : ?>
                                                            <tr style='height:70px'>
                                                                <td style='height:70px; padding: 5px; border:1px solid black;' valign='top'
                                                                    align='center'> <?= ++$i ?>
                                                                </td>
                                                                <td style='height:70px; padding: 5px; border:1px solid black;'valign='top'
                                                                    align='center'><?= $request->user->username; ?>
                                                                </td>   
                                                                <td style='height:70px; padding: 5px; border:1px solid black;'valign='top'
                                                                    align='center'><?= $request->user->fullName; ?>
                                                                </td>                           
                                                                <td style='height:70px; padding: 5px; border:1px solid black;'valign='top'
                                                                    align='center'><?= $request->user->email; ?>
                                                                </td> 
                                                                <td style='height:70px; padding: 5px; border:1px solid black;'valign='top'
                                                                    align='center'><?= $request->user->phone_number; ?>
                                                                </td> 
                                                                <td style='height:70px; padding: 5px; border:1px solid black;'valign='top'
                                                                    align='center'><?= $request->event->name; ?>
                                                                </td> 
                                                                <td style='height:70px; padding: 5px; border:1px solid black;' valign='top'
                                                                    align='center'><?= Yii::$app->util->formatEventPrice($request->event, $request->price); ?>
                                                                </td>
                                                                <td style='height:70px; padding: 5px; border:1px solid black;' valign='top'
                                                                    align='center'>                                              
                                                                        <?= $request->getPaymentTypeLabel() ?>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>

                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="color:red;text-align:justify; text-justify:inter-word;">
                                    <br/>
                                    <br/>
                                    <?= Yii::t('mail.exhibitor', 'Please contact the booked person immediately and sign a contract. An order shall only be deemed accepted if the order has been confirmed in written form by a contract between the contractor (hostess) and the client (you) within two working days after receipt of this e-mail. If the booked person has not signed the contract within the stipulated deadline, BOOKmyHOSTESS.com must be informed immediately in written form or by telephone. Otherwise, there is no claim for compensation and reimbursement of the booking fee.') ?>
                                </td>
                            </tr>
                            <?php if ($model->getIsNormalInvoice()) : ?>
                                <tr>
                                    <td style='text-align:justify; text-justify:inter-word;'>
                                        <br>
                                        <?= Yii::t('mail.exhibitor', 'Attached to this email we are sending the invoice for our service and a contract draft which You can use for Your purpuses. The daily fee you will pay to the booked person directly after the fair.') ?> 
                                    </td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td style='text-align:justify; text-justify:inter-word;'>
                                        <br>
                                        <?= Yii::t('mail.exhibitor', 'Attached to this email we are sending the receipt for our service and a contract draft which You can use for Your purpuses. The daily fee you will pay to the booked person directly after the fair.') ?> 
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <td style='text-align:justify; text-justify:inter-word;'>
                                    <br/>
                                    <?= Yii::t('mail.exhibitor', 'Please ensure that the contract is signed by the hostess prior to the trade show in order to avoid cancellation.') ?>
                                </td>
                            </tr>
                            <tr>
                                <td style='text-align:justify; text-justify:inter-word;'><br/>
                                    <?= Yii::t('mail.exhibitor', 'Do not hesitate to contact us if You have any inquiries.') ?>
                                </td>
                            </tr>    
                            <tr><td style="height: 10%;"><br></td></tr>
                            <tr>
                                <td>
                                    <?= Yii::t('mail.exhibitor', 'If you would like to provide catering on your stand or you plan a stand party, our partner <a href="http://www.exev.tv/de/home/">EXEV GmbH</a> would be happy to take care of the organization of beverages, food, music, furniture, equipment and decoration for the stand as well.') ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
