<?php
/* @var $model \common\models\Invoice */
?>

<table cellpadding='0' cellspacing='0' style="width: 100%;">
    <tr>
        <td style='min-height:500px;'>
            <table cellpadding='0' cellspacing='0' border=0  align='center' style= 'width: 100%;font-family:Arial, Helvetica, sans-serif; font-size:14px;'>
                <tr align=left>
                    <td>
                        <table cellpadding='0' cellspacing='0' border=0 style='width: 100%;font-family:Arial, Helvetica, sans-serif; font-size:12px; color: black'>

                            <?= $this->render("../exhibitor/_dear", ["model" => $model->userRequested]) ?>

                            <tr>
                                <td>
                                    <?=
                                    Yii::t('mail.exhibitor', 'On {date} You have booked personnel through our platform <a href="https://www.bookmyhostess.com." style="color: #0CAA9D">BOOKmyHOSTESS.com</a>.<br> Our records show that we haven’t yet received payment for the Invoice attached, which is overdue. <br> I am sure You are busy, but I would appreciate if you could take a moment to check this out on Your end.', [
                                        'date' => Yii::$app->util->formatDate($model->date_created)
                                    ])
                                    ?>
                            </tr>
                            <tr><td style="height: 10%;"><br></td></tr>
                            <tr>
                                <td>
                                    <?= Yii::t('mail.exhibitor', 'If the payment has already been sent, please disregard this notice. <br> Please let me know if you have any questions.') ?>
                                </td>
                            </tr>
                            <tr><td style="height: 10%;"><br></td></tr>
                            <tr>
                                <td>
                                    <?= Yii::t('mail.exhibitor', 'Thank You in advance.') ?>
                                </td>
                            </tr>                            
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
