<table cellpadding='0' cellspacing='0' style="width: 100%;">

    <tr>
        <td style=' min-height:500px;'>
            <table cellpadding='0' cellspacing='0' border=0 align='center' style= 'width: 100%;font-family:Arial, Helvetica, sans-serif; font-size:14px;'>
                <tr align=left>
                    <td>
                        <table cellpadding='0' cellspacing='0' border=0 style= 'width: 100%;font-family:Arial, Helvetica, sans-serif; font-size:14px;'>
                            <?php if (Yii::$app->util->isMale($model->user->gender)) : ?>
                                <tr>
                                    <td><?= Yii::t('mail.hostess', 'Dear {host},', ["host" => $model->user->first_name]) ?></td>
                                </tr>
                            <?php else : ?> 
                                <tr>
                                    <td><?= Yii::t('mail.hostess', 'Dear {hostess},', ["hostess" => $model->user->first_name]) ?></td>
                                </tr>  
                            <?php endif; ?>
                            <tr><td style="height: 10%;"><br></td></tr>

                            <tr>
                                <td colspan='2'>
                                    <?= Yii::t('mail.hostess', 'Your service has been bindingly booked for the following event:') ?></td> 
                            </tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td width='200px'><?= Yii::t('mail.hostess', 'Event Name:') ?> </td>
                                <td><b><?= $model->event->name; ?></b> </td>
                            </tr>
                            <tr height='10px'></tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td width='200px'><?= Yii::t('mail.hostess', 'Period:') ?> </td>
                                <td><b><?= \Yii::$app->util->formatDate($model->date_from) ?> - <?= \Yii::$app->util->formatDate($model->date_to); ?> (<?= $model->getTotalBookingDays() ?> &nbsp; <?= Yii::t('mail.hostess', 'day(s)') ?>)</b></td>
                            </tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td width='200px'><?= Yii::t('mail.hostess', 'Venue:') ?></td> 
                                <td><b><?= $model->event->venue; ?></b></td>
                            </tr>   
                            <tr height='10px'></tr>
                            <tr>
                                <td width='200px'><?= Yii::t('mail.hostess', 'Event Category:') ?></td> 
                                <td><b><?= $model->event->type->name; ?></b></td>
                            </tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td valign='top' width='200px'><?= Yii::t('mail.hostess', 'Job description:') ?> </td>
                                <td valign='top'><?= $model->jobName; ?></td>
                            <tr height='10px'></tr>
                            <tr>
                                <td valign='top' width='200px'><?= Yii::t('mail.hostess', 'Outfit:') ?> </td>
                                <td valign='top'><?= $model->dressCode->translatedName; ?></td>
                            </tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td valign='top' width='200px'><?= Yii::t('mail.hostess', 'Daily rate:') ?> </td>

                                <td>
                                    <?= Yii::$app->util->formatEventPrice($model->event, $model->price) ?>
                                </td>
                            </tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td valign='top' width='200px'>
                                    <?= Yii::t('mail.hostess', 'Payment of the daily rate:') ?>
                                </td>
                                <td>
                                    <?= $model->getPaymentTypeLabel() ?>
                                </td>
                            </tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td colspan='2' style="color:red;text-align:justify; text-justify:inter-word;"><br/><?= Yii::t('mail.hostess', 'Please contact the client immediately and sign a contract. An order shall only be accepted if the order has been confirmed in writen form by a contract between the contractor (you) and the customer at the latest two working days after receipt of this e-mail. If the customer has not signed the contract within the stipulated deadline, BOOKmyHOSTESS.com must be notified immediately in written form or by telephone. Otherwise, there is no claim to an assignment. <br><br> Please contact your client to clarify details about your assignment using the information below:') ?></td> 
                            </tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td>                                                            
                                    <?= Yii::t('mail.hostess', 'Name:') ?>
                                </td> 
                                <td>                                                            
                                    <b><?= $model->userRequested->first_name; ?> <?= $model->userRequested->last_name; ?>
                                </td> 
                            </tr> 
                            <tr height='10px'></tr>
                            <tr>
                                <td>                                                               
                                    <?= Yii::t('mail.hostess', 'E-mail:') ?>
                                </td> 
                                <td >                                                               
                                    <b><?= $model->userRequested->email; ?>
                                </td> 
                            </tr> 
                            <tr height='10px'></tr>
                            <tr>
                                <td>                                                        
                                    <?= Yii::t('mail.hostess', 'Tel.:') ?>
                                </td> 
                                <td>                                                        
                                    <?= $model->userRequested->phone_number; ?>
                                </td> 
                            </tr>
                            <tr height='10px'></tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td colspan='2'><?= Yii::t('mail.hostess', 'In attachment of this E-mail you can find invoice template.') ?></td> 
                            </tr>
                            <tr height='10px'></tr>
                            <tr height='10px'></tr>
                            <tr>
                                <td colspan='2'><?= Yii::t('mail.hostess', 'We wish you a successfull <b>{event}</b>!', ["event" => $model->event->name]) ?></td> 
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
