<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <table style="width:100%; padding: 20px 10px 20px 10px; background-color: #f0f0f0;">
            <tr>
                <td>
                    <table style="width:100%;padding: 20px 10px 20px 10px;background-color: #fff; font-family:Arial,sans-serif;table-layout:fixed;text-align:left;border:1px solid #cecece;border-bottom:2px solid #bcbcbc;">
                        <tr>
                            <td>
                                <?= $content; ?> 
                                <?= $this->render("_footer") ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br/>
                </td>
            </tr>
            <tr>
                <td style="color:#999999;font-size:13px;line-height:15px;font-family:arial,Arial,sans-serif;letter-spacing:0px;text-decoration:none; text-align: center;">
                    &copy; <?= date("Y") ?> BOOKmyHOSTESS.com
                </td>
            </tr>
        </table>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
