<br>
<br>
<table style="width: 100%;">
    <tr>
        <td>
            <div style="opacity: 1;">
                <div style="max-width:800px;  direction: ltr;">
                    <div class="wisestamp_app main_sig ui-sortable-handle" id="tp1s" style="max-width: 800px;">
                        <table style="width: 800px;" width="800" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="display: inline-block; text-align:initial; font:13px Arial;color:black;">

                                        <?= Yii::t('mail.exhibitor', 'Best Regards,') ?>
                                        <br>
                                        <br>

                                        <b>
                                            <?= Yii::t('mail.exhibitor', 'Your BOOKmyHOSTESS.com - Team.') ?>
                                        </b>

                                        <br>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <table style="width: 800px;" width="800" cellspacing="0" cellpadding="0" border="0">
                            <tbody>

                                <tr valign="top">
                                    <td style="width: 10px; padding-right:10px;"><img
                                            src="https://www.bookmyhostess.com/images/logo.png"
                                            alt="photo"
                                            style="border-radius:0;moz-border-radius:0;khtml-border-radius:0;o-border-radius:0;webkit-border-radius:0;ms-border-radius: 0;width:237px;height:75px;max-width: 237px;"
                                            width="237" height="75"></td>
                                    <td style="border-right: 1px solid #0caa9d;"></td>
                                    <td style="display: inline-block; text-align:initial; font:12px Arial;color:#646464;padding:0px 10px">
                                        <table cellspacing="0" cellpadding="0" border="0">
                                            <tbody>

                                                <tr>

                                                    <td style="font:12px Arial; color:#8d8d8d;font-size:12px;padding:0px 0">
                                                        <span style="display:inline-block;">
                                                            <a href="mailto:info@bookmyhostess.com" style="display: inline-block; color:#8d8d8d;text-decoration: none;">
                                                                info@bookmyhostess.com
                                                            </a>
                                                        </span>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="font:12px Arial; color:#8d8d8d;font-size:12px;padding:5px 0">
                                                        <span style="white-space: nowrap;display:inline-block;">
                                                            <a href="https://www.bookmyhostess.com" rel="nofollow" target="_blank" style="display: inline-block; color:#8d8d8d;text-decoration: none;">
                                                                www.BOOKmyHOSTESS.com
                                                            </a>
                                                        </span>   
                                                    </td> 
                                                </tr> 
                                                <tr> 
                                                    <td style="font:12px Arial; color:#8d8d8d;font-size:12px;padding:5px 0">
                                                        <span style="color:#8d8d8d; display:inline-block;">Ottostr. 7 50859 Cologne Germany</span>
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td style="font:12px Arial; color:#8d8d8d;font-size:12px;padding:5px 0">
                                                        <span style="white-space: nowrap;display:inline-block;">
                                                            <a href="tel:0049015202964261" rel="nofollow" target="_blank" style="display: inline-block; color:#8d8d8d;text-decoration: none;">
                                                                +49 (0) 152/02 964 261
                                                            </a>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
<br>
<table style="width: 100%">
    <tr height="20"><td></td></tr>
    <tr style="height: 10%;"><td><br></td></tr>
    <tr>
        <td style="font-size:7.5pt;">
            <?= Yii::t('app', 'This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited.') ?>
        </td>
    </tr>
    <tr>
        <td><span style="font-size:18pt;color:green;"><i>P</i></span><span style="font-size:7.5pt;color:green;"><i><?= Yii::t('app', 'Please consider the environment before printing this e-mail.') ?></i></span></td>
    </tr>
</table>