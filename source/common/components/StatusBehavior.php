<?php

namespace common\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class StatusBehavior extends Behavior
{

    public $statusAttribute = 'status_id';
    public $status_id;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    public function beforeValidate($event)
    {
        if ($event->sender->isNewRecord && $event->sender->hasAttribute($this->statusAttribute) && empty($event->sender->getAttribute($this->statusAttribute)))
        {
            $event->sender->setAttribute($this->statusAttribute, $this->status_id);
        }
    }

}
