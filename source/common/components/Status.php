<?php

namespace common\components;

use common\models\Status as StatusModel;

/**
 *
 */
class Status extends \yii\base\Component
{
    private $statuses;
    private $statusMap = [];
    private $_statusIdCaption = [];
    private $_statusCaptions = [];

    public function init()
    {
        parent::init();
        $this->statuses = StatusModel::find()->all();
        foreach ($this->statuses as $status)
        {
            $this->statusMap[$status->name] = $status->id;
        }
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->statusMap))
        {
            return $this->statusMap[$name];
        }

        return parent::__get($name);
    }

    /**
     * Delayed because of translations.
     *
     * They would try to access labels which would try to access this component
     *
     * @return array
     */
    public function getStatusIdCaption()
    {
        if (empty($this->_statusIdCaption))
        {
            foreach ($this->statuses as $status)
            {
                $this->_statusIdCaption[$status->id] = $status->label;
            }
        }

        return $this->_statusIdCaption;
    }

    /**
     * Delayed because of translations.
     *
     * They would try to access labels which would try to access this component
     *
     * @return array
     */
    public function getStatusCaptions()
    {
        if (empty($this->_statusCaptions))
        {
            foreach ($this->statuses as $status)
            {
                $this->_statusCaptions[$status->name] = $status->label;
            }
        }

        return $this->_statusCaptions;
    }

    /**
     *
     * @param string $name
     *
     * @return int
     */
    public function getStatusIdFromName($name)
    {
        return $this->statusMap[$name];
    }

    /**
     *
     * @param string $name
     *
     * @return int
     */
    public function getStatusCaptionFromName($name)
    {
        return $this->statusCaptions[$name];
    }

    /**
     * Create array with id => caption that can be used for dropdown menus
     * from a list of status names
     * @param $statuses
     * @return array
     */
    public function createStatusDropdownList($statuses)
    {
        $dropdownList = [];

        foreach ($statuses as $id)
        {
            $dropdownList[$id] = $this->statusIdCaption[$id];
        }

        return $dropdownList;
    }
}
