<?php 

namespace common\components;

use Yii;

/**
 * Helper za kreiranje thumb url-ova
 * 
 * Lista parametara koji se mogu koristiti i default vrijednosti
  src  = null;     // SouRCe filename
  new  = null;     // NEW image (phpThumb.php only)
  w    = null;     // Width
  h    = null;     // Height
  wp   = null;     // Width  (Portrait Images Only)
  hp   = null;     // Height (Portrait Images Only)
  wl   = null;     // Width  (Landscape Images Only)
  hl   = null;     // Height (Landscape Images Only)
  ws   = null;     // Width  (Square Images Only)
  hs   = null;     // Height (Square Images Only)
  f    = null;     // output image Format
  q    = 75;       // jpeg output Quality
  sx   = null;     // Source crop top-left X position
  sy   = null;     // Source crop top-left Y position
  sw   = null;     // Source crop Width
  sh   = null;     // Source crop Height
  zc   = null;     // Zoom Crop
  bc   = null;     // Border Color
  bg   = null;     // BackGround color
  fltr = array();  // FiLTeRs
  goto = null;     // GO TO url after processing
  err  = null;     // default ERRor image filename
  xto  = null;     // extract eXif Thumbnail Only
  ra   = null;     // Rotate by Angle
  ar   = null;     // Auto Rotate
  aoe  = null;     // Allow Output Enlargement
  far  = null;     // Fixed Aspect Ratio
  iar  = null;     // Ignore Aspect Ratio
  maxb = null;     // MAXimum Bytes
  down = null;     // DOWNload thumbnail filename
  md5s = null;     // MD5 hash of Source image
  sfn  = 0;        // Source Frame Number
  dpi  = 150;      // Dots Per Inch for vector source formats
  sia  = null;     // Save Image As filename
 */
class PhpThumb
{

	//phpthumb configuration, check phpThumb.config.php file
	const HIGH_SECURITY_URL_SEPARATOR = '&';
	const HIGH_SECURITY_PASSWORD = 'Z^hd$p&*Q8#UD7F&y6jS29GxZHxeY%R^';
	
	//thumb script
	const PHPTHUMB_SCRIPT_PATH = '/thumb/phpThumb.php';

	/**
	 * Funkcija kreira URL za thumb slike na osnovu datih parametara
	 * @param array $params
	 */
	public static function Url($params = [])
	{
		//encode params
		$paramsEncoded = [];
		foreach ($params as $key => $value) {
			if (is_array($value)) {
				// e.g. fltr[] is passed as an array
				foreach ($value as $subvalue) {
					$paramsEncoded[] = $key . '[]=' . rawurlencode($subvalue);
				}
			} else {
				$paramsEncoded[] = $key . '=' . rawurlencode($value);
			}
		}

		$paramsString = implode(self::HIGH_SECURITY_URL_SEPARATOR, $paramsEncoded);

		return Yii::getAlias("@web") . self::PHPTHUMB_SCRIPT_PATH . '?' . $paramsString . self::HIGH_SECURITY_URL_SEPARATOR . 'hash=' . md5($paramsString . self::HIGH_SECURITY_PASSWORD);
	}
}

?>
