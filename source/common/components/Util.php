<?php

namespace common\components;

use Yii;
use common\models\Country;
use DateTime;

class Util extends \yii\base\Component
{

    public $thousandSeparator;
    public $decimalSeparator;
    public $currencyCode;
    public $decimalPlaces;
    public $sufix;
    public $prefix;
    public $femaleNumber;
    public $maleNumber;
    public $businessLicence;
    public $shortTermEmployment;

    public function getPaymentTypeForHostess()
    {
        return [
            $this->businessLicence => Yii::t('app', 'On account (trade licence) *'),
            $this->shortTermEmployment => Yii::t('app', 'On income tax card **')
        ];
    }

    public function getLastMinutePercentage()
    {
        return Yii::$app->settings->last_minute_percentage;
    }

    public function getLastMinutePriceAddition($price)
    {
        $lastMinutePercentage = $this->getLastMinutePercentage();
        $lastMinuteCoefficient = $lastMinutePercentage / 100;

        return round($lastMinuteCoefficient * $price, 2);
    }

    public function getLastMinutePrice($startPrice)
    {
        $addition = $this->getLastMinutePriceAddition($startPrice);

        return round($startPrice + $addition, 2);
    }

    public function getDaysToEvent(\common\models\Event $event)
    {
        $zone = new \DateTimeZone('UTC');

        $now = new DateTime(date('Y-m-d'), $zone);
        $eventStart = new DateTime($event->start_date, $zone);
        $now->setTimeZone($zone);
        $eventStart->setTimeZone($zone);

        return $now->diff($eventStart)->format('%R%a');
    }

    public function isRequestLastMinute(\common\models\Event $event)
    {
        $days = $this->getDaysToEvent($event);

        if ($days <= Yii::$app->settings->last_minute_days)
        {
            return true;
        }

        return false;
    }

    public function getVideoLink()
    {
        if (isset(Yii::$app->language) && isset(Yii::$app->params['videos'][Yii::$app->language]))
        {
            return Yii::$app->params['videos'][Yii::$app->language];
        }
        return Yii::$app->params['videos']["de"];
    }

    public function formatDate($date)
    {
        return date("d.m.Y", strtotime($date));
    }

    public function formatDateTime($dateTime)
    {
        return date("d.m.Y H:i:s", strtotime($dateTime));
    }

    public function formatEventPrice($event, $price)
    {
        return $this->formatPrice($price) . ' ' . $event->country->currency_label;
    }

    public function formatPrice($price)
    {
        return number_format($price, $this->decimalPlaces, $this->decimalSeparator, $this->thousandSeparator);
    }

    public function formatPriceAsCurrency($price)
    {
        return $this->prefix . $this->formatPrice($price) . $this->sufix;
    }

    public function isGerman($country_id)
    {
        $germany = Country::findOne(["name" => "Germany"]);
        return is_object($germany) && $germany->id == $country_id;
    }

    public function isFemale($s)
    {
        return $s == $this->femaleNumber;
    }

    public function isMale($s)
    {
        return $s == $this->maleNumber;
    }

    public function calculateDiscountFromCoupon($originalAmount, $coupon)
    {
        if ($coupon->isPercentage)
        {
            return $this->calculateDiscountFromPercentage($originalAmount, $coupon->amount);
        }
        if ($coupon->isFixed)
        {
            return $this->calculateDiscountFromAmount($originalAmount, $coupon->amount);
        }
        return 0.0;
    }

    private function calculateDiscountFromPercentage($originalAmount, $discountPercentage)
    {
        $amount = (1 - $discountPercentage / 100) * $originalAmount;
        if ($amount < 0)
        {
            $amount = 0;
        }
        return $amount;
    }

    private function calculateDiscountFromAmount($originalAmount, $discountAmount)
    {
        $amount = $originalAmount - $discountAmount;
        if ($amount < 0)
        {
            $amount = 0;
        }
        return $amount;
    }

}
