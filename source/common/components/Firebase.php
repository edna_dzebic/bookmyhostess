<?php

namespace common\components;

use Yii;

class Firebase extends \yii\base\Component
{

    /**
     * @var string the auth_key Firebase cloude messageing server key.
     */
    public $key;

    /**
     * @var string the api_url for Firebase cloude messageing.
     */
    private $url = "https://fcm.googleapis.com/fcm/send";

    /**
     * high level method to send notification for a specific tokens (registration_ids) with FCM
     * see https://firebase.google.com/docs/cloud-messaging/http-server-ref
     * see https://firebase.google.com/docs/cloud-messaging/concept-options#notifications_and_data_messages
     * 
     * @param array  $tokens the registration ids
     * @param array  $data
     * @return mixed
     */
    public function sendDataNotification($tokens = [], $data = [])
    {

        $body = [
            'registration_ids' => $tokens,
            'data' => $data
        ];

        return $this->send($body);
    }

    /**
     * high level method to send notification for a specific tokens (registration_ids) with FCM
     * see https://firebase.google.com/docs/cloud-messaging/http-server-ref
     * see https://firebase.google.com/docs/cloud-messaging/concept-options#notifications_and_data_messages
     * 
     * @param array  $tokens the registration ids
     * @param array  $data
     * @param array  $notification
     * @return mixed
     */
    public function sendIOSDataNotification($tokens = [], $data = [], $notification = [])
    {

        $body = [
            'registration_ids' => $tokens,
            'data' => $data,
            'notification' => $notification
        ];

        return $this->send($body);
    }

    /**
     * send raw body to FCM
     * @param array $body
     * @return mixed
     */
    private function send($body)
    {
        $headers = [
            "Authorization:key={$this->key}",
            'Content-Type: application/json',
            'Expect: ',
        ];

        $ch = curl_init($this->url);

        curl_setopt_array($ch, [
            CURLOPT_POST => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FRESH_CONNECT => false,
            CURLOPT_FORBID_REUSE => false,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_POSTFIELDS => json_encode($body),
        ]);

        $result = curl_exec($ch);

        if ($result === false)
        {
            Yii::error('Curl failed: ' . curl_error($ch) . ", with result=$result");
        }
        $code = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code < 200 || $code >= 300)
        {
            Yii::error("got unexpected response code $code with result=$result");
        }
        curl_close($ch);
        $result = json_decode($result, true);

        return $result;
    }

}
