<?php

namespace common\components;

use Yii;
use yii\base\Object;

class AuthMan extends Object
{
    public function checkAccess($userId, $permissionName, $params = [])
    {
        if (!\Yii::$app->user->isGuest)
        {
            return Yii::$app->user->identity->role == $permissionName;
        }
        
        return false;
    }

}
