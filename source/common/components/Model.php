<?php

namespace common\components;

use Yii;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

abstract class Model extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => $this->getBeforeInsertAttrib(),
                    ActiveRecord::EVENT_BEFORE_UPDATE => $this->getBeforeUpdateAttrib(),
                ],
                'value' => function ()
        {
            return $this->currentDateTime();
        }
            ],
            'blameable' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'createdByAttribute' => $this->hasAttribute('created_by') ? 'created_by' : false,
                'updatedByAttribute' => $this->hasAttribute('updated_by') ? 'updated_by' : false,
            ],
            'setStatus' => [
                'class' => \common\components\StatusBehavior::className(),
                'status_id' => $this->getDefaultBehaviourStatus()
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'status_id' => Yii::t('admin', 'Status'),
            'date_created' => Yii::t('admin', 'Created'),
            'date_updated' => Yii::t('admin', 'Updated'),
            'created_by' => Yii::t('admin', 'Created By'),
            'updated_by' => Yii::t('admin', 'Updated By'),
            'statusLabel' => Yii::t('admin', 'Status')
        ];
    }

    /**
     * @return string date
     */
    protected function currentDateTime()
    {
        return date("Y-m-d H:i:s");
    }

    /**
     * Soft delete model if possible
     * @return boolean
     */
    public function delete()
    {
        if ($this->hasAttribute('status_id'))
        {
            $this->status_id = Yii::$app->status->deleted;
            return $this->save(false);
        } else
        {
            return parent::delete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
        $query = Yii::createObject(ActiveQuery::className(), [get_called_class()]);
        $model = Yii::createObject(get_called_class());
        if ($model->hasAttribute('status_id'))
        {
            $query->andWhere(["not", [$model::tableName() . ".status_id" => Yii::$app->status->deleted]]);
        }

        return $query;
    }

    public function uniqueWithStatus($attribute)
    {
        if (!$this->hasErrors())
        {
            if (!$this->isUnique($attribute, $this->$attribute, $this->id))
            {
                $this->addError($attribute, Yii::t('app', '{attribute} is already taken', ['attribute' => $attribute]));
            }
        }
    }

    /**
     * Check if there are rows with the same attribute => value combination in the database
     *
     * @param $attribute
     * @param $value
     * @return boolean
     */
    public function isUnique($attribute, $value, $id)
    {
        if ($this->find()->andWhere(['not', ['id' => $id]])->andWhere([$attribute => $value])->one())
        {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    protected function getBeforeInsertAttrib()
    {
        $attribs = [];

        if ($this->hasAttribute('date_created'))
        {
            $attribs[] = 'date_created';
        }

        if ($this->hasAttribute('date_updated'))
        {
            $attribs[] = 'date_updated';
        }

        return $attribs;
    }

    /**
     *
     * @return array
     */
    protected function getBeforeUpdateAttrib()
    {
        $attribs = [];

        if ($this->hasAttribute('date_updated'))
        {
            $attribs[] = 'date_updated';
        }

        return $attribs;
    }

    /**
     *
     * @return int
     */
    protected function getDefaultBehaviourStatus()
    {
        return Yii::$app->status->active;
    }

    /**
     * Set status to disabled.
     *
     */
    public function disable()
    {
        $this->status_id = Yii::$app->status->disabled;
        return $this->save();
    }

    /**
     * If status is disabled set it to enable.
     *
     */
    public function enable()
    {
        $this->status_id = Yii::$app->status->active;
        return $this->save();
    }
    
    /**
     * Check if status is disabled
     *
     * @return boolean
     */
    public function isDisabled()
    {
        return $this->status_id === Yii::$app->status->disabled;
    }

    /**
     * Check if status is pending
     *
     * @return boolean
     */
    public function isPending()
    {
        return $this->status_id === Yii::$app->status->pending;
    }

    /**
     * Check if status is active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->status_id === Yii::$app->status->active;
    }

    /**
     * Check if status is deleted
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->status_id === Yii::$app->status->deleted;
    }

}
