<?php

namespace common\components;

use Yii;
use common\models\Setting as SettingModel;
use yii\web\NotFoundHttpException;

class Settings extends \yii\base\Component
{

    private $settings = [];

    public function init()
    {
        parent::init();
        $settings = SettingModel::find()->all();

        foreach ($settings as $setting)
        {
            $this->settings[$setting->key] = $setting->value;
        }
    }

    public function __get($name)
    {
        return $this->settingByKey($name);
    }

    public function isPromotion()
    {
        return $this->is_promotion_time == 1;
    }

    private function settingByKey($name)
    {
        if (!array_key_exists($name, $this->settings))
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested setting ({name}) does not exist.', ['name' => $name]));
        }

        return $this->settings[$name];
    }

    public function getTimeLevelsWithCoefficients()
    {
        return [
            $this->prs_time_level_zero =>
            $this->prs_time_level_zero_coeff,
            $this->prs_time_level_one =>
            $this->prs_time_level_one_coeff,
            $this->prs_time_level_two =>
            $this->prs_time_level_two_coeff,
            $this->prs_time_level_three =>
            $this->prs_time_level_three_coeff,
            $this->prs_time_level_four =>
            $this->prs_time_level_four_coeff,
            $this->prs_time_level_five =>
            $this->prs_time_level_five_coeff,
        ];
    }

}
