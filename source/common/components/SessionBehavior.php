<?php

namespace common\components;

use yii\base\ActionEvent;
use yii\base\Behavior;

/**
 * Session behavior that sets session values based on actions performed
 *
 * @package backend\components
 */
class SessionBehavior extends Behavior
{
    /**
     * Array of actions and keys to set.
     * First levels key defines actions on which to act.
     * First level value defines arrays with session keys and values
     *
     * Example:
     * [
     *      'update' => [
     *          'action' => 'update',
     *          'url' => Url::current()
     *      ]
     * ]
     *
     * @var array
     */
    public $actions;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            \yii\base\Controller::EVENT_BEFORE_ACTION => 'setSession'
        ];
    }

    /**
     * Set session values specified in the actions
     * @param ActionEvent $event
     */
    public function setSession(ActionEvent $event)
    {
        if (isset($this->actions[$event->action->id]))
        {
            $actions = $this->actions[$event->action->id];
            if (is_array($actions))
            {
                foreach ($actions as $key => $value)
                {
                    \Yii::$app->session->set($key, $value);
                }
            }
        }
    }
}