<?php

namespace common\components;

use common\models\AuditLog;
use Yii;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\helpers\Json;

class AuditBehavior extends Behavior
{

    /**
     * Key value pairs for log messages where the key is the action and values is the message to log.
     * Key can contain http method by appending it after the action separated by a semicolon (action:METHOD)
     * @var array
     */
    public $actions = [];

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            \yii\base\Controller::EVENT_AFTER_ACTION => 'log'
        ];
    }

    /**
     * Add log if a message is specified
     * @param ActionEvent $event
     * @return bool
     */
    public function log(ActionEvent $event)
    {
        if ($this->shouldLog($event->action->id))
        {                        
            $model = new AuditLog();
            $model->get = Json::encode($this->getGet());
            $model->post = Json::encode($this->getPost());
            $model->user_id = Yii::$app->user->id;
            $model->page = $event->action->controller->id;
            $model->action = $event->action->id;
            $model->page_item_id = Yii::$app->request->get('id');
            $model->ip = Yii::$app->request->userIP;
            $model->url = \yii\helpers\Url::current();            
            $model->message = $this->populateValues($this->getMessage($event->action->id));

            $model->save();
        }
    }

    /**
     * Get the message from actions
     * @param string $action
     * @return string|null
     */
    private function getMessage($action)
    {
        if (isset($this->actions[$action]))
        {
            return $this->actions[$action];
        } else if (isset($this->actions[$this->getActionMethod($action)]))
        {
            return $this->actions[$this->getActionMethod($action)];
        }

        return null;
    }

    /**
     * Replace value placeholders with actual values
     * @param string $message
     * @return string
     */
    private function populateValues($message)
    {
        $message = $this->replaceWithValues($message, $this->getGet());
        $message = $this->replaceWithValues($message, $this->getPost());

        return $message;
    }

    /**
     * Replace value placeholder in string with a values from values
     * @param string $message
     * @param array $values
     * @return string
     */
    private function replaceWithValues($message, array $values)
    {
        foreach ($values as $key => $value)
        {
            if (is_array($value))
            {
                $message = $this->replaceWithValues($message, $value);
            } else
            {
                $message = str_replace('{' . $key . '}', $value, $message);
            }
        }

        return $message;
    }

    /**
     * Get action with method attached
     * @param string $action
     * @return string
     */
    private function getActionMethod($action)
    {
        return "$action:" . Yii::$app->request->method;
    }

    /**
     * Get request POST parameters
     * @return array
     */
    private function getPost()
    {
        return Yii::$app->request->post();
    }

    /**
     * Get request GET parameters
     * @return array
     */
    private function getGet()
    {
        return Yii::$app->request->get();
    }

    /**
     * Is there an action specified
     * @param string $action
     * @return bool
     */
    private function shouldLog($action)
    {
        return !empty($this->getMessage($action));
    }

}
