<?php

namespace common\components;

use Yii;
use common\models\AuditLog;
use yii\helpers\Url;
use yii\helpers\Json;

/**
 * Log component that saves the current application state to a database table
 */
class Log extends \yii\base\Component {

    /**
     * Whether to skip the next log or not.
     * Base controller afterAction calls the component, it will skip this step if this is true
     * @var boolean
     */
    private $skipNext = false;

    
    /**
     * Save Log to dabase
     * 
     * @param string $page
     * @param int $id
     * @param string $action
     * @param string $message
     * @param boolean $skipNext
     */
    public function log($page, $id, $action, $message = "", $skipNext = false) {
        if ($this->skipNext) {
            $this->skipNext = false;
            return;
        }

        if ($skipNext) {
            $this->skipNext = true;
        }

        $log = new AuditLog();
        $log->user_id = Yii::$app->user->id;
        $log->page = $page;
        $log->page_item_id = (string) $id;
        $log->action = $action;
        $log->ip = Yii::$app->request->userIP;
        $log->url = Url::current();
        $log->get = Json::encode(Yii::$app->request->get());
        $log->post = Json::encode(Yii::$app->request->post());
        $log->date_created = date("Y-m-d H:i:s");

        if ($message !== "") {
            $log->message = Yii::t('app', 'User') . ' ID:' . $log->user_id . ' | ' . $message;
        }

        $log->save();
    }

    /**
     * @param boolean $value
     */
    public function skipNext($value) {
        $this->skipNext = boolval($value);
    }

    /**
     * 
     * @param type $model
     * @return type
     */
    public function defaultMessage($model) {
        return $model->user->username . ' ' . Yii::t('app', 'visited') . ' ' . $model->url;
    }

}
