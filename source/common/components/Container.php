<?php

namespace common\components;

use Yii;

/**
 * Container class for all models
 * @package common\components
 */
class Container extends \yii\base\Model
{

    /**
     * Scenario constants
     */
    const SCENARIO_NEW = 'new';
    const SCENARIO_UPDATE = 'update';
    const SESSION_KEY_BACK_URL = '';
    const SESSION_KEY_BACK_URL_DELETE = '';

    /**
     * @var Model Instance of model
     */
    protected $modelObject;

    /**
     * @var string Class of model
     */
    protected $modelClass = '\common\component\Model';

    /**
     * Container constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if ($this->modelObject === null)
        {
            throw new \InvalidArgumentException("model is required");
        }

        if ($this->modelClass !== null && !is_a($this->modelObject, $this->modelClass))
        {
            throw new \InvalidArgumentException("model has to be instance of " . $this->modelClass);
        }

        $this->setAttributes($this->modelObject->attributes, false);
    }

    /**
     * Attribute labels
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'Id'),
            'statusLabel' => Yii::t('admin', 'Status'),
            'createdByUsername' => Yii::t('admin', 'Created by'),
            'updatedByUsername' => Yii::t('admin', 'Updated by'),
            'status_id' => Yii::t('admin', 'Status'),
            'date_created' => Yii::t('admin', 'Date Created'),
            'date_updated' => Yii::t('admin', 'Date Updated'),
            'created_by' => Yii::t('admin', 'Created By'),
            'updated_by' => Yii::t('admin', 'Updated By'),
        ];
    }

    /**
     * Retrieve back url from session
     *
     * @return mixed
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, \yii\helpers\Url::previous());
    }

    /**
     * Retrieve back url from session
     *
     * @return mixed
     */
    public function getDeleteBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL_DELETE, \yii\helpers\Url::previous());
    }

    /**
     * @return Model
     */
    protected function getModelObject()
    {
        return $this->modelObject;
    }

    /**
     * @param Model $value
     */
    protected function setModelObject($value)
    {
        $this->modelObject = $value;
    }

    /**
     * Get status label
     * @return string
     */
    public function getStatusLabel()
    {
        return is_object($this->modelObject->status) ? $this->modelObject->status->label : "";
    }

    /**
     * Get created by username
     * @return string
     */
    public function getCreatedByUsername()
    {
        return is_object($this->modelObject->createdBy) ? $this->modelObject->createdBy->username : "";
    }

    /**
     * Get updated by username
     * @return string
     */
    public function getUpdatedByUsername()
    {
        return is_object($this->modelObject->updatedBy) ? $this->modelObject->updatedBy->username : "";
    }

}
