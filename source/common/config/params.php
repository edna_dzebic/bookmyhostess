<?php

return [
    'contracts_path' => [
        'exhibitor' => '/www/htdocs/w018f340/bookmyhostess.com/httpdocs/contracts/exhibitor/',
        'hostess' => '/www/htdocs/w018f340/bookmyhostess.com/httpdocs/contracts/hostess/'
    ],
    'invoices_templates_path' => [
        'exhibitor' => '/www/htdocs/w018f340/bookmyhostess.com/httpdocs/invoices_templates/exhibitor/',
        'hostess' => '/www/htdocs/w018f340/bookmyhostess.com/httpdocs/invoices_templates/hostess/'
    ],
    'invoice_save_path' => '/www/htdocs/w018f340/bookmyhostess.com/httpdocs/',
    'uploads_save_path' => '/www/htdocs/w018f340/bookmyhostess.com/httpdocs/uploads/',
    'gallery_save_path' => '/www/htdocs/w018f340/bookmyhostess.com/httpdocs/gallery-images/',
    //
    'user.passwordResetTokenExpire' => 3600,
    //
    'languages' => [
        'en' => 'English',
        'de' => 'Deutsch',
        'it' => 'Italiano'
    ],
    'exhibitor_contract_files' => [
        'en' => 'en/contract.doc',
        'de' => 'de/vertrag.doc',
        'it' => 'it/contratto.doc'
    ],
    'hostess_contract_files' => [
        'en' => 'en/Contract-between-hostess-and-the-client.doc',
        'de' => 'de/Vertrag-zw.-Hostess-und-Kunde.doc',
        'it' => 'it/Contratto-cliente-hostess.doc'
    ],
    'hostess_invoice_files' => [
        'en' => 'en/Invoice-template-from-BOOKmyHOSTESS.odt',
        'de' => 'de/Musterrechnung-von-BOOKmyHOSTESS.odt',
        'it' => 'it/Ricevo.odt'
    ],
    'user_images_url' => '/uploads/',
    'default_image' => '/uploads/no_image.jpg',
    "paypal" =>
    [
//        "url" => "https://www.sandbox.paypal.com/cgi-bin/webscr",
//        "business" => "bmh-fct@gmail.com",
        "custom" => "164527a759e2bdbd4b792c38564371ee",
        "return" => "paypal/complete",
        "notify_url" => "paypal/ipn",
        "item_name" => "BOOKmyHOSTESS.com booking",
        "currency_code" => "EUR",
        'url' => 'https://www.paypal.com/cgi-bin/webscr',
        'business' => 'finanzen@bookmyhostess.com',
    ],
    'api_auth_key' => 'AIzaSyCn5_nEgqUjIv7WpUzNR0STNkeWFDrfOQs',
    'firebase_key' => 'AAAALNEidmY:APA91bFg_mKiKUChm6nwYe_1gQ1iEn0zMQGnrWU52eTn6mbyL-LX23cO3cB6TjL7oBOTmxHZF4lhASqK9_9cT4Y4X_9vWBt62vSAdswTjySfTsJVorCraIUVJxLame5jpozR1Zs9zs1yTKGIhbYE8IBbChpraVuYOw',
    //videos
    'videos' => [
        'de' => '/videos/BOOKmyHOSTESS.mp4',
        'en' => '/videos/BOOKmyHOSTESS.mp4',
        'it' => '/videos/BOOKmyHOSTESS-it.mp4'
    ],
    //
    "pricing" => [
        "germany" =>
        [
            "name" => "Germany",
            "currency" => "EUR",
            "min_price" => 100,
            "max_price" => 150,
            "booking_fee" => 35,
            "overtime_fee" => 15
        ],
        "austria" =>
        [
            "name" => "Austria",
            "currency" => "EUR",
            "min_price" => 100,
            "max_price" => 140,
            "booking_fee" => 35,
            "overtime_fee" => 15
        ],
        "netherlands" =>
        [
            "name" => "Netherlands",
            "currency" => "EUR",
            "min_price" => 100,
            "max_price" => 140,
            "booking_fee" => 35,
            "overtime_fee" => 15
        ],
        "uae" =>
        [
            "name" => "UAE",
            "currency" => "AED",
            "min_price" => 400,
            "max_price" => 600,
            "booking_fee" => 35,
            "overtime_fee" => 60
        ],
        "schweiz" =>
        [
            "name" => "Switzerland",
            "currency" => "CHF",
            "min_price" => 210,
            "max_price" => 270,
            "booking_fee" => 35,
            "overtime_fee" => 30
        ],
        "italy" =>
        [
            "name" => "Italy",
            "currency" => "EUR",
            "min_price" => 80,
            "max_price" => 120,
            "booking_fee" => 35,
            "overtime_fee" => 13
        ],
        "spain" =>
        [
            "name" => "Spain",
            "currency" => "EUR",
            "min_price" => 100,
            "max_price" => 150,
            "booking_fee" => 35,
            "overtime_fee" => 15
        ],
        "france" =>
        [
            "name" => "France",
            "currency" => "EUR",
            "min_price" => 100,
            "max_price" => 150,
            "booking_fee" => 35,
            "overtime_fee" => 15
        ]    
    ]
];
