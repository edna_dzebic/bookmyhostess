<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => 'BOOKmyHOSTESS.com',
    'language' => 'de',
    'components' => [
        'urlManagerFrontend' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'urlManagerBackend' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'status' => [
            'class' => '\common\components\Status'
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
        ],
        'settings' => [
            'class' => 'common\components\Settings'
        ],
        'authManager' => [
            'class' => 'common\components\AuthMan',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LdYcxUUAAAAANc9YOovGHMtmeDIuQAz2skukpnd',
            'secret' => '6LdYcxUUAAAAAF-uSHAH-Nz0PuspbWWhC_9-jEWu',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.bookmyhostess.com',
                'username' => 'dev@bookmyhostess.com',
                'password' => 'bookmyhostess2017!',
                'port' => '25',
//                'encryption' => 'tls'
            ]
        ],
        'pdf' => [
            'class' => kartik\mpdf\Pdf::classname(),
            'format' => kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => kartik\mpdf\Pdf::DEST_DOWNLOAD
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => '.',
            'decimalSeparator' => ',',
            'currencyCode' => 'EUR',
            'dateFormat' => 'MM/dd/Y',
            'datetimeFormat' => 'MM/dd/Y H:i:s'
        ],
        'util' => [
            'class' => 'common\components\Util',
            'thousandSeparator' => '.',
            'decimalSeparator' => ',',
            'currencyCode' => 'EUR',
            'decimalPlaces' => 2,
            'sufix' => ' EUR',
            'prefix' => '',
            'femaleNumber' => 0,
            'maleNumber' => 1,
            'businessLicence' => 1,
            'shortTermEmployment' => 2
        ],
        'auditLog' => [
            'class' => 'common\components\Log'
        ],
        'firebase' => [
            'class' => 'common\components\Firebase',
            'key' => 'AAAALNEidmY:APA91bFg_mKiKUChm6nwYe_1gQ1iEn0zMQGnrWU52eTn6mbyL-LX23cO3cB6TjL7oBOTmxHZF4lhASqK9_9cT4Y4X_9vWBt62vSAdswTjySfTsJVorCraIUVJxLame5jpozR1Zs9zs1yTKGIhbYE8IBbChpraVuYOw'
        ],
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        "yii" => 'yii.php'
                    ]
                ],
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        "app" => 'app.php',
                        "app.hostess" => 'app.hostess.php',
                        "app.exhibitor" => 'app.exhibitor.php',
                        "app.cities" => 'app.cities.php',
                        "app.countries" => 'app.countries.php',
                        "app.languages" => 'app.languages.php',
                        "app.language_levels" => 'app.language_levels.php',
                        "app.colors" => 'app.colors.php',
                        "app.jobs" => 'app.jobs.php',
                        "app.event_types" => 'app.event_types.php',
                        "app.dress_codes" => 'app.dress_codes.php',
                        "app.partner" => 'app.partner.php',
                        "app.cookie" => 'app.cookie.php'
                    ]
                ],
                'mail*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        "mail" => 'mail.php',
                        "mail.hostess" => 'mail.hostess.php',
                        "mail.exhibitor" => 'mail.exhibitor.php',
                    ]
                ],
                'admin' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        "admin" => 'admin.php'
                    ]
                ],
                'impressum' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        "impressum" => 'impressum.php'
                    ]
                ],
                'terms' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        "terms" => 'terms.php'
                    ]
                ],
                'privacy' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        "privacy" => 'privacy.php'
                    ]
                ],
                'new.features' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        "new.features" => 'new.features.php'
                    ]
                ],
                'cities' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        "new.features" => 'cities.php'
                    ]
                ]
            ],
        ],
    ],
    'modules' => [
        'dynagrid' => [
            'class' => '\kartik\dynagrid\Module',
        // other settings (refer documentation)
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        // other module settings
        ],
    ]
];
