<?php

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=d02ce555',
            'username' => 'd02ce555', 
            'password' => 'hdZXw8bqk8AwPL4Y*',
            'charset' => 'utf8',
            'enableSchemaCache' => false,
            'enableQueryCache' => false
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'urlManagerFrontend' => [
            'baseUrl' => "https://www.bookmyhostess.com"
        ],
        'urlManagerBackend' => [
            'baseUrl' => "http://admin.bookmyhostess.com"
        ],
    ],
];
