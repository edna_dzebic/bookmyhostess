<?php

namespace backend\widgets;

use rmrevin\yii\fontawesome\component\Icon;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Menu extends \yii\widgets\Menu
{

    /**
     * @inheritdoc
     */
    public $labelTemplate = '{label}';

    /**
     * @inheritdoc
     */
    public $linkTemplate = '<a href="{url}">{icon}<span>{label}</span>{badge}</a>';

    /**
     * @inheritdoc
     */
    public $submenuTemplate = "\n<ul class=\"nav child_menu\">\n{items}\n</ul>\n";

    /**
     * @inheritdoc
     */
    public $activateParents = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        Html::addCssClass($this->options, 'nav side-menu');
        parent::init();
    }

    /**
     * @inheritdoc
     */
    protected function renderItem($item)
    {
        $renderedItem = parent::renderItem($item);
        if (isset($item['badge']))
        {
            $badgeOptions = ArrayHelper::getValue($item, 'badgeOptions', []);
            Html::addCssClass($badgeOptions, 'label pull-right');
        } else
        {
            $badgeOptions = null;
        }

        return strtr($renderedItem, [
            '{icon}' => isset($item['icon']) ? new Icon($item['icon'], ArrayHelper::getValue($item, 'iconOptions', [])) : '',
            '{badge}' => (isset($item['badge']) ? Html::tag('small', $item['badge'], $badgeOptions) : '') . (isset($item['items']) && count($item['items']) > 0 ? (new Icon('chevron-down'))->tag('span') : ''),
        ]);
    }

    /**
     * Overridden to check for controller default action
     * @inheritdoc
     */
    protected function isItemActive($item)
    {
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0]))
        {
            $route = Yii::getAlias($item['url'][0]);

            if ($route[0] !== '/' && Yii::$app->controller)
            {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }

            $base = ltrim($route, '/');
            $defaultAction = Yii::$app->controller->defaultAction;

            if ($base !== $this->route && "$base/$defaultAction" !== $this->route)
            {
                return false;
            }

            unset($item['url']['#']);
            if (count($item['url']) > 1)
            {
                $params = $item['url'];
                unset($params[0]);
                foreach ($params as $name => $value)
                {
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }

}
