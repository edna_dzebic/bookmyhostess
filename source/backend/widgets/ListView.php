<?php

namespace backend\widgets;

use Yii;
use yii\helpers\Html;

class ListView extends \yii\widgets\ListView
{

    public function __construct($config = [])
    {

        $this->emptyText = Yii::t('admin', 'No results.');

        $this->summary = Yii::t('admin', 'Showing') . " <span>{begin}</span> - <span>{end}</span> " . Yii::t('admin', 'of') . " <span>{totalCount}</span> " . Yii::t('admin', 'results') . ".";

        $this->pager = ['nextPageLabel' => '>', 'prevPageLabel' => '<', 'firstPageLabel' => '<<', 'lastPageLabel' => '>>'];

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function renderPager()
    {
        return Html::tag('div', parent::renderPager(), ['class' => 'dataTables_paginate paging_simple_numbers']);
    }

}
