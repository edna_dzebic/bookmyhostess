<?php

namespace backend\components;

use Yii;

trait CachedSearchTrait
{

    /**
     * True if the search params are not empty
     * False otherwise
     * @var boolean
     */
    public $searchNotEmpty = true;

    /**
     * Get the params from session if $params is empty
     * if not, save the new params to session
     *
     * @param array $params
     *
     * @return array
     */
    public function getCachedSearchParams(array $params)
    {
        $key = static::className() . ".search.params";

        $params = array_merge(Yii::$app->session->get($key, []), $params);


        Yii::$app->session->set($key, $params);
        unset($params['sort']);
        unset($params['page']);
        $this->searchNotEmpty = $this->areValuesInArrayEmpty($params);

        return $params;
    }

    public function areValuesInArrayEmpty($array)
    {
        foreach ($array as $value)
        {
            $empty = true;

            if (is_array($value))
            {
                $empty = $this->areValuesInArrayEmpty($value);
            } else
            {
                if ($value !== "")
                {
                    $empty = false;
                }
            }

            if (!$empty)
            {
                return $empty;
            }
        }

        return true;
    }

}
