<?php

namespace backend\components\ButtonsHeaderWidget;

use yii\base\Widget;
use yii\helpers\Html;

class ButtonsWidget extends Widget
{

    public $buttons = [];
    public $alignButtons;

    public function __construct($config = [])
    {
        $param['align_buttons'] = 'right';
        if (isset($param['align_buttons']))
        {
            $this->alignButtons = $param['align_buttons'];
        }

        parent::__construct($config);
    }

    /**
     * Called to initiate widget
     *
     * @return string
     */
    public function init()
    {
        
    }

    /**
     * Called to run widget
     * @return string
     */
    public function run()
    {
        return $this->render('_buttons', ['buttons' => $this->buttonsToHtml($this->buttons), 'align' => $this->alignButtons]);
    }

    /**
     * Create array with button HTML
     *
     * @param $buttons
     *
     * @return array
     */
    public function buttonsToHtml($buttons)
    {
        $result = [];

        if (empty($buttons))
        {
            return $result;
        }


        foreach ($buttons as $key => $button)
        {
            $class = 'header-button ' . $button['class'];
            $type = isset($button['type']) ? $button['type'] : 'link';
            $content = $button['label'];
            $url = $button['url'];
            $options = empty($button['options']) ? [] : $button['options'];

            if ($type === 'submit')
            {
                $result[] = Html::submitButton($content, ['class' => $class] + $options);
            } else
            {
                $result[] = Html::a($content, $url, ['class' => $class] + $options);
            }
        }

        return $result;
    }

}
