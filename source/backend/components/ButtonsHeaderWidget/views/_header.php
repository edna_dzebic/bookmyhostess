<?php ?>

<div class="x_title">
    <div class="row flex-row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 flex-left">
            <?= "<$titleTag>" ?>
            <?= $title ?>
            <?= "</$titleTag>" ?>
        </div>
        <?php if (isset($buttons)): ?>
            <?= backend\components\ButtonsHeaderWidget\ButtonsWidget::widget(['buttons' => $buttons]) ?>
        <?php endif; ?>
    </div>
</div>



