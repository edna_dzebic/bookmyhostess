<?php

namespace backend\components\ButtonsHeaderWidget;

use Yii;
use yii\base\Widget;

class HeaderWidget extends Widget
{

    public $title;
    public $smallTitle = false;
    public $buttons = [];
    public $showSearch = true;
    public $searchIsEmpty = true;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        if ($this->showSearch)
        {
            $this->addSearchButtons();
        }

        return $this->render('_header', ['title' => ucwords(str_replace("_", " ", $this->title)), 'titleTag' => $this->smallTitle ? "h2" : "h1", 'buttons' => $this->buttons]);
    }

    public function addSearchButtons()
    {
        $this->buttons = array_merge([
            'show-search' => [
                'class' => 'show-search btn btn-default ' . ($this->searchIsEmpty ? '' : 'not-important-hidden'),
                'url' => 'javascript:void(0)',
                'label' => Yii::t('admin', 'Show search')
            ],
            'hide-search' => [
                'class' => 'hide-search btn btn-default ' . ($this->searchIsEmpty ? 'not-important-hidden' : ''),
                'url' => 'javascript:void(0)',
                'label' => Yii::t('admin', 'Hide search')
            ]
                ], $this->buttons);
    }

}
