<?php

namespace backend\components;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;

class Controller extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'sysadmin']
                    ]
                ]
            ]
        ];
    }

    public function init()
    {
        if (Yii::$app->user->isGuest && $this->id !== 'site' && $this->id !== 'user')
        {
            $this->redirect('/');
        }


        $request = Yii::$app->request;
        $prev_uri = $request->referrer;
        $current_url = $request->url;

        if (!$request->isAjax)
        {
            if ($prev_uri != null && strstr($prev_uri, $current_url) == false)
            {
                Url::remember($prev_uri);
            } else
            {
                if ($prev_uri == null)
                {
                    $prev_uri = $this->createBackUrl(Yii::$app->request);
                    Url::remember($prev_uri);
                } else
                {
                    Url::remember(Url::previous());
                }
            }
        }

        parent::init();
    }

    public function getUserIdentity()
    {
        return Yii::$app->user->identity ? : null;
    }

    private function createBackUrl($request)
    {
        try
        {
            $query_params = $request->queryParams;
            $current_url = $request->pathInfo;
            $controller = explode("/", $current_url)[0];

            if (isset($query_params["id"]))
            {
                return Url::to(["/" . $controller . "/view", "id" => $query_params["id"]]);
            } else
            {
                return Url::to(["/" . $controller . "/index"]);
            }
        } catch (\ErrorException $e)
        {
            return Url::to("/dashboard");
        }
    }

}
