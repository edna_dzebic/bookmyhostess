<?php

namespace backend\components\Sidebar;

use yii\helpers\Url;

class Sidebar extends \yii\base\Widget
{

    /**
     * The items to be rendered
     * @var array
     */
    public $items;

    /**
     * Class of the main container
     * @var string
     */
    public $class = 'stretch-full-height';

    /**
     * ID of the main container
     * @var string
     */
    public $id = 'sidebar-wrapper';

    /**
     * Class of the background element
     * @var string
     */
    public $backClass = 'sidebar-back';

    /**
     * Class of the list element containing the main items
     * @var string
     */
    public $ulClass = 'sidebar-nav';

    /**
     * Class of the active <li> item
     * @var type
     */
    public $activeClass = 'active-title';

    /**
     * Class of the span element that contains the label
     * @var string
     */
    public $labelClass = 'sidebar-menu-item-text';

    /**
     * Class of the <ul> element containg the sub items
     * @var string
     */
    public $subMenuClass = 'nav nav-list collapse submenu';

    /**
     * Renders the widget
     * @return string html
     */
    public function run()
    {
        return $this->render('sidebar', [
                    'items' => $this->parseItems(),
                    'id' => $this->id,
                    'class' => $this->class,
                    'ulClass' => $this->ulClass,
                    'backClass' => $this->backClass,
                    'activeClass' => $this->activeClass,
                    'labelClass' => $this->labelClass,
                    'subMenuClass' => $this->subMenuClass
        ]);
    }

    /**
     * Takes $items and parses them for rendering
     * Checks urls to find out which item will be active
     * @return array
     */
    private function parseItems()
    {
        $items = [];
        foreach ($this->items as $item)
        {
            $item['class'] = $this->issetDefault($item, 'class');
            $item['id'] = $this->issetDefault($item, 'id');
            $item['label'] = $this->issetDefault($item, 'label');

            $item['url'] = isset($item['url']) && !isset($item['items']) ? Url::to($item['url']) : 'javascript:void(0)';
            $item['isActive'] = strstr(Url::current(), Url::to($item['url']));

            if (isset($item['items']))
            {
                $subItems = [];
                foreach ($item['items'] as $subItem)
                {
                    $subItem['url'] = isset($subItem['url']) ? Url::to($subItem['url']) : 'javascript:void()';
                    //                    $subItem['isActive'] = strstr(Url::current(), Url::to($subItem['url'])) ? 'active' : '';

                    $subItem['isActive'] = Url::current() == Url::to($subItem['url']) ? 'active' : '';

                    $subItem['activeClass'] = $subItem['isActive'] ? 'active' : '';

                    if ($subItem['isActive'])
                    {
                        $item['isActive'] = true;
                    }

                    $subItems[] = $subItem;
                }
                $item['items'] = $subItems;
            }

            $item['activeClass'] = $item['isActive'] ? $this->activeClass : '';

            $items[] = $item;
        }

        return $items;
    }

    /**
     * Check if $key exists in $array and return it
     * if not, return $default instead
     * @param array $array
     * @param string|int $key
     * @param mixed $default
     * @return mixed
     */
    private function issetDefault($array, $key, $default = '')
    {
        return isset($array[$key]) ? $array[$key] : $default;
    }

}
