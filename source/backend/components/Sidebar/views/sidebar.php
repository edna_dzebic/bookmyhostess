<div class="<?= $backClass ?>"></div>
<div id="<?= $id ?>" class="<?= $class ?>">
    <ul class="<?= $ulClass ?>">

        <?php foreach ($items as $item): ?>
            <li class="<?= $item['class'] ?>" id="<?= $item['id'] ?>">
                <a href="<?= $item['url'] ?>" class="<?= $item['activeClass'] ?>">
                    <span class="nav-icon">
                        <i class="<?= isset($item['icon']) ? $item['icon'] : '' ?> icon-2x"></i>
                    </span>
                    <span class="<?= $labelClass ?>">
                        <?= $item['label'] ?>
                    </span>
                    <?php if (isset($item['items'])): ?>
                        <i class="icon-chevron-down pull-right"></i>
                    <?php endif ?>
                </a>

                <?php if (isset($item['items'])): ?>
                    <ul class="<?= $subMenuClass ?> <?= $item['isActive'] ? 'in' : '' ?>" id="">
                        <?php foreach ($item['items'] as $subItem): ?>
                            <li class="<?= $subItem['activeClass'] ?>">
                                <a href="<?= $subItem['url'] ?>">
                                    <?= $subItem['label'] ?>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                <?php endif ?>
            </li>
        <?php endforeach ?>
    </ul>
</div>