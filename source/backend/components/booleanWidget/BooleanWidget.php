<?php

namespace backend\components\booleanWidget;

use yii\base\Widget;

class BooleanWidget extends Widget {

    public $value;

    public function init() {
        parent::init();
    }

    public function run() {
        return $this->render("index", [
                    "value" => $this->value
        ]);
    }

}
