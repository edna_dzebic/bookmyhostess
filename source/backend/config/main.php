<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'),
        //
        require(__DIR__ . '/../../common/config/params-local.php'),
        //
        require(__DIR__ . '/params.php'),
        //
        require(__DIR__ . '/params-local.php')
        //
);

return [
    'id' => 'app-backend',
    "name" => "BookMyHostess",
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'en',
    'modules' => [
        'redactor' => 'yii\redactor\RedactorModule',
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User', // User must implement the IdentityInterface
            'enableAutoLogin' => true,
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'forceCopy' => YII_DEBUG,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        'i18n' => [
            'translations' => [
                'admin*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'admin' => 'admin.php'
                    ],
                ]
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mail' => array(
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => "@common/messages",
            'sourceLanguage' => 'en_US',
            'fileMap' => array(
                'app' => 'mail.php',
            )
        ),
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    ],
    'params' => $params,
];
