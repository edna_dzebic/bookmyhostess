<?php
/* @var $this yii\web\View */
/* @var $model backend\models\dresscode\DressCodeForm */

$this->title = $title;
?>
<div class="dress-code-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
