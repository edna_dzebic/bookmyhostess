<?php

/* @var $this yii\web\View */
/* @var $model backend\models\exhibitortestimonial\ExhibitorTestimonialForm */

$this->title = $title;
?>
<div class="exhibitor-testimonial-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
