<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\exhibitortestimonial\ExhibitorTestimonialView */

$this->title = $model->id;
?>
<div class="exhibitor-testimonial-view x_panel">

    <?= backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'text:ntext',
                'source',
                'languageName',
            ],
        ]) ?>
    </div>
</div>
