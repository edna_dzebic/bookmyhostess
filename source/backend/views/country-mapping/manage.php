<?php

/* @var $this yii\web\View */
/* @var $model backend\models\countrymapping\CountryMappingForm */

$this->title = $title;
?>
<div class="country-mapping-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
