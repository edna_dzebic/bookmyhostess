<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\countrymapping\CountryMappingView */

$this->title = $model->id;
?>
<div class="country-mapping-view x_panel">

    <?= backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'sourceCountryName',
                'targetCountryName',
            ],
        ]) ?>
    </div>
</div>
