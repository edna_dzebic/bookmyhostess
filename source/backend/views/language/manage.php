<?php
/* @var $this yii\web\View */
/* @var $model backend\models\language\LanguageForm */

$this->title = $title;
?>
<div class="language-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
