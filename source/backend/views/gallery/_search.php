<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\gallery\GalleryIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
    ]);
    ?>

    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'id') ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'file_name') ?>
        </div>
        
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'include_on_home')->widget(Select2::classname(), [
                'data' => $model->getIncludeList(),
                'options' => ['placeholder' => Yii::t("admin", "Select")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'sort_order') ?>
        </div>


        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'alt') ?>
        </div>

    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
