<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\gallery\GalleryForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->errorSummary($model) ?>

    <div class="user-form x_panel-body">

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'alt')->input('text') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'title')->input('text') ?>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, "path")->widget(FileInput::classname(), [
                    'options' => [
                        'accept' => ['image/jpeg', 'image/png']
                    ],
                    'pluginOptions' => [
                        'initialPreview' => [
                            empty($model->modelObject->path) ? null :
                                    Html::img(Yii::$app->urlManagerFrontend->createAbsoluteUrl($model->modelObject->path), ["class" => "file-preview-image img-responsive"])
                        ],
                        'showUpload' => false
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'include_on_home')->checkbox() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'sort_order')->input('number') ?>
            </div>
        </div>

    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
