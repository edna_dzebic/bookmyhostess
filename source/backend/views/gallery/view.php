<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\gallery\GalleryView */

$this->title = $model->id;
?>
<div class="gallery-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'path:ntext',
                'includeHome',
                'sort_order',
                'file_name',
                'alt',
                'title',
                'date_created',
                'date_updated',
                'statusLabel',
                'createdByUsername',
                'updatedByUsername',
            ],
        ])
        ?>

        <div class="row">
            <div class="col-xs-12">
                <label><?= Yii::t('admin', 'Image') ?></label>
            </div>
            <div class="col-xs-12 margin-top-10">
                <?= \yii\helpers\Html::img(Yii::$app->urlManagerFrontend->createAbsoluteUrl($model->modelObject->path), ["class" => "img-responsive"]) ?>
            </div>
        </div>



    </div>
</div>
