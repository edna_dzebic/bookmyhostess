<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\gallery\GalleryIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Gallery');
?>
<div class="gallery-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => [
                    'create' => [
                        'label' => Yii::t('admin', Yii::t('app', 'Add  Image')),
                        'class' => 'btn btn-success',
                        'url' => ['create']
                    ]
                ]
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?>">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'file_name',
                'include_on_home',
                'sort_order',
                'alt',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    'buttons' => [
                        'view' => function ($_, $model)
                        {
                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), ['gallery/view', 'id' => $model->id], [
                                        'title' => Yii::t('admin', 'View'),
                                        'class' => "btn btn-default action-btn btn-xs"
                            ]);
                        },
                                'update' => function ($_, $model)
                        {
                            return Html::a('<i class="glyphicon white glyphicon-pencil"></i> &nbsp;' . Yii::t('admin', 'Update'), ['gallery/update', 'id' => $model->id], [
                                        'title' => Yii::t('admin', 'Update'),
                                        'class' => "btn btn-success action-btn btn-xs"
                            ]);
                        }
                            ],
                            'contentOptions' => [
                                'style' => "text-align: center;"
                            ]
                        ],
                    ]
                ]);
                ?>
    </div>
</div>
