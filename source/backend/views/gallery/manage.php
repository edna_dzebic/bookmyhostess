<?php

/* @var $this yii\web\View */
/* @var $model backend\models\gallery\GalleryForm */

$this->title = $title;
?>
<div class="gallery-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
