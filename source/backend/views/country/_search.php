<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\country\CountryIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
    ]);
    ?>

    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'name') ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'booking_price') ?>
        </div>

    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
