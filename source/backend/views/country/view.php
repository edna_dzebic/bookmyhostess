<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\country\CountryView */

$this->title = $model->name;
?>
<div class="country-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'isEU',
                'isInvoicePaymentAllowed',
                'invoiceTypeLabel',
                'currency_label',
                'currency_symbol',
                'booking_price',
                'date_created',
                'date_updated',
                'statusLabel',
                'createdByUsername',
                'updatedByUsername',
            ],
        ])
        ?>
    </div>
</div>
