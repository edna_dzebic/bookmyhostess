<?php
/* @var $this yii\web\View */
/* @var $model backend\models\country\CountryForm */

$this->title = $title;
?>
<div class="country-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
