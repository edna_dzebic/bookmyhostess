<?php

/* @var $this yii\web\View */
/* @var $model backend\models\hostess\HostessView */

\backend\assets\HostessViewAsset::register($this);

$this->title = $model->modelObject->fullName;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">

                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => $this->title,
                    'showSearch' => false,
                    'buttons' => $model->getButtons()
                ]);
                ?>

                <?=
                $this->render('profile/profile', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>

                <?=
                $this->render('profile/images', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>

                <?=
                $this->render('profile/documents', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>

                <?=
                $this->render('profile/billing_details', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>

                <?=
                $this->render('profile/work_experience', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>

                <?=
                $this->render('profile/education', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>

                <?=
                $this->render('profile/languages', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>

                <?=
                $this->render('profile/preferred_jobs', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>

                <?=
                $this->render('profile/preferred_cities', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>
                
                <?=
                $this->render('profile/country_mappings', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>

                <?=
                $this->render('profile/statistics', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>
                
                <?=
                $this->render('profile/reviews', [
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
                ])
                ?>
            </div>
        </div>
    </div>
</div>


<?= $this->render('disapprove_modal') ?>
