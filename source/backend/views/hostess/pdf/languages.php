<table>    
    <?php
    foreach ($model->getLanguageList() as $languageName => $level) {
        ?>
        <tr>
            <td><?= Yii::t('app', $languageName) ?></td>
            <td><?= Yii::t('app', $level) ?></td>
        </tr>
    <?php }
    ?>
</table>