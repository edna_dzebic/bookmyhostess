<table>  
    <tr>
        <td><?= Yii::t('app', 'Graduation:'); ?> </td>
        <td><?= $model->education_graduation ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('app', 'University:'); ?> </td>
        <td><?= $model->education_university ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('app', 'Vocational Training:'); ?> </td>
        <td><?= $model->education_vocational_training ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('app', 'Profession:'); ?> </td>
        <td><?= $model->education_profession ?></td>
    </tr>
    <tr>
        <td><?= Yii::t('app', 'Special Knowledge:'); ?> </td>
        <td><?= $model->education_special_knowledge ?></td>
    </tr>
</table>