<table style="width: 100%;">
    <tr>
        <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
        </td>
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td style="font-size: 18px; font-weight: bold;" class="green-text">
            <?= Yii::t('app', 'Working experience'); ?>
        </td>
    </tr>
    <tr>
        <td>
            <table style="width: 100%;" class="table-padding table-bordered">
                <thead>
                    <tr>
                        <td><?= Yii::t('app', 'Starting date'); ?></td>
                        <td><?= Yii::t('app', 'Ending date'); ?></td>
                        <td><?= Yii::t('app', 'Event name'); ?></td>
                        <td><?= Yii::t('app', 'Job description'); ?></td>
                        <td><?= Yii::t('app', 'Position'); ?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($model->pastWork) && count($model->pastWork) > 0)
                    {

                        foreach ($model->pastWork as $pastWorkModel)
                        {
                            ?>
                            <tr>
                                <td><?= Yii::$app->formatter->asDate(date("Y-m-d", strtotime($pastWorkModel->start_date))); ?></td>
                                <td><?= Yii::$app->formatter->asDate(date("Y-m-d", strtotime($pastWorkModel->end_date))); ?></td>
                                <td><?= $pastWorkModel->event_name ?></td>
                                <td><?= $pastWorkModel->job_description ?></td>
                                <td><?= $pastWorkModel->position ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
        </td>
    </tr>
</table>


 