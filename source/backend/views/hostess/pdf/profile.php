<?php

use backend\components\booleanWidget\BooleanWidget;
use yii\helpers\Html;

$thumbnailImageUrl = $model->getThumbnailImageUrl(235, 256);
?>

<table style="width: 100%;">
    <tr>
        <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
        </td>
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td class="green-text" style="font-size:18px; font-weight: bold; text-align: left;">
            <b><?= $model->user->username; ?></b>
        </td>
        <td class="green-text" style="font-size:18px; font-weight: bold; text-align: right;">
            <a href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["/hostess/view", "id" => $model->user->id]) ?>" style="color:#0caa9d; "><?= Yii::t('app.exhibitor', 'View Profile') ?></a>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>

                        <?php if (isset($thumbnailImageUrl)) : ?>

                            <?= Html::img(Yii::$app->urlManagerFrontend->createAbsoluteUrl($thumbnailImageUrl)) ?>
                        <?php endif; ?>
                    </td>
                </tr>               
            </table>
        </td> 
        <td>
            <table class="details" style="width: 100%;">
                <tr>
                    <td height="20" colspan="2" >           
                    </td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Age:'); ?></td>
                    <td><?= $model->getCurrentAge(); ?></td>
                </tr>

                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Hair color:'); ?> </td>
                    <td><?= isset($model->hairColor->name) == true ? Yii::t('app', $model->hairColor->name) : ""; ?></td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Height:'); ?></td>
                    <td><?= $model->height; ?></td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Weight:'); ?> </td>
                    <td><?= $model->weight; ?></td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Jeans waist size:'); ?> </td>
                    <td><?= $model->waist_size; ?></td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Shoe size:'); ?></td>
                    <td><?= $model->shoe_size; ?></td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Visible piercing/tattoo:'); ?></td>
                    <td><?=
                        BooleanWidget::widget([
                            "value" => $model->has_tattoo
                        ]);
                        ?></td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Owns a car:'); ?></td>
                    <td><?=
                        BooleanWidget::widget([
                            "value" => $model->has_car
                        ]);
                        ?></td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Driving licence:'); ?></td>
                    <td><?=
                        BooleanWidget::widget([
                            "value" => $model->has_driving_licence
                        ]);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Trade licence:'); ?></td>
                    <td><?=
                        BooleanWidget::widget([
                            "value" => $model->has_trade_licence
                        ]);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 70%;"><?= Yii::t('app', 'Health cert:'); ?></td>
                    <td><?=
                        BooleanWidget::widget([
                            "value" => $model->has_health_certificate
                        ]);
                        ?>
                    </td>
                </tr>

            </table>
        </td> 
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
        </td>
    </tr>
</table>
