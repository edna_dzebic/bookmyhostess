<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use dosamigos\multiselect\MultiSelect;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\hostess\HostessIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-search">

    <?php
    $form = ActiveForm::begin([]);
    ?>

    <div class="row">

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'username') ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'first_name') ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'last_name') ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'email') ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_from')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_to')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'country_id')->widget(Select2::classname(), [
                'data' => $model->getCountryList(),
                'options' => ['placeholder' => Yii::t("admin", "Select country")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label><?= Yii::t('admin', 'Languages') ?></label>
            <?=
            $form->field($model, 'languages')->widget(
                    MultiSelect::classname(), [
                "options" => [
                    'id' => uniqid(),
                    "class" => "hostess-languages-filter",
                    'multiple' => "multiple",
                ], // for the actual multiselect
                'data' => $model->getLanguageList(),
                "clientOptions" => [
                    "enableCaseInsensitiveFiltering" => true,
                    "filterPlaceholder" => Yii::t('admin', "Search.."),
                    "nonSelectedText" => Yii::t('admin', "Select"),
                    "enableFiltering" => true,
                    "includeSelectAllOption" => true,
                    'numberDisplayed' => 0
                ]
                    ]
            )->label(false);
            ?>
        </div>


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'gender')->widget(Select2::classname(), [
                'data' => $model->getGenderList(),
                'options' => ['placeholder' => Yii::t("admin", "Select gender")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'hair_color_id')->widget(Select2::classname(), [
                'data' => $model->getHairColorList(),
                'options' => ['placeholder' => Yii::t("admin", "Select hair color")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, "ageMin") ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, "ageMax") ?>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, "heightMin") ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, "heightMax") ?>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, "weightMin") ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, "weightMax") ?>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, "waistSizeMin") ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, "waistSizeMax") ?>
                </div>
            </div>
        </div>


        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, "shoeSizeMin") ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, "shoeSizeMax") ?>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-3">

            <?= $form->field($model, "hasTattoo")->checkbox(["uncheck" => null]) ?>
            <?= $form->field($model, "hasHealthCertificate")->checkbox(["uncheck" => null]) ?>

        </div>
        <div class="col-xs-3">

            <?= $form->field($model, "hasDrivingLicense")->checkbox(["uncheck" => null]) ?>
            <?= $form->field($model, "hasCar")->checkbox(["uncheck" => null]) ?>

        </div>
        <div class="col-xs-3">

            <?= $form->field($model, "hasTradeLicense")->checkbox(["uncheck" => null]) ?>
            <?= $form->field($model, "isTrained")->checkbox(["uncheck" => null]) ?>

        </div>
        <div class="col-xs-3">

            <?= $form->field($model, "hasMobileApp")->checkbox(["uncheck" => null]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'status_id')->widget(Select2::classname(), [
                'data' => $model->getStatusList(),
                'options' => ['placeholder' => Yii::t("admin", "Select status")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'approval_status')->widget(Select2::classname(), [
                'data' => $model->getApprovedList(),
                'options' => ['placeholder' => Yii::t("admin", "Select approval status")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'approved_at')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>

    </div>
        
    <div class="row">

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label><?= Yii::t('admin', 'Preferred cities') ?></label>
            <?=
            $form->field($model, 'pref_cities')->widget(
                    MultiSelect::classname(), [
                "options" => [
                    'id' => uniqid(),
                    "class" => "hostess-cities-filter",
                    'multiple' => "multiple",
                ], // for the actual multiselect
                'data' => $model->getCityList(),
                "clientOptions" => [
                    "enableCaseInsensitiveFiltering" => true,
                    "filterPlaceholder" => Yii::t('admin', "Search.."),
                    "nonSelectedText" => Yii::t('admin', "Select"),
                    "enableFiltering" => true,
                    "includeSelectAllOption" => true,
                    'numberDisplayed' => 0
                ]
                    ]
            )->label(false);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <label><?= Yii::t('admin', 'Preferred jobs') ?></label>
            <?=
            $form->field($model, 'pref_jobs')->widget(
                    MultiSelect::classname(), [
                "options" => [
                    'id' => uniqid(),
                    "class" => "hostess-jobs-filter",
                    'multiple' => "multiple",
                ], // for the actual multiselect
                'data' => $model->getJobList(),
                "clientOptions" => [
                    "enableCaseInsensitiveFiltering" => true,
                    "filterPlaceholder" => Yii::t('admin', "Search.."),
                    "nonSelectedText" => Yii::t('admin', "Select"),
                    "enableFiltering" => true,
                    "includeSelectAllOption" => true,
                    'numberDisplayed' => 0
                ]
                    ]
            )->label(false);
            ?>
        </div>
        
    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
