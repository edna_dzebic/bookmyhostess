<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\hostess\HostessIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Hostesses');
?>
<div class="event-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => []
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?> ">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">


        <?php
        $widget = $searchModel->getWidget($this->title, $dataProvider);

        $export_menu = "<label>" . Yii::t('admin', 'Export') . "</label><ul class='export-menu-list'>" . $widget . "</ul>";
        ?>

        <?= $export_menu ?>
        <br>
        <br>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'first_name',
                'last_name',
                'email',
                'date_created',
                [
                    'attribute' => 'approved_at',
                    'label' => Yii::t('admin', 'Approved'),
                    'value' => function( $model)
                    {
                        return is_object($model->hostessProfile) ? $model->hostessProfile->approved_at : '';
                    }
                ],
                [
                    'attribute' => 'hostessProfile.approvalStatusLabel',
                    'label' => 'Approval'
                ],
                'statusLabel',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($_, $model)
                        {
                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), [ 'hostess/view', 'id' => $model->id], [
                                        'title' => Yii::t('admin', 'View'),
                                        'class' => "btn btn-default action-btn btn-xs"
                            ]);
                        }
                            ],
                            'contentOptions' => [
                                'style' => "text-align: center;"
                            ]
                        ],
                    ]
                ]);
                ?>
    </div>
</div>
