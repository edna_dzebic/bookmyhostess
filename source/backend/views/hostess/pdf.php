<?php 
use yii\helpers\Html;
?>


<?= $this->render("pdf/profile", ["model" => $model]) ?>

<table style="width: 100%">
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td>
            <table  style="width: 100%;">
                <tr>
                    <td style="font-size: 18px; font-weight: bold;"  class="green-text">
                        <?= Yii::t('app', 'Languages') ?>
                    </td>
                    <td style="font-size: 18px; font-weight: bold;"  class="green-text">
                        <?= Yii::t('app', 'Education') ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <?= $this->render("pdf/languages", ["model" => $model]) ?>
                    </td>
                    <td style="width: 70%;">
                        <?= $this->render("pdf/education", ["model" => $model]) ?>
                    </td>
                </tr>                
            </table>
        </td> 
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
        </td>
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td>
            <?= $this->render("pdf/statistics", ["model" => $model]) ?>
        </td> 
    </tr>
    <tr>
        <td height="20">           
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
        </td>
    </tr>
</table>



<?php if (isset($model->pastWork) && count($model->pastWork) > 0) : ?>

    <div  style="page-break-before:always;"></div>
    <?= $this->render("pdf/work_experience", ["model" => $model]) ?>

<?php endif; ?>

<?= $this->render("pdf/preferred_jobs", ["model" => $model]) ?>


<div  style="page-break-before:always;"></div>
<?php if (isset($model->images) && count($model->images) > 0) : ?>

    <table style="width: 100%;">
        <tr>
            <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
            </td>
        </tr>
        <tr>
            <td height="20">           
            </td>
        </tr>
        <tr>
            <td style="font-size: 18px; font-weight: bold;"  class="green-text">
                <?= Yii::t('app.exhibitor', 'Images'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <table class="table-padding" style="width: 100%;">

                    <tbody>
                        <?php for ($i = 0; $i <= count($model->images) - 1; $i = $i + 2): ?>

                            <tr>
                                <td style="width: 50%;">

                                    <?= Html::img(Yii::$app->urlManagerFrontend->createAbsoluteUrl($model->images[$i]->getCachedImage(360, 400)), ["class" => "img-responsive"]) ?>
                                </td>

                                <?php if ($i + 1 < count($model->images)) : ?>
                                    <td style="width: 50%;">

                                        <?= Html::img(Yii::$app->urlManagerFrontend->createAbsoluteUrl($model->images[$i + 1]->getCachedImage(360, 400)), ["class" => "img-responsive"]) ?>
                                    </td>
                                <?php endif; ?>
                            </tr>
                            <tr>
                                <td height="40">           
                                </td>
                            </tr> 
                        <?php endfor; ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="20">           
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; border-bottom: 2px solid #0caa9d; height: 10%;">           
            </td>
        </tr>
    </table>
<?php endif; ?>
 