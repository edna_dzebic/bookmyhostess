<?php

use yii\helpers\Html;

$thumbnailImageUrl = $profileModel->getThumbnailImageUrl(235, 256);
$defaultImage = $profileModel->getDefaultImageModel(true);
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Images'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => [
                        [
                            'label' => Yii::t('admin', 'Add Image'),
                            'class' => 'btn btn-success',
                            'url' => ['hostess-edit/add-image', 'userId' => $userModel->id]
                        ]
                    ]
                ]);
                ?>
                <?php if (count($profileModel->images) == 0) : ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="text-danger">
                                <?= Yii::t('admin', 'No uploaded images') ?>
                            </p>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="x_panel-body well-lg">
                        <div class="row">
                            <div class="col-xs-12">
                                <p>
                                    <label>
                                        <?= Yii::t('admin', 'Current default image (first image if default is not set)') ?>
                                    </label>
                                </p>    
                            </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <?php if (isset($defaultImage)) : ?>
                                        <?= Html::img(Yii::$app->urlManagerFrontend->createAbsoluteUrl(Yii::$app->urlManagerFrontend->createAbsoluteUrl(Yii::$app->params['user_images_url'] . $defaultImage->image_url)), ["class" => "img-responsive"]) ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                        <div class="row margin-top-30">
                            <div class="col-xs-12">
                                <p>
                                    <label>
                                        <?= Yii::t('admin', 'All images') ?>
                                    </label>
                                </p>    
                            </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <?php
                                    if (isset($profileModel->images) && count($profileModel->images) > 0)
                                    {
                                        ?>
                                        <?php
                                        foreach ($profileModel->images as $image)
                                        {
                                            ?>
                                            <div class="col-lg-4 profile-image-cont">
                                                <div class="row text-center">
                                                    <div class="col-xs-12">
                                                        <a href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl([$image->getImagePath()]) ?>" data-fancybox="images">
                                                            <?=
                                                            Html::img(
                                                                    Yii::$app->urlManagerFrontend->createAbsoluteUrl($image->getImagePath()), [
                                                                "class" => "img-responsive profile-img",
                                                                "height" => 256,
                                                                        "style" => "min-height : 256px; height:256px;"
                                                            ])
                                                            ?>

                                                        </a>
                                                        <br>

                                                    </div>
                                                    <div class="col-xs-12 text-center">

                                                        <?php if ($image->is_default !== 1) : ?>

                                                            <?= Html::a('Set as default', ["/hostess-edit/set-image-as-default", 'userId' => $userModel->id, 'imageId' => $image->id], ["class" => "btn btn-default btn-xs"]) ?>
                                                        <?php endif; ?>

                                                        <?= Html::a('Delete', ["/hostess-edit/delete-image", 'userId' => $userModel->id, 'imageId' => $image->id], ["class" => "btn btn-danger btn-xs"]) ?>                                               
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>  
            </div>
        </div>
    </div>
</div>