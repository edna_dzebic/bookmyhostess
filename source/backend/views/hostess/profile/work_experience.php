<?php

use yii\helpers\Html;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Work experience'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => [
                        [
                            'label' => Yii::t('admin', 'Add Experience'),
                            'class' => 'btn btn-success',
                            'url' => ['hostess-edit/add-experience', 'userId' => $userModel->id]
                        ]
                    ]
                ]);
                ?>


                <div class="table-responsive">
                    <?php if (count($profileModel->pastWork)) : ?>

                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td><?= Yii::t('admin', 'Starting date'); ?></td>
                                    <td><?= Yii::t('admin', 'Ending date'); ?></td>
                                    <td><?= Yii::t('admin', 'Event name'); ?></td>
                                    <td><?= Yii::t('admin', 'Job description'); ?></td>
                                    <td><?= Yii::t('admin', 'Position'); ?></td>
                                    <td class="text-center"><?= Yii::t('admin', 'Options'); ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($profileModel->pastWork as $pastWorkModel) : ?>
                                    <tr>
                                        <td><?= Yii::$app->formatter->asDate(date("Y-m-d", strtotime($pastWorkModel->start_date))); ?></td>
                                        <td><?= Yii::$app->formatter->asDate(date("Y-m-d", strtotime($pastWorkModel->end_date))); ?></td>
                                        <td><?= $pastWorkModel->event_name ?></td>
                                        <td><?= $pastWorkModel->job_description ?></td>
                                        <td><?= $pastWorkModel->position ?></td>

                                        <td class="text-center">
                                            <div class="">
                                                <?= Html::a(Yii::t("admin", "Edit"), ['hostess-edit/update-experience', 'userId' => $userModel->id, 'expId' => $pastWorkModel->id], ['class' => 'btn btn-default btn-xs']) ?> 

                                                <?= Html::a(Yii::t("admin", "Delete"), ['hostess-edit/delete-experience', 'userId' => $userModel->id, 'expId' => $pastWorkModel->id], ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this work experience? Operation cannot be reverted!'), "class" => 'btn btn-danger btn-xs']) ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <div>
                            <p class="text-danger">
                                <?= Yii::t('admin', 'No work experience') ?>
                            </p>
                        </div>
                    <?php endif; ?>    
                </div>


            </div>
        </div>
    </div>
</div>