<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Education'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => [
                        [
                            'label' => Yii::t('admin', 'Edit Education'),
                            'class' => 'btn btn-success',
                            'url' => ['hostess-edit/update-education', 'userId' => $userModel->id]
                        ]
                    ]
                ]);
                ?>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">

                        <tr>
                            <td><?= Yii::t('admin', 'Graduation'); ?> </td>
                            <td><?= $profileModel->education_graduation ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'University'); ?> </td>
                            <td><?= $profileModel->education_university ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Vocational Training'); ?> </td>
                            <td><?= $profileModel->education_vocational_training ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Profession'); ?> </td>
                            <td><?= $profileModel->education_profession ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Special Knowledge'); ?> </td>
                            <td><?= $profileModel->education_special_knowledge ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>