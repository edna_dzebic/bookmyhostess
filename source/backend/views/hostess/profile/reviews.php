<?php

use kartik\rating\StarRating;
use yii\helpers\Html;

$pluginOptions = [
    'readonly' => true,
    'showClear' => false,
    'showCaption' => false,
    'size' => 'xs'
];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Reviews'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => [
                        [
                            'label' => Yii::t('admin', 'Add Review'),
                            'class' => 'btn btn-success',
                            'url' => ['hostess-edit/add-review', 'userId' => $userModel->id]
                        ]
                    ]
                ]);
                ?>

                <div class="table-responsive">

                    <?php if (count($profileModel->hostessCountryMappings)) : ?>

                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td><?= Yii::t('admin', 'Exhibitor') ?></td>
                                    <td><?= Yii::t('admin', 'Event') ?></td>
                                    <td><?= Yii::t('admin', 'Rating') ?></td>
                                    <td><?= Yii::t('admin', 'Comment') ?></td>
                                    <td><?= Yii::t('admin', 'Date created') ?></td>
                                    <td><?= Yii::t('admin', 'Status') ?></td>
                                    <td class="text-center"><?= Yii::t('admin', 'Options') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($profileModel->reviews as $object) : ?>
                                    <tr>
                                        <td><?= $object->exhibitor->fullName ?></td>
                                        <td><?= $object->event->name ?></td>
                                        <td>
                                            <?=
                                            StarRating::widget([
                                                'name' => "rating",
                                                'value' => $object->rating,
                                                'pluginOptions' => $pluginOptions
                                            ]);
                                            ?>
                                        </td>
                                        <td><?= $object->comment ?></td>
                                        <td>
                                            <?= Yii::$app->util->formatDateTime($object->date_created) ?>
                                        </td>
                                        <td>
                                            <?= $object->status->name ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="">
                                                <?= Html::a(Yii::t("admin", "Edit"), ['hostess-edit/update-review', 'userId' => $userModel->id, 'reviewId' => $object->id], ['class' => 'btn btn-default btn-xs']) ?> 

                                                <?= Html::a(Yii::t("admin", "Delete"), ['hostess-edit/delete-review', 'userId' => $userModel->id, 'reviewId' => $object->id], ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this review? Operation cannot be reverted!'), "class" => 'btn btn-danger btn-xs']) ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <div>
                            <p class="text-danger">
                                <?= Yii::t('admin', 'No reviews for this user.') ?>
                            </p>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>