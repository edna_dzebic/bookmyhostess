<?php

use yii\helpers\Html;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Languages'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => [
                        [
                            'label' => Yii::t('admin', 'Add Language'),
                            'class' => 'btn btn-success',
                            'url' => ['hostess-edit/add-language', 'userId' => $userModel->id]
                        ]
                    ]
                ]);
                ?>

                <div class="table-responsive">
                    <?php if (count($profileModel->languages)) : ?>

                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td><?= Yii::t('admin', 'Language') ?></td>
                                    <td><?= Yii::t('admin', 'Level') ?></td>
                                    <td><?= Yii::t('admin', 'Created At') ?></td>
                                    <td class="text-center"><?= Yii::t('admin', 'Options') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($profileModel->languages as $lng): ?>
                                    <tr>
                                        <td>
                                            <?= $lng->language->name ?>
                                        </td>
                                        <td>
                                            <?= $lng->level->name ?>
                                        </td>
                                        <td>
                                            <?= Yii::$app->util->formatDateTime($lng->date_created) ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="">
                                                <?= Html::a(Yii::t("admin", "Edit"), ['hostess-edit/update-language', 'userId' => $userModel->id, 'lngId' => $lng->id], ['class' => 'btn btn-default btn-xs']) ?> 

                                                <?= Html::a(Yii::t("admin", "Delete"), ['hostess-edit/delete-language', 'userId' => $userModel->id, 'lngId' => $lng->id], ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this language? Operation cannot be reverted!'), "class" => 'btn btn-danger btn-xs']) ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <div>
                            <p class="text-danger">
                                <?= Yii::t('admin', 'No languages') ?>
                            </p>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>