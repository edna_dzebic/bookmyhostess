<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Profile'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => [
                        [
                            'label' => Yii::t('admin', 'Edit Profile'),
                            'class' => 'btn btn-success',
                            'url' => ['hostess-edit/update', 'userId' => $userModel->id]
                        ]
                    ]
                ]);
                ?>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td><?= Yii::t('admin', 'ID'); ?></td>
                            <td><?= $userModel->id; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Name'); ?></td>
                            <td><?= $userModel->getFullName(); ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Email'); ?></td>
                            <td><?= $userModel->email; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Username'); ?></td>
                            <td><?= $userModel->username; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Birth date'); ?></td>
                            <td>
                                <?= Yii::$app->util->formatDate($profileModel->birth_date) ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Age'); ?></td>
                            <td><?= $profileModel->getCurrentAge(); ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Hair color'); ?> </td>
                            <td>
                                <?= is_object($profileModel->hairColor) ? Yii::t('admin', $profileModel->hairColor->name) : ""; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Height'); ?></td>
                            <td><?= $profileModel->height; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Weight'); ?> </td>
                            <td><?= $profileModel->weight; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Waist size'); ?> </td>
                            <td><?= $profileModel->waist_size; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Shoe size'); ?></td>
                            <td><?= $profileModel->shoe_size; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Visible piercing/tattoo'); ?></td>
                            <td>
                                <?=
                                $profileModel->has_tattoo ? Yii::t('admin', 'Yes') : Yii::t('admin', 'No')
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Owns a car'); ?></td>
                            <td>
                                <?=
                                $profileModel->has_car ? Yii::t('admin', 'Yes') : Yii::t('admin', 'No')
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Driving licence'); ?></td>
                            <td>
                                <?=
                                $profileModel->has_driving_licence ? Yii::t('admin', 'Yes') : Yii::t('admin', 'No')
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Trade licence'); ?></td>
                            <td>
                                <?=
                                $profileModel->has_trade_licence ? Yii::t('admin', 'Yes') : Yii::t('admin', 'No')
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Health cert'); ?></td>
                            <td>
                                <?=
                                $profileModel->has_health_certificate ? Yii::t('admin', 'Yes') : Yii::t('admin', 'No')
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Overall rating'); ?></td>
                            <td><?= $profileModel->cached_rating; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Price class'); ?></td>
                            <td><?= $profileModel->price_class; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Profile completeness'); ?></td>
                            <td><?= $profileModel->profile_completeness_score; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Profile reliability'); ?></td>
                            <td><?= $profileModel->profile_reliability_score; ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Has mobile app'); ?></td>
                            <td>
                                <?=
                                $profileModel->has_mobile_app ? Yii::t('admin', 'Yes') : Yii::t('admin', 'No')
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Number Rejections After Booking'); ?></td>
                            <td><?= $profileModel->num_rejections_after_booking ?></td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p class="title">
                            <label><?= Yii::t('admin', 'Summary'); ?></label>
                        </p>
                        <p>
                            <?= $profileModel->summary ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>