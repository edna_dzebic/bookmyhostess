<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Billing details'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => [
                        [
                            'label' => Yii::t('admin', 'Edit billing details'),
                            'class' => 'btn btn-success',
                            'url' => ['hostess-edit/update-billing', 'userId' => $userModel->id]
                        ]
                    ]
                ]);
                ?>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td><?= Yii::t('admin', 'Tax number'); ?> </td>
                            <td><?= $profileModel->tax_number ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Phone number'); ?> </td>
                            <td><?= $userModel->phone_number ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Address'); ?> </td>
                            <td><?= $profileModel->billing_address ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Postal code'); ?> </td>
                            <td><?= $profileModel->postal_code ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'City'); ?> </td>
                            <td><?= $profileModel->city ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Country'); ?> </td>
                            <td><?= is_object($profileModel->country)? $profileModel->country->name : '' ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>