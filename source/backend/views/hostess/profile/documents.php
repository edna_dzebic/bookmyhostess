<?php

use yii\helpers\Html;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Documents'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => [
                        [
                            'label' => Yii::t('admin', 'Add Document'),
                            'class' => 'btn btn-success',
                            'url' => ['hostess-edit/add-document', 'userId' => $userModel->id]
                        ]
                    ]
                ]);
                ?>

                <div class="table-responsive">

                    <?php if (count($profileModel->documents)) : ?>

                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td><?= Yii::t('admin', 'Filename') ?></td>
                                    <td><?= Yii::t('admin', 'Created At') ?></td>
                                    <td class="text-center"><?= Yii::t('admin', 'Options') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($profileModel->documents as $doc): ?>
                                    <tr>
                                        <td>
                                            <?= $doc->getShortFileName() ?>
                                        </td>
                                        <td>
                                            <?= Yii::$app->util->formatDateTime($doc->date_created) ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="">
                                                <?= Html::a(Yii::t("admin", "Download"), $doc->getDownloadLink(), ["target" => "_blank", 'class' => 'btn btn-default btn-xs']) ?> 

                                                <?= Html::a(Yii::t("admin", "Delete"), ['hostess-edit/delete-document', 'userId' => $userModel->id, 'documentId' => $doc->id], ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this file? Operation cannot be reverted!'), "class" => 'btn btn-danger btn-xs']) ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <div>
                            <p class="text-danger">
                                <?= Yii::t('admin', 'No uploaded documents') ?>
                            </p>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Postpone trade license'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => []
                ]);
                ?>


                <div class="table-responsive">

                    <?php if (count($profileModel->tradeLicenseExtends)) : ?>

                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td><?= Yii::t('admin', 'Created At') ?></td>
                                    <td><?= Yii::t('admin', 'Requested Until') ?></td>
                                    <td><?= Yii::t('admin', 'Reason') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($profileModel->tradeLicenseExtends as $req): ?>
                                    <tr>
                                        <td>
                                            <?= Yii::$app->util->formatDateTime($req->date_created) ?>
                                        </td>
                                        <td>
                                            <?= Yii::$app->util->formatDate($req->date) ?>
                                        </td>

                                        <td>
                                            <?= $req->reason ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <div>
                            <p class="text-danger">
                                <?= Yii::t('admin', 'Not requested postpone of trade license') ?>
                            </p>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
</div>

