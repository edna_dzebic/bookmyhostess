<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Statistics'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => []
                ]);
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td><?= Yii::t('admin', 'Profile views'); ?></td>
                            <td><?= $profileModel->profile_views ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Last login'); ?> </td>
                            <td>
                                <?= Yii::$app->util->formatDateTime($userModel->last_login) ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Time to respond'); ?></td>
                            <td><?= $profileModel->getTimeToRespond(); ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Request'); ?></td>
                            <td><?= $profileModel->getTotalRequestsCount(); ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Confirmed'); ?></td>
                            <td><?= $profileModel->getConfirmedRequestsCount(); ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Rejected'); ?></td>
                            <td><?= $profileModel->getRejectedRequestCount(); ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Booked'); ?></td>
                            <td><?= $profileModel->getBookedRequestCount(); ?></td>
                        </tr>
                        <tr>
                            <td><?= Yii::t('admin', 'Not answered'); ?></td>
                            <td><?= $profileModel->getNotAnsweredRequestCount(); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>