<?php

use yii\helpers\Html;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Country mappings'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => [
                        [
                            'label' => Yii::t('admin', 'Add Country Mapping'),
                            'class' => 'btn btn-success',
                            'url' => ['hostess-edit/add-country-mapping', 'userId' => $userModel->id]
                        ]
                    ]
                ]);
                ?>

                <div class="table-responsive">

                    <?php if (count($profileModel->hostessCountryMappings)) : ?>

                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td><?= Yii::t('admin', 'Country') ?></td>
                                    <td><?= Yii::t('admin', 'Created At') ?></td>
                                    <td class="text-center"><?= Yii::t('admin', 'Options') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($profileModel->hostessCountryMappings as $object) : ?>
                                    <tr>
                                        <td><?= $object->country->name ?></td>
                                        <td>
                                            <?= Yii::$app->util->formatDateTime($object->date_created) ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="">
                                                <?= Html::a(Yii::t("admin", "Delete"), ['hostess-edit/delete-country-mapping', 'userId' => $userModel->id, 'mappingId' => $object->id], ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this country mapping? Operation cannot be reverted!'), "class" => 'btn btn-danger btn-xs']) ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <div>
                            <p class="text-danger">
                                <?= Yii::t('admin', 'No country mapped for this user.') ?>
                            </p>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>