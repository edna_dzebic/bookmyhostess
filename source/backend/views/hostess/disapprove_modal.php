<div class="modal fade" id="disapprove-hostess-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog left-aligned">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title"><?= Yii::t('admin', 'Disapprove') ?></h3>
            </div>
            <div class="modal-body panel-body" style="min-height: 305px;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <?= Yii::t('admin', 'Cancel') ?>
                </button>
                <button id="options-modal-confirm" type="button" class="btn btn-warning js-btn-confirm-disapprove">
                    <?= Yii::t('admin', 'Disapprove') ?>
                </button>
            </div>           
        </div>
    </div>
</div>