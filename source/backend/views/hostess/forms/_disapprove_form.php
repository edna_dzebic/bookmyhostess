<?php

use yii\widgets\ActiveForm;
?>

<div class="text-center loading-content">
    <div>
        <?= Yii::t('admin', 'Please wait...') ?>
    </div>
    <br>
    <div class="loader js-loader"></div>    
</div>

<?php
$form = ActiveForm::begin([
            "id" => "disapprove-form",
            "action" => ["/hostess/disapprove", "id" => $user_id]]);
?>

<?=
        $form->field($model, 'reason')
        ->dropDownList($model->getReasons(), [
            "prompt" => Yii::t('admin', 'Please select reason')
        ]);
?>

<?=
$form->field($model, 'reason_text')->textarea(["rows" => 6]);
?>

<?php
ActiveForm::end();
?>
