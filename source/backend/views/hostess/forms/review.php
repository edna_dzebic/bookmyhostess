<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\rating\StarRating;

$pluginOptions = [
    'step' => 0.5,
    'readonly' => false,
    'showClear' => true,
    'showCaption' => true,
    'size' => 'xs',
    'defaultCaption' => '{rating}',
    'clearCaption' => Yii::t('admin', '0 stars'),
    'starCaptions' => [
        0 => Yii::t('admin', "Extremely bad"),
        1 => Yii::t('admin', "Very bad"),
        2 => Yii::t('admin', "Bad"),
        3 => Yii::t('admin', "Ok"),
        4 => Yii::t('admin', "Good"),
        5 => Yii::t('admin', "Excellent")
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\hostess\form\ReviewForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = $title;
?>

<div class="review-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="user-form x_panel-body">

        <?= $form->field($model, 'hostess_id')->hiddenInput()->label(false) ?>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'event_id')->widget(Select2::classname(), [
                    'data' => $model->getEventList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select event")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'exhibitor_id')->widget(Select2::classname(), [
                    'data' => $model->getExhibitorList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select exhibitor")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'rating')->widget(StarRating::classname(), [
                    'value' => 0,
                    'pluginOptions' => $pluginOptions,
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'comment')->textarea(["rows" => 6]) ?>
            </div>
        </div>
    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
