<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

$this->title = $title;
?>

<div class="car-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="user-form x_panel-body">

        
        <?= $form->field($model, 'user_id')->input('hidden')->label(false) ?>
        
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'is_default')->checkbox() ?>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, "image_url")->widget(FileInput::classname(), [
                    'options' => [
                        'accept' => ['image/jpeg', 'image/png']
                    ],
                    'pluginOptions' => [
                        'showUpload' => false
                    ]
                ]);
                ?>
            </div>
        </div>

    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
