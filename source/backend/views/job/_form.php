<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\job\JobForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="user-form x_panel-body">

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'order')->textInput() ?>
            </div>
        </div>

    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
