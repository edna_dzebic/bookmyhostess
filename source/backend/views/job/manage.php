<?php
/* @var $this yii\web\View */
/* @var $model backend\models\job\JobForm */

$this->title = $title;
?>
<div class="job-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
