<?php
/* @var $this yii\web\View */
/* @var $model backend\models\countryprice\CountryPriceForm */

$this->title = $title;
?>
<div class="country-price-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
