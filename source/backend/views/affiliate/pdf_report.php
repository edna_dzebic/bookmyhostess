<?php
/* @var $this yii\web\View */
/* @var $model \common\models\Invoice */
?>

<?= $this->render('pdf_parts/_header', ["partner" => $report['partner']]) ?>

<?= $this->render('pdf_parts/_separator') ?>

<?= $this->render('pdf_parts/_title', ["report" => $report]) ?>

        <?= $this->render('pdf_parts/_separator') ?>

<?= $this->render('pdf_parts/_summary', ["report" => $report]) ?>

<?= $this->render('pdf_parts/_separator') ?>

<?= $this->render('pdf_parts/_footer') ?>

<?= $this->render('pdf_parts/_separator') ?>

<div  style="page-break-before:always;"></div>

<table width='100%' style=" width: 100%;">
    <tr>
        <td align="center" style='alignment-baseline: middle;'>
            <div style=" padding:0px; width:100%; font-size:20pt;"><br/>

                <table cellspacing="0" style='border:1px solid black; min-width: 100%; width: 100%;'>
                    <?= $this->render('pdf_parts/_table_header') ?>
                    <?= $this->render('pdf_parts/_items', ["models" => $report['models']]) ?>
                    <?= $this->render('pdf_parts/_totals', ["report" => $report]) ?>
                </table>
            </div>
        </td>
    </tr>
</table>
