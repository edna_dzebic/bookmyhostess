<?php ?>

<table style='border:1px solid black; min-width: 100%; width: 100%;'>
    <tr>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
            <b><?= Yii::t('app.partner', 'Period') ?></b>
        </td>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
            <?= $report['from'] ?> - <?= $report['to'] ?>
        </td>
    </tr>
    <tr>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
            <b><?= Yii::t('app.partner', 'Number of items') ?></b>
        </td>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
                <?= $report['count'] ?>
        </td>
    </tr>
    <tr>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
            <b><?= Yii::t('app.partner', 'Profit') ?></b>
        </td>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
                <?= $report['total'] ?>
        </td>
    </tr>
    <tr>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
            <b><?= Yii::t('app.partner', 'Provision') ?></b>
        </td>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
            <?= $report['partner']->provision ?> %
        </td>
    </tr>
    <tr>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
            <b><?= Yii::t('app.partner', 'Total') ?></b>
        </td>

        <td style='padding: 4px; border:1px solid black;'valign='center'
            align='center'>
            <b><?= $report['provision'] ?></b>
        </td>
    </tr>
</table>

