<tr style="font-size:30px; font-weight: bold;">
    <td style='padding:4px; border:1px solid black;' colspan='3' align='left'>
        <b><?= Yii::t('app.partner', 'PROFIT') ?> </b>
    </td>     
    <td style='padding:4px; border:1px solid black;' align="right">
        <b><?= $report['total'] ?></b>
    </td>
</tr>
<tr style="font-size:30px; font-weight: bold;">
    <td style='padding:4px; border:1px solid black;' colspan='3' align='left'>
        <b><?= Yii::t('app.partner', 'PROVISION') ?> </b>
    </td>     
    <td style='padding:4px; border:1px solid black;' align="right">
        <b><?= $report['partner']->provision ?> %</b>
    </td>
</tr>
<tr style="font-size:30px; font-weight: bold;">
    <td style='padding:4px; border:1px solid black;' colspan='3' align='left'>
        <b><?= Yii::t('app.partner', 'TOTAL') ?> </b>
    </td>     
    <td style='padding:4px; border:1px solid black;' align="right">
        <b><?= $report['provision'] ?></b>
    </td>
</tr>