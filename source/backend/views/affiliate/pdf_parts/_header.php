<table style="width: 100%; font-size:10pt;">
    <tr>
        <td style="width: 70%"></td>
        <td style="width: 30%">
            <table style="width: 100%">
                <tr>
                    <td>
                        <img src='<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["images/pdf-logo.png"]) ?>' width="262px" height="91px">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 70%;">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <span  style="color:#0caa9d;">BOOKmyHOSTESS</span>  |  Ottostr 7. | 50859 <?= Yii::t('app', 'Cologne') ?> - <?= Yii::t('app', 'Germany') ?>
                                </td>                               
                            </tr>
                        </table>                        
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 30%;color:#0caa9d;padding-left: 20px;">
            BOOKmyHOSTESS.com
        </td>
    </tr>
    <tr>
        <td style="width: 70%">
            <table style="width: 100%;font-size:10pt;">
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td>
                        <b><?= $partner->name ?></b>
                    </td>
                </tr>
                <tr>
                    <td><?= $partner->street ?> 
                    </td>
                </tr> 
                <tr>
                    <td>
                        <?= $partner->zip ?> &nbsp;  
                        <?= $partner->city ?> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= isset($partner->country) ? $partner->country->getTranslatedName() : ""; ?>
                    </td>
                </tr>  
                <tr>
                    <td>
                        <br><b><?= Yii::t('app.partner', 'Tax number') ?></b> :&nbsp;<?= $partner->tax_number ?>
                    </td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>

                <tr>
                    <td>
                        <?= Yii::t('app.partner', 'Date:') ?>&nbsp;<?= Yii::$app->util->formatDate(date("Y-m-d")) ?> 
                    </td>
                </tr>

            </table>
        </td>
        <td style="width: 30%">
            <table style="width: 100%;font-size:12px;font-weight: bold;padding-left: 20px;">
                <tr>
                    <td> 
                        <?= Yii::t('app.partner', 'Owner') ?>
                        <br>
                        Adela Kadiric
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        Ottostr 7.
                        <br>
                        50859 <?= Yii::t('app', 'Cologne') ?> - <?= Yii::t('app', 'Germany') ?>
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        M: +49 (0) 152/02 964 261
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        a.kadiric@bookmyhostess.com
                        <br>
                        www.BOOKmyHOSTESS.com
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        <?= Yii::t('app.partner', 'Local tax office') ?> : Köln West
                        <br>
                        <?= Yii::t('app.partner', 'Head office') ?> : Köln
                        <br>
                        <?= Yii::t('app.partner', 'Tax number') ?> : DE 299791123
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        Sparkasse​​KölnBonn
                        <br>
                        IBAN:
                        <br>
                        DE56​3705 0198 1016 8636 96
                        <br>
                        BIC/SWIFT
                        <br>
                        COLSDE33XXX
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>