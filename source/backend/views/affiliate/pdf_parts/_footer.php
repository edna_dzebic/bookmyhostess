<table style=" text-align:justify; ">
    <tr>
        <td align="left" height="20">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.partner', 'The report was issued electronically and is therefore valid without signature.') ?>
        </td>
    </tr>
    <tr>
        <td align="left" height="20">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.partner', 'If you have any questions or comments, please contact us at +4922344301397.') ?>
        </td>
    </tr>
    <tr>
        <td align="left" height="10">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.partner', 'Thank You for being BOOKmyHOSTESS.com partner!') ?>
        </td>
    </tr>
    <tr>
        <td align="left" height="10">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.partner', 'Best Regards, <br> Your BOOKmyHOSTESS.com - Team.') ?>
        </td>
    </tr>
</table>