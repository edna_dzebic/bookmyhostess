<?php

use yii\widgets\DetailView;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">

                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Partner details'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => []
                ]);
                ?>
                <div class="table-responsive">
                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'identification',
                            'name',
                            'provision',
                            'description',
                            'tax_number',
                            'street',
                            'zip',
                            'city',
                            'phone',
                            'email:email',
                            'web',
                            'fax',
                            'contact',
                            'countryName',
                            'date_created',
                            'date_updated',
                            'statusLabel',
                            'createdByUsername',
                            'updatedByUsername',
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>