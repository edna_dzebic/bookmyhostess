<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\affiliate\AffiliateForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="affiliate-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="user-form x_panel-body">

        <div class="row">
            <div class="col-xs-12">
                <p><label><?= Yii::t('admin', 'Basic details') ?></label></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'provision')->textInput(['maxlength' => true]) ?>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'tax_number')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-xs-12">
                <p><label><?= Yii::t('admin', 'Address') ?></label></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                <?= $form->field($model, 'zip')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'country_id')->widget(Select2::classname(), [
                    'data' => $model->getCountryList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select country")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-xs-12">
                <p><label><?= Yii::t('admin', 'Contact') ?></label></p>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'contact')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'web')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
            </div>
        </div>


    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
