<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\affiliate\AffiliateIndex */
/* @var $reportProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Report');
?>
<div class="affiliate-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => false,
                'showSearch' => false,
                'buttons' => []
            ]
    );
    ?>

    <div class="x_panel-body well-lg search">

        <?=
        $this->render(
                '_report_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>


    <div class="x_panel-body">

        <?php if (!empty($report)) : ?>


            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_panel-body well-lg">

                            <?=
                            backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                                'title' => Yii::t('admin', 'Report overview'),
                                'showSearch' => false,
                                'smallTitle' => true,
                                'buttons' => [
                                    [
                                        'label' => Yii::t('admin', 'Download PDF'),
                                        'class' => 'btn btn-success',
                                        'url' => ['affiliate/pdf-report']
                                    ]
                                ]
                            ]);
                            ?>


                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            <b><?= Yii::t('admin', 'Partner') ?></b>
                                        </td>
                                        <td>
                                            <?= $report['partner']->name ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b><?= Yii::t('admin', 'Period') ?></b>
                                        </td>
                                        <td>
                                            <?= $report['from'] ?> - <?= $report['to'] ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <b><?= Yii::t('admin', 'Number of requests') ?></b>
                                        </td>
                                        <td>
                                            <?= $report['count'] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b><?= Yii::t('admin', 'Profit') ?></b>
                                        </td>
                                        <td>
                                            <?= $report['total'] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b><?= Yii::t('admin', 'Partners provision') ?></b>
                                        </td>
                                        <td>
                                            <?= $report['partner']->provision ?> %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b><?= Yii::t('admin', 'Total') ?></b>
                                        </td>
                                        <td>
                                            <?= $report['provision'] ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_panel-body well-lg">

                            <?=
                            backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                                'title' => Yii::t('admin', 'Requests'),
                                'showSearch' => false,
                                'smallTitle' => true,
                                'buttons' => []
                            ]);
                            ?>


                            <?=
                            GridView::widget([
                                'dataProvider' => $report['dataProvider'],
                                'summary' => false,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        'attribute' => 'event_id',
                                        'format' => 'raw',
                                        'value' => function($model)
                                        {
                                            return Html::a($model->getEventName(), ["/event/view", "id" => $model->event_id], ["target" => "_blank"]);
                                        }
                                            ],
                                            [
                                                'attribute' => 'user_id',
                                                'format' => 'raw',
                                                'value' => function($model)
                                                {
                                                    return Html::a($model->getHostessFullName(), ["/hostess/view", "id" => $model->user_id], ["target" => "_blank"]);
                                                }
                                                    ],
                                                    [
                                                        'attribute' => 'requested_user_id',
                                                        'format' => 'raw',
                                                        'value' => function($model)
                                                        {
                                                            return Html::a($model->getExhibitorFullName(), ["/exhibitor/view", "id" => $model->requested_user_id], ["target" => "_blank"]);
                                                        }
                                                            ],
                                                            'number_of_days',
                                                            'booking_price_per_day',
                                                            'total_booking_price',
                                                            [
                                                                'class' => 'yii\grid\ActionColumn',
                                                                'template' => '{view}',
                                                                'buttons' => [
                                                                    'view' => function ($_, $model)
                                                                    {
                                                                        return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), ['request/view', 'id' => $model->id], [
                                                                                    'target' => '_blank',
                                                                                    'title' => Yii::t('admin', 'View'),
                                                                                    'class' => "btn btn-default action-btn btn-xs"
                                                                        ]);
                                                                    }
                                                                        ],
                                                                        'contentOptions' => [
                                                                            'style' => "text-align: center;"
                                                                        ]
                                                                    ],
                                                                ]
                                                            ]);
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endif ?>

    </div>
</div>
