<?php

/* @var $this yii\web\View */
/* @var $model backend\models\affiliate\AffiliateForm */

$this->title = $title;
?>
<div class="affiliate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
