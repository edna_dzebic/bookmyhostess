<?php
/* @var $this yii\web\View */
/* @var $model backend\models\affiliate\AffiliateView */

$this->title = $model->name;
?>
<div class="affiliate-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?=
        $this->render('view/details', [
            'model' => $model
        ])
        ?>

        <?=
        $this->render('view/requests', [
            'model' => $model
        ])
        ?>
        
        <?=
        $this->render('view/bookings', [
            'model' => $model
        ])
        ?>
    </div>
</div>
