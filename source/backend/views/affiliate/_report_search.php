<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];


/* @var $this yii\web\View */
/* @var $model backend\models\affiliate\AffiliateReport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="affiliate-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['report'],
                'method' => 'post',
    ]);
    ?>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'affiliate_id')->widget(Select2::classname(), [
                'data' => $model->getListForDropdown(),
                'options' => ['placeholder' => Yii::t("admin", "Select affiliate")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_from')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_to')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'include_option')->widget(Select2::classname(), [
                'data' => $model->getIncludeOptionsForDropdown(),
                'options' => ['placeholder' => Yii::t("admin", "Select")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
