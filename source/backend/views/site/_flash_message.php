<?php

use Yii;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
?>
<div class="flash-message">

    <?php if (!empty(Yii::$app->getSession()->getFlash('success', ''))) : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="<?= Yii::t('admin', 'Close') ?>">
                <span aria-hidden="true">&times;</span>
            </button>
            <?= Yii::$app->getSession()->getFlash('success', '') ?>
        </div>
    <?php elseif (!empty(Yii::$app->getSession()->getFlash('error', ''))) : ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="<?= Yii::t('admin', 'Close') ?>">
                <span aria-hidden="true">&times;</span>
            </button>
            <?= Yii::$app->getSession()->getFlash('error', '') ?>
        </div>
    <?php endif; ?>

</div>
