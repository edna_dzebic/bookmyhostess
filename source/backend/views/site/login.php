<?php

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('admin', 'Sign in');
?>

<div>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'form-signin']]); ?>

                <h1><?= Yii::t('admin', 'Login') ?></h1>

                <?= $form->errorSummary($model, ['header' => '']) ?>

                <?=
                $form->field($model, 'username', ['template' => '{input}'])->input(
                        'text', [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('admin', 'Username'),
                    'required' => true
                        ]
                );
                ?>

                <?=
                $form->field($model, 'password', ['template' => '{input}'])->passwordInput(
                        [
                            'class' => 'form-control',
                            'placeholder' => Yii::t('admin', 'Password'),
                            'required' => true
                        ]
                )
                ?>

                <?=
                $form->field($model, 'rememberMe')->checkbox()
                ?>

                <button class="btn btn-default submit" type="submit">
                    <?= Yii::t('admin', 'Sign in') ?>
                </button>


                <?php $form->end() ?>

            </section>
        </div>
    </div>
</div>