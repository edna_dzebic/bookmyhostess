<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
?>

<div class="row margin-top-10">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group text-right">
            <?= Html::resetButton(Yii::t('admin', 'Reset'), ['class' => 'btn btn-default js-reset']) ?>
            <?= Html::submitButton(Yii::t('admin', 'Search'), ['class' => 'btn btn-primary js-submit']) ?>
        </div>
    </div>
</div>