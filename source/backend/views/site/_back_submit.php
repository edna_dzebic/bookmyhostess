<?php

use yii\helpers\Url;
?>
<div class="row">
    <?=
    backend\components\ButtonsHeaderWidget\ButtonsWidget::widget([
        'alignButtons' => 'left',
        'buttons' => [
            'edit_button' => [
                'class' => 'btn btn-success submit',
                'type' => 'submit',
                'url' => '/',
                'label' => Yii::t('admin', 'Save')],
            'back_button' => [
                'class' => 'btn btn-default btn-grey',
                'url' => Url::previous(),
                'label' => Yii::t('admin', 'Back')]
    ]]);
    ?>
</div>