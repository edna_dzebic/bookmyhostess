<?php

/* @var $this yii\web\View */
/* @var $model backend\models\hostesstestimonial\HostessTestimonialForm */

$this->title = $title;
?>
<div class="hostess-testimonial-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
