<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\adminUser\AdminUserForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('admin', 'Change Password ') . $model->modelObject->username;

?>

<div class="change-password-form x_panel">

    <?= backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>
    
    <?php $form = ActiveForm::begin() ?>

    <div class="user-form x_panel-body">

        <div class="row">
            <div class="col-lg-4">
                <?= $form->field($model, 'password') ?>
            </div>
        </div>
    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?= $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
