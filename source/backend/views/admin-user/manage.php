<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\adminUser\AdminUserForm */

$this->title = $title;
?>
<div class="user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
