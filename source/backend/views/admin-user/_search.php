<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\adminUser\AdminUserIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([]); ?>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'username') ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'email') ?>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'status_id')->dropDownList($model->getStatusList(), ["prompt" => Yii::t("admin", "Select status")]) ?>

        </div>
    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
