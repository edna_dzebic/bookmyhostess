<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\adminUser\AdminUserView */

$this->title = $model->username;
?>
<div class="user-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'username',
                'email:email',
                'statusLabel',
                'date_created',
                'date_updated',
                'statusLabel',
                'createdByUsername',
                'updatedByUsername',
            ],
        ])
        ?>

    </div>
</div>
