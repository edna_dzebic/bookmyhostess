<?php

use backend\widgets\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\adminUser\AdminUserIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Users');
?>
<div class="user-index x_panel">

    <?= backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
        [
            'title' => $this->title,
            'searchIsEmpty' => $hideSearch,
            'showSearch' => true,
            'buttons' => [
                'create' => [
                    'label' => Yii::t('admin', 'Add  Users')),
                    'class' => 'btn btn-success',
                    'url' => ['create']
                ]
            ]
        ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?>">

        <?= $this->render(
            '_search',
            [
                'model' => $searchModel
            ]
        );
        ?>
    </div>

    <div class="x_panel-body">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'username',
                'email:email',
                'statusLabel',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    'headerOptions' => ['class' => 'text-center'],
                    'header' => Yii::t('admin', 'Action'),
                    'buttons' => [
                        'view' => function ($_, $model)
                        {
                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), ['admin-user/view', 'id' => $model->id], [
                                'title' => Yii::t('admin', 'View'),
                                'class' => "btn btn-default action-btn btn-xs"
                            ]);
                        },
                        'update' => function ($_, $model)
                        {
                            return Html::a('<i class="glyphicon white glyphicon-pencil"></i> &nbsp;' . Yii::t('admin', 'Update'), ['admin-user/update', 'id' => $model->id], [
                                'title' => Yii::t('admin', 'Update'),
                                'class' => "btn btn-success action-btn btn-xs"
                            ]);
                        }
                    ],
                    'contentOptions' => [
                        'style' => "text-align: center;"
                    ]
                ],
            ]
        ]); ?>
    </div>
</div>
