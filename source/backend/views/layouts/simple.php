<?php

use yii\helpers\Html;

\backend\assets\SimpleAsset::register($this);
?>
<!DOCTYPE html>
<?php $this->beginPage() ?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->urlManager->createAbsoluteUrl(["/favicon.ico"]) ?>">
        <?php $this->head() ?>

    </head>
    <body class="login">
        <?php $this->beginBody() ?>
        <?= $content ?>
        <span class="clearfix">
            <?php $this->endBody() ?>
        </span>
    </body>
</html>
<?php $this->endPage() ?>

