<?php

use yii\helpers\Url;

$cnt = backend\models\Layout::getRequestedHostessCount();
?>
<div class="top_nav">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">

                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <?= Yii::$app->user->identity->username ?>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu">
                        <li>
                            <a href="<?= Url::to(['/admin-user/change-password', "id" => Yii::$app->user->id]) ?>">
                                <?= Yii::t('admin', 'Change password') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/site/logout']) ?>">
                                <?= Yii::t('admin', 'Sign out') ?>
                            </a>
                        </li>
                    </ul>
                </li>

                <?php if ($cnt > 0) : ?>
                    <li class="">
                        <a class="nav-link" href="<?= Url::to(['/hostess/requested']) ?>" role="button">
                            <span class="badge badge-danger"><?= $cnt ?> <?= Yii::t('admin', 'Requested') ?></span>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
        </nav>
    </div>
</div>