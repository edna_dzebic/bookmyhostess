<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */
use yii\helpers\Html;

\backend\assets\AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->urlManager->createAbsoluteUrl(["/favicon.ico"]) ?>">
        <?php $this->head() ?>
    </head>
    <body class="nav-md">
        <?php $this->beginBody(); ?>
        <div class="container body">

            <div class="main_container">

                <?= $this->render('_sidebar') ?>

                <!-- top navigation -->
                <?= $this->render('_navbar') ?>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <?php if (isset($this->params['h1'])): ?>
                        <div class="page-title">
                            <div class="title_left">
                                <h1><?= $this->params['h1'] ?></h1>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>

                    <?= \backend\widgets\Alert::widget() ?>

                    <?= $content ?>
                </div>
                <!-- /page content -->
            </div>
        </div>
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>
