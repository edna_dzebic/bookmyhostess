<?php

use yii\helpers\Url;
?>

<div class="main_container">

    <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

            <div class="navbar nav_title" style="border: 0;">
                <a href="/" class="site_title"><i class="fa fa-home"></i> <span><?= Yii::$app->name ?></span></a>
            </div>
            <div class="clearfix"></div>

            <!-- menu prile quick info -->
            <br>
            <!-- /menu prile quick info -->

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                <div class="menu_section">
                    <h3><?= Yii::t('admin', 'Menu') ?></h3>
                    <?=
                    \backend\widgets\Menu::widget([
                        'items' => (new \backend\models\Layout())->getMenuForUser()
                    ])
                    ?>
                </div>

            </div>
            <!-- /sidebar menu -->
            
              <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                <a data-toggle="tooltip" data-placement="top" title="<?= Yii::t('admin', 'Home') ?>" href="<?= Url::to(['/']) ?>">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="<?= Yii::t('admin', 'Settings') ?>" href="<?= Url::to(['/admin-user/change-password', 'id' => Yii::$app->user->id]) ?>">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="<?= Yii::t('admin', 'Frontend') ?>" href="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/']) ?>">
                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="<?= Yii::t('admin', 'Logout') ?>" href="<?= Url::to(['/site/logout']) ?>">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
            </div>
            <!-- /menu footer buttons -->
        </div>
    </div>
</div>