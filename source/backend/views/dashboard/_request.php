<?php ?>

<div class="row">
    <div class="col-xs-12">
        <div class="drive-view x_panel">

            <?=
            backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                'title' => Yii::t('admin', 'Requests'),
                'smallTitle' => true,
                'showSearch' => false,
                'buttons' => [
                    'update' => [
                        'class' => 'btn btn-success',
                        'url' => ['/request/index'],
                        'label' => '<i class="glyphicon white glyphicon-eye"></i> &nbsp;' . Yii::t('admin', 'Requests')
                    ],
                ]
            ]);
            ?>

            <div class="x_panel-body well-lg" id="drive-content">
                <div class="row">
                    <div class="col-xs-12  margin-top-10">
                        <div class="row">                          
                            <div class="col-xs-12">
                                <div id="test-canvas" style="width:100%;">
                                    <canvas id="request"></canvas>
                                </div>                               
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
