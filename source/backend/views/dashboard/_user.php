<?php ?>

<div class="row">
    <div class="col-xs-12">
        <div class="drive-view x_panel">


            <?=
            backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                'title' => Yii::t('admin', 'Users'),
                'smallTitle' => true,
                'showSearch' => false,
                'buttons' => [
                    'update' => [
                        'class' => 'btn btn-success',
                        'url' => ['/user'],
                        'label' => '<i class="glyphicon white glyphicon-eye"></i> &nbsp;' . Yii::t('admin', 'Users')
                    ],
                ]
            ]);
            ?>

            <div class="x_panel-body well-lg" id="user-content">
                <div class="row">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getNewCount() ?></div>
                            <h3><?= Yii::t('admin', 'Created Today') ?></h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getActiveCount() ?></div>
                            <h3><?= Yii::t('admin', 'Active') ?></h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getInactiveCount() ?></div>
                            <h3><?= Yii::t('admin', 'Inactive') ?></h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getDeletedCount() ?></div>
                            <h3><?= Yii::t('admin', 'Deleted') ?></h3>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-10">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getReferedCount() ?></div>
                            <h3><?= Yii::t('admin', 'With referal') ?></h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getNotReferedCount() ?></div>
                            <h3><?= Yii::t('admin', 'Without referal') ?></h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getOnlyDriversCount() ?></div>
                            <h3><?= Yii::t('admin', 'Only Driver') ?></h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getOnlyPassangerCount() ?></div>
                            <h3><?= Yii::t('admin', 'Only Passanger') ?></h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  margin-top-10">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4><?= Yii::t('admin', 'Users Count By Reliability') ?></h4>
                            </div>
                            <div class="col-xs-12 margin-top-10">
                                <canvas id="reliability-user-canvas"  style="width: 280px; height: 280px;"></canvas>   
                            </div>
                        </div>              
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  margin-top-10">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4><?= Yii::t('admin', 'Users Count By Month') ?></h4>
                            </div>
                            <div class="col-xs-12 margin-top-10">
                                <div id="monthly-user-canvas" style="width:100%;"></div>                               </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>