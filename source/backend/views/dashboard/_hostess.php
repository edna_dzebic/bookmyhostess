<?php
/* @var $this yii\web\View */
/* @var $model backend\models\dashboard\Hostess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="drive-view x_panel">


            <?=
            backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                'title' => Yii::t('admin', 'Hostesses'),
                'smallTitle' => true,
                'showSearch' => false,
                'buttons' => [
                    'update' => [
                        'class' => 'btn btn-success',
                        'url' => ['/hostess/index'],
                        'label' => '<i class="glyphicon white glyphicon-eye"></i> &nbsp;' . Yii::t('admin', 'All Hostess')
                    ],
                ]
            ]);
            ?>

            <div class="x_panel-body well-lg" id="user-content">
                <div class="row">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getCreatedToday() ?></div>
                            <h6><?= Yii::t('admin', 'Created Today') ?></h6>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getActiveApprovedCount() ?></div>
                            <h6><?= Yii::t('admin', 'Active Approved') ?></h6>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getActiveRequestedCount() ?></div>
                            <h6><?= Yii::t('admin', 'Active Requested') ?></h6>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getActiveNotRequestedCount() ?></div>
                            <h6><?= Yii::t('admin', 'Active Not Requested') ?></h6>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-10">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getActiveCount() ?></div>
                            <h6><?= Yii::t('admin', 'Active') ?></h6>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getPendingCount() ?></div>
                            <h6><?= Yii::t('admin', 'Pending') ?></h6>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getDisabledCount() ?></div>
                            <h6><?= Yii::t('admin', 'Disabled') ?></h6>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="count"><?= $model->getDeletedCount() ?></div>
                            <h6><?= Yii::t('admin', 'Deleted') ?></h6>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  margin-top-10">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4><?= Yii::t('admin', 'Registrations By Month') ?></h4>
                            </div>
                            <div class="col-xs-12 margin-top-10">
                                <div id="monthly-hostess-canvas" style="width:100%;"></div>                               </div>
                        </div> 
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  margin-top-10">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4><?= Yii::t('admin', 'Hostesses by Price class') ?></h4>
                            </div>
                            <div class="col-xs-12 margin-top-10">
                                <canvas id="price-class-canvas"  style="width: 280px; height: 280px;"></canvas>   
                            </div>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>