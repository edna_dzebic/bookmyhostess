<?php

/* @var $this yii\web\View */

\backend\assets\DashboardAsset::register($this);

$this->title = Yii::t('admin', 'Administration');
?>

<?= $this->render("_invoice") ?>
<?= $this->render("_request") ?>
<?= $this->render("_hostess", ["model" => $hostess]) ?>
<?= $this->render("_exhibitor", ["model" => $exhibitor]) ?>

