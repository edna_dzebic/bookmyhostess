<?php ?>

<div class="row">
    <div class="col-xs-12">
        <div class="drive-view x_panel">


            <?=
            backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                'title' => Yii::t('admin', 'Credits'),
                'smallTitle' => true,
                'showSearch' => false,
                'buttons' => [
                    'update' => [
                        'class' => 'btn btn-success',
                        'url' => ['/credit-transaction'],
                        'label' => '<i class="glyphicon white glyphicon-eye"></i> &nbsp;' . Yii::t('admin', 'Credits')
                    ],
                ]
            ]);
            ?>

            <div class="x_panel-body well-lg" id="credit-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <label>
                                    <?= Yii::t('admin', 'Total credits in system') ?>
                                </label>
                                &nbsp;
                                <?= $model->getCreditsAvailableSum() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">      
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  margin-top-10">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4><?= Yii::t('admin', 'Remove Credits Transactions') ?></h4>
                            </div>
                            <div class="col-xs-12 margin-top-10">
                                <canvas id="remove-credit-canvas"  style="width: 280px; height: 280px;"></canvas>             
                            </div>
                        </div>              
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  margin-top-10">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h4><?= Yii::t('admin', 'Add Credits Transactions') ?></h4>
                            </div>
                            <div class="col-xs-12 margin-top-10">
                                <canvas id="add-credit-canvas"  style="width: 280px; height: 280px;"></canvas>             
                            </div>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>