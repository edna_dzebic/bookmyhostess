<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\event\EventView */

$this->title = $model->name;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="event-view x_panel">

            <?=
            backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                'title' => $this->title,
                'showSearch' => false,
                'buttons' => $model->getButtons()
            ]);
            ?>

            <div class="table-responsive">
                <div class="x_panel-body well-lg">
                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'name',
                            'countryName',
                            'cityName',
                            'venue',
                            'url:url',
                            'typeName',
                            'brief_description',
                            'start_date',
                            'end_date',
                            'date_updated',
                            'date_created',
                            'statusLabel',
                            'createdByUsername',
                            'updatedByUsername',
                        ],
                    ])
                    ?>
                </div>
            </div>


        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">

                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Event jobs'),
                    'smallTitle' => true,
                    'showSearch' => false,
                    'buttons' => [
                        'create' => [
                            'label' => Yii::t('admin', 'Add new job'),
                            'class' => 'btn btn-success',
                            'url' => ['add-job', 'id' => $model->id]
                        ]
                    ]
                ]);
                ?>

                <?=
                \backend\widgets\grid\GridView::widget([
                    'dataProvider' => $model->getEventJobsDataProvider(),
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'label' => Yii::t('admin', 'Job'),
                            'attribute' => 'job.name'
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'buttons' => [
                                'delete' => function ($_, $model)
                                {
                                    return \yii\helpers\Html::a('<i class="glyphicon white glyphicon-trash"></i> &nbsp;' . Yii::t('admin', 'Delete'), ['/event/delete-job', 'event_id' => $model->event_id, 'job_id' => $model->job_id], ['class' => 'btn btn-default btn-xs']);
                                }
                                    ],
                                    'contentOptions' => [
                                        'style' => "text-align: center;"
                                    ]
                                ]
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="x_panel">
                    <div class="x_panel-body well-lg">

                        <?=
                        backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                            'title' => Yii::t('admin', 'Dress codes'),
                            'smallTitle' => true,
                            'showSearch' => false,
                            'buttons' => [
                                'create' => [
                                    'label' => Yii::t('admin', 'Add new'),
                                    'class' => 'btn btn-success',
                                    'url' => ['add-dress-code', 'id' => $model->id]
                                ]
                            ]
                        ]);
                        ?>

                        <?=
                        \backend\widgets\grid\GridView::widget([
                            'dataProvider' => $model->getDressCodesDataProvider(),
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'label' => Yii::t('admin', 'Dress Code'),
                                    'attribute' => 'eventDressCode.name'
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{delete}',
                                    'buttons' => [
                                        'delete' => function ($_, $model)
                                        {
                                            return \yii\helpers\Html::a('<i class="glyphicon white glyphicon-trash"></i> &nbsp;' . Yii::t('admin', 'Delete'), ['/event/delete-dress-code', 'event_id' => $model->event_id, 'code_id' => $model->event_dress_code_id], ['class' => 'btn btn-default btn-xs']);
                                        }
                                            ],
                                            'contentOptions' => [
                                                'style' => "text-align: center;"
                                            ]
                                        ]
                                    ]
                                ]);
                                ?>
            </div>
        </div>
    </div>
</div>

