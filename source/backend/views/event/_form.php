<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\event\EventForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="user-form x_panel-body">

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'name')->input('text') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?=
                $form->field($model, 'type_id')->widget(Select2::classname(), [
                    'data' => $model->getTypeList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select type")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'start_date')->widget(DatePicker::className(), $datePickerOptions)
                ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'end_date')->widget(DatePicker::className(), $datePickerOptions)
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'country_id')->widget(Select2::classname(), [
                    'data' => $model->getCountryList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select country")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'city_id')->widget(Select2::classname(), [
                    'data' => $model->getCityList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select city")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'venue')->input('text') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'url')->input('text') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?=
                $form->field($model, 'brief_description')->widget(\yii\redactor\widgets\Redactor::className(), [
                    'clientOptions' => [
                        'lang' => 'en',
                        'replaceDivs' => false,
                        'plugins' => [ 'fontcolor', 'table', 'fontfamily']
                    ]
                ])
                ?>
            </div>
        </div>



    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
