<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\event\EventIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-search">

    <?php
    $form = ActiveForm::begin([
    ]);
    ?>

    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'country_id')->widget(Select2::classname(), [
                'data' => $model->getCountryList(),
                'options' => ['placeholder' => Yii::t("admin", "Select country")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'city_id')->widget(Select2::classname(), [
                'data' => $model->getCityList(),
                'options' => ['placeholder' => Yii::t("admin", "Select city")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'name') ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'venue') ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'start_date')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'end_date')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
