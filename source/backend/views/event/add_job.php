<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\event\EventJobForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = $title;
?>

<div class="event-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="user-form x_panel-body">

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'event_id')->hiddenInput()->label(false) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?=
                $form->field($model, 'job_id')->widget(Select2::classname(), [
                    'data' => $model->getJobsList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select job")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
