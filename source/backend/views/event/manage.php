<?php
/* @var $this yii\web\View */
/* @var $model backend\models\event\EventForm */

$this->title = $title;
?>
<div class="event-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
