<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\event\EventIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Events');
?>
<div class="event-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => [
                    'create' => [
                        'label' => Yii::t('admin', 'Add  Events')),
                        'class' => 'btn btn-success',
                        'url' => ['create']
                    ]
                ]
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?> ">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'country_id',
                    'format' => 'raw',
                    'value' => function($model)
                    {
                        return Html::a($model->getCountryName(), ["/country/view", "id" => $model->country_id], ["target" => "_blank"]);
                    }
                        ],
                        [
                            'attribute' => 'city_id',
                            'format' => 'raw',
                            'value' => function($model)
                            {
                                return Html::a($model->getCityName(), ["/city/view", "id" => $model->city_id], ["target" => "_blank"]);
                            }
                                ],
                                'name',
                                'venue',
                                'start_date',
                                'end_date',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view}',
                                    'buttons' => [
                                        'view' => function ($_, $model)
                                        {
                                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), [ 'event/view', 'id' => $model->id], [
                                                        'title' => Yii::t('admin', 'View'),
                                                        'class' => "btn btn-default action-btn btn-xs"
                                            ]);
                                        }
                                            ],
                                            'contentOptions' => [
                                                'style' => "text-align: center;"
                                            ]
                                        ],
                                    ]
                                ]);
                                ?>
    </div>
</div>
