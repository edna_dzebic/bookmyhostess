<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\newslettersubscriber\NewsletterSubscriberIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Newsletter Subscribers');
?>
<div class="newsletter-subscriber-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => [
                    'create' => [
                        'label' => Yii::t('admin', 'Add  Newsletter Subscribers')),
                        'class' => 'btn btn-success',
                        'url' => ['create']
                    ]
                ]
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?>">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">

        <?php
        $widget = $searchModel->getWidget($this->title, $dataProvider);

        $export_menu = "<label>" . Yii::t('admin', 'Export') . "</label><ul class='export-menu-list'>" . $widget . "</ul>";
        ?>

        <?= $export_menu ?>
        <br>
        <br>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'email:email',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    'buttons' => [
                        'view' => function ($_, $model)
                        {
                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), ['newsletter-subscriber/view', 'id' => $model->id], [
                                        'title' => Yii::t('admin', 'View'),
                                        'class' => "btn btn-default action-btn btn-xs"
                            ]);
                        },
                                'update' => function ($_, $model)
                        {
                            return Html::a('<i class="glyphicon white glyphicon-pencil"></i> &nbsp;' . Yii::t('admin', 'Update'), ['newsletter-subscriber/update', 'id' => $model->id], [
                                        'title' => Yii::t('admin', 'Update'),
                                        'class' => "btn btn-success action-btn btn-xs"
                            ]);
                        }
                            ],
                            'contentOptions' => [
                                'style' => "text-align: center;"
                            ]
                        ],
                    ]
                ]);
                ?>
    </div>
</div>
