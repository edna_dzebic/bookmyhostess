<?php
/* @var $this yii\web\View */
/* @var $model backend\models\newslettersubscriber\NewsletterSubscriberForm */

$this->title = $title;
?>
<div class="newsletter-subscriber-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
