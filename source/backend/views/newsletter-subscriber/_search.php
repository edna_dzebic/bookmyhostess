<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\newslettersubscriber\NewsletterSubscriberIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-subscriber-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
    ]);
    ?>

    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'email') ?>
        </div>

    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
