<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\newslettersubscriber\NewsletterSubscriberView */

$this->title = $model->id;
?>
<div class="newsletter-subscriber-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'email:email',
                'date_created',
                'date_updated',
                'statusLabel',
                'createdByUsername',
                'updatedByUsername'
            ],
        ])
        ?>
    </div>
</div>
