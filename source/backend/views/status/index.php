<?php

use backend\widgets\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\status\StatusIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Statuses');
?>
<div class="status-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => []
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?>">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'name',
                'label'
            ]
        ]);
        ?>
    </div>
</div>
