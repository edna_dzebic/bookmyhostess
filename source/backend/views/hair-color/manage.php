<?php
/* @var $this yii\web\View */
/* @var $model backend\models\haircolor\HairColorForm */

$this->title = $title;
?>
<div class="hair-color-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
