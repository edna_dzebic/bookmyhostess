<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\city\CityIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index']
    ]);
    ?>

    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">            
            <?=
            $form->field($model, 'country_id')->widget(Select2::classname(), [
                'data' => $model->getCountryList(),
                'options' => ['placeholder' => Yii::t("admin", "Select country")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'name') ?>
        </div>

    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
