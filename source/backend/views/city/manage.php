<?php
/* @var $this yii\web\View */
/* @var $model backend\models\city\CityForm */

$this->title = $title;
?>
<div class="city-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
