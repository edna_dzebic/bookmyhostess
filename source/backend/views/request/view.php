<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\request\RequestView */

$this->title = $model->id;
?>
<div class="request-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <div class="table-responsive">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'eventName',
                    'hostessFullName',
                    'exhibitorFullName',
                    'dressCodeName',
                    'jobName',
                    'completedLabel',
                    'invoice_id',
                    'statusLabel',
                    'lastMinuteLabel',
                    'date_created',
                    'date_from',
                    'date_to',
                    'date_responded',
                    'formattedHostessPrice',
                    'paymentTypeLabel',
                    'reason',
                    'date_rejected',
                    'ex_withdraw_reason',
                    'ex_withdraw_date',
                    'hostess_withdraw_reason',
                    'hostess_withdraw_date',
                    'number_of_days',
                    'booking_price_per_day',
                    'total_booking_price',
                    'date_updated',
                    'createdByUsername',
                    'updatedByUsername',
                    'affiliateName'
                ],
            ])
            ?>
        </div>
    </div>
</div>
