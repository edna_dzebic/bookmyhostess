<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\select2\Select2;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\request\RequestForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="user-form x_panel-body">

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'event_id')->widget(Select2::classname(), [
                    'data' => $model->getEventList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select event")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'data' => $model->getHostessList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select hostess")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'requested_user_id')->widget(Select2::classname(), [
                    'data' => $model->getExhibitorList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select exhibitor")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'event_dress_code')->widget(Select2::classname(), [
                    'data' => $model->getDressCodeList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select dress code")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'date_from')->widget(DatePicker::className(), $datePickerOptions)
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'date_to')->widget(DatePicker::className(), $datePickerOptions)
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'payment_type')->widget(Select2::classname(), [
                    'data' => Yii::$app->util->getPaymentTypeForHostess(),
                    'options' => ['placeholder' => Yii::t("admin", "Select payment of the daily rate")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'number_of_days')->input('number') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'booking_price_per_day')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'total_booking_price')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
