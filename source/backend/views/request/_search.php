<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\request\RequestIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-search">

    <?php
    $form = ActiveForm::begin([
    ]);
    ?>

    <div class="row">

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'id') ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'event_id')->widget(Select2::classname(), [
                'data' => $model->getEventList(),
                'options' => ['placeholder' => Yii::t("admin", "Select event")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'user_id')->widget(Select2::classname(), [
                'data' => $model->getHostessList(),
                'options' => ['placeholder' => Yii::t("admin", "Select hostess")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'requested_user_id')->widget(Select2::classname(), [
                'data' => $model->getExhibitorList(),
                'options' => ['placeholder' => Yii::t("admin", "Select exhibitor")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'status_id')->widget(Select2::classname(), [
                'data' => $model->getStatusList(),
                'options' => ['placeholder' => Yii::t("admin", "Select status")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'completed')->widget(Select2::classname(), [
                'data' => $model->getCompletedList(),
                'options' => ['placeholder' => Yii::t("admin", "Select completed")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_from')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_to')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'price') ?>
        </div>
        
        
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'is_last_minute')->widget(Select2::classname(), [
                'data' => $model->getLastMinuteList(),
                'options' => ['placeholder' => Yii::t("admin", "Search by last minute")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'payment_type')->widget(Select2::classname(), [
                'data' => $model->getPaymentTypeList(),
                'options' => ['placeholder' => Yii::t("admin", "Select payment of the daily rate")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>        
    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
