<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\request\RequestIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Requests');
?>
<div class="request-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => []
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?>">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">
        
        <?php
        $widget = $searchModel->getWidget($this->title, $dataProvider);

        $export_menu = "<label>" . Yii::t('admin', 'Export') . "</label><ul class='export-menu-list'>" . $widget . "</ul>";
        ?>

        <?= $export_menu ?>
        <br>
        <br>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'event_id',
                    'format' => 'raw',
                    'value' => function($model)
                    {
                        return Html::a($model->getEventName(), ["/event/view", "id" => $model->event_id], ["target" => "_blank"]);
                    }
                        ],
                        [
                            'attribute' => 'user_id',
                            'format' => 'raw',
                            'value' => function($model)
                            {
                                return Html::a($model->getHostessFullName(), ["/hostess/view", "id" => $model->user_id], ["target" => "_blank"]);
                            }
                                ],
                                [
                                    'attribute' => 'requested_user_id',
                                    'format' => 'raw',
                                    'value' => function($model)
                                    {
                                        return Html::a($model->getExhibitorFullName(), ["/exhibitor/view", "id" => $model->requested_user_id], ["target" => "_blank"]);
                                    }
                                        ],
                                        [
                                            'attribute' => 'status_id',
                                            'value' => function($model)
                                            {
                                                return $model->getStatusLabel();
                                            }
                                        ],
                                        [
                                            'attribute' => 'completed',
                                            'value' => function($model)
                                            {
                                                return $model->getCompletedLabel();
                                            }
                                        ],
                                        'date_from',
                                        'date_to',
                                        'date_created',
                                        [
                                            'attribute' => 'date_responded',
                                            'value' => function($model)
                                            {
                                                if ($model->getIsResponded())
                                                {
                                                    return $model->date_responded;
                                                }
                                                return '';
                                            }
                                        ],
                                        'price',
                                        [
                                            'attribute' => 'payment_type',
                                            'value' => function($model)
                                            {
                                                return $model->getPaymentTypeLabel();
                                            }
                                        ],
                                        [
                                            'attribute' => 'is_last_minute',
                                            'value' => function($model)
                                            {
                                                return $model->getLastMinuteLabel();
                                            }
                                        ],
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '{view}',
                                            'buttons' => [
                                                'view' => function ($_, $model)
                                                {
                                                    return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), ['request/view', 'id' => $model->id], [
                                                                'title' => Yii::t('admin', 'View'),
                                                                'class' => "btn btn-default action-btn btn-xs"
                                                    ]);
                                                }
                                                    ],
                                                    'contentOptions' => [
                                                        'style' => "text-align: center;"
                                                    ]
                                                ],
                                            ]
                                        ]);
                                        ?>
    </div>
</div>
