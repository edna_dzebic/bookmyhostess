<br>
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'manual_invoice_count')->input('number'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'invoice_offset')->input('number'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'booking_price')->input('number'); ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'last_minute_days')->input('number'); ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'last_minute_percentage')->input('number'); ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <p class="text-muted">
            <?= Yii::t('admin', 'Formula for new inovice number is: $newNumber = $offset + $manualCount + $countThisYear + 1') ?>
            <br>
            <?= Yii::t('admin', 'Where values are :') ?>
            <br>
            <?= Yii::t('admin', '<b>$offset</b> : number of skipped numbers at the beginning of the new year. You can change it manually in settings. For the 2017 Adela decided to skip 50 numbers so that invoice numbers start from 51/2017') ?>
            <br>
            <?= Yii::t('admin', '<b>$manualCount</b> : number of manually issued invoices. Make sure to change this number before you issue new manual invoice!') ?>
            <br>
            <?= Yii::t('admin', '<b>$countThisYear</b> : number of automatically issued invoices this year.') ?>             
        </p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <p>       
            <label class="label label-danger">
                <?= Yii::t('admin', 'Current number of invoices this year is') ?> : <?= $countInvoice ?>
            </label>            
        </p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <p>       
            <label class="label label-danger">
                <?= Yii::t('admin', 'Next invoice number is') ?> : 
                <?=
                Yii::$app->settings->invoice_offset + $countInvoice + Yii::$app->settings->manual_invoice_count + 1
                ?>
            </label>            
        </p>
    </div>
</div>