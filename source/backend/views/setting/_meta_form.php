<br>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'meta_keywords')->textarea(["rows" => 6]); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'meta_description')->textarea(["rows" => 6]); ?>
    </div>
</div>