<br>
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'include_google_analytics')->checkbox(['uncheck' => 0, 'check' => 1]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'include_zopim')->checkbox(['uncheck' => 0, 'check' => 1]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'inlude_hotjar')->checkbox(['uncheck' => 0, 'check' => 1]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12  col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'google_analytics_tracking_id')->input('text'); ?>
    </div>
</div>