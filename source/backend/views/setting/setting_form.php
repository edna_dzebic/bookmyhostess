<?php

use yii\bootstrap\Tabs;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Setting */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('admin', 'Manage Settings');
?>
<div class="x_panel">

    <div class="panel-heading">
        <h1><?= $this->title ?></h1>
    </div>

    <div class="x_panel-body">
        <?php
        $form = ActiveForm::begin([
                    'id' => 'account-setting-form',
                    'options' => ['enctype' => 'multipart/form-data']]);
        ?>

        <?= $form->errorSummary($settings); ?>


        <?=
        Tabs::widget([
            'navType' => 'nav-tabs bar_tabs',
            'items' => [
                [
                    'label' => Yii::t('admin', 'Invoice Settings'),
                    'content' => $this->render('_invoice_form', [
                        'form' => $form,
                        'settings' => $settings,
                        'countInvoice' => $countInvoice
                    ])
                ],
                [
                    'label' => Yii::t('admin', 'Emails'),
                    'content' => $this->render('_emails_form', [
                        'form' => $form,
                        'settings' => $settings
                    ])
                ],
                [
                    'label' => Yii::t('admin', 'Plugins Settings'),
                    'content' => $this->render('_plugins_form', [
                        'form' => $form,
                        'settings' => $settings
                    ])
                ],
                [
                    'label' => Yii::t('admin', 'Apps Settings'),
                    'content' => $this->render('_apps_form', [
                        'form' => $form,
                        'settings' => $settings
                    ])
                ],
                [
                    'label' => Yii::t('admin', 'Meta Settings'),
                    'content' => $this->render('_meta_form', [
                        'form' => $form,
                        'settings' => $settings
                    ])
                ],
                [
                    'label' => Yii::t('admin', 'General Settings'),
                    'content' => $this->render('_general_form', [
                        'form' => $form,
                        'settings' => $settings
                    ])
                ]
            ]
        ])
        ?>


    </div>


    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>