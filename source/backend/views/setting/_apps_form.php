<br>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'send_firebase_notification')->checkbox(['uncheck' => 0, 'check' => 1]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'google_play')->input('text'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'app_store')->input('text'); ?>
    </div>
</div>