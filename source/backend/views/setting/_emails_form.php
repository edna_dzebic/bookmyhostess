<br>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'email_from')->input('text'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'admin_email')->input('text'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'support_email')->input('text'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'finance_email')->input('text'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'contact_email')->input('text'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <?= $form->field($settings, 'developer_email')->input('text'); ?>
    </div>
</div>  