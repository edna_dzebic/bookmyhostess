<?php
/* @var $this yii\web\View */
/* @var $model \common\models\Invoice */
?>

<?= $this->render('parts/_header', ["model" => $model]) ?>

<?= $this->render('parts/_separator') ?>

<?= $this->render('parts/_title', ["model" => $model]) ?>

<?= $this->render('parts/_separator') ?>

<?= $this->render('parts/_greeting', ["model" => $model]) ?>

<table width='100%' style=" width: 100%;">
    <tr>
        <td align="center" style='alignment-baseline: middle;'>
            <div style=" padding:0px; width:100%; font-size:8pt;"><br/>

                <table cellspacing="0" style='border:1px solid black; min-width: 100%; width: 100%;'>
                    <?= $this->render('parts/_table_header', ["model" => $model]) ?>
                    <?= $this->render('parts/_items', ["model" => $model]) ?>
                    <?= $this->render('parts/_totals', ["model" => $model]) ?>
                </table>
            </div>
        </td>
    </tr>
</table>

<?= $this->render('parts/_separator') ?>

<?= $this->render('parts/_footer', ["model" => $model]) ?>