<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\invoice\InvoiceIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Invoices');

\backend\assets\InvoiceAsset::register($this);
?>
<div class="invoice-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => []
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?>">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'attribute' => 'requested_user_id',
                    'format' => 'raw',
                    'value' => function($model)
                    {
                        return Html::a($model->getExhibitorFullName(), ["/exhibitor/view", "id" => $model->requested_user_id], ["target" => "_blank"]);
                    }
                        ],
                        'amount',
                        'date_created',
                        [
                            'attribute' => 'real_invoice_id',
                            'value' => function($model)
                            {
                                return $model->getRealIdText();
                            }
                        ],
                        'coupon_code',
                        [
                            'attribute' => 'payment_type',
                            'value' => function($model)
                            {
                                return $model->getPaymentTypeLabel();
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function($model)
                            {
                                return $model->getPaymentStatusLabel();
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($_, $model)
                                {
                                    return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), ['invoice/view', 'id' => $model->id], [
                                                'title' => Yii::t('admin', 'View'),
                                                'class' => "btn btn-default action-btn btn-xs"
                                    ]);
                                }
                                    ],
                                    'contentOptions' => [
                                        'style' => "text-align: center;"
                                    ]
                                ],
                            ]
                        ]);
                        ?>
    </div>
</div>
