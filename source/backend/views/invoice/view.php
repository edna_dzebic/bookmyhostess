<?php
/* @var $this yii\web\View */
/* @var $model backend\models\invoice\InvoiceView */

$this->title = $model->id;
?>
<div class="invoice-view x_panel">

    <div class="x_panel-body well-lg">

        <?=
        backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
            'title' => Yii::t('admin', 'Invoice details'),
            'showSearch' => false,
            'buttons' => $model->getButtons()
        ]);
        ?>

        <?=
        $this->render('view/details', [
            'model' => $model
        ])
        ?>

        <?=
        $this->render('view/requests', [
            'model' => $model
        ])
        ?>
        
        <?=
        $this->render('view/reminders', [
            'model' => $model
        ])
        ?>

    </div>
</div>
