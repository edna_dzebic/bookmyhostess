<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\Html;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\invoice\InvoiceIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoice-search">

    <?php
    $form = ActiveForm::begin([
                "id" => "invoice-form",
                'action' => ['index']
    ]);
    ?>

    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'requested_user_id')->widget(Select2::classname(), [
                'data' => $model->getExhibitorList(),
                'options' => ['placeholder' => Yii::t("admin", "Select exhibitor")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        
        

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'real_invoice_id')->textInput() ?>
        </div>
        
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'id')->textInput() ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'amount') ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_created_from')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_created_to')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'coupon_id')->widget(Select2::classname(), [
                'data' => $model->getCouponList(),
                'options' => ['placeholder' => Yii::t("admin", "Select coupon")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>

        </div>
        
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
             <?=
            $form->field($model, 'payment_type')->widget(Select2::classname(), [
                'data' => $model->getPaymentTypeList(),
                'options' => ['placeholder' => Yii::t("admin", "Select payment type")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
             <?=
            $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => $model->getPaymentStatusList(),
                'options' => ['placeholder' => Yii::t("admin", "Select payment status")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row margin-top-10">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group text-right">
                <?= Html::resetButton(Yii::t('admin', 'Reset'), ['class' => 'btn btn-default js-reset']) ?>
                <?=
                Html::submitButton(Yii::t('admin', 'Search'), [
                    'name' => "action",
                    'class' => 'btn btn-primary'
                ])
                ?>

                <?=
                Html::button(Yii::t('admin', 'Download ZIP'), [
                    'name' => "action",
                    'class' => 'btn btn-success js-download-zip'
                ])
                ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
