<?php

use yii\helpers\Html;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Requests'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => []
                ]);
                ?>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>#</td>                                                           
                                <td><?= Yii::t('admin', 'Username') ?></td>                      
                                <td><?= Yii::t('admin', 'Name') ?></td>                      
                                <td><?= Yii::t('admin', 'Email') ?></td>                      
                                <td><?= Yii::t('admin', 'Phone') ?></td> 
                                <td><?= Yii::t('admin', 'Event') ?></td>
                                <td><?= Yii::t('admin', 'Price /<br>Day') ?></td>
                                <td><?= Yii::t('admin', 'Payment of the daily rate') ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            <?php foreach ($model->modelObject->eventRequests as $request) : ?>
                                <tr>           
                                    <td>
                                        <?= ++$i ?>
                                    </td>
                                    <td >
                                        <?=
                                        Html::a($request->user->username, ["/hostess/view", "id" => $request->user->id], ["target" => "_blank"])
                                        ?>                                       
                                    </td>
                                    <td>
                                        <?= $request->user->fullName; ?>
                                    </td>                           
                                    <td>
                                        <?= $request->user->email; ?>
                                    </td> 
                                    <td> 
                                        <?= $request->user->phone_number; ?>
                                    </td> 
                                    <td> 
                                        <?= $request->event->name; ?>
                                    </td> 
                                    <td>
                                        <?= Yii::$app->util->formatEventPrice($request->event, $request->price); ?>
                                    </td>
                                    <td>
                                        <?= $request->getPaymentTypeLabel() ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>