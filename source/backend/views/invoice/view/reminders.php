<?php

use yii\helpers\Html;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <?=
                backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
                    'title' => Yii::t('admin', 'Reminders'),
                    'showSearch' => false,
                    'smallTitle' => true,
                    'buttons' => []
                ]);
                ?>

                <div class="table-responsive">

                    <?php if (count($model->modelObject->invoiceReminders)) : ?>

                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td><?= Yii::t('admin', 'Reminder') ?></td>
                                    <td><?= Yii::t('admin', 'Created At') ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($model->modelObject->invoiceReminders as $reminder): ?>
                                    <tr>
                                        <td>
                                            <?= $reminder->flag ?>
                                        </td>
                                        <td>
                                            <?= Yii::$app->util->formatDateTime($reminder->date_created) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <div>
                            <p class="text-danger">
                                <?= Yii::t('admin', 'No reminders sent yet!') ?>
                            </p>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
</div>

