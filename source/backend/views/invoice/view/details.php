<?php

use yii\widgets\DetailView;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_panel-body well-lg">
                <div class="table-responsive">
                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'exhibitorFullName',
                            'amount',
                            'date_created',
                            'date_paid',
                            'realNumberLabel',
                            'paypal_transaction_id',
                            'language',
                            'isExhibitorInGermanyLabel',
                            'invoiceTypeLabel',
                            'exhibitor_company_name',
                            'exhibitor_street',
                            'exhibitor_postal_code',
                            'exhibitor_city',
                            'exhibitor_country',
                            'exhibitor_tax_number',
                            'net_amount',
                            'tax_amount',
                            'gross_amount',
                            'total_price_with_discount',
                            'coupon_id',
                            'coupon_code',
                            'coupon_amount',
                            'couponTypeLabel',
                            'coupon_netto_amount',
                            'coupon_tax_amount',
                            'coupon_gross_amount',
                            'paymentTypeLabel',
                            'paymentStatusLabel',
                            'isReminderAllowedLabel'
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>