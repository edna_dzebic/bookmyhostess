<?php if (isset($model->coupon_code)) : ?>


    <tr>
        <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
            <b><?=
                Yii::t('app.exhibitor', 'Discount for coupon with code {couponCode} - {couponNettoAmount} EUR', [
                    "couponCode" => $model->coupon_code,
                    "couponNettoAmount" => Yii::$app->util->formatPrice($model->coupon_netto_amount)
                ])
                ?>
            </b>
        </td>     
        <td style='height:30px;  padding:4px; border:1px solid black;' align="right">

        </td>
    </tr>

    <tr>
        <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
            <b><?=
                Yii::t('app.exhibitor', 'Coupon 19% VAT - {couponTaxAmount} EUR', [
                    "couponTaxAmount" => Yii::$app->util->formatPrice($model->coupon_tax_amount)
                ])
                ?>
            </b>
        </td>     
        <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
            <b>- <?= Yii::$app->util->formatPrice($model->coupon_gross_amount) ?></b>

        </td>
    </tr>
    <tr>
        <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
            <b><?= Yii::t('app.exhibitor', 'Total amount with discount') ?></b>
        </td>     
        <td style='height:30px;  padding:4px; border:1px solid black;' align="right">

            <b><?= Yii::$app->util->formatPrice($model->total_price_with_discount) ?></b>
        </td>
    </tr>
<?php endif; ?>