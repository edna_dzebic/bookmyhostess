<?php if (is_object($model->coupon)) : ?>


    <tr>
        <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
            <b><?=
                Yii::t('app.exhibitor', 'Discount for coupon with code {couponCode} - {couponNettoAmount} EUR', [
                    "couponCode" => $model->coupon->code,
                    "couponNettoAmount" => Yii::$app->util->formatPrice($model->getCouponNettoAmount())
                ])
                ?>
            </b>
        </td>     
        <td style='height:30px;  padding:4px; border:1px solid black;' align="right">

        </td>
    </tr>

    <tr>
        <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
            <b><?=
                Yii::t('app.exhibitor', 'Coupon 19% VAT - {couponTaxAmount} EUR', [
                    "couponTaxAmount" => Yii::$app->util->formatPrice($model->getCouponTaxAmount($model->getCouponGrossAmount()))
                ])
                ?>
            </b>
        </td>     
        <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
            <b>- <?= Yii::$app->util->formatPrice($model->getCouponGrossAmount()) ?></b>

        </td>
    </tr>
    <tr>
        <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
            <b><?= Yii::t('app.exhibitor', 'Total amount with discount') ?></b>
        </td>     
        <td style='height:30px;  padding:4px; border:1px solid black;' align="right">

            <b><?= Yii::$app->util->formatPrice($model->getPayPalAmount()) ?></b>
        </td>
    </tr>
<?php endif; ?>