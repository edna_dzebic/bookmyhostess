<table style="width: 100%; font-size:10pt;">
    <tr>
        <td style="width: 70%"></td>
        <td style="width: 30%">
            <table style="width: 100%">
                <tr>
                    <td>
                        <img src='<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(["images/pdf-logo.png"]) ?>' width="262px" height="91px">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 70%;">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <span  style="color:#0caa9d;">BOOKmyHOSTESS</span>  |  Ottostr 7. | 50859 <?= Yii::t('app', 'Cologne') ?> - <?= Yii::t('app', 'Germany') ?>
                                </td>                               
                            </tr>
                        </table>                        
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 30%;color:#0caa9d;padding-left: 20px;">
            BOOKmyHOSTESS.com
        </td>
    </tr>
    <tr>
        <td style="width: 70%">
            <table style="width: 100%;font-size:10pt;">
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td>
                        <b><?= $model->userRequested->exhibitorProfile->company_name ?></b></td>
                </tr>
                <tr>
                    <td>
                        <?= $model->userRequested->exhibitorProfile->street ?> &nbsp;  <?= $model->userRequested->exhibitorProfile->home_number ?> 
                    </td>
                </tr> 
                <tr>
                    <td>
                        <?= $model->userRequested->exhibitorProfile->postal_code ?> &nbsp;  <?= $model->userRequested->exhibitorProfile->city ?> 
                    </td>
                </tr>
                <tr>
                    <td><?= isset($model->userRequested->exhibitorProfile->country) ? $model->userRequested->exhibitorProfile->country->getTranslatedName() : ""; ?></td>
                </tr>  
                <tr>
                    <td>
                        <br><b><?= Yii::t('app.exhibitor', 'Tax number') ?></b> :&nbsp;<?= $model->userRequested->exhibitorProfile->tax_number ?>
                    </td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>
                <tr>
                    <td height='20'></td>
                </tr>

                <?php if ($model->getIsNormalInvoice()) : ?>

                    <tr>
                        <td>
                            <?= Yii::t('app.exhibitor', 'Invoice Date:') ?>&nbsp;<?= Yii::$app->util->formatDate($model->date_created) ?> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= Yii::t('app.exhibitor', 'Invoice Number:') ?>&nbsp;<?= $model->getRealIdText(); ?>
                        </td>
                    </tr>   

                <?php else: ?>

                    <tr>
                        <td>
                            <?= Yii::t('app.exhibitor', 'Date:') ?>&nbsp;<?= Yii::$app->util->formatDate($model->date_created) ?> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= Yii::t('app.exhibitor', 'Number:') ?>&nbsp;<?= $model->getRealIdText(); ?>
                        </td>
                    </tr> 
                <?php endif; ?>
            </table>
        </td>
        <td style="width: 30%">
            <table style="width: 100%;font-size:12px;font-weight: bold;padding-left: 20px;">
                <tr>
                    <td> 
                        <?= Yii::t('app.exhibitor', 'Owner') ?>
                        <br>
                        Adela Kadiric
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        Ottostr 7.
                        <br>
                        50859 <?= Yii::t('app', 'Cologne') ?> - <?= Yii::t('app', 'Germany') ?>
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        Tel.:  +49 (0) 2234 4301397
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        a.kadiric@bookmyhostess.com
                        <br>
                        www.BOOKmyHOSTESS.com
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        <?= Yii::t('app.exhibitor', 'Local tax office') ?> : Köln West
                        <br>
                        <?= Yii::t('app.exhibitor', 'Head office') ?> : Köln
                        <br>
                        <?= Yii::t('app.exhibitor', 'Tax number') ?> : DE 299791123
                    </td>
                </tr>
                <tr>
                    <td height='10'></td>
                </tr>
                <tr>
                    <td> 
                        N26
                        <br>
                        IBAN:
                        <br>
                        DE08 1001 1001 2624 4467 05
                        <br>
                        BIC/SWIFT:
                        <br>
                        NTSBDEB1XXX
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>