<?php if ($model->getIsNormalInvoice()) : ?>

    <?php
    // if from Germany
    if (isset($model->userRequested->exhibitorProfile->country) &&
            Yii::$app->util->isGerman($model->userRequested->exhibitorProfile->country->id)):
        ?>

        <tr>
            <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                <b><?= Yii::t('app.exhibitor', 'Net amount') ?></b>
            </td>  
            <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                <?= Yii::$app->util->formatPrice($model->net_amount) ?>
            </td>
        </tr>
        <tr>
            <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                <b><?= Yii::t('app.exhibitor', '+19% value added tax') ?></b>
            </td>   
            <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                <?= Yii::$app->util->formatPrice($model->tax_amount) ?>
            </td>
        </tr>
        
        <tr>
            <td style='height:30px;  padding:4px; border:1px solid black;' colspan='7' align='left'>
                <b><?= Yii::t('app.exhibitor', 'Gross amount') ?></b>
            </td>     
            <td style='height:30px;  padding:4px; border:1px solid black;' align="right">
                <b><?= Yii::$app->util->formatPrice($model->gross_amount) ?></b>
            </td>
        </tr>

        <?= $this->render('_reverse_coupon_germany', ["model" => $model]) ?>

    <?php else: ?>

        <?= $this->render('_reverse_gross_total', ["model" => $model]) ?>

        <?= $this->render('_reverse_coupon_other', ["model" => $model]) ?>

    <?php endif; ?>

<?php else: ?>

    <?= $this->render('_reverse_receipt_total', ["model" => $model]) ?>

<?php endif; ?>