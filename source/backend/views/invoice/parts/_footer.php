<table style=" text-align:justify; ">
    <?php if ($model->getIsNormalInvoice()) : ?>
        <?php
        // if exhibitor is not German
        if (
                isset($model->userRequested->exhibitorProfile->country) &&
                Yii::$app->util->isGerman($model->userRequested->exhibitorProfile->country->id) == false
        ):
            ?>

            <?php
            // if is EU invoice type
            if ($model->userRequested->exhibitorProfile->country->getIsEuInvoice()):
                ?>

                <tr>
                    <td align="left" height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Yii::t('app.exhibitor', 'Reverse Charge - According to Article 194, 196 of Council Directive 2006/112/EEC, VAT liability rests with the service recipient {tax_id}.', ["tax_id" => $model->userRequested->exhibitorProfile->tax_number]) ?>
                    </td>
                </tr>

                <?php
            // if third country - not Germany and not EU
            else :
                ?>
                <tr>
                    <td align="left" height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Yii::t('app.exhibitor', 'Not taxable in Germany.') ?>
                    </td>
                </tr>
            <?php endif; ?>
        <?php endif; ?>


        <?php if (!empty($model->paypal_transaction_id)): ?>
            <tr>
                <td>
                    <?= Yii::t('app.exhibitor', 'According to our General Terms and Conditions, payment is due immediately. The amount will be automatically collected from Your account. The PayPal transaction ID is: {paypalTransactionID}.', ["paypalTransactionID" => $model->paypal_transaction_id]) ?>
                </td> 
            </tr>
        <?php endif; ?>

        <tr>
            <td align="left" height="20">
            </td>
        </tr>
        <tr>
            <td>
                <?= Yii::t('app.exhibitor', 'The invoice was issued electronically and is therefore valid without signature.') ?>
            </td>
        </tr>

    <?php endif; ?>

    <tr>
        <td align="left" height="20">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.exhibitor', 'If you have any questions or comments, please contact us at +4922344301397.') ?>
        </td>
    </tr>
    <tr>
        <td align="left" height="10">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.exhibitor', 'Thank You for using BOOKmyHOSTESS.com! We are looking forward to serving You in the near future.') ?>
        </td>
    </tr>
    <tr>
        <td align="left" height="10">
        </td>
    </tr>
    <tr>
        <td>
            <?= Yii::t('app.exhibitor', 'Best Regards, <br> Your BOOKmyHOSTESS.com - Team.') ?>
        </td>
    </tr>
</table>