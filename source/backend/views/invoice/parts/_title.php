<table align = "center">
    <?php if ($model->getIsNormalInvoice()) : ?>
        <tr>
            <td style="text-align: center;">
                <b style="text-align:center"><?= Yii::t('app.exhibitor', 'INVOICE') ?></b>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td style="text-align: center;">
                <b style="text-align:center"><?= Yii::t('app.exhibitor', 'RECEIPT') ?></b>
            </td>
        </tr>
    <?php endif; ?>
</table>