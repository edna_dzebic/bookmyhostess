<?php
$i = 0;

foreach ($model->invoiceItems as $request)
{
    ?>
    <tr style='height:68px'>
        <td style='height:68px; padding: 4px; border:1px solid black;' valign='top'
            align='center'> <?= ++$i ?>
        </td>

        <?php if ($request->is_last_minute && $request->amount > 0): ?>
            <td style='height:68px; padding: 4px; border:1px solid black;'  valign='top'
                align='center'><?= Yii::t('app.exhibitor', 'Online booking of staff, <br> including additional fee for late bookings.') ?>
            </td>
        <?php else: ?>
            <td style='height:68px; padding: 4px; border:1px solid black;'  valign='top'
                align='center'><?= Yii::t('app.exhibitor', 'Online booking of temporary <br/>trade fair staff') ?>
            </td>
        <?php endif; ?>
        <td style='height:68px; padding: 4px; border:1px solid black;'valign='top'
            align='center'><?= $request->event_name; ?>
        </td>
        <td style='height:68px; padding: 4px; border:1px solid black;' valign='top'
            align='center'><?= Yii::$app->util->formatDate($request->from_date); ?>
            <br>
            - 
            <br>
            <?= Yii::$app->util->formatDate($request->to_date); ?>
        </td>
        <td style='height:68px; padding: 4px; border:1px solid black;'  valign='top'
            align='center'><?= $request->number_of_days; ?>
        </td>
        <td style='height:68px; padding: 4px; border:1px solid black;'valign='top'
            align='center'><?= $request->hostess_name; ?>
        </td>                           
        <td style='height:68px; padding: 4px; border:1px solid black;' valign='top'
            align='center'><?= Yii::$app->util->formatPrice($request->booking_price_per_day); ?>
        </td>
        <td style='height:68px; padding: 4px; border:1px solid black;'  valign='top'
            align='right'><?= Yii::$app->util->formatPrice($request->amount) ?>
        </td>
    </tr>
<?php } ?>