<?php
/* @var $this yii\web\View */
/* @var $model backend\models\type\TypeForm */

$this->title = $title;
?>
<div class="type-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
