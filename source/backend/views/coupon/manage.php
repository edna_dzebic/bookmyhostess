<?php
/* @var $this yii\web\View */
/* @var $model backend\models\coupon\CouponForm */

$this->title = $title;
?>
<div class="coupon-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
