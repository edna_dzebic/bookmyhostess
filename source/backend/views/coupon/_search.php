<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\coupon\CouponIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coupon-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
    ]);
    ?>

    <div class="row">

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <?= $form->field($model, 'code') ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <?= $form->field($model, 'amount') ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'coupon_type')->widget(Select2::classname(), [
                'data' => $model->getTypeDropdown(),
                'options' => ['placeholder' => Yii::t("admin", "Select type")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <?= $form->field($model, 'title') ?>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_from')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_to')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
