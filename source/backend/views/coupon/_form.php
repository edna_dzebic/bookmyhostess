<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\select2\Select2;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\coupon\CouponForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coupon-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="user-form x_panel-body">

        <div class="row">
            <div class="col-lg-12">
                <?= $form->field($model, 'bar_code')->hiddenInput()->label(false) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'date_from')->widget(DatePicker::className(), $datePickerOptions)
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'date_to')->widget(DatePicker::className(), $datePickerOptions)
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'coupon_type')->widget(Select2::classname(), [
                    'data' => $model->getTypeDropdown(),
                    'options' => ['placeholder' => Yii::t("admin", "Select type")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
