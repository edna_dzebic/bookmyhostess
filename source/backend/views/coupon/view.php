<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\coupon\CouponView */

$this->title = $model->title;
?>
<div class="coupon-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'code',
                'date_from',
                'date_to',
                'amount',
                'typeLabel',
                'title',
                'bar_code',
                'date_created',
                'date_updated',
                'statusLabel',
                'createdByUsername',
                'updatedByUsername',
            ],
        ])
        ?>
    </div>
</div>
