<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\languagelevel\LanguageLevelView */

$this->title = $model->name;
?>
<div class="language-level-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'date_created',
                'date_updated',
                'statusLabel',
                'createdByUsername',
                'updatedByUsername',
            ],
        ])
        ?>
    </div>
</div>
