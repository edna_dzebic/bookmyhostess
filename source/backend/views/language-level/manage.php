<?php
/* @var $this yii\web\View */
/* @var $model backend\models\languagelevel\LanguageLevelForm */

$this->title = $title;
?>
<div class="language-level-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
