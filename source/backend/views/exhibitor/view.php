<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\exhibitor\ExhibitorView */

$this->title = $model->modelObject->fullName;
?>
<div class="exhibitor-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <div class="table-responsive">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'user_id',
                    'first_name',
                    'last_name',
                    'email',
                    'phone_number',
                    'company_name',
                    'tax_number',
                    'street',
                    'home_number',
                    'postal_code',
                    'city',
                    'countryName',
                    'website',
                    'staffDemandTypeLabel',
                    'bookingTypeLabel',
                    'booking_price_discount',
                    'isInvoicePaymentAllowed',
                    'date_updated',
                    'date_created',
                    'statusLabel',
                    'createdByUsername',
                    'updatedByUsername',
                    'lang'
                ],
            ])
            ?>
        </div>
    </div>
</div>
