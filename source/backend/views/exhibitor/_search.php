<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];

/* @var $this yii\web\View */
/* @var $model backend\models\exhibitor\ExhibitorIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-search">

    <?php
    $form = ActiveForm::begin([
    ]);
    ?>

    <div class="row">

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'id') ?>
        </div>
         <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'gender')->widget(Select2::classname(), [
                'data' => $model->getGenderList(),
                'options' => ['placeholder' => Yii::t("admin", "Select gender")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'first_name') ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'last_name') ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'email') ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'company') ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'tax_number') ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?= $form->field($model, 'city') ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'country_id')->widget(Select2::classname(), [
                'data' => $model->getCountryList(),
                'options' => ['placeholder' => Yii::t("admin", "Select country")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'staff_demand_type')->widget(Select2::classname(), [
                'data' => $model->getStaffDemandTypesList(),
                'options' => ['placeholder' => Yii::t("admin", "Select staff demand tpye")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'booking_type')->widget(Select2::classname(), [
                'data' => $model->getBookingTypesList(),
                'options' => ['placeholder' => Yii::t("admin", "Select booking tpye")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_from')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_to')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <?= $form->field($model, 'booking_price_discount') ?>
        </div>
        
    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
