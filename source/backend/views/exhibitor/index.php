<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\exhibitor\ExhibitorIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Exhibitors');
?>
<div class="event-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => [
                    'create' => [
                        'label' => Yii::t('admin', 'Add  Exhibitor')),
                        'class' => 'btn btn-success',
                        'url' => ['create']
                    ]
                ]
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?> ">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">

        <?php
        $widget = $searchModel->getWidget($this->title, $dataProvider);

        $export_menu = "<label>" . Yii::t('admin', 'Export') . "</label><ul class='export-menu-list'>" . $widget . "</ul>";
        ?>

        <?= $export_menu ?>
        <br>
        <br>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'first_name',
                'last_name',
                'email',
                'phone_number',
                'date_created',
                [
                    'attribute' => 'company',
                    'value' => function ($model)
                    {
                        return $model->exhibitorProfile->company_name;
                    }
                ],
                [
                    'attribute' => 'tax_number',
                    'value' => function ($model)
                    {
                        return $model->exhibitorProfile->tax_number;
                    }
                ],
                [
                    'attribute' => 'staff_demand_type',
                    'value' => function($model)
                    {
                        return $model->exhibitorProfile->getStaffDemandTypeLabel();
                    }
                ],
                [
                    'attribute' => 'booking_type',
                    'value' => function($model)
                    {
                        return $model->exhibitorProfile->getBookingTypeLabel();
                    }
                ],
                [
                    'attribute' => 'booking_price_discount',
                    'value' => function($model)
                    {
                        return $model->exhibitorProfile->booking_price_discount;
                    }
                ],
                'statusLabel',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($_, $model)
                        {
                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), [ 'exhibitor/view', 'id' => $model->id], [
                                        'title' => Yii::t('admin', 'View'),
                                        'class' => "btn btn-default action-btn btn-xs"
                            ]);
                        }
                            ],
                            'contentOptions' => [
                                'style' => "text-align: center;"
                            ]
                        ],
                    ]
                ]);
                ?>
    </div>
</div>
