<?php
/* @var $this yii\web\View */
/* @var $model backend\models\exhibitor\ExhibitorForm */

$this->title = $title;
?>
<div class="exhibitor-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
