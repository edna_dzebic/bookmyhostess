<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\exhibitor\ExhibitorForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="exhibitor-form x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false
    ]);
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="user-form x_panel-body">

        <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

        <div class="row">
            <div class="col-xs-12">
                <p><label><?= Yii::t('admin', 'User details') ?></label></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'gender')->widget(Select2::classname(), [
                    'data' => $model->getGenderList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select gender")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'lang')->widget(Select2::classname(), [
                    'data' => $model->getLanguageList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select language")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <?php if ($model->isNew()) : ?>
            <div class="row">
                <div class="col-xs-8">
                    <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
            </div>            
        </div>

        <hr>

        <div class="row">
            <div class="col-xs-12">
                <p><label><?= Yii::t('admin', 'Company details') ?></label></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'tax_number')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'home_number')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'country_id')->widget(Select2::classname(), [
                    'data' => $model->getCountryList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select country")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-xs-12">
                <p><label><?= Yii::t('admin', 'Booking details') ?></label></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'staff_demand_type')->widget(Select2::classname(), [
                    'data' => $model->getStaffDemandTypesList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select staff demand type")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?=
                $form->field($model, 'booking_type')->widget(Select2::classname(), [
                    'data' => $model->getBookingTypesList(),
                    'options' => ['placeholder' => Yii::t("admin", "Select booking type")],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'booking_price_discount')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <?= $form->field($model, 'is_invoice_payment_allowed')->checkbox() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <label><?= Yii::t('admin', 'Pricing:') ?></label>
                <ul>
                    <li>
                        <b>1.</b> 
                        <?= Yii::t('admin', 'If you chose booking type free, booking price per day in all countries will be 0 EUR') ?> 
                    </li>
                    <li>
                        <b>2.</b> 
                        <?= Yii::t('admin', 'If you chose booking type regular, booking price per day will be taken from country settings') ?> 
                    </li>
                    <li>
                        <b>3.</b> 
                        <?= Yii::t('admin', 'If you chose booking type special and you enter booking price discount, booking price per day will be calculated using booking price for specific country, but with given discount applied. Enter this valus as a normal number ( without sign % !) <br> For example, if booking price per day for Germany is 25 EUR, and you give exhibitor 10% discount, his booking price per day for Germany will be 22.5 EUR.') ?> 
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="x_panel-footer">
        <div class="form-group">
            <?=
            $this->render('//site/_back_submit', [
                'backUrl' => $model->getBackUrl()
            ]);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
