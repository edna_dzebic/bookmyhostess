<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;

$datePickerOptions = [
    'language' => "en",
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true
    ]
];


/* @var $this yii\web\View */
/* @var $model backend\models\hostessreview\HostessReviewIndex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hostess-review-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
    ]);
    ?>

    <div class="row">


        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'exhibitor_id')->widget(Select2::classname(), [
                'data' => $model->getExhibitorList(),
                'options' => ['placeholder' => Yii::t("admin", "Select exhibitor")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'hostess_id')->widget(Select2::classname(), [
                'data' => $model->getHostessList(),
                'options' => ['placeholder' => Yii::t("admin", "Select hostess")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'event_id')->widget(Select2::classname(), [
                'data' => $model->getEventList(),
                'options' => ['placeholder' => Yii::t("admin", "Select event")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <?=
            $form->field($model, 'status_id')->widget(Select2::classname(), [
                'data' => $model->getStatusList(),
                'options' => ['placeholder' => Yii::t("admin", "Select status")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_from')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <?=
            $form->field($model, 'date_to')->widget(DatePicker::className(), $datePickerOptions)
            ?>
        </div>
        <div class="col-lg-4 col-md-11 col-sm-12 col-xs-12">
            <?=
            $form->field($model, 'rating')->widget(Select2::classname(), [
                'data' => $model->getRatingDropdown(),
                'options' => ['placeholder' => Yii::t("admin", "Select rating")],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>

    <?= $this->render("//site/_search_submit") ?>

    <?php ActiveForm::end(); ?>

</div>
