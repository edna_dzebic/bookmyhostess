<?php

/* @var $this yii\web\View */
/* @var $model backend\models\hostessreview\HostessReviewForm */

$this->title = $title;
?>
<div class="hostess-review-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
