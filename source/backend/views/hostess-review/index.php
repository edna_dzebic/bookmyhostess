<?php

use kartik\rating\StarRating;
use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\hostessreview\HostessReviewIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Hostess Reviews');
?>
<div class="hostess-review-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => [
                    'create' => [
                        'label' => Yii::t('admin', Yii::t('app', 'Add  Hostess Review')),
                        'class' => 'btn btn-success',
                        'url' => ['create']
                    ]
                ]
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?>">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'exhibitor_id',
                    'format' => 'raw',
                    'value' => function($model)
                    {
                        return Html::a($model->getExhibitorFullName(), ["/exhibitor/view", "id" => $model->exhibitor_id], ["target" => "_blank"]);
                    }
                        ],
                        [
                            'attribute' => 'hostess_id',
                            'format' => 'raw',
                            'value' => function($model)
                            {
                                return Html::a($model->getHostessFullName(), ["/hostess/view", "id" => $model->hostess_id], ["target" => "_blank"]);
                            }
                                ],
                                [
                                    'attribute' => 'event_id',
                                    'format' => 'raw',
                                    'value' => function($model)
                                    {
                                        return Html::a($model->getEventName(), ["/event/view", "id" => $model->event_id], ["target" => "_blank"]);
                                    }
                                        ],
                                        [
                                            'attribute' => 'rating',
                                            'format' => 'raw',
                                            'value' => function ($model)
                                            {

                                                if (is_null($model->rating))
                                                {
                                                    return "-";
                                                }
                                                return StarRating::widget([
                                                            'name' => "rating",
                                                            'value' => $model->rating,
                                                            'pluginOptions' => [
                                                                'readonly' => true,
                                                                'showClear' => false,
                                                                'showCaption' => false,
                                                                'size' => 'xxs'
                                                            ]
                                                ]);
                                            }
                                                ],
                                                'date_created',
                                                [
                                                    'attribute' => 'status_id',
                                                    'value' => function($model)
                                                    {
                                                        return $model->getStatusLabel();
                                                    }
                                                ],
                                                [
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'template' => '{view}',
                                                    'buttons' => [
                                                        'view' => function ($_, $model)
                                                        {
                                                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), ['hostess-review/view', 'id' => $model->id], [
                                                                        'title' => Yii::t('admin', 'View'),
                                                                        'class' => "btn btn-default action-btn btn-xs"
                                                            ]);
                                                        }
                                                            ],
                                                            'contentOptions' => [
                                                                'style' => "text-align: center;"
                                                            ]
                                                        ],
                                                    ]
                                                ]);
                                                ?>
    </div>
</div>
