<?php

use yii\widgets\DetailView;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model backend\models\hostessreview\HostessReviewView */

$this->title = $model->id;
?>
<div class="hostess-review-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'eventName',
                'hostessFullName',
                'exhibitorFullName',
                [
                    'attribute' => 'rating',
                    'format' => 'raw',
                    'value' => function($model, $widget)
                    {
                        if (is_null($model->rating))
                        {
                            return "-";
                        }
                        return StarRating::widget([
                                    'name' => "rating",
                                    'value' => $model->rating,
                                    'pluginOptions' => [
                                        'readonly' => true,
                                        'showClear' => false,
                                        'showCaption' => false,
                                        'size' => 'xxs'
                                    ]
                        ]);
                    }
                        ],
                        'comment',
                        'date_created',
                        'date_updated',
                        'createdByUsername',
                        'updatedByUsername',
                        'statusLabel',
                    ],
                ])
                ?>
    </div>
</div>
