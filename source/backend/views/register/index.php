<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\register\RegisterIndex */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Event registrations');
?>
<div class="register-index x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget(
            [
                'title' => $this->title,
                'searchIsEmpty' => $hideSearch,
                'showSearch' => true,
                'buttons' => [
                    'create' => [
                        'label' => Yii::t('admin', 'Add')),
                        'class' => 'btn btn-success',
                        'url' => ['create']
                    ]
                ]
            ]
    );
    ?>
    <div class="x_panel-body well-lg search <?= $hideSearch ? 'not-important-hidden' : '' ?>">

        <?=
        $this->render(
                '_search', [
            'model' => $searchModel
                ]
        );
        ?>
    </div>

    <div class="x_panel-body">
        
        <?php
        $widget = $searchModel->getWidget($this->title, $dataProvider);

        $export_menu = "<label>" . Yii::t('admin', 'Export') . "</label><ul class='export-menu-list'>" . $widget . "</ul>";
        ?>

        <?= $export_menu ?>
        <br>
        <br>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'event_id',
                    'format' => 'raw',
                    'value' => function($model)
                    {
                        return Html::a($model->getEventName(), ["/event/view", "id" => $model->event_id], ["target" => "_blank"]);
                    }
                        ],
                        [
                            'attribute' => 'user_id',
                            'label' => Yii::t('admin', 'Username'),
                            'format' => 'raw',
                            'value' => function($model)
                            {
                                return Html::a($model->getUserUserName(), ["/hostess/view", "id" => $model->user_id], ["target" => "_blank"]);
                            }
                                ],
                                [
                                    'attribute' => 'user_id',
                                    'label' => Yii::t('admin', 'First name'),
                                    'format' => 'raw',
                                    'value' => function($model)
                                    {
                                        return $model->getUserFirstName();
                                    }
                                ],
                                [
                                    'attribute' => 'user_id',
                                    'label' => Yii::t('admin', 'Last name'),
                                    'format' => 'raw',
                                    'value' => function($model)
                                    {
                                        return $model->getUserLastName();
                                    }
                                ],
                                [
                                    'attribute' => 'user_id',
                                    'label' => Yii::t('admin', 'Email'),
                                    'format' => 'raw',
                                    'value' => function($model)
                                    {
                                        return $model->getUserEmail();
                                    }
                                ],
                                [
                                    'attribute' => 'user_id',
                                    'label' => Yii::t('admin', 'Phone'),
                                    'format' => 'raw',
                                    'value' => function($model)
                                    {
                                        return $model->getUserPhone();
                                    }
                                ],
                                'price',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view}{update}',
                                    'buttons' => [
                                        'view' => function ($_, $model)
                                        {
                                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i> &nbsp;' . Yii::t('admin', 'View'), ['register/view', 'id' => $model->id], [
                                                        'title' => Yii::t('admin', 'View'),
                                                        'class' => "btn btn-default action-btn btn-xs"
                                            ]);
                                        },
                                                'update' => function ($_, $model)
                                        {
                                            return Html::a('<i class="glyphicon white glyphicon-pencil"></i> &nbsp;' . Yii::t('admin', 'Update'), ['register/update', 'id' => $model->id], [
                                                        'title' => Yii::t('admin', 'Update'),
                                                        'class' => "btn btn-success action-btn btn-xs"
                                            ]);
                                        }
                                            ],
                                            'contentOptions' => [
                                                'style' => "text-align: center;"
                                            ]
                                        ],
                                    ]
                                ]);
                                ?>
    </div>
</div>
