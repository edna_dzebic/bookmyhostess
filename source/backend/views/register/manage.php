<?php
/* @var $this yii\web\View */
/* @var $model backend\models\register\RegisterForm */

$this->title = $title;
?>
<div class="register-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
