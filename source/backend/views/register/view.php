<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\register\RegisterView */

$this->title = $model->id;
?>
<div class="register-view x_panel">

    <?=
    backend\components\ButtonsHeaderWidget\HeaderWidget::widget([
        'title' => $this->title,
        'showSearch' => false,
        'buttons' => $model->getButtons()
    ]);
    ?>

    <div class="x_panel-body well-lg">
        <div class="table-responsive">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'userFullName',
                    'eventName',
                    'price',
                    'date_created',
                    'date_updated',
                    'statusLabel',
                    'createdByUsername',
                    'updatedByUsername',
                ],
            ])
            ?>
        </div>
    </div>
</div>
