<?php

namespace backend\assets;

class DashboardAsset extends \yii\web\AssetBundle
{

    public $sourcePath = '@backend/assets';
    public $css = [
        'css/dashboard.css'
    ];
    public $js = [
        'js/moment.min.js',
        'js/chart.js',
        'js/raphael.min.js',
        'js/morris.min.js',
        'js/dashboard.js'
    ];
    public $depends = [
        '\backend\assets\AppAsset'
    ];

}
