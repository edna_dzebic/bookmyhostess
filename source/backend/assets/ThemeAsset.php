<?php

namespace backend\assets;

use yii\web\AssetBundle;

class ThemeAsset extends AssetBundle
{

    public $sourcePath = '@backend/assets';
    public $css = [
        'css/custom.css'
    ];
    public $js = [
        'js/custom.js',
        'js/extension.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\JqueryAsset'
    ];

}
