<?php

namespace backend\assets;

class InvoiceAsset extends \yii\web\AssetBundle
{

    public $sourcePath = '@backend/assets';
    public $css = [
    ];
    public $js = [
        'js/invoice.js'
    ];
    public $depends = [
        '\backend\assets\AppAsset'
    ];

}
