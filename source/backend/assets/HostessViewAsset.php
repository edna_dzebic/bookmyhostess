<?php

namespace backend\assets;

class HostessViewAsset extends \yii\web\AssetBundle
{

    public $sourcePath = '@backend/assets';
    public $css = [
    ];
    public $js = [
        'js/hostess-view.js'
    ];
    public $depends = [
        '\backend\assets\AppAsset'
    ];

}
