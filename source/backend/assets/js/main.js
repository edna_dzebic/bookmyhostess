"use strict";

var Main = {};

$("document").ready(function () {
    new SearchToggle('.show-search', '.hide-search', '.search');

    Main.reset.init();
});

Main.reset = {
    init: function () {
        $('.js-reset').click(function () {
            var form = $(this)[0].form;

            var csrf = $("[name='_csrf']", $(form)).val();

            $(form).find(':radio, :checkbox').removeAttr('checked').end()
                    .find('textarea, :text, select, :hidden').val('');

            if ($('.js-multi-select').length !== 0)
            {
                $('.js-multi-select').multiselect('refresh');
            }

            $("[name='_csrf']", $(form)).val(csrf);
            $(form).submit();

            return false;
        });
    }
};