"use strict";

var Main = Main || {};


$("document").ready(function () {
    Main.hostessView.init();
});

Main.hostessView = {
    init: function () {
        $('.js-btn-disapprove-hostess').unbind('click').click(function (e) {
            e.preventDefault();
            Main.hostessView.showDisapproveModal($(this));
            return false;
        });
    },
    showDisapproveModal: function ($btn) {
        var $modal = $("#disapprove-hostess-modal");
        var href = $btn.attr('href');

        Main.hostessView.getDisapproveModalBody($modal, href);

        $modal.modal();
    },
    getDisapproveModalBody: function ($modal, url)
    {
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'json',
            success: function (data) {
                $modal.find(".modal-body").html(data["view"]);
                Main.hostessView.bindSubmitDisapprove($modal, url);
            }
        });
    },
    bindSubmitDisapprove: function ($modal, url) {
        $('.js-btn-confirm-disapprove', $modal).unbind('click').click(function (e) {
            e.preventDefault();

            $(".loading-content", $modal).show();
            var $btn = $(".js-btn-confirm-disapprove", $modal);
            $btn.attr("disabled", true);
            $("#disapprove-form").hide();

            $.ajax({
                url: url,
                data: $("#disapprove-form").serialize(),
                method: 'POST',
                dataType: "json",
                success: function (data) {
                    $(".loading-content", $modal).hide();
                    $btn.attr("disabled", false);                    
                    $modal.find(".modal-body").html(data["view"]);
                }
            });

            return false;
        });
    }
};