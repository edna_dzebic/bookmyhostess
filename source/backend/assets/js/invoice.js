"use strict";

var Main = Main || {};

$("document").ready(function () {
    Main.download.init();
});

Main.download = {
    init: function () {
        $('.js-download-zip').click(function () {
            $.ajax({
                url: "/invoice/download-zip",
                data: $("#invoice-form").serialize(),
                method: 'POST',
                dataType: "json",
                success: function (data) {

                    if (data.status == 0)
                    {
                        $("body").append("<iframe src='" + data.url +
                                "' style='display: none;' ></iframe>");
                    }
                    else {
                        alert(data.message);
                    }
                }
            });
        });
    }
};