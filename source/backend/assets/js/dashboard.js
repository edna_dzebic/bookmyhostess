"use strict";

var Main = Main || {};

$("document").ready(function () {

    Main.dashboard.init();
});


Main.dashboard = {
    init: function ()
    {
        Main.dashboard.invoice.init();
        Main.dashboard.request.init();
        Main.dashboard.hostess.init();
        Main.dashboard.exhibitor.init();
    },
    initLineTimeChart: function (data, elem) {

        var ctx = document.getElementById(elem).getContext("2d");

        var cfg = {
            type: 'bar',
            data: {
                labels: data["labels"],
                datasets: [{
                        label: data["label"],
                        data: data["dataset"],
                        type: 'line',
                        pointRadius: 0,
                        fill: false,
                        lineTension: 0,
                        borderWidth: 2
                    }]
            },
            options: {
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                            type: 'time',
                            distribution: 'series',
                            ticks: {
                                source: 'labels'
                            }
                        }]
                },
                responsive: true,
                maintainAspectRatio: false
            }
        };

        new Chart(ctx, cfg);
    },
    initDoughnutChart: function (data, elem) {

        var options = {
            legend: {
                display: true,
                position: 'bottom'
            },
            responsive: true,
            maintainAspectRatio: false
        };

        new Chart(elem, {
            type: 'doughnut',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: data["labels"],
                datasets: data["datasets"]
            },
            options: options
        });
    },
    initPieChart: function (data, elem) {

        var options = {
            legend: {
                display: true,
                position: 'bottom'
            },
            responsive: true,
            maintainAspectRatio: false
        };

        new Chart(elem, {
            type: 'pie',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: data["labels"],
                datasets: data["datasets"]
            },
            options: options
        });
    },
    initBarChart: function (data, elem) {

        Morris.Bar({
            element: elem,
            data: data["dataset"],
            xkey: data["xkey"],
            hideHover: 'auto',
            ykeys: data["ykeys"],
            labels: data["labels"],
            xLabelAngle: 45,
            resize: true
        });

        // $MENU_TOGGLE defined in custom.js
        $MENU_TOGGLE.on('click', function () {
            $(window).resize();
        });

    },
    hostess: {
        init: function ()
        {

            $.ajax({
                url: "/dashboard/monthly-hostess-registration",
                type: "GET",
                dataType: "json",
                success: function (data)
                {
                    Main.dashboard.initBarChart(data, "monthly-hostess-canvas");
                },
                error: function (data)
                {
                    console.log(data);
                }
            });

            $.ajax({
                url: "/dashboard/price-class",
                type: "GET",
                dataType: "json",
                success: function (data)
                {
                    Main.dashboard.initPieChart(data, $("#price-class-canvas"));
                },
                error: function (data)
                {
                    console.log(data);
                }
            });

        }
    },
    exhibitor: {
        init: function ()
        {

            $.ajax({
                url: "/dashboard/monthly-exhibitor-registration",
                type: "GET",
                dataType: "json",
                success: function (data)
                {
                    Main.dashboard.initBarChart(data, "monthly-exhibitor-canvas");
                },
                error: function (data)
                {
                    console.log(data);
                }
            });
        }
    },
    invoice: {
        init: function () {

            $.ajax({
                url: "/dashboard/invoice-amount-this-month",
                type: "GET",
                dataType: "json",
                success: function (data)
                {
                    Main.dashboard.initLineTimeChart(data, "money");
                },
                error: function (data)
                {
                    console.log(data);
                }
            });
        }
    },
    request: {
        init: function () {

            $.ajax({
                url: "/dashboard/request-number-this-month",
                type: "GET",
                dataType: "json",
                success: function (data)
                {
                    Main.dashboard.initLineTimeChart(data, "request");
                },
                error: function (data)
                {
                    console.log(data);
                }
            });
        }
    }
};