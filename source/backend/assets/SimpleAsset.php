<?php

namespace backend\assets;

use yii\web\AssetBundle;

class SimpleAsset extends AssetBundle
{

    public $sourcePath = '@backend/assets';
    public $css = ['css/site.css'];
    public $js = [];
    public $depends = ['\backend\assets\ThemeAsset'];

}
