<?php

namespace backend\assets;

class GridViewAsset extends \yii\web\AssetBundle
{

    public $sourcePath = '@backend/assets';
    public $css = ['css/data-tables.css',];
    public $js = [];
    public $depends = ['\backend\assets\AppAsset'];

}
