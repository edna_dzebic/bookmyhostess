<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{

    public $sourcePath = '@backend/assets';
    public $css = [
        'css/fancybox.min.css',
        'css/site.css'
    ];
    public $js = [
        'js/fancybox.min.js',
        'js/SearchToggle.js',
        'js/main.js'
    ];
    public $depends = [
        '\backend\assets\ThemeAsset'
    ];

}
