<?php

namespace backend\controllers;

use Yii;
use common\models\DressCode;
use backend\models\dresscode\DressCodeView;
use backend\models\dresscode\DressCodeForm;
use backend\models\dresscode\DressCodeIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * DressCodeController implements the CRUD actions for DressCode model.
 */
class DressCodeController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "DressCode {} created",
                            'update:POST' => "DressCode {id} updated",
                            'delete' => "DressCode {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                DressCodeView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                DressCodeForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                DressCodeForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all DressCode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DressCodeIndex(['modelObject' => new DressCode()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single DressCode model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new DressCode model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DressCodeForm(['modelObject' => new DressCode()]);

        return $this->manage($model, Yii::t('admin', 'DressCode successfully created'));
    }

    /**
     * Updates an existing DressCode model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'DressCode successfully updated'));
    }

    /**
     * Deletes an existing DressCode model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'DressCode successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'DressCode delete failed'));
        }

        $return = Yii::$app->session->get(
                DressCodeView::SESSION_KEY_BACK_URL, ['/dress-code']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the DressCode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\dresscode\DressCodeForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new DressCodeForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the DressCodeView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\dresscode\DressCodeView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new DressCodeView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the DressCode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\DressCode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DressCode::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param DressCodeForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(DressCodeForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param DressCodeForm $formModel
     * @return string
     */
    private function createTitle(DressCodeForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Dress Code');
        } else
        {
            $title = Yii::t('admin', 'Update Dress Code') . ' ' . $formModel->modelObject->name;
        }

        return $title;
    }

}
