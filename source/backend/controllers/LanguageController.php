<?php

namespace backend\controllers;

use Yii;
use common\models\Language;
use backend\models\language\LanguageView;
use backend\models\language\LanguageForm;
use backend\models\language\LanguageIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * LanguageController implements the CRUD actions for Language model.
 */
class LanguageController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "Language {} created",
                            'update:POST' => "Language {id} updated",
                            'delete' => "Language {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                LanguageView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                LanguageForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                LanguageForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all Language models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LanguageIndex(['modelObject' => new Language()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single Language model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new Language model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LanguageForm(['modelObject' => new Language()]);

        return $this->manage($model, Yii::t('admin', 'Language successfully created'));
    }

    /**
     * Updates an existing Language model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'Language successfully updated'));
    }

    /**
     * Deletes an existing Language model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Language successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Language delete failed'));
        }
        $return = Yii::$app->session->get(
                LanguageView::SESSION_KEY_BACK_URL, ['/language']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the Language model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\language\LanguageForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new LanguageForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the LanguageView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\language\LanguageView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new LanguageView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Language model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\Language the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Language::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param LanguageForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(LanguageForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param LanguageForm $formModel
     * @return string
     */
    private function createTitle(LanguageForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Language');
        } else
        {
            $title = Yii::t('admin', 'Update Language') . ' ' . $formModel->modelObject->name;
        }

        return $title;
    }

}
