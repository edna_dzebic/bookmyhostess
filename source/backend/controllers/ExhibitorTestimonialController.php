<?php

namespace backend\controllers;

use Yii;
use common\models\ExhibitorTestimonial;
use backend\models\exhibitortestimonial\ExhibitorTestimonialView;
use backend\models\exhibitortestimonial\ExhibitorTestimonialForm;
use backend\models\exhibitortestimonial\ExhibitorTestimonialIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * ExhibitorTestimonialController implements the CRUD actions for ExhibitorTestimonial model.
 */
class ExhibitorTestimonialController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            //'create:POST' => "ExhibitorTestimonial {} created",
                            'update:POST' => "ExhibitorTestimonial {id} updated",
                            'delete' => "ExhibitorTestimonial {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                ExhibitorTestimonialView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ExhibitorTestimonialForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all ExhibitorTestimonial models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExhibitorTestimonialIndex(['modelObject' => new ExhibitorTestimonial()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single ExhibitorTestimonial model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new ExhibitorTestimonial model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ExhibitorTestimonialForm(['modelObject' => new ExhibitorTestimonial()]);

        return $this->manage($model, Yii::t('admin', 'ExhibitorTestimonial successfully created'));
    }

    /**
     * Updates an existing ExhibitorTestimonial model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'ExhibitorTestimonial successfully updated'));
    }

    /**
     * Deletes an existing ExhibitorTestimonial model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'ExhibitorTestimonial successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'ExhibitorTestimonial delete failed'));
        }
        $return = Yii::$app->session->get(
                ExhibitorTestimonialView::SESSION_KEY_BACK_URL, ['/exhibitor-testimonial']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the ExhibitorTestimonial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\exhibitortestimonial\ExhibitorTestimonialForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new ExhibitorTestimonialForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the ExhibitorTestimonialView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\exhibitortestimonial\ExhibitorTestimonialView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new ExhibitorTestimonialView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the ExhibitorTestimonial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\ExhibitorTestimonial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ExhibitorTestimonial::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param ExhibitorTestimonialForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(ExhibitorTestimonialForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param ExhibitorTestimonialForm $formModel
     * @return string
     */
    private function createTitle(ExhibitorTestimonialForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Exhibitor Testimonial');
        } else
        {
            $title = Yii::t('admin', 'Update Exhibitor Testimonial') . ' ' . $formModel->modelObject->id;
        }

        return $title;
    }

}
