<?php

namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use common\models\Setting;

/**
 * SettingSubscriberController implements the CRUD actions for Setting model_
 */
class SettingController extends \backend\components\Controller
{

    /**
     * Lists all Setting models
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new \backend\models\setting\SettingForm();

        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'New changes are successfully saved.'));

                return $this->refresh();
            } else
            {
                Yii::$app->session->setFlash('error', Yii::t('admin', 'Error occured while savign data. Please try again or contact developers.'));
            }
        }

        $invoice = new \common\models\Invoice();
        $countInvoice = $invoice->getThisYearCount();

        return $this->render('setting_form', ['settings' => $model, 'countInvoice' => $countInvoice]);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param $key
     * @return Setting the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($key)
    {
        if (($model = Setting::findByKey($key)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

}
