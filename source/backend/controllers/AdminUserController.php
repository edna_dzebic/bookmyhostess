<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use backend\models\adminUser\AdminUserForm;
use backend\models\adminUser\AdminUserIndex;
use backend\models\adminUser\AdminUserView;
use backend\models\user\ChangePasswordForm;
use common\models\User;

/**
 * AdminUserController implements the CRUD actions for User model.
 */
class AdminUserController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['sysadmin']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-password'],
                        'roles' => ['admin'],
                    ],
                ]
            ],
            'audit' => [
                'class' => \common\components\AuditBehavior::className(),
                'actions' => [
                    'create:POST' => "User {} created",
                    'update:POST' => "User {id} updated",
                    'delete' => "User {id} deleted",
                    'changePassword:POST' => "Changed User {id} password"
                ]
            ],
            'session' => [
                'class' => \common\components\SessionBehavior::className(),
                'actions' => [
                    'index' => [
                        AdminUserView::SESSION_KEY_BACK_URL => Url::current(),
                        AdminUserForm::SESSION_KEY_BACK_URL => Url::current()
                    ],
                    'view' => [
                        AdminUserForm::SESSION_KEY_BACK_URL => Url::current()
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdminUserIndex(['modelObject' => new User()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id)
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdminUserForm([
            'modelObject' => new User(),
            'scenario' => AdminUserForm::SCENARIO_NEW
        ]);

        return $this->manage($model, Yii::t('admin', 'User created successfully'));
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'User updated successfully'));
    }

    /**
     * Updates an existing Users password
     * @param $id
     * @return string
     */
    public function actionChangePassword($id)
    {
        $model = new ChangePasswordForm(['modelObject' => $this->findModel($id)]);

        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Password changed successfully'));

            if (Yii::$app->user->id == $id)
            {
                return $this->redirect(["/dashboard"]);
            }

            return $this->redirect($model->getBackUrl());
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Password not changed successfully. Errors are ' . implode(", ", $model->getErrors()) ));
        }

        return $this->render("change-password", [
                    'model' => $model
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);

        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'User deleted successfully'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Cannot delete user'));
        }

        $return = Yii::$app->session->get(
                AdminUserView::SESSION_KEY_BACK_URL, ['/admin-user']
        );
        return $this->redirect($return);
    }

    /**
     * Disable an existing User model.
     * If disable is successful, the browser will be redirected to the 'previous' page.
     * @param string $id
     * @return mixed
     */
    public function actionDisable($id)
    {
        $model = $this->findFormModel($id);
        if ($model->disable())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'User disabled successfully'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Cannot disable user'));
        }


        return $this->redirect($model->getBackUrl());
    }

    /**
     * Enable an existing User model.
     * If enable is successful, the browser will be redirected to the 'previous' page.
     * @param string $id
     * @return mixed
     */
    public function actionEnable($id)
    {
        $model = $this->findFormModel($id);
        if ($model->enable())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'User enabled successfully'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Cannot enable user'));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Finds the UserView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return \backend\models\adminUser\AdminUserView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new AdminUserView(['modelObject' => User::findOne($id)]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return \backend\models\adminUser\AdminUserForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new AdminUserForm(['modelObject' => User::findOne($id)]);
    }

    /**
     * Find the User model based on its primary key value
     * @param $id
     * @return \common\models\User
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) != null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param AdminUserForm $model
     * @param $message
     * @return mixed
     */
    private function manage(AdminUserForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
                        ]
        );
    }

    /**
     * Generate page title
     *
     * @param AdminUserForm $formModel
     * @return string
     */
    private function createTitle(AdminUserForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create User');
        } else
        {
            $title = Yii::t('admin', 'Update User') . ' ' . $formModel->modelObject->id;
        }

        return $title;
    }

}
