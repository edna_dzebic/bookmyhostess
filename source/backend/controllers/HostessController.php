<?php

namespace backend\controllers;

use Yii;
use common\models\Register;
use common\models\HostessProfile;
use backend\models\User;
use backend\models\hostess\form\ProfileForm;
use backend\models\hostess\form\HostessForm;
use backend\models\hostess\form\EducationForm;
use backend\models\hostess\form\WorkExperienceForm;
use backend\models\hostess\form\BillingForm;
use backend\models\hostess\form\ImageForm;
use backend\models\hostess\form\DocumentForm;
use backend\models\hostess\form\LanguageForm;
use backend\models\hostess\form\CityForm;
use backend\models\hostess\form\JobForm;
use backend\models\hostess\form\CountryMappingForm;
use backend\models\hostess\form\ReviewForm;
use backend\models\hostess\HostessIndex;
use backend\models\hostess\HostessView;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

/**
 * HostessController implements the CRUD actions for Hostess model.
 */
class HostessController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                HostessView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ProfileForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL_DELETE => \yii\helpers\Url::current(),
                            ],
                            'pending' => [
                                HostessView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ProfileForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL_DELETE => \yii\helpers\Url::current(),
                            ],
                            'active' => [
                                HostessView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ProfileForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL_DELETE => \yii\helpers\Url::current(),
                            ],
                            'disabled' => [
                                HostessView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ProfileForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL_DELETE => \yii\helpers\Url::current(),
                            ],
                            'deleted' => [
                                HostessView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ProfileForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL_DELETE => \yii\helpers\Url::current(),
                            ],
                            'approved' => [
                                HostessView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ProfileForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL_DELETE => \yii\helpers\Url::current(),
                            ],
                            'requested' => [
                                HostessView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ProfileForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL_DELETE => \yii\helpers\Url::current(),
                            ],
                            'not-requested' => [
                                HostessView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ProfileForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL_DELETE => \yii\helpers\Url::current(),
                            ],
                            'view' => [
                                ProfileForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                EducationForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                BillingForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                WorkExperienceForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ImageForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                DocumentForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                LanguageForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                JobForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                CityForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                CountryMappingForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ReviewForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Search all hostesses
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->renderIndex("searchAll");
    }

    /**
     * Search hostesses with active profile
     * @return mixed
     */
    public function actionActive()
    {
        return $this->renderIndex("searchActive");
    }

    /**
     * Search newly registered hostesses
     * @return mixed
     */
    public function actionPending()
    {
        return $this->renderIndex("searchPending");
    }

    /**
     * Search hostesses that have been blocked
     *
     * @return string
     */
    public function actionDisabled()
    {
        return $this->renderIndex("searchDisabled");
    }

    /**
     * Search hostesses that have been deleted
     *
     * @return string
     */
    public function actionDeleted()
    {
        return $this->renderIndex("searchDeleted");
    }

    /**
     * Search hostesses that have requested approval
     *
     * @return string
     */
    public function actionRequested()
    {
        return $this->renderIndex("searchRequested");
    }

    /**
     * Search hostesses that have not requested approval
     *
     * @return string
     */
    public function actionNotRequested()
    {
        return $this->renderIndex("searchNotRequested");
    }

    /**
     * Search hostesses that have been approved
     *
     * @return string
     */
    public function actionApproved()
    {
        return $this->renderIndex("searchApproved");
    }

    /**
     * Displays a single Hostess model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findViewModel($id);
        $profileModel = $model->profileObject;
        $userModel = $model->modelObject;

        return $this->render('view', [
                    'model' => $model,
                    'userModel' => $userModel,
                    'profileModel' => $profileModel
        ]);
    }

    /**
     * Downloads hostess profile PDF
     *
     * @param int $id
     *
     * @throws \yii\web\NotFoundHttpException
     * @throws BadRequestHttpException
     */
    public function actionPdf($id)
    {

        $this->layout = "pdf";

        $model = HostessProfile::find()
                ->with(["user", "pastWork", "preferredJobs"])
                ->andWhere(["user_id" => $id])
                ->one();

        list($eventAvailability, $availabilityData) = Register::getAllForHostess($id);

        $content = $this->renderPartial(
                "pdf", [
            "model" => $model,
            'eventAvailability' => $eventAvailability,
                ]
        );

        $pdf = new Pdf([
            'filename' => Yii::t('admin', 'User') . ' - ' . $model->user->username . ".pdf",
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::FORMAT_LETTER,
            // stream to browser inline
            'destination' => Pdf::DEST_DOWNLOAD,
            // your html content input
            'content' => $content,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'cssInline' => file_get_contents(Yii::getAlias('@backend/assets/css/pdf_profile.css')),
            'options' => ['title' => Yii::t('admin', 'User') . ' - ' . $model->user->username . " | " . Yii::$app->name],
            'marginTop' => 30,
            'marginBottom' => 30,
            'methods' => [
                'SetHTMLFooter' => ['<div class="foot-div green-text"><div class="pull-left">{PAGENO}</div><div class="pull-right">&copy;&nbsp;www.BOOKmyHOSTESS.com</div></div>']
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Finds the HostessView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\exhibitor\HostessView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new HostessView([
            'modelObject' => $this->findModel($id),
            'profileObject' => $this->findProfileModel($id)
        ]);
    }

    /**
     * Finds the Hostess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findHostess($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * @param $searchFunction
     *
     * @return string
     */
    private function renderIndex($searchFunction)
    {
        $searchModel = new HostessIndex(['modelObject' => new User()]);
        $dataProvider = $searchModel->$searchFunction(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => true
        ]);
    }

    /**
     * Find hostess profile model elminating status from query
     * @param int $id
     * @return type
     */
    private function findProfileModel($id)
    {
        if (($model = HostessProfile::find()->where(["user_id" => $id])->one()) !== null)
        {
            return $model;
        }

        return null;
    }

}
