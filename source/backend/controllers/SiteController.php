<?php

namespace backend\controllers;

use Yii;
use backend\models\LoginForm;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => [ '@']
                    ],
                ],
            ]
        ];
    }

    /**
     * Start entry point
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(["/dashboard"]);
    }

    /**
     * Login
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = "simple";
        if (!\Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            return $this->goBack();
        }

        return $this->render('login', ['model' => $model,]);
    }

    /**
     * Logout
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Error handling action
     * @return mixed
     */
    public function actionError()
    {
        /* @var \yii\web\HttpException $exception */
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null)
        {
            return $this->render("error", ['name' => $exception->statusCode, 'message' => $exception->getMessage()]);
        }
    }

}
