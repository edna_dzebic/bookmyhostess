<?php

namespace backend\controllers;

use Yii;
use common\models\Invoice;
use common\models\Request;
use common\models\Email;
use common\models\Register;
use common\models\ExhibitorProfile;
use backend\models\request\RequestView;
use backend\models\request\RequestForm;
use backend\models\request\RequestIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

/**
 * RequestController implements the CRUD actions for Request model.
 */
class RequestController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "Request {} created",
                            'update:POST' => "Request {id} updated",
                            'mark-as-completed' => "Request {id} marked as completed",
                            'delete' => "Request {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                RequestView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                RequestForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                RequestForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestIndex(['modelObject' => new Request()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single Request model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Updates an existing Request model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'Request successfully updated'));
    }

    /**
     * Deletes an existing Request model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Request successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Request delete failed'));
        }
        $return = Yii::$app->session->get(
                RequestView::SESSION_KEY_BACK_URL, ['/request']
        );
        return $this->redirect($return);
    }

    /**
     * Mark request as completed and send invoice emails to exhibitor and hostess
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionMarkAsCompleted($id)
    {
        $referrer = Yii::$app->request->referrer;

        $eventRequest = $this->findModel($id);

        $userModel = $eventRequest->requestedUser;

        if (!isset($userModel))
        {
            throw new \yii\web\NotFoundHttpException('Exhibitor user model not found');
        }

        $profileModel = $userModel->exhibitorProfile;

        if (!isset($profileModel))
        {
            throw new \yii\web\NotFoundHttpException('Exhibitor profile model not found');
        }

        if (!$profileModel->isProfileCompleted())
        {
            Yii::$app->session->setFlash('error', 'Exhibitor profile is not completed!');

            if (isset($referrer))
            {
                return $this->redirect($referrer);
            }

            return $this->redirect(["request/index"]);
        }

        $transaction = Yii::$app->db->beginTransaction();
        $invoice = $this->createInvoice($eventRequest);

        if (!isset($invoice))
        {
            $transaction->rollBack();
            throw new \Exception('Cannot create invoice', 500);
        }

        $eventRequest->invoice_id = $invoice->id;
        $eventRequest->completed = Request::COMPLETED;

        if ($eventRequest->update(false) !== false)
        {
            // remove hostess from this event since she is now booked
            $eventRegisterModel = Register::find()
                    ->andWhere([
                        "user_id" => $eventRequest->user_id,
                        "event_id" => $eventRequest->event_id])
                    ->one();

            if ($eventRegisterModel != null)
            {
                $eventRegisterModel->delete();
            } else
            {
                $transaction->rollBack();

                Yii::$app->session->setFlash('error', 'Event register model not found. This hostess is probably booked by another exhibitor and therefore registration has been already deleted.');

                if (isset($referrer))
                {
                    return $this->redirect($referrer);
                }

                return $this->redirect(["request/index"]);
            }
        }

        Email::sendInvoiceEmailToHostess($eventRequest);

        $oldLng = Yii::$app->language;

        $invoiceFileName = $this->createInvoiceFile($invoice);

        Yii::$app->language = $oldLng;

        $invoice->sendNotificationEmails($invoiceFileName);

        $transaction->commit();

        $invoice->fillHistoryData();

        Yii::$app->session->setFlash('success', Yii::t('admin', 'Invoice issued!'));

        if (isset($referrer))
        {
            return $this->redirect($referrer);
        }

        return $this->redirect(["request/index"]);
    }

    /**
     * Create invoice and store it to DB for specific request
     * @param Request $eventRequestModel
     * @return Invoice
     */
    private function createInvoice(Request $eventRequestModel)
    {
        $invoice = new Invoice();

        $invoice->status = Invoice::STATUS_PAID;
        $invoice->date_paid = date('Y-m-d H:i:s');
        $invoice->paypal_transaction_id = NULL;
        $invoice->date_created = $invoice->date_paid;

        $profileModel = $eventRequestModel->userRequested->exhibitorProfile;
        $invoice->requested_user_id = $profileModel->user_id;
        $invoice->exhibitor_booking_type = $profileModel->booking_type;

        $invoice->amount = $this->getInvoiceAmount($eventRequestModel, $profileModel);

        $invoice->is_reminder_allowed = 0;

        if ($invoice->getIsNormalInvoice())
        {
            $invoice->setNewRealId();
            $invoice->payment_type = Invoice::PAYMENT_TYPE_INVOICE;
            $invoice->status = Invoice::STATUS_NOT_PAID;
            $invoice->is_reminder_allowed = 1;
        } else
        {
            $invoice->payment_type = Invoice::PAYMENT_TYPE_RECEIPT;
            $invoice->status = Invoice::STATUS_PAID;
        }

        if ($invoice->save())
        {
            $invoice->filename = $invoice->getIsNormalInvoice() ? 'invoice/invoice_' . $invoice->id . '.pdf' : 'receipt/receipt_' . $invoice->id . '.pdf';

            if ($invoice->update() !== false)
            {
                return $invoice;
            }
        }

        return null;
    }

    /**
     * Calculate total invoice amount
     * @param Request $eventRequestModel
     * @param ExhibitorProfile $profileModel
     * @return double
     */
    private function getInvoiceAmount(Request $eventRequestModel, ExhibitorProfile $profileModel)
    {
        $amount = $eventRequestModel->total_booking_price;

        return $amount + $this->getTaxAmount($profileModel, $amount);
    }

    /**
     * Calculate tax amount for invoice
     * @param ExhibitorProfile $profileModel
     * @param double $total
     * @return double
     */
    public function getTaxAmount(ExhibitorProfile $profileModel, $total)
    {
        if (Yii::$app->util->isGerman($profileModel->country_id))
        {
            return 0.19 * $total;
        }

        return 0.0;
    }

    /**
     * Create pdf invoice file
     * @param Invoice $model
     * @return string
     */
    private function createInvoiceFile(Invoice $model)
    {

        define('_MPDF_TTFONTDATAPATH', Yii::getAlias('@runtime/mpdf'));

        Yii::$app->language = $model->userRequested->lang;

        $content = $this->renderPartial('/invoice/invoice_pdf', ["model" => $model]);
        $fileName = $model->filename;
        $pdf = new Pdf([
            'marginTop' => 9,
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_FILE,
            'content' => $content,
            'filename' => Yii::$app->params['invoice_save_path'] . $model->filename,
            'cssFile' => 'css/invoice_pdf.css',
            'cssInline' => file_get_contents(Yii::getAlias('@backend/assets/css/invoice_pdf.css')),
            'options' => ['title' => \Yii::$app->name],
        ]);

        $pdf->render();
        return $fileName;
    }

    /**
     * Finds the Request model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\request\RequestForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new RequestForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the RequestView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\request\RequestView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new RequestView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Request model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\Request the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param RequestForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(RequestForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param RequestForm $formModel
     * @return string
     */
    private function createTitle(RequestForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Request');
        } else
        {
            $title = Yii::t('admin', 'Update Request') . ' ' . $formModel->modelObject->id;
        }

        return $title;
    }

}
