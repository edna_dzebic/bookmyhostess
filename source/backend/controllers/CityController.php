<?php

namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use common\models\City;
use backend\models\city\CityView;
use backend\models\city\CityForm;
use backend\models\city\CityIndex;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "City {} created",
                            'update:POST' => "City {id} updated",
                            'delete' => "City {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                CityView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                CityForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                CityForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CityIndex(['modelObject' => new City()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single City model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new City model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CityForm(['modelObject' => new City()]);

        return $this->manage($model, Yii::t('admin', 'City successfully created'));
    }

    /**
     * Updates an existing City model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'City successfully updated'));
    }

    /**
     * Deletes an existing City model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'City successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'City delete failed'));
        }
        
        $return = Yii::$app->session->get(
                CityView::SESSION_KEY_BACK_URL, ['/city']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\city\CityForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new CityForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the CityView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\city\CityView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new CityView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\City the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = City::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param CityForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(CityForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param CityForm $formModel
     * @return string
     */
    private function createTitle(CityForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create City');
        } else
        {
            $title = Yii::t('admin', 'Update City') . ' ' . $formModel->modelObject->name;
        }

        return $title;
    }

}
