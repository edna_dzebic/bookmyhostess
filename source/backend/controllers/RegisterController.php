<?php

namespace backend\controllers;

use Yii;
use common\models\Register;
use backend\models\register\RegisterView;
use backend\models\register\RegisterForm;
use backend\models\register\RegisterIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * RegisterController implements the CRUD actions for Register model.
 */
class RegisterController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "Register {} created",
                            'update:POST' => "Register {id} updated",
                            'delete' => "Register {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                RegisterView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                RegisterForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                RegisterForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all Register models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RegisterIndex(['modelObject' => new Register()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single Register model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new Register model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RegisterForm(['modelObject' => new Register()]);

        return $this->manage($model, Yii::t('admin', 'Register successfully created'));
    }

    /**
     * Updates an existing Register model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'Register successfully updated'));
    }

    /**
     * Deletes an existing Register model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Register successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Register delete failed'));
        }
        $return = Yii::$app->session->get(
                RegisterView::SESSION_KEY_BACK_URL, ['/register']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the Register model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\register\RegisterForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new RegisterForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the RegisterView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\register\RegisterView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new RegisterView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Register model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\Register the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Register::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param RegisterForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(RegisterForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param RegisterForm $formModel
     * @return string
     */
    private function createTitle(RegisterForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Registration');
        } else
        {
            $title = Yii::t('admin', 'Update Registration') . ' ' . $formModel->modelObject->id;
        }

        return $title;
    }

}
