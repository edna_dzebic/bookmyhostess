<?php

namespace backend\controllers;

use Yii;
use common\models\LanguageLevel;
use backend\models\languagelevel\LanguageLevelView;
use backend\models\languagelevel\LanguageLevelForm;
use backend\models\languagelevel\LanguageLevelIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * LanguageLevelController implements the CRUD actions for LanguageLevel model.
 */
class LanguageLevelController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "LanguageLevel {} created",
                            'update:POST' => "LanguageLevel {id} updated",
                            'delete' => "LanguageLevel {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                LanguageLevelView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                LanguageLevelForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                LanguageLevelForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all LanguageLevel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LanguageLevelIndex(['modelObject' => new LanguageLevel()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single LanguageLevel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new LanguageLevel model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LanguageLevelForm(['modelObject' => new LanguageLevel()]);

        return $this->manage($model, Yii::t('admin', 'LanguageLevel successfully created'));
    }

    /**
     * Updates an existing LanguageLevel model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'LanguageLevel successfully updated'));
    }

    /**
     * Deletes an existing LanguageLevel model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'LanguageLevel successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'LanguageLevel delete failed'));
        }
        $return = Yii::$app->session->get(
                LanguageLevelView::SESSION_KEY_BACK_URL, ['/language-level']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the LanguageLevel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\languagelevel\LanguageLevelForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new LanguageLevelForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the LanguageLevelView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\languagelevel\LanguageLevelView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new LanguageLevelView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the LanguageLevel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\LanguageLevel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LanguageLevel::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param LanguageLevelForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(LanguageLevelForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param LanguageLevelForm $formModel
     * @return string
     */
    private function createTitle(LanguageLevelForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Language Level');
        } else
        {
            $title = Yii::t('admin', 'Update Language Level') . ' ' . $formModel->modelObject->name;
        }

        return $title;
    }

}
