<?php

namespace backend\controllers;

use Yii;
use common\models\CountryMapping;
use backend\models\countrymapping\CountryMappingView;
use backend\models\countrymapping\CountryMappingForm;
use backend\models\countrymapping\CountryMappingIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * CountryMappingController implements the CRUD actions for CountryMapping model.
 */
class CountryMappingController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "CountryMapping {} created",
                            'update:POST' => "CountryMapping {id} updated",
                            'delete' => "CountryMapping {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                CountryMappingView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                CountryMappingForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all CountryMapping models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CountryMappingIndex(['modelObject' => new CountryMapping()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single CountryMapping model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new CountryMapping model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CountryMappingForm(['modelObject' => new CountryMapping()]);

        return $this->manage($model, Yii::t('admin', 'CountryMapping successfully created'));
    }

    /**
     * Updates an existing CountryMapping model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'CountryMapping successfully updated'));
    }

    /**
     * Deletes an existing CountryMapping model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'CountryMapping successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'CountryMapping delete failed'));
        }

        $return = Yii::$app->session->get(
                CountryMappingView::SESSION_KEY_BACK_URL, ['/country-mapping']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the CountryMapping model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\countrymapping\CountryMappingForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new CountryMappingForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the CountryMappingView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\countrymapping\CountryMappingView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new CountryMappingView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the CountryMapping model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\CountryMapping the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CountryMapping::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param CountryMappingForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(CountryMappingForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param CountryMappingForm $formModel
     * @return string
     */
    private function createTitle(CountryMappingForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Country Mapping');
        } else
        {
            $title = Yii::t('admin', 'Update Country Mapping') . ' ' . $formModel->modelObject->id;
        }

        return $title;
    }

}
