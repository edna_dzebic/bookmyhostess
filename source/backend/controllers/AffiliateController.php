<?php

namespace backend\controllers;

use Yii;
use common\models\Affiliate;
use backend\models\affiliate\AffiliateView;
use backend\models\affiliate\AffiliateForm;
use backend\models\affiliate\AffiliateIndex;
use backend\models\affiliate\AffiliateReport;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

/**
 * AffiliateController implements the CRUD actions for Affiliate model.
 */
class AffiliateController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "Affiliate {} created",
                            'update:POST' => "Affiliate {id} updated",
                            'delete' => "Affiliate {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                AffiliateView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                AffiliateForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all Affiliate models by partner and criteria.
     * @return mixed
     */
    public function actionReport()
    {
        $searchModel = new AffiliateReport(['modelObject' => new Affiliate()]);
        $report = $searchModel->getReport(Yii::$app->request->post());

        return $this->render('report', [
                    'searchModel' => $searchModel,
                    'report' => $report
        ]);
    }

    public function actionPdfReport()
    {
        $searchModel = new AffiliateReport(['modelObject' => new Affiliate()]);
        $report = $searchModel->getReport([]);

        define('_MPDF_TTFONTDATAPATH', Yii::getAlias('@runtime/mpdf'));

        $content = $this->renderPartial('pdf_report', ["report" => $report]);
        $fileName = 'report_' . $report['partner']->identification . '.pdf';

        $pdf = new Pdf([
            'marginTop' => 9,
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_DOWNLOAD,
            'content' => $content,
            'filename' => $fileName,
            'cssFile' => 'css/invoice_pdf.css',
            'cssInline' => file_get_contents(Yii::getAlias('@backend/assets/css/invoice_pdf.css')),
            'options' => ['title' => \Yii::$app->name],
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * Lists all Affiliate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AffiliateIndex(['modelObject' => new Affiliate()]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single Affiliate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new Affiliate model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AffiliateForm(['modelObject' => new Affiliate()]);

        return $this->manage($model, Yii::t('admin', 'Affiliate successfully created'));
    }

    /**
     * Updates an existing Affiliate model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'Affiliate successfully updated'));
    }

    /**
     * Deletes an existing Affiliate model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Affiliate successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Affiliate delete failed'));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Finds the Affiliate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\affiliate\AffiliateForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new AffiliateForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the AffiliateView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\affiliate\AffiliateView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new AffiliateView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Affiliate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\Affiliate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Affiliate::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param AffiliateForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(AffiliateForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param AffiliateForm $formModel
     * @return string
     */
    private function createTitle(AffiliateForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Affiliate');
        } else
        {
            $title = Yii::t('admin', 'Update Affiliate') . ' ' . $formModel->modelObject->name;
        }

        return $title;
    }

}
