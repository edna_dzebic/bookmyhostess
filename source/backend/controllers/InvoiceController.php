<?php

namespace backend\controllers;

use Yii;
use common\models\Invoice;
use backend\models\invoice\InvoiceView;
use backend\models\invoice\InvoiceIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

/**
 * InvoiceController implements the CRUD actions for Invoice model.
 */
class InvoiceController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'reverse:GET' => "Invoice {id} reversed",
                            'mark-as-paid:GET' => "Invoice {id} marked as paid",
                            'enable-reminder:GET' => "Invoice {id} reminder enabled",
                            'disable-reminder:GET' => "Invoice {id} reminder disabled"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                InvoiceView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvoiceIndex(['modelObject' => new Invoice()]);
        $dataProvider = $searchModel->getDataProvider(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single Invoice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    public function actionMarkAsPaid($id)
    {
        $model = $this->findViewModel($id);

        if ($model->markAsPaid() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Invoice marked as paid!'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while marking invoice as paid!'));
        }

        return $this->redirect(['/invoice/view', "id" => $id]);
    }

    public function actionEnableReminder($id)
    {
        $model = $this->findViewModel($id);

        if ($model->enableReminder() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Payment reminder enabled!'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while enabling reminder!'));
        }

        return $this->redirect(['/invoice/view', "id" => $id]);
    }

    public function actionDisableReminder($id)
    {
        $model = $this->findViewModel($id);

        if ($model->disableReminder() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Payment reminder disabled!'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while disabling reminder!'));
        }

        return $this->redirect(['/invoice/view', "id" => $id]);
    }

    /**
     * Creates reverse invoice file
     * @param integer $id
     * @return mixed
     */
    public function actionReverse($id)
    {
        $model = $this->findViewModel($id);

        if (is_object($model))
        {
            $model->disableReminder();
        }

        return $this->createReverseInvoiceFile($id);
    }

    public function actionDownloadZip()
    {
        $zip = new \ZipArchive();

        $filename = "invoices_" . time() . ".zip";

        $filePath = Yii::$app->params['invoice_export_path'] .
                $filename;

        $zipname = Yii::$app->params['backend_web_path'] .
                $filePath;

        $zip->open($zipname, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $searchModel = new InvoiceIndex(['modelObject' => new Invoice()]);
        $models = $searchModel->search(Yii::$app->request->post());

        $ids = [];
        foreach ($models as $model)
        {
            array_push($ids, $model->id);
        }

        $dir = Yii::$app->params['invoice_save_path'] . 'invoice/';

        $count = 0;

        if ($handle = opendir($dir))
        {
            while (false !== ($entry = readdir($handle)))
            {
                if (
                        $entry != "." &&
                        $entry != ".." &&
                        strstr($entry, '.pdf')
                )
                {
                    $parts = explode("_", $entry);

                    $id = null;
                    if (count($parts) > 0)
                    {
                        $id = $parts[count($parts) - 1];
                    }

                    if (isset($id) && in_array($id, $ids))
                    {
                        $count++;
                        $content = file_get_contents($dir . $entry);
                        $zip->addFromString($entry, $content);
                    }
                }
            }
            closedir($handle);
        }

        $zip->close();

        if (file_exists($zipname))
        {
            $url = Yii::$app->urlManager->createAbsoluteUrl([$filePath]);

            return json_encode([
                "status" => 0,
                "url" => $url
            ]);
        }

        $message = '';
        if ($count == 0)
        {
            $message = Yii::t('admin', 'No files added to archive!');
        } else
        {
            $message = Yii::t('admin', 'Zip archive cannot be created!');
        }

        return json_encode([
            'status' => 1,
            'message' => $message
        ]);
    }

    /**
     * Creates reverse PDF invoice and returns it
     * @param type $invoiceId
     * @return string
     */
    private function createReverseInvoiceFile($invoiceId)
    {
        $model = $this->findModel($invoiceId);

        define('_MPDF_TTFONTDATAPATH', Yii::getAlias('@runtime/mpdf'));

        if (isset($model))
        {
            Yii::$app->language = $model->language;

            $content = $this->renderPartial('reverse_pdf', ["model" => $model]);
            $fileName = 'reverse_invoice_' . $model->id . '.pdf';

            $pdf = new Pdf([
                'marginTop' => 9,
                'mode' => Pdf::MODE_UTF8,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_DOWNLOAD,
                'content' => $content,
                'filename' => $fileName,
                'cssFile' => 'css/invoice_pdf.css',
                'cssInline' => file_get_contents(Yii::getAlias('@backend/assets/css/invoice_pdf.css')),
                'options' => ['title' => \Yii::$app->name],
            ]);

            // return the pdf output as per the destination setting
            return $pdf->render();
        }
    }

    /**
     * Finds the InvoiceView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\invoice\InvoiceView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new InvoiceView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

}
