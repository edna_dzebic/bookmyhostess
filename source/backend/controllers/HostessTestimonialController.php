<?php

namespace backend\controllers;

use Yii;
use common\models\HostessTestimonial;
use backend\models\hostesstestimonial\HostessTestimonialView;
use backend\models\hostesstestimonial\HostessTestimonialForm;
use backend\models\hostesstestimonial\HostessTestimonialIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * HostessTestimonialController implements the CRUD actions for HostessTestimonial model.
 */
class HostessTestimonialController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            //'create:POST' => "HostessTestimonial {} created",
                            'update:POST' => "HostessTestimonial {id} updated",
                            'delete' => "HostessTestimonial {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                HostessTestimonialView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessTestimonialForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all HostessTestimonial models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HostessTestimonialIndex(['modelObject' => new HostessTestimonial()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single HostessTestimonial model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new HostessTestimonial model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HostessTestimonialForm(['modelObject' => new HostessTestimonial()]);

        return $this->manage($model, Yii::t('admin', 'HostessTestimonial successfully created'));
    }

    /**
     * Updates an existing HostessTestimonial model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'HostessTestimonial successfully updated'));
    }

    /**
     * Deletes an existing HostessTestimonial model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'HostessTestimonial successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'HostessTestimonial delete failed'));
        }
        $return = Yii::$app->session->get(
                HostessTestimonialView::SESSION_KEY_BACK_URL, ['/hostess-testimonial']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the HostessTestimonial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\hostesstestimonial\HostessTestimonialForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new HostessTestimonialForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the HostessTestimonialView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\hostesstestimonial\HostessTestimonialView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new HostessTestimonialView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the HostessTestimonial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\HostessTestimonial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HostessTestimonial::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param HostessTestimonialForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(HostessTestimonialForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param HostessTestimonialForm $formModel
     * @return string
     */
    private function createTitle(HostessTestimonialForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Hostess Testimonial');
        } else
        {
            $title = Yii::t('admin', 'Update Hostess Testimonial') . ' ' . $formModel->modelObject->id;
        }

        return $title;
    }

}
