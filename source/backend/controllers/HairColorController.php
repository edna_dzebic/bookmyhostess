<?php

namespace backend\controllers;

use Yii;
use common\models\HairColor;
use backend\models\haircolor\HairColorView;
use backend\models\haircolor\HairColorForm;
use backend\models\haircolor\HairColorIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * HairColorController implements the CRUD actions for HairColor model.
 */
class HairColorController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "HairColor {} created",
                            'update:POST' => "HairColor {id} updated",
                            'delete' => "HairColor {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                HairColorView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HairColorForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                HairColorForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all HairColor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HairColorIndex(['modelObject' => new HairColor()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single HairColor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new HairColor model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HairColorForm(['modelObject' => new HairColor()]);

        return $this->manage($model, Yii::t('admin', 'HairColor successfully created'));
    }

    /**
     * Updates an existing HairColor model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'HairColor successfully updated'));
    }

    /**
     * Deletes an existing HairColor model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'HairColor successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'HairColor delete failed'));
        }
        $return = Yii::$app->session->get(
                HairColorView::SESSION_KEY_BACK_URL, ['/hair-color']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the HairColor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\haircolor\HairColorForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new HairColorForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the HairColorView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\haircolor\HairColorView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new HairColorView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the HairColor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\HairColor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HairColor::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param HairColorForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(HairColorForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param HairColorForm $formModel
     * @return string
     */
    private function createTitle(HairColorForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Hair Color');
        } else
        {
            $title = Yii::t('admin', 'Update Hair Color') . ' ' . $formModel->modelObject->name;
        }

        return $title;
    }

}
