<?php

namespace backend\controllers;

use backend\components\Controller;
use backend\models\dashboard\Hostess;
use backend\models\dashboard\Exhibitor;
use backend\models\dashboard\Invoice;
use backend\models\dashboard\Request;

class DashboardController extends Controller
{
    public function actionIndex()
    {
        $hostess = new Hostess();
        $exhibitor = new Exhibitor();

        return $this->render("index", [
                    "hostess" => $hostess,
                    "exhibitor" => $exhibitor
        ]);
    }

    public function actionRequestNumberThisMonth()
    {
        $model = new Request();
        $data = $model->getAmountThisMonth();

        return json_encode($data);
    }

    public function actionInvoiceAmountThisMonth()
    {
        $model = new Invoice();
        $data = $model->getAmountThisMonth();

        return json_encode($data);
    }

    public function actionMonthlyExhibitorRegistration()
    {
        $model = new Exhibitor();
        $data = $model->getRegistrationsPerMonth();

        return json_encode($data);
    }

    public function actionMonthlyHostessRegistration()
    {
        $model = new Hostess();
        $data = $model->getRegistrationsPerMonth();

        return json_encode($data);
    }

    public function actionPriceClass()
    {
        $model = new Hostess();
        $data = $model->getPriceClassForChart();

        return json_encode($data);
    }

}
