<?php

namespace backend\controllers;

use Yii;
use common\models\Type;
use backend\models\type\TypeView;
use backend\models\type\TypeForm;
use backend\models\type\TypeIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * TypeController implements the CRUD actions for Type model.
 */
class TypeController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "Type {} created",
                            'update:POST' => "Type {id} updated",
                            'delete' => "Type {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                TypeView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                TypeForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                TypeForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all Type models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TypeIndex(['modelObject' => new Type()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single Type model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new Type model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TypeForm(['modelObject' => new Type()]);

        return $this->manage($model, Yii::t('admin', 'Type successfully created'));
    }

    /**
     * Updates an existing Type model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'Type successfully updated'));
    }

    /**
     * Deletes an existing Type model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Type successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Type delete failed'));
        }
        $return = Yii::$app->session->get(
                TypeView::SESSION_KEY_BACK_URL, ['/type']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\type\TypeForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new TypeForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the TypeView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\type\TypeView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new TypeView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\Type the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Type::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param TypeForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(TypeForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param TypeForm $formModel
     * @return string
     */
    private function createTitle(TypeForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Type');
        } else
        {
            $title = Yii::t('admin', 'Update Type') . ' ' . $formModel->modelObject->name;
        }

        return $title;
    }

}
