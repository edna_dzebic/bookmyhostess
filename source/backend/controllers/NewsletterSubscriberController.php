<?php

namespace backend\controllers;

use Yii;
use common\models\NewsletterSubscriber;
use backend\models\newslettersubscriber\NewsletterSubscriberView;
use backend\models\newslettersubscriber\NewsletterSubscriberForm;
use backend\models\newslettersubscriber\NewsletterSubscriberIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * NewsletterSubscriberController implements the CRUD actions for NewsletterSubscriber model.
 */
class NewsletterSubscriberController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "NewsletterSubscriber {} created",
                            'update:POST' => "NewsletterSubscriber {id} updated",
                            'delete' => "NewsletterSubscriber {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                NewsletterSubscriberView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                NewsletterSubscriberForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                NewsletterSubscriberForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all NewsletterSubscriber models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsletterSubscriberIndex(['modelObject' => new NewsletterSubscriber()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single NewsletterSubscriber model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new NewsletterSubscriber model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsletterSubscriberForm(['modelObject' => new NewsletterSubscriber()]);

        return $this->manage($model, Yii::t('admin', 'NewsletterSubscriber successfully created'));
    }

    /**
     * Updates an existing NewsletterSubscriber model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'NewsletterSubscriber successfully updated'));
    }

    /**
     * Deletes an existing NewsletterSubscriber model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'NewsletterSubscriber successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'NewsletterSubscriber delete failed'));
        }
        $return = Yii::$app->session->get(
                NewsletterSubscriberView::SESSION_KEY_BACK_URL, ['/newsletter-subscriber']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the NewsletterSubscriber model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\newslettersubscriber\NewsletterSubscriberForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new NewsletterSubscriberForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the NewsletterSubscriberView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\newslettersubscriber\NewsletterSubscriberView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new NewsletterSubscriberView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the NewsletterSubscriber model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\NewsletterSubscriber the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsletterSubscriber::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param NewsletterSubscriberForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(NewsletterSubscriberForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param NewsletterSubscriberForm $formModel
     * @return string
     */
    private function createTitle(NewsletterSubscriberForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Newsletter Subscriber');
        } else
        {
            $title = Yii::t('admin', 'Update Newsletter Subscriber') . ' ' . $formModel->modelObject->id;
        }

        return $title;
    }

}
