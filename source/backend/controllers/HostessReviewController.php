<?php

namespace backend\controllers;

use Yii;
use common\models\HostessReview;
use backend\models\hostessreview\HostessReviewView;
use backend\models\hostessreview\HostessReviewForm;
use backend\models\hostessreview\HostessReviewIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * HostessReviewController implements the CRUD actions for HostessReview model.
 */
class HostessReviewController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "HostessReview created",
                            'update:POST' => "HostessReview {id} updated",
                            'delete' => "HostessReview {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                HostessReviewView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                HostessReviewForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all HostessReview models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HostessReviewIndex(['modelObject' => new HostessReview()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single HostessReview model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new HostessReview model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HostessReviewForm(['modelObject' => new HostessReview()]);

        return $this->manage($model, Yii::t('admin', 'HostessReview successfully created'));
    }

    /**
     * Updates an existing HostessReview model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'HostessReview successfully updated'));
    }

    /**
     * Deletes an existing HostessReview model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'HostessReview successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'HostessReview delete failed'));
        }
        // todo change all of redirects after delete!!!
        $return = Yii::$app->session->get(
                HostessReviewView::SESSION_KEY_BACK_URL, ['/hostess-review']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the HostessReview model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\hostessreview\HostessReviewForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new HostessReviewForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the HostessReviewView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\hostessreview\HostessReviewView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new HostessReviewView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the HostessReview model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\HostessReview the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HostessReview::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param HostessReviewForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(HostessReviewForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->loadAndSave(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param HostessReviewForm $formModel
     * @return string
     */
    private function createTitle(HostessReviewForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Hostess Review');
        } else
        {
            $title = Yii::t('admin', 'Update Hostess Review') . ' ' . $formModel->modelObject->id;
        }

        return $title;
    }

}
