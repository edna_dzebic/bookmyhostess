<?php

namespace backend\controllers;

use Yii;
use common\models\Event;
use backend\models\event\EventView;
use backend\models\event\EventForm;
use backend\models\event\EventIndex;
use backend\models\event\EventJobForm;
use backend\models\event\EventDressCodeForm;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "Event created",
                            'update:POST' => "Event {id} updated",
                            'clone:POST' => "Event {id} cloned",
                            'add-job:POST' => "New job for event {id} added",
                            'delete-job' => "Job for event {id} deleted",
                            'add-dress-code:POST' => "New dress code for event {id} added",
                            'delete-dress-code' => "Dress code for event {id} deleted",
                            'delete' => "Event {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                EventView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                EventForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'past' => [
                                EventView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                EventForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'upcoming' => [
                                EventView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                EventForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                EventForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                EventJobForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                EventDressCodeForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventIndex(['modelObject' => new Event()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Lists all past Event models.
     * @return mixed
     */
    public function actionPast()
    {
        $searchModel = new EventIndex(['modelObject' => new Event()]);
        $dataProvider = $searchModel->searchPast(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Lists all upcoming Event models.
     * @return mixed
     */
    public function actionUpcoming()
    {
        $searchModel = new EventIndex(['modelObject' => new Event()]);
        $dataProvider = $searchModel->searchUpcoming(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EventForm(['modelObject' => new Event()]);

        return $this->manage($model, Yii::t('admin', 'Event successfully created'), 'create');
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'Event successfully updated'), 'update');
    }

    /**
     * Clones an existing Event model.
     * If clone is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionClone($id)
    {
        $model = $this->getClonedModel($id);

        Yii::$app->session->set(EventForm::SESSION_KEY_BACK_URL, ['index']);

        return $this->manage($model, Yii::t('admin', 'Event successfully copied as new'), 'clone');
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Event successfully deleted'));
            $return = Yii::$app->session->get(
                    EventView::SESSION_KEY_BACK_URL, ['/event']
            );
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Event delete failed with errors ' . $errors));
            $return = $model->getBackUrl();
        }


        return $this->redirect($return);
    }

    public function actionAddJob($id)
    {
        $event = $this->findModel($id);

        $model = new EventJobForm(['modelObject' => $event]);

        if (Yii::$app->request->isPost && $model->loadAndSave(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Job successfully added'));

            return $this->redirect($model->getBackUrl());
        }

        return $this->render('add_job', [
                    'model' => $model,
                    'title' => Yii::t('admin', 'Add job for ' . $event->name)
                        ]
        );
    }

    public function actionDeleteJob($event_id, $job_id)
    {
        $event = $this->findModel($event_id);

        $model = new EventJobForm(['modelObject' => $event, 'event_id' => $event_id, 'job_id' => $job_id]);

        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Job successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Job delete failed'));
        }

        return $this->redirect($model->getBackUrl());
    }

    public function actionAddDressCode($id)
    {
        $event = $this->findModel($id);

        $model = new EventDressCodeForm(['modelObject' => $event]);

        if (Yii::$app->request->isPost && $model->loadAndSave(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Dress code successfully added'));

            return $this->redirect($model->getBackUrl());
        }

        return $this->render('add_dress_code', [
                    'model' => $model,
                    'title' => Yii::t('admin', 'Add dress code for ' . $event->name)
                        ]
        );
    }

    public function actionDeleteDressCode($event_id, $code_id)
    {
        $event = $this->findModel($event_id);

        $model = new EventDressCodeForm(['modelObject' => $event, 'event_id' => $event_id, 'event_dress_code_id' => $code_id]);

        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Dress code successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Dress code delete failed'));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\event\EventForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new EventForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the EventView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\event\EventView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new EventView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param EventForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(EventForm $model, $message, $action)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);
            
            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model, $action);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param EventForm $formModel
     * @param string $action
     * @return string
     */
    private function createTitle(EventForm $formModel, $action)
    {
        if ($action === 'clone')
        {
            $title = Yii::t('admin', 'Copy Event') . ' ' . $formModel->modelObject->name;
        } else
        {
            if ($formModel->modelObject->isNewRecord)
            {
                $title = Yii::t('admin', 'Create Event');
            } else
            {
                $title = Yii::t('admin', 'Update Event') . ' ' . $formModel->modelObject->name;
            }
        }



        return $title;
    }

    /**
     * Create event model with all attributes from existing event
     * @param integer $id
     * @return EventForm
     */
    private function getClonedModel($id)
    {
        $model = $this->findModel($id);

        $newModel = new Event();
        $newModel->setAttributes($model->getAttributes());
        $newModel->parent_id = $id;
        $newModel->status_id = Yii::$app->status->active;
        unset($newModel->id);

        return new EventForm(['modelObject' => $newModel]);
    }

}
