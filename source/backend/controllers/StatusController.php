<?php

namespace backend\controllers;

use Yii;
use common\models\Status;
use backend\models\status\StatusIndex;
use yii\helpers\ArrayHelper;

/**
 * StatusController implements the CRUD actions for Status model.
 */
class StatusController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), []);
    }

    /**
     * Lists all Status models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StatusIndex(['modelObject' => new Status]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

}
