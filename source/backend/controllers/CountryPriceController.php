<?php

namespace backend\controllers;

use Yii;
use common\models\CountryPrice;
use backend\models\countryprice\CountryPriceView;
use backend\models\countryprice\CountryPriceForm;
use backend\models\countryprice\CountryPriceIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * CountryPriceController implements the CRUD actions for CountryPrice model.
 */
class CountryPriceController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "CountryPrice {} created",
                            'update:POST' => "CountryPrice {id} updated",
                            'delete' => "CountryPrice {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                CountryPriceView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                CountryPriceForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                CountryPriceForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all CountryPrice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CountryPriceIndex(['modelObject' => new CountryPrice()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single CountryPrice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new CountryPrice model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CountryPriceForm(['modelObject' => new CountryPrice()]);

        return $this->manage($model, Yii::t('admin', 'CountryPrice successfully created'));
    }

    /**
     * Updates an existing CountryPrice model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'CountryPrice successfully updated'));
    }

    /**
     * Deletes an existing CountryPrice model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'CountryPrice successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'CountryPrice delete failed'));
        }

        $return = Yii::$app->session->get(
                CountryPriceView::SESSION_KEY_BACK_URL, ['/country-price']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the CountryPrice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\countryprice\CountryPriceForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new CountryPriceForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the CountryPriceView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\countryprice\CountryPriceView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new CountryPriceView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the CountryPrice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\CountryPrice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CountryPrice::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param CountryPriceForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(CountryPriceForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param CountryPriceForm $formModel
     * @return string
     */
    private function createTitle(CountryPriceForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Country Price');
        } else
        {
            $title = Yii::t('admin', 'Update Country Price') . ' ' . $formModel->modelObject->id;
        }

        return $title;
    }

}
