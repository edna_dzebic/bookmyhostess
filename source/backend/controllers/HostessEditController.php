<?php

namespace backend\controllers;

use Yii;
use common\models\HostessPastWork;
use common\models\UserDocuments;
use common\models\UserImages;
use common\models\HostessPreferredCity;
use common\models\HostessPreferredJob;
use common\models\HostessLanguage;
use common\models\HostessCountryMapping;
use common\models\HostessReview;
use common\models\Request;
use backend\models\User;
use backend\models\hostess\form\DisapproveForm;
use backend\models\hostess\form\HostessForm;
use backend\models\hostess\form\ProfileForm;
use backend\models\hostess\form\EducationForm;
use backend\models\hostess\form\WorkExperienceForm;
use backend\models\hostess\form\BillingForm;
use backend\models\hostess\form\ImageForm;
use backend\models\hostess\form\DocumentForm;
use backend\models\hostess\form\LanguageForm;
use backend\models\hostess\form\CityForm;
use backend\models\hostess\form\JobForm;
use backend\models\hostess\form\CountryMappingForm;
use backend\models\hostess\form\ReviewForm;
use backend\models\hostess\form\HostessFormInterface;
use backend\models\hostess\HostessView;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * HostessEditController implements the update and delete actions for Hostess model.
 */
class HostessEditController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'add-review' => 'Review added for profile {userId}',
                            'update-review' => 'Review {reviewId} for profile {userId} updated',
                            'delete-review' => 'Review {reviewId} deleted for profile {userId}',
                            'add-country-mapping' => 'Country added for profile {userId}',
                            'delete-country-mapping' => 'Country deleted for profile {userId}',
                            'update' => 'Profile {userId} updated',
                            'update-billing' => 'Billing details for profile {userId} updated',
                            'update-education' => 'Education for {userId} updated',
                            'add-language' => 'Language added for {userId}',
                            'update-language' => 'Language {lngId} updated',
                            'delete-language' => 'Language {lngId} deleted',
                            'add-image' => 'Image added for {userId}',
                            'set-image-as-default' => 'Image {imageId} set as default',
                            'delete-image' => 'Image {imageId} updated',
                            'add-job' => 'Job added for {userId}',
                            'delete-job' => 'Job {jobId} deleted',
                            'add-city' => 'City added for {userId}',
                            'delete-city' => 'City {cityId} deleted',
                            'add-document' => 'Document added for {userId}',
                            'delete-document' => 'Document {documentId} deleted',
                            'add-experience' => 'Experience added for {userId}',
                            'update-experience' => 'Experience {expId} updated',
                            'delete-experience' => 'Experience {expId} deleted',
                            'delete' => "Hostess {id} deleted",
                            'disable' => "Hostess {id} disabled",
                            'enable' => "Hostess {id} enabled",
                            'approve' => "Hostess {id} approved",
                            'disapprove' => "Hostess {id} disapproved",
                        ]
                    ]
        ]);
    }

    public function actionAddReview($userId, $requestId = null)
    {
        $userModel = $this->findModel($userId);
        $requestModel = null;

        if (isset($requestId))
        {
            $requestModel = $this->findRequestModel($requestId);
        }

        $formModel = new ReviewForm([
            'hostess_id' => $userId,
            'modelObject' => $userModel,
            'reviewModel' => new HostessReview(),
            'requestModel' => $requestModel
        ]);

        $view = 'review';
        $title = Yii::t('admin', 'Add review');
        $message = Yii::t('admin', 'Review added successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionUpdateReview($userId, $reviewId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new ReviewForm([
            'hostess_id' => $userId,
            'modelObject' => $userModel,
            'reviewModel' => $this->findReviewModel($userId, $reviewId)
        ]);

        $view = 'review';
        $title = Yii::t('admin', 'Update review');
        $message = Yii::t('admin', 'Review updated successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionDeleteReview($userId, $reviewId)
    {
        $userModel = $this->findModel($userId);

        $review = $this->findReviewModel($userId, $reviewId);

        if ($review->delete() !== false)
        {
            if (is_object($userModel->hostessProfile))
            {
                $userModel->hostessProfile->recalculateRating();
            }

            Yii::$app->session->setFlash('success', Yii::t('admin', 'Review deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while deleting review'));
        }

        return $this->redirect(['/hostess/view', "id" => $userId]);
    }

    public function actionAddCountryMapping($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new CountryMappingForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'countryMappingModel' => new HostessCountryMapping()
        ]);

        $view = 'country_mapping';
        $title = Yii::t('admin', 'Add Country Mapping');
        $message = Yii::t('admin', 'Country mapping added successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionDeleteCountryMapping($userId, $mappingId)
    {
        $model = $this->findCountryMappingModel($userId, $mappingId);

        if ($model->delete() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Country mapping deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while deleting country mapping'));
        }

        return $this->redirect(['/hostess/view', "id" => $userId]);
    }

    public function actionUpdateBilling($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new BillingForm([
            'user_id' => $userId,
            'modelObject' => $userModel
        ]);

        $view = 'billing';
        $title = Yii::t('admin', 'Update billing details');
        $message = Yii::t('admin', 'Billing details updated successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionUpdate($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new ProfileForm([
            'user_id' => $userId,
            'modelObject' => $userModel
        ]);

        $view = 'profile';
        $title = Yii::t('admin', 'Update hostess');
        $message = Yii::t('admin', 'Profile updated successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionUpdateEducation($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new EducationForm([
            'user_id' => $userId,
            'modelObject' => $userModel
        ]);

        $view = 'education';
        $title = Yii::t('admin', 'Update hostess education');
        $message = Yii::t('admin', 'Education updated successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionAddLanguage($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new LanguageForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'languageModel' => new HostessLanguage()
        ]);

        $view = 'language';
        $title = Yii::t('admin', 'Add language');
        $message = Yii::t('admin', 'Language added successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionUpdateLanguage($userId, $lngId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new LanguageForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'languageModel' => $this->findLanguageModel($userId, $lngId)
        ]);

        $view = 'language';
        $title = Yii::t('admin', 'Update language');
        $message = Yii::t('admin', 'Language updated successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionDeleteLanguage($userId, $lngId)
    {
        $lng = $this->findLanguageModel($userId, $lngId);

        if ($lng->delete() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Language deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while deleting language'));
        }

        return $this->redirect(['/hostess/view', "id" => $userId]);
    }

    public function actionAddJob($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new JobForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'jobModel' => new HostessPreferredJob()
        ]);

        $view = 'preferred_job';
        $title = Yii::t('admin', 'Add job');
        $message = Yii::t('admin', 'Job added successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionDeleteJob($userId, $jobId)
    {
        $job = $this->findJobModel($userId, $jobId);

        if ($job->delete() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Job deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while deleting job'));
        }

        return $this->redirect(['/hostess/view', "id" => $userId]);
    }

    public function actionAddCity($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new CityForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'cityModel' => new HostessPreferredCity()
        ]);

        $view = 'preferred_city';
        $title = Yii::t('admin', 'Add city');
        $message = Yii::t('admin', 'City added successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionDeleteCity($userId, $cityId)
    {
        $city = $this->findCityModel($userId, $cityId);

        if ($city->delete() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'City deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while deleting city'));
        }

        return $this->redirect(['/hostess/view', "id" => $userId]);
    }

    public function actionAddImage($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new ImageForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'imageModel' => new UserImages()
        ]);

        $view = 'image';
        $title = Yii::t('admin', 'Add image');
        $message = Yii::t('admin', 'Image added successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionSetImageAsDefault($userId, $imageId)
    {
        $userModel = $this->findModel($userId);

        $image = $this->findImageModel($userId, $imageId);

        $formModel = new ImageForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'imageModel' => $image
        ]);

        if ($formModel->setAsDefault($image))
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Image updated'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while updating image'));
        }

        return $this->redirect(['/hostess/view', "id" => $userId]);
    }

    public function actionDeleteImage($userId, $imageId)
    {
        $image = $this->findImageModel($userId, $imageId);

        if ($image->delete() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Image deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while deleting image'));
        }

        return $this->redirect(['/hostess/view', "id" => $userId]);
    }

    public function actionAddDocument($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new DocumentForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'documentModel' => new UserDocuments()
        ]);

        $view = 'document';
        $title = Yii::t('admin', 'Add document');
        $message = Yii::t('admin', 'Document added successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionDeleteDocument($userId, $documentId)
    {
        $document = $this->findDocumentModel($userId, $documentId);

        if ($document->delete() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Document deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while deleting document'));
        }

        return $this->redirect(['/hostess/view', "id" => $userId]);
    }

    public function actionAddExperience($userId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new WorkExperienceForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'workModel' => new HostessPastWork()
        ]);

        $view = 'work_experience';
        $title = Yii::t('admin', 'Add hostess experience');
        $message = Yii::t('admin', 'Experience added successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionUpdateExperience($userId, $expId)
    {
        $userModel = $this->findModel($userId);

        $formModel = new WorkExperienceForm([
            'user_id' => $userId,
            'modelObject' => $userModel,
            'workModel' => $this->findExperienceModel($userId, $expId)
        ]);

        $view = 'work_experience';
        $title = Yii::t('admin', 'Update hostess experience');
        $message = Yii::t('admin', 'Experience updated successfully');

        return $this->displayForm($formModel, $view, $title, $message);
    }

    public function actionDeleteExperience($userId, $expId)
    {
        $experience = $this->findExperienceModel($userId, $expId);

        if ($experience->delete() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Experience deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while deleting experience'));
        }

        return $this->redirect(['/hostess/view', "id" => $userId]);
    }

    /**
     * Approve hostess
     * @param integer $id
     * @return mixed
     */
    public function actionApprove($id)
    {
        $userModel = $this->findModel($id);

        $model = new HostessView([
            'modelObject' => $userModel,
            'profileObject' => $userModel->hostessProfile
        ]);

        if ($model->approve() !== false)
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'You have approved hostess!'), true);
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Error while approving hostess. Errors are ' . $errors));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Disapprove hostess
     * @param integer $id
     * @return mixed
     */
    public function actionDisapprove($id)
    {
        $userModel = $this->findModel($id);

        $model = new DisapproveForm([
            'user_id' => $id,
            'modelObject' => $userModel
        ]);

        $data = [
            'status_code' => 0,
            'view' => ''
        ];

        if (Yii::$app->request->isPost)
        {
            if ($model->loadAndValidate(Yii::$app->request->post()) && $model->disapprove())
            {
                Yii::$app->session->setFlash('success', Yii::t('admin', 'You have disapproved hostess!'), true);

                $return = Yii::$app->session->get(
                        HostessView::SESSION_KEY_BACK_URL, ['/hostess']
                );
                return $this->redirect($return);
            }
        }

        $data["view"] = $this->renderPartial("/hostess/forms/_disapprove_form", [
            "model" => $model,
            "user_id" => $userModel->id
        ]);

        return Json::encode($data);
    }

    /**
     * Deletes an existing Hostess model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Hostess successfully deleted'));
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Hostess delete failed with errors ' . $errors));
        }
        
        $return = Yii::$app->session->get(
                HostessView::SESSION_KEY_BACK_URL, ['/hostess']
        );
        
        return $this->redirect($return);
    }

    /**
     * Activate an existing Hostess model.
     * If action is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionActivate($id)
    {
        $model = $this->findFormModel($id);
        if ($model->activate())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Hostess successfully activated'));
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Hostess activation failed with errors ' . $errors));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Disable an existing Hostess model.
     * If action is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDisable($id)
    {
        $model = $this->findFormModel($id);
        if ($model->disable())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Hostess successfully disabled'));
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Hostess disabled failed with errors ' , $errors));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Enable an existing Hostess model.
     * If action is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEnable($id)
    {
        $model = $this->findFormModel($id);
        if ($model->enable())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Hostess successfully enabled'));
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Hostess enable failed with errors ' . $errors));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Finds the HostessForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\exhibitor\HostessForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new HostessForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Hostess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findHostess($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Display specific form for specific operation
     * @param $model
     * @param string $view
     * @param string $title
     * @return mixed
     */
    private function displayForm(HostessFormInterface $model, $view, $title, $message)
    {
        if (Yii::$app->request->isPost)
        {
            if ($model->loadAndSave(Yii::$app->request->post()))
            {
                // if anything has been changed recalculate everything
                $model->getProfileModel()->recalculateProfileCompleteness();

                Yii::$app->session->setFlash('success', $message);
                return $this->redirect($model->getBackUrl());
            }
        }


        return $this->render('/hostess/forms/' . $view, [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * 
     * @param integer $userId
     * @param integer $expId
     * @return type
     * @throws NotFoundHttpException
     */
    private function findExperienceModel($userId, $expId)
    {

        if (($model = HostessPastWork::findOne(["user_id" => $userId, "id" => $expId])) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * 
     * @param integer $userId
     * @param integer $docId
     * @return type
     * @throws NotFoundHttpException
     */
    private function findDocumentModel($userId, $docId)
    {

        if (($model = UserDocuments::findOne(["user_id" => $userId, "id" => $docId])) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * 
     * @param integer $userId
     * @param integer $imgId
     * @return type
     * @throws NotFoundHttpException
     */
    private function findImageModel($userId, $imgId)
    {

        if (($model = UserImages::findOne(["user_id" => $userId, "id" => $imgId])) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * 
     * @param integer $userId
     * @param integer $reviewId
     * @return HostessReview
     * @throws NotFoundHttpException
     */
    private function findReviewModel($userId, $reviewId)
    {

        if (($model = HostessReview::findOne(["hostess_id" => $userId, "id" => $reviewId])) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * 
     * @param integer $userId
     * @param integer $lngId
     * @return HostessLanguage
     * @throws NotFoundHttpException
     */
    private function findLanguageModel($userId, $lngId)
    {

        if (($model = HostessLanguage::findOne(["user_id" => $userId, "id" => $lngId])) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * 
     * @param integer $userId
     * @param integer $jobId
     * @return type
     * @throws NotFoundHttpException
     */
    private function findJobModel($userId, $jobId)
    {

        if (($model = HostessPreferredJob::findOne(["user_id" => $userId, "id" => $jobId])) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * 
     * @param integer $userId
     * @param integer $cityId
     * @return type
     * @throws NotFoundHttpException
     */
    private function findCityModel($userId, $cityId)
    {
        if (($model = HostessPreferredCity::findOne(["user_id" => $userId, "id" => $cityId])) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * 
     * @param integer $userId
     * @param integer $mappingId
     * @return type
     * @throws NotFoundHttpException
     */
    private function findCountryMappingModel($userId, $mappingId)
    {
        if (($model = HostessCountryMapping::findOne([
                    "user_id" => $userId,
                    "id" => $mappingId
                ])) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    private function findRequestModel($id)
    {
        if (($model = Request::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }
}
