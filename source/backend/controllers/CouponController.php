<?php

namespace backend\controllers;

use Yii;
use common\models\Coupon;
use backend\models\coupon\CouponView;
use backend\models\coupon\CouponForm;
use backend\models\coupon\CouponIndex;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * CouponController implements the CRUD actions for Coupon model.
 */
class CouponController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "Coupon {} created",
                            'update:POST' => "Coupon {id} updated",
                            'delete' => "Coupon {id} deleted"
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                CouponView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                CouponForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                CouponForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Lists all Coupon models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CouponIndex(['modelObject' => new Coupon()]);
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

    /**
     * Displays a single Coupon model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new Coupon model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CouponForm(['modelObject' => new Coupon()]);

        return $this->manage($model, Yii::t('admin', 'Coupon successfully created'));
    }

    /**
     * Updates an existing Coupon model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'Coupon successfully updated'));
    }

    /**
     * Deletes an existing Coupon model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Coupon successfully deleted'));
        } else
        {
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Coupon delete failed'));
        }
        $return = Yii::$app->session->get(
                CouponView::SESSION_KEY_BACK_URL, ['/coupon']
        );
        return $this->redirect($return);
    }

    /**
     * Finds the Coupon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\coupon\CouponForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new CouponForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the CouponView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\coupon\CouponView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new CouponView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Coupon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\Coupon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Coupon::findOne($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param CouponForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(CouponForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param CouponForm $formModel
     * @return string
     */
    private function createTitle(CouponForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Coupon');
        } else
        {
            $title = Yii::t('admin', 'Update Coupon') . ' ' . $formModel->modelObject->title;
        }

        return $title;
    }

}
