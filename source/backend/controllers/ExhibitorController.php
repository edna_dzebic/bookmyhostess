<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\exhibitor\ExhibitorIndex;
use backend\models\exhibitor\ExhibitorForm;
use backend\models\exhibitor\ExhibitorView;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * ExhibitorController implements the CRUD actions for Exhibitor model.
 */
class ExhibitorController extends \backend\components\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
                    'audit' => [
                        'class' => \common\components\AuditBehavior::className(),
                        'actions' => [
                            'create:POST' => "Exhibitor {} created",
                            'update:POST' => "Exhibitor {id} updated",
                            'activate' => "Exhibitor {id} activated",
                            'delete' => "Exhibitor {id} deleted",
                            'disable' => "Exhibitor {id} disabled",
                            'enable' => "Exhibitor {id} enabled",
                        ]
                    ],
                    'session' => [
                        'class' => \common\components\SessionBehavior::className(),
                        'actions' => [
                            'index' => [
                                ExhibitorView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ExhibitorForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'pending' => [
                                ExhibitorView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ExhibitorForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'active' => [
                                ExhibitorView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ExhibitorForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'disabled' => [
                                ExhibitorView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ExhibitorForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'deleted' => [
                                ExhibitorView::SESSION_KEY_BACK_URL => \yii\helpers\Url::current(),
                                ExhibitorForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ],
                            'view' => [
                                ExhibitorForm::SESSION_KEY_BACK_URL => \yii\helpers\Url::current()
                            ]
                        ]
                    ]
        ]);
    }

    /**
     * Search all exhibitors
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->renderIndex("searchAll");
    }

    /**
     * Search exhibitors with active profile
     * @return mixed
     */
    public function actionActive()
    {
        return $this->renderIndex("searchActive");
    }

    /**
     * Search newly registered exhibitors
     * @return mixed
     */
    public function actionPending()
    {
        return $this->renderIndex("searchPending");
    }

    /**
     * Search exhibitors that have been blocked
     *
     * @return string
     */
    public function actionDisabled()
    {
        return $this->renderIndex("searchDisabled");
    }

    /**
     * Search exhibitors that have been deleted
     *
     * @return string
     */
    public function actionDeleted()
    {
        return $this->renderIndex("searchDeleted");
    }

    /**
     * Displays a single Exhibitor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findViewModel($id),
        ]);
    }

    /**
     * Creates a new Exhibitor model.
     * If creation is successful, the browser will be redirected to the 'previous' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ExhibitorForm(['modelObject' => new User()]);
        $model->setScenario('create');

        return $this->manage($model, Yii::t('admin', 'Exhibitor successfully created'));
    }

    /**
     * Updates an existing Exhibitor model.
     * If update is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFormModel($id);

        return $this->manage($model, Yii::t('admin', 'Exhibitor successfully updated'));
    }

    /**
     * Deletes an existing Exhibitor model.
     * If deletion is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findFormModel($id);
        if ($model->delete())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Exhibitor successfully deleted'));
            $return = Yii::$app->session->get(
                    ExhibitorView::SESSION_KEY_BACK_URL, ['/exhibitor']
            );
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Exhibitor delete failed with errors ' . $errors));
            $return = $model->getBackUrl();
        }

        return $this->redirect($return);
    }

    /**
     * Disable an existing Exhibitor model.
     * If action is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDisable($id)
    {
        $model = $this->findFormModel($id);
        if ($model->disable())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Exhibitor successfully disabled'));
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Exhibitor disable failed with errors ' . $errors));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Enable an existing Exhibitor model.
     * If action is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEnable($id)
    {
        $model = $this->findFormModel($id);
        if ($model->enable())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Exhibitor successfully enabled'));
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Exhibitor enable failed with errors ' . $errors));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Activate an existing Exhibitor model.
     * If action is successful, the browser will be redirected to the 'previous' page.
     * @param integer $id
     * @return mixed
     */
    public function actionActivate($id)
    {
        $model = $this->findFormModel($id);
        if ($model->activate())
        {
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Exhibitor successfully activated'));
        } else
        {
            $errors = var_export($model->getErrors(), true);
            Yii::$app->session->setFlash('error', Yii::t('admin', 'Exhibitor activation failed with errors ' . $errors));
        }

        return $this->redirect($model->getBackUrl());
    }

    /**
     * Finds the ExhibitorForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\exhibitor\ExhibitorForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFormModel($id)
    {
        return new ExhibitorForm(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the ExhibitorView model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \backend\models\exhibitor\ExhibitorView the loaded view model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findViewModel($id)
    {
        return new ExhibitorView(['modelObject' => $this->findModel($id)]);
    }

    /**
     * Finds the Exhibitor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findExhibitor($id)) !== null)
        {
            return $model;
        } else
        {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
        }
    }

    /**
     * Base function handles update/create
     *
     * @param ExhibitorForm $model
     * @param string $message Flash message to display when successful
     * @return mixed
     */
    private function manage(ExhibitorForm $model, $message)
    {
        if (Yii::$app->request->isPost && $model->save(Yii::$app->request->post()))
        {
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect($model->getBackUrl());
        }

        $title = $this->createTitle($model);

        return $this->render('manage', [
                    'model' => $model,
                    'title' => $title
        ]);
    }

    /**
     * Generate page title
     *
     * @param ExhibitorForm $formModel
     * @return string
     */
    private function createTitle(ExhibitorForm $formModel)
    {
        if ($formModel->modelObject->isNewRecord)
        {
            $title = Yii::t('admin', 'Create Exhibitor');
        } else
        {
            $title = Yii::t('admin', 'Update Exhibitor') . ' ' . $formModel->modelObject->getFullName();
        }

        return $title;
    }

    /**
     * @param $searchFunction
     *
     * @return string
     */
    private function renderIndex($searchFunction)
    {
        $searchModel = new ExhibitorIndex(['modelObject' => new User()]);
        $dataProvider = $searchModel->$searchFunction(Yii::$app->request->post());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'hideSearch' => $searchModel->searchNotEmpty
        ]);
    }

}
