<?php

namespace backend\models\adminUser;

use common\models\User;
use Yii;

/**
 * UserForm represents the model behind the form about `\backend\models\user\User`.
 *
 * @property User $model
 */
class AdminUserForm extends AdminUser
{

    const SESSION_KEY_BACK_URL = 'adminUser.form.backUrl';

    /**
     * Username
     * @var string
     */
    public $username;

    /**
     * Password
     * @var string
     */
    public $password;

    /**
     * Email
     * @var string
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            [['password'], 'required', 'on' => static::SCENARIO_NEW],
            [['password'], 'string', 'min' => 8],
            [['username', 'password', 'email'], 'string', 'max' => 255],
            [
                ['username', 'email'],
                'unique',
                'targetClass' => $this->modelClass,
                'filter' => function ($query)
        {
            $query->andWhere(['not', ['id' => $this->modelObject->id]]);
        }
            ]
        ];
    }

    /**
     * Load and save User
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {

            $this->modelObject->setAttributes($this->attributes);
            $this->modelObject->setPassword($this->password);
            $this->modelObject->role = \common\models\User::ROLE_ADMIN;

            if ($this->modelObject->save() !== false)
            {
                return true;
            }

            $this->addErrors($this->modelObject->getErrors());
            return false;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function disable()
    {
        return $this->modelObject->disable();
    }

    /**
     * @return boolean
     */
    public function enable()
    {
        return $this->modelObject->enable();
    }

    /**
     * Delete model
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

}
