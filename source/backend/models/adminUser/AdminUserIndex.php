<?php

namespace backend\models\adminUser;

use Yii;
use backend\components\CachedSearchTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserIndex represents the model behind the search about `\backend\models\user\User`.
 */
class AdminUserIndex extends AdminUser
{

    use CachedSearchTrait;

    /**
     * Username
     * @var string
     */
    public $username;

    /**
     * Email
     * @var string
     */
    public $email;

    /**
     * Status Id
     * @var string
     */
    public $status_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'safe'],
            [['status_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /* @var ActiveQuery $query */
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $this->addOnlyAdminCondition($query);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['status_id' => $this->status_id]);

        $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'email', $this->email]);
        
        $query->andWhere(
                ["<>", "id", Yii::$app->user->id]);

        return $dataProvider;
    }

}
