<?php

namespace backend\models\adminUser;

use Yii;

/**
 * UserView represents the model behind the view about `\backend\models\user\User`.
 */
class AdminUserView extends AdminUser
{

    const SESSION_KEY_BACK_URL = 'adminUser.view.backUrl';

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Username
     * @var string
     */
    public $username;

    /**
     * Email
     * @var string
     */
    public $email;

    /**
     * Status Id
     * @var string
     */
    public $status_id;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date Updated
     * @var string
     */
    public $date_updated;

    /**
     * Created By
     * @var integer
     */
    public $created_by;

    /**
     * Updated By
     * @var integer
     */
    public $updated_by;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Get header buttons
     * @returns array
     */
    public function getButtons()
    {

        $buttons = [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')],
            'edit_button' => ['class' => 'btn btn-success', 'url' => ['update', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Update')],
            'password_button' => ['class' => 'btn btn-info', 'url' => ['change-password', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Password')],
            'delete_button' => [
                'class' => 'btn btn-danger',
                'url' => ['delete', 'id' => $this->modelObject->id],
                'label' => Yii::t('admin', 'Delete'),
                'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this item?')]
            ]
        ];

        if ($this->modelObject->isDisabled())
        {
            $buttons["enable_button"] = ['class' => 'btn btn-warning', 'url' => ['enable', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Activate')];
        } else
        {
            $buttons["enable_button"] = ['class' => 'btn btn-warning', 'url' => ['disable', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Deactivate')];
        }

        return $buttons;
    }

}
