<?php

namespace backend\models\adminUser;

use Yii;
use yii\db\ActiveQuery;

/**
 * User represents the backend model for `\common\models\User`.
 */
class AdminUser extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\User';

        parent::__construct($config);
    }

    public function getStatusList()
    {
        return [
            Yii::$app->status->active => Yii::t('admin', 'Active'),
            Yii::$app->status->disabled => Yii::t('admin', 'Disabled'),
        ];
    }

    /**
     * @param ActiveQuery $query
     */
    protected function addOnlyAdminCondition(ActiveQuery &$query)
    {
        $query->andWhere(["user.role" => \common\models\User::ROLE_ADMIN]);
    }

}
