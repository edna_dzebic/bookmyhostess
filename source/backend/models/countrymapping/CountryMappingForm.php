<?php

namespace backend\models\countrymapping;

use Yii;
use common\models\CountryMapping as CountryMappingModel;

/**
 * CountryMappingForm represents the model behind the form about `\backend\models\countrymapping\CountryMapping`.
 */
class CountryMappingForm extends \backend\models\countrymapping\CountryMapping
{

    const SESSION_KEY_BACK_URL = 'countrymapping.form.backUrl';

    /**
     * Source Country Id
     * @var integer
     */
    public $source_country_id;

    /**
     * Target Country Id
     * @var integer
     */
    public $target_country_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_country_id', 'target_country_id'], 'required'],
            [['source_country_id', 'target_country_id'], 'integer'],
            ['source_country_id', 'compare', 'compareAttribute' => 'target_country_id', 'operator' => '!=', 'message' => Yii::t('admin', 'Source and target should not be same')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Load and save CountryMapping     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            return $this->modelObject->save();
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        $query = CountryMappingModel::find()
                ->andWhere([
            "source_country_id" => $this->source_country_id,
            "target_country_id" => $this->target_country_id
        ]);

        if (!$this->modelObject->isNewRecord)
        {
            $query->andWhere(["<>", "id", $this->modelObject->id]);
        }

        $found = $query->exists();

        if ($found)
        {
            $this->addError('source_country_id', 'The same mapping already exists');
            $this->addError('target_country_id', 'The same mapping already exists');
        }

        return !$this->hasErrors();
    }

}
