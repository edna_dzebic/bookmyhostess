<?php

namespace backend\models\countrymapping;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\countrymapping\CountryMapping;

/**
 * CountryMappingIndex represents the model behind the search about `\backend\models\countrymapping\CountryMapping`.
 */
class CountryMappingIndex extends CountryMapping
{

    use CachedSearchTrait;

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Source Country Id
     * @var integer
     */
    public $source_country_id;

    /**
     * Target Country Id
     * @var integer
     */
    public $target_country_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'source_country_id', 'target_country_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'source_country_id' => $this->source_country_id,
            'target_country_id' => $this->target_country_id
        ]);

        return $dataProvider;
    }

}
