<?php

namespace backend\models\countrymapping;

use Yii;
use backend\models\country\Country;

/**
 * CountryMapping represents the backend model for `\common\models\CountryMapping`.
 */
class CountryMapping extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\CountryMapping';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [
            'id' => Yii::t('admin', 'Id'),
            'source_country_id' => Yii::t('admin', 'Source Country'),
            'target_country_id' => Yii::t('admin', 'Target Country'),
                ]
        );
    }

    /**
     * @return array
     */
    public function getCountryList()
    {
        return Country::getForDropdownList();
    }
    
      /**
     * @return string
     */
    public function getSourceCountryName()
    {
        return $this->modelObject->sourceCountryName;
    }

    /**
     * @return string
     */
    public function getTargetCountryName()
    {
        return $this->modelObject->targetCountryName;
    }
}
