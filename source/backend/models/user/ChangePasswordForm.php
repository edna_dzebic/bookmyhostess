<?php

namespace backend\models\user;

/**
 * @property \common\models\User $model
 */
class ChangePasswordForm extends \common\components\Container
{

    public $password;

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\User';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[['password'], 'required'], [['password'], 'string', 'min' => 8],];
    }

    /**
     * Load and save User
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setPassword($this->password);

            if ($this->modelObject->update())
            {
                return true;
            } else
            {
                $this->addErrors($this->modelObject->getErrors());
            }
        }

        return false;
    }

}
