<?php

namespace backend\models\newslettersubscriber;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\newslettersubscriber\NewsletterSubscriber;
use kartik\export\ExportMenu;

/**
 * NewsletterSubscriberIndex represents the model behind the search about `\backend\models\newslettersubscriber\NewsletterSubscriber`.
 */
class NewsletterSubscriberIndex extends NewsletterSubscriber
{

    use CachedSearchTrait;

    /**
     * Email
     * @var string
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

    /**
     * @param string $fileName
     * @param ActiveDataProvider $dataProvider
     * @return string
     */
    public function getWidget($fileName, $dataProvider)
    {

        $fileName = str_replace(" ", "-", $fileName);

        $provider = clone($dataProvider);
        $provider->pagination = false;

        $widget = ExportMenu::widget([
                    'dataProvider' => $provider,
                    'columns' => [
                        'email',
                        'date_created'
                    ],
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false
                    ],
                    'showColumnSelector' => true,
                    'fontAwesome' => true,
                    'target' => ExportMenu::TARGET_SELF,
                    'filename' => $fileName . '-' . date("d-m-Y"),
                    'showConfirmAlert' => false,
        ]);

        return $widget;
    }

}
