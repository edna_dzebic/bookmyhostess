<?php

namespace backend\models\newslettersubscriber;

use Yii;

/**
 * NewsletterSubscriberForm represents the model behind the form about `\backend\models\newslettersubscriber\NewsletterSubscriber`.
 */
class NewsletterSubscriberForm extends NewsletterSubscriber
{

    const SESSION_KEY_BACK_URL = 'newslettersubscriber.form.backUrl';

    /**
     * Email
     * @var string
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Load and save NewsletterSubscriber     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            return $this->modelObject->save();
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
