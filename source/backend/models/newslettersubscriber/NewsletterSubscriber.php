<?php

namespace backend\models\newslettersubscriber;

use Yii;

/**
 * NewsletterSubscriber represents the backend model for `\common\models\NewsletterSubscriber`.
 */
class NewsletterSubscriber extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\NewsletterSubscriber';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

}
