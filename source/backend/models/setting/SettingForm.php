<?php

namespace backend\models\setting;

use common\models\Setting;
use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;

/**
 * Setting form
 */
class SettingForm extends Model
{

    public $admin_email;
    public $developer_email;
    public $finance_email;
    public $contact_email;
    public $support_email;
    public $email_from;
    public $include_google_analytics;
    public $include_zopim;
    public $inlude_hotjar;
    public $google_analytics_tracking_id;
    public $meta_keywords;
    public $meta_description;
    public $google_play;
    public $app_store;
    public $send_firebase_notification;
    public $booking_price;
    public $manual_invoice_count;
    public $invoice_offset;
    public $manufacturer_code;
    // PCS
    public $pcs_images;
    public $pcs_basic_profile;
    public $pcs_languages;
    public $pcs_work_experience;
    public $pcs_summary;
    public $pcs_mobile_app;
    public $pcs_education;
    public $pcs_preferred_cities;
    public $pcs_preferred_jobs;
    public $pcs_images_min_cnt;
    public $pcs_languages_min_cnt;
    public $pcs_past_work_min_cnt;
    public $pcs_preferred_cities_min_cnt;
    public $pcs_preferred_jobs_min_cnt;
    // PRS
    public $prs_profile_completeness;
    public $prs_booked_count;
    public $prs_review_count;
    public $prs_no_answer_count;
    public $prs_reject_count;
    public $prs_booked_count_min_cnt;
    public $prs_review_count_min_cnt;
    // AVG time
    public $prs_avg_answer_time_percent;
    // Time levels
    public $prs_time_level_zero;
    public $prs_time_level_one;
    public $prs_time_level_two;
    public $prs_time_level_three;
    public $prs_time_level_four;
    public $prs_time_level_five;
    // Time level coeff
    public $prs_time_level_zero_coeff;
    public $prs_time_level_one_coeff;
    public $prs_time_level_two_coeff;
    public $prs_time_level_three_coeff;
    public $prs_time_level_four_coeff;
    public $prs_time_level_five_coeff;
    //
    public $prs_rejections_after_booking;
    // Last minute
    public $last_minute_days;
    public $last_minute_percentage;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $settings = Setting::find()->all();

        foreach ($settings as $setting)
        {
            $key = $setting->key;
            $this->$key = $setting->value;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'admin_email',
                    'developer_email',
                    'support_email',
                    'contact_email',
                    'support_email',
                    'email_from',
                    'include_google_analytics',
                    'include_zopim',
                    'inlude_hotjar',
                    'google_analytics_tracking_id',
                    'meta_keywords',
                    'meta_description',
                    'google_play',
                    'app_store',
                    'send_firebase_notification',
                    'booking_price',
                    'invoice_offset',
                    'manual_invoice_count',
                    'manufacturer_code',
                    'prs_rejections_after_booking',
                    'last_minute_days',
                    'last_minute_percentage'
                ],
                'required'
            ],
            [
                [
                    'booking_price',
                    'invoice_offset',
                    'manual_invoice_count',
                    'prs_rejections_after_booking',
                    'last_minute_days',
                    'last_minute_percentage'
                ],
                'number'
            ]
        ];
    }

    public function attributeLabels()
    {
        return [];
    }

    public function attributeHints()
    {
        return [
            'manual_invoice_count' => Yii::t('admin', 'How many invoices has been issued manually'),
            'invoice_offset' => Yii::t('admin', 'How many invoices is skipped at the beginning of the year'),
            'booking_price' => Yii::t('admin', 'Default booking price per day'),
            'last_minute_days' => Yii::t('admin', 'Number of days when will system charge for late booking'),
            'last_minute_percentage' => Yii::t('admin', 'Percentage for how much will prices will be increased in case of last minute booking')
        ];
    }

    public function save()
    {
        $attributes = $this->attributes;

        foreach ($attributes as $key => $value)
        {
            $setting_model = Setting::findByKey($key);

            if ($setting_model !== null)
            {
                $setting_model->value = $value;
                $setting_model->save();
            } else
            {
                throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
            }
        }

        return true;
    }

}
