<?php

namespace backend\models\affiliate;

use Yii;

/**
 * AffiliateForm represents the model behind the form about `\backend\models\affiliate\Affiliate`.
 */
class AffiliateForm extends Affiliate
{

    const SESSION_KEY_BACK_URL = 'affiliate.form.backUrl';

    /**
     * Identification
     * @var string
     */
    public $identification;

    /**
     * Name
     * @var string
     */
    public $name;
    
    /**
     * Provision
     * @var integer
     */
    public $provision;

    /**
     * Description
     * @var string
     */
    public $description;

    /**
     * Tax Number
     * @var string
     */
    public $tax_number;

    /**
     * Street
     * @var string
     */
    public $street;

    /**
     * Zip
     * @var string
     */
    public $zip;

    /**
     * City
     * @var string
     */
    public $city;

    /**
     * Phone
     * @var string
     */
    public $phone;

    /**
     * Email
     * @var string
     */
    public $email;

    /**
     * Web
     * @var string
     */
    public $web;

    /**
     * Fax
     * @var string
     */
    public $fax;

    /**
     * Contact
     * @var string
     */
    public $contact;

    /**
     * Country Id
     * @var integer
     */
    public $country_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'provision'], 'required'],
            [['country_id', 'provision'], 'integer'],
            [['identification', 'name', 'description', 'tax_number', 'street', 'zip', 'city', 'phone', 'email', 'web', 'fax', 'contact'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    public function attributeHints()
    {
        return [
            'web' => Yii::t('admin', 'Please enter with http or https ( http://website.domain or https://website.domain )'),
            'contact' => Yii::t('admin', 'If you know the name of the contact person on the partners side.'),
            'name' => Yii::t('admin', 'System will generate identification based on name.'),
            'provision' => Yii::t('admin', 'Partner provision in percentage.')
        ];
    }

    /**
     * Load and save Affiliate     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);


            if ($this->modelObject->save() == false)
            {
                $this->addErrors($this->modelObject->getErrors());
                return false;
            }
            
            return true;
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    private function generateIdentification()
    {
        
    }

}
