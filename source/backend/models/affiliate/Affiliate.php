<?php

namespace backend\models\affiliate;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Affiliate represents the backend model for `\common\models\Affiliate`.
 */
class Affiliate extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Affiliate';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public function getCountryList()
    {
        return \backend\models\country\Country::getForDropdownList();
    }

    /**
     * 
     * @return string
     */
    public function getCountryName()
    {
        return $this->modelObject->getCountryName();
    }
    
     /**
     * 
     * @return array
     */
    public function getListForDropdown()
    {
        return ArrayHelper::map(\common\models\Affiliate::find()->all(), 'id', 'name');
    }


}
