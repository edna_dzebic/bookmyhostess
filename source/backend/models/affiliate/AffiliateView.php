<?php

namespace backend\models\affiliate;

use Yii;

/**
 * AffiliateView represents the model behind the view about `\backend\models\affiliate\Affiliate`.
 */
class AffiliateView extends Affiliate
{

    const SESSION_KEY_BACK_URL = 'affiliate.view.backUrl';

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Identification
     * @var string
     */
    public $identification;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Provision
     * @var integer
     */
    public $provision;

    /**
     * Description
     * @var string
     */
    public $description;

    /**
     * Tax Number
     * @var string
     */
    public $tax_number;

    /**
     * Street
     * @var string
     */
    public $street;

    /**
     * Zip
     * @var string
     */
    public $zip;

    /**
     * City
     * @var string
     */
    public $city;

    /**
     * Phone
     * @var string
     */
    public $phone;

    /**
     * Email
     * @var string
     */
    public $email;

    /**
     * Web
     * @var string
     */
    public $web;

    /**
     * Fax
     * @var string
     */
    public $fax;

    /**
     * Contact
     * @var string
     */
    public $contact;

    /**
     * Country Id
     * @var integer
     */
    public $country_id;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date Updated
     * @var string
     */
    public $date_updated;

    /**
     * Created By
     * @var integer
     */
    public $created_by;

    /**
     * Updated By
     * @var integer
     */
    public $updated_by;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), []
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        return [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')],
            'edit_button' => ['class' => 'btn btn-success', 'url' => ['update', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Update')],
            'delete_button' => ['class' => 'btn btn-danger', 'url' => ['delete', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Delete'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this item?')]]
        ];
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
