<?php

namespace backend\models\affiliate;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\affiliate\Affiliate;

/**
 * This is class for generating reports for affiliates
 */
class AffiliateReport extends Affiliate
{

    use CachedSearchTrait;

    const INCLUDE_BOOKINGS = 0;
    const INCLUDE_REQUESTS = 1;
    const INCLUDE_ALL = 2;

    public $affiliate_id;
    public $date_from;
    public $date_to;
    public $include_option = true;

    /**
     * 
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->date_from = date("Y-m-d", strtotime('first day of last month'));
        $this->date_to = date("Y-m-d", strtotime('last day of last month'));
        $this->include_option = self::INCLUDE_BOOKINGS;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['affiliate_id', 'date_from', 'date_to', 'include_option'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'affiliate_id' => Yii::t('admin', 'Partner'),
            'date_from' => Yii::t('admin', 'From'),
            'date_to' => Yii::t('admin', 'To'),
            'include_option' => Yii::t('admin', 'Filter requests'),
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 
     * @return array
     */
    public function getIncludeOptionsForDropdown()
    {
        return [
            self::INCLUDE_BOOKINGS => Yii::t('admin', 'Include only bookings'),
            self::INCLUDE_REQUESTS => Yii::t('admin', 'Include only requests'),
            self::INCLUDE_ALL => Yii::t('admin', 'Include all')
        ];
    }

    /**
     * 
     * @param array $params
     * @return array
     */
    public function getReport($params)
    {
        if (false === $this->loadAndValidate($params))
        {
            return [];
        }

        $dataProvider = $this->search($params);

        $partner = $this->findAffiliate();

        $total = 0;

        foreach ($dataProvider->models as $request)
        {
            $total = $request->total_booking_price;
        }

        $provision = $partner->provision / 100 * $total;

        $result = [
            'partner' => $partner,
            'count' => count($dataProvider->models),
            'models' => $dataProvider->models,
            'from' => Yii::$app->util->formatDate($this->date_from),
            'to' => Yii::$app->util->formatDate($this->date_to),
            'total' => Yii::$app->util->formatPriceAsCurrency($total),
            'provision' => Yii::$app->util->formatPriceAsCurrency($provision),
            'dataProvider' => $dataProvider
        ];

        return $result;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @return ActiveDataProvider
     */
    private function search()
    {
        $query = \common\models\Request::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
            'pagination' => false
        ]);

        $query->andWhere(['affiliate_id' => $this->affiliate_id]);

        $query->andWhere(['>=', 'DATE(request.date_created)',
            date('Y-m-d', strtotime($this->date_from))]);

        $query->andWhere(['<=', 'DATE(request.date_created)',
            date('Y-m-d', strtotime($this->date_to))]);

        if (self::INCLUDE_BOOKINGS == $this->include_option)
        {
            $query->andWhere(['completed' => \common\models\Request::COMPLETED]);
        } else if (self::INCLUDE_REQUESTS == $this->include_option)
        {
            $query->andWhere(['completed' => \common\models\Request::NOT_COMPLETED]);
        }

        return $dataProvider;
    }

    /**
     * 
     * @return \common\models\Affiliate
     */
    private function findAffiliate()
    {
        $object = \common\models\Affiliate::findOne([
                    "id" => $this->affiliate_id
        ]);

        return $object;
    }

    /**
     * 
     * @param array $params
     * @return boolean
     */
    private function loadAndValidate($params)
    {
        $params = $this->getCachedSearchParams($params);

        $this->load($params);

        if (!$this->validate())
        {
            return false;
        }

        return true;
    }

}
