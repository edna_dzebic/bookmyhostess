<?php

namespace backend\models\affiliate;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\affiliate\Affiliate;

/**
 * AffiliateIndex represents the model behind the search about `\backend\models\affiliate\Affiliate`.
 */
class AffiliateIndex extends Affiliate
{
    use CachedSearchTrait;

    /**
     * Identification
     * @var string
     */
    public $identification;

    /**
     * Name
     * @var string
     */
    public $name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['identification', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'identification', $this->identification])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
