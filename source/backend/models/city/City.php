<?php

namespace backend\models\city;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * City represents the backend model for `\common\models\City`.
 */
class City extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\City';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public static function getForDropdownList()
    {
        return ArrayHelper::map(\common\models\City::find()->all(), 'id', 'name');
    }

    /**
     * 
     * @return array
     */
    public function getCountryList()
    {
        return \backend\models\country\Country::getForDropdownList();
    }

    /**
     * 
     * @return array
     */
    public static function getListForEventDropdown()
    {
        return ArrayHelper::map(
                        \common\models\City::find()
                                ->innerJoin('country', 'country.id = city.country_id')
                                ->innerJoin('country_price', 'country_price.country_id=country.id')->all(), "id", "name");
    }

    /**
     * 
     * @return string
     */
    public function getCountryName()
    {
        return $this->modelObject->getCountryName();
    }

}
