<?php

namespace backend\models\request;

use Yii;

/**
 * RequestForm represents the model behind the form about `\backend\models\request\Request`.
 */
class RequestForm extends Request
{

    const SESSION_KEY_BACK_URL = 'request.form.backUrl';

    /**
     * Event Id
     * @var integer
     */
    public $event_id;

    /**
     * User Id
     * @var integer
     */
    public $user_id;

    /**
     * Requested User Id
     * @var integer
     */
    public $requested_user_id;

    /**
     * Event Dress Code
     * @var integer
     */
    public $event_dress_code;

    /**
     * Completed
     * @var integer
     */
    public $completed;

    /**
     * Invoice Id
     * @var integer
     */
    public $invoice_id;

    /**
     * Note
     * @var string
     */
    public $note;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date From
     * @var string
     */
    public $date_from;

    /**
     * Date To
     * @var string
     */
    public $date_to;

    /**
     * Date Responded
     * @var string
     */
    public $date_responded;

    /**
     * Price
     * @var string
     */
    public $price;

    /**
     * Reason
     * @var string
     */
    public $reason;

    /**
     * Date Rejected
     * @var string
     */
    public $date_rejected;

    /**
     * Ex Withdraw Reason
     * @var string
     */
    public $ex_withdraw_reason;

    /**
     * Ex Withdraw Date
     * @var string
     */
    public $ex_withdraw_date;

    /**
     * Hostess Withdraw Reason
     * @var string
     */
    public $hostess_withdraw_reason;

    /**
     * Hostess Withdraw Date
     * @var string
     */
    public $hostess_withdraw_date;

    /**
     * Number Of Days
     * @var integer
     */
    public $number_of_days;

    /**
     * Booking Price Per Day
     * @var string
     */
    public $booking_price_per_day;

    /**
     * Total Booking Price
     * @var string
     */
    public $total_booking_price;
    
    /**
     * Payment type
     * @var integer
     */
    public $payment_type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'user_id', 'requested_user_id', 'event_dress_code', 'number_of_days', 'booking_price_per_day', 'total_booking_price', 'date_from', 'date_to', 'price', 'event_dress_code', 'payment_type'], 'required'],
            [['event_id', 'user_id', 'requested_user_id', 'event_dress_code', 'completed', 'invoice_id', 'number_of_days'], 'integer'],
            [['date_created', 'date_responded', 'date_rejected', 'hostess_withdraw_reason', 'hostess_withdraw_date'], 'safe'],
            [['price', 'booking_price_per_day', 'total_booking_price'], 'number'],
            [['note'], 'string', 'max' => 50],
            [['reason', 'ex_withdraw_reason', 'ex_withdraw_date'], 'string', 'max' => 255],
            ['date_from', 'compare', 'compareAttribute' => 'date_to', 'operator' => '<=', 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Load and save Request     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            return $this->modelObject->save();
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
