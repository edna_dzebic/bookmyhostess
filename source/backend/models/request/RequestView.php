<?php

namespace backend\models\request;

use Yii;

/**
 * RequestView represents the model behind the view about `\backend\models\request\Request`.
 */
class RequestView extends Request
{

    const SESSION_KEY_BACK_URL = 'request.view.backUrl';

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Event Id
     * @var integer
     */
    public $event_id;

    /**
     * User Id
     * @var integer
     */
    public $user_id;

    /**
     * Requested User Id
     * @var integer
     */
    public $requested_user_id;

    /**
     * Event Dress Code
     * @var integer
     */
    public $event_dress_code;

    /**
     * Completed
     * @var integer
     */
    public $completed;

    /**
     * Invoice Id
     * @var integer
     */
    public $invoice_id;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * Note
     * @var string
     */
    public $note;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date From
     * @var string
     */
    public $date_from;

    /**
     * Date To
     * @var string
     */
    public $date_to;

    /**
     * Date Responded
     * @var string
     */
    public $date_responded;

    /**
     * Price
     * @var string
     */
    public $price;

    /**
     * Reason
     * @var string
     */
    public $reason;

    /**
     * Date Rejected
     * @var string
     */
    public $date_rejected;

    /**
     * Ex Withdraw Reason
     * @var string
     */
    public $ex_withdraw_reason;

    /**
     * Ex Withdraw Date
     * @var string
     */
    public $ex_withdraw_date;

    /**
     * Hostess Withdraw Reason
     * @var string
     */
    public $hostess_withdraw_reason;

    /**
     * Hostess Withdraw Date
     * @var string
     */
    public $hostess_withdraw_date;

    /**
     * Number Of Days
     * @var integer
     */
    public $number_of_days;

    /**
     * Booking Price Per Day
     * @var string
     */
    public $booking_price_per_day;

    /**
     * Total Booking Price
     * @var string
     */
    public $total_booking_price;

    /**
     * Date Updated
     * @var string
     */
    public $date_updated;

    /**
     * Created By
     * @var integer
     */
    public $created_by;

    /**
     * Updated By
     * @var integer
     */
    public $updated_by;

    /**
     * Is last minute
     * @var boolean
     */
    public $is_last_minute;

    /**
     * Payment type
     * @var integer
     */
    public $payment_type;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        $buttons = [];

        $buttons['back_button'] = [
            'class' => 'btn btn-default',
            'url' => $this->getBackUrl(),
            'label' => Yii::t('admin', 'Back')
        ];

        if ($this->completed !== 1)
        {
            $buttons["edit_button"] = [
                'class' => 'btn btn-success',
                'url' => ['update', "id" => $this->modelObject->id],
                'label' => Yii::t('admin', 'Update')
            ];
        }

        if ($this->canBeCompletedByAdmin())
        {
            $buttons["mark_as_complete"] = [
                'class' => 'btn btn-primary',
                'url' => ['mark-as-completed', "id" => $this->modelObject->id],
                'label' => Yii::t('admin', 'Mark as complete')
            ];
        }

        if ($this->completed === 1 && $this->modelObject->event->isPast && is_object($this->modelObject->user))
        {
            $buttons["review"] = [
                'class' => 'btn btn-primary',
                'url' => ['/hostess-edit/add-review', "userId" => $this->modelObject->user->id, "requestId" => $this->modelObject->id],
                'label' => Yii::t('admin', 'Create review')
            ];
        }

        $buttons["delete_button"] = [
            'class' => 'btn btn-danger',
            'url' => ['delete', 'id' => $this->modelObject->id],
            'label' => Yii::t('admin', 'Delete'),
            'options' => [
                "data-confirm" => Yii::t('admin', 'Are you sure you want to delete this item?')
            ]
        ];

        return $buttons;
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
