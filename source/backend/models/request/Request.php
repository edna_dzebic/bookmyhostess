<?php

namespace backend\models\request;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Request represents the backend model for `\common\models\Request`.
 */
class Request extends \common\components\Container
{

    /**
     * 
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Request';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public function getEventList()
    {
        return \backend\models\event\Event::getForDropdownList();
    }

    /**
     * 
     * @return array
     */
    public function getCompletedList()
    {
        return [
            "-" => Yii::t('admin', '-'),
            "not_completed" => Yii::t('admin', 'Not completed'),
            "completed" => Yii::t('admin', 'Completed')
        ];
    }

    /**
     * 
     * @return array
     */
    public function getLastMinuteList()
    {
        return [
            "-" => Yii::t('admin', '-'),
            "1" => Yii::t('admin', 'Yes'),
            "0" => Yii::t('admin', 'No')
        ];
    }

    /**
     * 
     * @return array
     */
    public function getPaymentTypeList()
    {
        return [
            "-" => Yii::t('admin', '-'),
            Yii::$app->util->businessLicence => Yii::t('admin', 'Business license'),
            Yii::$app->util->shortTermEmployment => Yii::t('admin', 'Short term employment')
        ];
    }

    /**
     * 
     * @return array
     */
    public function getHostessList()
    {
        return \backend\models\User::getHostessList();
    }

    /**
     * 
     * @return array
     */
    public function getExhibitorList()
    {
        return \backend\models\User::getExhibitorList();
    }

    /**
     * 
     * @return array
     */
    public function getDressCodeList()
    {
        return ArrayHelper::map(\common\models\DressCode::find()->all(), 'id', 'name');
    }

    /**
     * 
     * @return array
     */
    public function getStatusList()
    {
        return [
            Yii::$app->status->request_new => Yii::t('admin', 'New'),
            Yii::$app->status->request_accept => Yii::t('admin', 'Hostess accepted'),
            Yii::$app->status->request_reject => Yii::t('admin', 'Hostess rejected'),
            Yii::$app->status->request_expired => Yii::t('admin', 'Expired: exhibitor'),
            Yii::$app->status->request_expired_hostess => Yii::t('admin', 'Expired: hostess'),
            Yii::$app->status->request_exhibitor_withdraw => Yii::t('admin', 'Withdraw: exhibitor'),
            Yii::$app->status->request_exhibitor_withdraw_after_accept => Yii::t('admin', 'Withdraw: exhibitor after accept'),
            Yii::$app->status->request_hostess_withdraw_after_accept => Yii::t('admin', 'Withdraw: hostess after accept'),
            Yii::$app->status->request_paypal_pending => Yii::t('admin', 'Waiting Paypal Info')
        ];
    }

    /**
     * 
     * @return boolean
     */
    public function canBeCompletedByAdmin()
    {

        if (isset($this->modelObject->completed) && $this->modelObject->completed == true)
        {
            return false;
        }

        if (
                $this->modelObject->status_id == Yii::$app->status->request_new ||
                $this->modelObject->status_id == Yii::$app->status->request_accept ||
                $this->modelObject->status_id == Yii::$app->status->request_expired
        )
        {
            return true;
        }

        return false;
    }

    /**
     * 
     * @return string
     */
    public function getLastMinuteLabel()
    {
        return $this->modelObject->getLastMinuteLabel();
    }

    /**
     * 
     * @return string
     */
    public function getCompletedLabel()
    {
        return $this->modelObject->getCompletedLabel();
    }

    /**
     * 
     * @return string
     */
    public function getPaymentTypeLabel()
    {
        return $this->modelObject->getPaymentTypeLabel();
    }

    /**
     * 
     * @return string
     */
    public function getStatusLabel()
    {
        return $this->modelObject->getStatusLabel();
    }

    /**
     * 
     * @return string
     */
    public function getEventName()
    {
        return $this->modelObject->getEventName();
    }

    /**
     * 
     * @return string
     */
    public function getHostessFullName()
    {
        return $this->modelObject->getHostessFullName();
    }

    /**
     * 
     * @return string
     */
    public function getExhibitorFullName()
    {
        return $this->modelObject->getExhibitorFullName();
    }

    /**
     * 
     * @return string
     */
    public function getDressCodeName()
    {
        return $this->modelObject->getNormalizedDressCodeName();
    }

    /**
     * 
     * @return string
     */
    public function getJobName()
    {
        return $this->modelObject->getNormalizedJobName();
    }

    /**
     * 
     * @return string
     */
    public function getFormattedHostessPrice()
    {
        return Yii::$app->util->formatEventPrice($this->modelObject->event, $this->modelObject->price);
    }
    
    /**
     * 
     * @return string
     */
    public function getAffiliateName()
    {
        return $this->modelObject->getAffiliateName();
    }

}
