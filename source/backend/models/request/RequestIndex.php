<?php

namespace backend\models\request;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\request\Request;
use kartik\export\ExportMenu;

/**
 * RequestIndex represents the model behind the search about `\backend\models\request\Request`.
 */
class RequestIndex extends Request
{

    use CachedSearchTrait;

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Event Id
     * @var integer
     */
    public $event_id;

    /**
     * User Id
     * @var integer
     */
    public $user_id;

    /**
     * Requested User Id
     * @var integer
     */
    public $requested_user_id;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date From
     * @var string
     */
    public $date_from;

    /**
     * Date To
     * @var string
     */
    public $date_to;

    /**
     * Date Responded
     * @var string
     */
    public $date_responded;

    /**
     * Price
     * @var string
     */
    public $price;

    /**
     * Completed
     * @var boolean
     */
    public $completed = '-';

    /**
     * Is last minute
     * @var boolean 
     */
    public $is_last_minute = '';

    /**
     * Payment type for hostess
     * @var integer
     */
    public $payment_type = '';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'user_id', 'requested_user_id', 'status_id', 'payment_type', 'id'], 'integer'],
            [['date_created', 'date_from', 'date_to', 'date_responded', 'completed', 'is_last_minute'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'event_id' => $this->event_id,
            'user_id' => $this->user_id,
            'requested_user_id' => $this->requested_user_id,
            'status_id' => $this->status_id
        ]);

        if ($this->completed === "completed")
        {
            $query->andWhere('completed is not null and completed = 1');
        } else if ($this->completed === "not_completed")
        {
            $query->andWhere('completed is null or completed = 0');
        }

        if (!empty($this->is_last_minute) && $this->is_last_minute != '-')
        {
            $query->andWhere('is_last_minute = ' . $this->is_last_minute);
        }

        if (!empty($this->payment_type) && $this->payment_type != '-')
        {
            $query->andWhere('payment_type = ' . $this->payment_type);
        }

        $query->andFilterWhere([">=", 'date_from', $this->date_from]);
        $query->andFilterWhere(["<=", 'date_to', $this->date_to]);

        $query->andFilterWhere(['like', 'price', $this->price]);

        return $dataProvider;
    }

    /**
     * @param string $fileName
     * @param ActiveDataProvider $dataProvider
     * @return string
     */
    public function getWidget($fileName, $dataProvider)
    {

        $fileName = str_replace(" ", "-", $fileName);

        $provider = clone($dataProvider);
        $provider->pagination = false;

        $widget = ExportMenu::widget([
                    'dataProvider' => $provider,
                    'columns' => [
                        'eventName',
                        'exhibitorFullName',
                        'exhibitorEmail',
                        'hostessFullName',
                        'hostessEmail',
                        'hostessPhone',
                        'completedDate'
                    ],
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false
                    ],
                    'showColumnSelector' => true,
                    'fontAwesome' => true,
                    'target' => ExportMenu::TARGET_SELF,
                    'filename' => $fileName . '-' . date("d-m-Y"),
                    'showConfirmAlert' => false,
        ]);

        return $widget;
    }

}
