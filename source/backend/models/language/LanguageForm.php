<?php

namespace backend\models\language;

use Yii;

/**
 * LanguageForm represents the model behind the form about `\backend\models\language\Language`.
 */
class LanguageForm extends Language
{

    const SESSION_KEY_BACK_URL = 'language.form.backUrl';

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Frontend Selectable
     * @var integer
     */
    public $frontend_selectable;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['frontend_selectable'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return array_merge(
                parent::attributeHints(), [
            'name' => Yii::t('admin', 'Please write name in English')
                ]
        );
    }

    /**
     * Load and save Language     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            return $this->modelObject->save();
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
