<?php

namespace backend\models\language;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Language represents the backend model for `\common\models\Language`.
 */
class Language extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Language';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [
            'isFrontendSelectable' => Yii::t('admin', 'Frontend Selectable'),
                ], parent::attributeLabels()
        );
    }

    /**
     * Is selectable on frontend label
     * @return string
     */
    public function getIsFrontendSelectable()
    {
        if ($this->modelObject->frontend_selectable !== 0)
        {
            return Yii::t('admin', 'Yes');
        }

        return Yii::t('admin', 'No');
    }
    
    /**
     * Return full language list
     * @return array
     */
    public static function getForDropdownList()
    {
        return ArrayHelper::map(\common\models\Language::find()->all(), 'id', 'name');
    }


}
