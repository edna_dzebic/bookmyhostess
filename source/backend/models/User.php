<?php

namespace backend\models;

class User extends \common\models\User
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHostessProfile()
    {
        return \common\models\HostessProfile::
                        find()
                        ->where([ "user_id" => $this->id])
                        ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExhibitorProfile()
    {
        return \common\models\ExhibitorProfile::
                        find()
                        ->where([ "user_id" => $this->id])
                        ->one();
    }

    /**
     * Find exhibitor based on id ( use where to eliminate status not deleted )
     * @param integer $id
     * @return User
     */
    public static function findExhibitor($id)
    {
        $query = static::find()
                ->where(["id" => $id, "role" => static::ROLE_EXHIBITOR]);

        return $query->one();
    }

    /**
     * Find hostess based on id ( use where to eliminate status not deleted )
     * @param integer $id
     * @return User
     */
    public static function findHostess($id)
    {
        $query = static::find()
                ->where(["id" => $id, "role" => static::ROLE_HOSTESS]);

        return $query->one();
    }

    /**
     * List of allowed roles for login
     *
     * @return array
     */
    protected static function allowedRoles()
    {
        return [static::ROLE_ADMIN, static::ROLE_SYS_ADMIN];
    }

    /**
     * 
     * @return array
     */
    public static function getHostessList($approved = true)
    {
        $query = static::find()
                ->innerJoin("hostess_profile", 'user.id = hostess_profile.user_id')
                ->andWhere(["role" => static::ROLE_HOSTESS]);

        if ($approved)
        {
            $query->andWhere("hostess_profile.admin_approved = " . \common\models\HostessProfile::APPROVAL_APPROVED);
        }

        $models = $query->all();
        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->getFullName();
        }

        return $result;
    }

    /**
     * 
     * @return array
     */
    public static function getExhibitorList()
    {
        $models = static::find()
                ->innerJoin("exhibitor_profile", 'user.id = exhibitor_profile.user_id')
                ->andWhere(["role" => static::ROLE_EXHIBITOR])
                ->all();

        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->getFullName();
        }

        return $result;
    }

}
