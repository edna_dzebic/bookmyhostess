<?php

namespace backend\models\hostess;

use Yii;
use common\models\HostessProfile;

/**
 * HostessView represents the model behind the view about `\backend\models\hostess\Hostess`.
 */
class HostessView extends Hostess
{

    const SESSION_KEY_BACK_URL = 'hostess.view.backUrl';

    public $user_id;
    public $lang;
    public $gender;
    public $first_name;
    public $last_name;
    public $email;
    public $phone_number;
    public $company_name;
    public $tax_number;
    public $street;
    public $home_number;
    public $postal_code;
    public $city;
    public $country_id;
    public $website;
    public $booking_type;
    public $staff_demand_type;
    public $booking_price_per_day;
    public $cached_rating;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date Updated
     * @var string
     */
    public $date_updated;

    /**
     * Created By
     * @var integer
     */
    public $created_by;

    /**
     * Updated By
     * @var integer
     */
    public $updated_by;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        $buttons = [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')]
        ];

        if (!$this->modelObject->isDeleted())
        {
            $buttons['delete_button'] = [
                'class' => 'btn btn-danger',
                'url' => [ '/hostess-edit/delete', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Delete'),
                'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this user? This action cannot be reverted!')]
            ];

            if ($this->modelObject->isPending())
            {
                $buttons["disable_button"] = ['class' => 'btn btn-default', 'url' => ['/hostess-edit/disable', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Disable'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to disable this user?')]];

                $buttons["activate_button"] = ['class' => 'btn btn-info', 'url' => ['/hostess-edit/activate', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Activate'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to activate this user?')]];
            }

            if ($this->modelObject->isDisabled())
            {
                $buttons["enable_button"] = ['class' => 'btn btn-info', 'url' => ['/hostess-edit/enable', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Enable'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to enable this user?')]];
            }

            if ($this->modelObject->isActive())
            {
                $buttons["disable_button"] = ['class' => 'btn btn-default', 'url' => ['/hostess-edit/disable', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Disable'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to disable this user?')]];
            }

            if ($this->profileObject->isApproved())
            {
                $buttons["pdf"] = [
                    'class' => 'btn btn-default',
                    'url' => ['pdf', "id" => $this->modelObject->id],
                    'label' => Yii::t('admin', 'PDF'),
                ];
            }

            if ($this->profileObject->isRequested())
            {
                $buttons["approve"] = [
                    'class' => 'btn btn-primary',
                    'url' => ['/hostess-edit/approve', "id" => $this->modelObject->id],
                    'label' => Yii::t('admin', 'Approve'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to approve this user?')]
                ];
                $buttons["disapprove"] = [
                    'class' => 'btn btn-warning js-btn-disapprove-hostess',
                    'url' => ['/hostess-edit/disapprove', "id" => $this->modelObject->id],
                    'label' => Yii::t('admin', 'Disapprove')
                ];
            }
        }

        return $buttons;
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    /**
     * Approve profile of hostess
     * @return boolean
     */
    public function approve()
    {
        $this->profileObject->admin_approved = HostessProfile::APPROVAL_APPROVED;
        $this->profileObject->approved_at = date("Y-m-d H:i:s");

        if ($this->profileObject->update() !== false)
        {
            return \common\models\Email::sendHostessProfileApprovedEmail($this->profileObject);
        }

        return false;
    }

}
