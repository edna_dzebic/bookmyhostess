<?php

namespace backend\models\hostess;

use Yii;
use common\models\HostessProfile;

/**
 * Hostess represents the backend model for `\common\models\HostessProfile`.
 */
class Hostess extends \common\components\Container
{

    /**
     * Profile Model Object
     * @var \common\models\HostessProfile 
     */
    public $profileObject;

    /**
     * Hostess constructor
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->modelClass = '\backend\models\User';

        parent::__construct($config);

        if (is_object($this->modelObject->hostessProfile))
        {
            $this->profileObject = $this->modelObject->hostessProfile;
        }

        if (!is_object($this->profileObject))
        {
            $this->profileObject = new HostessProfile();
            $this->profileObject->user_id = $this->modelObject->id;
        }

        $this->setAttributes($this->profileObject->attributes, false);
    }
    
    /**
     * @return \common\models\HostessProfile
     */
    public function getProfileModel()
    {
        return $this->profileObject;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), $this->profileObject->attributeLabels(), [
            'first_name' => Yii::t('admin', 'First Name'),
            'last_name' => Yii::t('admin', 'Last name'),
            'email' => Yii::t('admin', 'Email'),
            'date_created' => Yii::t('admin', 'Date Created'),
            'date_from' => Yii::t('admin', 'Created From'),
            'date_to' => Yii::t('admin', 'Created To'),
            'city' => Yii::t('admin', 'City'),
            'country_id' => Yii::t('admin', 'Country'),
            'tax_number' => Yii::t('admin', 'Tax number'),
            'status_id' => Yii::t('admin', 'Status'),
            'statusLabel' => Yii::t('admin', 'Status')
                ], parent::attributeLabels()
        );
    }

    /**
     * Get list of languages for preferred system language
     * @return array
     */
    public function getPreferredLanguageList()
    {
        return \common\models\Language::getPrefferedLanguageList();
    }

    public function getStatusList()
    {
        return [
            Yii::$app->status->active => Yii::t('admin', 'Active'),
            Yii::$app->status->pending => Yii::t('admin', 'Pending'),
            Yii::$app->status->disabled => Yii::t('admin', 'Disabled'),
            Yii::$app->status->deleted => Yii::t('admin', 'Deleted'),
        ];
    }

    /**
     * Get list of approval statuses for filter
     * @return array
     */
    public function getApprovedList()
    {
        return [
            HostessProfile::APPROVAL_APPROVED => Yii::t('admin', 'Approved'),
            HostessProfile::APPROVAL_NOT_REQUESTED => Yii::t('admin', 'Not requested'),
            HostessProfile::APPROVAL_REQUESTED => Yii::t('admin', 'Requested')
        ];
    }

    /**
     * Return list of jobs
     * @return array
     */
    public function getJobList()
    {
        return \backend\models\job\Job::getForDropdownList();
    }

    /**
     * Return list of cities
     * @return array
     */
    public function getCityList()
    {
        return \backend\models\city\City::getForDropdownList();
    }

    /**
     * Return list of countries
     * @return array
     */
    public function getCountryList()
    {
        return \backend\models\country\Country::getForDropdownList();
    }

    /**
     * Return gender list
     * @return array
     */
    public function getGenderList()
    {
        return [
            Yii::t('admin', 'Female'),
            Yii::t('admin', 'Male'),
        ];
    }

    /**
     * Return full language list
     * @return array
     */
    public function getLanguageList()
    {
        return \backend\models\language\Language::getForDropdownList();
    }

    /**
     * Return full language levels list
     * @return array
     */
    public function getLanguageLevelList()
    {
        return \backend\models\languagelevel\LanguageLevel::getForDropdownList();
    }

    /**
     * Return hair color list
     * @return array
     */
    public function getHairColorList()
    {
        return \backend\models\haircolor\HairColor::getForDropdownList();
    }

    /**
     * Country name
     * @return string
     */
    public function getCountryName()
    {
        if (is_object($this->profileObject->country))
        {
            return $this->profileObject->country->name;
        }

        return '';
    }

}
