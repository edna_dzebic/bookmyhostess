<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;

/**
 * ProfileForm represents the model behind the form about `\backend\models\hostess\Hostess`.
 */
class ProfileForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.edit.form.backUrl';

    public $user_id;
    // user model fields
    public $username;
    public $first_name;
    public $last_name;
    public $email;
    public $lang;
    public $gender;
    // profile fields
    public $birth_date;
    public $hair_color_id;
    public $height;
    public $weight;
    public $waist_size;
    public $shoe_size;
    public $has_tattoo;
    public $has_car;
    public $has_driving_licence;
    public $has_trade_licence;
    public $has_health_certificate;
    public $has_mobile_app;
    // not required
    public $summary;
    public $num_rejections_after_booking;
    public $is_trained;

    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->setAttributes($this->modelObject->getAttributes());
        $this->setAttributes($this->profileObject->getAttributes());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'username', 'first_name', 'last_name', 'username', 'lang', 'gender', 'birth_date', 'hair_color_id', 'height', 'weight', 'waist_size', 'shoe_size', 'has_tattoo', 'has_car', 'has_driving_licence', 'has_trade_licence', 'has_health_certificate'], 'required'],
            [['user_id', 'username', 'first_name', 'last_name', 'username', 'lang', 'gender', 'birth_date', 'hair_color_id', 'height', 'weight', 'waist_size', 'shoe_size', 'has_tattoo', 'has_car', 'has_driving_licence', 'has_trade_licence', 'has_health_certificate', 'summary'], 'filter', 'filter' => 'trim'],
            [['user_id', 'username', 'first_name', 'last_name', 'username', 'lang', 'gender', 'birth_date', 'hair_color_id', 'height', 'weight', 'waist_size', 'shoe_size', 'has_tattoo', 'has_car', 'has_driving_licence', 'has_trade_licence', 'has_health_certificate', 'summary'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            [['summary', 'username', 'email', 'first_name', 'last_name', 'lang'], 'string', 'max' => 255],
            [['height', 'weight', 'waist_size', 'shoe_size'], 'number'],
            [['user_id', 'gender', 'hair_color_id', 'has_tattoo', 'has_car', 'has_driving_licence', 'has_trade_licence', 'has_health_certificate', 'num_rejections_after_booking', 'has_mobile_app'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'lang' => Yii::t('admin', 'Preferred System Language'),
            'username' => Yii::t('admin', 'Username'),
            'first_name' => Yii::t('admin', 'First name'),
            'last_name' => Yii::t('admin', 'Last name'),
            'birth_date' => Yii::t('admin', 'Birth Date'),
            'hair_color_id' => Yii::t('admin', 'Hair Color'),
            'height' => Yii::t('admin', 'Height'),
            'weight' => Yii::t('admin', 'Weight'),
            'waist_size' => Yii::t('admin', 'Waist Size'),
            'shoe_size' => Yii::t('admin', 'Shoe Size'),
            'has_tattoo' => Yii::t('admin', 'Has Tattoo'),
            'has_car' => Yii::t('admin', 'Has Car'),
            'has_driving_licence' => Yii::t('admin', 'Has Driving Licence'),
            'has_trade_licence' => Yii::t('admin', 'Has Trade Licence'),
            'has_health_certificate' => Yii::t('admin', 'Has Health Certificate'),
            'is_trained' => Yii::t('admin', 'Is Trained'),
            'gender' => Yii::t('admin', 'Gender'),
            'num_rejections_after_booking' => Yii::t('admin', 'Number Rejections After Booking'),
            'has_mobile_admin' => Yii::t('admin', 'Has Mobile App'),
            'summary' => Yii::t('admin', 'Summary'),
                ]
        );
    }

    /**
     * Load and save Hostess
     * @param array $data
     * @returns bool is operation successful
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $transaction = Yii::$app->db->beginTransaction();

            if ($this->saveUserModel() && $this->saveProfileModel())
            {
                $this->profileObject->recalculateProfileCompleteness();
                $transaction->commit();
                return true;
            }

            $this->addErrors($this->modelObject->getErrors());
            $this->addErrors($this->profileObject->getErrors());

            $transaction->rollBack();
            return false;
        }

        return false;
    }

    /**
     * Save data to user model
     * @return boolean
     */
    private function saveUserModel()
    {
        $this->modelObject->setAttributes($this->attributes);

        return $this->modelObject->save();
    }

    /**
     * Save data to hostess profile
     * @return boolean
     */
    private function saveProfileModel()
    {
        $this->setProfileModel();

        return $this->profileObject->save();
    }

    /**
     * Set attributes for profile model
     */
    private function setProfileModel()
    {
        $this->profileObject->setAttributes($this->attributes);
    }

}
