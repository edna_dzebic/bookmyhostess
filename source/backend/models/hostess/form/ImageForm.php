<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;
use common\models\UserImages;
use yii\web\UploadedFile;

class ImageForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.image.form.backUrl';

    public $user_id;
    public $image_url;
    public $is_default;
    
    /**
     * @var \common\models\UserImages
     */
    public $imageModel;

    /**
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->setAttributes($this->imageModel->getAttributes());

        if ($this->imageModel->isNewRecord)
        {
            $this->user_id = $config["user_id"];
        }
    }

    public function rules()
    {
        return [
            [['image_url', 'user_id'], 'required'],
            [['is_default', 'user_id'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('admin', 'User'),
            'image_url' => Yii::t('admin', 'Image'),
            'is_default' => Yii::t('admin', 'Is default')
        ];
    }

    public function loadAndSave($data)
    {
        if ($this->loadData($data) && $this->validate())
        {
            $transaction = Yii::$app->db->beginTransaction();

            $this->imageModel->setAttributes($this->getAttributes());

            if ($this->imageModel->saveImage() && $this->setDefault())
            {
                $transaction->commit();
                return true;
            }

            $this->addErrors($this->imageModel->getErrors());

            $transaction->rollBack();
        }

        return false;
    }

    public function setAsDefault($image)
    {
        $images = $this->profileObject->images;
        $filtered = [];

        if (count($images) > 0)
        {
            foreach ($images as $img)
            {
                if ($img->id !== $image->id)
                {
                    $filtered[] = $img->id;
                }
            }
        }

        $image->is_default = true;

        if ($image->update() !== false)
        {
            if (count($filtered) > 0)
            {
                return UserImages::updateAll(["is_default" => 0], ["id" => $filtered]) >= 0;
            }

            return true;
        }

        return false;
    }

    private function setDefault()
    {
        if ($this->imageModel->is_default)
        {
            return $this->setAsDefault($this->imageModel);
        }

        return true;
    }

    private function loadData($data)
    {
        $this->load($data);

        $this->image_url = UploadedFile::getInstance($this, 'image_url');

        return true;
    }

}
