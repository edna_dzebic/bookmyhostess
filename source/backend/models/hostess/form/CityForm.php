<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;
use common\models\HostessPreferredCity;

class CityForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.city.form.backUrl';

    public $user_id;
    public $city_id;

    /**
     * @var \common\models\HostessPreferredCity
     */
    public $cityModel;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'city_id'], 'integer'],
            [['user_id', 'city_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'city_id' => Yii::t('admin', 'City'),
                ]
        );
    }

    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->cityModel->setAttributes($this->attributes);

            if ($this->cityModel->save() !== false)
            {
                $this->profileObject->recalculateProfileCompleteness();
                return true;
            }

            $this->addErrors($this->cityModel->getErrors());
        }

        return false;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        $found = HostessPreferredCity::find()
                        ->andWhere([
                            "user_id" => $this->user_id,
                            "city_id" => $this->city_id
                        ])->exists();

        if ($found)
        {
            $this->addError('city_id', Yii::t('admin', 'This city is already added for this hostess.'));
        }

        return !$this->hasErrors();
    }

}
