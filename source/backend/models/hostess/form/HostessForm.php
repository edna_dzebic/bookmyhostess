<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;

/**
 * HostessForm represents the model behind the form about `\backend\models\hostess\Hostess`.
 */
class HostessForm extends Hostess
{

    const SESSION_KEY_BACK_URL = 'hostess.form.backUrl';
    const SESSION_KEY_BACK_URL_DELETE = 'hostess.delete.form.backUrl';

    /**
     * Activate user profile
     * @return boolean
     */
    public function activate()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->modelObject->activate() !== false)
        {
            if ($this->profileObject->activate() !== false)
            {
                $transaction->commit();
                return true;
            }
        }
        
        $this->profileObject->addErrors($this->modelObject->getErrors());

        $transaction->rollBack();
        return false;
    }

    /**
     * Delete model
     *
     * @returns boolean
     */
    public function delete()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->modelObject->delete() !== false)
        {
            if ($this->profileObject->delete() !== false)
            {
                $transaction->commit();
                return true;
            }
        }

        $this->profileObject->addErrors($this->modelObject->getErrors());
        
        $transaction->rollBack();
        return false;
    }

    /**
     * Disable model
     *
     * @returns boolean
     */
    public function disable()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->modelObject->disable() !== false)
        {
            if ($this->profileObject->disable() !== false)
            {
                $transaction->commit();
                return true;
            }
        }
        
        
        $this->profileObject->addErrors($this->modelObject->getErrors());

        $transaction->rollBack();
        return false;
    }

    /**
     * Enable model
     *
     * @returns boolean
     */
    public function enable()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->modelObject->enable() !== false)
        {
            if ($this->profileObject->enable() !== false)
            {
                $transaction->commit();
                return true;
            }
        }
        
        $this->profileObject->addErrors($this->modelObject->getErrors());

        $transaction->rollBack();
        return false;
    }

}
