<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;
use yii\web\UploadedFile;

class DocumentForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.document.form.backUrl';

    public $user_id;
    public $filename;
    public $real_filename;
    
    /**
     * @var \common\models\UserDocuments
     */
    public $documentModel;

    /**
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->setAttributes($this->documentModel->getAttributes());

        if ($this->documentModel->isNewRecord)
        {
            $this->user_id = $config["user_id"];
        }
    }

    public function rules()
    {
        return [
            [['user_id', 'filename', 'real_filename'], 'required'],
            ['user_id', 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('admin', 'User'),
            'filename' => Yii::t('admin', 'File')
        ];
    }

    public function loadAndSave($data)
    {
        if ($this->loadData($data) && $this->validate())
        {
            $this->documentModel->setAttributes($this->getAttributes());

            if ($this->documentModel->saveDocument())
            {
                return true;
            }

            $this->addErrors($this->documentModel->getErrors());
        }

        return false;
    }

    private function loadData($data)
    {
        $this->load($data);

        $this->filename = UploadedFile::getInstance($this, 'filename');
        $this->real_filename = $this->filename;

        return true;
    }

}
