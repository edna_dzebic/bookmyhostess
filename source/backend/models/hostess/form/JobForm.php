<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;
use common\models\HostessPreferredJob;

class JobForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.job.form.backUrl';

    public $user_id;
    public $event_job_id;

    /**
     * @var \common\models\HostessPreferredJob
     */
    public $jobModel;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'event_job_id'], 'integer'],
            [['user_id', 'event_job_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'event_job_id' => Yii::t('admin', 'Job'),
                ]
        );
    }

    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->jobModel->setAttributes($this->attributes);

            if ($this->jobModel->save() !== false)
            {
                $this->profileObject->recalculateProfileCompleteness();
                return true;
            }

            $this->addErrors($this->jobModel->getErrors());
        }

        return false;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        $found = HostessPreferredJob::find()
                        ->andWhere([
                            "user_id" => $this->user_id,
                            "event_job_id" => $this->event_job_id
                        ])->exists();

        if ($found)
        {
            $this->addError('event_job_id', Yii::t('admin', 'This job is already added for this hostess.'));
        }

        return !$this->hasErrors();
    }

}
