<?php

namespace backend\models\hostess\form;

interface HostessFormInterface
{
    /**
     * @param array $data
     */
    public function loadAndSave($data);
    
    /**
     *  @return string
     */
    public function getBackUrl();
    
    /**
     * @return \common\models\HostessProfile
     */
    public function getProfileModel();
}