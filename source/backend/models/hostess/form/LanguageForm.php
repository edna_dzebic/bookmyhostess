<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;
use common\models\HostessLanguage;

class LanguageForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.language.form.backUrl';

    public $user_id;
    public $language_id;
    public $level_id;

    /**
     * @var \common\models\HostessLanguage
     */
    public $languageModel;

    /**
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->setAttributes($this->languageModel->getAttributes());

        if ($this->languageModel->isNewRecord)
        {
            $this->user_id = $config["user_id"];
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'language_id', 'level_id'], 'integer'],
            [['user_id', 'language_id', 'level_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'language_id' => Yii::t('admin', 'Language'),
            'level_id' => Yii::t('admin', 'Level'),
                ]
        );
    }

    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->languageModel->setAttributes($this->attributes);

            if ($this->languageModel->save() !== false)
            {
                $this->profileObject->recalculateProfileCompleteness();
                return true;
            }

            $this->addErrors($this->languageModel->getErrors());
        }

        return false;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        $query = HostessLanguage::find()
                ->andWhere([
            "user_id" => $this->user_id,
            "language_id" => $this->language_id
        ]);

        if (!$this->languageModel->isNewRecord)
        {
            $query->andWhere(["<>", 'id', $this->languageModel->id]);
        }

        $found = $query->exists();
        if ($found)
        {
            $this->addError('language_id', Yii::t('admin', 'This language is already added for this hostess.'));
        }

        return !$this->hasErrors();
    }

}
