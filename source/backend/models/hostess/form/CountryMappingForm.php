<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;
use common\models\HostessCountryMapping;

class CountryMappingForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.country.mapping.form.backUrl';

    public $user_id;
    public $country_id;

    /**
     * @var \common\models\HostessCountryMapping
     */
    public $countryMappingModel;

    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->country_id = null;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'country_id'], 'integer'],
            [['user_id', 'country_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'country_id' => Yii::t('admin', 'Country')
                ]
        );
    }

    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->countryMappingModel->setAttributes($this->attributes);

            if ($this->countryMappingModel->save() !== false)
            {
                return true;
            }

            $this->addErrors($this->countryMappingModel->getErrors());
        }

        return false;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        $found = HostessCountryMapping::find()
                        ->andWhere([
                            "user_id" => $this->user_id,
                            "country_id" => $this->country_id
                        ])->exists();

        if ($found)
        {
            $this->addError('country_id', Yii::t('admin', 'This country is already added for this hostess.'));
        }

        return !$this->hasErrors();
    }

}
