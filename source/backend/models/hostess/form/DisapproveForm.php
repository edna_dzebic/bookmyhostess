<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;
use common\models\HostessProfile;

class DisapproveForm extends Hostess
{

    public $user_id;
    public $reason;
    public $reason_text;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['reason', 'reason_text'], 'filter', 'filter' => 'trim'],
            [['reason', 'reason_text'], 'string', 'max' => 255],
            [['reason', 'reason_text'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            ['reason', 'validateReason', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['reason_text', 'validateReason', 'skipOnEmpty' => false, 'skipOnError' => false]
        ];
    }

    /**
     * Load and validate data
     * @param array $data
     * @return boolean
     */
    public function loadAndValidate($data)
    {
        return $this->load($data) && $this->validate();
    }

    /**
     * Disapprove profile of hostess
     * @return boolean
     */
    public function disapprove()
    {
        $this->profileObject->admin_approved = HostessProfile::APPROVAL_NOT_REQUESTED;

        $message = $this->saveMessage();

        if (is_object($message) && ($this->profileObject->update() !== false))
        {
            return \common\models\Email::sendHostessProfileNotApprovedEmail(
                            $this->profileObject, $message
            );
        } else
        {
            $this->addError('reason', Yii::t('admin', 'Error during saving message.'));
        }

        return false;
    }

    /**
     * Custom validation, either reason or reason_text are required, not both
     * @param string $attribute_name
     * @param array $params
     * @return boolean
     */
    public function validateReason($attribute_name, $params)
    {
        if (empty($this->reason) && empty($this->reason_text))
        {
            $this->addError($attribute_name, Yii::t('admin', 'At least 1 of the field must be filled up properly'));

            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reason' => Yii::t('admin', 'Rejection reason')
        ];
    }

    /**
     * Get possible reasons for disapprove for dropdown
     * @return array
     */
    public function getReasons()
    {
        return [
            'trade_license_missing' => Yii::t('app', 'Trade licence not provided'),
            'bad_pictures' => Yii::t('app', 'At least one professional picture not provided'),
            'trade_license_and_bad_pictures' => Yii::t('app', 'Trade licence and at least one professional picture not provided'),
            'username_and_email' => Yii::t('app', 'Username can not be email. You will receive a new username by mail.'),
            'username_and_name' => Yii::t('app', 'Username can not consist of the name and last name. You will receive a new username by mail.')
        ];
    }

    /**
     * Save message to database
     * @return \common\models\UserMessages
     */
    private function saveMessage()
    {
        // save message on hostess language
        $lang = $this->profileObject->user->lang;

        $messageModel = new \common\models\UserMessages();
        $messageModel->to_user_id = $this->user_id;
        $messageModel->from_user_id = \Yii::$app->user->id;
        $messageModel->subject = Yii::t('app', "Rejection comment", [], $lang);
        $messageModel->message = $this->getReasonAsText($lang);

        if ($messageModel->save())
        {
            return $messageModel;
        }

        return null;
    }

    /**
     * Get full reason text
     * @return type
     */
    private function getReasonAsText($lang)
    {
        if (empty($this->reason_text))
        {
            $text = $this->getReasons()[$this->reason];
            return Yii::t('app', $text, [], $lang);
        }

        return $this->reason_text;
    }

}
