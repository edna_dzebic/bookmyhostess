<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;

class EducationForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.education.form.backUrl';

    public $user_id;
    public $education_graduation;
    public $education_university;
    public $education_vocational_training;
    public $education_profession;
    public $education_special_knowledge;

    /**
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->setAttributes($this->profileObject->getAttributes());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'required'],
            [['education_graduation', 'education_university', 'education_vocational_training', 'education_profession', 'education_special_knowledge'], 'filter', 'filter' => 'trim'],
            [['education_graduation', 'education_university', 'education_vocational_training', 'education_profession', 'education_special_knowledge'], 'string', 'max' => 100],
            [['education_graduation', 'education_university', 'education_vocational_training', 'education_profession', 'education_special_knowledge'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'education_graduation' => Yii::t('admin', 'Education Graduation'),
            'education_university' => Yii::t('admin', 'Education University'),
            'education_vocational_training' => Yii::t('admin', 'Education Vocational Training'),
            'education_profession' => Yii::t('admin', 'Education Profession'),
            'education_special_knowledge' => Yii::t('admin', 'Education Special Knowledge')
                ]
        );
    }

    /**
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->profileObject->setAttributes($this->attributes);

            if ($this->profileObject->update() !== false)
            {
                $this->profileObject->recalculateProfileCompleteness();
                return true;
            }

            $this->addErrors($this->profileObject->getErrors());
        }

        return false;
    }

}
