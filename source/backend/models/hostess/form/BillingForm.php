<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;

class BillingForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.billing.form.backUrl';

    public $user_id;
    public $phone_number;
    public $tax_number;
    public $billing_address;
    public $postal_code;
    public $city;
    public $country_id;

    /**
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->setAttributes($this->modelObject->getAttributes());
        $this->setAttributes($this->profileObject->getAttributes());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'country_id'], 'integer'],
            ['phone_number', 'number'],
            [['user_id', 'phone_number', 'tax_number', 'billing_address', 'postal_code', 'city', 'country_id'], 'required'],
            [['phone_number', 'tax_number', 'billing_address', 'postal_code', 'city', 'country_id'], 'filter', 'filter' => 'trim'],
            [['tax_number', 'billing_address', 'postal_code', 'city'], 'string', 'max' => 255],
            [['phone_number', 'tax_number', 'billing_address', 'postal_code', 'city', 'country_id'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'phone' => Yii::t('admin', 'Phone number'),
            'tax_number' => Yii::t('admin', 'Tax number'),
            'billing_address' => Yii::t('admin', 'Address'),
            'postal_code' => Yii::t('admin', 'Postal code'),
            'city' => Yii::t('admin', 'City'),
            'country_id' => Yii::t('admin', 'Country')
                ]
        );
    }

    /**
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $transaction = Yii::$app->db->beginTransaction();

            if ($this->saveUserModel() && $this->saveProfileModel())
            {
                $transaction->commit();
                return true;
            }

            $this->addErrors($this->modelObject->getErrors());
            $this->addErrors($this->profileObject->getErrors());

            $transaction->rollBack();
            return false;
        }

        return false;
    }

    /**
     * Save data to user model
     * @return boolean
     */
    private function saveUserModel()
    {
        $this->modelObject->setAttributes($this->attributes);

        return $this->modelObject->save();
    }

    /**
     * Save data to hostess profile
     * @return boolean
     */
    private function saveProfileModel()
    {
        $this->setProfileModel();

        return $this->profileObject->save();
    }

    /**
     * Set attributes for profile model
     */
    private function setProfileModel()
    {
        $this->profileObject->setAttributes($this->attributes);
    }

}
