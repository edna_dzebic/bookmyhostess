<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;
use common\models\HostessReview;
use yii\helpers\ArrayHelper;

/**
 * TODO
 * Lack of time - this is almost duplicate of HostessReview.
 * Refactor and redo part from hostess profile -> to review with usage
 * of general HostessReview models.
 */
class ReviewForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.review.form.backUrl';

    public $hostess_id;
    public $event_id;
    public $exhibitor_id;
    public $rating;
    public $comment;
    public $group_code;
    public $review_requested;
    public $event_request_id;
    public $status_id;

    /**
     * @var \common\models\HostessReview
     */
    public $reviewModel;

    /**
     *
     * @var \common\models\Request
     */
    public $requestModel;

    /**
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->setAttributes($this->reviewModel->getAttributes());

        if ($this->reviewModel->isNewRecord)
        {
            $this->hostess_id = $config["hostess_id"];
            $this->group_code = strtotime("now") . rand(1000, 9999);
            $this->review_requested = 0;
            $this->status_id = Yii::$app->status->active;
        }

        if (is_object($this->requestModel))
        {
            $this->event_request_id = $this->requestModel->id;
            $this->event_id = $this->requestModel->event_id;
            $this->exhibitor_id = $this->requestModel->requested_user_id;
            $this->hostess_id = $this->requestModel->user_id;
        }        
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hostess_id', 'event_id', 'exhibitor_id', 'event_request_id'], 'integer'],
            [['hostess_id', 'event_id', 'exhibitor_id', 'rating', 'comment'], 'required'],
            [['comment'], 'filter', 'filter' => 'trim'],
            [['group_code'], 'string', 'max' => 255],
            [['comment', 'group_code'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'event_id' => Yii::t('admin', 'Event'),
            'exhibitor_id' => Yii::t('admin', 'Exhibitor'),
            'hostess_id' => Yii::t('admin', 'Hostess'),
            'comment' => Yii::t('admin', 'Comment'),
            'rating' => Yii::t('admin', 'Rating'),
                ]
        );
    }

    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->reviewModel->setAttributes($this->attributes);

            if ($this->reviewModel->save() !== false)
            {
                $this->profileObject->recalculateRating();
                return true;
            }

            $this->addErrors($this->reviewModel->getErrors());
        }

        return false;
    }

    /**
     * 
     * @param array $attributeNames
     * @param boolean $clearErrors
     * @return boolean
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        $query = HostessReview::find()
                ->andWhere([
            "event_id" => $this->event_id,
            "hostess_id" => $this->hostess_id,
            "exhibitor_id" => $this->exhibitor_id
        ]);

        if (!$this->reviewModel->isNewRecord)
        {
            $query->andWhere(["<>", 'id', $this->reviewModel->id]);
        }

        $found = $query->exists();
        if ($found)
        {
            $this->addError('event_id', Yii::t('admin', 'This review is already added for this hostess for given event for given exhibitor. It is possible that review is still pending!'));
        }

        return !$this->hasErrors();
    }

    /**
     * 
     * @return array
     */
    public function getEventList()
    {
        $query = \common\models\Event::find()
                ->andWhere("end_date <= NOW()");

        $models = $query->all();

        return ArrayHelper::map($models, 'id', 'name');
    }

    /**
     * 
     * @return array
     */
    public function getExhibitorList()
    {
        return \backend\models\User::getExhibitorList();
    }

}
