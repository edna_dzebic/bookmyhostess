<?php

namespace backend\models\hostess\form;

use Yii;
use backend\models\hostess\Hostess;

class WorkExperienceForm extends Hostess implements HostessFormInterface
{

    const SESSION_KEY_BACK_URL = 'hostess.work.experience.form.backUrl';

    public $user_id;
    public $start_date;
    public $end_date;
    public $position;
    public $event_name;
    public $job_description;
    
    /**
     * @var \common\models\HostessPastWork
     */
    public $workModel;

    /**
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        parent::__construct($config);

        $this->setAttributes($this->workModel->getAttributes());

        if ($this->workModel->isNewRecord)
        {
            $this->user_id = $config["user_id"];
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['user_id', 'job_description', 'position'], 'required'],
            [['start_date', 'end_date'], 'safe'],
            [['event_name', 'job_description', 'position'], 'string', 'max' => 255],
            [['event_name', 'job_description', 'position', 'start_date', 'end_date'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
            ['start_date', 'compare', 'compareAttribute' => 'end_date', 'operator' => '<=', 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'user_id' => Yii::t('admin', 'User'),
            'start_date' => Yii::t('admin', 'Start Date'),
            'end_date' => Yii::t('admin', 'End Date'),
            'event_name' => Yii::t('admin', 'Event'),
            'position' => Yii::t('admin', 'Position'),
            'job_description' => Yii::t('admin', 'Job description'),
                ]
        );
    }

    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->workModel->setAttributes($this->attributes);

            if ($this->workModel->save() !== false)
            {
                $this->profileObject->recalculateProfileCompleteness();
                return true;
            }

            $this->addErrors($this->profileObject->getErrors());
        }

        return false;
    }

}
