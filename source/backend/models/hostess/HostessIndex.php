<?php

namespace backend\models\hostess;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HostessProfile;
use kartik\export\ExportMenu;
use backend\components\CachedSearchTrait;

/**
 * HostessIndex represents the model behind the search about `\backend\models\hostess\Hostess`.
 */
class HostessIndex extends Hostess
{
    use CachedSearchTrait;

    /**
     * ID
     * @var integer
     */
    public $id;

    /**
     * Username
     * @var string
     */
    public $username;

    /**
     * First name
     * @var string
     */
    public $first_name;

    /**
     * Last name
     * @var string
     */
    public $last_name;

    /**
     * Email
     * @var string
     */
    public $email;

    /**
     * Date from for filter
     * @var string
     */
    public $date_from;

    /**
     * Date to for filter
     * @var string
     */
    public $date_to;

    /**
     * Hair color id
     * @var integer
     */
    public $hair_color_id;

    /**
     * Minimum age
     * @var integer
     */
    public $ageMin;

    /**
     * Maximmum age
     * @var integer
     */
    public $ageMax;
    public $heightMin;
    public $heightMax;
    public $weightMin;
    public $weightMax;
    public $waistSizeMin;
    public $waistSizeMax;
    public $shoeSizeMin;
    public $shoeSizeMax;
    public $hasTattoo;
    public $hasCar;
    public $hasDrivingLicense;
    public $hasTradeLicense;
    public $hasHealthCertificate;
    public $hasMobileApp;
    public $isTrained;
    public $languages;
    public $country_id;
    public $gender;
    public $num_rejections_after_booking;
    public $status_id = null;
    public $approval_status = null;
    public $approved_at;
    public $pref_cities;
    public $pref_jobs;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'username',
                    'first_name',
                    'last_name',
                    'email',
                    'date_from',
                    'date_to',
                    'country_id',
                    'hairColor',
                    'ageMin',
                    'ageMax',
                    'heightMin',
                    'heightMax',
                    'weightMin',
                    'weightMax',
                    'waistSizeMin',
                    'waistSizeMax',
                    'shoeSizeMin',
                    'shoeSizeMax',
                    'hasTattoo',
                    'hasCar',
                    'hasDrivingLicense',
                    'hasTradeLicense',
                    'hasHealthCertificate',
                    'hasMobileApp',
                    'isTrained',
                    'languages',
                    'pref_cities',
                    'pref_jobs',
                    'gender',
                    'num_rejections_after_booking',
                    'status_id',
                    'approval_status',
                    'approved_at'
                ],
                'safe'
            ],
            ['ageMin', 'compare', 'compareValue' => 18, 'operator' => '>'],
            ['ageMax', 'compare', 'compareAttribute' => "ageMin", 'operator' => '>'],
            ['heightMax', 'compare', 'compareAttribute' => "heightMin", 'operator' => '>'],
            ['weightMax', 'compare', 'compareAttribute' => "weightMin", 'operator' => '>'],
            ['waistSizeMin', 'compare', 'compareAttribute' => "waistSizeMax", 'operator' => '>'],
            ['shoeSizeMin', 'compare', 'compareAttribute' => "shoeSizeMax", 'operator' => '>'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), []
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * Search all
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAll($params)
    {
        $query = $this->baseSearch($params);

        return $this->createDataProvider($query);
    }

    /**
     * Creates data provider instance with search query applied
     * Search by active status
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchActive($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere([
            'hostess_profile.status_id' => Yii::$app->status->active,
            'user.status_id' => Yii::$app->status->active
        ]);

        return $this->createDataProvider($query);
    }

    /**
     * Creates data provider instance with search query applied
     * Search by pending status
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchPending($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere([
            'hostess_profile.status_id' => Yii::$app->status->pending,
            'user.status_id' => Yii::$app->status->pending
        ]);

        return $this->createDataProvider($query);
    }

    /**
     * Creates data provider instance with search query applied
     * Search by deleted status
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDeleted($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere([
            'hostess_profile.status_id' => Yii::$app->status->deleted,
            'user.status_id' => Yii::$app->status->deleted
        ]);

        return $this->createDataProvider($query);
    }

    /**
     * Creates data provider instance with search query applied
     * Search disabled status
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDisabled($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere([
            'hostess_profile.status_id' => Yii::$app->status->disabled,
            'user.status_id' => Yii::$app->status->disabled
        ]);

        return $this->createDataProvider($query);
    }

    /**
     * Creates data provider instance with search query applied
     * Search requested
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchRequested($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere([
            'hostess_profile.admin_approved' => HostessProfile::APPROVAL_REQUESTED
        ]);
        $query->andWhere([
            "<>", "user.status_id", Yii::$app->status->deleted
        ]);

        return $this->createDataProvider($query);
    }

    /**
     * Creates data provider instance with search query applied
     * Search not requested
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchNotRequested($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere([
            'hostess_profile.admin_approved' => HostessProfile::APPROVAL_NOT_REQUESTED
        ]);
        $query->andWhere([
            "<>", "user.status_id", Yii::$app->status->deleted
        ]);

        return $this->createDataProvider($query);
    }

    /**
     * Creates data provider instance with search query applied
     * Search approved
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchApproved($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere([
            'hostess_profile.admin_approved' => HostessProfile::APPROVAL_APPROVED
        ]);
        $query->andWhere([
            "<>", "user.status_id", Yii::$app->status->deleted
        ]);


        $query->addOrderBy(
                [
                    "hostess_profile.approved_at" => SORT_DESC
                ]
        );


        return $this->createDataProvider($query);
    }

    /**
     * @param string $fileName
     * @param ActiveDataProvider $dataProvider
     * @return string
     */
    public function getWidget($fileName, $dataProvider)
    {

        $fileName = str_replace(" ", "-", $fileName);

        $provider = clone($dataProvider);
        $provider->pagination = false;

        $widget = ExportMenu::widget([
                    'dataProvider' => $provider,
                    'columns' => [
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'date_created',
                        'statusLabel',
                        'hostessProfile.approvalStatusLabel',
                        'hostessProfile.approved_at'
                    ],
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false
                    ],
                    'showColumnSelector' => true,
                    'fontAwesome' => true,
                    'target' => ExportMenu::TARGET_SELF,
                    'filename' => $fileName . '-' . date("d-m-Y"),
                    'showConfirmAlert' => false,
        ]);

        return $widget;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param array $additional
     *
     * @return ActiveDataProvider
     */
    private function search($params, $additional = [])
    {
        $query = $this->baseSearch($params);

        if (!empty($additional))
        {
            $query->andFilterWhere($additional);
        }

        return $this->createDataProvider($query);
    }

    /**
     * Prepare query for search
     * @param array $params
     * @return \yii\db\ActiveQuery
     */
    private function baseSearch($params)
    {              
        $params = $this->getCachedSearchParams($params);
        
        $this->load($params);

        $class = $this->modelClass;
        $query = $class::find()
                ->innerJoin('hostess_profile', 'hostess_profile.user_id = user.id')
                // where to eliminate status condition
                ->where(["role" => \common\models\User::ROLE_HOSTESS]);

        if (!$this->validate())
        {
            $query->where('0=1');
            return $query;
        }

        $query->andFilterWhere([
            'user.id' => $this->id,
        ]);

        $query->andFilterWhere([">=", 'user.date_created', $this->date_from]);
        $query->andFilterWhere(["<=", 'user.date_created', $this->date_to]);

        $query->andFilterWhere(['like', 'user.first_name', $this->first_name])
                ->andFilterWhere(['like', 'user.last_name', $this->last_name])
                ->andFilterWhere(['like', 'user.username', $this->username])
                ->andFilterWhere(['like', 'user.email', $this->email]);

        if (!empty($this->ageMin))
        {
            $maxDate = date("Y-m-d", strTotime("{$this->ageMin} years ago"));
            $query->andFilterWhere(["<=", "hostess_profile.birth_date", $maxDate]);
        }

        if (!empty($this->ageMax))
        {
            $minDate = date("Y-m-d", strTotime("{$this->ageMax} years ago"));
            $query->andFilterWhere([">=", "hostess_profile.birth_date", $minDate]);
        }

        $query->andFilterWhere([">=", "hostess_profile.height", $this->heightMin]);
        $query->andFilterWhere(["<=", "hostess_profile.height", $this->heightMax]);

        $query->andFilterWhere([">=", "hostess_profile.weight", $this->weightMin]);
        $query->andFilterWhere(["<=", "hostess_profile.weight", $this->weightMax]);

        $query->andFilterWhere([">=", "hostess_profile.waist_size", $this->waistSizeMin]);
        $query->andFilterWhere(["<=", "hostess_profile.waist_size", $this->waistSizeMax]);

        $query->andFilterWhere([">=", "hostess_profile.shoe_size", $this->shoeSizeMin]);
        $query->andFilterWhere(["<=", "hostess_profile.shoe_size", $this->shoeSizeMax]);

        $query->andFilterWhere([
            'hostess_profile.country_id' => $this->country_id,
            'hostess_profile.hair_color_id' => $this->hair_color_id,
            'hostess_profile.has_tattoo' => $this->hasTattoo,
            'hostess_profile.has_car' => $this->hasCar,
            'hostess_profile.has_driving_licence' => $this->hasDrivingLicense,
            'hostess_profile.has_trade_licence' => $this->hasTradeLicense,
            'hostess_profile.has_health_certificate' => $this->hasHealthCertificate,
            'hostess_profile.is_trained' => $this->isTrained,
            'hostess_profile.has_mobile_app' => $this->hasMobileApp,
            'hostess_profile.gender' => $this->gender,
            'user.status_id' => $this->status_id,
            'hostess_profile.admin_approved' => $this->approval_status
        ]);

        $query->andFilterWhere([">=", 'hostess_profile.approved_at', $this->approved_at]);

        if (!empty($this->languages))
        {
            $query->innerJoin('hostess_language', 'hostess_language.user_id = hostess_profile.user_id');


            $query->andWhere('hostess_language.language_id IN ( ' . implode(", ", $this->languages) . ')');

            $query->groupBy('hostess_language.user_id');

            $query->having('COUNT(DISTINCT hostess_language.language_id) >= ' . count($this->languages));
        }

        if (!empty($this->pref_cities))
        {
            $query->innerJoin('hostess_preferred_city', 'hostess_preferred_city.user_id = hostess_profile.user_id');

            $query->andWhere('hostess_preferred_city.city_id IN ( ' . implode(", ", $this->pref_cities) . ')');

            $query->groupBy('hostess_preferred_city.user_id');

            $query->having('COUNT(DISTINCT hostess_preferred_city.city_id) >= ' . count($this->pref_cities));
        }

        if (!empty($this->pref_jobs))
        {
            $query->innerJoin('hostess_preferred_job', 'hostess_preferred_job.user_id = hostess_profile.user_id');

            $query->andWhere('hostess_preferred_job.event_job_id IN ( ' . implode(", ", $this->pref_jobs) . ')');

            $query->groupBy('hostess_preferred_job.user_id');

            $query->having('COUNT(DISTINCT hostess_preferred_job.event_job_id) >= ' . count($this->pref_jobs));
        }

        return $query;
    }

    /**
     * Create date provider for query
     * @param \yii\db\ActiveQuery $query
     * @return ActiveDataProvider
     */
    private function createDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = new \yii\data\Sort([
            'attributes' => [
                'user_id' => [
                    'asc' => ['user.id' => SORT_ASC],
                    'desc' => ['user.id' => SORT_DESC],
                    'default' => SORT_DESC
                ],
            ],
            'defaultOrder' => [
                'user_id' => SORT_DESC
            ]
        ]);

        $dataProvider->sort = $sort;

        return $dataProvider;
    }

}
