<?php

namespace backend\models\gallery;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\gallery\Gallery;

/**
 * GalleryIndex represents the model behind the search about `\backend\models\gallery\Gallery`.
 */
class GalleryIndex extends Gallery
{

    use CachedSearchTrait;

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * File Name
     * @var string
     */
    public $file_name;

    /**
     * Include On Home
     * @var integer
     */
    public $include_on_home;

    /**
     * Sort Order
     * @var integer
     */
    public $sort_order;

    /**
     * Alt
     * @var string
     */
    public $alt;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'include_on_home', 'sort_order'], 'integer'],
            [['path', 'alt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'sort_order' => SORT_ASC,
                    'include_on_home' => SORT_DESC,
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'include_on_home' => $this->include_on_home,
            'sort_order' => $this->sort_order,
        ]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name])
                ->andFilterWhere(['like', 'alt', $this->alt]);

        return $dataProvider;
    }

    /**
     * Return include on home list
     * @return array
     */
    public function getIncludeList()
    {
        return [
            Yii::t('admin', 'No'),
            Yii::t('admin', 'Yes'),
        ];
    }

}
