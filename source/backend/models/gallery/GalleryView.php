<?php

namespace backend\models\gallery;

use Yii;

/**
 * GalleryView represents the model behind the view about `\backend\models\gallery\Gallery`.
 */
class GalleryView extends Gallery
{

    const SESSION_KEY_BACK_URL = 'gallery.view.backUrl';

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Path
     * @var string
     */
    public $path;

    /**
     * Include On Home
     * @var integer
     */
    public $include_on_home;

    /**
     * Sort Order
     * @var integer
     */
    public $sort_order;

    /**
     * Created By
     * @var integer
     */
    public $created_by;

    /**
     * Updated By
     * @var integer
     */
    public $updated_by;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date Updated
     * @var string
     */
    public $date_updated;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * File Name
     * @var string
     */
    public $file_name;

    /**
     * Alt
     * @var string
     */
    public $alt;

    /**
     * Title
     * @var string
     */
    public $title;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'created_by' => Yii::t('admin', 'Created By'),
            'updated_by' => Yii::t('admin', 'Updated By'),
            'date_created' => Yii::t('admin', 'Date Created'),
            'date_updated' => Yii::t('admin', 'Date Updated'),
            'status_id' => Yii::t('admin', 'Status Id'),
                ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        return [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')],
            'edit_button' => ['class' => 'btn btn-success', 'url' => ['update', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Update')],
            'delete_button' => ['class' => 'btn btn-danger', 'url' => ['delete', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Delete'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this item?')]]
        ];
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
