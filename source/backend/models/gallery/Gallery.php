<?php

namespace backend\models\gallery;

use Yii;

/**
 * Gallery represents the backend model for `\common\models\Gallery`.
 */
class Gallery extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Gallery';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [
            'id' => Yii::t('admin', 'Id'),
            'path' => Yii::t('admin', 'Path'),
            'include_on_home' => Yii::t('admin', 'Include On Home'),
            'sort_order' => Yii::t('admin', 'Sort Order'),
            'file_name' => Yii::t('admin', 'File Name'),
            'alt' => Yii::t('admin', 'Alt'),
            'includeHome' => Yii::t('admin', 'Include On Home'),
                ]
        );
    }

    /**
     * Include on home
     * @return string
     */
    public function getIncludeHome()
    {
        if ($this->modelObject->include_on_home === 1)
        {
            return Yii::t('admin', 'Yes');
        }

        return Yii::t('admin', 'No');
    }

}
