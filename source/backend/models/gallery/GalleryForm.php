<?php

namespace backend\models\gallery;

use Yii;

/**
 * GalleryForm represents the model behind the form about `\backend\models\gallery\Gallery`.
 */
class GalleryForm extends Gallery
{

    const SESSION_KEY_BACK_URL = 'gallery.form.backUrl';

    /**
     * Path
     * @var string
     */
    public $path;

    /**
     * Include On Home
     * @var integer
     */
    public $include_on_home;

    /**
     * Sort Order
     * @var integer
     */
    public $sort_order;

    /**
     * File Name
     * @var string
     */
    public $file_name;

    /**
     * Alt
     * @var string
     */
    public $alt;

    /**
     * Title
     * @var string
     */
    public $title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path'], 'string'],
            [['include_on_home', 'sort_order'], 'integer'],
            [['file_name', 'alt', 'title'], 'required'],
            [['file_name', 'alt', 'title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    public function attributeHints()
    {
        return [
            'path' => Yii::t('admin', 'Make sure to put watermark and compress image!<br>You can compress image using <a href="https://tinypng.com/" target="_blank">TINYPNG</a>.')
        ];
    }

    /**
     * Load and save Gallery     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data))
        {
            $path = $this->uploadFiles();

            $this->setPath($path);

            if ($this->validate())
            {
                $this->modelObject->setAttributes($this->attributes);

                if ($this->modelObject->save())
                {
                    return true;
                }

                $this->addErrors($this->modelObject->getErrors());
            }
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    private function uploadFiles()
    {
        if (isset($_FILES) && isset($_FILES['GalleryForm']))
        {
            $uploaddir = Yii::$app->params['gallery_save_path'];
            $path = $_FILES['GalleryForm']['name']['path'];

            if (!empty($path))
            {
                $file_name = $this->fixName($path);
                $uploadfile = $uploaddir . $file_name;

                if (move_uploaded_file($_FILES['GalleryForm']['tmp_name']['path'], $uploadfile))
                {
                    $this->file_name = $file_name;
                    return '/gallery-images/' . $file_name;
                }
            }
        }

        return '';
    }

    private function setPath($path)
    {
        $this->path = $path;

        if (!$this->modelObject->isNewRecord && empty($this->path))
        {
            $this->path = $this->modelObject->path;
        }
    }

    private function fixName($string)
    {
        $string = trim($string);

        $find = [
            'ö',
            'ü',
            'ä',
            'Ä',
            'Ü',
            'Ö',
            " ",
            "/",
            "\\",
            "(",
            ")",
            "{",
            "}",
            "&",
            "?",
            "="
        ];

        $replace = [
            'oe',
            'ue',
            'ae',
            'Ae',
            'Ue',
            'Oe',
            "-",
            "",
            "",
            "",
            "",
            "",
            "",
            "-",
            "-",
            "-"
        ];

        $string = str_replace($find, $replace, $string);

        $string = preg_replace('/\s+/', '', $string);

        return $string;
    }

}
