<?php

namespace backend\models\dresscode;

use Yii;

/**
 * DressCode represents the backend model for `\common\models\DressCode`.
 */
class DressCode extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\DressCode';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

}
