<?php

namespace backend\models\exhibitortestimonial;

use Yii;

/**
 * ExhibitorTestimonialForm represents the model behind the form about `\backend\models\exhibitortestimonial\ExhibitorTestimonial`.
 */
class ExhibitorTestimonialForm extends ExhibitorTestimonial
{
    const SESSION_KEY_BACK_URL = 'exhibitortestimonial.form.backUrl';

    /**
     * Text
     * @var string
     */
    public $text;

    /**
     * Source
     * @var string
     */
    public $source;

    /**
     * Language Id
     * @var integer
     */
    public $language_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'source', 'language_id'], 'required'],
            [['text'], 'string'],
            [['language_id'], 'integer'],
            [['source'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
            ]
        );
    }

    /**
     * Load and save ExhibitorTestimonial     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            return $this->modelObject->save();
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }
}
