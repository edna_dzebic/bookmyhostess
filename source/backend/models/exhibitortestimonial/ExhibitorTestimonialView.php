<?php

namespace backend\models\exhibitortestimonial;

use Yii;

/**
 * ExhibitorTestimonialView represents the model behind the view about `\backend\models\exhibitortestimonial\ExhibitorTestimonial`.
 */
class ExhibitorTestimonialView extends ExhibitorTestimonial
{
    const SESSION_KEY_BACK_URL = 'exhibitortestimonial.view.backUrl';

     /**
     * Id
     * @var integer
     */
    public $id;

     /**
     * Text
     * @var string
     */
    public $text;

     /**
     * Source
     * @var string
     */
    public $source;

     /**
     * Language Id
     * @var integer
     */
    public $language_id;


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
            ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        return [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')],
            'edit_button' => ['class' => 'btn btn-success', 'url' => ['update', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Update')],
            'delete_button' => ['class' => 'btn btn-danger', 'url' => ['delete', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Delete'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this item?')]]
        ];
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }
}
