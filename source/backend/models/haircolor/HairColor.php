<?php

namespace backend\models\haircolor;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * HairColor represents the backend model for `\common\models\HairColor`.
 */
class HairColor extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\HairColor';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * Return hair color list
     * @return array
     */
    public static function getForDropdownList()
    {
        return ArrayHelper::map(\common\models\HairColor::find()->all(), 'id', 'name');
    }

}
