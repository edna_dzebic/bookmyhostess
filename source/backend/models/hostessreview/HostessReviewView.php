<?php

namespace backend\models\hostessreview;

use Yii;

/**
 * HostessReviewView represents the model behind the view about `\backend\models\hostessreview\HostessReview`.
 */
class HostessReviewView extends HostessReview
{

    const SESSION_KEY_BACK_URL = 'hostessreview.view.backUrl';

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Event Id
     * @var integer
     */
    public $event_id;

    /**
     * Hostess Id
     * @var integer
     */
    public $hostess_id;

    /**
     * Exhibitor Id
     * @var integer
     */
    public $exhibitor_id;

    /**
     * Rating
     * @var double
     */
    public $rating;

    /**
     * Comment
     * @var string
     */
    public $comment;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date Updated
     * @var string
     */
    public $date_updated;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'date_created' => Yii::t('admin', 'Date Created'),
            'date_updated' => Yii::t('admin', 'Date Updated'),
            'status_id' => Yii::t('admin', 'Status Id'),
                ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        return [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')],
            'edit_button' => ['class' => 'btn btn-success', 'url' => ['update', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Update')],
            'delete_button' => ['class' => 'btn btn-danger', 'url' => ['delete', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Delete'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this item?')]]
        ];
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
