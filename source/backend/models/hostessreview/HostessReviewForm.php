<?php

namespace backend\models\hostessreview;

use Yii;
use common\models\HostessReview as HostessReviewModel;

/**
 * HostessReviewForm represents the model behind the form about `\backend\models\hostessreview\HostessReview`.
 */
class HostessReviewForm extends HostessReview
{

    const SESSION_KEY_BACK_URL = 'hostessreview.form.backUrl';

    /**
     * Event Id
     * @var integer
     */
    public $event_id;

    /**
     * Hostess Id
     * @var integer
     */
    public $hostess_id;

    /**
     * Exhibitor Id
     * @var integer
     */
    public $exhibitor_id;

    /**
     * Rating
     * @var double
     */
    public $rating;

    /**
     * Comment
     * @var string
     */
    public $comment;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * Group code
     * @var string
     */
    public $group_code;

    /**
     * Review requested
     * @var boolean 
     */
    public $review_requested;

    /**
     * Event request
     * @var integer
     */
    public $event_request_id;

    /**
     *
     * @var \common\models\Request
     */
    public $requestModel;

    /**
     * @inheritdoc
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if ($this->modelObject->isNewRecord)
        {
            $this->group_code = strtotime("now") . rand(1000, 9999);
            $this->review_requested = 0;
        }

        if (is_object($this->requestModel))
        {
            $this->event_request_id = $this->requestModel->id;
            $this->event_id = $this->requestModel->event_id;
            $this->exhibitor_id = $this->requestModel->requested_user_id;
            $this->hostess_id = $this->requestModel->user_id;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hostess_id', 'event_id', 'exhibitor_id', 'event_request_id'], 'integer'],
            [['hostess_id', 'event_id', 'exhibitor_id', 'rating', 'comment'], 'required'],
            [['comment'], 'filter', 'filter' => 'trim'],
            [['group_code'], 'string', 'max' => 255],
            [['comment', 'group_code'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'event_id' => Yii::t('admin', 'Event'),
            'exhibitor_id' => Yii::t('admin', 'Exhibitor'),
            'hostess_id' => Yii::t('admin', 'Hostess'),
            'comment' => Yii::t('admin', 'Comment'),
            'rating' => Yii::t('admin', 'Rating'),
                ]
        );
    }

    /**
     * Load and save review
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->status_id = Yii::$app->status->active;
            $this->modelObject->setAttributes($this->attributes);

            if ($this->modelObject->save() !== false)
            {
                $this->modelObject->hostess->hostessProfile->recalculateRating();
                return true;
            }

            $this->addErrors($this->modelObject->getErrors());
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        if ($this->modelObject->delete())
        {
            $this->modelObject->hostess->hostessProfile->recalculateRating();
            return true;
        }

        return false;
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    /**
     * 
     * @param array $attributeNames
     * @param boolean $clearErrors
     * @return boolean
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        $query = HostessReviewModel::find()
                ->andWhere([
            "event_id" => $this->event_id,
            "hostess_id" => $this->hostess_id,
            "exhibitor_id" => $this->exhibitor_id
        ]);

        if (!$this->modelObject->isNewRecord)
        {
            $query->andWhere(["<>", 'id', $this->modelObject->id]);
        }

        $found = $query->exists();
        if ($found)
        {
            $this->addError('event_id', Yii::t('admin', 'This review is already added for this hostess for given event for given exhibitor. It is possible that review is still pending!'));
        }

        return !$this->hasErrors();
    }

}
