<?php

namespace backend\models\hostessreview;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * HostessReview represents the backend model for `\common\models\HostessReview`.
 */
class HostessReview extends \common\components\Container
{

    /**
     * @inheritdoc
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\HostessReview';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    public function getHostessList()
    {
        return \backend\models\User::getHostessList();
    }

    public function getExhibitorList()
    {
        return \backend\models\User::getExhibitorList();
    }

    public function getEventList()
    {
        $query = \common\models\Event::find()
                ->andWhere("end_date <= NOW()");

        $models = $query->all();

        return ArrayHelper::map($models, 'id', 'name');
    }
    
     /**
     * 
     * @return array
     */
    public function getStatusList()
    {
        return [
            Yii::$app->status->active => Yii::t('admin', 'Active'),
            Yii::$app->status->pending => Yii::t('admin', 'Pending')
        ];
    }

    /**
     * 
     * @return string
     */
    public function getStatusLabel()
    {
        return $this->modelObject->getStatusLabel();
    }

    /**
     * 
     * @return string
     */
    public function getEventName()
    {
        return $this->modelObject->getEventName();
    }

    /**
     * 
     * @return string
     */
    public function getHostessFullName()
    {
        return $this->modelObject->getHostessFullName();
    }

    /**
     * 
     * @return string
     */
    public function getExhibitorFullName()
    {
        return $this->modelObject->getExhibitorFullName();
    }

}
