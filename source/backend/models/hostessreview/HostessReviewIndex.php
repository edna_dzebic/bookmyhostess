<?php

namespace backend\models\hostessreview;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\hostessreview\HostessReview;

/**
 * HostessReviewIndex represents the model behind the search about `\backend\models\hostessreview\HostessReview`.
 */
class HostessReviewIndex extends HostessReview
{

    use CachedSearchTrait;

    /**
     * Event Id
     * @var integer
     */
    public $event_id;

    /**
     * Hostess Id
     * @var integer
     */
    public $hostess_id;

    /**
     * Exhibitor Id
     * @var integer
     */
    public $exhibitor_id;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Rating
     * @var double
     */
    public $rating;

    /**
     * Date From
     * @var string
     */
    public $date_from;

    /**
     * Date To
     * @var string
     */
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'hostess_id', 'exhibitor_id', 'status_id'], 'integer'],
            [['date_created', 'rating', 'date_from', 'date_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
            'date_from' => Yii::t('admin', 'Date from'),
            'date_to' => Yii::t('admin', 'Date to')
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 
     * @return array
     */
    public function getRatingDropdown()
    {
        return range(0, 5, 0.5);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'event_id' => $this->event_id,
            'hostess_id' => $this->hostess_id,
            'exhibitor_id' => $this->exhibitor_id,
            'status_id' => $this->status_id,
            'rating' => $this->rating
        ]);

        $query->andFilterWhere([">=", 'date_created', $this->date_from]);
        $query->andFilterWhere(["<=", 'date_created', $this->date_to]);

        return $dataProvider;
    }

}
