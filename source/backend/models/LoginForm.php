<?php

namespace backend\models;

class LoginForm extends \common\models\LoginForm
{

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null)
        {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

}
