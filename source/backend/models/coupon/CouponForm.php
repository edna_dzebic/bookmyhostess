<?php

namespace backend\models\coupon;

use Yii;

/**
 * CouponForm represents the model behind the form about `\backend\models\coupon\Coupon`.
 */
class CouponForm extends Coupon
{

    const SESSION_KEY_BACK_URL = 'coupon.form.backUrl';

    /**
     * Bar Code
     * @var string
     */
    public $bar_code;

    /**
     * Code
     * @var string
     */
    public $code;

    /**
     * Amount
     * @var string
     */
    public $amount;

    /**
     * Coupon Type
     * @var integer
     */
    public $coupon_type;

    /**
     * Title
     * @var string
     */
    public $title;

    /**
     * Date from
     * @var string 
     */
    public $date_from;

    /**
     * Date to
     * @var string 
     */
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'coupon_type'], 'required'],
            [['code', 'bar_code'], 'required', 'enableClientValidation' => false],
            [['amount'], 'number'],
            [['coupon_type'], 'integer'],
            [['code', 'title'], 'string', 'max' => 255],
            [['date_from', 'date_to'], 'safe'],
            ['date_from', 'compare', 'compareAttribute' => 'date_to', 'operator' => '<', 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return array_merge(
                parent::attributeHints(), [
            'code' => Yii::t('admin', 'You can leave empty and system will generate code.')
                ]
        );
    }

    /**
     * Load and save Coupon     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data))
        {
            $this->generateCode();
            $this->generateBarCode();

            if ($this->validate())
            {
                $this->modelObject->setAttributes($this->attributes);

                if ($this->modelObject->save())
                {
                    return true;
                }

                $this->addErrors($this->modelObject->getErrors());
            }
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    private function generateCode()
    {
        if (empty($this->code))
        {
            $this->code = "BmH-" . $this->generateRandomString();
        }
    }

    private function generateBarCode()
    {
        $gs1Prefix = "400"; // 3 digits
        $manufacturerCode = Yii::$app->settings->manufacturer_code; // 4 digits
        $productCode = $this->generateProductCode(); // 5 digits

        $code = $gs1Prefix . $manufacturerCode . $productCode;

        $check = $this->calculateCheckDigit($code); // 1 digit;

        $this->bar_code = $code . $check;
    }

    /**
     * 
     * @param int $length
     * @return string
     */
    private function generateProductCode($length = 5)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * 
     * @param string $code
     * @return int
     */
    private function calculateCheckDigit($code)
    {
        $partialSum = 0.0;
        for ($i = 0; $i < strlen($code); $i++)
        {
            $weight = 3;
            if ($i % 2 == 0)
            {
                $weight = 1;
            }

            $partialSum+= (int) $code[$i] * $weight;
        }
        $upLimit = ceil($partialSum / 10) * 10;
        return $upLimit - $partialSum;
    }

    /**
     * 
     * @param int $length
     * @return string
     */
    private function generateRandomString($length = 4)
    {
        $characters = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
