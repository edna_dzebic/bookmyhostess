<?php

namespace backend\models\coupon;

use Yii;

/**
 * Coupon represents the backend model for `\common\models\Coupon`.
 */
class Coupon extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Coupon';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [
            'typeLabel' => Yii::t('admin', 'Coupon Type')
                ], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public function getTypeDropdown()
    {
        return [
            \common\models\Coupon::TYPE_FIX => Yii::t('admin', 'Fixed Amount'),
            \common\models\Coupon::TYPE_PERECENTAGE => Yii::t('admin', 'Percentage Amount'),
        ];
    }

    /**
     * 
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->modelObject->getTypeLabel();
    }

}
