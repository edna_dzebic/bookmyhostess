<?php

namespace backend\models\coupon;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\coupon\Coupon;

/**
 * CouponIndex represents the model behind the search about `\backend\models\coupon\Coupon`.
 */
class CouponIndex extends Coupon
{

    use CachedSearchTrait;

    /**
     * Code
     * @var string
     */
    public $code;

    /**
     * Amount
     * @var string
     */
    public $amount;

    /**
     * Coupon Type
     * @var integer
     */
    public $coupon_type;

    /**
     * Title
     * @var string
     */
    public $title;

    /**
     * Date from
     * @var string
     */
    public $date_from;

    /**
     * Date to
     * @var string
     */
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'title', 'date_from', 'date_to'], 'safe'],
            [['amount'], 'number'],
            [['coupon_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'coupon_type' => $this->coupon_type,
        ]);

        $query->andFilterWhere([">=", 'date_from', $this->date_from]);
        $query->andFilterWhere(["<=", 'date_to', $this->date_to]);

        $query->andFilterWhere(['like', 'code', $this->code])
                ->andFilterWhere(['like', 'amount', $this->amount])
                ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

}
