<?php

namespace backend\models\event;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\event\Event;

/**
 * EventIndex represents the model behind the search about `\backend\models\event\Event`.
 */
class EventIndex extends Event
{

    use CachedSearchTrait;

    /**
     * Country
     * @var int
     */
    public $country_id;

    /**
     * City
     * @var int
     */
    public $city_id;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Venue
     * @var string
     */
    public $venue;

    /**
     * Start Date
     * @var string
     */
    public $start_date;

    /**
     * End Date
     * @var string
     */
    public $end_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'venue', 'start_date', 'end_date', 'country_id', 'city_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Search all past events
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchPast($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere("event.end_date < NOW()");

        return $this->createDataProvider($query);
    }

    /**
     * Search all upcoming events
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchUpcoming($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere("event.end_date >= NOW()");

        return $this->createDataProvider($query);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->baseSearch($params);

        return $this->createDataProvider($query);
    }

    /**
     * Prepare query for search
     * @param array $params
     * @return \yii\db\ActiveQuery
     */
    private function baseSearch($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
        }

        $query->andFilterWhere([
            'country_id' => $this->country_id,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere([">=", 'start_date', $this->start_date]);
        $query->andFilterWhere(["<=", 'end_date', $this->end_date]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'venue', $this->venue]);

        return $query;
    }

    /**
     * Create date provider for query
     * @param \yii\db\ActiveQuery $query
     * @return ActiveDataProvider
     */
    private function createDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        return $dataProvider;
    }

}
