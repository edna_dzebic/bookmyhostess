<?php

namespace backend\models\event;

use Yii;
use common\models\Job;
use common\models\DressCode;
use common\models\EventJob;
use common\models\EventDressCode;

/**
 * EventForm represents the model behind the form about `\backend\models\event\Event`.
 */
class EventForm extends Event
{

    const SESSION_KEY_BACK_URL = 'event.form.backUrl';

    /**
     * Country
     * @var int
     */
    public $country_id;

    /**
     * City
     * @var int
     */
    public $city_id;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Venue
     * @var string
     */
    public $venue;

    /**
     * Url
     * @var string
     */
    public $url;

    /**
     * Brief Description
     * @var string
     */
    public $brief_description;

    /**
     * Start Date
     * @var string
     */
    public $start_date;

    /**
     * End Date
     * @var string
     */
    public $end_date;

    /**
     * Type
     * @var int 
     */
    public $type_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'start_date', 'end_date', 'country_id', 'city_id', 'venue', 'type_id'], 'required'],
            [['start_date', 'end_date'], 'safe'],
            [['url', 'name', 'venue'], 'string', 'max' => 150],
            [['brief_description'], 'string', 'max' => 1450],
            ['start_date', 'compare', 'compareAttribute' => 'end_date', 'operator' => '<=', 'enableClientValidation' => false],
            ['url', 'url']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Load and save Event
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            $isNew = $this->modelObject->isNewRecord;

            $transaction = Yii::$app->db->beginTransaction();

            if ($this->modelObject->save() !== false)
            {
                if ($isNew === true)
                {
                    if ($this->saveJobsAndDressCodes() !== false)
                    {
                        $transaction->commit();
                        return true;
                    }
                } else
                {
                    $transaction->commit();
                    return true;
                }
            }

            $transaction->rollBack();
            return false;
        }

        return false;
    }

    /**
     * By default on creation of new event, all dress codes and jobs must be added to it
     * @return boolean
     */
    private function saveJobsAndDressCodes()
    {
        //event jobs
        $eventJobs = Job::find()->all();
        foreach ($eventJobs as $eventJob)
        {
            $eventJobModel = new EventJob();
            $eventJobModel->event_id = $this->modelObject->id;
            $eventJobModel->job_id = $eventJob->id;

            if ($eventJobModel->save() === false)
            {
                return false;
            }
        }

        //event dress codes
        $dressCodes = DressCode::find()->all();
        foreach ($dressCodes as $eventDressCode)
        {
            $eventDressCodeModel = new EventDressCode();
            $eventDressCodeModel->event_id = $this->modelObject->id;
            $eventDressCodeModel->event_dress_code_id = $eventDressCode->id;

            if ($eventDressCodeModel->save() === false)
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        if($this->modelObject->delete())
        {
            return true;
        }
                
        $this->addErrors($this->modelObject->getErrors());
        return false;
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
