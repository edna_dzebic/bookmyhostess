<?php

namespace backend\models\event;

use Yii;

/**
 * EventView represents the model behind the view about `\backend\models\event\Event`.
 */
class EventView extends Event
{

    const SESSION_KEY_BACK_URL = 'event.view.backUrl';

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Url
     * @var string
     */
    public $url;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Venue
     * @var string
     */
    public $venue;

    /**
     * Brief Description
     * @var string
     */
    public $brief_description;

    /**
     * Start Date
     * @var string
     */
    public $start_date;

    /**
     * End Date
     * @var string
     */
    public $end_date;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * Updated At
     * @var string
     */
    public $date_updated;

    /**
     * Created At
     * @var string
     */
    public $date_created;

    /**
     * Created By
     * @var integer
     */
    public $created_by;

    /**
     * Updated By
     * @var integer
     */
    public $updated_by;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        return [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')],
            'edit_button' => ['class' => 'btn btn-success', 'url' => ['update', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Update')],
            'clone_button' => ['class' => 'btn btn-info', 'url' => ['clone', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Copy as new')],
            'delete_button' => ['class' => 'btn btn-danger', 'url' => ['delete', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Delete'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this item?')]]
        ];
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    /**
     * Get dataprovider for jobs
     * @return \yii\data\ArrayDataProvider
     */
    public function getEventJobsDataProvider()
    {
        return $this->createArrayDataProvider($this->modelObject->eventJobs);
    }

    /**
     * Get dataprovider for dress codes
     * @return \yii\data\ArrayDataProvider
     */
    public function getDressCodesDataProvider()
    {
        return $this->createArrayDataProvider($this->modelObject->eventDressCodes);
    }

    /**
     * Create ArrayDataProvider for models
     * @param $models
     * @return ArrayDataProvider
     */
    private function createArrayDataProvider($models)
    {
        $dataProvider = new \yii\data\ArrayDataProvider();
        $dataProvider->models = $models;
        $dataProvider->pagination = false;

        return $dataProvider;
    }

}
