<?php

namespace backend\models\event;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Event represents the backend model for `\common\models\Event`.
 */
class Event extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Event';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public static function getForDropdownList()
    {
        $models = \common\models\Event::find()->all();
        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->name . " - " . date("Y", strtotime($model->start_date));
        }

        return $result;
    }

    /**
     * 
     * @return array
     */
    public function getCountryList()
    {
        return \backend\models\country\Country::getListForEventDropdown();
    }

    /**
     * 
     * @return array
     */
    public function getCityList()
    {
        return \backend\models\city\City::getListForEventDropdown();
    }

    /**
     * 
     * @return array
     */
    public function getTypeList()
    {
        return \backend\models\type\Type::getForDropdownList();
    }

    /**
     * 
     * @return string
     */
    public function getCountryName()
    {
        return $this->modelObject->getCountryName();
    }

    /**
     * 
     * @return string
     */
    public function getCityName()
    {
        return $this->modelObject->getCityName();
    }

    /**
     * 
     * @return string
     */
    public function getTypeName()
    {
        return $this->modelObject->getTypeName();
    }

}
