<?php

namespace backend\models\event;

use Yii;
use common\models\EventDressCode;
use yii\helpers\ArrayHelper;

class EventDressCodeForm extends \yii\base\Model
{

    const SESSION_KEY_BACK_URL = 'eventdresscode.form.backUrl';

    public $event_id;
    public $event_dress_code_id;

    /**
     * @var Model Instance of model
     */
    protected $modelObject;

    /**
     * EventDressCodeForm constructor
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->event_id = $this->modelObject->id;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'event_dress_code_id'], 'required'],
            [['event_id', 'event_dress_code_id'], 'integer']
        ];
    }

    /**
     * Save new dress code for event
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $model = $this->findModel();

            if (is_object($model))
            {
                return $model->enable();
            } else
            {
                $model = $this->createModel();
                return $model->save();
            }
        }

        return false;
    }

    /**
     * Delete existing dress code
     * @return boolean
     */
    public function delete()
    {
        $model = $this->findModel();

        if (is_object($model))
        {
            return $model->delete();
        }

        return false;
    }

    /**
     * Get list of dress codes
     * @return array
     */
    public function getDressCodeList()
    {
        return ArrayHelper::map(\common\models\DressCode::find()->all(), 'id', 'name');
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    /**
     * @return Model
     */
    protected function getModelObject()
    {
        return $this->modelObject;
    }

    /**
     * @param Model $value
     */
    protected function setModelObject($value)
    {
        $this->modelObject = $value;
    }

    /**
     * Find existing model
     * @return EventDressCode
     */
    private function findModel()
    {
        $model = EventDressCode::find()
                ->where(["event_id" => $this->event_id]) // where to eliminate status condition
                ->andWhere(["event_dress_code_id" => $this->event_dress_code_id])
                ->one();

        return $model;
    }

    /**
     * Create new EventDressCode model
     * @return EventDressCode
     */
    private function createModel()
    {
        $model = new EventDressCode();

        $model->setAttributes($this->getAttributes());

        return $model;
    }

}
