<?php

namespace backend\models\event;

use Yii;
use common\models\EventJob;
use yii\helpers\ArrayHelper;

class EventJobForm extends \yii\base\Model
{

    const SESSION_KEY_BACK_URL = 'eventjob.form.backUrl';

    public $event_id;
    public $job_id;

    /**
     * @var Model Instance of model
     */
    protected $modelObject;

    /**
     * EventJobForm constructor
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->event_id = $this->modelObject->id;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'job_id'], 'required'],
            [['event_id', 'job_id'], 'integer']
        ];
    }

    /**
     * Save new job for event
     * @param array $data
     * @return boolean
     */
    public function loadAndSave($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $model = $this->findModel();

            if (is_object($model))
            {
                return $model->enable();
            } else
            {
                $model = $this->createModel();
                return $model->save();
            }
        }

        return false;
    }

    /**
     * Delete existing event job
     * @return boolean
     */
    public function delete()
    {
        $model = $this->findModel();

        if (is_object($model))
        {
            return $model->delete();
        }

        return false;
    }

    /**
     * Get list of jos
     * @return array
     */
    public function getJobsList()
    {
        return ArrayHelper::map(\common\models\Job::find()->all(), 'id', 'name');
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    /**
     * @return Model
     */
    protected function getModelObject()
    {
        return $this->modelObject;
    }

    /**
     * @param Model $value
     */
    protected function setModelObject($value)
    {
        $this->modelObject = $value;
    }

    /**
     * Find existing model
     * @return EventJob
     */
    private function findModel()
    {
        $model = EventJob::find()
                ->where(["event_id" => $this->event_id]) // where to eliminate status condition
                ->andWhere(["job_id" => $this->job_id])
                ->one();

        return $model;
    }

    /**
     * Create new EventJob model
     * @return EventJob
     */
    private function createModel()
    {
        $model = new EventJob();

        $model->setAttributes($this->getAttributes());

        return $model;
    }

}
