<?php

namespace backend\models\invoice;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\invoice\Invoice;

/**
 * InvoiceIndex represents the model behind the search about `\backend\models\invoice\Invoice`.
 */
class InvoiceIndex extends Invoice
{

    use CachedSearchTrait;

    /**
     * Id
     * @var integer
     */
    public $id;
    
    /**
     * Requested User Id
     * @var integer
     */
    public $requested_user_id;

    /**
     * Amount
     * @var double
     */
    public $amount;

    /**
     * Date Created From
     * @var string
     */
    public $date_created_from;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date Created To
     * @var string
     */
    public $date_created_to;

    /**
     * Real Invoice Id
     * @var string
     */
    public $real_invoice_id;

    /**
     * Coupon Id
     * @var string
     */
    public $coupon_id;

    /**
     * Payment type
     * @var string
     */
    public $payment_type;
    
    /**
     * Payment status
     * @var int
     */
    public $status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requested_user_id', 'id'], 'integer'],
            [['amount', 'status'], 'number'],
            [['date_created', 'date_created_from', 'date_created_to', 'real_invoice_id', 'coupon_id', 'payment_type', 'id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), []
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = $this->createQuery($params);

        return $query->all();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function getDataProvider($params)
    {
        $query = $this->createQuery($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        return $dataProvider;
    }

    private function createQuery($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
        }

        $query->andFilterWhere([
            'requested_user_id' => $this->requested_user_id,
            'date_created' => $this->date_created,
            'coupon_id' => $this->coupon_id,
            'status' => $this->status
        ]);

        $query
                ->andFilterWhere(['=', 'real_invoice_id', $this->real_invoice_id])
                ->andFilterWhere(['=', 'id', $this->id])
                ->andFilterWhere(['=', 'amount', $this->amount])
                ->andFilterWhere(['=', 'payment_type', $this->payment_type]);

        $query->andFilterWhere([">=", 'date_created', $this->date_created_from]);
        $query->andFilterWhere(["<=", 'date_created', $this->date_created_to]);
        
        return $query;
    }

}
