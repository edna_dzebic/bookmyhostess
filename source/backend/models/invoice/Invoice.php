<?php

namespace backend\models\invoice;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Invoice represents the backend model for `\common\models\Invoice`.
 */
class Invoice extends \common\components\Container
{

    /**
     * 
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Invoice';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public function getExhibitorList()
    {
        return \backend\models\User::getExhibitorList();
    }

    /**
     * 
     * @return array
     */
    public function getCouponList()
    {

        $models = \common\models\Coupon::find()->all();

        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->title . " - " . $model->code;
        }

        return $result;
    }
    
    /**
     * 
     * @return string
     */
    public function getIsReminderAllowedLabel()
    {
        return $this->modelObject->getIsReminderAllowedLabel();
    }
    
    /**
     * 
     * @return string
     */
    public function getIsExhibitorInGermanyLabel()
    {
        return $this->modelObject->getIsExhibitorInGermanyLabel();
    }

    /**
     * 
     * @return string
     */
    public function getPaymentTypeLabel()
    {
        return $this->modelObject->getPaymentTypeLabel();
    }
    
    /**
     * 
     * @return string
     */
    public function getPaymentStatusLabel()
    {
        return $this->modelObject->getPaymentStatusLabel();
    }
    
    /**
     * 
     * @return string
     */
    public function getExhibitorFullName()
    {
        return $this->modelObject->getExhibitorFullName();
    }

    /**
     * 
     * @return string
     */
    public function getRealNumberLabel()
    {
        return $this->modelObject->getRealIdText();
    }

    /**
     * 
     * @return string
     */
    public function getInvoiceTypeLabel()
    {
        switch ($this->modelObject->invoice_type)
        {
            case \common\models\Country::INVOICE_EU:
                return Yii::t('admin', 'EU');
            case \common\models\Country::INVOICE_DE:
                return Yii::t('admin', 'Inland');
            default :
                return Yii::t('admin', 'Ausland');
        }
    }

    /**
     * 
     * @return string
     */
    public function getCouponTypeLabel()
    {
        if (is_object($this->modelObject->coupon))
        {
            return $this->modelObject->coupon->getTypeLabel();
        }

        return '';
    }

    public function getPaymentTypeList()
    {
        return [
            \common\models\Invoice::PAYMENT_TYPE_INVOICE => Yii::t('admin', 'Invoice'),
            \common\models\Invoice::PAYMENT_TYPE_PAYPAL => Yii::t('admin', 'PayPal'),
            \common\models\Invoice::PAYMENT_TYPE_RECEIPT => Yii::t('admin', 'Receipt'),
        ];
    }

    public function getPaymentStatusList()
    {
        return [
            \common\models\Invoice::STATUS_PAID => Yii::t('admin', 'Paid'),
            \common\models\Invoice::STATUS_NOT_PAID => Yii::t('admin', 'Not Paid'),
        ];
    }

}
