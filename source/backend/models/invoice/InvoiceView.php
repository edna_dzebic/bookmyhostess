<?php

namespace backend\models\invoice;

use Yii;

/**
 * InvoiceView represents the model behind the view about `\backend\models\invoice\Invoice`.
 */
class InvoiceView extends Invoice
{

    const SESSION_KEY_BACK_URL = 'invoice.view.backUrl';

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Requested User Id
     * @var integer
     */
    public $requested_user_id;

    /**
     * Amount
     * @var double
     */
    public $amount;

    /**
     * Status
     * @var integer
     */
    public $status;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date Paid
     * @var string
     */
    public $date_paid;

    /**
     * Paypal Transaction Id
     * @var string
     */
    public $paypal_transaction_id;

    /**
     * Real Invoice Id
     * @var string
     */
    public $real_invoice_id;

    /**
     * Filename
     * @var string
     */
    public $filename;

    /**
     * Coupon Code
     * @var string
     */
    public $coupon_code;

    /**
     * Coupon Amount
     * @var string
     */
    public $coupon_amount;

    /**
     * Coupon Id
     * @var integer
     */
    public $coupon_id;

    /**
     * Language
     * @var string
     */
    public $language;

    /**
     * Coupon Type
     * @var integer
     */
    public $coupon_type;

    /**
     * Is Exhibitor German
     * @var integer
     */
    public $is_exhibitor_german;

    /**
     * Invoice Type
     * @var integer
     */
    public $invoice_type;

    /**
     * Exhibitor Company Name
     * @var string
     */
    public $exhibitor_company_name;

    /**
     * Exhibitor Street
     * @var string
     */
    public $exhibitor_street;

    /**
     * Exhibitor Postal Code
     * @var string
     */
    public $exhibitor_postal_code;

    /**
     * Exhibitor City
     * @var string
     */
    public $exhibitor_city;

    /**
     * Exhibitor Country
     * @var string
     */
    public $exhibitor_country;

    /**
     * Exhibitor Tax Number
     * @var string
     */
    public $exhibitor_tax_number;

    /**
     * Net Amount
     * @var string
     */
    public $net_amount;

    /**
     * Tax Amount
     * @var string
     */
    public $tax_amount;

    /**
     * Gross Amount
     * @var string
     */
    public $gross_amount;

    /**
     * Total Price With Discount
     * @var string
     */
    public $total_price_with_discount;

    /**
     * Coupon Netto Amount
     * @var string
     */
    public $coupon_netto_amount;

    /**
     * Coupon Tax Amount
     * @var string
     */
    public $coupon_tax_amount;

    /**
     * Coupon Gross Amount
     * @var string
     */
    public $coupon_gross_amount;

    /**
     * Payment type
     * @var string
     */
    public $payment_type;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        $buttons = [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')],
            'download' => [
                'class' => 'btn btn-default',
                'url' => Yii::$app->urlManagerFrontend->createUrl([$this->filename]),
                'label' => Yii::t('admin', 'Download'),
                'options' => ['target' => "_blank"]
            ]
        ];

        if ($this->modelObject->getIsNormalInvoice())
        {
            $buttons["reverse"] = [
                'class' => 'btn btn-default',
                'url' => ['reverse', "id" => $this->modelObject->id],
                'label' => Yii::t('admin', 'Reverse'),
                'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to reverse this invoice?')]
            ];

            if ($this->modelObject->status !== \common\models\Invoice::STATUS_PAID)
            {
                $buttons["mark_paid"] = [
                    'class' => 'btn btn-default',
                    'url' => ['mark-as-paid', "id" => $this->modelObject->id],
                    'label' => Yii::t('admin', 'Mark as paid'),
                    'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to mark this invoice as paid?')]
                ];

                if ($this->modelObject->isReminderEnabled())
                {

                    $buttons["disable_reminder"] = [
                        'class' => 'btn btn-default',
                        'url' => ['disable-reminder', "id" => $this->modelObject->id],
                        'label' => Yii::t('admin', 'Disable reminder'),
                        'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to disable payment reminder?')]
                    ];
                } else
                {

                    $buttons["enable_reminder"] = [
                        'class' => 'btn btn-default',
                        'url' => ['enable-reminder', "id" => $this->modelObject->id],
                        'label' => Yii::t('admin', 'Enable reminder'),
                        'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to enable payment reminder?')]
                    ];
                }
            }
        }

        return $buttons;
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    /**
     * Mark invoice as paid
     * @return boolean
     */
    public function markAsPaid()
    {
        if ($this->modelObject->status !== \common\models\Invoice::STATUS_PAID)
        {
            $this->modelObject->status = \common\models\Invoice::STATUS_PAID;
            return $this->modelObject->update() !== false;
        }

        return true;
    }

    /**
     * Disable reminder
     * @return boolean
     */
    public function disableReminder()
    {
        if ($this->modelObject->isReminderEnabled())
        {
            $this->modelObject->is_reminder_allowed = 0;
            return $this->modelObject->update() !== false;
        }

        return true;
    }

    /**
     * Disable reminder
     * @return boolean
     */
    public function enableReminder()
    {
        if ($this->modelObject->isReminderDisabled())
        {
            $this->modelObject->is_reminder_allowed = 1;
            return $this->modelObject->update() !== false;
        }

        return true;
    }

}
