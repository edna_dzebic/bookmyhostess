<?php

namespace backend\models\register;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Register represents the backend model for `\common\models\Register`.
 */
class Register extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Register';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public function getHostessList()
    {
        $models = \backend\models\User::find()
                ->innerJoin("hostess_profile", "hostess_profile.user_id = user.id")
                ->andWhere("hostess_profile.admin_approved = " . \common\models\HostessProfile::APPROVAL_APPROVED)
                ->andWhere(["user.role" => \backend\models\User::ROLE_HOSTESS])
                ->all();

        $result = [];
        foreach ($models as $model)
        {
            $result[$model->id] = $model->getFullName();
        }

        return $result;
    }

    /**
     * 
     * @return array
     */
    public function getEventList()
    {
        return ArrayHelper::map(\common\models\Event::find()
                                ->andWhere("end_date >= NOW()")
                                ->all(), 'id', 'name');
    }

    /**
     * 
     * @return string
     */
    public function getEventName()
    {
        return $this->modelObject->getEventName();
    }

    /**
     * 
     * @return string
     */
    public function getUserFullName()
    {
        return $this->modelObject->getUserFullName();
    }

}
