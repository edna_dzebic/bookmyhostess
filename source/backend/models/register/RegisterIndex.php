<?php

namespace backend\models\register;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\register\Register;
use kartik\export\ExportMenu;

/**
 * RegisterIndex represents the model behind the search about `\backend\models\register\Register`.
 */
class RegisterIndex extends Register
{

    use CachedSearchTrait;

    /**
     * User Id
     * @var integer
     */
    public $user_id;

    /**
     * Event Id
     * @var integer
     */
    public $event_id;

    /**
     * Price
     * @var string
     */
    public $price;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'event_id'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'event_id' => $this->event_id,
            'price' => $this->price,
        ]);

        return $dataProvider;
    }

    
     /**
     * @param string $fileName
     * @param ActiveDataProvider $dataProvider
     * @return string
     */
    public function getWidget($fileName, $dataProvider)
    {

        $fileName = str_replace(" ", "-", $fileName);

        $provider = clone($dataProvider);
        $provider->pagination = false;

        $widget = ExportMenu::widget([
                    'dataProvider' => $provider,
                    'columns' => [
                        'eventName',
                        'userUserName',
                        'userFirstName',
                        'userLastName',
                        'userEmail',
                        'userPhone',
                        'price'
                    ],
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false
                    ],
                    'showColumnSelector' => true,
                    'fontAwesome' => true,
                    'target' => ExportMenu::TARGET_SELF,
                    'filename' => $fileName . '-' . date("d-m-Y"),
                    'showConfirmAlert' => false,
        ]);

        return $widget;
    }
}
