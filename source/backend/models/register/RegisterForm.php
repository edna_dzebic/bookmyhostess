<?php

namespace backend\models\register;

use Yii;
use common\models\Register as RegisterModel;

/**
 * RegisterForm represents the model behind the form about `\backend\models\register\Register`.
 */
class RegisterForm extends Register
{

    const SESSION_KEY_BACK_URL = 'register.form.backUrl';

    /**
     * User Id
     * @var integer
     */
    public $user_id;

    /**
     * Event Id
     * @var integer
     */
    public $event_id;

    /**
     * Price
     * @var string
     */
    public $price;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'event_id', 'price'], 'required'],
            [['user_id', 'event_id'], 'integer'],
            [['price'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Load and save Register     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            return $this->modelObject->save();
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        $query = RegisterModel::find()
                        ->andWhere([
                            "user_id" => $this->user_id,
                            "event_id" => $this->event_id
                        ]);

        if (!$this->modelObject->isNewRecord)
        {
            $query->andWhere(["<>", "id", $this->modelObject->id]);
        }

        $found = $query->exists();
        
        if ($found)
        {
            $this->addError('event_id', Yii::t('admin', 'This hostess is already registered for this event.'));
        }

        return !$this->hasErrors();
    }

}
