<?php

namespace backend\models\type;

use Yii;

/**
 * TypeForm represents the model behind the form about `\backend\models\type\Type`.
 */
class TypeForm extends Type
{

    const SESSION_KEY_BACK_URL = 'type.form.backUrl';

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return array_merge(
                parent::attributeHints(), [
            'name' => Yii::t('admin', 'Please write name in English')
                ]
        );
    }

    /**
     * Load and save Type     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            return $this->modelObject->save();
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
