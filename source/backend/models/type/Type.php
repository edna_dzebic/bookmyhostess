<?php

namespace backend\models\type;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Type represents the backend model for `\common\models\Type`.
 */
class Type extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Type';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public static function getForDropdownList()
    {
        return ArrayHelper::map(\common\models\Type::find()->all(), 'id', 'name');
    }

}
