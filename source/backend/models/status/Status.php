<?php

namespace backend\models\status;

/**
 * Status represents the backend model for `\common\models\Status`.
 */
class Status extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Status';

        parent::__construct($config);
    }

}
