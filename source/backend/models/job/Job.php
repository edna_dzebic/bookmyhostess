<?php

namespace backend\models\job;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Job represents the backend model for `\common\models\Job`.
 */
class Job extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Job';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public static function getForDropdownList()
    {
        return ArrayHelper::map(\common\models\Job::find()->all(), 'id', 'name');
    }

}
