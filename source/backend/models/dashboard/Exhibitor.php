<?php

namespace backend\models\dashboard;

use Yii;
use common\models\User;

class Exhibitor extends \yii\base\Model
{

    public function getRegistrationsPerMonth()
    {
        $values = User::find()
                ->innerJoin('exhibitor_profile', 'user.id = exhibitor_profile.user_id')
                ->where([ // to eliminate not deleted in query
                    "user.role" => User::ROLE_EXHIBITOR
                ]) // to eliminate status from query
                ->select(["COUNT(*) as cnt", "YEAR(user.date_created) as yearCreated", "MONTH(user.date_created) as monthCreated"])
                ->groupBy(["YEAR(user.date_created)", "MONTH(user.date_created)"])
                ->having(["yearCreated" => date("Y")])
                ->asArray()
                ->all();

        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $data = [
            [
                "month" => Yii::t('admin', 'Jan'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Feb'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Mar'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Apr'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'May'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Jun'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Jul'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Aug'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Sep'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Oct'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Nov'),
                "count" => 0
            ],
            [
                "month" => Yii::t('admin', 'Dec'),
                "count" => 0
            ],
        ];

        foreach ($months as $month)
        {
            $key = array_search($month, array_column($values, 'monthCreated'));

            if ($key !== false)
            {
                $data[$values[$key]["monthCreated"] - 1]["count"] = $values[$key]["cnt"];
            }
        }

        $labels = [
            Yii::t('admin', 'Count')
        ];

        return [
            "labels" => $labels,
            "xkey" => 'month',
            "ykeys" => ["count"],
            "dataset" => $data,
        ];
    }

    public function getCreatedToday()
    {
        $query = User::find()
                ->innerJoin('exhibitor_profile', 'user.id = exhibitor_profile.user_id')
                ->andWhere([
            "user.role" => User::ROLE_EXHIBITOR,
            "DATE(user.date_created)" => date("Y-m-d")
        ]);

        return $query->count();
    }

    public function getActiveCount()
    {
        return $this->getByStatus(Yii::$app->status->active);
    }

    public function getDisabledCount()
    {
        return $this->getByStatus(Yii::$app->status->disabled);
    }

    public function getPendingCount()
    {
        return $this->getByStatus(Yii::$app->status->pending);
    }

    public function getDeletedCount()
    {
        return $this->getByStatus(Yii::$app->status->deleted);
    }

    private function getByStatus($status)
    {
        $query = User::find()
                ->innerJoin('exhibitor_profile', 'user.id = exhibitor_profile.user_id')
                ->where([ // to eliminate not deleted in query
            "user.role" => User::ROLE_EXHIBITOR,
            "user.status_id" => $status
        ]);

        return $query->count();
    }

}
