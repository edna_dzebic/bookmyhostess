<?php

namespace backend\models\dashboard;

use Yii;
use common\models\Request as RequestModel;

class Request extends \yii\base\Model
{

    public function getAmountThisMonth()
    {
        $values = RequestModel::find()
                ->select([
                    "count(*) as cnt",
                    "date_created"
                ])
                ->andWhere([
                    "YEAR(date_created)" => date("Y"),
                    "MONTH(date_created)" => date("m")
                ])
                ->groupBy(["DAY(date_created)"])
                ->asArray()
                ->all();

        $labels = [];
        $data = [];

        foreach ($values as $record)
        {
            $time = strtotime($record["date_created"]);
            $t = date("Y-m-d", $time);
            $y = (int) $record["cnt"];

            $labels[] = $t;

            $data[] = [
                "t" => $t,
                "y" => $y
            ];
        }

        return [
            "labels" => $labels,
            "dataset" => $data,
            "label" => Yii::t('admin', 'Number')
        ];
    }

}
