<?php

namespace backend\models\dashboard;

use Yii;
use common\models\Invoice as InvoiceModel;

class Invoice extends \yii\base\Model
{

    public function getAmountThisMonth()
    {
        $values = InvoiceModel::find()
                ->select([
                    "sum(amount) as amount",
                    "date_created"
                ])
                ->andWhere([
                    "YEAR(date_created)" => date("Y"),
                    "MONTH(date_created)" => date("m")
                ])
                ->groupBy(["DAY(date_created)"])
                ->asArray()
                ->all();

        $labels = [];
        $data = [];

        foreach ($values as $record)
        {
            $time = strtotime($record["date_created"]);
            $t = date("Y-m-d", $time);
            $y = (int) $record["amount"];

            $labels[] = $t;

            $data[] = [
                "t" => $t,
                "y" => $y
            ];
        }

        return [
            "labels" => $labels,
            "dataset" => $data,
            "label" => Yii::t('admin', 'Amount')
        ];
    }

}
