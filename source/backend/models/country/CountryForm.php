<?php

namespace backend\models\country;

use Yii;

/**
 * CountryForm represents the model behind the form about `\backend\models\country\Country`.
 */
class CountryForm extends Country
{

    const SESSION_KEY_BACK_URL = 'country.form.backUrl';

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Invoice Type
     * @var integer
     */
    public $invoice_type;

    /**
     * Currency Label
     * @var string
     */
    public $currency_label;

    /**
     * Currency Symbol
     * @var string
     */
    public $currency_symbol;

    /**
     * Booking Price
     * @var string
     */
    public $booking_price;

    /**
     * Is EU member
     * @var integer
     */
    public $is_eu;

    /**
     * Is invoice payment allowed 
     * @var integer
     */
    public $is_invoice_payment_allowed;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'invoice_type', 'booking_price', 'currency_label', 'currency_symbol', 'is_eu', 'is_invoice_payment_allowed'], 'required'],
            [['invoice_type'], 'integer'],
            [['booking_price'], 'number'],
            [['name'], 'string', 'max' => 50],
            [['currency_label', 'currency_symbol'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return array_merge(
                parent::attributeHints(), [
            'name' => Yii::t('admin', 'Please write name in English')
                ]
        );
    }

    /**
     * Load and save Country     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            return $this->modelObject->save();
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
