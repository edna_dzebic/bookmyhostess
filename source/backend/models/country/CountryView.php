<?php

namespace backend\models\country;

use Yii;

/**
 * CountryView represents the model behind the view about `\backend\models\country\Country`.
 */
class CountryView extends Country
{

    const SESSION_KEY_BACK_URL = 'country.view.backUrl';

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Invoice Type
     * @var integer
     */
    public $invoice_type;

    /**
     * Currency Label
     * @var string
     */
    public $currency_label;

    /**
     * Currency Symbol
     * @var string
     */
    public $currency_symbol;

    /**
     * Booking Price
     * @var string
     */
    public $booking_price;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date Updated
     * @var string
     */
    public $date_updated;

    /**
     * Created By
     * @var integer
     */
    public $created_by;

    /**
     * Updated By
     * @var integer
     */
    public $updated_by;

    /**
     * Is EU member
     * @var integer
     */
    public $is_eu;

    /**
     * Is invoice payment allowed 
     * @var integer
     */
    public $is_invoice_payment_allowed;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        return [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')],
            'edit_button' => ['class' => 'btn btn-success', 'url' => ['update', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Update')],
            'delete_button' => ['class' => 'btn btn-danger', 'url' => ['delete', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Delete'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this item?')]]
        ];
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
