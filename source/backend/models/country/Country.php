<?php

namespace backend\models\country;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Country represents the backend model for `\common\models\Country`.
 */
class Country extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\Country';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [
            'invoiceTypeLabel' => Yii::t('admin', 'Invoice Type'),
            'isInvoicePaymentAllowed' => Yii::t('admin', 'Is payment on invoice allowed'),
            'isEU' => Yii::t('admin', 'Member of EU')
                ], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public static function getForDropdownList()
    {
        return ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'name');
    }

    /**
     * 
     * @return array
     */
    public static function getListForEventDropdown()
    {
        return ArrayHelper::map(\common\models\Country::find()->innerJoin('country_price', 'country_price.country_id=country.id')->all(), "id", "name");
    }

    /**
     * 
     * @return array
     */
    public function getInvoiceTypesForDropdown()
    {
        return [
            \common\models\Country::INVOICE_EU => Yii::t('admin', 'EU'),
            \common\models\Country::INVOICE_DE => Yii::t('admin', 'Inland'),
            \common\models\Country::INVOCE_OTHER => Yii::t('admin', 'Ausland')
        ];
    }

    /**
     * 
     * @return string
     */
    public function getInvoiceTypeLabel()
    {
        switch ($this->modelObject->invoice_type)
        {
            case \common\models\Country::INVOICE_EU:
                return Yii::t('admin', 'EU');
            case \common\models\Country::INVOICE_DE:
                return Yii::t('admin', 'Inland');
            default :
                return Yii::t('admin', 'Ausland');
        }
    }

    /**
     * Is member of EU
     * @return string
     */
    public function getIsEU()
    {
        if ($this->modelObject->is_eu === 1)
        {
            return Yii::t('admin', 'Yes');
        }

        return Yii::t('admin', 'No');
    }

    /**
     * Is invoice payment allowed
     * @return string
     */
    public function getIsInvoicePaymentAllowed()
    {
        if ($this->modelObject->is_invoice_payment_allowed === 1)
        {
            return Yii::t('admin', 'Yes');
        }

        return Yii::t('admin', 'No');
    }

}
