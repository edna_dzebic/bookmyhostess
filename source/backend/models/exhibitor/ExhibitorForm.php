<?php

namespace backend\models\exhibitor;

use Yii;

/**
 * ExhibitorForm represents the model behind the form about `\backend\models\exhibitor\Exhibitor`.
 */
class ExhibitorForm extends Exhibitor
{

    const SESSION_KEY_BACK_URL = 'exhibitor.form.backUrl';

    public $user_id;
    public $lang;
    public $gender;
    public $first_name;
    public $last_name;
    public $email;
    public $phone_number;
    public $company_name;
    public $tax_number;
    public $street;
    public $home_number;
    public $postal_code;
    public $city;
    public $country_id;
    public $website;
    public $booking_type;
    public $staff_demand_type;
    public $booking_price_per_day;
    public $booking_price_discount;
    public $password;
    public $is_invoice_payment_allowed;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required', "on" => "create"],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            [['gender', 'first_name', 'last_name', 'email', 'lang', 'booking_type', 'staff_demand_type', 'is_invoice_payment_allowed'], 'required'],
            [['user_id', 'country_id', 'booking_type', 'staff_demand_type'], 'integer'],
            [['booking_price_discount'], 'number'],
            [['phone_number'], 'safe'],
            [['company_name', 'street', 'city', 'website'], 'string', 'max' => 255],
            [['tax_number', 'home_number', 'postal_code'], 'string', 'max' => 50]
        ];
    }

    /**
     * 
     * @return array
     */
    public function attributeHints()
    {
        return [
            'booking_price_discount' => Yii::t('admin', 'You can enter discount which will be applied for all countries'),
            'booking_price_per_day' => Yii::t('admin', 'If you choose booking type "Free", price will be automatically set to 0. <br> If you choose booking type "Normal", price will be automatically set to regular booking price of ' . Yii::$app->settings->booking_price . ' EUR per day. <br> If you choose booking type "Special", enter booking price per day. <br>'),
            'website' => Yii::t('admin', 'Please enter with http or https ( http://website.domain or https://website.domain )')
        ];
    }

    public function isNew()
    {
        return $this->modelObject->isNewRecord;
    }

    /**
     * Load and save Exhibitor
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->setUserModel();

            $transaction = Yii::$app->db->beginTransaction();

            if ($this->modelObject->save())
            {
                $this->setProfileModel();

                if ($this->profileObject->save())
                {
                    $transaction->commit();
                    return true;
                }
            }

            $this->addErrors($this->modelObject->getErrors());
            $this->addErrors($this->profileObject->getErrors());

            $transaction->rollBack();
            return false;
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->modelObject->delete() !== false)
        {
            if ($this->profileObject->delete() !== false)
            {
                $transaction->commit();
                return true;
            }
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * Disable model
     *
     * @returns integer|false Number of rows deleted
     */
    public function disable()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->modelObject->disable() !== false)
        {
            if ($this->profileObject->disable() !== false)
            {
                $transaction->commit();
                return true;
            }
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * Disable model
     *
     * @returns boolean
     */
    public function enable()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->modelObject->enable() !== false)
        {
            if ($this->profileObject->enable() !== false)
            {
                $transaction->commit();
                return true;
            }
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * Activate user profile
     * @return boolean
     */
    public function activate()
    {
        if ($this->modelObject->activate() !== false)
        {
            $emailSent = \common\models\Email::sendActivateByAdminEmailToExhibitor($this->modelObject);

            if ($emailSent !== true)
            {
                $this->modelObject->addError('email', Yii::t('admin', 'New email not sent!'));
            }
            
            return true;
        }

        $this->addErrors($this->modelObject->getErrors());
        return false;
    }

    /**
     * Set attributes to the user model
     */
    private function setUserModel()
    {
        $this->modelObject->setAttributes($this->attributes);
        $this->modelObject->role = \common\models\User::ROLE_EXHIBITOR;
        $this->modelObject->username = $this->email;

        if ($this->modelObject->isNewRecord)
        {
            $this->modelObject->status_id = Yii::$app->status->active;
            $this->modelObject->setPassword($this->password);
            $this->modelObject->generateAuthKey();
            $this->modelObject->activated_at = date("Y-m-d H:i:s");
        }
    }

    /**
     * Set attributes for profile model
     */
    private function setProfileModel()
    {
        $this->profileObject->setAttributes($this->attributes);
        $this->profileObject->user_id = $this->modelObject->id;

        $this->setBookingPrice();

        if ($this->modelObject->isNewRecord)
        {
            $this->profileObject->status_id = Yii::$app->status->active;
        }
    }

    /**
     * Set booking price based on selected booking type
     */
    private function setBookingPrice()
    {
        if ($this->profileObject->booking_type == \common\models\ExhibitorProfile::BOOKING_TYPE_FREE)
        {
            $this->profileObject->booking_price_per_day = 0;
        }
    }

}
