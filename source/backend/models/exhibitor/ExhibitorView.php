<?php

namespace backend\models\exhibitor;

use Yii;

/**
 * ExhibitorView represents the model behind the view about `\backend\models\exhibitor\Exhibitor`.
 */
class ExhibitorView extends Exhibitor
{

    const SESSION_KEY_BACK_URL = 'exhibitor.view.backUrl';

    public $user_id;
    public $lang;
    public $gender;
    public $first_name;
    public $last_name;
    public $email;
    public $phone_number;
    public $company_name;
    public $tax_number;
    public $street;
    public $home_number;
    public $postal_code;
    public $city;
    public $country_id;
    public $website;
    public $booking_type;
    public $staff_demand_type;
    public $booking_price_discount;
    public $is_invoice_payment_allowed;
    public $status_id;
    public $date_created;
    public $date_updated;
    public $created_by;
    public $updated_by;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        $buttons = [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')]
        ];

        if (!$this->modelObject->isDeleted())
        {
            $buttons["delete_button"] = ['class' => 'btn btn-danger', 'url' => ['delete', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Delete'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this user?')]];

            $buttons["edit_button"] = ['class' => 'btn btn-success', 'url' => ['update', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Update')];
        }

        if ($this->modelObject->isPending())
        {
            $buttons["disable_button"] = ['class' => 'btn btn-default', 'url' => ['disable', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Disable'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to disable this user?')]];

            $buttons["activate_button"] = ['class' => 'btn btn-info', 'url' => ['activate', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Activate'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to activate this user?')]];
        }

        if ($this->modelObject->isDisabled())
        {
            $buttons["enable_button"] = ['class' => 'btn btn-info', 'url' => ['enable', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Enable'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to enable this user?')]];
        }

        if ($this->modelObject->isActive())
        {
            $buttons["disable_button"] = ['class' => 'btn btn-default', 'url' => ['disable', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Disable'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to disable this user?')]];
        }

        return $buttons;
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
