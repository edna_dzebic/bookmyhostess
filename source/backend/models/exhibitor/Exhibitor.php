<?php

namespace backend\models\exhibitor;

use Yii;
use common\models\ExhibitorProfile;

/**
 * Exhibitor represents the backend model for `\common\models\ExhibitorProfile`.
 */
class Exhibitor extends \common\components\Container
{

    /**
     * Profile Model Object
     * @var \common\models\ExhibitorProfile 
     */
    public $profileObject;

    /**
     * Exhibitor constructor
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->modelClass = '\backend\models\User';
        $this->profileObject = new ExhibitorProfile();

        parent::__construct($config);

        if (is_object($this->modelObject->exhibitorProfile))
        {
            $this->profileObject = $this->modelObject->exhibitorProfile;
        }

        $this->setAttributes($this->profileObject->attributes, false);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), $this->profileObject->attributeLabels(), [
            'gender' => Yii::t('admin', 'Gender'),
            'first_name' => Yii::t('admin', 'First Name'),
            'last_name' => Yii::t('admin', 'Last name'),
            'email' => Yii::t('admin', 'Email'),
            'date_created' => Yii::t('admin', 'Date Created'),
            'date_from' => Yii::t('admin', 'Created From'),
            'date_to' => Yii::t('admin', 'Created To'),
            'city' => Yii::t('admin', 'City'),
            'country_id' => Yii::t('admin', 'Country'),
            'tax_number' => Yii::t('admin', 'Tax number'),
            'booking_type' => Yii::t('admin', 'Booking Type'),
            'staff_demand_type' => Yii::t('admin', 'Staff Demand Type'),
            'status_id' => Yii::t('admin', 'Status'),
            'statusLabel' => Yii::t('admin', 'Status'),
            'isInvoicePaymentAllowed' => Yii::t('admin', 'Is payment on invoice allowed'),
            'phone_number' => Yii::t('admin', 'Phone number'),
                ], parent::attributeLabels()
        );
    }

    /**
     * Get list of languages for preferred system language
     * @return array
     */
    public function getLanguageList()
    {
        return \common\models\Language::getPrefferedLanguageList();
    }

    /**
     * Get gender list
     * @return type
     */
    public function getGenderList()
    {
        return [
            0 => Yii::t('admin', 'Female'),
            1 => Yii::t('admin', 'Male'),
        ];
    }

    /**
     * Return list of countries
     * @return array
     */
    public function getCountryList()
    {
        return \backend\models\country\Country::getForDropdownList();
    }

    /**
     * Get list of booking types
     * @return array
     */
    public static function getBookingTypesList()
    {
        return [
            \common\models\ExhibitorProfile::BOOKING_TYPE_FREE => Yii::t('admin', 'Free'),
            \common\models\ExhibitorProfile::BOOKING_TYPE_REGULAR => Yii::t('admin', 'Normal'),
            \common\models\ExhibitorProfile::BOOKING_TYPE_SPECIAL => Yii::t('admin', 'Special')
        ];
    }

    /**
     * Get list of staff demand types
     * @return array
     */
    public function getStaffDemandTypesList()
    {
        return \common\models\ExhibitorProfile::getBookingStaffDemandTypesForDropdown();
    }

    /**
     * Country name
     * @return string
     */
    public function getCountryName()
    {
        if (is_object($this->profileObject->country))
        {
            return $this->profileObject->country->name;
        }

        return '';
    }

    /**
     * Staff demand type
     * @return string
     */
    public function getStaffDemandTypeLabel()
    {
        return $this->profileObject->getStaffDemandTypeLabel();
    }

    /**
     * Booking type
     * @return string
     */
    public function getBookingTypeLabel()
    {
        return $this->profileObject->getBookingTypeLabel();
    }

    /**
     * Is invoice payment allowed
     * @return string
     */
    public function getIsInvoicePaymentAllowed()
    {
        if ($this->profileObject->is_invoice_payment_allowed === 1)
        {
            return Yii::t('admin', 'Yes');
        }

        return Yii::t('admin', 'No');
    }

}
