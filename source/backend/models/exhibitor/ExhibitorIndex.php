<?php

namespace backend\models\exhibitor;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use kartik\export\ExportMenu;

/**
 * ExhibitorIndex represents the model behind the search about `\backend\models\exhibitor\Exhibitor`.
 */
class ExhibitorIndex extends Exhibitor
{

    use CachedSearchTrait;

    public $id;
    public $first_name;
    public $last_name;
    public $email;
    public $date_from;
    public $date_to;
    public $company;
    public $tax_number;
    public $city;
    public $country_id;
    public $staff_demand_type;
    public $booking_type;
    public $phone_numer;
    public $booking_price_discount;
    public $gender;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'first_name',
                    'last_name',
                    'email',
                    'date_from',
                    'date_to',
                    'company',
                    'tax_number',
                    'city',
                    'country_id',
                    'staff_demand_type',
                    'booking_type',
                    'phone_number',
                    'booking_price_discount',
                    'gender'
                ],
                'safe'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), []
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * Search all
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAll($params)
    {
        return $this->search($params);
    }

    /**
     * Creates data provider instance with search query applied
     * Search by active status
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchActive($params)
    {
        return $this->search($params, [
                    'user.status_id' => Yii::$app->status->active
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     * Search by pending status
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchPending($params)
    {
        return $this->search($params, [
                    'user.status_id' => Yii::$app->status->pending
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     * Search by deleted status
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDeleted($params)
    {
        $query = $this->baseSearch($params);

        $query->andWhere([
            'exhibitor_profile.status_id' => Yii::$app->status->deleted,
            'user.status_id' => Yii::$app->status->deleted
        ]);

        return $this->createDataProvider($query);
    }

    /**
     * Creates data provider instance with search query applied
     * Search disabled status
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDisabled($params)
    {
        return $this->search($params, [
                    'user.status_id' => Yii::$app->status->disabled
        ]);
    }

    /**
     * @param string $fileName
     * @param ActiveDataProvider $dataProvider
     * @return string
     */
    public function getWidget($fileName, $dataProvider)
    {

        $fileName = str_replace(" ", "-", $fileName);

        $provider = clone($dataProvider);
        $provider->pagination = false;

        $widget = ExportMenu::widget([
                    'dataProvider' => $provider,
                    'columns' => [
                        'gender',
                        'first_name',
                        'last_name',
                        'email',
                        'phone_number',
                        'date_created',
                        'exhibitorProfile.company_name',
                        'exhibitorProfile.tax_number',
                        'exhibitorProfile.staffDemandTypeLabel',
                        'exhibitorProfile.bookingTypeLabel',
                        'exhibitorProfile.booking_price_discount',
                        'statusLabel'
                    ],
                    'exportConfig' => [
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false
                    ],
                    'showColumnSelector' => true,
                    'fontAwesome' => true,
                    'target' => ExportMenu::TARGET_SELF,
                    'filename' => $fileName . '-' . date("d-m-Y"),
                    'showConfirmAlert' => false,
        ]);

        return $widget;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param array $additional
     *
     * @return ActiveDataProvider
     */
    private function search($params, $additional = [])
    {
        $query = $this->baseSearch($params);

        if (!empty($additional))
        {
            $query->andFilterWhere($additional);
        }

        return $this->createDataProvider($query);
    }

    /**
     * Prepare query for search
     * @param array $params
     * @return \yii\db\ActiveQuery
     */
    private function baseSearch($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find()
                ->innerJoin('exhibitor_profile', 'exhibitor_profile.user_id = user.id')
                // where to eliminate status condition
                ->where(["role" => \common\models\User::ROLE_EXHIBITOR]);

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
        }

        $query->andFilterWhere([
            'user.id' => $this->id
        ]);

        $query->andFilterWhere([">=", 'user.date_created', $this->date_from]);
        $query->andFilterWhere(["<=", 'user.date_created', $this->date_to]);

        $query->andFilterWhere(['like', 'user.first_name', $this->first_name])
                ->andFilterWhere(['like', 'user.last_name', $this->last_name])
                ->andFilterWhere(['like', 'user.email', $this->email]);

        $query->andFilterWhere([
            'exhibitor_profile.staff_demand_type' => $this->staff_demand_type,
            'exhibitor_profile.booking_type' => $this->booking_type,
            'exhibitor_profile.country_id' => $this->country_id,
            'exhibitor_profile.booking_price_discount' => $this->booking_price_discount,
            'user.gender' => $this->gender,
        ]);

        $query->andFilterWhere(['like', 'exhibitor_profile.company_name', $this->company])
                ->andFilterWhere(['like', 'exhibitor_profile.tax_number', $this->tax_number])
                ->andFilterWhere(['like', 'exhibitor_profile.city', $this->city]);

        return $query;
    }

    /**
     * Create date provider for query
     * @param \yii\db\ActiveQuery $query
     * @return ActiveDataProvider
     */
    private function createDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        return $dataProvider;
    }

}
