<?php

namespace backend\models\languagelevel;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * LanguageLevel represents the backend model for `\common\models\LanguageLevel`.
 */
class LanguageLevel extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\LanguageLevel';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * Return full language list
     * @return array
     */
    public static function getForDropdownList()
    {
        return ArrayHelper::map(\common\models\LanguageLevel::find()->all(), 'id', 'name');
    }

}
