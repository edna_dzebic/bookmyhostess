<?php

namespace backend\models\hostesstestimonial;

use Yii;

/**
 * HostessTestimonial represents the backend model for `\common\models\HostessTestimonial`.
 */
class HostessTestimonial extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\HostessTestimonial';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [
            'id' => Yii::t('admin', 'Id'),
            'text' => Yii::t('admin', 'Text'),
            'source' => Yii::t('admin', 'Source'),
            'language_id' => Yii::t('admin', 'Language'),
            'languageName' => Yii::t('admin', 'Language'),
                ]
        );
    }

    /**
     * Return full language list
     * @return array
     */
    public function getLanguageList()
    {
        return \backend\models\language\Language::getForDropdownList();
    }

    /**
     * Get language name
     * @return string
     */
    public function getLanguageName()
    {
        return $this->modelObject->language->name;
    }

}
