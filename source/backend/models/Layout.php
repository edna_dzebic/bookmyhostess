<?php

namespace backend\models;

use Yii;
use backend\models\User;
use backend\models\hostess\HostessIndex;

class Layout
{

    public static function getRequestedHostessCount()
    {
        $model = new HostessIndex(['modelObject' => new User()]);
        $dataProvider = $model->searchRequested([]);
        $count = $dataProvider->count;
        return $count;
    }

    public function getMenuForUser()
    {
        $tree = $this->getItems();

        return $this->getMenuItems($tree);
    }

    /**
     * Menu items with permission for user roles
     * @return array
     */
    protected function getItems()
    {
        return [
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Dashboard'),
                'icon' => 'dashboard',
                'url' => ['/dashboard']
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Reports'),
                'url' => 'javaScript:void(0);',
                'icon' => 'info',
                'items' => [
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Invoices'),
                        'url' => ['/invoice/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Event registrations'),
                        'url' => ['/register/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Requests'),
                        'url' => ['/request/index']
                    ],
                ]
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Hostesses'),
                'url' => 'javaScript:void(0);',
                'icon' => 'users',
                'items' => [
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Requested'),
                        'url' => ['/hostess/requested']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Not Requested'),
                        'url' => ['/hostess/not-requested']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Approved'),
                        'url' => ['/hostess/approved']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Pending'),
                        'url' => ['/hostess/pending']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Active'),
                        'url' => ['/hostess/active']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Disabled'),
                        'url' => ['/hostess/disabled']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Deleted'),
                        'url' => ['/hostess/deleted']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'All'),
                        'url' => ['/hostess/index']
                    ],
                ],
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Exhibitors'),
                'url' => 'javaScript:void(0);',
                'icon' => 'user-secret',
                'items' => [
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Pending'),
                        'url' => ['/exhibitor/pending']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Active'),
                        'url' => ['/exhibitor/active']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Disabled'),
                        'url' => ['/exhibitor/disabled']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Deleted'),
                        'url' => ['/exhibitor/deleted']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'All'),
                        'url' => ['/exhibitor/index']
                    ],
                ],
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Events'),
                'url' => 'javaScript:void(0);',
                'icon' => 'calendar',
                'items' => [
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Create'),
                        'url' => ['/event/create']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Upcoming'),
                        'url' => ['/event/upcoming']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Past'),
                        'url' => ['/event/past']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'All'),
                        'url' => ['/event/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Types'),
                        'url' => ['/type/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Jobs'),
                        'url' => ['/job/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Dress codes'),
                        'url' => ['/dress-code/index']
                    ]
                ]
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Pricing'),
                'url' => 'javaScript:void(0);',
                'icon' => 'money',
                'items' => [
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Coupons'),
                        'url' => ['/coupon/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Prices'),
                        'url' => ['/country-price/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Countries'),
                        'url' => ['/country/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Cities'),
                        'url' => ['/city/index']
                    ],
                ],
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Reviews'),
                'url' => 'javaScript:void(0);',
                'icon' => 'star',
                'items' => [
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Create Review'),
                        'url' => ['/hostess-review/create']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'List all'),
                        'url' => ['/hostess-review/index']
                    ],
                ],
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Homepage'),
                'url' => 'javaScript:void(0);',
                'icon' => 'home',
                'items' => [
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Testimonials Exhibitors'),
                        'url' => ['/exhibitor-testimonial/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Testimonials Hostesses'),
                        'url' => ['/hostess-testimonial/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Gallery'),
                        'url' => ['/gallery/index']
                    ],
                ],
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Basic data'),
                'url' => 'javaScript:void(0);',
                'icon' => 'wrench',
                'items' => [
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Settings'),
                        'url' => ['/setting/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Languages'),
                        'url' => ['/language/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Language levels'),
                        'url' => ['/language-level/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Hair colors'),
                        'url' => ['/hair-color/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Country mappings'),
                        'url' => ['/country-mapping/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Statuses'),
                        'url' => ['/status/index']
                    ]
                ]
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Newsletter'),
                'icon' => 'inbox',
                'url' => ['/newsletter-subscriber']
            ],
            [
                'roles' => [ 'admin', 'sysadmin'],
                'label' => Yii::t('admin', 'Affiliates'),
                'url' => 'javaScript:void(0);',
                'icon' => 'object-group',
                'items' => [
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Create'),
                        'url' => ['/affiliate/create']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'List'),
                        'url' => ['/affiliate/index']
                    ],
                    [
                        'roles' => [ 'admin', 'sysadmin'],
                        'label' => Yii::t('admin', 'Reports'),
                        'url' => ['/affiliate/report']
                    ],
                ],
            ],
            [
                'roles' => ['sysadmin'],
                'label' => Yii::t('admin', 'Administrators'),
                'icon' => 'user',
                'url' => ['/admin-user']
            ]
        ];
    }

    /**
     * Parse items and check permission
     * Will be called for each sub item (items) in the item array
     * @param array $items
     * @return array
     */
    protected function getMenuItems($items)
    {
        $menu = [];

        foreach ($items as $item)
        {
            if (in_array(Yii::$app->user->identity->role, $item['roles']))
            {
                $menu[] = $this->getItem($item);
            }
        }

        return $menu;
    }

    /**
     * Calls getMenuItems if the item has sub items
     * @param array $item
     * @return array
     */
    protected function getItem($item)
    {
        if (isset($item['items']))
        {
            $item['items'] = $this->getMenuItems($item['items']);
        }

        return $item;
    }

}
