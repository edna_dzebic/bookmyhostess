<?php

namespace backend\models\countryprice;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\components\CachedSearchTrait;
use backend\models\countryprice\CountryPrice;

/**
 * CountryPriceIndex represents the model behind the search about `\backend\models\countryprice\CountryPrice`.
 */
class CountryPriceIndex extends CountryPrice
{

    use CachedSearchTrait;

    /**
     * Country Id
     * @var integer
     */
    public $country_id;

    /**
     * Value
     * @var string
     */
    public $value;

    /**
     * Price Class
     * @var string
     */
    public $price_class;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'integer'],
            [['value'], 'number'],
            [['price_class'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $params = $this->getCachedSearchParams($params);

        $class = $this->modelClass;
        $query = $class::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate())
        {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'country_id' => $this->country_id,
            'value' => $this->value,
        ]);

        $query->andFilterWhere(['like', 'price_class', $this->price_class]);

        return $dataProvider;
    }

}
