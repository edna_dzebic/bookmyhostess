<?php

namespace backend\models\countryprice;

use Yii;

/**
 * CountryPriceForm represents the model behind the form about `\backend\models\countryprice\CountryPrice`.
 */
class CountryPriceForm extends CountryPrice
{

    const SESSION_KEY_BACK_URL = 'countryprice.form.backUrl';

    /**
     * Country Id
     * @var integer
     */
    public $country_id;

    /**
     * Value
     * @var string
     */
    public $value;

    /**
     * Price Class
     * @var string
     */
    public $price_class;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'value', 'price_class'], 'required'],
            [['country_id'], 'integer'],
            [['value'], 'number'],
            [['price_class'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {

        return array_merge(
                parent::attributeHints(), [
            'price_class' => Yii::t('admin', 'Separate numbers with comma. Do not put comma after last number!')
                ]
        );
    }

    /**
     * Load and save CountryPrice     *
     * @param array $data
     * @returns bool is operation successful
     */
    public function save($data)
    {
        if ($this->load($data) && $this->validate())
        {
            $this->modelObject->setAttributes($this->attributes);

            return $this->modelObject->save();
        }

        return false;
    }

    /**
     * Delete model
     *
     * @returns integer|false Number of rows deleted
     */
    public function delete()
    {
        return $this->modelObject->delete();
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
