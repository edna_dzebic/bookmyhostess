<?php

namespace backend\models\countryprice;

use Yii;

/**
 * CountryPriceView represents the model behind the view about `\backend\models\countryprice\CountryPrice`.
 */
class CountryPriceView extends CountryPrice
{

    const SESSION_KEY_BACK_URL = 'countryprice.view.backUrl';

    /**
     * Id
     * @var integer
     */
    public $id;

    /**
     * Country Id
     * @var integer
     */
    public $country_id;

    /**
     * Value
     * @var string
     */
    public $value;

    /**
     * Date Created
     * @var string
     */
    public $date_created;

    /**
     * Date Updated
     * @var string
     */
    public $date_updated;

    /**
     * Status Id
     * @var integer
     */
    public $status_id;

    /**
     * Price Class
     * @var string
     */
    public $price_class;

    /**
     * Created By
     * @var integer
     */
    public $created_by;

    /**
     * Updated By
     * @var integer
     */
    public $updated_by;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                parent::attributeLabels(), [
                ]
        );
    }

    /**
     * Get header buttons
     *
     * @returns array
     */
    public function getButtons()
    {
        return [
            'back_button' => ['class' => 'btn btn-default', 'url' => $this->getBackUrl(), 'label' => Yii::t('admin', 'Back')],
            'edit_button' => ['class' => 'btn btn-success', 'url' => ['update', "id" => $this->modelObject->id], 'label' => Yii::t('admin', 'Update')],
            'delete_button' => ['class' => 'btn btn-danger', 'url' => ['delete', 'id' => $this->modelObject->id], 'label' => Yii::t('admin', 'Delete'), 'options' => ["data-confirm" => Yii::t('admin', 'Are you sure you want to delete this item?')]]
        ];
    }

    /**
     * Get back url from session
     *
     * @returns string|array
     */
    public function getBackUrl()
    {
        return Yii::$app->session->get(static::SESSION_KEY_BACK_URL, ['index']);
    }

}
