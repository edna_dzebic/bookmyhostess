<?php

namespace backend\models\countryprice;

use Yii;

/**
 * CountryPrice represents the backend model for `\common\models\CountryPrice`.
 */
class CountryPrice extends \common\components\Container
{

    public function __construct(array $config = [])
    {
        $this->modelClass = '\common\models\CountryPrice';

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
                $this->modelObject->attributeLabels(), [], parent::attributeLabels()
        );
    }

    /**
     * 
     * @return array
     */
    public function getCountryList()
    {
        return \backend\models\country\Country::getForDropdownList();
    }

    /**
     * 
     * @return string
     */
    public function getCountryName()
    {
        return $this->modelObject->getCountryName();
    }

}
