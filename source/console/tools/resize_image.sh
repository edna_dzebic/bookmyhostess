
#!/bin/bash 

#dependecy imagemagic

#usage example
# ./resize_image origin_directory_name destination_directory_name

origin=$1
destination=$2

mkdir $destination/ -p

echo "Image resize started"

for file in $origin/*
do
filename=`basename "$file"`
convert -resize 2000X2000 $origin/$filename $destination/$filename
done

echo "Image resize finished"

