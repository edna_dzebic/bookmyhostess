<?php

namespace console\components;


use Yii;
use yii\base\InvalidConfigException;
use yii\rbac\DbManager;

class Migration extends \yii\db\Migration
{

    /**
     * Create index simple, creates name from table and column
     * @param string $table
     * @param string $column
     */
    public function createIndexS($table, $column)
    {
        parent::createIndex($this->createIndexKeyName($table, $column), $table, $column);
    }

    /**
     * Create index key name
     *
     * @param $table
     * @param $column
     * @return string
     */
    protected function createIndexKeyName($table, $column)
    {
        return $this->createKeyName("idx", $table, $column);
    }

    /**
     * Create key name
     *
     * @param $key
     * @param $table
     * @param $column
     * @return string
     */
    private function createKeyName($key, $table, $column)
    {
        if (is_array($column))
        {
            $column = implode("-", $column);
        }

        return "{$this->cleanString($key)}-{$this->cleanString($table)}-{$this->cleanString($column)}";
    }

    /**
     * Remove any special characters from string (except for '-' and '_')
     *
     * @param $string
     *
     * @return mixed
     */
    private function cleanString($string)
    {
        return preg_replace("/[^a-zA-Z0-9_-]+/", "", $string);
    }

    /**
     * @return null|string
     */
    protected function getTableOptions()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        return $tableOptions;
    }

    protected function addRole($name, $description)
    {
        $this->insert(
            $this->getAuthManager()->itemTable,
            [
                "name" => $name,
                "type" => 1,
                "description" => $description,
                "created_at" => time(),
                "updated_at" => time()
            ]
        );

        return $this->getAuthManager()->getRole($name);
    }

    /**
     * @return \yii\rbac\DbManager
     * @throws InvalidConfigException
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager)
        {
            throw new InvalidConfigException(
                'You should configure "authManager" component to use database before executing this migration.'
            );
        }

        return $authManager;
    }

    /**
     * Default columns that almost all tables should have
     * @param string $statusCmnt status_id comment
     * @param string $createdAtCmnt created_at comment
     * @param string $updatedAtCmnt updated_at comment
     * @param string $createdByCmnt created_by comment
     * @param string $updatedByCmnt updated_by commend
     * @return array
     */
    protected function getDefaultColumns($statusCmnt = '', $createdAtCmnt = '', $updatedAtCmnt = '', $createdByCmnt = '', $updatedByCmnt = '')
    {
        return [
            'status_id' => $this->bigInteger()->notNull()->defaultValue(1)->comment($statusCmnt),
            'date_created' => $this->dateTime()->comment($createdAtCmnt),
            'date_updated' => $this->dateTime()->comment($updatedAtCmnt),
            'created_by' => $this->integer()->defaultValue(28)->comment($createdByCmnt),
            'updated_by' => $this->integer()->defaultValue(28)->comment($updatedByCmnt)
        ];
    }

    /**
     * Add foreign keys to default columns
     * @param string $table table to add the foreign keys to
     */
    protected function addDefaultColumnsForeignKeys($table)
    {
        $this->addForeignKeyS($table, "status_id", "status", "id", "NO ACTION", "CASCADE");
        $this->addForeignKeyS($table, "created_by", "user", "id", "RESTRICT", "CASCADE");
        $this->addForeignKeyS($table, "updated_by", "user", "id", "RESTRICT", "CASCADE");
    }

    /**
     * Create foreign key simple, automatically creates name from table and column
     * @param $table
     * @param $columns
     * @param $refTable
     * @param $refColumns
     * @param null $delete
     * @param null $update
     */
    public function addForeignKeyS($table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        parent::addForeignKey($this->createForeignKeyName($table, $columns), $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    /**
     * Create foreign key name
     * @param $table
     * @param $column
     * @return string
     */
    protected function createForeignKeyName($table, $column)
    {
        return $this->createKeyName("fk", $table, $column);
    }

}