<?php

use yii\db\Migration;

class m171218_201347_insert_status extends Migration
{

    const TABLE_NAME = 'status';

    public function safeUp()
    {
        $columns = ['name', 'label'];

        $values = [
            ['active', 'Active'], // 1
            ['deleted', 'Deleted'], // 2
            ['disabled', 'Disabled'], // 3
            ['pending', 'Pending'], // 4
            ['request_accept', 'Accept'], // 5
            ['request_reject', 'Reject'], // 6
            ['request_expired', 'expired'], // 7
            ['request_new', 'New'], // 8
            ['request_paypal_pending', 'Paypal pending'], // 9
            ['request_expired_hostess', 'Expired hostess'], // 10
            ['request_exhibitor_withdraw', 'Exhibitor Withdraw'], // 11
            ['request_exhibitor_withdraw_after_accept', 'Exhibitor Withdraw After Accept'], // 12
            ['request_hostess_withdraw_after_accept', 'Hostess Withdraw After Accept'], // 13
            
        ];

        $this->batchInsert(static::TABLE_NAME, $columns, $values);
    }

    public function safeDown()
    {
        $this->delete(static::TABLE_NAME, ['name' => ['active', 'deleted', 'disabled']]);
    }

}
