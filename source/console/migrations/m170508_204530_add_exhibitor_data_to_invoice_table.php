<?php

use yii\db\Migration;

class m170508_204530_add_exhibitor_data_to_invoice_table extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $table = 'invoice';

        $this->addColumn($table, 'coupon_type', $this->integer());

        $this->addColumn($table, 'is_exhibitor_german', $this->smallInteger()->defaultValue(0));
        $this->addColumn($table, 'invoice_type', $this->smallInteger()->defaultValue(0));

        $this->addColumn($table, 'exhibitor_company_name', $this->string());
        $this->addColumn($table, 'exhibitor_street', $this->string());
        $this->addColumn($table, 'exhibitor_postal_code', $this->string());
        $this->addColumn($table, 'exhibitor_city', $this->string());
        $this->addColumn($table, 'exhibitor_country', $this->string());
        $this->addColumn($table, 'exhibitor_tax_number', $this->string());

        $this->addColumn($table, 'net_amount', $this->decimal(10, 2));
        $this->addColumn($table, 'tax_amount', $this->decimal(10, 2));
        $this->addColumn($table, 'gross_amount', $this->decimal(10, 2));

        $this->addColumn($table, 'total_price_with_discount', $this->decimal(10, 2));

        $this->addColumn($table, 'coupon_netto_amount', $this->decimal(10, 2));
        $this->addColumn($table, 'coupon_tax_amount', $this->decimal(10, 2));
        $this->addColumn($table, 'coupon_gross_amount', $this->decimal(10, 2));
    }

    public function safeDown()
    {

        $table = 'invoice';

        $this->dropColumn($table, 'coupon_type');

        $this->dropColumn($table, 'is_exhibitor_german');
        $this->dropColumn($table, 'invoice_type');

        $this->dropColumn($table, 'exhibitor_company_name');
        $this->dropColumn($table, 'exhibitor_street');
        $this->dropColumn($table, 'exhibitor_postal_code');
        $this->dropColumn($table, 'exhibitor_city');
        $this->dropColumn($table, 'exhibitor_country');
        $this->dropColumn($table, 'exhibitor_tax_number');

        $this->dropColumn($table, 'net_amount');
        $this->dropColumn($table, 'tax_amount');
        $this->dropColumn($table, 'gross_amount');

        $this->dropColumn($table, 'total_price_with_discount');

        $this->dropColumn($table, 'coupon_gross_amount');
        $this->dropColumn($table, 'coupon_tax_amount');
        $this->dropColumn($table, 'coupon_netto_amount');
    }

}
