<?php

use yii\db\Migration;
use yii\db\Schema;

class m160924_231612_insert_gender_in_user extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('user', 'gender', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'gender');
    }

}
