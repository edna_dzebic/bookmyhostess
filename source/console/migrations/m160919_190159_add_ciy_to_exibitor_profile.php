<?php

use yii\db\Migration;
use yii\db\Schema;

class m160919_190159_add_ciy_to_exibitor_profile extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('exhibitor_profile', 'city', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn('exhibitor_profile', 'city');
    }

}
