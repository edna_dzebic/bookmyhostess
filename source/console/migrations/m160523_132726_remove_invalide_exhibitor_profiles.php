<?php

use yii\db\Migration;

class m160523_132726_remove_invalide_exhibitor_profiles extends Migration
{

    public function up()
    {
        Yii::$app->db->createCommand("
            DELETE FROM exhibitor_profile 
            WHERE
            country_id IS NULL 
            AND company_name IS NULL
            AND tax_number IS NULL
            AND street IS NULL
            AND home_number IS NULL
            AND postal_code IS NULL"
        )->execute();
    }

    public function down()
    {
        
    }

}
