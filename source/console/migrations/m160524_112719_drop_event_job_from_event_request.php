<?php

use yii\db\Migration;

/**
 * Handles dropping event_job from table `event_request`.
 */
class m160524_112719_drop_event_job_from_event_request extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey('FK_event_request_event_job', 'event_request');
        $this->dropIndex('FK_event_request_event_job', 'event_request');
        $this->dropColumn('event_request', 'event_job');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }

}
