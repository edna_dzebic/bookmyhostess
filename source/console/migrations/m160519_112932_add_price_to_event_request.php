<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles adding price to table `event_request`.
 */
class m160519_112932_add_price_to_event_request extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('event_request', 'price', Schema::TYPE_INTEGER);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('event_request', 'price');
    }

}
