<?php

use yii\db\Migration;
use yii\db\Schema;

class m170303_170652_add_address_fields_to_hostess extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('hostess_profile', 'postal_code', Schema::TYPE_STRING);
        $this->addColumn('hostess_profile', 'city', Schema::TYPE_STRING);
        $this->addColumn('hostess_profile', 'country_id', Schema::TYPE_INTEGER);
    }

    public function safeDown()
    {
        $this->dropColumn('hostess_profile', 'postal_code');
        $this->dropColumn('hostess_profile', 'city');
        $this->dropColumn('hostess_profile', 'country_id');
    }

}
