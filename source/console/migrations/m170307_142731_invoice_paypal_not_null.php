<?php

use yii\db\Migration;

class m170307_142731_invoice_paypal_not_null extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('invoice', 'paypal_transaction_id', $this->string(255));
    }

    public function safeDown()
    {
        $this->alterColumn('invoice', 'paypal_transaction_id', $this->string(150)->notNull());
    }

}
