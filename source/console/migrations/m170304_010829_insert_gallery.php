<?php

use yii\db\Migration;

class m170304_010829_insert_gallery extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $columns = ['path', 'include_on_home', 'sort_order', 'status_id'];
        $values = [

            ['/gallery_images/58ba2276b5167.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276bb2ff.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276bede5.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276c184d.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276c42e0.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276c6cb6.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276c97a8.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276cc690.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276cf448.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276d1c6c.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276d4c7f.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276d7be8.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276dbd93.jpg', 1, 0, 1],
            ['/gallery_images/58ba2276df4f4.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276e395f.jpg', 1, 2, 1],
            ['/gallery_images/58ba2276e7d24.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276ec28b.jpg', 0, 9, 1],
            ['/gallery_images/58ba2276f0685.jpg', 0, 9, 1],
            ['/gallery_images/58ba2277008f1.jpg', 0, 9, 1],
            ['/gallery_images/58ba2277047c2.jpg', 1, 5, 1],
            ['/gallery_images/58ba227708bfd.jpg', 0, 9, 1],
            ['/gallery_images/58ba22770cdc0.jpg', 1, 3, 1],
            ['/gallery_images/58ba2277100cd.jpg', 1, 6, 1],
            ['/gallery_images/58ba22771354c.jpg', 0, 9, 1],
            ['/gallery_images/58ba227716c8f.jpg', 0, 9, 1],
            ['/gallery_images/58ba22771b169.jpg', 1, 1, 1],
            ['/gallery_images/58ba22771ebd6.jpg', 1, 4, 1],
            ['/gallery_images/58ba2277226d8.jpg', 1, 7, 1]
        ];

        $this->batchInsert('gallery', $columns, $values);
    }

    public function safeDown()
    {
        $this->truncateTable('gallery');
    }

}
