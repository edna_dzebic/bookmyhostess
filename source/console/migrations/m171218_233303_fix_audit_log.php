<?php

class m171218_233303_fix_audit_log extends \console\components\Migration
{

    const TABLE_NAME = 'audit_log';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->renameColumn(static::TABLE_NAME, 'created_at', 'date_created');
    }

    public function safeDown()
    {
        return true;
    }

}
