<?php

use yii\db\Schema;
use yii\db\Migration;

class m171217_100612_setting extends Migration {

    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%setting}}', [
            'id' => Schema::TYPE_BIGPK,
            'key' => Schema::TYPE_STRING . "(255) NOT NULL",
            'value' => Schema::TYPE_TEXT . " NOT NULL",
            'updated_at' => Schema::TYPE_DATETIME,
            'updated_by' => Schema::TYPE_INTEGER
        ]);

        $this->addForeignKey('fk_setting_updated_by', 'setting', 'updated_by', 'user', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_setting_updated_by', 'setting');
        $this->dropTable('{{%setting}}');
    }

}
