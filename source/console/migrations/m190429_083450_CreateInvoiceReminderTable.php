<?php

class m190429_083450_CreateInvoiceReminderTable extends \console\components\Migration
{
     const TABLE_NAME = 'invoice_reminder';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable(
                static::TABLE_NAME, array_merge(
                        [
            'id' => $this->bigPrimaryKey(),
            'invoice_id' => $this->integer()->notNull(),
            'flag' => $this->integer()->notNull()
                        ], $this->getDefaultColumns()
                ), $this->getTableOptions()
        );

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);

        $this->addForeignKeyS(static::TABLE_NAME, 'invoice_id', 'invoice', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(static::TABLE_NAME);
    }
}
