<?php

use yii\db\Migration;
use yii\db\Schema;

class m160919_022228_drop_invoice_fk extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropForeignKey('fk_invoice_user_id2', 'invoice');
        $this->dropIndex('fk_invoice_user_id2', 'invoice');

        $this->dropForeignKey('fk_invoice_request_id', 'invoice');
        $this->dropIndex('fk_invoice_request_id', 'invoice');

        $this->alterColumn('invoice', 'user_id', Schema::TYPE_INTEGER);
        $this->alterColumn('invoice', 'event_request_id', Schema::TYPE_INTEGER);

        // should drop columns but no time to change model and retest everything!!!
    }

    public function safeDown()
    {
        
    }

}
