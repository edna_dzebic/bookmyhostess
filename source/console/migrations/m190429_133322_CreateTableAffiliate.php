<?php

class m190429_133322_CreateTableAffiliate extends \console\components\Migration
{

    const TABLE_NAME = 'affiliate';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable(
                static::TABLE_NAME, array_merge(
                        [
            'id' => $this->bigPrimaryKey(),
            'identification' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'provision' => $this->integer()->notNull(),
            'description' => $this->string()->null(),
            'tax_number' => $this->string()->null(),
            'street' => $this->string()->null(),
            'zip' => $this->string()->null(),
            'city' => $this->string()->null(),
            'phone' => $this->string()->null(),
            'email' => $this->string()->null(),
            'web' => $this->string()->null(),
            'fax' => $this->string()->null(),
            'contact' => $this->string()->null(),
            'country_id' => $this->integer()->null(),
                        ], $this->getDefaultColumns()
                ), $this->getTableOptions()
        );

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
        $this->addForeignKeyS(static::TABLE_NAME, 'country_id', 'country', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(static::TABLE_NAME);
    }

}
