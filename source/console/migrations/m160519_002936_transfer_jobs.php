<?php

use yii\db\Migration;
use common\models\EventRequest;
use common\models\EventRequestJob;

//transfer current jobs to new table
//in order to implement multiple jobs inside one eventrequest, 
//instead of previous one-one connection
class m160519_002936_transfer_jobs extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $eventRequests = EventRequest::find()->all();

        foreach ($eventRequests as $req)
        {

            $eventRequestId = $req->id;
            $jobId = $req->event_job;

            $job = EventRequestJob::findOne(["event_request_id" => $eventRequestId]);

            if ($job == null)
            {
                $job = new EventRequestJob();
                $job->event_request_id = $eventRequestId;
                $job->event_job = $jobId;
                $job->save();
            }
        }
    }

    public function safeDown()
    {
        // there is no back!
    }

}
