<?php

use yii\db\Migration;

class m181007_175838_AddIsInoviceAllowedToCountry extends Migration
{

    const TABLE_NAME = 'country';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'is_invoice_payment_allowed', $this->smallInteger()->notNull() . " DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_invoice_payment_allowed');
    }

}
