<?php

use yii\db\Migration;

class m180330_181633_add_is_eu_to_country extends Migration
{

    const TABLE_NAME = 'country';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'is_eu', $this->smallInteger()->notNull() . " DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_eu');
    }

}
