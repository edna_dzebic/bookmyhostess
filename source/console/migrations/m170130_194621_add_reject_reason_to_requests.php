<?php

use yii\db\Migration;
use yii\db\Schema;

class m170130_194621_add_reject_reason_to_requests extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('event_request', 'reason', Schema::TYPE_STRING);
        $this->addColumn('event_request', 'date_rejected', Schema::TYPE_DATETIME);
    }

    public function safeDown()
    {
        $this->dropColumn('event_request', 'reason');
        $this->dropColumn('event_request', 'date_rejected');
    }

}
