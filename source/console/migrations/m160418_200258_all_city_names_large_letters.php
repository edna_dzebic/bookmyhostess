<?php

use yii\db\Migration;

class m160418_200258_all_city_names_large_letters extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        Yii::$app->db->createCommand("UPDATE city SET name = UPPER(name)")->execute();
    }

    public function safeDown()
    {
        
    }

}
