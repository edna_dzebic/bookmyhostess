<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `audit_log`.
 */
class m170226_234055_create_audit_log_table extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%audit_log}}', [
            'id' => Schema::TYPE_BIGPK,
            'user_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'page' => Schema::TYPE_STRING . "(255)",
            'page_item_id' => Schema::TYPE_TEXT,
            'action' => Schema::TYPE_STRING . "(255)",
            'ip' => Schema::TYPE_STRING . "(255)",
            'url' => Schema::TYPE_TEXT,
            'get' => Schema::TYPE_TEXT,
            'post' => Schema::TYPE_TEXT,
            'message' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_DATETIME
        ]);

        $this->addForeignKey('fk_audit_log_user_id', 'audit_log', 'user_id', 'user', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_audit_log_user_id', 'audit_log');
        $this->dropTable('{{%audit_log}}');
    }

}
