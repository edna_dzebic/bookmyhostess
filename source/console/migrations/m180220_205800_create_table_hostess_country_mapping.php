<?php

class m180220_205800_create_table_hostess_country_mapping extends \console\components\Migration
{

    const TABLE_NAME = 'hostess_country_mapping';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable(
                static::TABLE_NAME, array_merge(
                        [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull()
                        ], $this->getDefaultColumns()
                ), $this->getTableOptions()
        );

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);

        $this->addForeignKeyS(static::TABLE_NAME, 'country_id', 'country', 'id');
        $this->addForeignKeyS(static::TABLE_NAME, 'user_id', 'user', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(static::TABLE_NAME);
    }

}
