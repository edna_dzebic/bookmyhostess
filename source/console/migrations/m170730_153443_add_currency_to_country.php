<?php

use yii\db\Migration;

class m170730_153443_add_currency_to_country extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('country', 'currency_label', $this->string()->defaultValue('')->notNull());
        $this->addColumn('country', 'currency_symbol', $this->string()->defaultValue('')->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('country', 'currency_label');
        $this->dropColumn('country', 'currency_symbol');
    }

}
