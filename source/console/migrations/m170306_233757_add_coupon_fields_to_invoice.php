<?php

use yii\db\Migration;

class m170306_233757_add_coupon_fields_to_invoice extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('invoice', 'coupon_code', $this->string());
        $this->addColumn('invoice', 'coupon_amount', $this->decimal(10, 2));
        $this->addColumn('invoice', 'coupon_id', $this->integer());

        $this->createIndex('invoice_coupon_code_idx', 'invoice', 'coupon_code');
        $this->addForeignKey('fk_invoice_coupon_id', 'invoice', 'coupon_id', 'coupon', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropIndex('invoice_coupon_code_idx', 'invoice');
        $this->dropForeignKey('fk_invoice_coupon_id', 'invoice');

        $this->dropColumn('invoice', 'coupon_code');
        $this->dropColumn('invoice', 'coupon_amount');
        $this->dropColumn('invoice', 'coupon_id');
    }

}
