<?php

class m190429_084316_AddReminderAllowedFlagToInvoice extends \console\components\Migration
{

    public function safeUp()
    {
        $this->addColumn('invoice', 'is_reminder_allowed', $this->smallInteger()->notNull() . " DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'is_reminder_allowed');
    }

}
