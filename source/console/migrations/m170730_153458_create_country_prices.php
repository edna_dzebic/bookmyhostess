<?php

use yii\db\Migration;

class m170730_153458_create_country_prices extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('country_price', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),            
            'value' => $this->decimal(10, 2)->notNull(),
            'created_at' => $this->dateTime()->defaultValue(NULL),
            'updated_at' => $this->dateTime()->defaultValue(NULL),
            'created_by' => $this->integer()->defaultValue(NULL),
            'updated_by' => $this->integer()->defaultValue(NULL),           
            'status' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('fk_country_price_country_id', 'country_price', 'country_id', 'country', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_country_price_created_by', 'country_price', 'created_by', 'user', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_country_price_updated_by', 'country_price', 'updated_by', 'user', 'id', 'RESTRICT', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropTable('country_price');
    }
    
}
