<?php

use yii\db\Migration;

class m170221_231355_add_os_type_to_notification_token extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('notification_token', 'os_type', \yii\db\Schema::TYPE_STRING . " DEFAULT 'android'");
    }

    public function safeDown()
    {
        $this->dropColumn('notification_token', 'os_type');
    }

}
