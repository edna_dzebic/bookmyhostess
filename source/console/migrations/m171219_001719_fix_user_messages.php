<?php

class m171219_001719_fix_user_messages extends \console\components\Migration
{

    const TABLE_NAME = 'user_messages';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        

        $this->alterColumn(static::TABLE_NAME, 'date_created', $this->dateTime());

        $columns = [
            'date_updated' => $this->dateTime(),
            'created_by' => $this->integer()->defaultValue(28),
            'updated_by' => $this->integer()->defaultValue(28),
            'status_id' => $this->bigInteger()->notNull()->defaultValue(1)
        ];

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
