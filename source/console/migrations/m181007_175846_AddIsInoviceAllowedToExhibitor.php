<?php

use yii\db\Migration;

class m181007_175846_AddIsInoviceAllowedToExhibitor extends Migration
{

    const TABLE_NAME = 'exhibitor_profile';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'is_invoice_payment_allowed', $this->smallInteger()->notNull() . " DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_invoice_payment_allowed');
    }

}
