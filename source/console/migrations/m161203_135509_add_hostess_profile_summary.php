<?php

use yii\db\Migration;
use yii\db\Schema;

class m161203_135509_add_hostess_profile_summary extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('hostess_profile', 'summary', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn('hostess_profile', 'summary');
    }

}
