<?php

use yii\db\Migration;
use yii\db\Schema;

class m160927_223155_add_last_view_profile_to_exhibitor extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('exhibitor_profile', 'last_profile_view', Schema::TYPE_DATETIME);
    }

    public function safeDown()
    {
        $this->dropColumn('exhibitor_profile', 'last_profile_view');
    }

}
