<?php

use yii\db\Migration;

class m180226_191842_add_real_filename_to_gallery_and_alt extends Migration
{

    const TABLE_NAME = 'gallery';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn(static::TABLE_NAME, 'file_name', $this->string()->notNull());
        $this->addColumn(static::TABLE_NAME, 'alt', $this->string()->notNull());
        $this->addColumn(static::TABLE_NAME, 'title', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn(static::TABLE_NAME, 'file_name');
        $this->dropColumn(static::TABLE_NAME, 'alt');
        $this->dropColumn(static::TABLE_NAME, 'title');
    }

}
