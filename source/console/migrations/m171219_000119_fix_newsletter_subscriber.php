<?php

class m171219_000119_fix_newsletter_subscriber extends \console\components\Migration
{

    const TABLE_NAME = 'newsletter_subscriber';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        

        $this->renameColumn(static::TABLE_NAME, 'created_at', 'date_created');
        $this->renameColumn(static::TABLE_NAME, 'updated_at', 'date_updated');

        $this->alterColumn(static::TABLE_NAME, 'date_created', $this->dateTime());
        $this->alterColumn(static::TABLE_NAME, 'date_updated', $this->dateTime());

        $this->renameColumn(static::TABLE_NAME, 'status', 'status_id');

        $this->alterColumn(static::TABLE_NAME, 'status_id', $this->bigInteger()->notNull()->defaultValue(1));

        $this->addColumn(static::TABLE_NAME, 'created_by', $this->integer()->defaultValue(28));

        $this->addForeignKeyS(static::TABLE_NAME, "status_id", "status", "id", "NO ACTION", "CASCADE");
        $this->addForeignKeyS(static::TABLE_NAME, "created_by", "user", "id", "RESTRICT", "CASCADE");
    }

    public function safeDown()
    {
        return true;
    }

}
