<?php

use yii\db\Migration;

/**
 * Handles the creation of table `invoice_item`.
 */
class m170508_203709_create_invoice_item_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('invoice_item', [
            'id' => $this->primaryKey(),
            'invoice_id' => $this->integer()->notNull(),
            'event_name' => $this->string(),
            'number_of_days' => $this->integer(),
            'booking_price_per_day' => $this->decimal(10, 2),
            'amount' => $this->decimal(10, 2),
            'from_date' => $this->dateTime(),
            'to_date' => $this->dateTime(),
            'hostess_name' => $this->string(),            
            'created_at' => $this->dateTime()->defaultValue(NULL),
            'updated_at' => $this->dateTime()->defaultValue(NULL),
            'created_by' => $this->integer()->defaultValue(NULL),
            'updated_by' => $this->integer()->defaultValue(NULL)
        ]);

        $this->addForeignKey('fk_invoice_item_invoice_id', 'invoice_item', 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('invoice_item');
    }

}
