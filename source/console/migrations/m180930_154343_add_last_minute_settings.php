<?php

use yii\db\Migration;

class m180930_154343_add_last_minute_settings extends Migration
{
    const TABLE_NAME = 'setting';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $columns = ['key', 'value', 'updated_by', 'date_updated'];

        $user_id = 28; // admin user

        $values = [
            ['last_minute_percentage', 10, $user_id, date("Y-m-d H:i:s")],
            ['last_minute_days', 10, $user_id, date("Y-m-d H:i:s")]
        ];

        $this->batchInsert(self::TABLE_NAME, $columns, $values);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, ['key' => 'last_minute_percentage']);
        $this->delete(self::TABLE_NAME, ['key' => 'last_minute_days']);
    }

}
