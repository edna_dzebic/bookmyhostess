<?php

use yii\db\Migration;

class m180103_005745_add_booking_discount_to_exhibitor extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('exhibitor_profile', 'booking_price_discount', $this->decimal(10, 2));
    }

    public function safeDown()
    {
        $this->dropColumn('exhibitor_profile', 'booking_price_discount');
    }

}
