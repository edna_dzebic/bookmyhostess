<?php

class m171219_001451_fix_user_images extends \console\components\Migration
{

    const TABLE_NAME = 'user_images';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
