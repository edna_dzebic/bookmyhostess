<?php

class m171219_001847_fix_user extends \console\components\Migration
{

    const TABLE_NAME = 'user';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropForeignKey('FK_user_city', static::TABLE_NAME);
        $this->dropForeignKey('FK_user_country', static::TABLE_NAME);
        $this->dropIndex('FK_user_city', static::TABLE_NAME);
        $this->dropIndex('FK_user_country', static::TABLE_NAME);
        $this->dropColumn(static::TABLE_NAME, 'city_id');
        $this->dropColumn(static::TABLE_NAME, 'country_id');

        $this->dropColumn(static::TABLE_NAME, 'type');

        

        $this->renameColumn(static::TABLE_NAME, 'created_at', 'date_created');
        $this->renameColumn(static::TABLE_NAME, 'updated_at', 'date_updated');

        $this->alterColumn(static::TABLE_NAME, 'date_created', $this->dateTime());
        $this->alterColumn(static::TABLE_NAME, 'date_updated', $this->dateTime());

        $this->renameColumn(static::TABLE_NAME, 'status', 'status_id');

        $this->alterColumn(static::TABLE_NAME, 'status_id', $this->bigInteger()->notNull()->defaultValue(1));

        $this->addColumn(static::TABLE_NAME, 'created_by', $this->integer()->defaultValue(28));

        $this->addColumn(static::TABLE_NAME, 'updated_by', $this->integer()->defaultValue(28));

        $this->addForeignKeyS(static::TABLE_NAME, "status_id", "status", "id", "NO ACTION", "CASCADE");

        $this->addForeignKeyS(static::TABLE_NAME, "updated_by", "user", "id", "RESTRICT", "CASCADE");
        $this->addForeignKeyS(static::TABLE_NAME, "created_by", "user", "id", "RESTRICT", "CASCADE");
    }

    public function safeDown()
    {
        return true;
    }

}
