<?php

use yii\db\Migration;

class m170508_203731_add_language_to_invoice_table extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('invoice', 'language', \yii\db\Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'language');
    }

}
