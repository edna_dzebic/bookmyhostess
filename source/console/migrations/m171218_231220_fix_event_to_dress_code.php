<?php

class m171218_231220_fix_event_to_dress_code extends \console\components\Migration
{
    const TABLE_NAME = 'event_dress_code';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->renameTable('event_to_dress_code', static::TABLE_NAME);

        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }
}
