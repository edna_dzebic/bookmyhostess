<?php

use yii\db\Migration;
use yii\db\Schema;

class m160922_000653_insert_lang_in_user extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('user', 'lang', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'lang');
    }

}
