<?php

use yii\db\Schema;
use yii\db\Migration;

class m160520_212442_payment_table extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $this->createTable('payment', [
            'id' => Schema::TYPE_BIGPK,
            'created_at' => Schema::TYPE_DATETIME,
                ]
        );

        $this->addColumn('payment', 'user_id', Schema::TYPE_INTEGER);
        $this->addColumn('payment', 'amount', Schema::TYPE_DECIMAL . "(10, 6)");
        $this->addColumn('payment', 'business', Schema::TYPE_STRING);
        $this->addColumn('payment', 'receiver_email', Schema::TYPE_STRING);
        $this->addColumn('payment', 'receiver_id', Schema::TYPE_STRING);
        $this->addColumn('payment', 'residence_country', Schema::TYPE_STRING);
        $this->addColumn('payment', 'test_ipn', Schema::TYPE_STRING);
        $this->addColumn('payment', 'transaction_subject', Schema::TYPE_STRING);
        $this->addColumn('payment', 'txn_id', Schema::TYPE_STRING);
        $this->addColumn('payment', 'txn_type', Schema::TYPE_STRING);
        $this->addColumn('payment', 'payer_email', Schema::TYPE_STRING);
        $this->addColumn('payment', 'payer_id', Schema::TYPE_STRING);
        $this->addColumn('payment', 'payer_status', Schema::TYPE_STRING);
        $this->addColumn('payment', 'first_name', Schema::TYPE_STRING);
        $this->addColumn('payment', 'last_name', Schema::TYPE_STRING);
        $this->addColumn('payment', 'address_city', Schema::TYPE_STRING);
        $this->addColumn('payment', 'address_country', Schema::TYPE_STRING);
        $this->addColumn('payment', 'address_state', Schema::TYPE_STRING);
        $this->addColumn('payment', 'address_status', Schema::TYPE_STRING);
        $this->addColumn('payment', 'address_country_code', Schema::TYPE_STRING);
        $this->addColumn('payment', 'address_name', Schema::TYPE_STRING);
        $this->addColumn('payment', 'address_street', Schema::TYPE_STRING);
        $this->addColumn('payment', 'address_zip', Schema::TYPE_STRING);
        $this->addColumn('payment', 'custom', Schema::TYPE_STRING);
        $this->addColumn('payment', 'handling_amount', Schema::TYPE_STRING);
        $this->addColumn('payment', 'item_name', Schema::TYPE_STRING);
        $this->addColumn('payment', 'item_number', Schema::TYPE_STRING);
        $this->addColumn('payment', 'mc_currency', Schema::TYPE_STRING);
        $this->addColumn('payment', 'mc_fee', Schema::TYPE_STRING);
        $this->addColumn('payment', 'mc_gross', Schema::TYPE_STRING);
        $this->addColumn('payment', 'payment_date', Schema::TYPE_STRING);
        $this->addColumn('payment', 'payment_fee', Schema::TYPE_STRING);
        $this->addColumn('payment', 'payment_gross', Schema::TYPE_STRING);
        $this->addColumn('payment', 'payment_status', Schema::TYPE_STRING);
        $this->addColumn('payment', 'payment_type', Schema::TYPE_STRING);
        $this->addColumn('payment', 'protection_eligibility', Schema::TYPE_STRING);
        $this->addColumn('payment', 'quantity', Schema::TYPE_STRING);
        $this->addColumn('payment', 'shipping', Schema::TYPE_STRING);
        $this->addColumn('payment', 'tax', Schema::TYPE_STRING);
        $this->addColumn('payment', 'notify_version', Schema::TYPE_STRING);
        $this->addColumn('payment', 'charset', Schema::TYPE_STRING);
        $this->addColumn('payment', 'verify_sign', Schema::TYPE_STRING);

        $this->addForeignKey('fk_payment_user_id', 'payment', 'user_id', 'user', 'id');
    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_payment_user_id', 'payment');

        $this->dropColumn('payment', 'user_id');
        $this->dropColumn('payment', 'amount');
        $this->dropColumn('payment', 'business');
        $this->dropColumn('payment', 'receiver_email');
        $this->dropColumn('payment', 'receiver_id');
        $this->dropColumn('payment', 'residence_country');
        $this->dropColumn('payment', 'test_ipn');
        $this->dropColumn('payment', 'transaction_subject');
        $this->dropColumn('payment', 'txn_id');
        $this->dropColumn('payment', 'txn_type');
        $this->dropColumn('payment', 'payer_email');
        $this->dropColumn('payment', 'payer_id');
        $this->dropColumn('payment', 'payer_status');
        $this->dropColumn('payment', 'first_name');
        $this->dropColumn('payment', 'last_name');
        $this->dropColumn('payment', 'address_city');
        $this->dropColumn('payment', 'address_country');
        $this->dropColumn('payment', 'address_state');
        $this->dropColumn('payment', 'address_status');
        $this->dropColumn('payment', 'address_country_code');
        $this->dropColumn('payment', 'address_name');
        $this->dropColumn('payment', 'address_street');
        $this->dropColumn('payment', 'address_zip');
        $this->dropColumn('payment', 'custom');
        $this->dropColumn('payment', 'handling_amount');
        $this->dropColumn('payment', 'item_name');
        $this->dropColumn('payment', 'item_number');
        $this->dropColumn('payment', 'mc_currency');
        $this->dropColumn('payment', 'mc_fee');
        $this->dropColumn('payment', 'mc_gross');
        $this->dropColumn('payment', 'payment_date');
        $this->dropColumn('payment', 'payment_fee');
        $this->dropColumn('payment', 'payment_gross');
        $this->dropColumn('payment', 'payment_status');
        $this->dropColumn('payment', 'payment_type');
        $this->dropColumn('payment', 'protection_eligibility');
        $this->dropColumn('payment', 'quantity');
        $this->dropColumn('payment', 'shipping');
        $this->dropColumn('payment', 'tax');
        $this->dropColumn('payment', 'notify_version');
        $this->dropColumn('payment', 'charset');
        $this->dropColumn('payment', 'verify_sign');

        $this->dropTable('payment');
    }

}
