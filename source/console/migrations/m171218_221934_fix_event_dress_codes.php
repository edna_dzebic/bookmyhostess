<?php

class m171218_221934_fix_event_dress_codes extends \console\components\Migration
{

    const TABLE_NAME = 'dress_code';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->renameTable('event_dress_code', static::TABLE_NAME);

        $this->dropColumn(static::TABLE_NAME, 'active');

        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
