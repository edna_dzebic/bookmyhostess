<?php


class m190429_133356_AddAffiliateToRequest   extends \console\components\Migration
{
    public function safeUp()
    {
        $this->addColumn('request', 'affiliate_id', $this->bigInteger()->null());
        $this->addForeignKeyS('request', 'affiliate_id', 'affiliate', 'id');
    }

    public function safeDown()
    {
        $this->dropIndex('fk-request-affiliate_id', 'request');
        $this->dropColumn('request', 'affiliate_id');
    }
}
