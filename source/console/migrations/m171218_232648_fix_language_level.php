<?php

class m171218_232648_fix_language_level extends \console\components\Migration
{

    const TABLE_NAME = 'language_level';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropForeignKey('fk_language_level_id', static::TABLE_NAME);
        $this->dropIndex('fk_language_level_id', static::TABLE_NAME);
        $this->dropColumn(static::TABLE_NAME, 'language_id');

        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
