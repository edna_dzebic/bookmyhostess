<?php

class m171218_232003_fix_hostess_past_work extends \console\components\Migration
{
    const TABLE_NAME = 'hostess_past_work';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
