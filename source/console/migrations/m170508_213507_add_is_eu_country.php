<?php

use yii\db\Migration;

class m170508_213507_add_is_eu_country extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('country', 'invoice_type', $this->smallInteger()->defaultValue(3));
    }

    public function safeDown()
    {
        $this->dropColumn('country', 'invoice_type');
    }

}
