<?php

class m171218_235735_fix_invoice_item extends \console\components\Migration
{
    const TABLE_NAME = 'invoice_item';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        

        $this->renameColumn(static::TABLE_NAME, 'created_at', 'date_created');
        $this->renameColumn(static::TABLE_NAME, 'updated_at', 'date_updated');

        $this->alterColumn(static::TABLE_NAME, 'date_created', $this->dateTime());
        $this->alterColumn(static::TABLE_NAME, 'date_updated', $this->dateTime());

        $this->addColumn(static::TABLE_NAME, 'status_id', $this->bigInteger()->notNull()->defaultValue(1));

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
