<?php

use yii\db\Migration;

class m171216_004037_new_jobs extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $table = "event_job";
        $columns = ["name", "active", "order"];
        $this->update($table, ["order" => 20], ["name" => "Other"]);
        $values = [
            [
                "name" => "Assembly/ removal support",
                "active" => 1,
                "order" => 21
            ],
            [
                "name" => "Counter support",
                "active" => 1,
                "order" => 19
            ],
            [
                "name" => "Inventory support",
                "active" => 1,
                "order" => 18
            ]
        ];
        $this->batchInsert($table, $columns, $values);
    }

    public function safeDown()
    {
        return true;
    }

}
