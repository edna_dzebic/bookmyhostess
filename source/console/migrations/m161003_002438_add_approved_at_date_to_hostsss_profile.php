<?php

use yii\db\Migration;
use yii\db\Schema;

class m161003_002438_add_approved_at_date_to_hostsss_profile extends Migration {

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
        $this->addColumn('hostess_profile', 'approved_at', Schema::TYPE_DATETIME);
    }

    public function safeDown() {
        $this->dropColumn('hostess_profile', 'approved_at');
    }

}
