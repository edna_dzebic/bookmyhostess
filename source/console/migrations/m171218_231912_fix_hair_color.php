<?php

class m171218_231912_fix_hair_color extends \console\components\Migration
{
    const TABLE_NAME = 'hair_color';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }
}
