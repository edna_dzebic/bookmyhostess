<?php

class m171218_201347_create_table_status extends \console\components\Migration
{

    const TABLE_NAME = 'status';

    public function safeUp()
    {
        $this->createTable(
                static::TABLE_NAME, [
            "id" => $this->bigPrimaryKey(),
            "name" => $this->string()->notNull()->unique(),
            "label" => $this->string()->notNull(),
            "updated_at" => $this->dateTime(),
            "updated_by" => $this->bigInteger()
                ], $this->getTableOptions()
        );

        $this->createIndexS(static::TABLE_NAME, "name");
    }

    public function safeDown()
    {
        $this->dropTable(static::TABLE_NAME);
    }

}
