<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `gallery`.
 */
class m170304_010353_create_gallery_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%gallery}}', [
            'id' => Schema::TYPE_BIGPK,
            'path' => Schema::TYPE_TEXT,
            'include_on_home' => Schema::TYPE_SMALLINT,
            'sort_order' => Schema::TYPE_INTEGER,
            'created_by' => Schema::TYPE_INTEGER,
            'updated_by' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
            'status_id' => Schema::TYPE_INTEGER
        ]);

        $this->addForeignKey('fk_gallery_created_by', 'gallery', 'created_by', 'user', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_gallery_updated_by', 'gallery', 'updated_by', 'user', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('gallery');
    }

}
