<?php

class m171218_231316_fix_event_to_job extends \console\components\Migration
{

    const TABLE_NAME = 'event_job';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->renameTable('event_to_job', static::TABLE_NAME);

        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
