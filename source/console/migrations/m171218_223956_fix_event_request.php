<?php

class m171218_223956_fix_event_request extends \console\components\Migration
{

    const TABLE_NAME = 'request';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->renameTable('event_request', static::TABLE_NAME);

        $this->alterColumn(static::TABLE_NAME, 'price', $this->decimal(10, 2));

        

        $this->alterColumn(static::TABLE_NAME, 'date_created', $this->dateTime());

        $this->renameColumn(static::TABLE_NAME, 'status', 'status_id');

        $this->alterColumn(static::TABLE_NAME, 'status_id', $this->bigInteger()->notNull()->defaultValue(1));

        $columns = [
            'date_updated' => $this->dateTime(),
            'created_by' => $this->integer()->defaultValue(28),
            'updated_by' => $this->integer()->defaultValue(28)
        ];

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
