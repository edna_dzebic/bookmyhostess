<?php

use yii\db\Migration;

class m160507_124245_insert_new_event_job extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->insert('event_job', ["name" => "Moderator", 'active' => 1]);
    }

    public function safeDown()
    {
        $this->delete('event_job', ["name" => "Moderator"]);
    }

}
