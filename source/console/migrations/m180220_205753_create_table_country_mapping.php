<?php

class m180220_205753_create_table_country_mapping extends \console\components\Migration
{

    const TABLE_NAME = 'country_mapping';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable(
                static::TABLE_NAME, array_merge(
                        [
            'id' => $this->bigPrimaryKey(),
            'source_country_id' => $this->integer()->notNull(),
            'target_country_id' => $this->integer()->notNull()
                        ], $this->getDefaultColumns()
                ), $this->getTableOptions()
        );

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);

        $this->addForeignKeyS(static::TABLE_NAME, 'source_country_id', 'country', 'id');
        $this->addForeignKeyS(static::TABLE_NAME, 'target_country_id', 'country', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(static::TABLE_NAME);
    }

}
