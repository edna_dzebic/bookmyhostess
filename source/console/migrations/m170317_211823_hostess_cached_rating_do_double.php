<?php

use yii\db\Migration;

class m170317_211823_hostess_cached_rating_do_double extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('hostess_profile', 'cached_rating', $this->double(3, 2)->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn('hostess_profile', 'cached_rating', $this->smallInteger());
    }

}
