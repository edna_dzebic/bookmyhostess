<?php

use yii\db\Migration;
use yii\db\Schema;

class m160919_232319_remove_50_limit_in_exibitor_profile extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('exhibitor_profile', 'company_name', Schema::TYPE_STRING);
        $this->alterColumn('exhibitor_profile', 'street', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        
    }

}
