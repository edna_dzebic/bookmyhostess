<?php

use yii\db\Migration;
use common\models\Invoice;

class m181007_182630_AddPaymentTypeToInvoice extends Migration
{
    const TABLE_NAME = 'invoice';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'payment_type', $this->string());
        
        $this->update(self::TABLE_NAME, ["payment_type" => Invoice::PAYMENT_TYPE_INVOICE]);
        
        $this->update(self::TABLE_NAME, ["payment_type" => Invoice::PAYMENT_TYPE_RECEIPT], ' amount = 0 ');
        
        $this->update(self::TABLE_NAME, ["payment_type" => Invoice::PAYMENT_TYPE_PAYPAL], ' paypal_transaction_id is not null and amount > 0 ');
        
        $this->alterColumn(self::TABLE_NAME, 'payment_type', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'payment_type');
    }

}
