<?php

class m171218_221929_fix_event_jobs extends \console\components\Migration
{

    const TABLE_NAME = 'job';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->renameTable('event_job', static::TABLE_NAME);
        
        $this->dropColumn(static::TABLE_NAME, 'active');

        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
