<?php

class m171218_232519_fix_hostess_to_language extends \console\components\Migration
{

    const TABLE_NAME = 'hostess_language';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->renameTable('hostess_to_language', static::TABLE_NAME);

        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
        
        $this->renameColumn(static::TABLE_NAME, 'level', 'level_id');
        $this->addForeignKeyS(static::TABLE_NAME, 'level_id', 'language_level', 'id');
    }

    public function safeDown()
    {
        return true;
    }

}
