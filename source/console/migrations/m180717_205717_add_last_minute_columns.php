<?php

use yii\db\Migration;

class m180717_205717_add_last_minute_columns extends Migration
{

    const TABLE_NAME = 'request';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'payment_type', $this->smallInteger()->notNull() . " DEFAULT 1");

        // for hostess price
        $this->addColumn(self::TABLE_NAME, 'hostess_original_price', $this->decimal(10, 2)->notNull() . " DEFAULT 0");
        $this->addColumn(self::TABLE_NAME, 'original_booking_price_per_day', $this->decimal(10, 2)->notNull() . " DEFAULT 0");

        // general last minute
        $this->addColumn(self::TABLE_NAME, 'days_to_event', $this->integer()->notNull() . " DEFAULT 0");
        $this->addColumn(self::TABLE_NAME, 'is_last_minute', $this->smallInteger()->notNull() . " DEFAULT 0");
        $this->addColumn(self::TABLE_NAME, 'last_minute_percentage', $this->decimal(10, 2)->notNull() . " DEFAULT 0");
        
        $this->addColumn('invoice_item', 'is_last_minute', $this->smallInteger()->notNull() . " DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn('invoice_item', 'is_last_minute');
        
        $this->dropColumn(self::TABLE_NAME, 'last_minute_percentage');
        $this->dropColumn(self::TABLE_NAME, 'is_last_minute');
        $this->dropColumn(self::TABLE_NAME, 'days_to_event');

        $this->dropColumn(self::TABLE_NAME, 'original_booking_price_per_day');
        $this->dropColumn(self::TABLE_NAME, 'hostess_original_price');
        
        $this->dropColumn(self::TABLE_NAME, 'payment_type');
    }

}
