<?php

class m171218_234512_fix_gallery extends \console\components\Migration
{

    const TABLE_NAME = 'gallery';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        

        $this->renameColumn(static::TABLE_NAME, 'created_at', 'date_created');
        $this->renameColumn(static::TABLE_NAME, 'updated_at', 'date_updated');

        $this->alterColumn(static::TABLE_NAME, 'date_created', $this->dateTime());
        $this->alterColumn(static::TABLE_NAME, 'date_updated', $this->dateTime());

        $this->alterColumn(static::TABLE_NAME, 'status_id', $this->bigInteger()->notNull()->defaultValue(1));

        $this->addForeignKeyS(static::TABLE_NAME, "status_id", "status", "id", "NO ACTION", "CASCADE");
    }

    public function safeDown()
    {
        return true;
    }

}
