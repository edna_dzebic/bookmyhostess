<?php

use yii\db\Migration;
use yii\db\Schema;

class m161001_235701_add_activate_date_in_user extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('user', 'activated_at', Schema::TYPE_DATETIME);
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'activated_at');
    }

}
