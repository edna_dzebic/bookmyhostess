<?php

use yii\db\Migration;
use yii\db\Schema;

class m161201_123414_add_order_in_event_job extends Migration {

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
        $this->addColumn('event_job', 'order', Schema::TYPE_INTEGER);
    }

    public function safeDown() {
        $this->dropColumn('event_job', 'order');
    }

}
