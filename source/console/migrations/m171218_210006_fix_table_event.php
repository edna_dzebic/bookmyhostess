<?php

class m171218_210006_fix_table_event extends \console\components\Migration
{

    const TABLE_NAME = 'event';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropForeignKey('fk_event_user_id', static::TABLE_NAME);
        $this->dropForeignKey('FK_event_country', static::TABLE_NAME);
        $this->dropForeignKey('FK_event_event_type', static::TABLE_NAME);
        
        $this->dropIndex('FK_event_country', static::TABLE_NAME);
        $this->dropIndex('FK_event_event_type', static::TABLE_NAME);
        $this->dropIndex('fk_event_user_id', static::TABLE_NAME);
        
        $this->dropColumn(static::TABLE_NAME, 'city_name');
        $this->dropColumn(static::TABLE_NAME, 'is_official');
        $this->dropColumn(static::TABLE_NAME, 'user_id');
        $this->dropColumn(static::TABLE_NAME, 'image_url');
        
        $this->alterColumn(static::TABLE_NAME, 'name', $this->string(150)->notNull());
        $this->alterColumn(static::TABLE_NAME, 'venue', $this->string(150)->notNull());        
        $this->alterColumn(static::TABLE_NAME, 'city_id', $this->integer()->notNull());
        $this->alterColumn(static::TABLE_NAME, 'type_id', $this->integer()->notNull());
        $this->alterColumn(static::TABLE_NAME, 'country_id', $this->integer()->notNull());
        $this->alterColumn(static::TABLE_NAME, 'start_date', $this->dateTime()->notNull());
        $this->alterColumn(static::TABLE_NAME, 'end_date', $this->dateTime()->notNull());
        
        $this->addColumn(static::TABLE_NAME, 'parent_id', $this->integer()->defaultValue(null));

        $this->addColumn(static::TABLE_NAME, 'created_by', $this->integer()->defaultValue(28));

        $this->addColumn(static::TABLE_NAME, 'updated_by', $this->integer()->defaultValue(28));

        $this->renameColumn(static::TABLE_NAME, 'status', 'status_id');
        $this->renameColumn(static::TABLE_NAME, 'created_at', 'date_created');
        $this->renameColumn(static::TABLE_NAME, 'updated_at', 'date_updated');

        $this->alterColumn(static::TABLE_NAME, 'status_id', $this->bigInteger()->notNull()->defaultValue(1));

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);

        $this->addForeignKeyS(static::TABLE_NAME, 'city_id', 'city', 'id', "RESTRICT", "CASCADE");
        $this->addForeignKeyS(static::TABLE_NAME, 'country_id', 'country', 'id', "RESTRICT", "CASCADE");
        $this->addForeignKeyS(static::TABLE_NAME, 'type_id', 'event_type', 'id', "RESTRICT", "CASCADE");
        $this->addForeignKeyS(static::TABLE_NAME, 'parent_id', 'event', 'id', "RESTRICT", "CASCADE");
    }

    public function safeDown()
    {
        return true;
    }

}
