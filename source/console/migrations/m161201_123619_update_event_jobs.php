<?php

use yii\db\Migration;

class m161201_123619_update_event_jobs extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $table = "event_job";

        $this->update($table, ["name" => "Interpretation"], ["name" => "Interpreter"]);

        $this->update($table, ["order" => 1], ["name" => "Explainer"]);
        $this->update($table, ["order" => 2], ["name" => "Hostess"]);
        $this->update($table, ["order" => 3], ["name" => "Interviewer"]);
        $this->update($table, ["order" => 4], ["name" => "Modeling"]);
        $this->update($table, ["order" => 5], ["name" => "Promotion"]);
        $this->update($table, ["order" => 6], ["name" => "Service"]);
        $this->update($table, ["order" => 7], ["name" => "Walking Act"]);
        $this->update($table, ["order" => 8], ["name" => "Interpretation"]);
        $this->update($table, ["order" => 9], ["name" => "Chef Hostess"]);
        $this->update($table, ["order" => 10], ["name" => "Moderator"]);
        $this->update($table, ["order" => 11], ["name" => "Sales"]);
        $this->update($table, ["order" => 12], ["name" => "Driver"]);
        $this->update($table, ["order" => 13], ["name" => "Barkeeper"]);
        $this->update($table, ["order" => 14], ["name" => "Other"]);
    }

    public function safeDown()
    {
        // not needed
    }

}
