<?php

use yii\db\Migration;
use yii\db\Schema;

class m160919_232738_remove_50_limit_in_hostess_past_work extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('hostess_past_work', 'event_name', Schema::TYPE_STRING);
        $this->alterColumn('hostess_past_work', 'job_description', Schema::TYPE_STRING);
        $this->alterColumn('hostess_past_work', 'position', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        
    }

}
