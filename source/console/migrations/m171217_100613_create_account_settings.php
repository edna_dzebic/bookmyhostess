<?php

use yii\db\Migration;
use common\models\User;

class m171217_100613_create_account_settings extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $columns = ['key', 'value', 'updated_by', 'updated_at'];

        $user_id = User::findByUsername('admin')->id;

        $values = [
            //emails
            ['admin_email', 'info@bookmyhostess.com', $user_id, date("Y-m-d H:i:s")],
            ['developer_email', 'dev@bookmyhostess.com', $user_id, date("Y-m-d H:i:s")],
            ['finance_email', 'finanzen@bookmyhostess.com', $user_id, date("Y-m-d H:i:s")],
            ['contact_email', 'contact@bookmyhostess.com', $user_id, date("Y-m-d H:i:s")],
            ['support_email', 'support@bookmyhostess.com', $user_id, date("Y-m-d H:i:s")],
            ['email_from', 'support@bookmyhostess.com', $user_id, date("Y-m-d H:i:s")],
            //plugins
            ['include_google_analytics', '1', $user_id, date("Y-m-d H:i:s")],
            ['include_zopim', '1', $user_id, date("Y-m-d H:i:s")],
            ['inlude_hotjar', '1', $user_id, date("Y-m-d H:i:s")],
            ['google_analytics_tracking_id', 'UA-101086580-1', $user_id, date("Y-m-d H:i:s")],
            //meta tags
            ['meta_keywords', 'messehostessen,Hostesse,hostess agentur,Messehostess,Hostess jobs,Messe hostess, Hostess,Hostess Berlin,Hostess Service,Messepersonal,Nebenjob,Promotion,Promoter,Promotionjobs,Models,Modelkartei,Modelagentur,Hostess buchen,Promotion Agentur,Messehostess jobs,Messejobs,Messehostess buchen,Hostess Messe,Hostess werden,Messehostess werden,Messehostess buchen,Hostess Service,Messe Hostess,Hostess München', $user_id, date("Y-m-d H:i:s")],
            ['meta_description', 'BOOKmyHOSTESS.com is a platform for online booking of exhibition staff, hostesses, models and promoters. - BOOKmyHOSTESS.com ist eine Platform für die Online­Buchung von Messepersonal, Hostessen, Models und Promotern.', $user_id, date("Y-m-d H:i:s")],
            //apps
            ['google_play', 'https://play.google.com/store/apps/details?id=com.bookmyhostess.android&hl=en', $user_id, date("Y-m-d H:i:s")],
            ['app_store', 'https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1206044192&mt=8', $user_id, date("Y-m-d H:i:s")],
            ['send_firebase_notification', '1', $user_id, date("Y-m-d H:i:s")],            
            //configs
            ['booking_price', '25', $user_id, date("Y-m-d H:i:s")],
            ['invoice_offset', '50', $user_id, date("Y-m-d H:i:s")],
            ['manual_invoice_count', '14', $user_id, date("Y-m-d H:i:s")],
            ['manufacturer_code', '62013', $user_id, date("Y-m-d H:i:s")],            
        ];

        $this->batchInsert('setting', $columns, $values);
    }

    public function safeDown()
    {
        $this->delete('setting');
    }

}
