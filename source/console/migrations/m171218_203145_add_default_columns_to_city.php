<?php

class m171218_203145_add_default_columns_to_city extends \console\components\Migration
{

    const TABLE_NAME = 'city';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
