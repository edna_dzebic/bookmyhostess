<?php

use yii\db\Migration;
use yii\db\Schema;

class m170211_224958_trade_license_extend extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql')
        {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('trade_license_extend', [
            'id' => Schema::TYPE_BIGPK,
            'hostess_profile_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'date' => Schema::TYPE_DATETIME . ' NOT NULL',
            'reason' => Schema::TYPE_TEXT . " NOT NULL",
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
            'status' => Schema::TYPE_INTEGER,
            'updated_by' => Schema::TYPE_INTEGER
                ], $tableOptions);

        $this->addForeignKey('fk_trade_license_extend_updated_by', 'trade_license_extend', 'updated_by', 'user', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_trade_license_extend_hostess_profile_id', 'trade_license_extend', 'hostess_profile_id', 'hostess_profile', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_trade_license_extend_updated_by', 'trade_license_extend');
        $this->dropTable('trade_license_extend');
    }

}
