<?php

use yii\db\Migration;

class m161201_122613_add_new_evvet_jobs extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $table = "event_job";
        $columns = ["name", "active"];
        $values = [
            [
                "name" => "Sales",
                "active" => 1
            ],
            [
                "name" => "Barkeeper",
                "active" => 1
            ],
            [
                "name" => "Driver",
                "active" => 1
            ]
        ];
        $this->batchInsert($table, $columns, $values);
    }

    public function safeDown()
    {
        // not needed
    }

}
