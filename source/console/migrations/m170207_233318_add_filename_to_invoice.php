<?php

use yii\db\Migration;

class m170207_233318_add_filename_to_invoice extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('invoice', 'filename', \yii\db\Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'filename');
    }
    
}
