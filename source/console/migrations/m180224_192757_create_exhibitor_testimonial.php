<?php

class m180224_192757_create_exhibitor_testimonial extends \console\components\Migration
{

    const TABLE_NAME = 'exhibitor_testimonial';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable(
                static::TABLE_NAME, array_merge(
                        [
            'id' => $this->bigPrimaryKey(),
            'text' => $this->text()->notNull(),
            'source' => $this->string()->notNull(),
            'language_id' => $this->integer()->notNull(),
                        ], $this->getDefaultColumns()
                ), $this->getTableOptions()
        );

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
        $this->addForeignKeyS(static::TABLE_NAME, 'language_id', 'language', 'id');
    }

    public function safeDown()
    {
        $this->dropTable(static::TABLE_NAME);
    }

}
