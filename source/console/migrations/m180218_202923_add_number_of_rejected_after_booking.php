<?php

use yii\db\Migration;

class m180218_202923_add_number_of_rejected_after_booking extends Migration
{

    const TABLE_NAME = 'hostess_profile';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn(
                self::TABLE_NAME, 'num_rejections_after_booking', $this->integer() . " DEFAULT 0"
        );
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'num_rejections_after_booking');
    }

}
