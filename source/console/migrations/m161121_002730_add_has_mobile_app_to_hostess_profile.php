<?php

use yii\db\Migration;
use yii\db\Schema;

class m161121_002730_add_has_mobile_app_to_hostess_profile extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('hostess_profile', 'has_mobile_app', Schema::TYPE_BOOLEAN . " DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn('hostess_profile',  'has_mobile_app');
    }
    
}
