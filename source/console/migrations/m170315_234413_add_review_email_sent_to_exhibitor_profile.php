<?php

use yii\db\Migration;

class m170315_234413_add_review_email_sent_to_exhibitor_profile extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('exhibitor_profile', 'review_email_sent', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('exhibitor_profile', 'review_email_sent');
    }

}
