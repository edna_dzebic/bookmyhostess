<?php

use yii\db\Migration;

class m170701_191813_add_coupon_title_field extends Migration
{

    public function up()
    {
        $this->addColumn('coupon', 'title', $this->string());
    }

    public function down()
    {
        $this->dropColumn('coupon', 'title');
    }

}
