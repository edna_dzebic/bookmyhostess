<?php

use yii\db\Migration;

class m160418_195816_insert_croatian_language extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $values = [
            "name" => "Croatian",
            "active" => 1
        ];

        $this->insert('language', $values);
    }

    public function safeDown()
    {
        $this->delete('language', ["name" => "Croatian"]);
    }

}
