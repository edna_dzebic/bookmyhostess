<?php

use yii\db\Migration;
use yii\db\Schema;

class m170223_183421_add_withdrawal_fields_to_request extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('event_request', 'ex_withdraw_reason', Schema::TYPE_STRING);
        $this->addColumn('event_request', 'ex_withdraw_date', Schema::TYPE_STRING);
        $this->addColumn('event_request', 'hostess_withdraw_reason', Schema::TYPE_DATETIME);
        $this->addColumn('event_request', 'hostess_withdraw_date', Schema::TYPE_DATETIME);
    }

    public function safeDown()
    {

        $this->dropColumn('event_request', 'ex_withdraw_reason');
        $this->dropColumn('event_request', 'ex_withdraw_date');
        $this->dropColumn('event_request', 'hostess_withdraw_reason');
        $this->dropColumn('event_request', 'hostess_withdraw_date');
    }

}
