<?php

use yii\db\Migration;

class m170702_110350_add_bar_code_to_coupon extends Migration
{

    public function up()
    {
        $this->addColumn('coupon', 'bar_code', $this->string()->notNull());
    }

    public function down()
    {
        $this->dropColumn('coupon', 'bar_code');
    }

}
