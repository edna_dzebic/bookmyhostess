<?php

use yii\db\Migration;

class m171218_190058_add_booking_price_to_country extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('country', 'booking_price', $this->decimal(10, 2) . " NOT NULL DEFAULT " . 25);
    }

    public function safeDown()
    {
        $this->dropColumn('country', 'booking_price');
    }

}
