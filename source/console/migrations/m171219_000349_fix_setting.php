<?php

class m171219_000349_fix_setting extends \console\components\Migration
{

    const TABLE_NAME = 'setting';

    public function safeUp()
    {
        $this->renameColumn(static::TABLE_NAME, 'updated_at', 'date_updated');
    }

    public function safeDown()
    {
        return true;
    }

}
