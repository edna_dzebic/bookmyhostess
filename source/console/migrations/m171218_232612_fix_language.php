<?php

class m171218_232612_fix_language extends \console\components\Migration
{

    const TABLE_NAME = 'language';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn(static::TABLE_NAME, 'frontend_selectable', $this->smallInteger()->defaultValue(0));

        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
