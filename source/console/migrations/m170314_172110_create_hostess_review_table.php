<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hostess_review`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `user`
 * - `event`
 * - `event_request`
 */
class m170314_172110_create_hostess_review_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('hostess_review', [
            'id' => $this->primaryKey(),
            'hostess_id' => $this->integer()->notNull(),
            'exhibitor_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'event_request_id' => $this->integer()->notNull(),
            'group_code' => $this->string()->notNull()->comment('set when cron is being sent, used for easier searching reviews for exhibitor'),
            'rating' => $this->decimal(3, 2)->comment('from 1 to 5'),
            'comment' => $this->string(),
            'review_requested' => $this->boolean()->defaultValue(false)->comment('true if hostess request the comment'),
            'created_at' => $this->dateTime()->defaultValue(NULL),
            'updated_at' => $this->dateTime()->defaultValue(NULL),
            'created_by' => $this->integer()->defaultValue(NULL),
            'updated_by' => $this->integer()->defaultValue(NULL),
            'status' => $this->integer()->notNull()
        ]);

        // creates index for column `hostess_id`
        $this->createIndex('idx-hostess_review-hostess_id', 'hostess_review', 'hostess_id');

        // add foreign key for table `user`
        $this->addForeignKey('fk-hostess_review-hostess_id', 'hostess_review', 'hostess_id', 'user', 'id', 'CASCADE');

        // creates index for column `exhibitor_id`
        $this->createIndex('idx-hostess_review-exhibitor_id', 'hostess_review', 'exhibitor_id');

        // add foreign key for table `user`
        $this->addForeignKey('fk-hostess_review-exhibitor_id', 'hostess_review', 'exhibitor_id', 'user', 'id', 'CASCADE');

        // creates index for column `event_id`
        $this->createIndex('idx-hostess_review-event_id', 'hostess_review', 'event_id');

        // add foreign key for table `event`
        $this->addForeignKey('fk-hostess_review-event_id', 'hostess_review', 'event_id', 'event', 'id', 'CASCADE');

        // creates index for column `event_request_id`
        $this->createIndex('idx-hostess_review-event_request_id', 'hostess_review', 'event_request_id');

        // add foreign key for table `event_request`
        $this->addForeignKey('fk-hostess_review-event_request', 'hostess_review', 'event_request_id', 'event_request', 'id', 'CASCADE');

        // creates index for column `group_code`
        $this->createIndex('idx-hostess_review-group_code', 'hostess_review', 'group_code');

        // add foreign keys for behaviors
        $this->addForeignKey('fk-hostess_review-created_by', 'hostess_review', 'created_by', 'user', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-hostess_review-updated_by', 'hostess_review', 'updated_by', 'user', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey('fk-hostess_review-hostess_id', 'hostess_review');

        // drops index for column `hostess_id`
        $this->dropIndex('idx-hostess_review-hostess_id', 'hostess_review');

        // drops foreign key for table `user`
        $this->dropForeignKey('fk-hostess_review-exhibitor_id', 'hostess_review');

        // drops index for column `exhibitor_id`
        $this->dropIndex('idx-hostess_review-exhibitor_id', 'hostess_review');

        // drops foreign key for table `event`
        $this->dropForeignKey('fk-hostess_review-event_id', 'hostess_review');

        // drops index for column `event_id`
        $this->dropIndex('idx-hostess_review-event_id', 'hostess_review');

        // drops foreign key for table `event_request`
        $this->dropForeignKey('fk-hostess_review-event_request', 'hostess_review');

        // drops index for column `event_request_id`
        $this->dropIndex('idx-hostess_review-event_request_id', 'hostess_review');

        // drops index for column `group_code`
        $this->dropIndex('idx-hostess_review-group_code', 'hostess_review');

        $this->dropTable('hostess_review');
    }
}
