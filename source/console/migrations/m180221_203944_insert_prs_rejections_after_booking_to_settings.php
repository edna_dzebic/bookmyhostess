<?php

use yii\db\Migration;
use backend\models\User;

class m180221_203944_insert_prs_rejections_after_booking_to_settings extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $key = 'prs_rejections_after_booking';
        $value = 5;

        $columns = ['key', 'value', 'updated_by', 'date_updated'];

        $user_id = User::findByUsername('admin')->id;

        $values = [
            [$key, $value, $user_id, date("Y-m-d H:i:s")]
        ];

        $this->batchInsert('setting', $columns, $values);
    }

    public function safeDown()
    {
        $this->delete('setting', ['key' => 'prs_rejections_after_booking']);
    }

}
