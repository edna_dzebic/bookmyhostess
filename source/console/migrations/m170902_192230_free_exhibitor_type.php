<?php

use yii\db\Migration;

class m170902_192230_free_exhibitor_type extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('event_request', 'number_of_days', $this->integer()->notNull());
        $this->addColumn('event_request', 'booking_price_per_day', $this->decimal(10, 2)->notNull());
        $this->addColumn('event_request', 'total_booking_price', $this->decimal(10, 2)->notNull());

        $this->addColumn('exhibitor_profile', 'booking_type', $this->integer()->notNull());
        $this->addColumn('exhibitor_profile', 'booking_price_per_day', $this->decimal(10, 2)->notNull());
        
        $this->addColumn('invoice', 'exhibitor_booking_type', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('event_request', 'number_of_days');
        $this->dropColumn('event_request', 'booking_price_per_day');
        $this->dropColumn('event_request', 'total_booking_price');

        $this->dropColumn('exhibitor_profile', 'booking_type');
        $this->dropColumn('exhibitor_profile', 'booking_price_per_day');
        
        $this->dropColumn('invoice', 'exhibitor_booking_type');
    }

}
