<?php

use yii\db\Migration;

class m170910_182449_add_exhibitor_staff_demand_type extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('exhibitor_profile', 'staff_demand_type', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('exhibitor_profile', 'staff_demand_type');
    }

}
