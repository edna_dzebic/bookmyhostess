<?php

use yii\db\Migration;
use yii\db\Schema;

class m160919_232504_remove_50_limit_in_country extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->alterColumn('country', 'name', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        
    }

}
