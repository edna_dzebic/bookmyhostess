<?php

use yii\db\Migration;
use yii\db\Schema;

class m161007_183633_create_table_notification_token extends Migration {

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
        $this->createTable('notification_token', [
            'id' => Schema::TYPE_BIGPK,
            'user_id' => Schema::TYPE_INTEGER,
            'token' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
                ]
        );

        $this->addForeignKey('fk_notification_token_id', 'notification_token', 'user_id', 'user', 'id');
    }

    public function safeDown() {
        $this->dropTable('notification_token');
    }

}
