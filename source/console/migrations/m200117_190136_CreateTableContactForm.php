<?php

class m200117_190136_CreateTableContactForm extends \console\components\Migration
{

    const TABLE_NAME = 'contact_form';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable(
                static::TABLE_NAME, [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'url' => $this->string(),
            'message' => $this->text()->notNull(),
            'subject' => $this->string()->null(),
            'captcha' => $this->string()->notNull(),
            'dsgvo' => $this->integer()->notNull(),
                ]
                , $this->getTableOptions()
        );
    }

    public function safeDown()
    {
        $this->dropTable(static::TABLE_NAME);
    }

}
