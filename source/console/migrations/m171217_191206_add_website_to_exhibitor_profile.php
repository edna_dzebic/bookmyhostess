<?php

use yii\db\Migration;

class m171217_191206_add_website_to_exhibitor_profile extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('exhibitor_profile', 'website', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('exhibitor_profile', 'website');
    }

}
