<?php

class m171218_231515_fix_exhibitor_favorites extends \console\components\Migration
{
    const TABLE_NAME = 'exhibitor_favorites';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropColumn(static::TABLE_NAME, 'note');
        
        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }
}
