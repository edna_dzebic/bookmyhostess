<?php

class m171218_203139_add_default_columns_to_country extends \console\components\Migration
{

    const TABLE_NAME = 'country';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->dropColumn(static::TABLE_NAME, 'has_active_events');
        
        $columns = $this->getDefaultColumns();

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addDefaultColumnsForeignKeys(static::TABLE_NAME);
    }

    public function safeDown()
    {
        return true;
    }

}
