<?php

use yii\db\Migration;
use yii\db\Schema;

class m171104_195539_add_hostess_profile_completeness_and_score extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('hostess_profile', 'price_class', Schema::TYPE_INTEGER . " DEFAULT 3");
        $this->addColumn('hostess_profile', 'profile_completeness_score', Schema::TYPE_FLOAT . " DEFAULT 0");
        $this->addColumn('hostess_profile', 'profile_reliability_score', Schema::TYPE_FLOAT . " DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn('hostess_profile', 'price_class');
        $this->dropColumn('hostess_profile', 'profile_completeness_score');
        $this->dropColumn('hostess_profile', 'profile_reliability_score');
    }

}
