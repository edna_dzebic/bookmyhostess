<?php

use yii\db\Migration;
use yii\db\Schema;

class m171104_214125_add_price_class_to_cuntry_price extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('country_price', 'price_class', Schema::TYPE_STRING . ' NOT NULL DEFAULT "1, 2, 3"');
    }

    public function safeDown()
    {
        $this->dropColumn('country_price', 'price_class');
    }

}
