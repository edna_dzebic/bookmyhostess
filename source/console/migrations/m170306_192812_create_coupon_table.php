<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coupon`.
 */
class m170306_192812_create_coupon_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('coupon', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->notNull(),
            'date_from' => $this->dateTime()->defaultValue(NULL),
            'date_to' => $this->dateTime()->defaultValue(NULL),
            'amount' => $this->decimal(10, 2)->notNull(),
            'created_at' => $this->dateTime()->defaultValue(NULL),
            'updated_at' => $this->dateTime()->defaultValue(NULL),
            'created_by' => $this->integer()->defaultValue(NULL),
            'updated_by' => $this->integer()->defaultValue(NULL),
            'coupon_type' => $this->integer()->comment('0 for fixed amount, 1 for percentage amount'),
            'status' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('fk_coupon_created_by', 'coupon', 'created_by', 'user', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk_coupon_updated_by', 'coupon', 'updated_by', 'user', 'id', 'RESTRICT', 'CASCADE');

        $this->createIndex('fk_coupon_code_idx', 'coupon', 'code');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('coupon');
    }

}
