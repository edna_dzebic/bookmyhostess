<?php

use yii\db\Migration;
use common\models\User;

class m171104_200026_insert_profile_settings extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $columns = ['key', 'value', 'updated_by', 'updated_at'];

        $user_id = User::findByUsername('admin')->id;

        $values = [
            //profile completeness PCS = 20+5+15+15+5+10+5+15+10
            ['pcs_images', '20', $user_id, date("Y-m-d H:i:s")],
            ['pcs_basic_profile', '5', $user_id, date("Y-m-d H:i:s")],
            ['pcs_languages', '15', $user_id, date("Y-m-d H:i:s")],
            ['pcs_work_experience', '15', $user_id, date("Y-m-d H:i:s")],
            ['pcs_summary', '5', $user_id, date("Y-m-d H:i:s")],
            ['pcs_mobile_app', '10', $user_id, date("Y-m-d H:i:s")],
            ['pcs_education', '5', $user_id, date("Y-m-d H:i:s")],
            ['pcs_preferred_cities', '15', $user_id, date("Y-m-d H:i:s")],
            ['pcs_preferred_jobs', '10', $user_id, date("Y-m-d H:i:s")],
            ['pcs_images_min_cnt', '3', $user_id, date("Y-m-d H:i:s")],
            ['pcs_languages_min_cnt', '2', $user_id, date("Y-m-d H:i:s")],
            ['pcs_past_work_min_cnt', '3', $user_id, date("Y-m-d H:i:s")],
            ['pcs_preferred_cities_min_cnt', '1', $user_id, date("Y-m-d H:i:s")],
            ['pcs_preferred_jobs_min_cnt', '1', $user_id, date("Y-m-d H:i:s")],
            // profile reliability PRS
            ['prs_profile_completeness', '20', $user_id, date("Y-m-d H:i:s")],
            ['prs_booked_count', '10', $user_id, date("Y-m-d H:i:s")],
            ['prs_review_count', '10', $user_id, date("Y-m-d H:i:s")],
            ['prs_no_answer_count', '20', $user_id, date("Y-m-d H:i:s")],
            ['prs_reject_count', '20', $user_id, date("Y-m-d H:i:s")],
            ['prs_booked_count_min_cnt', '1', $user_id, date("Y-m-d H:i:s")],
            ['prs_review_count_min_cnt', '1', $user_id, date("Y-m-d H:i:s")],
            // AVG time
            ['prs_avg_answer_time_percent', '20', $user_id, date("Y-m-d H:i:s")],
            // AVG time coeff
            ['prs_time_level_zero_coeff', '1', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_one_coeff', '0.8', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_two_coeff', '0.6', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_three_coeff', '0.4', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_four_coeff', '0.2', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_five_coeff', '0.1', $user_id, date("Y-m-d H:i:s")],
            // AVG time levels
            ['prs_time_level_zero', '4', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_one', '8', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_two', '12', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_three', '16', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_four', '20', $user_id, date("Y-m-d H:i:s")],
            ['prs_time_level_five', '24', $user_id, date("Y-m-d H:i:s")],
        ];

        $this->batchInsert('setting', $columns, $values);
    }

    public function safeDown()
    {
        $keys = [
            'pcs_images',
            'pcs_basic_profile',
            'pcs_languages',
            'pcs_work_experience',
            'pcs_summary',
            'pcs_mobile_app',
            'pcs_education',
            'pcs_preferred_cities',
            'pcs_preferred_jobs',
            'pcs_images_min_cnt',
            'pcs_languages_min_cnt',
            'pcs_past_work_min_cnt',
            'pcs_preferred_cities_min_cnt',
            'pcs_preferred_jobs_min_cnt',
            // PRS
            'prs_profile_completeness',
            'prs_booked_count',
            'prs_review_count',
            'prs_no_answer_count',
            'prs_reject_count',
            'prs_booked_count_min_cnt',
            'prs_review_count_min_cnt',
            // AVG time
            'prs_avg_answer_time_percent',
            // Time levels
            'prs_time_level_zero',
            'prs_time_level_one',
            'prs_time_level_two',
            'prs_time_level_three',
            'prs_time_level_four',
            'prs_time_level_five',
            // Time level coeff
            'prs_time_level_zero_coeff',
            'prs_time_level_one_coeff',
            'prs_time_level_two_coeff',
            'prs_time_level_three_coeff',
            'prs_time_level_four_coeff',
            'prs_time_level_five_coeff',
        ];
        $this->delete('setting', ['key' => $keys]);
    }

}
