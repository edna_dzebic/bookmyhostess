<?php

use yii\db\Migration;
use yii\db\Schema;

class m161120_231816_create_table_newsletter extends Migration {

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('newsletter_subscriber', [
            'id' => Schema::TYPE_BIGPK,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
            'status' => Schema::TYPE_INTEGER,
            'updated_by' => Schema::TYPE_INTEGER
                ], $tableOptions);

        $this->addForeignKey('fk_newsletter_subscriber_updated_by', 'newsletter_subscriber', 'updated_by', 'user', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_newsletter_subscriber_updated_by', 'newsletter_subscriber');
        $this->dropTable('newsletter_subscriber');
    }

}
