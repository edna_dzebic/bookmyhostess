<?php

use yii\db\Migration;
use common\models\EventRequest;

class m160523_155747_set_new_status_to_event_requests extends Migration
{

    public function up()
    {
        Yii::$app->db->createCommand("
            UPDATE event_request
            SET status = " . EventRequest::STATUS_NEW .
                " WHERE
            status IS NULL "
        )->execute();
    }

    public function down()
    {
        
    }

}
