<?php

class m171218_234615_fix_hostess_profile extends \console\components\Migration
{

    const TABLE_NAME = 'hostess_profile';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        

        $this->alterColumn(static::TABLE_NAME, 'date_created', $this->dateTime());

        $columns = [
            'date_updated' => $this->dateTime(),
            'updated_by' => $this->integer()->defaultValue(28),
            'status_id' => $this->bigInteger()->notNull()->defaultValue(1)
        ];

        foreach ($columns as $name => $definition)
        {
            $this->addColumn(static::TABLE_NAME, $name, $definition);
        }

        $this->addForeignKeyS(static::TABLE_NAME, "status_id", "status", "id", "NO ACTION", "CASCADE");
        $this->addForeignKeyS(static::TABLE_NAME, "updated_by", "user", "id", "RESTRICT", "CASCADE");

        $this->addForeignKeyS(static::TABLE_NAME, "country_id", "country", "id", "RESTRICT", "CASCADE");
    }

    public function safeDown()
    {
        return true;
    }

}
