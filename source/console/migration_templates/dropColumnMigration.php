<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
/* @var $table string the name table */
/* @var $fields array the fields */

echo "<?php\n";
?>

use console\components\Migration;

class <?= $className ?> extends Migration
{

    const TABLE_NAME = '<?= $table ?>';

    public function safeUp()
    {
<?php foreach ($fields as $field): ?>
        $this->dropColumn(static::TABLE_NAME, <?= "'" . $field['property'] . "'" ?>);
<?php endforeach; ?>
    }

    public function safeDown()
    {
<?php foreach ($fields as $field): ?>
        $this->addColumn(static::TABLE_NAME, <?= "'" . $field['property'] . "', \$this->" . $field['decorators'] ?>);
<?php endforeach; ?>
    }
}
