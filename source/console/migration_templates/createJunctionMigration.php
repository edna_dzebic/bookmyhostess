<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
/* @var $table string the name table */
/* @var $field_first string the name field first */
/* @var $field_second string the name field second */

echo "<?php\n";
?>

use console\components\Migration;

class <?= $className ?> extends Migration
{

    const TABLE_NAME = '<?= $table ?>';

    public function safeUp()
    {
        $this->createTable(static::TABLE_NAME, [
            '<?= $field_first ?>_id' => $this->integer(),
            '<?= $field_second ?>_id' => $this->integer(),
            'PRIMARY KEY(<?= $field_first ?>_id, <?= $field_second ?>_id)'
        ]);

        $this->createIndex('idx-' . static::TABLE_NAME . '-<?= $field_first ?>_id', '<?= $table ?>', '<?= $field_first ?>_id');
        $this->createIndex('idx-' . static::TABLE_NAME . '-<?= $field_second ?>_id', '<?= $table ?>', '<?= $field_second ?>_id');

        $this->addForeignKey('fk-' . static::TABLE_NAME . '-<?= $field_first ?>_id', '<?= $table ?>', '<?= $field_first ?>_id', '<?= $field_first ?>', 'id', 'CASCADE');
        $this->addForeignKey('fk-' . static::TABLE_NAME . '-<?= $field_second ?>_id', '<?= $table ?>', '<?= $field_second ?>_id', '<?= $field_second ?>', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(static::TABLE_NAME);
    }
}
