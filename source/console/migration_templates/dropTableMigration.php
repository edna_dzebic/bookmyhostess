<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
/* @var $table string the name table */
/* @var $fields array the fields */

echo "<?php\n";
?>

use console\components\Migration;

class <?= $className ?> extends Migration
{

    const TABLE_NAME = '<?= $table ?>';

    public function safeUp()
    {
        $this->dropTable(static::TABLE_NAME);
    }

    public function safeDown()
    {
        $this->createTable(
            static::TABLE_NAME,
            [
<?php foreach ($fields as $field): ?>
<?php if ($field == end($fields)): ?>
                '<?= $field['property'] ?>' => $this-><?= $field['decorators'] . "\n" ?>
<?php else: ?>
                '<?= $field['property'] ?>' => $this-><?= $field['decorators'] . ",\n" ?>
<?php endif; ?>
<?php endforeach; ?>
            ],
            $this->getTableOptions()
        );
    }
}
