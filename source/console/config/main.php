<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'),
        //
        require(__DIR__ . '/params.php'),
        //
        require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'console\controllers',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ]
    ],
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'generatorTemplateFiles' => [
                'create_table' => '@console/migration_templates/createTableMigration.php',
                'drop_table' => '@console/migration_templates/dropTableMigration.php',
                'add_column' => '@console/migration_templates/addColumnMigration.php',
                'drop_column' => '@console/migration_templates/dropColumnMigration.php',
                'create_junction' => '@console/migration_templates/createJunctionMigration.php'
            ]
        ]
    ],
    'params' => $params,
];
