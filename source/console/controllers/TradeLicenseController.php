<?php

namespace console\controllers;

use Yii;
use common\models\TradeLicenseExtend;

class TradeLicenseController extends BaseController
{

    /**
     * Delete all passed requests for extending trade license
     * 
     * @return int
     */
    public function actionClear()
    {
        $query = TradeLicenseExtend::find()
                ->andWhere(['<', "date", date('Y-m-d')]);

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        $models = $query->all();

        foreach ($models as $model)
        {
            $model->delete();
        }

        return 0;
    }

}
