<?php

namespace console\controllers;

use common\models\User;
use common\models\ExhibitorProfile;


class ExhibitorImportController extends \yii\console\Controller {

    public function actionIndex($inputFile) {
        //  Read your Excel workbook
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFile);
        } catch (Exception $e) {
            echo 'Error loading file "' . pathinfo($inputFile, PATHINFO_BASENAME) . '": ' . $e->getMessage();
            return 0;
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        // Loop through each row of the worksheet in turn
        for ($row = 1; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE)[0];

            if ($rowData[0] === null) {
                return 1;
            }

            $username = trim(htmlspecialchars($rowData[0]));

            $company = trim(htmlspecialchars($rowData[3]));
            $countryId = trim(htmlspecialchars($rowData[5]));

            $street = trim(htmlspecialchars($rowData[7]));
            $postalCode = trim(htmlspecialchars($rowData[8]));
            $cityName = trim(htmlspecialchars($rowData[9]));


            $userModel = User::findOne(["username" => $username]);

            if (isset($userModel)) {
                $profileModel = ExhibitorProfile::findOne(["user_id" => $userModel->id]);

                $profileModel->company_name = $company;
                $profileModel->city = $cityName;
                $profileModel->street = $street;
                $profileModel->postal_code = $postalCode;
                $profileModel->country_id = $countryId;

                if ($profileModel->update(false) === false) {
                    echo "Failed Importing : " . $rowData[0] . "\n"
                    . var_export($profileModel->getErrors()) . " \n";
                }
            } else {
                echo "\n";
                echo $row;
                echo "\n";
            }
        }
    }

    public function actionGender($inputFile) {
        //  Read your Excel workbook
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFile);
        } catch (Exception $e) {
            echo 'Error loading file "' . pathinfo($inputFile, PATHINFO_BASENAME) . '": ' . $e->getMessage();
            return 0;
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        // Loop through each row of the worksheet in turn
        for ($row = 1; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE)[0];

            if ($rowData[0] === null) {
                return 1;
            }

            $username = trim(htmlspecialchars($rowData[0]));

            $gender = trim(htmlspecialchars($rowData[3]));

            $userModel = User::findOne(["username" => $username]);

            if (isset($userModel)) {

                $userModel->gender = $gender;

                if ($userModel->update(false) === false) {
                    echo "Failed Importing : " . $rowData[0] . "\n"
                    . var_export($userModel->getErrors()) . " \n";
                }
            } else {
                echo "\n";
                echo $row;
                echo "\n";
            }
        }
    }

}
