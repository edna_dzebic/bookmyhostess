<?php

namespace console\controllers;

use Yii;
use common\models\Email;
use common\models\User;
use common\models\HostessProfile;

class ApproveController extends BaseController
{

    /**
     * This cron job sends and email to hostesses to send approval request
     * Email is sent 5 days after profile is activated
     * Email is sent only once
     * 
     * @return int
     */
    public function actionReminder()
    {

        $approvals = [
            HostessProfile::APPROVAL_APPROVED,
            HostessProfile::APPROVAL_REQUESTED
        ];

        $query = User::find()
                ->leftJoin("hostess_profile", "user.id = hostess_profile.user_id")
                ->andWhere(["user.status_id" => Yii::$app->status->active])
                ->andWhere(["user.role" => User::ROLE_HOSTESS])
                ->andWhere("hostess_profile.admin_approved NOT IN ( " . implode(",", $approvals) . " )")
                ->andWhere(['=', 'DATE(user.activated_at)', date('Y-m-d', strtotime('-5 days'))]);


        $models = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($models as $model)
        {
            Email::sendApproveReminderEmailToHostess($model);
        }
        
        return 0;
    }

}
