<?php

namespace console\controllers;

use Yii;
use common\models\Email;
use common\models\User;

class RequestController extends BaseController
{

    /**
     * This cron jobs sends and email to newly created exhibitors, which still
     * havent sent any requests, to do so.
     * Current rule is three days after activation of profile.
     * 
     * @return int
     */
    public function actionReminder()
    {
        $query = User::find()
                ->andWhere(["user.status_id" => Yii::$app->status->active])
                ->andWhere(["user.role" => User::ROLE_EXHIBITOR])
                ->andWhere("user.id NOT IN (SELECT request.requested_user_id FROM request)")
                ->andWhere("user.activated_at IS NOT NULL")
                ->andWhere(['=', 'DATE(user.activated_at)', date('Y-m-d', strtotime('-3 days'))]);

        $models = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($models as $model)
        {
            Email::sendRequestReminderEmailToExhibitor($model);
        }

        return 0;
    }

}
