<?php

namespace console\controllers;

use Yii;
use common\models\HostessProfile;
use yii\console\Controller;

class HostessMapperController extends Controller
{

    public function actionMap()
    {
        $users = HostessProfile::find()
                ->andWhere("country_id is not null")
                ->all();

        foreach ($users as $user)
        {
            $this->insertMappings($user);
        }
    }

    private function insertMappings($user)
    {
        $countryId = $user->country_id;
        $country = \common\models\Country::findOne(["id" => $countryId]);

        if (is_object($country))
        {
            $tableName = \common\models\HostessCountryMapping::tableName();

            $attributes = [
                'user_id',
                'country_id',
                'created_by',
                'updated_by',
                'date_created',
                'date_updated',
                'status_id'
            ];
            $values = [];

            // insert country of origin by default
            $values[] = [
                $user->user_id,
                $countryId,
                $user->user_id,
                $user->user_id,
                date("Y-m-d H:i:s"),
                date("Y-m-d H:i:s"),
                Yii::$app->status->active
            ];

            // add all mappings for country of origin
            $mappings = $country->sourceMappings;
            foreach ($mappings as $mapping)
            {
                $values[] = [
                    $user->user_id,
                    $mapping->target_country_id,
                    $user->user_id,
                    $user->user_id,
                    date("Y-m-d H:i:s"),
                    date("Y-m-d H:i:s"),
                    Yii::$app->status->active
                ];
            }


            Yii::$app->db->createCommand()
                    ->batchInsert($tableName, $attributes, $values)
                    ->execute();
        } else
        {
            echo "Country $user->country_id not found for user $user->user_id!\n";
        }
    }

}
