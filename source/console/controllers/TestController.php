<?php

namespace console\controllers;

use common\models\Email;

/**
 * Test controller
 */
class TestController extends BaseController
{

    public function actionMail()
    {
        Email::sendKronTestEmail();
        return 0;
    }

}
