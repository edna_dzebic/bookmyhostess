<?php

namespace console\controllers;

use Yii;

class HostessProfileController extends BaseController
{

    /**
     * This cron job recalculates all scores for profile on daily basis
     * 
     * @return int
     */
    public function actionScores()
    {
        $query = \common\models\HostessProfile::find()
                ->andWhere(["status_id" => Yii::$app->status->active]);

        foreach ($query->each() as $profile)
        {
           $profile->recalculateProfileCompleteness(); 
        }        

        return 0;
    }

}
