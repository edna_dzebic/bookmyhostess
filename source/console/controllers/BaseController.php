<?php

namespace console\controllers;

class BaseController extends \yii\console\Controller
{

    private $start;
    private $className;

    /**
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, $module, $config = array())
    {
        parent::__construct($id, $module, $config);

        $this->className = get_class($this);

        $start = new \DateTime('now');
        $this->start = $start;
    }

    /**
     * 
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $start = new \DateTime('now');

        $messages = [
            "Class is " . $this->className . " and action is " . $action->id,
            "Start time " . $start->format("Y-m-d H:i:s")
        ];

        $this->printMessages($messages);

        return parent::beforeAction($action);
    }

    /**
     * 
     * @param \yii\base\Action $action
     * @return bool
     */
    public function afterAction($action, $result)
    {
        $end = new \DateTime('now');

        $messages = [
            "Class is " . $this->className . " and action is " . $action->id,
            "End time " . $end->format("Y-m-d H:i:s")
        ];

        if (isset($this->start))
        {
            $diff = $this->start->diff($end);
            $messages[] = "Elapsed time " . $diff->format('%H:%I:%S');
        }

        $this->printMessages($messages);

        return parent::afterAction($action, $result);
    }

    protected function printMessages($messages)
    {
        foreach ($messages as $message)
        {
            $this->printMessage($message);
        }
    }

    protected function printMessage($message)
    {
        $this->stdout($message . PHP_EOL);
    }

}
