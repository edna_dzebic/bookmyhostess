<?php

namespace console\controllers;

use Yii;
use common\models\Email;
use common\models\Request;

/**
 * BookNotify controller.
 */
class BookNotifyController extends BaseController
{

    /**
     * This cron jobs reminds exhibitors to complete booking.
     * Emails are sent only on working days.
     * Emails are sent 24 working hours after request is accepted.
     * On Monday, those are requests which are accepted on Friday, Saturday and Sunday.
     * Other days those are requests accepted one working day before ( yesterday ).
     * 
     * @return int
     */
    public function actionNotify()
    {
        $day = date('N', strtotime('now'));

        if ($day > 5)
        {
            $this->printMessage("Cron should be executed only on work days!");
            return 0;
        }

        $query = Request::find()
                ->joinWith([
                    'event'
                ])
                ->andWhere(
                        [
                            'or',
                            ['<>', 'request.completed', 1],
                            ['request.completed' => null]
                        ]
                )
                ->andWhere(['request.status_id' => Yii::$app->status->request_accept])
                ->andWhere(['>=', "event.end_date", date('Y-m-d')]);

        /**
         * If today is Monday, send reminder for requests accepted on
         * Friday, Saturday and Sunday.
         * Otherwise, send reminder only to requests accepted yesterday
         */
        if ($day == 1)
        {
            $query->andWhere(['>=', 'DATE(request.date_responded)', date('Y-m-d', strtotime('-3 days'))]);
            $query->andWhere(['<>', 'DATE(request.date_responded)', date('Y-m-d', strtotime('now'))]);
        } else
        {
            $query->andWhere(['=', 'DATE(request.date_responded)', date('Y-m-d', strtotime('yesterday'))]);
        }

        $requests = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($requests as $req)
        {
            // try to find one complete booking, if not found then send email
            $exists = Request::find()
                    ->andWhere(["request.requested_user_id" => $req->requested_user_id])
                    ->andWhere(["request.event_id" => $req->event_id])
                    ->andWhere(["request.completed" => 1])
                    ->exists();

            if (!$exists)
            {
                Email::sendBookNotifyEmailToExhibitor($req);
            }
        }

        return 0;
    }

    /**
     * This cron jobs clears bookings not completed by exhibitor.
     * Emails are sent only on working days.
     * Emails are sent 48 working hours after request is accepted.
     * On Monday, those are requests which are accepted on past Thursday
     * On Tuesday, those are requests accepted on Friday and for Weekend
     * Other days those are requests accepted 2 working days before ( day before yesterday )
     * 
     * @return int
     */
    public function actionClear()
    {

        $day = date('N', strtotime('now'));

        if ($day > 5)
        {
            $this->printMessage("Cron should be executed only on work days!");
            return 0;
        }

        $query = Request::find()
                ->joinWith([
                    'event'
                ])
                ->andWhere(
                        [
                            'or',
                            ['<>', 'request.completed', 1],
                            ['request.completed' => null]
                        ]
                )
                ->andWhere(['request.status_id' => Yii::$app->status->request_accept])
                ->andWhere(['>=', "event.end_date", date('Y-m-d')]);



        /**
         * If it is Monday, send email only for last Thursday.
         * If it is Tuesday, send email for last Friday and Weekend
         * If it is some other day, send email for 2 days before
         */
        if ($day == 1)
        {
            $query->andWhere(['=', 'DATE(request.date_responded)', date('Y-m-d', strtotime('-4 days'))]);
        } else if ($day == 2)
        {
            $query->andWhere(['>=', 'DATE(request.date_responded)', date('Y-m-d', strtotime('-4 days'))]);
            $query->andWhere(['<', 'DATE(request.date_responded)', date('Y-m-d', strtotime('yesterday'))]);
        } else
        {
            $query->andWhere(['=', 'DATE(request.date_responded)', date('Y-m-d', strtotime('-2 days'))]);
        }

        $requests = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($requests as $req)
        {
            // try to find one complete booking, if not found then send email to exhibitor
            $exists = Request::find()
                    ->andWhere(["request.requested_user_id" => $req->requested_user_id])
                    ->andWhere(["request.event_id" => $req->event_id])
                    ->andWhere(["request.completed" => 1])
                    ->exists();

            $req->status_id = Yii::$app->status->request_expired;
            $req->update(false);

            if (!$exists)
            {
                Email::sendBookDeletedEmailToExhibitor($req);
            }

            Email::sendBookDeletedEmailToHostess($req);
        }

        return 0;
    }

    /**
     * This cron job reminder hostess to respond to request.
     * Emails are sent only on working days.
     * Hostess has one working day to accept request, after that, request is deleted.
     * On Monday, emails are sent for past Fridays and Weekend
     * On other days, emails are sent for requests created yesterday.
     * 
     * @return int
     */
    public function actionNotifyHostess()
    {

        $day = date('N', strtotime('now'));

        if ($day > 5)
        {
            $this->printMessage("Cron should be executed only on work days!");
            return 0;
        }

        $query = Request::find()
                ->joinWith([
                    'event'
                ])
                ->andWhere(
                        [
                            'or',
                            ['<>', 'request.completed', 1],
                            ['request.completed' => null]
                        ]
                )
                ->andWhere(['request.status_id' => Yii::$app->status->request_new])
                ->andWhere(['>=', "event.end_date", date('Y-m-d')]);

        /**
         * If today is Monday, send reminder for requests created on
         * Friday, Saturday and Sunday.
         * Otherwise, send reminder only to requests created yesterday
         */
        if ($day == 1)
        {
            $query->andWhere(['>=', 'DATE(request.date_created)', date('Y-m-d', strtotime('-3 days'))]);
            $query->andWhere(['<>', 'DATE(request.date_created)', date('Y-m-d', strtotime('now'))]);
        } else
        {
            $query->andWhere(['=', 'DATE(request.date_created)', date('Y-m-d', strtotime('yesterday'))]);
        }

        $requests = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($requests as $req)
        {
            Email::sendRespondToRequestReminderToHostess($req);
        }

        return 0;
    }

    /**
     * This cron jobs clears requests not responded by hostess.
     * Emails are sent only on working days.
     * Emails are sent 48 working hours after request is created.
     * On Monday, those are requests which are created on past Thursday
     * On Tuesday, those are requests created on Friday and for Weekend
     * Other days those are requests created 2 working days before ( day before yesterday )
     * 
     * @return int
     */
    public function actionClearHostess()
    {

        $day = date('N', strtotime('now'));

        if ($day > 5)
        {
            $this->printMessage("Cron should be executed only on work days!");
            return 0;
        }

        $query = Request::find()
                ->joinWith([
                    'event'
                ])
                ->andWhere(
                        [
                            'or',
                            ['<>', 'request.completed', 1],
                            ['request.completed' => null]
                        ]
                )
                ->andWhere(['request.status_id' => Yii::$app->status->request_new])
                ->andWhere(['>=', "event.end_date", date('Y-m-d')]);

        /**
         * If it is Monday, send email only for last Thursday.
         * If it is Tuesday, send email for last Friday and Weekend
         * If it is some other day, send email for 2 days before
         */
        if ($day == 1)
        {
            $query->andWhere(['=', 'DATE(request.date_created)', date('Y-m-d', strtotime('-4 days'))]);
        } else if ($day == 2)
        {
            $query->andWhere(['>', 'DATE(request.date_created)', date('Y-m-d', strtotime('-4 days'))]);
            $query->andWhere(['<', 'DATE(request.date_created)', date('Y-m-d', strtotime('yesterday'))]);
        } else
        {
            $query->andWhere(['=', 'DATE(request.date_created)', date('Y-m-d', strtotime('-2 days'))]);
        }

        $requests = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($requests as $req)
        {
            // try to find one complete booking, if not found then send email
            $exists = Request::find()
                    ->andWhere(["request.requested_user_id" => $req->requested_user_id])
                    ->andWhere(["request.event_id" => $req->event_id])
                    ->andWhere(["request.completed" => 1])
                    ->exists();


            $req->status_id = Yii::$app->status->request_expired_hostess;
            $req->update(false);

            if (!$exists)
            {
                Email::sendBookDeletedEmailToExhibitor($req);
            }

            Email::sendRequestDeletedNotificationToHostess($req);
        }

        return 0;
    }

}
