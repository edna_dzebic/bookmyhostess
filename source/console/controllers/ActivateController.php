<?php

namespace console\controllers;

use Yii;
use common\models\Email;
use common\models\User;

class ActivateController extends BaseController
{

    /**
     * This cron job sends and reminder email to activate profile
     * Email is sent to hostesses and exhibitors
     * Email is sent only once one day after registration
     * 
     * @return int
     */
    public function actionNotify()
    {

        $query = User::find()
                ->andWhere(["status_id" => Yii::$app->status->pending])
                ->andWhere(['=', 'DATE(date_created)', date('Y-m-d', strtotime('yesterday'))]);

        $models = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($models as $model)
        {

            $model->email_validation_secret = $this->generateEmailValidationSecret($model);
            $model->update(false);

            if ($model->role == User::ROLE_EXHIBITOR)
            {
                Email::sendActivateReminderEmailToExhibitor($model);
            } else
            {
                Email::sendActivateReminderEmailToHostess($model);
            }
        }
        
        return 0;
    }

    private function generateEmailValidationSecret($model)
    {
        return sha1(microtime(true) . $model->email);
    }

}
