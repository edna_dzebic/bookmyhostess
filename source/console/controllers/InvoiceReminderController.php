<?php

namespace console\controllers;

use Yii;
use common\models\Invoice;
use common\models\InvoiceReminder;

class InvoiceReminderController extends BaseController
{

    public function actionFirstReminder()
    {
        $day = date('N', strtotime('now'));

        if ($day > 5)
        {
            $this->printMessage("Cron should be executed only on work days!");
            return 0;
        }

        $query = Invoice::find()
                ->joinWith([
                    'userRequested'
                ])
                ->andWhere(['invoice.is_reminder_allowed' => 1])
                ->andWhere(['invoice.payment_type' => Invoice::PAYMENT_TYPE_INVOICE])
                ->andWhere(['invoice.status' => Invoice::STATUS_NOT_PAID]);

        /**
         * If day is bigger than Wednesday, we need to take last week.
         * If day is lower than Wednesday, then week before last week!
         */
        if ($day < 3)
        {
            $date = date('Y-m-d', strtotime('-11 days'));
        } else
        {
            $date = date('Y-m-d', strtotime('-9 days'));
        }

        $query->andWhere(['=', 'DATE(invoice.date_created)', $date]);
        
        $invoices = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($invoices as $invoice)
        {
            if ($this->checkReminderExists($invoice->id, InvoiceReminder::FLAG_FIRST_REMINDER))
            {
                continue;
            }

            if ($this->saveReminder($invoice, InvoiceReminder::FLAG_FIRST_REMINDER))
            {
                \common\models\Email::sendInvoiceFirstReminderEmailToExibitor($invoice);
            }
        }
    }

    public function actionSecondReminder()
    {
        $day = date('N', strtotime('now'));

        if ($day > 5)
        {
            $this->printMessage("Cron should be executed only on work days!");
            return 0;
        }

        $query = InvoiceReminder::find()
                ->joinWith([
                    'invoice.userRequested'
                ])
                ->andWhere(['invoice_reminder.flag' => InvoiceReminder::FLAG_FIRST_REMINDER])
                ->andWhere(['invoice.is_reminder_allowed' => 1])
                ->andWhere(['invoice.payment_type' => Invoice::PAYMENT_TYPE_INVOICE])
                ->andWhere(['invoice.status' => Invoice::STATUS_NOT_PAID]);

        /**
         * If day is bigger than Wednesday, we need to take last week.
         * If day is lower than Wednesday, then week before last week!
         */
        if ($day < 3)
        {
            $date = date('Y-m-d', strtotime('-11 days'));
        } else
        {
            $date = date('Y-m-d', strtotime('-9 days'));
        }

        $query->andWhere(['=', 'DATE(invoice_reminder.date_created)', $date]);
        
        $reminders = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($reminders as $reminder)
        {
            $invoice = $reminder->invoice;

            if ($this->checkReminderExists($invoice->id, InvoiceReminder::FLAG_SECOND_REMINDER))
            {
                continue;
            }

            if ($this->saveReminder($invoice, InvoiceReminder::FLAG_SECOND_REMINDER))
            {
                \common\models\Email::sendInvoiceSecondReminderEmailToExibitor($invoice);
            }
        }
    }

    /**
     * 
     * @param Invoice $invoice
     * @param type $flag
     * @return type
     */
    private function saveReminder(Invoice $invoice, $flag)
    {
        $reminder = new InvoiceReminder();
        $reminder->flag = $flag;
        $reminder->invoice_id = $invoice->id;

        if ($reminder->save() === false)
        {
            $this->printMessage(var_export($reminder->getErrors(), true));
            return false;
        }

        return true;
    }

    private function checkReminderExists($invoiceId, $flag)
    {
        $reminder = InvoiceReminder::findOne([
                    'invoice_id' => $invoiceId,
                    'flag' => $flag
        ]);

        return is_object($reminder);
    }

}
