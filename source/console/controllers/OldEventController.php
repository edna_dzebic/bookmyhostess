<?php

namespace console\controllers;

use Yii;
use common\models\Request;
use common\models\Register;

class OldEventController extends BaseController
{

    /**
     * This cron job deletes all even registrations after event end date is passed.
     * 
     * @return int
     */
    public function actionClearRegistration()
    {

        $query = Register::find()
                ->joinWith([
                    'event'
                ])->andWhere(['<', "event.end_date", date('Y-m-d')])
                ->select("register.id");

        $requests = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        Register::deleteAll(["id" => $requests]);
        
        return 0;
    }

    /**
     * This cron job deletes all not completed requests after end date of event passes
     * 
     * @return int
     */
    public function actionClearRequests()
    {

        $query = Request::find()
                ->joinWith([
                    'event'
                ])
                ->andWhere(['<', "event.end_date", date('Y-m-d')])
                ->andWhere(
                        [
                            'or',
                            ['<>', 'completed', 1],
                            ['completed' => null]
                        ]
                )
                ->andWhere(["<>", "request.status_id", Yii::$app->status->request_expired])
                ->select("request.id");

        $requests = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        Request::updateAll(["status_id" => Yii::$app->status->request_expired], ["id" => $requests]);
        
        return 0;
    }

}
