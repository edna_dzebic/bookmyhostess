<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\ExhibitorProfile;
use common\models\Request;
use common\models\Event;
use common\models\Invoice;

/**
 * Fix controller
 */
class FixController extends Controller
{

    public function actionHostessProfileScores()
    {
        $profiles = \common\models\HostessProfile::find()->all();
        foreach ($profiles as $profile)
        {
            $profile->recalculateProfileCompleteness();
        }
    }
    
    public function actionFixDataForFreeBookings()
    {
        $profiles = ExhibitorProfile::find()->all();
        foreach ($profiles as $profile)
        {
            $profile->booking_price_per_day = Yii::$app->settings->booking_price;
            $profile->booking_type = ExhibitorProfile::BOOKING_TYPE_REGULAR;
            $profile->update(false);
        }


        $models = Request::find()->all();
        foreach ($models as $model)
        {
            $model->booking_price_per_day = Yii::$app->settings->booking_price;
            $model->number_of_days = $model->calculateBookingDays();
            $model->total_booking_price = $model->calculateBookingPrice();
            $model->update(false);
        }

        $invoices = Invoice::find()->all();
        foreach ($invoices as $invoice)
        {
            $invoice->exhibitor_booking_type = ExhibitorProfile::BOOKING_TYPE_REGULAR;
            $invoice->update(false);
        }
    }

    public function actionFixInvoices()
    {
        $directory = Yii::getAlias("@frontend") . "/web/invoices/";

        $array = scandir($directory);
        foreach ($array as $fileName)
        {
            echo "\n" . $fileName . "\n";

            $splitted = split("_", $fileName);

            if (isset($splitted[1]))
            {
                $id = str_replace(".pdf", '', $splitted[1]);

                $model = Invoice::findOne(["id" => $id]);

                if (is_object($model))
                {
                    $model->filename = "invoice/" . $fileName;

                    if ($model->update() === false)
                    {
                        echo var_dump($model->getErrors());
                        echo "\n";
                    }
                } else
                {
                    echo " \n Invoice " . $id . " not found \n";
                }
            }
        }

        echo 'DONE';
    }

    public function actionIndexx()
    {

        $models = ExhibitorProfile::find()
                ->joinWith(["user"])
                ->all();

        foreach ($models as $model)
        {
            $model->last_profile_view = $model->user->last_login;
            $model->update(false);
        }

        echo 'DONE';
    }

    public function actionFixEvents()
    {

        $events = Event::find()
                ->andWhere('YEAR(start_date) = "2017" OR ( YEAR(start_date) = "2016" AND (MONTH(start_date) = "11" OR MONTH(start_date) = "12") )')
                ->all();

        $result = [];

        foreach ($events as $event)
        {
            $startDate = $event->start_date;
            $endDate = $event->end_date;
            $lastYearStartTime = strtotime("-1 year", strtotime($startDate));
            $lastYearEndTime = strtotime("-1 year", strtotime($endDate));

            $lastYearStartDate = date("Y-m-d", $lastYearStartTime);
            $lastYearEndDate = date("Y-m-d", $lastYearEndTime);


            $newModel = $this->getNewModel($event);

            if (is_object($newModel))
            {

                $event->start_date = $lastYearStartDate;
                $event->end_date = $lastYearEndDate;
                $event->update(false);

                $this->updateRegistrations($event, $newModel);
                $this->updateRequests($event, $newModel);
            }
        }

        echo 'DONE';
        return;
    }

    private function getNewModel($model)
    {
        $newModel = clone $model;
        $newModel->setIsNewRecord(true);
        unset($newModel->id);

        if ($newModel->save())
        {

            foreach ($model->eventToDressCodes as $dressCode)
            {
                $dressCodeModel = new \common\models\EventDressCode();
                $dressCodeModel->event_id = $newModel->id;
                $dressCodeModel->event_dress_code_id = $dressCode->event_dress_code_id;
                $dressCodeModel->save();
            }

            foreach ($model->eventToJobs as $job)
            {
                $jobModel = new \common\models\EventJob();
                $jobModel->event_id = $newModel->id;
                $jobModel->job_id = $job->job_id;
                $jobModel->save();
            }


            return $newModel;
        }

        return null;
    }

    private function updateRegistrations($oldModel, $newModel)
    {

        $registrations = \common\models\Register::find()
                ->andWhere(["event_id" => $oldModel->id])
                ->andWhere("DATE(date_created) > DATE(\"" . $oldModel->start_date . "\")")
                ->all();

        foreach ($registrations as $registration)
        {
            $registration->event_id = $newModel->id;
            $registration->update(false);
        }
    }

    private function updateRequests($oldModel, $newModel)
    {

        $requests = \common\models\Request::find()
                ->andWhere(["event_id" => $oldModel->id])
                ->andWhere("DATE(request.date_created) > DATE(\"" . $oldModel->start_date . "\")")
                ->all();

        foreach ($requests as $request)
        {
            $request->event_id = $newModel->id;
            $request->update(false);
        }
    }

}
