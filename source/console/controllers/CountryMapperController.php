<?php

namespace console\controllers;

use Yii;
use common\models\Country;
use common\models\CountryMapping;
use yii\console\Controller;

class CountryMapperController extends Controller
{

    public function actionEu()
    {
        $countries = $this->getCountries();

        foreach ($countries as $data)
        {
            $c = Country::findOne(["name" => $data["name"]]);

            if (is_object($c))
            {
                $c->is_eu = 1;
                $c->currency_label = $data["currency_label"];
                $c->currency_symbol = $data["currency_symbol"];

                if ($c->update() === false)
                {
                    echo var_export($c->getErrors(), true);
                    echo "\n";
                }
            } else
            {
                echo "Country not found " . $data["name"];
                echo "\n";
            }
        }
    }

    public function actionEuMappings()
    {
        $countries = Country::find()->andWhere(["is_eu" => 1])->all();
        foreach ($countries as $source)
        {
            $targets = Country::find()
                    ->andWhere(["is_eu" => 1])
                    ->andWhere(["<>", "id", $source->id])
                    ->all();

            foreach ($targets as $target)
            {
                $mapping = new CountryMapping();
                $mapping->source_country_id = $source->id;
                $mapping->target_country_id = $target->id;
                $mapping->save();
            }

            $mapping = new CountryMapping();
            $mapping->source_country_id = $source->id;
            $mapping->target_country_id = 13; // United Arab Emirates
            $mapping->save();
        }
    }

    public function actionOtherMappings()
    {
        $mappings = $this->getMappings();

        foreach ($mappings as $key => $data)
        {
            $cs = Country::findOne(["name" => $key]);

            if (is_object($cs))
            {
                $targets = $data;

                foreach ($targets as $target)
                {
                    $ct = Country::findOne(["name" => $target]);

                    if (is_object($ct))
                    {
                        $mapping = new CountryMapping();
                        $mapping->source_country_id = $cs->id;
                        $mapping->target_country_id = $ct->id;
                        $mapping->save();
                    } else
                    {
                        echo "Country not found " . $target;
                        echo "\n";
                    }
                }
            } else
            {
                echo "Country not found " . $key;
                echo "\n";
            }
        }
    }

    private function getMappings()
    {
        return [
            "Australia" => [
                'Germany',
                'Austria',
                'Switzerland',
                'Italy',
                'UAE',
            ],
            "Bosnia and Herzegovina" => [
                'Germany',
                'Austria',
                'Switzerland',
                'UAE',
            ],
            "Bahrain" => [
                "UAE"
            ]
        ];
    }

    private function getCountries()
    {
        return [
            ['name' => 'Austria', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Italy', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Belgium', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Latvia', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Bulgaria', 'currency_label' => 'BGN', 'currency_symbol' => 'лв'],
            ['name' => 'Lithuania', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Croatia', 'currency_label' => 'HRK', 'currency_symbol' => 'kn'],
            ['name' => 'Luxembourg', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Cyprus', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Malta', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Czech Republic', 'currency_label' => 'CZK', 'currency_symbol' => 'Kč'],
            ['name' => 'Netherlands', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Denmark', 'currency_label' => 'DKK', 'currency_symbol' => 'kr'],
            ['name' => 'Poland', 'currency_label' => 'PLN', 'currency_symbol' => 'zł'],
            ['name' => 'Estonia', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Portugal', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Finland', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Romania', 'currency_label' => 'RON', 'currency_symbol' => 'lei'],
            ['name' => 'France', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Slovakia', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Germany', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Slovenia', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Greece', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Spain', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'Hungary', 'currency_label' => 'HUN', 'currency_symbol' => 'Ft'],
            ['name' => 'Sweden', 'currency_label' => 'SEK', 'currency_symbol' => 'kr'],
            ['name' => 'Ireland', 'currency_label' => 'EUR', 'currency_symbol' => '€'],
            ['name' => 'United Kingdom', 'currency_label' => 'BGP', 'currency_symbol' => '£'],
        ];
    }

}
