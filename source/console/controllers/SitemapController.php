<?php

namespace console\controllers;

use Yii;
use console\models\Sitemap;
use common\models\Event;
use common\models\User;

/**
 * Sitemap controller
 */
class SitemapController extends BaseController
{

    /**
     * This cron job generates sitemap every day.
     * 
     * @return int
     */
    public function actionGenerate()
    {
        $sitemap = new Sitemap('https://www.bookmyhostess.com');
        $sitemap->setPath(Yii::$app->params['sitemap_path']);

        $sitemap->addItem('/', '1.0', 'weekly', 'Today');
        $sitemap->addItem('/find-hostess', '0.8', 'daily', 'Today');
        $sitemap->addItem('/upcoming-events', '0.8', 'daily', 'Today');
        $sitemap->addItem('/site/hostess', '0.8', 'weekly', 'Today');
        $sitemap->addItem('/site/#about', '0.6', 'weekly', 'Today');
        $sitemap->addItem('/site/#pricing', '0.6', 'weekly', 'Today');
        $sitemap->addItem('/site/#contact', '0.6', 'weekly', 'Today');
        $sitemap->addItem('/login', '0.6', 'weekly', 'Today');
        $sitemap->addItem('/register', '0.6', 'weekly', 'Today');
        $sitemap->addItem('/exhibitor/register', '0.6', 'weekly', 'Today');
        $sitemap->addItem('/hostess-profile/register', '0.6', 'weekly', 'Today');
        $sitemap->addItem('/blog', '0.6', 'daily', 'Today');

        $hostesses = User::find()
                ->innerJoin("hostess_profile", "user.id = hostess_profile.user_id")
                ->andWhere(["user.status_id" => Yii::$app->status->active])
                ->andWhere(["user.role" => User::ROLE_HOSTESS])
                ->all();

        foreach ($hostesses as $hostess)
        {
            $sitemap->addItem('/hostess/' . $hostess->id, '0.5', 'weekly', 'Today');
        }

        $events = Event::find()
                ->andWhere(["event.status_id" => Yii::$app->status->active])
                ->andWhere('event.end_date>=NOW()')
                ->all();

        foreach ($events as $event)
        {
            $sitemap->addItem('/event/' . $event->id, '0.5', 'weekly', 'Today');
        }

        $sitemap->addItem('/blog/1', '0.5', 'never', 'Today');
        $sitemap->addItem('/blog/2', '0.5', 'never', 'Today');
        $sitemap->addItem('/blog/3', '0.5', 'never', 'Today');
        $sitemap->addItem('/blog/4', '0.5', 'never', 'Today');
        $sitemap->addItem('/blog/5', '0.5', 'never', 'Today');
        $sitemap->addItem('/blog/6', '0.5', 'never', 'Today');
        $sitemap->addItem('/blog/7', '0.5', 'never', 'Today');
        $sitemap->addItem('/blog/8', '0.5', 'never', 'Today');
        $sitemap->addItem('/blog/9', '0.5', 'never', 'Today');

        $sitemap->createSitemapIndex('https://www.bookmyhostess.com/sitemap/', 'Today');
        
        return 0;
    }

}
