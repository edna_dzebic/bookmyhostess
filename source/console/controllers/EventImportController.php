<?php

namespace console\controllers;

use Yii;
use common\models\Event;
use common\models\Job;
use common\models\DressCode;
use common\models\EventJob;
use common\models\EventDressCode;
use common\models\Type;
use common\models\City;
use common\models\Country;

class EventImportController extends \yii\console\Controller
{

    public function actionCsv($path)
    {
        Yii::$app->language = "en";
        
        $row = 0;
        if (($handle = fopen($path, "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle)) !== FALSE)
            {
                $row++;
                if($row == 1)
                {
                    continue;
                }                
                
                $this->parseData($row, $data);
            }
            fclose($handle);
        }
    }

    public function actionExcel($inputFile, $to = null)
    {
        Yii::$app->language = "en";
        
        //  Read your Excel workbook
        try
        {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFile);
        } catch (\Exception $e)
        {
            echo 'Error loading file "' .
            pathinfo($inputFile, PATHINFO_BASENAME) . '": ' . $e->getMessage() . "\n";
            return 1;
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        if (isset($to))
        {
            $highestRow = $to;
        }

        // Loop through each row of the worksheet in turn
        for ($row = 2; $row <= $highestRow; $row++)
        {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE)[0];
            
            $this->parseData($row, $rowData);
        }
    }

    private function parseData($row, $rowData)
    {
        if (empty($rowData[0]))
        {
            echo "Data in row not correct : " . $row . "\n";
            return;
        }


        try
        {
            $eventName = trim(htmlspecialchars($rowData[0]));
            $eventVenue = trim(htmlspecialchars($rowData[1]));
            $eventUrl = trim(htmlspecialchars($rowData[2]));
            $eventDescription = trim(htmlspecialchars($rowData[3]));

            $startDate = trim(htmlspecialchars($rowData[4]));
            $endDate = trim(htmlspecialchars($rowData[5]));

            $cityName = strtoupper(trim(htmlspecialchars($rowData[6])));
            $countryName = trim(htmlspecialchars($rowData[7]));

            $typeName = trim(htmlspecialchars($rowData[8]));

            $typeModel = $this->getType($typeName);

            $countryModel = $this->getCountry($countryName);

            if (isset($countryModel))
            {
                $countryId = $countryModel->id;

                $cityModel = $this->getCity($cityName, $countryId);

                if (isset($cityModel))
                {
                    $eventStartDate = $this->formatDate($startDate);

                    if (!isset($eventStartDate))
                    {
                        throw new \Exception("Cannot format date " . $startDate . " in row " . $row);
                    }

                    $eventEndDate = $this->formatDate($endDate);
                    if (!isset($eventEndDate))
                    {
                        throw new \Exception("Cannot format date " . $endDate . " in row " . $row);
                    }

                    $found = $this->findEvent(
                            $eventName, $eventStartDate, $eventEndDate, $cityModel->id, $countryId);

                    if (!$found)
                    {
                        $event = new Event();

                        $event->name = $eventName;
                        $event->venue = $eventVenue;
                        $event->url = $eventUrl;
                        $event->brief_description = $eventDescription;

                        $event->start_date = $eventStartDate;
                        $event->end_date = $eventEndDate;

                        $event->city_id = $cityModel->id;
                        $event->country_id = $countryId;

                        $event->type_id = $typeModel->id;

                        $transaction = Yii::$app->db->beginTransaction();

                        if ($event->save() === false)
                        {
                            $transaction->rollBack();
                            echo "Failed Importing : " . $eventName . " in row " . $row . " \n"
                            . var_export($event->getErrors()) . " \n";
                        } else
                        {
                            if ($this->saveJobsAndDressCodes($event))
                            {
                                $transaction->commit();
                                echo "Event saved " . $eventName . " in row " . $row . "\n";
                            } else
                            {
                                echo "Failed saving jobs and dress codes for : " . $eventName . " in row " . $row . " \n";
                                $transaction->rollBack();
                            }
                        }
                    } else
                    {
                        echo "Event already exists " . $eventName . " in row " . $row . " \n";
                    }
                } else
                {
                    echo "Failed Importing : " . $rowData[0] . " \n" . "City " . $cityName . " not found for country " . $countryId . " \n";
                }
            } else
            {
                echo "Failed Importing : " . $rowData[0] . " \n" . "Country " . $countryName . " not found!" . " \n";
            }
        } catch (\Exception $e)
        {
            echo "\n";
            echo $e->getMessage();
            echo "\n";
        }
    }

    private function formatDate($date)
    {
        $newDate = \DateTime::createFromFormat('d.m.Y.', $date);

        if ($newDate !== false)
        {
            return $newDate->format('Y-m-d');
        }

        return null;
    }

    private function getCountry($countryName)
    {
        return Country::findOne(["name" => $countryName]);
    }

    private function getCity($cityName, $countryId)
    {
        $model = City::findOne(["name" => $cityName, "country_id" => $countryId]);

        if (isset($model))
        {
            return $model;
        }

        return $this->createCity($cityName, $countryId);
    }

    private function createCity($cityName, $countryId)
    {
        $city = new City();
        $city->country_id = $countryId;
        $city->name = $cityName;

        if ($city->save())
        {
            return $city;
        }

        return null;
    }

    private function getType($name)
    {
        return Type::findOne(["name" => $name]);
    }

    private function findEvent($name, $startDate, $endDate, $city, $country)
    {
        return Event::find()
                        ->andWhere(
                                [
                                    "name" => $name,
                                    "start_date" => $startDate,
                                    "end_date" => $endDate,
                                    "city_id" => $city,
                                    "country_id" => $country
                        ])->exists();
    }

    /**
     * By default on creation of new event, all dress codes and jobs must be added to it
     * @return boolean
     */
    private function saveJobsAndDressCodes($event)
    {
        //event jobs
        $eventJobs = Job::find()->all();
        foreach ($eventJobs as $eventJob)
        {
            $eventJobModel = new EventJob();
            $eventJobModel->event_id = $event->id;
            $eventJobModel->job_id = $eventJob->id;

            if ($eventJobModel->save() === false)
            {
                return false;
            }
        }

        //event dress codes
        $dressCodes = DressCode::find()->all();
        foreach ($dressCodes as $eventDressCode)
        {
            $eventDressCodeModel = new EventDressCode();
            $eventDressCodeModel->event_id = $event->id;
            $eventDressCodeModel->event_dress_code_id = $eventDressCode->id;

            if ($eventDressCodeModel->save() === false)
            {
                return false;
            }
        }

        return true;
    }

}
