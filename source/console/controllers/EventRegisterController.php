<?php

namespace console\controllers;

use Yii;
use common\models\Email;
use common\models\User;
use common\models\HostessProfile;

class EventRegisterController extends BaseController
{

    /**
     * This cron job sends an email to hostesses to register for events.
     * Current rule is to send email 5 days after profile is approved.
     * 
     * @return int
     */
    public function actionReminder()
    {
        $query = User::find()
                ->leftJoin("hostess_profile", "user.id = hostess_profile.user_id")
                ->andWhere(["user.status_id" => Yii::$app->status->active])
                ->andWhere(["user.role" => User::ROLE_HOSTESS])
                ->andWhere(["hostess_profile.admin_approved" => HostessProfile::APPROVAL_APPROVED])
                ->andWhere("user.id NOT IN (SELECT register.user_id FROM register)")
                ->andWhere("user.id NOT IN (SELECT request.user_id FROM request)")
                ->andWhere("hostess_profile.approved_at IS NOT NULL")
                ->andWhere(['=', 'DATE(hostess_profile.approved_at)', date('Y-m-d', strtotime('-5 days'))]);

        $models = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        foreach ($models as $model)
        {
            Email::sendEventRegisterReminderEmailToHostess($model);
        }
        
        return 0;
    }

}
