<?php

namespace console\controllers;

use Yii;
use common\models\Request;
use common\models\HostessReview;
use common\models\Email;
use common\models\User;

class HostessReviewController extends BaseController
{

    /**
     * This cron job sends email for review request to exhibitor 
     * The email is sent 7 days after event finishes. 
     * The exhibitor should receive only one email from currently runned instance 
     * Email is sent every time booking is completed and 7 days is passed after event.
     * 
     * @return int
     */
    public function actionSendReviewEmail()
    {
        $query = Request::find()
                ->andWhere(["request.completed" => Request::COMPLETED])
                ->leftJoin("event", "request.event_id = event.id")
                ->andWhere(['=', 'DATE(event.end_date)', date('Y-m-d', strtotime('-7 days'))])
                ->leftJoin("user", "request.requested_user_id = user.id")
                ->andWhere(["user.status_id" => Yii::$app->status->active])
                ->andWhere(["user.role" => User::ROLE_EXHIBITOR])
                ->leftJoin("exhibitor_profile", "request.requested_user_id = exhibitor_profile.user_id");

        // Adela requested to eliminate this
        //        ->andWhere(["exhibitor_profile.review_email_sent" => 0]);

        $models = $query->all();

        $this->printMessage($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        
        // Store codes for each exhibitor group, insures that exhibitor gets only one email for all events/hostesses
        $uniqueGroupCodes = [];
        $sendMail = false;
        foreach ($models as $model)
        {

            $reviewExists = HostessReview::find()->where(
                            [
                                "hostess_id" => $model->user_id,
                                "event_id" => $model->event_id,
                                "exhibitor_id" => $model->requested_user_id
                            ]
                    )->exists();

            if ($reviewExists)
            {
                continue;
            }

            // If group code not set, generate random string
            if (!isset($uniqueGroupCodes[$model->requested_user_id]))
            {
                $uniqueGroupCodes[$model->requested_user_id] = strtotime("now") . rand(1000, 9999);
                $sendMail = true;
            }
            $groupCode = $uniqueGroupCodes[$model->requested_user_id];


            // Create new HostessReview record
            $newModel = new HostessReview();
            $newModel->review_requested = 0;
            $newModel->hostess_id = $model->user_id;
            $newModel->event_id = $model->event_id;
            $newModel->exhibitor_id = $model->requested_user_id;
            $newModel->event_request_id = $model->id;
            $newModel->group_code = $uniqueGroupCodes[$model->requested_user_id];
            $newModel->status_id = Yii::$app->status->pending;

            // After model has been saved, and exhibitor code set first time (insures only one email is sent for the group),
            // and hostess review email successfully sent
            if ($newModel->save() && $sendMail && Email::sendHostessReviewEmailToExhibitor($model, $groupCode))
            {
                // Set flag 'review_email_sent' (exhibitor_profile) to true
                // Adela requested to eliminate this
                //$model->userRequested->exhibitorProfile->review_email_sent = 1;
                //$model->userRequested->exhibitorProfile->save();
                // this here is to avoid multple email send for one group
                $sendMail = false;
            }
        }
        
        return 0;
    }

}
